package fr.irisa.cairn.model.gecos.scop.extractor

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import fr.irisa.cairn.gecos.model.analysis.dataflow.liveness.LivenessAnalyser
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.annotations.IAnnotation
import gecos.annotations.LivenessAnnotation
import gecos.blocks.CompositeBlock
import gecos.core.Procedure
import gecos.core.ProcedureSymbol
import gecos.core.Symbol
import gecos.gecosproject.GecosProject
import gecos.instrs.GenericInstruction
import gecos.instrs.Instruction
import gecos.instrs.SetInstruction
import java.util.List

class LivenessAnalysisPass {  
	
	static boolean VERBOSE = false;

	
	static def debug(String string) {
		if(VERBOSE) println(string)
	}
	
	package GecosProject proj

	new(GecosProject proj) {  
		this.proj = proj
	}
 
 
 
	public def void compute(GecosScopBlock blk) {
		
	}	
	public def void compute() {
		
		debug("Liveness analysis to determine live-in live-out array accesses in SCoPs")

		var List<GecosScopBlock> roots = EMFUtils.eAllContentsInstancesOf(proj, GecosScopBlock);
		// We first replace all scop by a BasicBlock summarizing (at the symbol level) the def/use/kill 
		// hapenning in the Scop.
		
		roots.forEach[r|replaceScopByBasicBlock(r)];
		
		new BuildControlFlow(proj).compute;
		
		for (proc : proj.listProcedures) {  
			var Symbol[] variables = EMFUtils.getAllReferencesOfType(proc.body, Symbol).filter[!(it instanceof ProcedureSymbol)];
			val LivenessAnalyser live = new LivenessAnalyser(proc, variables);
			live.compute();
			debug("done");
			for (b : proc.basicBlocks) {
				debug(
				'''BB «b.number» {
				«pp(b.annotations.get(LivenessAnalyser.LIVEIN_ANNOTATION))»
				«b.instructions»
				} 
				''')
			}
			removeUpdateInstructions(proc)
		}

		replaceBasicBlockByScop		
		for (proc : proj.listProcedures) {  
			for (b : proc.basicBlocks) {
				b.annotations.removeKey(LivenessAnalyser.LIVEIN_ANNOTATION)
				b.annotations.removeKey(LivenessAnalyser.DEFINED_ANNOTATION)
				b.annotations.removeKey(LivenessAnalyser.USED_ANNOTATION)
			}
		}
		new ClearControlFlow(proj).compute; 

		roots.forEach[r|println('''
				live in :«r.liveInSymbols»
				live out :  «r.liveOutSymbols»
			''')];

	}
	
	def private void removeUpdateInstructions(Procedure proc) {
		val setInsts = EMFUtils.eAllContentsInstancesOf(proc, SetInstruction)
		for (setInst : setInsts) {
			val src = setInst.source
			if (src instanceof GenericInstruction) {
				if (src.isNamed("update")) {
					/* for update instructions reconstruct ArrayInstruction */
					val listChildren = src.listChildren()
					val index = listChildren.drop(2).toList
					val setValue = listChildren.get(1)
					val dest = GecosUserInstructionFactory.array(setInst.getDest().copy(), index);
					setInst.setDest(dest);
					setInst.setSource(setValue);
				}
			}
		}
	}

	BiMap<CompositeBlock, GecosScopBlock> map = HashBiMap.create; //<BasicBlock, GecosScopBlock>();
	
	
	/* Goal : search for symbols that are live-in 
	/* Approach : arrays accessed in ScopRead instances that do have at least 
	/* one source/write outside of the scop are live-in 	*/
	def isLiveIn(Symbol rsym, ScopNode block) { 
		val reads = block.listAllReadAccess.filter[rd|rd.sym==rsym]
		debug("\t- Checking if "+ rsym.name + " is live in Scop");
			val rawDeps =ScopISLConverter.getRAWDepenceGraph(rsym,block);
		for (rd:  reads) {
			val rdStmt = rd.enclosingStatement
			val domS = ScopISLConverter.getDomain(rdStmt);    
			debug("\t\t- Read stmt "+ rdStmt);
			debug("\t\t\t- Read access "+ rd);
			debug("\t\t\t- RAW deps for "+rsym.name+" = "+ rawDeps);
			var JNIISLSet domR= null
			for (edge : rawDeps.copy.maps.filter[e|e.inputTupleName==rdStmt.id]) {
				debug("\t\t\t- Edge "+ edge);
				debug("\t\t\t\t- Read stmt domain "+ domS.copy);
			 	if (domR===null) {
			 		domR =edge.getDomain
			 	} else {
			 		domR = domR.union(edge.getDomain)
			 	}
				debug("\t\t\t\t- RAW validity domain"+ domR.copy);
			}
			if (domR===null) {
				debug("\t\t\t\t\t- No source statement")
				return true				
			} else if (domR.isStrictSubset(domS)) {
				debug("\t\t\t\t\t- No source on domain "+domS.copy.subtract(domR.copy))
				debug("\t\t\t\t\t- *"+rsym.name+" is live-in*")
				return true
			}
		}
		
		return false
	}

	def replaceScopByBasicBlock(GecosScopBlock block) {
		 
		debug("Summarizing SCOP for liveness")
		// Live in analysis
		var liveInsSyms = ScopAccessQuery.getAllReadSymbols(block).toSet
		debug("\t-Read symbols "+liveInsSyms.map[s|s.name]);
		val readOnlySyms = ScopAccessAnalyzer.getAllReadOnlyArrays(block)
		liveInsSyms-= readOnlySyms
		debug("\t-Read only symbols "+readOnlySyms.map[s|s.name]);
		var Instruction use =null
		debug("\t-Iterate over symbols "+liveInsSyms.map[s|s.name]);
		liveInsSyms= liveInsSyms.filter[s|isLiveIn(s,block)].toSet
		liveInsSyms+=readOnlySyms
		println('''
		**********************************************
		Symbols «liveInsSyms.map[s|s.name]» are live-in
		***********************************************''')

		val defuseBB=GecosUserBlockFactory.BBlock()
		val liveOutBB=GecosUserBlockFactory.BBlock()
		val cb= GecosUserBlockFactory.CompositeBlock(defuseBB,liveOutBB)
		
		use = GecosUserInstructionFactory.generic("use" ,null, liveInsSyms.map[s|GecosUserInstructionFactory.symbref(s)].toList)				
		var defOutSyms = block.listAllWriteAccess.map[w|w.sym].toSet
		//val writtenSyms = ScopAccessQuery.getAllWrittenSymbols(block)
		for (wsym : defOutSyms) {  
			defuseBB.instructions+= GecosUserInstructionFactory.set(wsym as Symbol,use.copy);
		}
		
		
		map.put(cb,block)
		for(i:defuseBB.instructions) {
			debug(i.toString);
		}
		block.parent.replace(block,cb);
	}
	
	def replaceBasicBlockByScop() {
		for(cb: map.keySet) {
			val defuse = cb.children.get(0);
			val liveOut = cb.children.get(1);

			val live_in = defuse.annotations.get(LivenessAnalyser.LIVEIN_ANNOTATION) as LivenessAnnotation
			val live_out = liveOut.annotations.get(LivenessAnalyser.LIVEIN_ANNOTATION) as LivenessAnnotation

			val scop = map.get(cb);
			scop.liveInSymbols+=live_in.refs
			scop.liveOutSymbols+=live_out.refs
			
			cb.parent.replace(cb,scop)
		}
	}

	def dispatch pp(IAnnotation annotation) {
		
	}
	def dispatch pp(LivenessAnnotation annotation) {
		'''live : «annotation.refs.map[s|s.name]»''' 
		
	}
}
 