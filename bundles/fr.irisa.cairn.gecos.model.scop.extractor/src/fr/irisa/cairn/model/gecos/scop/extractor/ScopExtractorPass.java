/*******************************************************************************
 * Copyright (c) 2018 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet; 
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.FixTypeContainers;
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;
import fr.irisa.cairn.gecos.model.transforms.initializationexposition.ExposeVariableInitialization;
import fr.irisa.cairn.gecos.model.transforms.others.RemoveUnnecessaryCasts;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.BooleanDisjunctions;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.Linearizer;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ScopAccessAnalyzer;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ScopDomainAnalyser;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.SideBySideAnalyser;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.SimpleForLoopExtractor;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager;
import fr.irisa.cairn.tools.ecore.DanglingAnalyzer;
import fr.irisa.cairn.tools.ecore.DanglingAnalyzer.DanglingException;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotatedElement;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.instrs.Instruction;
import gecos.types.Type; 

public class ScopExtractorPass {

	GecosProject proj;
	public static boolean VERBOSE = false;
	public static boolean CHECK_DANGLING = false;

	public static void debug(String message) {
		if (VERBOSE) {
			System.out.println(message);
		}
	}
	private Collection<Procedure> procs;
	private Map<Block, GecosScopBlock> scopMap;
	private Map<Block, GecosScopBlock> multipleStatementScopMap;
	private Map<Block, Block> scopBlockToBlock;
	private Map<Instruction, Instruction> ScopStatementToBlock;
	private Map<Instruction, ScopRead> scopReadToBlock;
	private Map<Instruction, ScopWrite> scopWriteToBlock;

	private void initMap() {
		scopMap = new HashMap<Block, GecosScopBlock>();
		multipleStatementScopMap = new HashMap<Block, GecosScopBlock>();
		scopBlockToBlock = new HashMap<Block, Block>();
		ScopStatementToBlock = new HashMap<Instruction, Instruction>();
		scopReadToBlock = new HashMap<Instruction, ScopRead>();
		scopWriteToBlock = new HashMap<Instruction, ScopWrite>();
	}

	public ScopExtractorPass() {
		this.procs = new ArrayList<Procedure>(0);
		initMap();
	}

	public ScopExtractorPass(GecosSourceFile file) {
		this(file.getModel());
	}

	public ScopExtractorPass(ProcedureSet ps) {
		this.procs = ps.listProcedures();
		initMap();
	}

	public ScopExtractorPass(Procedure proc) {
		this.procs = new ArrayList<Procedure>(1);
		this.procs.add(proc);
		initMap();
	}

	public ScopExtractorPass(GecosProject project) {
		proj=project;
		this.procs = new ArrayList<Procedure>();
		for (ProcedureSet ps : project.listProcedureSets()) {
			this.procs.addAll(ps.listProcedures());
		}
		initMap();
	}

	
	public void detectScop() {
		try {
			new FixTypeContainers(proj).compute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		boolean containsForBlock = false;
		if(!procs.isEmpty()) {
			for (Procedure p : this.procs) {
	
				
				containsForBlock |= !EMFUtils.eAllContentsInstancesOf(p, ForBlock.class).isEmpty();
				if (containsForBlock) {
					List<GecosScopBlock> scopRoots = EMFUtils.eAllContentsInstancesOf(p, GecosScopBlock.class);
					// cannot apply ExposeVariableInitialization when there is SCoP in the
					// Procedure: this pass uses control edges, which are not handle by
					// GecosScopBlock objects.
					CompositeBlock body = p.getBody();
					if (scopRoots.isEmpty()) {
						new RemoveUnnecessaryCasts(body).compute();
						new ExposeVariableInitialization(body.getContainingProcedure()).compute();
					}
					extractScopInBlock(p.getSymbolName(), body);
				}
				SideBySideAnalyser.eInstance.compute(scopMap, this);
				new ClearControlFlow(p).compute();
			}
		} 
		if (CHECK_DANGLING) try {
			DanglingAnalyzer analyzer = new DanglingAnalyzer(proj);
			analyzer.hasDangling(proj);
		} catch (DanglingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Collection<GecosScopBlock> insertScop() {
		for (Block block : scopMap.keySet()) {
			GecosScopBlock GecosScopBlock = scopMap.get(block);

			if (block instanceof ForBlock) {
				debug("Found ForBlock " + block + " as ScopRoot");
				block.getParent().replace(block, GecosScopBlock);
			} else if (block instanceof CompositeBlock) {
				block.getParent().replace(block, GecosScopBlock);
				debug("Found Composite " + block + " as ScopRoot");
			} else if (block instanceof IfBlock) {
				block.getParent().replace(block, GecosScopBlock);
				debug("Found If " + block + " as ScopRoot");
			} else if (block instanceof BasicBlock) {
				debug("Found BasicBlock " + block + " as ScopRoot");
			}

		}
		HashSet<GecosScopBlock> scops = new HashSet(multipleStatementScopMap.values());
		for (GecosScopBlock scop : scops) { 
			debug("Found Merged ScopRoot "+scop.getName());
			boolean first =true;
			for (Block block : multipleStatementScopMap.keySet()) { 
				if (multipleStatementScopMap.get(block)==scop) {
					Block parent = block.getParent();
					if(first) {
						debug("- Replacing "+block+" in "+parent);
						parent.replace(block, scop);
						first=false;
					} else {
						debug("- Removing "+block+" from IR ");
						parent.removeBlock(block);
					}
				}
			}
		}

		
		List<ScopDimension> dims = EMFUtils.eAllContentsInstancesOf(proj,ScopDimension.class);
		for (ScopDimension dim : dims) {
			if (dim.getSymbol().eContainer()==null) {
				System.out.println("No container for "+dim);
			}
		}

		if (CHECK_DANGLING) 
			try {
				DanglingAnalyzer analyzer = new DanglingAnalyzer(proj);
				analyzer.hasDangling(proj);
			} catch (DanglingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		// FIXME
		Collection<GecosScopBlock> scopRoots = new HashSet<GecosScopBlock>();
		scopRoots.addAll(scopMap.values());
		scopRoots.addAll(multipleStatementScopMap.values());
		fix(scopRoots);
		// Compute Array symbol level Liveness Info
		new LivenessAnalysisPass(proj).compute();
		return scopRoots;
	}
	
	public Collection<GecosScopBlock> compute() {
		detectScop();
		// 
		Collection<GecosScopBlock> scopRoots = insertScop();
		
		return scopRoots;
	}

	/*
	 * FIXME : start fix ADAInput : at the time of this implementation, the scop
	 * extrator does not build the input according to the new interfaces of 
	 * dataflow. This fixes it.
	 */
	private void fix(Collection<GecosScopBlock> values) {
		for (GecosScopBlock r : values) {
			ExposeVariableInitialization.fixReferencesAfterCopy(r);
			ScopLabelFixer.fixLabels(r);   
		}
	}



	/*
	 * FIXME : end
	 */

	public Map<Block, GecosScopBlock> getScopMap() {
		return scopMap;
	}

	public Map<Block, GecosScopBlock> getMultipleStatementScopMap() {
		return multipleStatementScopMap;
	}

	public Map<Block, Block> getScopBlockToBlock() {
		return scopBlockToBlock;
	}

	public Map<Instruction, Instruction> getScopStatementToBlock() {
		return ScopStatementToBlock;
	}

	public Map<Instruction, ScopRead> getScopReadToBlock() {
		return scopReadToBlock;
	}

	public Map<Instruction, ScopWrite> getScopWriteToBlock() {
		return scopWriteToBlock;
	}

	
	private void extractScopInProcedure(Procedure p) {
		extractScopInBlock(p.getSymbolName(), p.getBody());
	}

	public void extractScopInBlock(String name, Block blk) {
		
		boolean containsForBlock =false;
		 containsForBlock |= !EMFUtils.eAllContentsInstancesOf(blk, ForBlock.class).isEmpty();
		if (containsForBlock) {
			List<GecosScopBlock> scopRoots = EMFUtils.eAllContentsInstancesOf(blk, GecosScopBlock.class);
			// cannot apply ExposeVariableInitialization when there is SCoP in the
			// Procedure: this pass uses control edges, which are not handle by
			// GecosScopBlock objects.
			
			if (scopRoots.isEmpty()) {
				new RemoveUnnecessaryCasts(blk).compute();
				new ExposeVariableInitialization(blk.getContainingProcedure()).compute();
			}
		
			ScopFilter filter = new ScopFilter();
			int counter = 0;
			debug("\nFiltering function " + name + " for SCoP candidates\n	");
			filter.check(blk);
			List<Block> candidates = filter.candidates;
			if (candidates.size() == 0) {
				debug("Filtering results : No SCoP candidate in " + name);
			} else {
				while (candidates.size() != 0) {
					int oldsize = candidates.size();
					EList<Block> copy = new BasicEList<Block>(candidates);
					for (Block b : copy) {
						if (candidates.contains(b)) {
							debug("\nAnalyzing candidate " + b+ "\n");
							try {
								/** Try to build a Scop from the current block */
								GecosScopBlock candidateScop = buildSCoP(b);
								/** Give a proper name to the SCoP */
								candidateScop.setName(name+ "/scop_" + (counter++));
								candidateScop.setNumber( (counter++));
								debug("***********************************");
								debug("Found SCoP : " + candidateScop + "");
								debug("***********************************");
								/**
								 * If it did not went wrong (i.e no exception
								 * thrown) we add it to the scop map for later use
								 */
								scopMap.put(b, candidateScop);

								/**
								 * Because current is a SCoP, no need to check for
								 * its children, so we remove them all from the
								 * candidate list
								 **/
								List<Block> children = EMFUtils.eAllContentsInstancesOf(b, Block.class);
								candidates.removeAll(children);
								candidates.remove(b);
								break;
							} catch (SCoPException e) {
								/**
								 * the candidate is not a SCoP, we remove it from
								 * the list as it has already been analyzed
								 **/
								debug("Failed : Block " + b + "is not a SCoP : " + e.getMessage());
								debug("Removing it from candidate List");
								candidates.remove(b);
							}
							if (oldsize == candidates.size()) {
								throw new RuntimeException("Fixpoint Error !");
							} else {
								oldsize = candidates.size();
								debug("Only " + oldsize + " block left to analyze");
							}
						}
					}
				}
			}

		}
		SideBySideAnalyser.eInstance.compute(scopMap, this);
		new ClearControlFlow(blk.getContainingProcedure()).compute();

	}

	public GecosScopBlock buildSCoP(Block b) throws SCoPException {

		debug("\nbuild SCOP for candidate block "+b);

		Block candidateScop = copyAnnotatedElement(b);
		List<Scope> scopes = EMFUtils.eAllContentsInstancesOf(candidateScop, Scope.class);
		debug("\nFind disjunction expression\n**************************************");
		
		BooleanDisjunctions booleanDisjunctions = new BooleanDisjunctions();
		booleanDisjunctions.transform(candidateScop);

		debug("\nLinearizing expressions\n**************************************");

		Linearizer linearizer = new Linearizer();
		VariableContextManager context = linearizer.transform(candidateScop);

		debug("\nAnalyzing array/scalar accesses\n**************************************");
		ScopAccessAnalyzer stmtAnalyzer = new ScopAccessAnalyzer();
		stmtAnalyzer.transform(candidateScop);

		debug("\nAnalyzing domain/blocks\n**************************************");
		ScopDomainAnalyser domAnalyzer = new ScopDomainAnalyser(context);
		candidateScop = domAnalyzer.transform(candidateScop);
		
		// Copy symbols and types in new scope
		GecosScopBlock scop = (GecosScopBlock) candidateScop;
		scop.setScope(GecosUserCoreFactory.scope(new ArrayList<Symbol>()));
		for(Scope s : scopes) {
			scop.getScope().getSymbols().addAll(s.getSymbols());
			scop.getScope().getTypes().addAll(s.getTypes());
		}
		
		return scop;
	}

	/**
	 * Copy an annotated element and add reference adapters.
	 * 
	 * @param reference
	 * @return
	 */
	public static <A extends AnnotatedElement> A copyAnnotatedElement(A reference) {
		GecosCopier copier = new GecosCopier();
		@SuppressWarnings("unchecked")
		A copy = (A) copier.copy(reference);
		copier.copyReferences();
		return copy;
	}
}