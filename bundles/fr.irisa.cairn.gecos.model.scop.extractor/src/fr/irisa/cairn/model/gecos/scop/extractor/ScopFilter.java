/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RetInstruction;

public class ScopFilter extends BlocksDefaultSwitch<Boolean> {
	
	public static final String IGNORE_SCOP_PRAGMA = "gcs_scop_ignore";
	
	public static boolean VERBOSE = false && ScopExtractorPass.VERBOSE;
	
	public static void debug(String string) {
		if (VERBOSE) {
			System.out.println(string);
		}
	}
	
	List<Block> candidates;

	public ScopFilter() {
		candidates = new BasicEList<Block>();
	}
	
	public boolean check(Block b) {
		checkPragma(b);
		try {
			debug("  - Target block "+b);
			return doSwitch(b);
		} catch (SCoPException e) {
			return false;
		}
	}
	
	@Override
	public Boolean caseDoWhileBlock(DoWhileBlock loopBlock) {
		checkPragma(loopBlock);
		debug("     - Found do {} while block "+loopBlock);
		doSwitch(loopBlock.getBodyBlock());
		return true;
	}

	@Override
	public Boolean caseWhileBlock(WhileBlock whileBlock) {
		checkPragma(whileBlock);
		debug("     - Found while {} block "+whileBlock);
		doSwitch(whileBlock.getBodyBlock());
		return true;
	}

	@Override
	public Boolean caseForBlock(ForBlock forBlock) {
		checkPragma(forBlock);
		debug("     - Dirty ForBlock "+forBlock);
		doSwitch(forBlock.getBodyBlock());
		return true;
	}

	@Override
	public Boolean caseSwitchBlock(SwitchBlock b) {
		checkPragma(b);
		throw new SCoPException(b, "     - Found switch {} block "+b);
	}

	@Override
	public Boolean caseSimpleForBlock(SimpleForBlock simpleForBlock) {
		checkPragma(simpleForBlock);
		debug("     - Found SimpleForBlock "+simpleForBlock);
		doSwitch(simpleForBlock.getBodyBlock());
		if (simpleForBlock.getStride().isConstant())
			// if no exception was caught, then it is a candidate
			candidates.add(0,simpleForBlock);
		return true;
	}

	@Override
	public Boolean caseCompositeBlock(CompositeBlock b) {
		checkPragma(b);
		debug("     - Found CompositeBlock : "+b);
		
		// do not call the super doSwitch : we have to visit all the blocks,
		// whereas the super doswitch would stop after the first thrown
		// ScopException
//		super.caseCompositeBlock(compositeBlock);

		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Block[] tab = (Block[]) b.getChildren().toArray();
		boolean ok = true;
		SCoPException cause = null;
		for (Block block  : tab) {
			try {
				doSwitch(block);
			} catch (SCoPException e) {
				ok = false;
				cause = e;
				continue;
			}
		}
		
		if (ok && b.getParent() != null)
			// if no exception was caught, then it is a candidate
			candidates.add(0,b);
		
		if (!ok) throw cause;
		return true;
	}

	@Override
	public Boolean caseIfBlock(IfBlock b) {
		checkPragma(b);
		debug("     - Found IfBlock "+b);
		boolean ok = true;
		SCoPException cause = null;
		try {
			if (b.getThenBlock() != null)
				doSwitch(b.getThenBlock());
		} catch (SCoPException e) {
			ok = false;
			cause = e;
		}
		if (b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		
		if (ok) candidates.add(0,b);
		else throw cause;
		return true;
	}

	@Override
	public Boolean caseBasicBlock(BasicBlock b) {
		checkPragma(b);
		debug("     - BB : "+b);
//		try {
			for(Instruction i : b.getInstructions()) {
				SCoPInstructionFilter.eInstance.doSwitch(i);
			}
//		} catch (Throwable t) {
//			System.out.println("tt : "+t);
//			throw t;
//		}
		return true;
	}

	private void checkPragma(Block b) {
		if (b.getPragma() != null)
			for (String str : b.getPragma().getContent())
				if (str.equals(IGNORE_SCOP_PRAGMA))
					throw new SCoPException(b, "Not a SCoP : Annotation #pragma "+IGNORE_SCOP_PRAGMA+" found.");
	}
	
	public static class SCoPInstructionFilter extends DefaultInstructionSwitch<Object> {

		public static SCoPInstructionFilter eInstance =  new SCoPInstructionFilter();

		
		@Override
		public Object caseCallInstruction(CallInstruction callInst) {
			BasicBlock bb= callInst.getBasicBlock();
			Symbol procSym  = callInst.getProcedureSymbol();
			if(procSym!=null) {
				PragmaAnnotation pragma = procSym.getPragma();
				if(pragma!=null) {
					for(String string : pragma.getContent()) {
						if(string.matches(GecosUserAnnotationFactory.PRAGMA_PURE_FUNCTION)) {
							debug("Function "+procSym.getName()+ " is a 'pure' function");
							return super.caseCallInstruction(callInst);
						} 
					}
				}
				String message = "Not a SCoP: the following call to "+procSym.getName()+" can be annotated using the "+GecosUserAnnotationFactory.PRAGMA_PURE_FUNCTION+" pragma";
				debug(message);
				throw new SCoPException(bb, message);
			} else {
				String message = "Not a SCoP: Complex function calls such as :" + callInst+ " not supported";
				throw new SCoPException(bb,message );
			}
		}


		@Override
		public Object caseMethodCallInstruction(MethodCallInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "MethodCallInstruction "+object+" is not Scopable");
		}

		@Override
		public Object casePhiInstruction(PhiInstruction g) {
			throw new SCoPException(g.getBasicBlock(), "PhyInstruction are not Scopable");
		}

		@Override
		public Object caseNumberedSymbolInstruction(NumberedSymbolInstruction s) {
			throw new SCoPException(s.getBasicBlock(), "NumberedSymbolInstruction "+s+" is not Scopable");
		}

		@Override
		public Object caseArrayValueInstruction(ArrayValueInstruction instruction) {
			throw new SCoPException(instruction.getBasicBlock(), "ArrayValueInstruction " +instruction+" is not Scopable");
		}

		@Override
		public Object caseIndirInstruction(IndirInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "IndirInstruction "+object+" is not Scopable");
		}

		@Override
		public Object caseRetInstruction(RetInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "RetInstruction "+object+" is not Scopable");
		}

		@Override
		public Object caseBreakInstruction(BreakInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "BreakInstruction "+object+" is not Scopable");
		}

		@Override
		public Object caseGotoInstruction(GotoInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "GotoInstruction "+object+" is not Scopable");
		}

		@Override
		public Object caseContinueInstruction(ContinueInstruction object) {
			throw new SCoPException(object.getBasicBlock(), "ContinueInstruction "+object+" is not Scopable");
		}
		
	}
	
}