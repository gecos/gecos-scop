package fr.irisa.cairn.model.gecos.scop.extractor

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import java.util.HashMap
import gecos.instrs.LabelInstruction
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement

class ScopLabelFixer {
	
	static boolean VERBOSE = false; 
	
	static def debug(String mess) {
		if (VERBOSE) println(mess);
	}
	
	static def ScopStatement nextInBlock(ScopStatement s) {
		val blk = s.parentScop
		if (blk instanceof ScopBlock) {
			val pos = blk.children.indexOf(s)
			if (pos< blk.children.size) {
				val next = blk.children.get(pos+1)
				if (next instanceof ScopStatement) return next 
			}
		}
		null
	}
	static def fixLabels(GecosScopBlock root) {
		var map = new HashMap<String, ScopStatement>();
		val stmts = root.listAllStatements
		var int pos =0;
		for (s : stmts) 	{
			s.id=("S"+pos++);
			map.put(s.id,s);
		}
		for (s:  stmts.filter(ScopInstructionStatement)) {
			val instr = s.children.get(0)
			switch(instr) {
				LabelInstruction : {
					val next = nextInBlock(s)
					if (next!==null) {
						debug("Found Label instruction "+instr.name+" to be associated to "+next)
						var newName = instr.name
						if (map.containsKey(newName) && next!= map.get(newName)) {
							val stmt = map.get(newName)
							debug("\tLabel "+instr.name+" is already used by "+stmt)
							map.remove(stmt.id);
							debug("\tRenaming "+stmt)
							stmt.id = "_"+stmt.id
							while (map.containsKey(stmt.id))
								stmt.id = "_"+stmt.id
							map.put(stmt.id,stmt)
							debug("\tInto "+stmt)
						} 
						debug("\tRenaming "+next)
						map.remove(next.id)
						next.id=newName
						debug("\tInto "+next)
						map.put(newName,next)
						
					}
					s.parentScop.remove(s)
					debug("Removing LabelInstruction "+instr.name+" to be associated to "+s)
				}
			}
		}
		for(s:root.listAllStatements) {
			debug("Statement "+s)
		}
		
		
	}
}