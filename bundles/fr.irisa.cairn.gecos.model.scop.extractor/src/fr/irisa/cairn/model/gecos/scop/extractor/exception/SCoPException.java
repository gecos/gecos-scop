/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.exception;

import gecos.blocks.Block;

public class SCoPException extends RuntimeException{	
	public static boolean VERBOSE = true;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2852067166815357260L;
	private Block block;


	public SCoPException(Block b, String message) {
		super(message);
		this.block = b;
		if (VERBOSE) System.err.println(message);
		
	}


	public SCoPException(Block b, String message, Exception cause) {
		super(message,cause);
		this.block = b;
		if (VERBOSE) System.err.println(message);
	}
	
	public Block getBlock() {
		return block;
	}
	
	public void setBlock(Block block) {
		this.block = block;
	}
}