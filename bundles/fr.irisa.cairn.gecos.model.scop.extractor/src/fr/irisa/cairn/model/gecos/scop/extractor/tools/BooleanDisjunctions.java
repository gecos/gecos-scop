package fr.irisa.cairn.model.gecos.scop.extractor.tools;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class BooleanDisjunctions extends GenericInstructionTransformation {

private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_InnermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId())),tom_empty_list_Sequence()))))
;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_is_fun_sym_or(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("or");
}
private static  EList<Instruction>  tom_get_slot_or_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_le(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("le");
}
private static  EList<Instruction>  tom_get_slot_le_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_lt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("lt");
}
private static  EList<Instruction>  tom_get_slot_lt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_ge(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("ge");
}
private static  EList<Instruction>  tom_get_slot_ge_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_gt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("gt");
}
private static  EList<Instruction>  tom_get_slot_gt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_eq(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("eq");
}
private static  EList<Instruction>  tom_get_slot_eq_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}


protected Instruction apply(Instruction instruction) throws VisitFailure{
Instruction res = 
tom_make_InnermostId(tom_make_Disjunctions()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
return res;
}


private static boolean tom_is_fun_sym_boolconjunction(Instruction t) {
return  (t instanceof GenericInstruction) && ((GenericInstruction)t).getName().equals("boolconjunction");
}
private static  EList<Instruction>  tom_get_slot_boolconjunction_list(Instruction t) {
return  enforce(((GenericInstruction)t).getChildren()) ;
}
private static boolean tom_is_fun_sym_booldisjunction(Instruction t) {
return  (t instanceof GenericInstruction) && ((GenericInstruction)t).getName().equals("booldisjunction");
}
private static Instruction tom_make_booldisjunction( EList<Instruction>  l) { 
return  fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric("booldisjunction",l)  ;
}
private static  EList<Instruction>  tom_get_slot_booldisjunction_list(Instruction t) {
return  enforce(((GenericInstruction)t).getChildren()) ;
}
public static class Disjunctions extends tom.library.sl.AbstractStrategyBasic {
public Disjunctions() {
super(tom_make_Identity());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_1=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_1))) {
int tomMatch1_5=0;
if (!(tomMatch1_5 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tomMatch1_9=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_5);
boolean tomMatch1_31= false ;
 EList<Instruction>  tomMatch1_8= null ;
Instruction tomMatch1_13= null ;
Instruction tomMatch1_11= null ;
Instruction tomMatch1_12= null ;
Instruction tomMatch1_14= null ;
Instruction tomMatch1_10= null ;
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_9))) {
{
tomMatch1_31= true ;
tomMatch1_10=tomMatch1_9;
tomMatch1_8=tom_get_slot_gt_children(tomMatch1_10);
}
} else {
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_9))) {
{
tomMatch1_31= true ;
tomMatch1_11=tomMatch1_9;
tomMatch1_8=tom_get_slot_lt_children(tomMatch1_11);
}
} else {
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_9))) {
{
tomMatch1_31= true ;
tomMatch1_12=tomMatch1_9;
tomMatch1_8=tom_get_slot_ge_children(tomMatch1_12);
}
} else {
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_9))) {
{
tomMatch1_31= true ;
tomMatch1_13=tomMatch1_9;
tomMatch1_8=tom_get_slot_le_children(tomMatch1_13);
}
} else {
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_9))) {
{
tomMatch1_31= true ;
tomMatch1_14=tomMatch1_9;
tomMatch1_8=tom_get_slot_eq_children(tomMatch1_14);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_31) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_8))) {
int tomMatch1_23=0;
if (!(tomMatch1_23 >= tom_get_size_InstL_InstL(tomMatch1_8))) {
int tomMatch1_24=tomMatch1_23 + 1;
if (!(tomMatch1_24 >= tom_get_size_InstL_InstL(tomMatch1_8))) {
if (tomMatch1_24 + 1 >= tom_get_size_InstL_InstL(tomMatch1_8)) {
int tomMatch1_6=tomMatch1_5 + 1;
if (!(tomMatch1_6 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tomMatch1_16=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_6);
boolean tomMatch1_30= false ;
Instruction tomMatch1_20= null ;
Instruction tomMatch1_17= null ;
Instruction tomMatch1_21= null ;
 EList<Instruction>  tomMatch1_15= null ;
Instruction tomMatch1_19= null ;
Instruction tomMatch1_18= null ;
if (tom_is_sort_Inst(tomMatch1_16)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_16))) {
{
tomMatch1_30= true ;
tomMatch1_17=tomMatch1_16;
tomMatch1_15=tom_get_slot_gt_children(tomMatch1_17);
}
} else {
if (tom_is_sort_Inst(tomMatch1_16)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_16))) {
{
tomMatch1_30= true ;
tomMatch1_18=tomMatch1_16;
tomMatch1_15=tom_get_slot_lt_children(tomMatch1_18);
}
} else {
if (tom_is_sort_Inst(tomMatch1_16)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_16))) {
{
tomMatch1_30= true ;
tomMatch1_19=tomMatch1_16;
tomMatch1_15=tom_get_slot_ge_children(tomMatch1_19);
}
} else {
if (tom_is_sort_Inst(tomMatch1_16)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_16))) {
{
tomMatch1_30= true ;
tomMatch1_20=tomMatch1_16;
tomMatch1_15=tom_get_slot_le_children(tomMatch1_20);
}
} else {
if (tom_is_sort_Inst(tomMatch1_16)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_16))) {
{
tomMatch1_30= true ;
tomMatch1_21=tomMatch1_16;
tomMatch1_15=tom_get_slot_eq_children(tomMatch1_21);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_30) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_15))) {
int tomMatch1_27=0;
if (!(tomMatch1_27 >= tom_get_size_InstL_InstL(tomMatch1_15))) {
int tomMatch1_28=tomMatch1_27 + 1;
if (!(tomMatch1_28 >= tom_get_size_InstL_InstL(tomMatch1_15))) {
if (tomMatch1_28 + 1 >= tom_get_size_InstL_InstL(tomMatch1_15)) {
if (tomMatch1_6 + 1 >= tom_get_size_InstL_InstL(tomMatch1_1)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_6),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_5),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_33=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_33))) {
int tomMatch1_37=0;
if (!(tomMatch1_37 >= tom_get_size_InstL_InstL(tomMatch1_33))) {
Instruction tomMatch1_41=tom_get_element_InstL_InstL(tomMatch1_33,tomMatch1_37);
if (tom_is_sort_Inst(tomMatch1_41)) {
if (tom_is_fun_sym_boolconjunction(((Instruction)tomMatch1_41))) {
int tomMatch1_38=tomMatch1_37 + 1;
if (!(tomMatch1_38 >= tom_get_size_InstL_InstL(tomMatch1_33))) {
Instruction tomMatch1_44=tom_get_element_InstL_InstL(tomMatch1_33,tomMatch1_38);
boolean tomMatch1_53= false ;
Instruction tomMatch1_49= null ;
Instruction tomMatch1_47= null ;
Instruction tomMatch1_46= null ;
Instruction tomMatch1_45= null ;
Instruction tomMatch1_48= null ;
 EList<Instruction>  tomMatch1_43= null ;
if (tom_is_sort_Inst(tomMatch1_44)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_44))) {
{
tomMatch1_53= true ;
tomMatch1_45=tomMatch1_44;
tomMatch1_43=tom_get_slot_gt_children(tomMatch1_45);
}
} else {
if (tom_is_sort_Inst(tomMatch1_44)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_44))) {
{
tomMatch1_53= true ;
tomMatch1_46=tomMatch1_44;
tomMatch1_43=tom_get_slot_lt_children(tomMatch1_46);
}
} else {
if (tom_is_sort_Inst(tomMatch1_44)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_44))) {
{
tomMatch1_53= true ;
tomMatch1_47=tomMatch1_44;
tomMatch1_43=tom_get_slot_ge_children(tomMatch1_47);
}
} else {
if (tom_is_sort_Inst(tomMatch1_44)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_44))) {
{
tomMatch1_53= true ;
tomMatch1_48=tomMatch1_44;
tomMatch1_43=tom_get_slot_le_children(tomMatch1_48);
}
} else {
if (tom_is_sort_Inst(tomMatch1_44)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_44))) {
{
tomMatch1_53= true ;
tomMatch1_49=tomMatch1_44;
tomMatch1_43=tom_get_slot_eq_children(tomMatch1_49);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_53) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_43))) {
int tomMatch1_51=0;
if (!(tomMatch1_51 >= tom_get_size_InstL_InstL(tomMatch1_43))) {
if (tomMatch1_51 + 1 >= tom_get_size_InstL_InstL(tomMatch1_43)) {
if (tomMatch1_38 + 1 >= tom_get_size_InstL_InstL(tomMatch1_33)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_33,tomMatch1_38),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_33,tomMatch1_37),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_55=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_55))) {
int tomMatch1_59=0;
if (!(tomMatch1_59 >= tom_get_size_InstL_InstL(tomMatch1_55))) {
Instruction tomMatch1_63=tom_get_element_InstL_InstL(tomMatch1_55,tomMatch1_59);
boolean tomMatch1_75= false ;
Instruction tomMatch1_67= null ;
 EList<Instruction>  tomMatch1_62= null ;
Instruction tomMatch1_66= null ;
Instruction tomMatch1_68= null ;
Instruction tomMatch1_64= null ;
Instruction tomMatch1_65= null ;
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_63))) {
{
tomMatch1_75= true ;
tomMatch1_64=tomMatch1_63;
tomMatch1_62=tom_get_slot_gt_children(tomMatch1_64);
}
} else {
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_63))) {
{
tomMatch1_75= true ;
tomMatch1_65=tomMatch1_63;
tomMatch1_62=tom_get_slot_lt_children(tomMatch1_65);
}
} else {
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_63))) {
{
tomMatch1_75= true ;
tomMatch1_66=tomMatch1_63;
tomMatch1_62=tom_get_slot_ge_children(tomMatch1_66);
}
} else {
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_63))) {
{
tomMatch1_75= true ;
tomMatch1_67=tomMatch1_63;
tomMatch1_62=tom_get_slot_le_children(tomMatch1_67);
}
} else {
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_63))) {
{
tomMatch1_75= true ;
tomMatch1_68=tomMatch1_63;
tomMatch1_62=tom_get_slot_eq_children(tomMatch1_68);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_75) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_62))) {
int tomMatch1_73=0;
if (!(tomMatch1_73 >= tom_get_size_InstL_InstL(tomMatch1_62))) {
if (tomMatch1_73 + 1 >= tom_get_size_InstL_InstL(tomMatch1_62)) {
int tomMatch1_60=tomMatch1_59 + 1;
if (!(tomMatch1_60 >= tom_get_size_InstL_InstL(tomMatch1_55))) {
Instruction tomMatch1_70=tom_get_element_InstL_InstL(tomMatch1_55,tomMatch1_60);
if (tom_is_sort_Inst(tomMatch1_70)) {
if (tom_is_fun_sym_boolconjunction(((Instruction)tomMatch1_70))) {
if (tomMatch1_60 + 1 >= tom_get_size_InstL_InstL(tomMatch1_55)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_55,tomMatch1_60),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_55,tomMatch1_59),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_77=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_77))) {
int tomMatch1_81=0;
if (!(tomMatch1_81 >= tom_get_size_InstL_InstL(tomMatch1_77))) {
Instruction tomMatch1_85=tom_get_element_InstL_InstL(tomMatch1_77,tomMatch1_81);
if (tom_is_sort_Inst(tomMatch1_85)) {
if (tom_is_fun_sym_boolconjunction(((Instruction)tomMatch1_85))) {
int tomMatch1_82=tomMatch1_81 + 1;
if (!(tomMatch1_82 >= tom_get_size_InstL_InstL(tomMatch1_77))) {
Instruction tomMatch1_88=tom_get_element_InstL_InstL(tomMatch1_77,tomMatch1_82);
if (tom_is_sort_Inst(tomMatch1_88)) {
if (tom_is_fun_sym_boolconjunction(((Instruction)tomMatch1_88))) {
if (tomMatch1_82 + 1 >= tom_get_size_InstL_InstL(tomMatch1_77)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_77,tomMatch1_82),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_77,tomMatch1_81),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_91=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_91))) {
int tomMatch1_95=0;
if (!(tomMatch1_95 >= tom_get_size_InstL_InstL(tomMatch1_91))) {
Instruction tomMatch1_99=tom_get_element_InstL_InstL(tomMatch1_91,tomMatch1_95);
if (tom_is_sort_Inst(tomMatch1_99)) {
if (tom_is_fun_sym_booldisjunction(((Instruction)tomMatch1_99))) {
int tomMatch1_96=tomMatch1_95 + 1;
if (!(tomMatch1_96 >= tom_get_size_InstL_InstL(tomMatch1_91))) {
Instruction tomMatch1_102=tom_get_element_InstL_InstL(tomMatch1_91,tomMatch1_96);
boolean tomMatch1_112= false ;
Instruction tomMatch1_104= null ;
Instruction tomMatch1_103= null ;
 EList<Instruction>  tomMatch1_101= null ;
Instruction tomMatch1_105= null ;
Instruction tomMatch1_106= null ;
Instruction tomMatch1_107= null ;
if (tom_is_sort_Inst(tomMatch1_102)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_102))) {
{
tomMatch1_112= true ;
tomMatch1_103=tomMatch1_102;
tomMatch1_101=tom_get_slot_gt_children(tomMatch1_103);
}
} else {
if (tom_is_sort_Inst(tomMatch1_102)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_102))) {
{
tomMatch1_112= true ;
tomMatch1_104=tomMatch1_102;
tomMatch1_101=tom_get_slot_lt_children(tomMatch1_104);
}
} else {
if (tom_is_sort_Inst(tomMatch1_102)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_102))) {
{
tomMatch1_112= true ;
tomMatch1_105=tomMatch1_102;
tomMatch1_101=tom_get_slot_ge_children(tomMatch1_105);
}
} else {
if (tom_is_sort_Inst(tomMatch1_102)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_102))) {
{
tomMatch1_112= true ;
tomMatch1_106=tomMatch1_102;
tomMatch1_101=tom_get_slot_le_children(tomMatch1_106);
}
} else {
if (tom_is_sort_Inst(tomMatch1_102)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_102))) {
{
tomMatch1_112= true ;
tomMatch1_107=tomMatch1_102;
tomMatch1_101=tom_get_slot_eq_children(tomMatch1_107);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_112) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_101))) {
int tomMatch1_109=0;
if (!(tomMatch1_109 >= tom_get_size_InstL_InstL(tomMatch1_101))) {
int tomMatch1_110=tomMatch1_109 + 1;
if (!(tomMatch1_110 >= tom_get_size_InstL_InstL(tomMatch1_101))) {
if (tomMatch1_110 + 1 >= tom_get_size_InstL_InstL(tomMatch1_101)) {
if (tomMatch1_96 + 1 >= tom_get_size_InstL_InstL(tomMatch1_91)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_91,tomMatch1_96),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_91,tomMatch1_95),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_114=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_114))) {
int tomMatch1_118=0;
if (!(tomMatch1_118 >= tom_get_size_InstL_InstL(tomMatch1_114))) {
Instruction tomMatch1_122=tom_get_element_InstL_InstL(tomMatch1_114,tomMatch1_118);
boolean tomMatch1_135= false ;
Instruction tomMatch1_125= null ;
Instruction tomMatch1_123= null ;
Instruction tomMatch1_126= null ;
Instruction tomMatch1_124= null ;
Instruction tomMatch1_127= null ;
 EList<Instruction>  tomMatch1_121= null ;
if (tom_is_sort_Inst(tomMatch1_122)) {
if (tom_is_fun_sym_gt(((Instruction)tomMatch1_122))) {
{
tomMatch1_135= true ;
tomMatch1_123=tomMatch1_122;
tomMatch1_121=tom_get_slot_gt_children(tomMatch1_123);
}
} else {
if (tom_is_sort_Inst(tomMatch1_122)) {
if (tom_is_fun_sym_lt(((Instruction)tomMatch1_122))) {
{
tomMatch1_135= true ;
tomMatch1_124=tomMatch1_122;
tomMatch1_121=tom_get_slot_lt_children(tomMatch1_124);
}
} else {
if (tom_is_sort_Inst(tomMatch1_122)) {
if (tom_is_fun_sym_ge(((Instruction)tomMatch1_122))) {
{
tomMatch1_135= true ;
tomMatch1_125=tomMatch1_122;
tomMatch1_121=tom_get_slot_ge_children(tomMatch1_125);
}
} else {
if (tom_is_sort_Inst(tomMatch1_122)) {
if (tom_is_fun_sym_le(((Instruction)tomMatch1_122))) {
{
tomMatch1_135= true ;
tomMatch1_126=tomMatch1_122;
tomMatch1_121=tom_get_slot_le_children(tomMatch1_126);
}
} else {
if (tom_is_sort_Inst(tomMatch1_122)) {
if (tom_is_fun_sym_eq(((Instruction)tomMatch1_122))) {
{
tomMatch1_135= true ;
tomMatch1_127=tomMatch1_122;
tomMatch1_121=tom_get_slot_eq_children(tomMatch1_127);
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_135) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_121))) {
int tomMatch1_132=0;
if (!(tomMatch1_132 >= tom_get_size_InstL_InstL(tomMatch1_121))) {
int tomMatch1_133=tomMatch1_132 + 1;
if (!(tomMatch1_133 >= tom_get_size_InstL_InstL(tomMatch1_121))) {
if (tomMatch1_133 + 1 >= tom_get_size_InstL_InstL(tomMatch1_121)) {
int tomMatch1_119=tomMatch1_118 + 1;
if (!(tomMatch1_119 >= tom_get_size_InstL_InstL(tomMatch1_114))) {
Instruction tomMatch1_129=tom_get_element_InstL_InstL(tomMatch1_114,tomMatch1_119);
if (tom_is_sort_Inst(tomMatch1_129)) {
if (tom_is_fun_sym_booldisjunction(((Instruction)tomMatch1_129))) {
if (tomMatch1_119 + 1 >= tom_get_size_InstL_InstL(tomMatch1_114)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_114,tomMatch1_119),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_114,tomMatch1_118),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_137=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_137))) {
int tomMatch1_141=0;
if (!(tomMatch1_141 >= tom_get_size_InstL_InstL(tomMatch1_137))) {
Instruction tomMatch1_145=tom_get_element_InstL_InstL(tomMatch1_137,tomMatch1_141);
if (tom_is_sort_Inst(tomMatch1_145)) {
if (tom_is_fun_sym_booldisjunction(((Instruction)tomMatch1_145))) {
int tomMatch1_142=tomMatch1_141 + 1;
if (!(tomMatch1_142 >= tom_get_size_InstL_InstL(tomMatch1_137))) {
Instruction tomMatch1_148=tom_get_element_InstL_InstL(tomMatch1_137,tomMatch1_142);
if (tom_is_sort_Inst(tomMatch1_148)) {
if (tom_is_fun_sym_booldisjunction(((Instruction)tomMatch1_148))) {
if (tomMatch1_142 + 1 >= tom_get_size_InstL_InstL(tomMatch1_137)) {
return 
tom_make_booldisjunction(tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_137,tomMatch1_142),tom_cons_array_InstL(tom_get_element_InstL_InstL(tomMatch1_137,tomMatch1_141),tom_empty_array_InstL(2))));
}
}
}
}
}
}
}
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_Disjunctions() { 
return new Disjunctions();
}

}
