/*******************************************************************************
* Copyright (c) 2012 Universite de Rennes 1 / Inria.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the FreeBSD License v1.0
* which accompanies this distribution, and is available at
* http://www.freebsd.org/copyright/freebsd-license.html
*
* Contributors:
*    DERRIEN Steven - initial API and implementation
*    MORVAN Antoine - initial API and implementation
*    NAULLET Maxime - initial API and implementation
*******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;
import org.polymodel.algebra.tom.ArithmeticOperations;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.BaseLinearizer;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class Linearizer extends BaseLinearizer {



private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_InnermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId())),tom_empty_list_Sequence()))))
;
}
private static boolean tom_equal_term_List(Object l1, Object l2) {
return  l1.equals(l2) ;
}
private static boolean tom_is_sort_List(Object t) {
return  t instanceof java.util.List ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_org_polymodel_algebra_FuzzyBoolean(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_FuzzyBoolean(Object t) {
return t instanceof org.polymodel.algebra.FuzzyBoolean;
}
private static boolean tom_equal_term_org_polymodel_algebra_OUTPUT_FORMAT(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_OUTPUT_FORMAT(Object t) {
return t instanceof org.polymodel.algebra.OUTPUT_FORMAT;
}
private static boolean tom_equal_term_org_polymodel_algebra_Value(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_Value(Object t) {
return true;
}
private static boolean tom_equal_term_org_polymodel_algebra_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_ComparisonOperator(Object t) {
return t instanceof org.polymodel.algebra.ComparisonOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_CompositeOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_CompositeOperator(Object t) {
return t instanceof org.polymodel.algebra.CompositeOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_reductions_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_reductions_ReductionOperator(Object t) {
return t instanceof org.polymodel.algebra.reductions.ReductionOperator;
}
private static boolean tom_equal_term_ICS(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICS(Object t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static boolean tom_equal_term_ICSL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_equal_term_E(Object l1, Object l2) {
return (l1!=null && l2 instanceof IntExpression && ((IntExpression)l1).isEquivalent((IntExpression)l2) == FuzzyBoolean.YES) || l1==l2;
}
private static boolean tom_is_sort_E(Object t) {
return t instanceof IntExpression;
}
private static boolean tom_equal_term_EL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_EL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntExpression>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntExpression>)t).size()>0 && ((EList<org.polymodel.algebra.IntExpression>)t).get(0) instanceof org.polymodel.algebra.IntExpression));
}
private static boolean tom_equal_term_V(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_V(Object t) {
return t instanceof org.polymodel.algebra.Variable;
}
private static boolean tom_equal_term_vars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_vars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.Variable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.Variable>)t).size()>0 && ((EList<org.polymodel.algebra.Variable>)t).get(0) instanceof org.polymodel.algebra.Variable));
}
private static boolean tom_equal_term_T(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_T(Object t) {
return t instanceof org.polymodel.algebra.IntTerm;
}
private static boolean tom_equal_term_terms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_terms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static boolean tom_is_fun_sym_terms( EList<org.polymodel.algebra.IntTerm>  t) {
return  t instanceof EList<?> &&
 		(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
 		|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_empty_array_terms(int n) { 
return  new BasicEList<org.polymodel.algebra.IntTerm>(n) ;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_cons_array_terms(org.polymodel.algebra.IntTerm e,  EList<org.polymodel.algebra.IntTerm>  l) { 
return  append(e,l) ;
}
private static org.polymodel.algebra.IntTerm tom_get_element_terms_terms( EList<org.polymodel.algebra.IntTerm>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_terms_terms( EList<org.polymodel.algebra.IntTerm>  l) {
return  l.size() ;
}

  private static   EList<org.polymodel.algebra.IntTerm>  tom_get_slice_terms( EList<org.polymodel.algebra.IntTerm>  subject, int begin, int end) {
     EList<org.polymodel.algebra.IntTerm>  result =  new BasicEList<org.polymodel.algebra.IntTerm>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<org.polymodel.algebra.IntTerm>  tom_append_array_terms( EList<org.polymodel.algebra.IntTerm>  l2,  EList<org.polymodel.algebra.IntTerm>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<org.polymodel.algebra.IntTerm>  result =  new BasicEList<org.polymodel.algebra.IntTerm>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_C(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_C(Object t) {
return t instanceof org.polymodel.algebra.IntConstraint;
}
private static boolean tom_equal_term_CL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_CL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static boolean tom_is_fun_sym_CL( EList<org.polymodel.algebra.IntConstraint>  t) {
return  t instanceof EList<?> &&
 		(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
 		|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static  EList<org.polymodel.algebra.IntConstraint>  tom_empty_array_CL(int n) { 
return  new BasicEList<org.polymodel.algebra.IntConstraint>(n) ;
}
private static  EList<org.polymodel.algebra.IntConstraint>  tom_cons_array_CL(org.polymodel.algebra.IntConstraint e,  EList<org.polymodel.algebra.IntConstraint>  l) { 
return  append(e,l) ;
}
private static org.polymodel.algebra.IntConstraint tom_get_element_CL_CL( EList<org.polymodel.algebra.IntConstraint>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_CL_CL( EList<org.polymodel.algebra.IntConstraint>  l) {
return  l.size() ;
}

  private static   EList<org.polymodel.algebra.IntConstraint>  tom_get_slice_CL( EList<org.polymodel.algebra.IntConstraint>  subject, int begin, int end) {
     EList<org.polymodel.algebra.IntConstraint>  result =  new BasicEList<org.polymodel.algebra.IntConstraint>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<org.polymodel.algebra.IntConstraint>  tom_append_array_CL( EList<org.polymodel.algebra.IntConstraint>  l2,  EList<org.polymodel.algebra.IntConstraint>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<org.polymodel.algebra.IntConstraint>  result =  new BasicEList<org.polymodel.algebra.IntConstraint>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_pterm(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterm(Object t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialTerm;
}
private static boolean tom_equal_term_pterms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialTerm));
}
private static boolean tom_equal_term_pvar(Object l1, Object l2) {
return (l1!=null && l2 instanceof PolynomialVariable && ((PolynomialVariable)l1).isEquivalent((PolynomialVariable)l2)) || l1==l2;
}
private static boolean tom_is_sort_pvar(Object t) {
return t instanceof PolynomialVariable;
}
private static boolean tom_equal_term_pvars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pvars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialVariable));
}
private static boolean tom_is_fun_sym_affine(IntExpression t) {
return t instanceof org.polymodel.algebra.affine.AffineExpression;
}
private static IntExpression tom_make_affine( EList<org.polymodel.algebra.IntTerm>  _terms) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createAffine(_terms);
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_affine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.affine.AffineExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_term(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.affine.AffineTerm;
}
private static org.polymodel.algebra.IntTerm tom_make_term( long  _coef, org.polymodel.algebra.Variable _variable) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createTerm(_coef, _variable);
}
private static  long  tom_get_slot_term_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getCoef();
}
private static org.polymodel.algebra.Variable tom_get_slot_term_variable(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getVariable();
}
private static boolean tom_is_fun_sym_constant(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.affine.AffineTerm && ((org.polymodel.algebra.affine.AffineTerm)t).getVariable() == null;
}
private static org.polymodel.algebra.IntTerm tom_make_constant( long  _coef) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createConstant(_coef);
}
private static  long  tom_get_slot_constant_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getCoef();
}
private static IntExpression tom_make_qaffine( EList<org.polymodel.algebra.IntTerm>  _terms) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createQaffine(_terms);
}
private static org.polymodel.algebra.IntTerm tom_make_mod(IntExpression _expression,  long  _coef) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createMod(_expression, _coef);
}
private static org.polymodel.algebra.IntTerm tom_make_floor(IntExpression _expression,  long  _coef) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createFloor(_expression, _coef);
}
private static org.polymodel.algebra.IntTerm tom_make_div(IntExpression _expression,  long  _coef) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createDiv(_expression, _coef);
}
private static org.polymodel.algebra.IntConstraint tom_make_eq(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createEq(_lhs, _rhs);
}
private static org.polymodel.algebra.IntConstraint tom_make_ne(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createNe(_lhs, _rhs);
}
private static org.polymodel.algebra.IntConstraint tom_make_ge(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createGe(_lhs, _rhs);
}
private static org.polymodel.algebra.IntConstraint tom_make_le(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createLe(_lhs, _rhs);
}
private static org.polymodel.algebra.IntConstraint tom_make_gt(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createGt(_lhs, _rhs);
}
private static org.polymodel.algebra.IntConstraint tom_make_lt(IntExpression _lhs, IntExpression _rhs) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createLt(_lhs, _rhs);
}
private static boolean tom_is_fun_sym_system(org.polymodel.algebra.IntConstraintSystem t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static org.polymodel.algebra.IntConstraintSystem tom_make_system( EList<org.polymodel.algebra.IntConstraint>  _constraints) { 
return org.polymodel.algebra.internal.AlgebraTomFactory.createSystem(_constraints);
}
private static  EList<org.polymodel.algebra.IntConstraint>  tom_get_slot_system_constraints(org.polymodel.algebra.IntConstraintSystem t) {
return enforce(((org.polymodel.algebra.IntConstraintSystem)t).getConstraints());
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_equal_term_nodes(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_nodes(Object t) {
return  t instanceof EList<?> &&
    	(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
    	|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static boolean tom_equal_term_node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_node(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopNode;
}
private static boolean tom_equal_term_SymList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<gecos.core.Symbol>)t).size() == 0 
    	|| (((EList<gecos.core.Symbol>)t).size()>0 && ((EList<gecos.core.Symbol>)t).get(0) instanceof gecos.core.Symbol));
}
private static boolean tom_equal_term_ICSList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_is_fun_sym_sexpr(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
}
private static Instruction tom_make_sexpr(IntExpression _expr) { 
return fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory.createSexpr(_expr);
}
private static IntExpression tom_get_slot_sexpr_expr(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopIntExpression)t).getExpr();
}
private static boolean tom_is_fun_sym_sconstraintSys(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
}
private static Instruction tom_make_sconstraintSys(org.polymodel.algebra.IntConstraintSystem _system) { 
return fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory.createSconstraintSys(_system);
}
private static org.polymodel.algebra.IntConstraintSystem tom_get_slot_sconstraintSys_system(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem)t).getSystem();
}
private static boolean tom_is_fun_sym_symref(Instruction t) {
return t instanceof SymbolInstruction;
}
private static Symbol tom_get_slot_symref_symbol(Instruction t) {
return ((SymbolInstruction)t).getSymbol();
}
private static boolean tom_is_fun_sym_ival(Instruction t) {
return t instanceof IntInstruction;
}
private static  long  tom_get_slot_ival_value(Instruction t) {
return ((IntInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static Instruction tom_make_generic( String  _name,  EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric(_name, _children);
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_convert(Instruction t) {
return t instanceof ConvertInstruction;
}
private static Instruction tom_get_slot_convert_expr(Instruction t) {
return ((ConvertInstruction)t).getExpr();
}



static final boolean TOM_VERBOSE = true	; // &&  ScopExtractorPass.VERBOSE;

private static void traceTom(String mess) {
if(TOM_VERBOSE) {
System.out.println("[Linearizer/Tom]\t"+mess);
}
}


protected Instruction tom_rewrite(Instruction instruction) throws SCoPException {
try {
debug("Linearizing "+instruction.getClass().getSimpleName()+":"+instruction+" using "+iterators+" and "+parameters);
Instruction res = 
tom_make_InnermostId(tom_make_BuildLinearExpression(context)).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
debug("-> "+res.getClass().getSimpleName()+":"+res);
checkNoNull(res);
return res;
} catch (Exception e) {  
e.printStackTrace();
throw new SCoPException(instruction.getBasicBlock(), instruction + " is not Scopable due to runtime exception "+e.getMessage());
}
}


private static boolean tom_equal_term_Context(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Context(Object t) {
return t instanceof VariableContextManager;
}
public static class BuildLinearExpression extends tom.library.sl.AbstractStrategyBasic {
private VariableContextManager c;
public BuildLinearExpression(VariableContextManager c) {
super(tom_make_Identity());
this.c=c;
}
public VariableContextManager getc() {
return c;
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_1=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_2=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_1)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_2))) {
int tomMatch1_8=0;
if (!(tomMatch1_8 >= tom_get_size_InstL_InstL(tomMatch1_2))) {
Instruction tomMatch1_12=tom_get_element_InstL_InstL(tomMatch1_2,tomMatch1_8);
if (tom_is_sort_Inst(tomMatch1_12)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_12))) {
int tomMatch1_9=tomMatch1_8 + 1;
if (!(tomMatch1_9 >= tom_get_size_InstL_InstL(tomMatch1_2))) {
Instruction tomMatch1_15=tom_get_element_InstL_InstL(tomMatch1_2,tomMatch1_9);
if (tom_is_sort_Inst(tomMatch1_15)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_15))) {
if (tomMatch1_9 + 1 >= tom_get_size_InstL_InstL(tomMatch1_2)) {
Instruction tom_s=((Instruction)tom__arg);

touched=true;
EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 1 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
// s@imul(InstL(symref(a),ival(b)))
ScopDimension v = 
c.lookUp(
tom_get_slot_symref_symbol(tomMatch1_12));
if(v!=null) {
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(tom_get_slot_ival_value(tomMatch1_15),v),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 1 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
traceTom("Rule 1 "+(
tom_s)+" => "+(
tom_s));
return 
tom_s;  
} 

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_18=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_19=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_18)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_19))) {
int tomMatch1_25=0;
if (!(tomMatch1_25 >= tom_get_size_InstL_InstL(tomMatch1_19))) {
Instruction tomMatch1_29=tom_get_element_InstL_InstL(tomMatch1_19,tomMatch1_25);
if (tom_is_sort_Inst(tomMatch1_29)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_29))) {
int tomMatch1_26=tomMatch1_25 + 1;
if (!(tomMatch1_26 >= tom_get_size_InstL_InstL(tomMatch1_19))) {
Instruction tomMatch1_32=tom_get_element_InstL_InstL(tomMatch1_19,tomMatch1_26);
if (tom_is_sort_Inst(tomMatch1_32)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_32))) {
if (tomMatch1_26 + 1 >= tom_get_size_InstL_InstL(tomMatch1_19)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 2 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
// s@imul(InstL(symref(a),ival(b)))
ScopDimension v = 
c.lookUp(
tom_get_slot_symref_symbol(tomMatch1_32));
if(v!=null) {
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(tom_get_slot_ival_value(tomMatch1_29),v),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 2 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
return 
tom_s;  
}  

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_35=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_36=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("shr", tomMatch1_35)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_36))) {
int tomMatch1_42=0;
if (!(tomMatch1_42 >= tom_get_size_InstL_InstL(tomMatch1_36))) {
Instruction tomMatch1_46=tom_get_element_InstL_InstL(tomMatch1_36,tomMatch1_42);
if (tom_is_sort_Inst(tomMatch1_46)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_46))) {
int tomMatch1_43=tomMatch1_42 + 1;
if (!(tomMatch1_43 >= tom_get_size_InstL_InstL(tomMatch1_36))) {
Instruction tomMatch1_49=tom_get_element_InstL_InstL(tomMatch1_36,tomMatch1_43);
if (tom_is_sort_Inst(tomMatch1_49)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_49))) {
if (tomMatch1_43 + 1 >= tom_get_size_InstL_InstL(tomMatch1_36)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 3 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
// s@shr(InstL(symref(a),ival(b))) 
ScopDimension v = 
c.lookUp(
tom_get_slot_symref_symbol(tomMatch1_46));
if(v!=null) {
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(1<<tom_get_slot_ival_value(tomMatch1_49),v),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 3 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
return 
tom_s;
} 

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_52=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_53=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("shl", tomMatch1_52)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_53))) {
int tomMatch1_59=0;
if (!(tomMatch1_59 >= tom_get_size_InstL_InstL(tomMatch1_53))) {
Instruction tomMatch1_63=tom_get_element_InstL_InstL(tomMatch1_53,tomMatch1_59);
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_63))) {
int tomMatch1_60=tomMatch1_59 + 1;
if (!(tomMatch1_60 >= tom_get_size_InstL_InstL(tomMatch1_53))) {
Instruction tomMatch1_66=tom_get_element_InstL_InstL(tomMatch1_53,tomMatch1_60);
if (tom_is_sort_Inst(tomMatch1_66)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_66))) {
if (tomMatch1_60 + 1 >= tom_get_size_InstL_InstL(tomMatch1_53)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 4 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
// s@shl(InstL(symref(a),ival(b)))
ScopDimension v = 
c.lookUp(
tom_get_slot_symref_symbol(tomMatch1_63));
if(v!=null) {
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_qaffine(tom_cons_array_terms(tom_make_floor(tom_make_affine(tom_cons_array_terms(tom_make_term(1L,v),tom_empty_array_terms(1))),((long)1<<tom_get_slot_ival_value(tomMatch1_66))),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 4 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
return 
tom_s;
}

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)tom__arg)))) {
Symbol tom_a=tom_get_slot_symref_symbol(((Instruction)tom__arg));
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 5 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
ScopDimension v = 
c.lookUp(
tom_a);
/*				
if((`v).getSymbol().eContainer()==null) {
///debug("Houston, we have a problem with "+(`v)+ " symbol-> "+ (`v).getSymbol());
}
*/				
if(v!=null) {
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(1L,v),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 5 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
traceTom("Rule 5 (ignored since "+(
tom_a)+" is neither a parameter or an iterator) ");
return 
tom_s;
} 

}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_73=tom_get_slot_convert_expr(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_73)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_73))) {
Symbol tom_a=tom_get_slot_symref_symbol(tomMatch1_73);
Instruction tom_s=tomMatch1_73;
Instruction tom_conv=((Instruction)tom__arg);

EObject container = 
tom_conv.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 6 (ignored since instruction is used to define a type) ");
return 
tom_conv;
}
ScopDimension v = 
c.lookUp(
tom_a);
if(v!=null) {
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(1L,v),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 6 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;
} else {
traceTom("Rule 6 (ignored since "+(
tom_a)+" is neither a parameter or an iterator) ");
return 
tom_s;
} 

}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_ival(((Instruction)((Instruction)tom__arg)))) {
Instruction tom_valueInstr=((Instruction)tom__arg);

EObject container = 
tom_valueInstr.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 6b (ignored since instruction "+(
tom_valueInstr)+" is used within type/symbol definition "+container+") ");
return 
tom_valueInstr;
}
touched=true;
Instruction instr = 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_constant(tom_get_slot_ival_value(((Instruction)tom__arg))),tom_empty_array_terms(1))));
instr.setType((
tom_valueInstr).getType());
instr.getAnnotations().putAll(
tom_valueInstr.getAnnotations());
return instr;

}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_84=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_85=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("add", tomMatch1_84)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_85))) {
int tomMatch1_91=0;
if (!(tomMatch1_91 >= tom_get_size_InstL_InstL(tomMatch1_85))) {
Instruction tomMatch1_95=tom_get_element_InstL_InstL(tomMatch1_85,tomMatch1_91);
if (tom_is_sort_Inst(tomMatch1_95)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_95))) {
int tomMatch1_92=tomMatch1_91 + 1;
if (!(tomMatch1_92 >= tom_get_size_InstL_InstL(tomMatch1_85))) {
Instruction tomMatch1_98=tom_get_element_InstL_InstL(tomMatch1_85,tomMatch1_92);
if (tom_is_sort_Inst(tomMatch1_98)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_98))) {
if (tomMatch1_92 + 1 >= tom_get_size_InstL_InstL(tomMatch1_85)) {
Instruction tom_in=((Instruction)tom__arg);

EObject container = 
tom_in.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 7 (ignored since instruction is used to define a type/symbol) ");
return 
tom_in;
}
IntExpression expr = ArithmeticOperations.add(
tom_get_slot_sexpr_expr(tomMatch1_95),
tom_get_slot_sexpr_expr(tomMatch1_98)).simplify();
Instruction res = 
tom_make_sexpr(expr);
res.setType((
tom_in).getType());
checkNoNull(res);  
traceTom("Rule 7 : "+(
tom_in)+"=>"+res);
res.getAnnotations().putAll(
tom_in.getAnnotations());
touched=true;
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_101=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_102=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("sub", tomMatch1_101)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_102))) {
int tomMatch1_108=0;
if (!(tomMatch1_108 >= tom_get_size_InstL_InstL(tomMatch1_102))) {
Instruction tomMatch1_112=tom_get_element_InstL_InstL(tomMatch1_102,tomMatch1_108);
if (tom_is_sort_Inst(tomMatch1_112)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_112))) {
int tomMatch1_109=tomMatch1_108 + 1;
if (!(tomMatch1_109 >= tom_get_size_InstL_InstL(tomMatch1_102))) {
Instruction tomMatch1_115=tom_get_element_InstL_InstL(tomMatch1_102,tomMatch1_109);
if (tom_is_sort_Inst(tomMatch1_115)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_115))) {
if (tomMatch1_109 + 1 >= tom_get_size_InstL_InstL(tomMatch1_102)) {
Instruction tom_in=((Instruction)tom__arg);

EObject container = 
tom_in.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 8 (ignored since instruction is used to define a type/symbol) ");
return 
tom_in;
}
touched=true;
IntExpression expr = ArithmeticOperations.sub((
tom_get_slot_sexpr_expr(tomMatch1_112)),(
tom_get_slot_sexpr_expr(tomMatch1_115))).simplify();
checkNoNull(expr);
Instruction res = 
tom_make_sexpr(expr); 
res.setType((
tom_in).getType());
traceTom("Rule 8 : "+(
tom_in)+"=>"+res);
res.getAnnotations().putAll(
tom_in.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_118=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_119=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_118)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_119))) {
int tomMatch1_125=0;
if (!(tomMatch1_125 >= tom_get_size_InstL_InstL(tomMatch1_119))) {
Instruction tomMatch1_129=tom_get_element_InstL_InstL(tomMatch1_119,tomMatch1_125);
if (tom_is_sort_Inst(tomMatch1_129)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_129))) {
IntExpression tomMatch1_128=tom_get_slot_sexpr_expr(tomMatch1_129);
if (tom_is_sort_E(tomMatch1_128)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_128))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_131=tom_get_slot_affine_terms(tomMatch1_128);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_131))) {
int tomMatch1_138=0;
if (!(tomMatch1_138 >= tom_get_size_terms_terms(tomMatch1_131))) {
org.polymodel.algebra.IntTerm tomMatch1_141=tom_get_element_terms_terms(tomMatch1_131,tomMatch1_138);
if (tom_is_sort_T(tomMatch1_141)) {
if (tom_is_fun_sym_constant(((org.polymodel.algebra.IntTerm)tomMatch1_141))) {
if (tomMatch1_138 + 1 >= tom_get_size_terms_terms(tomMatch1_131)) {
int tomMatch1_126=tomMatch1_125 + 1;
if (!(tomMatch1_126 >= tom_get_size_InstL_InstL(tomMatch1_119))) {
Instruction tomMatch1_135=tom_get_element_InstL_InstL(tomMatch1_119,tomMatch1_126);
if (tom_is_sort_Inst(tomMatch1_135)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_135))) {
if (tomMatch1_126 + 1 >= tom_get_size_InstL_InstL(tomMatch1_119)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 9 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(ArithmeticOperations.scale(tom_get_slot_sexpr_expr(tomMatch1_135),tom_get_slot_constant_coef(tomMatch1_141)));
res.setType((
tom_s).getType());
traceTom("Rule 9 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_144=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_145=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_144)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_145))) {
int tomMatch1_151=0;
if (!(tomMatch1_151 >= tom_get_size_InstL_InstL(tomMatch1_145))) {
Instruction tomMatch1_155=tom_get_element_InstL_InstL(tomMatch1_145,tomMatch1_151);
if (tom_is_sort_Inst(tomMatch1_155)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_155))) {
int tomMatch1_152=tomMatch1_151 + 1;
if (!(tomMatch1_152 >= tom_get_size_InstL_InstL(tomMatch1_145))) {
Instruction tomMatch1_158=tom_get_element_InstL_InstL(tomMatch1_145,tomMatch1_152);
if (tom_is_sort_Inst(tomMatch1_158)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_158))) {
IntExpression tomMatch1_157=tom_get_slot_sexpr_expr(tomMatch1_158);
if (tom_is_sort_E(tomMatch1_157)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_157))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_160=tom_get_slot_affine_terms(tomMatch1_157);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_160))) {
int tomMatch1_164=0;
if (!(tomMatch1_164 >= tom_get_size_terms_terms(tomMatch1_160))) {
org.polymodel.algebra.IntTerm tomMatch1_167=tom_get_element_terms_terms(tomMatch1_160,tomMatch1_164);
if (tom_is_sort_T(tomMatch1_167)) {
if (tom_is_fun_sym_constant(((org.polymodel.algebra.IntTerm)tomMatch1_167))) {
if (tomMatch1_164 + 1 >= tom_get_size_terms_terms(tomMatch1_160)) {
if (tomMatch1_152 + 1 >= tom_get_size_InstL_InstL(tomMatch1_145)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 10 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(ArithmeticOperations.scale(tom_get_slot_sexpr_expr(tomMatch1_155),tom_get_slot_constant_coef(tomMatch1_167)));
res.setType((
tom_s).getType());
traceTom("Rule 10 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_170=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_171=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_170)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_171))) {
int tomMatch1_177=0;
if (!(tomMatch1_177 >= tom_get_size_InstL_InstL(tomMatch1_171))) {
Instruction tomMatch1_181=tom_get_element_InstL_InstL(tomMatch1_171,tomMatch1_177);
if (tom_is_sort_Inst(tomMatch1_181)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_181))) {
int tomMatch1_178=tomMatch1_177 + 1;
if (!(tomMatch1_178 >= tom_get_size_InstL_InstL(tomMatch1_171))) {
Instruction tomMatch1_184=tom_get_element_InstL_InstL(tomMatch1_171,tomMatch1_178);
if (tom_is_sort_Inst(tomMatch1_184)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_184))) {
if (tomMatch1_178 + 1 >= tom_get_size_InstL_InstL(tomMatch1_171)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 11 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(ArithmeticOperations.scale(tom_get_slot_sexpr_expr(tomMatch1_181),tom_get_slot_ival_value(tomMatch1_184)));
res.setType((
tom_s).getType());
traceTom("Rule 11 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_187=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_188=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch1_187)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_188))) {
int tomMatch1_194=0;
if (!(tomMatch1_194 >= tom_get_size_InstL_InstL(tomMatch1_188))) {
Instruction tomMatch1_198=tom_get_element_InstL_InstL(tomMatch1_188,tomMatch1_194);
if (tom_is_sort_Inst(tomMatch1_198)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_198))) {
int tomMatch1_195=tomMatch1_194 + 1;
if (!(tomMatch1_195 >= tom_get_size_InstL_InstL(tomMatch1_188))) {
Instruction tomMatch1_201=tom_get_element_InstL_InstL(tomMatch1_188,tomMatch1_195);
if (tom_is_sort_Inst(tomMatch1_201)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_201))) {
if (tomMatch1_195 + 1 >= tom_get_size_InstL_InstL(tomMatch1_188)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 12 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(ArithmeticOperations.scale(tom_get_slot_sexpr_expr(tomMatch1_201),tom_get_slot_ival_value(tomMatch1_198)));
res.setType((
tom_s).getType());
traceTom("Rule 12 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_204=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_205=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("div", tomMatch1_204)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_205))) {
int tomMatch1_211=0;
if (!(tomMatch1_211 >= tom_get_size_InstL_InstL(tomMatch1_205))) {
Instruction tomMatch1_215=tom_get_element_InstL_InstL(tomMatch1_205,tomMatch1_211);
if (tom_is_sort_Inst(tomMatch1_215)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_215))) {
int tomMatch1_212=tomMatch1_211 + 1;
if (!(tomMatch1_212 >= tom_get_size_InstL_InstL(tomMatch1_205))) {
Instruction tomMatch1_218=tom_get_element_InstL_InstL(tomMatch1_205,tomMatch1_212);
if (tom_is_sort_Inst(tomMatch1_218)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_218))) {
IntExpression tomMatch1_217=tom_get_slot_sexpr_expr(tomMatch1_218);
if (tom_is_sort_E(tomMatch1_217)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_217))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_220=tom_get_slot_affine_terms(tomMatch1_217);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_220))) {
int tomMatch1_224=0;
if (!(tomMatch1_224 >= tom_get_size_terms_terms(tomMatch1_220))) {
org.polymodel.algebra.IntTerm tomMatch1_227=tom_get_element_terms_terms(tomMatch1_220,tomMatch1_224);
if (tom_is_sort_T(tomMatch1_227)) {
if (tom_is_fun_sym_constant(((org.polymodel.algebra.IntTerm)tomMatch1_227))) {
if (tomMatch1_224 + 1 >= tom_get_size_terms_terms(tomMatch1_220)) {
if (tomMatch1_212 + 1 >= tom_get_size_InstL_InstL(tomMatch1_205)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 13 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_qaffine(tom_cons_array_terms(tom_make_div(tom_get_slot_sexpr_expr(tomMatch1_215),tom_get_slot_constant_coef(tomMatch1_227)),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 13 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_230=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_231=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("mod", tomMatch1_230)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_231))) {
int tomMatch1_237=0;
if (!(tomMatch1_237 >= tom_get_size_InstL_InstL(tomMatch1_231))) {
Instruction tomMatch1_241=tom_get_element_InstL_InstL(tomMatch1_231,tomMatch1_237);
if (tom_is_sort_Inst(tomMatch1_241)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_241))) {
int tomMatch1_238=tomMatch1_237 + 1;
if (!(tomMatch1_238 >= tom_get_size_InstL_InstL(tomMatch1_231))) {
Instruction tomMatch1_244=tom_get_element_InstL_InstL(tomMatch1_231,tomMatch1_238);
if (tom_is_sort_Inst(tomMatch1_244)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_244))) {
IntExpression tomMatch1_243=tom_get_slot_sexpr_expr(tomMatch1_244);
if (tom_is_sort_E(tomMatch1_243)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_243))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_246=tom_get_slot_affine_terms(tomMatch1_243);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_246))) {
int tomMatch1_250=0;
if (!(tomMatch1_250 >= tom_get_size_terms_terms(tomMatch1_246))) {
org.polymodel.algebra.IntTerm tomMatch1_253=tom_get_element_terms_terms(tomMatch1_246,tomMatch1_250);
if (tom_is_sort_T(tomMatch1_253)) {
if (tom_is_fun_sym_constant(((org.polymodel.algebra.IntTerm)tomMatch1_253))) {
if (tomMatch1_250 + 1 >= tom_get_size_terms_terms(tomMatch1_246)) {
if (tomMatch1_238 + 1 >= tom_get_size_InstL_InstL(tomMatch1_231)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 13.1 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_qaffine(tom_cons_array_terms(tom_make_mod(tom_get_slot_sexpr_expr(tomMatch1_241),tom_get_slot_constant_coef(tomMatch1_253)),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 13.1 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_256=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_257=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("neg", tomMatch1_256)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_257))) {
int tomMatch1_263=0;
if (!(tomMatch1_263 >= tom_get_size_InstL_InstL(tomMatch1_257))) {
Instruction tomMatch1_266=tom_get_element_InstL_InstL(tomMatch1_257,tomMatch1_263);
if (tom_is_sort_Inst(tomMatch1_266)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_266))) {
IntExpression tomMatch1_265=tom_get_slot_sexpr_expr(tomMatch1_266);
if (tom_is_sort_E(tomMatch1_265)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_265))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_268=tom_get_slot_affine_terms(tomMatch1_265);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_268))) {
int tomMatch1_272=0;
if (!(tomMatch1_272 >= tom_get_size_terms_terms(tomMatch1_268))) {
org.polymodel.algebra.IntTerm tomMatch1_276=tom_get_element_terms_terms(tomMatch1_268,tomMatch1_272);
if (tom_is_sort_T(tomMatch1_276)) {
if (tom_is_fun_sym_term(((org.polymodel.algebra.IntTerm)tomMatch1_276))) {
if (tomMatch1_272 + 1 >= tom_get_size_terms_terms(tomMatch1_268)) {
if (tomMatch1_263 + 1 >= tom_get_size_InstL_InstL(tomMatch1_257)) {
Instruction tom_s=((Instruction)tom__arg);

EObject container = 
tom_s.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 14 (ignored since instruction is used to define a type) ");
return 
tom_s;
}
touched=true;
Instruction res= 
tom_make_sexpr(tom_make_affine(tom_cons_array_terms(tom_make_term(tom_get_slot_term_coef(tomMatch1_276)* (-1),tom_get_slot_term_variable(tomMatch1_276)),tom_empty_array_terms(1))));
res.setType((
tom_s).getType());
traceTom("Rule 14 "+(
tom_s)+" => "+res);
res.getAnnotations().putAll(
tom_s.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_279=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_280=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("lt", tomMatch1_279)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_280))) {
int tomMatch1_286=0;
if (!(tomMatch1_286 >= tom_get_size_InstL_InstL(tomMatch1_280))) {
Instruction tomMatch1_290=tom_get_element_InstL_InstL(tomMatch1_280,tomMatch1_286);
if (tom_is_sort_Inst(tomMatch1_290)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_290))) {
int tomMatch1_287=tomMatch1_286 + 1;
if (!(tomMatch1_287 >= tom_get_size_InstL_InstL(tomMatch1_280))) {
Instruction tomMatch1_293=tom_get_element_InstL_InstL(tomMatch1_280,tomMatch1_287);
if (tom_is_sort_Inst(tomMatch1_293)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_293))) {
if (tomMatch1_287 + 1 >= tom_get_size_InstL_InstL(tomMatch1_280)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 15 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_lt(tom_get_slot_sexpr_expr(tomMatch1_290),tom_get_slot_sexpr_expr(tomMatch1_293)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_296=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_297=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("gt", tomMatch1_296)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_297))) {
int tomMatch1_303=0;
if (!(tomMatch1_303 >= tom_get_size_InstL_InstL(tomMatch1_297))) {
Instruction tomMatch1_307=tom_get_element_InstL_InstL(tomMatch1_297,tomMatch1_303);
if (tom_is_sort_Inst(tomMatch1_307)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_307))) {
int tomMatch1_304=tomMatch1_303 + 1;
if (!(tomMatch1_304 >= tom_get_size_InstL_InstL(tomMatch1_297))) {
Instruction tomMatch1_310=tom_get_element_InstL_InstL(tomMatch1_297,tomMatch1_304);
if (tom_is_sort_Inst(tomMatch1_310)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_310))) {
if (tomMatch1_304 + 1 >= tom_get_size_InstL_InstL(tomMatch1_297)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 16 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_gt(tom_get_slot_sexpr_expr(tomMatch1_307),tom_get_slot_sexpr_expr(tomMatch1_310)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_313=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_314=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("le", tomMatch1_313)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_314))) {
int tomMatch1_320=0;
if (!(tomMatch1_320 >= tom_get_size_InstL_InstL(tomMatch1_314))) {
Instruction tomMatch1_324=tom_get_element_InstL_InstL(tomMatch1_314,tomMatch1_320);
if (tom_is_sort_Inst(tomMatch1_324)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_324))) {
int tomMatch1_321=tomMatch1_320 + 1;
if (!(tomMatch1_321 >= tom_get_size_InstL_InstL(tomMatch1_314))) {
Instruction tomMatch1_327=tom_get_element_InstL_InstL(tomMatch1_314,tomMatch1_321);
if (tom_is_sort_Inst(tomMatch1_327)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_327))) {
if (tomMatch1_321 + 1 >= tom_get_size_InstL_InstL(tomMatch1_314)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 17 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_le(tom_get_slot_sexpr_expr(tomMatch1_324),tom_get_slot_sexpr_expr(tomMatch1_327)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_330=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_331=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("ge", tomMatch1_330)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_331))) {
int tomMatch1_337=0;
if (!(tomMatch1_337 >= tom_get_size_InstL_InstL(tomMatch1_331))) {
Instruction tomMatch1_341=tom_get_element_InstL_InstL(tomMatch1_331,tomMatch1_337);
if (tom_is_sort_Inst(tomMatch1_341)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_341))) {
int tomMatch1_338=tomMatch1_337 + 1;
if (!(tomMatch1_338 >= tom_get_size_InstL_InstL(tomMatch1_331))) {
Instruction tomMatch1_344=tom_get_element_InstL_InstL(tomMatch1_331,tomMatch1_338);
if (tom_is_sort_Inst(tomMatch1_344)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_344))) {
if (tomMatch1_338 + 1 >= tom_get_size_InstL_InstL(tomMatch1_331)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 18 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_ge(tom_get_slot_sexpr_expr(tomMatch1_341),tom_get_slot_sexpr_expr(tomMatch1_344)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_347=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_348=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("eq", tomMatch1_347)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_348))) {
int tomMatch1_354=0;
if (!(tomMatch1_354 >= tom_get_size_InstL_InstL(tomMatch1_348))) {
Instruction tomMatch1_358=tom_get_element_InstL_InstL(tomMatch1_348,tomMatch1_354);
if (tom_is_sort_Inst(tomMatch1_358)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_358))) {
int tomMatch1_355=tomMatch1_354 + 1;
if (!(tomMatch1_355 >= tom_get_size_InstL_InstL(tomMatch1_348))) {
Instruction tomMatch1_361=tom_get_element_InstL_InstL(tomMatch1_348,tomMatch1_355);
if (tom_is_sort_Inst(tomMatch1_361)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_361))) {
if (tomMatch1_355 + 1 >= tom_get_size_InstL_InstL(tomMatch1_348)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 19 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_eq(tom_get_slot_sexpr_expr(tomMatch1_358),tom_get_slot_sexpr_expr(tomMatch1_361)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_364=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_365=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("ne", tomMatch1_364)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_365))) {
int tomMatch1_371=0;
if (!(tomMatch1_371 >= tom_get_size_InstL_InstL(tomMatch1_365))) {
Instruction tomMatch1_375=tom_get_element_InstL_InstL(tomMatch1_365,tomMatch1_371);
if (tom_is_sort_Inst(tomMatch1_375)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_375))) {
int tomMatch1_372=tomMatch1_371 + 1;
if (!(tomMatch1_372 >= tom_get_size_InstL_InstL(tomMatch1_365))) {
Instruction tomMatch1_378=tom_get_element_InstL_InstL(tomMatch1_365,tomMatch1_372);
if (tom_is_sort_Inst(tomMatch1_378)) {
if (tom_is_fun_sym_sexpr(((Instruction)tomMatch1_378))) {
if (tomMatch1_372 + 1 >= tom_get_size_InstL_InstL(tomMatch1_365)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 20 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res = 
tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_make_ne(tom_get_slot_sexpr_expr(tomMatch1_375),tom_get_slot_sexpr_expr(tomMatch1_378)),tom_empty_array_CL(1))));
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_381=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_382=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("boolconjunction", tomMatch1_381)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_382))) {
int tomMatch1_end_391=0;
do {
{
if (!(tomMatch1_end_391 >= tom_get_size_InstL_InstL(tomMatch1_382))) {
Instruction tomMatch1_399=tom_get_element_InstL_InstL(tomMatch1_382,tomMatch1_end_391);
if (tom_is_sort_Inst(tomMatch1_399)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch1_399))) {
org.polymodel.algebra.IntConstraintSystem tomMatch1_398=tom_get_slot_sconstraintSys_system(tomMatch1_399);
if (tom_is_sort_ICS(tomMatch1_398)) {
if (tom_is_fun_sym_system(((org.polymodel.algebra.IntConstraintSystem)tomMatch1_398))) {
 EList<org.polymodel.algebra.IntConstraint>  tomMatch1_401=tom_get_slot_system_constraints(tomMatch1_398);
if (tom_is_fun_sym_CL((( EList<org.polymodel.algebra.IntConstraint> )tomMatch1_401))) {
int tomMatch1_411=0;
if (!(tomMatch1_411 >= tom_get_size_CL_CL(tomMatch1_401))) {
if (tomMatch1_411 + 1 >= tom_get_size_CL_CL(tomMatch1_401)) {
int tomMatch1_end_395=tomMatch1_end_391 + 1;
do {
{
if (!(tomMatch1_end_395 >= tom_get_size_InstL_InstL(tomMatch1_382))) {
Instruction tomMatch1_405=tom_get_element_InstL_InstL(tomMatch1_382,tomMatch1_end_395);
if (tom_is_sort_Inst(tomMatch1_405)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch1_405))) {
org.polymodel.algebra.IntConstraintSystem tomMatch1_404=tom_get_slot_sconstraintSys_system(tomMatch1_405);
if (tom_is_sort_ICS(tomMatch1_404)) {
if (tom_is_fun_sym_system(((org.polymodel.algebra.IntConstraintSystem)tomMatch1_404))) {
 EList<org.polymodel.algebra.IntConstraint>  tomMatch1_407=tom_get_slot_system_constraints(tomMatch1_404);
if (tom_is_fun_sym_CL((( EList<org.polymodel.algebra.IntConstraint> )tomMatch1_407))) {
int tomMatch1_414=0;
if (!(tomMatch1_414 >= tom_get_size_CL_CL(tomMatch1_407))) {
if (tomMatch1_414 + 1 >= tom_get_size_CL_CL(tomMatch1_407)) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 21 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res= 
tom_make_generic("boolconjunction",tom_cons_array_InstL(tom_make_sconstraintSys(tom_make_system(tom_cons_array_CL(tom_get_element_CL_CL(tomMatch1_407,tomMatch1_414),tom_cons_array_CL(tom_get_element_CL_CL(tomMatch1_401,tomMatch1_411),tom_empty_array_CL(2))))),tom_empty_array_InstL(1)));
res.setType((
tom_op).getType());
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
}
}
tomMatch1_end_395=tomMatch1_end_395 + 1;
}
} while(!(tomMatch1_end_395 > tom_get_size_InstL_InstL(tomMatch1_382)));
}
}
}
}
}
}
}
}
tomMatch1_end_391=tomMatch1_end_391 + 1;
}
} while(!(tomMatch1_end_391 > tom_get_size_InstL_InstL(tomMatch1_382)));
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 String  tomMatch1_417=tom_get_slot_generic_name(((Instruction)tom__arg));
 EList<Instruction>  tomMatch1_418=tom_get_slot_generic_children(((Instruction)tom__arg));
if ( true ) {
if (tom_equal_term_String("land", tomMatch1_417)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_418))) {
int tomMatch1_end_427=0;
do {
{
if (!(tomMatch1_end_427 >= tom_get_size_InstL_InstL(tomMatch1_418))) {
Instruction tomMatch1_435=tom_get_element_InstL_InstL(tomMatch1_418,tomMatch1_end_427);
if (tom_is_sort_Inst(tomMatch1_435)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch1_435))) {
org.polymodel.algebra.IntConstraintSystem tomMatch1_434=tom_get_slot_sconstraintSys_system(tomMatch1_435);
if (tom_is_sort_ICS(tomMatch1_434)) {
if (tom_is_fun_sym_system(((org.polymodel.algebra.IntConstraintSystem)tomMatch1_434))) {
 EList<org.polymodel.algebra.IntConstraint>  tomMatch1_437=tom_get_slot_system_constraints(tomMatch1_434);
if (tom_is_fun_sym_CL((( EList<org.polymodel.algebra.IntConstraint> )tomMatch1_437))) {
int tomMatch1_end_431=tomMatch1_end_427 + 1;
do {
{
if (!(tomMatch1_end_431 >= tom_get_size_InstL_InstL(tomMatch1_418))) {
Instruction tomMatch1_441=tom_get_element_InstL_InstL(tomMatch1_418,tomMatch1_end_431);
if (tom_is_sort_Inst(tomMatch1_441)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch1_441))) {
org.polymodel.algebra.IntConstraintSystem tomMatch1_440=tom_get_slot_sconstraintSys_system(tomMatch1_441);
if (tom_is_sort_ICS(tomMatch1_440)) {
if (tom_is_fun_sym_system(((org.polymodel.algebra.IntConstraintSystem)tomMatch1_440))) {
 EList<org.polymodel.algebra.IntConstraint>  tomMatch1_443=tom_get_slot_system_constraints(tomMatch1_440);
if (tom_is_fun_sym_CL((( EList<org.polymodel.algebra.IntConstraint> )tomMatch1_443))) {
Instruction tom_op=((Instruction)tom__arg);

EObject container = 
tom_op.getRoot().eContainer();
if (container instanceof Type || container instanceof Symbol) {
traceTom("Rule 22 (ignored since instruction is used to define a type) ");
return 
tom_op;
}
touched=true;
Instruction res= 
tom_make_sconstraintSys(tom_make_system(tom_append_array_CL(tom_get_slice_CL(tomMatch1_443,0,tom_get_size_CL_CL(tomMatch1_443)),tom_append_array_CL(tom_get_slice_CL(tomMatch1_437,0,tom_get_size_CL_CL(tomMatch1_437)),tom_empty_array_CL(2)))));
res.setType((
tom_op).getType());
res.getAnnotations().putAll(
tom_op.getAnnotations());
return res;

}
}
}
}
}
}
tomMatch1_end_431=tomMatch1_end_431 + 1;
}
} while(!(tomMatch1_end_431 > tom_get_size_InstL_InstL(tomMatch1_418)));
}
}
}
}
}
}
tomMatch1_end_427=tomMatch1_end_427 + 1;
}
} while(!(tomMatch1_end_427 > tom_get_size_InstL_InstL(tomMatch1_418)));
}
}
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_BuildLinearExpression(VariableContextManager t0) { 
return new BuildLinearExpression(t0);
}




}
