/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import gecos.blocks.Block;
import gecos.blocks.SimpleForBlock;
import gecos.core.Symbol;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import tom.library.sl.VisitFailure;

public class LoopCounterAnalyzer {
	private Block root;
	public static boolean VERBOSE = true;
	

	public void debug(String message) {
		if (VERBOSE) {
			System.out.println(message);
		}
	}
	
	public LoopCounterAnalyzer(Block block) {
		root = block;
	}

	public EList<Symbol> findSurroundingLoopCounters(Block block) throws VisitFailure {
		Block current = block;
		EList<Symbol> loopCounters = new BasicEList<Symbol>();
		while (current != null & current != root) {
			if (current instanceof SimpleForBlock) {
				loopCounters.add(((SimpleForBlock) current).getIterator());
			}
			current = current.getParent();
		}
		if (current != null) {
			if (current instanceof SimpleForBlock) {
				loopCounters.add(((SimpleForBlock) current).getIterator());
			}
		}
		debug("Loop iterators for block "+block+"= "+loopCounters);
		return loopCounters;
	}
}