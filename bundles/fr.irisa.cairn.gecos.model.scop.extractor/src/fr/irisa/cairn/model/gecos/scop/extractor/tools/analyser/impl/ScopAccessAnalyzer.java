/*******************************************************************************
* Copyright (c) 2012 Universite de Rennes 1 / Inria.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the FreeBSD License v1.0
* which accompanies this distribution, and is available at
* http://www.freebsd.org/copyright/freebsd-license.html
*
* Contributors:
*    DERRIEN Steven - initial API and implementation
*    MORVAN Antoine - initial API and implementation
*    NAULLET Maxime - initial API and implementation
*******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.*;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ScopAccessAnalyzer extends AbstractInstructionTransformation{

private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_InnermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId())),tom_empty_list_Sequence()))))
;
}
private static boolean tom_equal_term_List(Object l1, Object l2) {
return  l1.equals(l2) ;
}
private static boolean tom_is_sort_List(Object t) {
return  t instanceof java.util.List ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_equal_term_org_polymodel_algebra_FuzzyBoolean(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_FuzzyBoolean(Object t) {
return t instanceof org.polymodel.algebra.FuzzyBoolean;
}
private static boolean tom_equal_term_org_polymodel_algebra_OUTPUT_FORMAT(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_OUTPUT_FORMAT(Object t) {
return t instanceof org.polymodel.algebra.OUTPUT_FORMAT;
}
private static boolean tom_equal_term_org_polymodel_algebra_Value(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_Value(Object t) {
return true;
}
private static boolean tom_equal_term_org_polymodel_algebra_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_ComparisonOperator(Object t) {
return t instanceof org.polymodel.algebra.ComparisonOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_CompositeOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_CompositeOperator(Object t) {
return t instanceof org.polymodel.algebra.CompositeOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_reductions_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_reductions_ReductionOperator(Object t) {
return t instanceof org.polymodel.algebra.reductions.ReductionOperator;
}
private static boolean tom_equal_term_ICS(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICS(Object t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static boolean tom_equal_term_ICSL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_equal_term_E(Object l1, Object l2) {
return (l1!=null && l2 instanceof IntExpression && ((IntExpression)l1).isEquivalent((IntExpression)l2) == FuzzyBoolean.YES) || l1==l2;
}
private static boolean tom_is_sort_E(Object t) {
return t instanceof IntExpression;
}
private static boolean tom_equal_term_EL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_EL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntExpression>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntExpression>)t).size()>0 && ((EList<org.polymodel.algebra.IntExpression>)t).get(0) instanceof org.polymodel.algebra.IntExpression));
}
private static boolean tom_equal_term_V(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_V(Object t) {
return t instanceof org.polymodel.algebra.Variable;
}
private static boolean tom_equal_term_vars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_vars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.Variable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.Variable>)t).size()>0 && ((EList<org.polymodel.algebra.Variable>)t).get(0) instanceof org.polymodel.algebra.Variable));
}
private static boolean tom_equal_term_T(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_T(Object t) {
return t instanceof org.polymodel.algebra.IntTerm;
}
private static boolean tom_equal_term_terms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_terms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static boolean tom_equal_term_C(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_C(Object t) {
return t instanceof org.polymodel.algebra.IntConstraint;
}
private static boolean tom_equal_term_CL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_CL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static boolean tom_equal_term_pterm(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterm(Object t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialTerm;
}
private static boolean tom_equal_term_pterms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialTerm));
}
private static boolean tom_equal_term_pvar(Object l1, Object l2) {
return (l1!=null && l2 instanceof PolynomialVariable && ((PolynomialVariable)l1).isEquivalent((PolynomialVariable)l2)) || l1==l2;
}
private static boolean tom_is_sort_pvar(Object t) {
return t instanceof PolynomialVariable;
}
private static boolean tom_equal_term_pvars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pvars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialVariable));
}
private static boolean tom_is_fun_sym_affine(IntExpression t) {
return t instanceof org.polymodel.algebra.affine.AffineExpression;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_affine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.affine.AffineExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_qaffine(IntExpression t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_qaffine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.quasiAffine.QuasiAffineExpression)t).getTerms());
}

//%include { algebra_domains_ops.tom }

private static boolean tom_equal_term_nodes(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_nodes(Object t) {
return  t instanceof EList<?> &&
    	(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
    	|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static boolean tom_equal_term_node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_node(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopNode;
}
private static boolean tom_equal_term_SymList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<gecos.core.Symbol>)t).size() == 0 
    	|| (((EList<gecos.core.Symbol>)t).size()>0 && ((EList<gecos.core.Symbol>)t).get(0) instanceof gecos.core.Symbol));
}
private static boolean tom_equal_term_ICSList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_is_fun_sym_symref(Instruction t) {
return t instanceof SymbolInstruction;
}
private static Symbol tom_get_slot_symref_symbol(Instruction t) {
return ((SymbolInstruction)t).getSymbol();
}
private static boolean tom_is_fun_sym_ival(Instruction t) {
return t instanceof IntInstruction;
}
private static  long  tom_get_slot_ival_value(Instruction t) {
return ((IntInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static Instruction tom_make_generic( String  _name,  EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric(_name, _children);
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_set(Instruction t) {
return t instanceof SetInstruction;
}
private static Instruction tom_make_set(Instruction _dest, Instruction _source) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createSet(_dest, _source);
}
private static Instruction tom_get_slot_set_dest(Instruction t) {
return ((SetInstruction)t).getDest();
}
private static Instruction tom_get_slot_set_source(Instruction t) {
return ((SetInstruction)t).getSource();
}
private static boolean tom_is_fun_sym_sarray(Instruction t) {
return t instanceof SimpleArrayInstruction;
}
private static Instruction tom_get_slot_sarray_dest(Instruction t) {
return ((SimpleArrayInstruction)t).getDest();
}
private static  EList<Instruction>  tom_get_slot_sarray_index(Instruction t) {
return enforce(((SimpleArrayInstruction)t).getIndex());
}
private static boolean tom_is_fun_sym_address(Instruction t) {
return t instanceof AddressInstruction;
}
private static Instruction tom_make_address(Instruction _address) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createAddress(_address);
}
private static Instruction tom_get_slot_address_address(Instruction t) {
return ((AddressInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_indir(Instruction t) {
return t instanceof IndirInstruction;
}
private static Instruction tom_get_slot_indir_address(Instruction t) {
return ((IndirInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_convert(Instruction t) {
return t instanceof ConvertInstruction;
}
private static Instruction tom_get_slot_convert_expr(Instruction t) {
return ((ConvertInstruction)t).getExpr();
}
private static boolean tom_is_fun_sym_fcall(Instruction t) {
return t instanceof CallInstruction;
}
private static Instruction tom_get_slot_fcall_address(Instruction t) {
return ((CallInstruction)t).getAddress();
}
private static  EList<Instruction>  tom_get_slot_fcall_args(Instruction t) {
return enforce(((CallInstruction)t).getArgs());
}
private static boolean tom_is_fun_sym_brcond(Instruction t) {
return t instanceof CondInstruction;
}
private static Instruction tom_make_brcond(Instruction _cond) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createBrcond(_cond);
}
private static Instruction tom_get_slot_brcond_cond(Instruction t) {
return ((CondInstruction)t).getCond();
}
private static boolean tom_is_fun_sym_sexpr(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
}
private static IntExpression tom_get_slot_sexpr_expr(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopIntExpression)t).getExpr();
}
private static boolean tom_is_fun_sym_sconstraintSys(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
}
private static org.polymodel.algebra.IntConstraintSystem tom_get_slot_sconstraintSys_system(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem)t).getSystem();
}
private static boolean tom_is_fun_sym_iread(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopRead;
}
private static Instruction tom_make_iread(Symbol _sym,  EList<org.polymodel.algebra.IntExpression>  _indexExpressions) { 
return fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory.createIread(_sym, _indexExpressions);
}
private static Symbol tom_get_slot_iread_sym(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopRead)t).getSym();
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_iread_indexExpressions(Instruction t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopRead)t).getIndexExpressions());
}
private static boolean tom_is_fun_sym_iwrite(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopWrite;
}
private static Instruction tom_make_iwrite(Symbol _sym,  EList<org.polymodel.algebra.IntExpression>  _indexExpressions) { 
return fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory.createIwrite(_sym, _indexExpressions);
}
private static Symbol tom_get_slot_iwrite_sym(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopWrite)t).getSym();
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_iwrite_indexExpressions(Instruction t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopWrite)t).getIndexExpressions());
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_make_stmt( String  _name,  EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory.createStmt(_name, _children);
}


public ScopAccessAnalyzer() {
}

private static final boolean VERBOSE = true  & ScopExtractorPass.VERBOSE;

private static void debug(String mess) {
if(VERBOSE) {
System.out.println(mess);
}
}

@Override
protected Instruction apply(Instruction i) {
try {
debug("Analyzing "+i+ " for Read/Write SCOP access");
Instruction res = 
tom_make_InnermostId(tom_make_AnalyseStatements()).visitLight(i, tom.mapping.GenericIntrospector.INSTANCE);
debug("\t-Transformed into "+res.toString().replace("\n"," ")+ "\n");
return res;
} catch (VisitFailure e){
throw new SCoPException(i.getBasicBlock(),"Visit failure "+e.getMessage());
}
}

private static ScopWrite buildScopWriteAccess(SimpleArrayInstruction array) {
EList<IntExpression>  expList = new BasicEList<IntExpression>();
for(Instruction index : array.getIndex()) {
if(index instanceof ScopIntExpression) {
expList.add(((ScopIntExpression) index).getExpr());
} else {
throw new SCoPException(array.getBasicBlock(),"Non affine array access in "+array);
}
}
ScopWrite swrite = (ScopWrite)(
tom_make_iwrite(array.getSymbol(),expList));
swrite.getAnnotations().putAll(array.getAnnotations());
return swrite;
}

private static ScopRead buildScopReadAccess(SimpleArrayInstruction array) {
EList<IntExpression>  expList = new BasicEList<IntExpression>();
for(Instruction index : array.getIndex()) {
if(index instanceof ScopIntExpression) {
expList.add(((ScopIntExpression) index).getExpr());
} else {
throw new SCoPException(array.getBasicBlock(),"Non affine array access in "+array);
}
}
ScopRead sread = (ScopRead)(
tom_make_iread(array.getSymbol(),expList));
sread.getAnnotations().putAll(array.getAnnotations());
return sread;
}

private static ScopWrite buildScopWriteAccess(SymbolInstruction array) {
EList<IntExpression>  expList = new BasicEList<IntExpression>();
ScopWrite write = (ScopWrite)(
tom_make_iwrite(array.getSymbol(),expList));
write.getAnnotations().putAll(array.getAnnotations());
return write;
}

private static ScopRead buildScopReadAccess(SymbolInstruction array) {
EList<IntExpression>  expList = new BasicEList<IntExpression>();
ScopRead read = (ScopRead)(
tom_make_iread(array.getSymbol(),expList));
read.getAnnotations().putAll(array.getAnnotations());
return read;
}


public static class AnalyseStatements extends tom.library.sl.AbstractStrategyBasic {
public AnalyseStatements() {
super(tom_make_Identity());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_set(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_1=tom_get_slot_set_dest(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_1)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch1_1))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_sarray_index(tomMatch1_1)))) {
Instruction tom_a=tomMatch1_1;
Instruction tom_lhs=tom_get_slot_set_source(((Instruction)tom__arg));
Instruction tom_statement=((Instruction)tom__arg);

Instruction stmt= 
tom_statement;
//if ((`lhs) instanceof ScopIntExpression) {
debug("\t\tValue to set : " + 
tom_lhs);
debug("\t-Rule 1 : Array write operation "+(
tom_a)+ "found in "+(
tom_statement).getRoot());
Instruction symbol = 
buildScopWriteAccess((SimpleArrayInstruction)tom_a);
symbol.setType((
tom_a).getType());
Instruction setInstruction = 
tom_make_set(symbol,tom_lhs); 
setInstruction.setType(
tom_statement.getType());
String label= "S"; 
ScopInstructionStatement res = (ScopInstructionStatement) (
tom_make_stmt(label,tom_cons_array_InstL(setInstruction,tom_empty_array_InstL(1))));
res.setType(
tom_statement.getType()); 
res.getAnnotations().putAll(
tom_statement.getAnnotations());
return res;
/*} else {
throw new SCoPException(stmt.getBasicBlock(),"Right hand side of the set is not a GScopExpression");
}*/

}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_13=tom_get_slot_address_address(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_13)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch1_13))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_sarray_index(tomMatch1_13)))) {
Instruction tom_a=tomMatch1_13;

Instruction stmt= 
((Instruction)tom__arg);
if(isCallWriteArgument(
tom_a)) {
debug("\t-Rule 2 : Implicit array write operation "+(
tom_a)+ " found  in call "+(
tom_a).getRoot());
Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)
tom_a);
symbol.setType((
tom_a).getType());
Instruction address = 
tom_make_address(symbol);
address.setType(stmt.getType());
return address;
} else {
throw new SCoPException(stmt.getBasicBlock(),"Illegal array pattern "+stmt.getRoot());
}

}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sarray(((Instruction)((Instruction)tom__arg)))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_sarray_index(((Instruction)tom__arg))))) {
Instruction tom_statement=((Instruction)tom__arg);

if(!(isWrite(
tom_statement) || isCallWriteArgument(
tom_statement))){
debug("\t-Rule 3 : array read operation "+(
tom_statement)+ " found  in "+(
tom_statement).getRoot());
Instruction symbol = buildScopReadAccess((SimpleArrayInstruction)
tom_statement);
symbol.setType((
tom_statement).getType());
return symbol;
}

}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_set(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_32=tom_get_slot_set_dest(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_32)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch1_32))) {
Instruction tom_sym=tomMatch1_32;
Instruction tom_statement=((Instruction)tom__arg);

Symbol s = (
tom_get_slot_symref_symbol(tomMatch1_32));
Type t = s.getType();

// look for the base type when using aliases
while (t instanceof gecos.types.AliasType)
t = ((gecos.types.AliasType)t).getAlias();

if(t instanceof gecos.types.BaseType) {
debug("\t-Rule 4 : scalar write operation "+(
tom_statement)+ " found  in "+(
tom_statement).getRoot());
Instruction symbol = 
buildScopWriteAccess((SymbolInstruction)tom_sym);
symbol.setType((
tom_sym).getType());
Instruction setInstruction = 
tom_make_set(symbol,tom_get_slot_set_source(((Instruction)tom__arg)));
setInstruction.setType(
tom_statement.getType());
ScopInstructionStatement res = (ScopInstructionStatement) (
tom_make_stmt("S",tom_cons_array_InstL(setInstruction,tom_empty_array_InstL(1))));
res.setType(setInstruction.getType());
res.getAnnotations().putAll(
tom_statement.getAnnotations());
return res; 
} else {
throw new SCoPException(
tom_statement.getBasicBlock(),"Left hand side of the set has not a BaseType");
}

}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)tom__arg)))) {
Instruction tom_statement=((Instruction)tom__arg);

Symbol s = (
tom_get_slot_symref_symbol(((Instruction)tom__arg)));
Type t = s.getType();
// look for the base type when using aliases
while (t instanceof gecos.types.AliasType)
t = ((gecos.types.AliasType)t).getAlias();
if(t instanceof gecos.types.BaseType) {
//ScopWrite s = scalar(`var,`statement);
if(!isWrite(
tom_statement)){
debug("\t-Rule 5 : scalar read operation "+(
tom_statement)+ " found  in "+(
tom_statement).getRoot());
Instruction symbol = buildScopReadAccess((SymbolInstruction)
tom_statement);
symbol.setType((
tom_statement).getType());
return symbol;
}
}

}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_fcall(((Instruction)((Instruction)tom__arg)))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_fcall_args(((Instruction)tom__arg))))) {
Instruction tom_c=((Instruction)tom__arg);

Instruction i = (
tom_c);
debug("\t-Rule 6 : function call "+i.getRoot());
CallInstruction callInst = (CallInstruction) i;
callInst.setType(
tom_c.getType()); 
return AnalyseCallStatement(callInst);

}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_indir(((Instruction)((Instruction)tom__arg)))) {
Instruction tom_c=((Instruction)tom__arg);

throw new SCoPException((
tom_c).getBasicBlock(),"Found indirection "+(
tom_c));

}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_56=tom_get_slot_brcond_cond(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_56)) {
if (tom_is_fun_sym_generic(((Instruction)tomMatch1_56))) {
 String  tomMatch1_59=tom_get_slot_generic_name(tomMatch1_56);
if ( true ) {
if (tom_equal_term_String("booldisjunction", tomMatch1_59)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_generic_children(tomMatch1_56)))) {

return (
((Instruction)tom__arg));

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_69=tom_get_slot_brcond_cond(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_69)) {
if (tom_is_fun_sym_generic(((Instruction)tomMatch1_69))) {
 String  tomMatch1_72=tom_get_slot_generic_name(tomMatch1_69);
if ( true ) {
if (tom_equal_term_String("lor", tomMatch1_72)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_generic_children(tomMatch1_69)))) {

GenericInstruction copy = (GenericInstruction)(
tomMatch1_69).copy();
Instruction res = 
tom_make_brcond(tom_make_generic("booldisjunction",copy.getChildren()));
debug("\t-Rule 7 : normalizing constraints : "+(
((Instruction)tom__arg))+"->"+res);
return res ;

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_82=tom_get_slot_brcond_cond(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_82)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch1_82))) {

Instruction res =  
tom_make_brcond(tom_make_generic("booldisjunction",tom_cons_array_InstL(tomMatch1_82.copy(),tom_empty_array_InstL(1))));  
debug("\t-Rule 8 : normalizing constraints : "+(
((Instruction)tom__arg))+"->"+res);
return res ;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom__arg)))) {
Instruction tom_inst=((Instruction)tom__arg);

if (!((
tom_inst).getRoot() instanceof ScopInstructionStatement)) {
Instruction copy = (
tom_inst).copy();
Instruction res =  (Instruction) (
tom_make_stmt("S",tom_cons_array_InstL(copy,tom_empty_array_InstL(1))));  
debug("\t-Rule 9 : non affine guard : "+(
tom_inst)+"->"+res);
return res ;
} else {
return (
tom_inst);
}

}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_AnalyseStatements() { 
return new AnalyseStatements();
}



private static Instruction AnalyseCallStatement(CallInstruction callInst) {
Instruction rootIns = callInst.getRootInstuction();
List<Instruction> args = callInst.getArgs();

/* analyzing function call arguments, to determine if the call is supported 
as part of a SCoP */
for(Instruction arg: args){
boolean pass=false;

{
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_sarray(((Instruction)((Instruction)arg)))) {
 EList<Instruction>  tomMatch2_2=tom_get_slot_sarray_index(((Instruction)arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_2))) {
int tomMatch2_6=0;
if (!(tomMatch2_6 >= tom_get_size_InstL_InstL(tomMatch2_2))) {
if (tomMatch2_6 + 1 >= tom_get_size_InstL_InstL(tomMatch2_2)) {

debug("  - Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_9=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_9)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch2_9))) {
Instruction tomMatch2_12=tom_get_slot_address_address(tomMatch2_9);
if (tom_is_sort_Inst(tomMatch2_12)) {
if (tom_is_fun_sym_iwrite(((Instruction)tomMatch2_12))) {

debug("\t\t-Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_20=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_20)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch2_20))) {
Instruction tomMatch2_23=tom_get_slot_address_address(tomMatch2_20);
if (tom_is_sort_Inst(tomMatch2_23)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch2_23))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_sarray_index(tomMatch2_23)))) {

debug("\t\t-Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_34=tom_get_slot_address_address(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_34)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch2_34))) {
 EList<Instruction>  tomMatch2_38=tom_get_slot_sarray_index(tomMatch2_34);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_38))) {
int tomMatch2_42=0;
if (!(tomMatch2_42 >= tom_get_size_InstL_InstL(tomMatch2_38))) {
if (tomMatch2_42 + 1 >= tom_get_size_InstL_InstL(tomMatch2_38)) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_45=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_45)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch2_45))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)arg)))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_sexpr(((Instruction)((Instruction)arg)))) {
IntExpression tomMatch2_56=tom_get_slot_sexpr_expr(((Instruction)arg));
if (tom_is_sort_E(tomMatch2_56)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch2_56))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_sexpr(((Instruction)((Instruction)arg)))) {
IntExpression tomMatch2_63=tom_get_slot_sexpr_expr(((Instruction)arg));
if (tom_is_sort_E(tomMatch2_63)) {
if (tom_is_fun_sym_qaffine(((IntExpression)tomMatch2_63))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_70=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_70)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch2_70))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_ival(((Instruction)((Instruction)arg)))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_81=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_81)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch2_81))) {
Instruction tomMatch2_84=tom_get_slot_address_address(tomMatch2_81);
if (tom_is_sort_Inst(tomMatch2_84)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch2_84))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)arg)))) {
Instruction tomMatch2_91=tom_get_slot_address_address(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch2_91)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch2_91))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_iread(((Instruction)((Instruction)arg)))) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)arg)))) {
 String  tomMatch2_103=tom_get_slot_generic_name(((Instruction)arg));
if ( true ) {
if (tom_equal_term_String("add", tomMatch2_103)) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)arg)))) {
 String  tomMatch2_110=tom_get_slot_generic_name(((Instruction)arg));
if ( true ) {
if (tom_equal_term_String("sub", tomMatch2_110)) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)arg)))) {
 String  tomMatch2_117=tom_get_slot_generic_name(((Instruction)arg));
if ( true ) {
if (tom_equal_term_String("mul", tomMatch2_117)) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)arg)))) {
 String  tomMatch2_124=tom_get_slot_generic_name(((Instruction)arg));
if ( true ) {
if (tom_equal_term_String("div", tomMatch2_124)) {

debug("\t\t- Argument "+arg + " is OK");
pass=true;

}
}
}
}
}
}
}

if(!pass) {
throw new SCoPException(callInst.getBasicBlock(), "Argument "+arg+ ":"+arg.getClass().getSimpleName()+" in " +callInst + " is not compatible with a pure function call");
}
}

/* analyzing implicit Write operations (using pointers as arguments) */

for(Instruction arg: args){ 

{
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch3_1=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch3_1)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch3_1))) {
Instruction tomMatch3_4=tom_get_slot_address_address(tomMatch3_1);
if (tom_is_sort_Inst(tomMatch3_4)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch3_4))) {
 EList<Instruction>  tomMatch3_8=tom_get_slot_sarray_index(tomMatch3_4);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_8))) {
int tomMatch3_12=0;
if (!(tomMatch3_12 >= tom_get_size_InstL_InstL(tomMatch3_8))) {
if (tomMatch3_12 + 1 >= tom_get_size_InstL_InstL(tomMatch3_8)) {
Instruction tom_inst=tomMatch3_4;

Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)
tom_inst);
symbol.setType((
tom_inst).getType());
(
tom_inst).substituteWith(symbol);

}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)arg)))) {
Instruction tomMatch3_15=tom_get_slot_address_address(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch3_15)) {
if (tom_is_fun_sym_sarray(((Instruction)tomMatch3_15))) {
 EList<Instruction>  tomMatch3_19=tom_get_slot_sarray_index(tomMatch3_15);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_19))) {
int tomMatch3_23=0;
if (!(tomMatch3_23 >= tom_get_size_InstL_InstL(tomMatch3_19))) {
if (tomMatch3_23 + 1 >= tom_get_size_InstL_InstL(tomMatch3_19)) {
Instruction tom_inst=tomMatch3_15;

Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)
tom_inst);
symbol.setType((
tom_inst).getType());
(
tom_inst).substituteWith(symbol);

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)arg)))) {
Instruction tomMatch3_26=tom_get_slot_address_address(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch3_26)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch3_26))) {
Instruction tom_inst=tomMatch3_26;

Instruction symbol = buildScopWriteAccess((SymbolInstruction)
tom_inst);
symbol.setType((
tom_inst).getType());
(
tom_inst).substituteWith(symbol);

}
}
}
}
}
}
{
if (tom_is_sort_Inst(arg)) {
if (tom_is_sort_Inst(((Instruction)arg))) {
if (tom_is_fun_sym_convert(((Instruction)((Instruction)arg)))) {
Instruction tomMatch3_33=tom_get_slot_convert_expr(((Instruction)arg));
if (tom_is_sort_Inst(tomMatch3_33)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch3_33))) {
Instruction tomMatch3_36=tom_get_slot_address_address(tomMatch3_33);
if (tom_is_sort_Inst(tomMatch3_36)) {
if (tom_is_fun_sym_symref(((Instruction)tomMatch3_36))) {
Instruction tom_inst=tomMatch3_36;

Instruction symbol = buildScopWriteAccess((SymbolInstruction)
tom_inst);
symbol.setType((
tom_inst).getType());
(
tom_inst).substituteWith(symbol);

}
}
}
}
}
}
}
}
}

}
return callInst;
}

private static boolean isWrite(Instruction i){
if(i.getParent() instanceof SetInstruction){
SetInstruction set = (SetInstruction)i.getParent();
if(set.getDest()==i)
return true;
}
return false;
}

private static boolean isCallWriteArgument(Instruction i){
Instruction parent = i.getParent();
if(!(parent instanceof AddressInstruction)) return false;
while(parent!=null) {
if(parent instanceof CallInstruction) return true;
parent = parent.getParent();
}
return false;
}
}
