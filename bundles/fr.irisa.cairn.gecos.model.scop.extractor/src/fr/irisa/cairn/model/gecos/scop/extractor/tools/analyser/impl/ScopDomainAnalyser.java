/*******************************************************************************
* Copyright (c) 2012 Universite de Rennes 1 / Inria.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the FreeBSD License v1.0
* which accompanies this distribution, and is available at
* http://www.freebsd.org/copyright/freebsd-license.html
*
* Contributors:
*    DERRIEN Steven - initial API and implementation
*    MORVAN Antoine - initial API and implementation
*    NAULLET Maxime - initial API and implementation
*******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.*;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractBlockTransformation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.AbstractScopDomainAnalyser;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.ScopDomainBuilder;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.CoreFactory;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ScopDomainAnalyser extends ScopDomainBuilder {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_InnermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId())),tom_empty_list_Sequence()))))
;
}
private static boolean tom_equal_term_List(Object l1, Object l2) {
return  l1.equals(l2) ;
}
private static boolean tom_is_sort_List(Object t) {
return  t instanceof java.util.List ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_is_fun_sym_BlkL( EList<Block>  t) {
return  t instanceof EList<?> &&
 		(((EList<Block>)t).size() == 0 
 		|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static  EList<Block>  tom_empty_array_BlkL(int n) { 
return  new BasicEList<Block>(n) ;
}
private static  EList<Block>  tom_cons_array_BlkL(Block e,  EList<Block>  l) { 
return  append(e,l) ;
}
private static Block tom_get_element_BlkL_BlkL( EList<Block>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_BlkL_BlkL( EList<Block>  l) {
return  l.size() ;
}

  private static   EList<Block>  tom_get_slice_BlkL( EList<Block>  subject, int begin, int end) {
     EList<Block>  result =  new BasicEList<Block>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Block>  tom_append_array_BlkL( EList<Block>  l2,  EList<Block>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Block>  result =  new BasicEList<Block>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_equal_term_org_polymodel_algebra_FuzzyBoolean(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_FuzzyBoolean(Object t) {
return t instanceof org.polymodel.algebra.FuzzyBoolean;
}
private static boolean tom_equal_term_org_polymodel_algebra_OUTPUT_FORMAT(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_OUTPUT_FORMAT(Object t) {
return t instanceof org.polymodel.algebra.OUTPUT_FORMAT;
}
private static boolean tom_equal_term_org_polymodel_algebra_Value(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_Value(Object t) {
return true;
}
private static boolean tom_equal_term_org_polymodel_algebra_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_ComparisonOperator(Object t) {
return t instanceof org.polymodel.algebra.ComparisonOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_CompositeOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_CompositeOperator(Object t) {
return t instanceof org.polymodel.algebra.CompositeOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_reductions_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_reductions_ReductionOperator(Object t) {
return t instanceof org.polymodel.algebra.reductions.ReductionOperator;
}
private static boolean tom_equal_term_ICS(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICS(Object t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static boolean tom_equal_term_ICSL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_equal_term_E(Object l1, Object l2) {
return (l1!=null && l2 instanceof IntExpression && ((IntExpression)l1).isEquivalent((IntExpression)l2) == FuzzyBoolean.YES) || l1==l2;
}
private static boolean tom_is_sort_E(Object t) {
return t instanceof IntExpression;
}
private static boolean tom_equal_term_EL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_EL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntExpression>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntExpression>)t).size()>0 && ((EList<org.polymodel.algebra.IntExpression>)t).get(0) instanceof org.polymodel.algebra.IntExpression));
}
private static boolean tom_equal_term_V(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_V(Object t) {
return t instanceof org.polymodel.algebra.Variable;
}
private static boolean tom_equal_term_vars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_vars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.Variable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.Variable>)t).size()>0 && ((EList<org.polymodel.algebra.Variable>)t).get(0) instanceof org.polymodel.algebra.Variable));
}
private static boolean tom_equal_term_T(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_T(Object t) {
return t instanceof org.polymodel.algebra.IntTerm;
}
private static boolean tom_equal_term_terms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_terms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static boolean tom_equal_term_C(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_C(Object t) {
return t instanceof org.polymodel.algebra.IntConstraint;
}
private static boolean tom_equal_term_CL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_CL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static boolean tom_equal_term_pterm(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterm(Object t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialTerm;
}
private static boolean tom_equal_term_pterms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialTerm));
}
private static boolean tom_equal_term_pvar(Object l1, Object l2) {
return (l1!=null && l2 instanceof PolynomialVariable && ((PolynomialVariable)l1).isEquivalent((PolynomialVariable)l2)) || l1==l2;
}
private static boolean tom_is_sort_pvar(Object t) {
return t instanceof PolynomialVariable;
}
private static boolean tom_equal_term_pvars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pvars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialVariable));
}
private static boolean tom_equal_term_nodes(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_nodes(Object t) {
return  t instanceof EList<?> &&
    	(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
    	|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static boolean tom_is_fun_sym_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  t) {
return  t instanceof EList<?> &&
 		(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
 		|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_empty_array_nodes(int n) { 
return  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(n) ;
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_cons_array_nodes(fr.irisa.cairn.gecos.model.scop.ScopNode e,  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l) { 
return  append(e,l) ;
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_get_element_nodes_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_nodes_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l) {
return  l.size() ;
}

  private static   EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_get_slice_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  subject, int begin, int end) {
     EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  result =  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_append_array_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l2,  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  result =  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_node(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopNode;
}
private static boolean tom_equal_term_SymList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<gecos.core.Symbol>)t).size() == 0 
    	|| (((EList<gecos.core.Symbol>)t).size()>0 && ((EList<gecos.core.Symbol>)t).get(0) instanceof gecos.core.Symbol));
}
private static boolean tom_equal_term_ICSList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_is_fun_sym_ICSList( EList<org.polymodel.algebra.IntConstraintSystem>  t) {
return  t instanceof EList<?> &&
 		(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
 		|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static  EList<org.polymodel.algebra.IntConstraintSystem>  tom_empty_array_ICSList(int n) { 
return  new BasicEList<org.polymodel.algebra.IntConstraintSystem>(n) ;
}
private static  EList<org.polymodel.algebra.IntConstraintSystem>  tom_cons_array_ICSList(org.polymodel.algebra.IntConstraintSystem e,  EList<org.polymodel.algebra.IntConstraintSystem>  l) { 
return  append(e,l) ;
}
private static org.polymodel.algebra.IntConstraintSystem tom_get_element_ICSList_ICSList( EList<org.polymodel.algebra.IntConstraintSystem>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_ICSList_ICSList( EList<org.polymodel.algebra.IntConstraintSystem>  l) {
return  l.size() ;
}

  private static   EList<org.polymodel.algebra.IntConstraintSystem>  tom_get_slice_ICSList( EList<org.polymodel.algebra.IntConstraintSystem>  subject, int begin, int end) {
     EList<org.polymodel.algebra.IntConstraintSystem>  result =  new BasicEList<org.polymodel.algebra.IntConstraintSystem>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<org.polymodel.algebra.IntConstraintSystem>  tom_append_array_ICSList( EList<org.polymodel.algebra.IntConstraintSystem>  l2,  EList<org.polymodel.algebra.IntConstraintSystem>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<org.polymodel.algebra.IntConstraintSystem>  result =  new BasicEList<org.polymodel.algebra.IntConstraintSystem>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_BB(Block t) {
return t instanceof BasicBlock;
}
private static  EList<Instruction>  tom_get_slot_BB_instructions(Block t) {
return enforce(((BasicBlock)t).getInstructions());
}
private static boolean tom_is_fun_sym_cblock(Block t) {
return t instanceof CompositeBlock;
}
private static  EList<Block>  tom_get_slot_cblock_children(Block t) {
return enforce(((CompositeBlock)t).getChildren());
}
private static boolean tom_is_fun_sym_ifThenElse(Block t) {
return t instanceof IfBlock;
}
private static Block tom_get_slot_ifThenElse_testBlock(Block t) {
return ((IfBlock)t).getTestBlock();
}
private static Block tom_get_slot_ifThenElse_thenBlock(Block t) {
return ((IfBlock)t).getThenBlock();
}
private static Block tom_get_slot_ifThenElse_elseBlock(Block t) {
return ((IfBlock)t).getElseBlock();
}
private static boolean tom_is_fun_sym_forBlock(Block t) {
return t instanceof ForBlock;
}
private static Block tom_get_slot_forBlock_initBlock(Block t) {
return ((ForBlock)t).getInitBlock();
}
private static Block tom_get_slot_forBlock_testBlock(Block t) {
return ((ForBlock)t).getTestBlock();
}
private static Block tom_get_slot_forBlock_stepBlock(Block t) {
return ((ForBlock)t).getStepBlock();
}
private static Block tom_get_slot_forBlock_bodyBlock(Block t) {
return ((ForBlock)t).getBodyBlock();
}
private static boolean tom_is_fun_sym_brcond(Instruction t) {
return t instanceof CondInstruction;
}
private static Instruction tom_get_slot_brcond_cond(Instruction t) {
return ((CondInstruction)t).getCond();
}
private static boolean tom_is_fun_sym_sblk(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopBlock;
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_get_slot_sblk_children(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopBlock)t).getChildren());
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_make_guard( EList<org.polymodel.algebra.IntConstraintSystem>  _predicate, fr.irisa.cairn.gecos.model.scop.ScopNode _thenNode, fr.irisa.cairn.gecos.model.scop.ScopNode _elseNode) { 
return fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory.createGuard(_predicate, _thenNode, _elseNode);
}
private static boolean tom_is_fun_sym_stmt(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
}
private static  String  tom_get_slot_stmt_name(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement)t).getName();
}
private static  EList<Instruction>  tom_get_slot_stmt_children(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement)t).getChildren());
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_make_scopBlk( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  _children) { 
return fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory.createScopBlk(_children);
}
private static boolean tom_is_fun_sym_sconstraintSys(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
}
private static org.polymodel.algebra.IntConstraintSystem tom_get_slot_sconstraintSys_system(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem)t).getSystem();
}
private static boolean tom_equal_term_Context(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Context(Object t) {
return t instanceof VariableContextManager;
}
private static boolean tom_equal_term_RootScopNode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_RootScopNode(Object t) {
return t instanceof GecosScopBlock;
}
private static boolean tom_equal_term_ScopDomainAnalyser(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ScopDomainAnalyser(Object t) {
return t instanceof ScopDomainAnalyser;
}


public ScopDomainAnalyser(VariableContextManager context) {
super(context);
}

protected Block tom_rewrite(Block block) throws VisitFailure, SCoPException {

try {
return 
tom_make_InnermostId(tom_make_DomainAnalyzer(context,this)).visitLight(block, tom.mapping.GenericIntrospector.INSTANCE);
} catch(VisitFailure e) {
throw new SCoPException(block, "Tom strategy failed");
} catch(SCoPException e) {
throw e;
}
}




public static class DomainAnalyzer extends tom.library.sl.AbstractStrategyBasic {
private VariableContextManager _context;
private ScopDomainAnalyser scopDomainAnalyser;
public DomainAnalyzer(VariableContextManager _context, ScopDomainAnalyser scopDomainAnalyser) {
super(tom_make_Identity());
this._context=_context;
this.scopDomainAnalyser=scopDomainAnalyser;
}
public VariableContextManager get_context() {
return _context;
}
public ScopDomainAnalyser getscopDomainAnalyser() {
return scopDomainAnalyser;
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Blk(v)) {
return ((T)visit_Blk(((Block)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Block _visit_Blk(Block arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Block)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Block visit_Blk(Block tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Blk(tom__arg)) {
Block tom_a=((Block)tom__arg);

debug("\nVisiting "+(
tom_a).getClass().getSimpleName()+":"+(
tom_a));

}
}
{
if (tom_is_sort_Blk(tom__arg)) {
if (tom_is_sort_Blk(((Block)tom__arg))) {
if (tom_is_fun_sym_forBlock(((Block)((Block)tom__arg)))) {
Block tom_b=((Block)tom__arg);

int dimension = findDimension(
tom_b);
debug("Dimension for "+(
tom_b)+"="+dimension);
SimpleForBlock simpleForBlock = (SimpleForBlock)(
tom_b);
List<Symbol> iterators = loopCounterAnalyzer.findSurroundingLoopCounters((
tom_b));
debug("Entering for loop "+(
tom_b)+" iterators = "+iterators);
context.activateIterators(iterators, dimension);
ScopDimension iterator = context.lookUp(simpleForBlock.getIterator());
context.removeIterators(iterators);

Instruction lb = simpleForBlock.getLb();
Instruction ub = simpleForBlock.getUb();
Instruction stride = simpleForBlock.getStride();
try {
extractForInformation(scopDomainAnalyser,
simpleForBlock,
iterator,
((ScopIntExpression)lb).getExpr(),
((ScopIntExpression)ub).getExpr(),
((ScopIntExpression)stride).getExpr(),
simpleForBlock.getBodyBlock());

} catch (ClassCastException e) {
e.printStackTrace();
throw new ClassCastException("Non affine loop : something went wrong ! \n ub="+ub+"\nlb="+lb+"\nstride="+stride);
}		

}
}
}
}
{
if (tom_is_sort_Blk(tom__arg)) {
if (tom_is_sort_Blk(((Block)tom__arg))) {
if (tom_is_fun_sym_ifThenElse(((Block)((Block)tom__arg)))) {
Block tomMatch1_9=tom_get_slot_ifThenElse_testBlock(((Block)tom__arg));
if (tom_is_sort_Blk(tomMatch1_9)) {
if (tom_is_fun_sym_BB(((Block)tomMatch1_9))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_BB_instructions(tomMatch1_9)))) {

debug("\t- Checking empty If/Else guard "+(
((Block)tom__arg))); 

}
}
}
}
}
}
}
{
if (tom_is_sort_Blk(tom__arg)) {
if (tom_is_sort_Blk(((Block)tom__arg))) {
if (tom_is_fun_sym_ifThenElse(((Block)((Block)tom__arg)))) {
Block tomMatch1_21=tom_get_slot_ifThenElse_testBlock(((Block)tom__arg));
if (tom_is_sort_Blk(tomMatch1_21)) {
if (tom_is_fun_sym_BB(((Block)tomMatch1_21))) {
 EList<Instruction>  tomMatch1_26=tom_get_slot_BB_instructions(tomMatch1_21);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_26))) {
int tomMatch1_30=0;
if (!(tomMatch1_30 >= tom_get_size_InstL_InstL(tomMatch1_26))) {
Instruction tom_guard=tom_get_element_InstL_InstL(tomMatch1_26,tomMatch1_30);
if (tomMatch1_30 + 1 >= tom_get_size_InstL_InstL(tomMatch1_26)) {
Block tom__then=tom_get_slot_ifThenElse_thenBlock(((Block)tom__arg));
Block tom__else=tom_get_slot_ifThenElse_elseBlock(((Block)tom__arg));
Block tom_b=((Block)tom__arg);

debug("\t- Checking If/Else guard "+(
tom_guard)+":"+(
tom_guard).getClass().getSimpleName());
boolean affine =false;

{
{
if (tom_is_sort_Inst(tom_guard)) {
if (tom_is_sort_Inst(((Instruction)tom_guard))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom_guard)))) {
Instruction tomMatch2_1=tom_get_slot_brcond_cond(((Instruction)tom_guard));
if (tom_is_sort_Inst(tomMatch2_1)) {
if (tom_is_fun_sym_generic(((Instruction)tomMatch2_1))) {
 String  tomMatch2_4=tom_get_slot_generic_name(tomMatch2_1);
 EList<Instruction>  tomMatch2_5=tom_get_slot_generic_children(tomMatch2_1);
if ( true ) {
if (tom_equal_term_String("booldisjunction", tomMatch2_4)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_5))) {

debug("\t\t- If/Else guard is a union of affine constraint systems");
affine=true;
buildIf(
tom_b, 
tom_guard, 
tom_get_slice_InstL(tomMatch2_5,0,tom_get_size_InstL_InstL(tomMatch2_5)), 
tom__then, 
tom__else, scopDomainAnalyser);

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom_guard)) {
if (tom_is_sort_Inst(((Instruction)tom_guard))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom_guard)))) {
Instruction tomMatch2_14=tom_get_slot_brcond_cond(((Instruction)tom_guard));
if (tom_is_sort_Inst(tomMatch2_14)) {
if (tom_is_fun_sym_generic(((Instruction)tomMatch2_14))) {
 String  tomMatch2_17=tom_get_slot_generic_name(tomMatch2_14);
 EList<Instruction>  tomMatch2_18=tom_get_slot_generic_children(tomMatch2_14);
if ( true ) {
if (tom_equal_term_String("lor", tomMatch2_17)) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_18))) {

debug("\t\t- If/Else guard is a union of affine constraint systems");
affine=true;
buildIf(
tom_b, 
tom_guard, 
tom_get_slice_InstL(tomMatch2_18,0,tom_get_size_InstL_InstL(tomMatch2_18)), 
tom__then, 
tom__else, scopDomainAnalyser);
throw new SCoPException((
tom_b),"Should not happen !");


}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom_guard)) {
if (tom_is_sort_Inst(((Instruction)tom_guard))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)tom_guard)))) {
Instruction tomMatch2_27=tom_get_slot_brcond_cond(((Instruction)tom_guard));
if (tom_is_sort_Inst(tomMatch2_27)) {
if (tom_is_fun_sym_sconstraintSys(((Instruction)tomMatch2_27))) {
org.polymodel.algebra.IntConstraintSystem tom_system=tom_get_slot_sconstraintSys_system(tomMatch2_27);

debug("\t\t- If/Else guard is an affine constraint systems");
affine=true;
if(
tom__else==null) { 
try {
ScopNode thenScopNode=scopDomainAnalyser.get((
tom__then));
ScopNode newNode= (ScopNode)(
tom_make_guard(tom_cons_array_ICSList(tom_system,tom_empty_array_ICSList(1)),thenScopNode,null));
scopDomainAnalyser.put((
tom_b),newNode);
} catch (SCoPException e) {
throw new SCoPException((
tom_b)," then branch is not a ScopNode ");
}
} else {
scopDomainAnalyser.put((
tom_b),(ScopNode)(
tom_make_guard(tom_cons_array_ICSList(tom_system,tom_empty_array_ICSList(1)),scopDomainAnalyser.get(tom__then),scopDomainAnalyser.get(tom__else))));
}
throw new SCoPException((
tom_b),"Should not happen !");

}
}
}
}
}
}
}

if(!affine) {
Instruction guard = (
tom_guard);  
debug("\t\t- If/Else guard is *NOT* affine "+(
tom_guard));
if(
tom__else==null) { 
try {
ScopNode thenScopNode=scopDomainAnalyser.get((
tom__then));


{
{
if (tom_is_sort_node(thenScopNode)) {
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode))) {
if (tom_is_fun_sym_stmt(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode)))) {

ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(
((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode)), guard);
scopDomainAnalyser.put((
tom_b), stmt);


}
}
}
}
{
if (tom_is_sort_node(thenScopNode)) {
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode))) {
if (tom_is_fun_sym_sblk(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode)))) {
 EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tomMatch3_6=tom_get_slot_sblk_children(((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode));
if (tom_is_fun_sym_nodes((( EList<fr.irisa.cairn.gecos.model.scop.ScopNode> )tomMatch3_6))) {

EList<ScopInstructionStatement> stmts = new BasicEList<ScopInstructionStatement>();

for (ScopNode c : (
tom_get_slice_nodes(tomMatch3_6,0,tom_get_size_nodes_nodes(tomMatch3_6)))) {

{
{
if (tom_is_sort_node(c)) {
boolean tomMatch4_6= false ;
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)c))) {
if (tom_is_fun_sym_stmt(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)c)))) {
if (tom_equal_term_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)c), ((fr.irisa.cairn.gecos.model.scop.ScopNode)c))) {
tomMatch4_6= true ;
}
}
}
if (!(tomMatch4_6)) {

throw new SCoPException((
tom_b),"Non affine If/Else guard "+(
tom_guard) +" combined with complex scop then branch");

}
}
}
{
if (tom_is_sort_node(c)) {
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)c))) {
if (tom_is_fun_sym_stmt(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)c)))) {

ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(
((fr.irisa.cairn.gecos.model.scop.ScopNode)c)), guard); 
stmts.add((ScopInstructionStatement)stmt.copy());   

}
}
}
}
}

}
ScopBlock block = (ScopBlock)(
tom_make_scopBlk((EList)stmts)); 
scopDomainAnalyser.put((
tom_b), block);

}
}
}
}
}
{
if (tom_is_sort_node(thenScopNode)) {
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode))) {
if (tom_is_fun_sym_sblk(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode)))) {
 EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tomMatch3_13=tom_get_slot_sblk_children(((fr.irisa.cairn.gecos.model.scop.ScopNode)thenScopNode));
if (tom_is_fun_sym_nodes((( EList<fr.irisa.cairn.gecos.model.scop.ScopNode> )tomMatch3_13))) {
int tomMatch3_17=0;
if (!(tomMatch3_17 >= tom_get_size_nodes_nodes(tomMatch3_13))) {
fr.irisa.cairn.gecos.model.scop.ScopNode tomMatch3_21=tom_get_element_nodes_nodes(tomMatch3_13,tomMatch3_17);
if (tom_is_sort_node(tomMatch3_21)) {
if (tom_is_fun_sym_stmt(((fr.irisa.cairn.gecos.model.scop.ScopNode)tomMatch3_21))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_stmt_children(tomMatch3_21)))) {
if (tomMatch3_17 + 1 >= tom_get_size_nodes_nodes(tomMatch3_13)) {


ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(
tom_get_element_nodes_nodes(tomMatch3_13,tomMatch3_17)), guard);
scopDomainAnalyser.put((
tom_b), stmt);

}
}
}
}
}
}
}
}
}
}
}

} catch (SCoPException e) {
throw new SCoPException((
tom_b),"Non affine If/Else guard "+(
tom_guard) +" combined with non scopable then branch");
}
} else {
throw new SCoPException((
tom_b)," If/Else guard in "+(
tom_b)+" is *NOT* affine "+(
tom_guard));
}

}

}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Blk(tom__arg)) {
if (tom_is_sort_Blk(((Block)tom__arg))) {
if (tom_is_fun_sym_BB(((Block)((Block)tom__arg)))) {
Block tom_b=((Block)tom__arg);

Block parent = (
tom_b).getParent();
boolean skip = false;

{
{
if (tom_is_sort_Blk(parent)) {
if (tom_is_sort_Blk(((Block)parent))) {
if (tom_is_fun_sym_ifThenElse(((Block)((Block)parent)))) {

if ((
tom_get_slot_ifThenElse_testBlock(((Block)parent)))==(
tom_b)) {
skip=true;
debug("\t\t- Skipping guard basic block "+(
tom_b)+" in if/else "+parent);
}

}
}
}
}
{
if (tom_is_sort_Blk(parent)) {
if (tom_is_sort_Blk(((Block)parent))) {
if (tom_is_fun_sym_forBlock(((Block)((Block)parent)))) {

if ((
tom_get_slot_forBlock_initBlock(((Block)parent)))==(
tom_b)) {
skip=true;
debug("\t- Skipping for-loop init in "+ parent);
}
if ((
tom_get_slot_forBlock_testBlock(((Block)parent)))==(
tom_b)) {
skip=true;
debug("\t- Skipping for-loop test in "+ parent);
}
if ((
tom_get_slot_forBlock_stepBlock(((Block)parent)))==(
tom_b)) {
skip=true;
debug("\t- Skipping for-loop step in "+ parent);
}

}
}
}
}
}


if(!skip) {

/**
Decorating Gecos Instruction as ScopInstructionStatement
**/
debug("\t- Analyzing BasicBlock "+(
tom_b));
List<Instruction> instrs = new BasicEList<Instruction>(
tom_get_slot_BB_instructions(((Block)tom__arg)));
ScopNode resNode = null;

EList<ScopInstructionStatement> stmts = new BasicEList<ScopInstructionStatement>();
for(Instruction i : instrs) {
ScopInstructionStatement node = analyzeInstruction(i);
stmts.add((ScopInstructionStatement)node.copy());
debug("\t\t- Analyzing instruction "+i.toString().replace('\n',' ')+" -> "+node.toString().replace('\n',' '));
}
ScopBlock block = (ScopBlock)(
tom_make_scopBlk((EList)stmts));
resNode = block;
debug("\t\t- Building ScopBlock "+block.toString().replace('\n',' '));

GecosUserAnnotationFactory.copyAnnotations((AnnotatedElement)
tom_b, (AnnotatedElement) resNode);
scopDomainAnalyser.put((
tom_b), resNode);
debug("\t\t- Added ScopBlock to map "+block.toString().replace('\n',' '));
}

}
}
}
}
{
if (tom_is_sort_Blk(tom__arg)) {
if (tom_is_sort_Blk(((Block)tom__arg))) {
if (tom_is_fun_sym_cblock(((Block)((Block)tom__arg)))) {
 EList<Block>  tomMatch1_37=tom_get_slot_cblock_children(((Block)tom__arg));
if (tom_is_fun_sym_BlkL((( EList<Block> )tomMatch1_37))) {
 EList<Block>  tom_children=tom_get_slice_BlkL(tomMatch1_37,0,tom_get_size_BlkL_BlkL(tomMatch1_37));
Block tom_b=((Block)tom__arg);

//processComposite((`b),(`children) )
debug("\t- Analyzing CompositeBlock "+(
tom_b));
if (
tom_children.size() > 1 || !(
tom_b).getScope().getSymbols().isEmpty()) {
EList<ScopNode> _nodes = new BasicEList<ScopNode>();
for(Block child : 
tom_children) {
debug("\t\tLooking for child "+child.toShortString() + " in map");
try {
ScopNode node = scopDomainAnalyser.get(child);
_nodes.add(node); 
} catch (SCoPException e) {
throw new SCoPException((
tom_b),"Composite block children without ScopNode mapping" );
}
//if (node instanceof ScopBlock && ((ScopBlock) node).getScope() != null && ((ScopBlock) node).getScope().getSymbols().size() == 0)
//	_nodes.addAll(((ScopBlock) node).getChildren());
//else
} 
ScopBlock gblk = (ScopBlock) (
tom_make_scopBlk(_nodes));
GecosUserAnnotationFactory.copyAnnotations(
tom_b,gblk);
scopDomainAnalyser.put((
tom_b),(ScopNode)gblk);
} else if (
tom_children.size() == 1) {
Block child = 
tom_children.get(0);
try {
ScopNode childScopNode = scopDomainAnalyser.get(child);
scopDomainAnalyser.put((
tom_b),childScopNode);
GecosUserAnnotationFactory.copyAnnotations(
tom_b,childScopNode);
} catch (SCoPException e) {
throw new SCoPException((
tom_b),"children "+child.getClass().getSimpleName()+" of "+(
tom_b)+" is not a Scop !") ;
}
}

}
}
}
}
}
}
return _visit_Blk(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_DomainAnalyzer(VariableContextManager t0, ScopDomainAnalyser t1) { 
return new DomainAnalyzer(t0,t1);
}



/*
private static EList<IntConstraintSystem> analyzeGuard(List<Instruction> systems) {
EList<IntConstraintSystem> res= new BasicEList<IntConstraintSystem>();
for(int i=0 ; i < systems.size(); i++) {
%match(systems.get(i)) {
!sconstraintSys(_) -> {
throw new SCoPException(systems.get(i).getBasicBlock(),("If/Else guard '"+systems+"' is not Scopable "));
}
sconstraintSys(_) -> {
res.add(((ScopIntConstraintSystem)systems.get(i)).getSystem());
}
}
}
return res;
}
private static void buildIf(Block ifBlock, Instruction guard, List<Instruction> systems, Block thenBlock, Block elseBlock, ScopDomainAnalyser scopDomainAnalyser) {
if (systems.size() > 0) {
ScopNode thenScopNode = null;
try {
thenScopNode = scopDomainAnalyser.get(thenBlock);
} catch (SCoPException e) {
throw new SCoPException(ifBlock," then statement "+thenBlock+" is not a Scop !") ;
}

ScopNode elseScopNode = null;
if(elseBlock != null) {
elseScopNode =scopDomainAnalyser.get(elseBlock);
}

try {
EList<IntConstraintSystem> predicate = analyzeGuard(systems);
ScopGuard newNode = (ScopGuard)(`guard(predicate,thenScopNode,elseScopNode));
GecosUserAnnotationFactory.copyAnnotations(ifBlock, newNode);
scopDomainAnalyser.put(ifBlock,newNode); 
} catch (SCoPException e) {
%match(thenScopNode) { 
s@stmt(id,InstL(children)) -> {
debug("Fixing "+(`s));
throw new SCoPException(ifBlock," data-dependant guard "+systems+" for "+(`s)) ;
}
scopBlk(nodes(s@stmt(id,InstL(children)))) -> {
debug("Fixing "+(`s));
throw new SCoPException(ifBlock," data-dependant guard "+systems+" for "+(`s)) ;
}
}
throw new SCoPException(ifBlock," guard predicate "+systems+" is not polyhedral") ;
}
}
}

private static void extractForInformation(
ScopDomainAnalyser scopDomainAnalyser, 
SimpleForBlock simpleForBlock, 
ScopDimension variable, 
IntExpression lowerBound, 
IntExpression upperBound, 
IntExpression step, 
Block body) {

if (variable==null) throw new SCoPException(simpleForBlock, "null variable "+variable);
if (lowerBound==null) throw new SCoPException(simpleForBlock,"null lower bound for "+simpleForBlock);
if (upperBound==null) throw new SCoPException(simpleForBlock,"null upper bound for "+simpleForBlock);
if (step==null) throw new SCoPException(simpleForBlock,"null stride for "+simpleForBlock); 

try {

ScopNode bodyAsScop= scopDomainAnalyser.get(body);
ScopForLoop  res= (ScopForLoop) (`loop(variable,lowerBound.copy(),upperBound.copy(),step.copy(),bodyAsScop));
GecosUserAnnotationFactory.copyAnnotations(simpleForBlock, res);
//res.getAnnotations().putAll(simpleForBlock.getAnnotations());
if (simpleForBlock.getScope() != null && !simpleForBlock.getScope().getSymbols().isEmpty()) {
EList<ScopNode> nodes = new BasicEList<ScopNode>();
nodes.add(res);
ScopBlock gblk = (ScopBlock) (`scopBlk(nodes));
//gblk.setScope(simpleForBlock.getScope());
scopDomainAnalyser.put(simpleForBlock,((ScopNode)gblk));
} else {
scopDomainAnalyser.put(simpleForBlock,((ScopNode)res));
}
} catch (SCoPException e) {
throw new SCoPException(simpleForBlock,"Loop body for "+simpleForBlock+" is not a Scop !") ;
}
}
*/
}
