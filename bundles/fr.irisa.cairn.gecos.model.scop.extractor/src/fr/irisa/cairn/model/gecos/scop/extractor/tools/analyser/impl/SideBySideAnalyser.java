/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopFilter;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

public class SideBySideAnalyser {

	private static final boolean VERBOSE = false;
	public static SideBySideAnalyser eInstance = new SideBySideAnalyser();
	private Map<Block, Boolean> blockAnalysed;
	private List<Block> transformedBlock;
	private ScopExtractorPass scopExtractorPass;

	private void debug(String mess) {
		if (VERBOSE) {
			System.out.println("["+this.getClass().getSimpleName()+"]"+mess);
		}
	}
	
	public void compute(Map<Block, GecosScopBlock> blockToScops, ScopExtractorPass scopExtractorPass) {
		if (blockToScops.size() > 0) {
			this.scopExtractorPass = scopExtractorPass;
			blockAnalysed = new HashMap<Block, Boolean>();
			transformedBlock = new ArrayList<Block>();
			for(Block block : blockToScops.keySet())
				blockAnalysed.put(block, false);
			for(Block block : blockToScops.keySet()) {
				if (!blockAnalysed.get(block))
					findSideBySideBlock(block, blockToScops);
			}
			for(Block block : transformedBlock) {
				blockToScops.remove(block);
			}
		}
	}

	private void findSideBySideBlock(Block block, Map<Block, GecosScopBlock> scopMap) {
		if (!(block.eContainer().eContainer() instanceof Procedure)) {
			Block p = block.getParent();
			if (p instanceof CompositeBlock) {
				CompositeBlock parent = (CompositeBlock)p;
				List<Block> scopable = new ArrayList<Block>();
				Map<Block, Integer> scopablePosition = new HashMap<Block, Integer>();
				scopable.add(block);
				EList<Block> children = parent.getChildren();
				int blockOffset = children.indexOf(block);
				scopablePosition.put(block, blockOffset);
				debug("Reference block has offset "+(blockOffset));
				
				ScopFilter f = new ScopFilter();
				debug("Traversing blocks ["+(blockOffset + 1)+":"+children.size()+"] following "+block);
				for (int i = blockOffset + 1; i < children.size(); i++) {
					try {
						boolean ok = false;
						Block child = children.get(i);
						if (child instanceof BasicBlock) {
							ok = f.doSwitch(child);
						} else 
							ok = (scopMap.keySet().contains(child));
						if (ok) {
							scopable.add(child);
							scopablePosition.put(child, i);
							debug("- Successor block "+child+" scopable at position "+i);
						} else break;
					} catch (SCoPException e) {
						e.printStackTrace();
						break;
					}
				}
				debug("Traversing blocks ["+0+":"+(blockOffset-1)+"] preceeding "+block);
				for (int i = blockOffset-1; i >= 0; i--) {
					try {
						boolean ok = false;
						Block child = children.get(i);
						if (child instanceof BasicBlock) {
							ok = f.doSwitch(child);
						} else 
							ok = (scopMap.keySet().contains(child));
						if (ok) {
							scopable.add(0,child);
							scopablePosition.put(child, i);
							debug("- Predecessor block "+child+" scopable at position "+i);
						} else break;
					} catch (SCoPException e) {
						break;
					}
				}
				if (scopable.size() > 1) {
					debug("More than one scop in current block, check for merging opportunities");

					block = scopable.get(0);
					CompositeBlock compositeBlock = GecosUserBlockFactory.CompositeBlock();
					parent.addChildren(compositeBlock);
					int position = blockOffset;
					
					EList<Block> mergeChildren = compositeBlock.getChildren();
					mergeChildren.addAll(scopable);
	
					ScopExtractorPass scopExtractorPass = new ScopExtractorPass();
					Block candidate = null;
					while (candidate == null && mergeChildren.size() > 1) {
						try {
							candidate = scopExtractorPass.buildSCoP(compositeBlock);
						} catch(SCoPException e) {
							
							Block last = mergeChildren.get(mergeChildren.size()-1);
							children.add(scopablePosition.get(last) - (mergeChildren.size()-1), last);
							debug("- Removing "+last+" from merge set and put it back in "+parent);
							compositeBlock.removeBlock(last);
							scopable.remove(last);
						}
					}
					if (candidate == null) {
						debug("- No candidates ");
						children.add(scopablePosition.get(block), block);
						parent.removeBlock(compositeBlock);
					} else {
						debug("- Merged candidate ");
						// restore initial composite structure
						children.addAll(scopablePosition.get(block), scopable);
						// children.add(position, candidate);
						parent.removeBlock(compositeBlock);
						for (Block b : scopable) {
							transformedBlock.add(b);
							blockAnalysed.put(b, true);
							this.scopExtractorPass.getMultipleStatementScopMap().put(b, (GecosScopBlock) candidate);
						}
					}
				}
			}
		}
	}
}