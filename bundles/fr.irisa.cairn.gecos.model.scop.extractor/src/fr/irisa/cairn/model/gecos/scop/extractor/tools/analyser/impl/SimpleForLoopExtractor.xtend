package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl

import fr.irisa.cairn.gecos.model.analysis.forloops.ModelNormalForInformation
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.blocks.ForBlock
import gecos.blocks.SimpleForBlock
import gecos.gecosproject.GecosProject
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory

class SimpleForLoopExtractor {
	
	GecosProject p;
	
	new (GecosProject p) {
		this.p=p;
	}
	
	def compute () {
		val loops = EMFUtils.eAllContentsFirstInstancesOf(p,ForBlock)
		for (l:loops) {
			var SimpleForBlock sfor = null
			try {
				val analyzer = new ModelNormalForInformation(l)
				val lb = analyzer.startValue
				val step= analyzer.stepValue
				val ub =analyzer.stopValue
				val iterator = analyzer.iterationIndex
				if (lb!==null && ub!==null && iterator!==null && step!==null) {
					print("Replacing "+l)
					sfor = GecosUserBlockFactory.SimpleFor(l,iterator, lb,ub, step, GecosUserCoreFactory.scope);
					sfor.annotations+=l.annotations
					println(" by "+sfor)
					l.parent.replace(l,sfor)
				}
			} catch (NullPointerException e) {
				println("not simple for block")								
			}
		}
	}
}