/** 
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 * Contributors:
 * DERRIEN Steven - initial API and implementation
 * MORVAN Antoine - initial API and implementation
 * NAULLET Maxime - initial API and implementation
 */
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl

import java.util.Collection
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Map.Entry
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopParameter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass
import gecos.core.Symbol

class VariableContextManager {
	public static boolean VERBOSE = ScopExtractorPass.VERBOSE
	Map<Symbol, ScopDimension> varMap
	Map<Symbol, ScopParameter> parMap
	Map<Integer, ScopDimension> index
	Map<Symbol, Boolean> visible

	def debug(String mess) {
		if (VERBOSE) print("[VariableContextManager] "+mess)		
	}

	new() {
		varMap = new HashMap<Symbol, ScopDimension>()
		parMap = new HashMap<Symbol, ScopParameter>()
		index = new HashMap<Integer, ScopDimension>()
		// existentialMap = new HashMap<Symbol, GScopParameter>();
		visible = new HashMap<Symbol, Boolean>() // dimensionsManager = GScopFactory.eINSTANCE.createGScopDimensionsManager();
	}

	// public void addIterator(Symbol s) {
	// if (varMap.containsKey(s)) {
	// throw new RuntimeException("Error");
	// } else {
	// LoopIterator i = GScopFactory.eINSTANCE.createLoopIterator();
	// i.setSymbol(s);
	// i.setName(s.getName());
	// varMap.put(s, i);
	// }
	// }
	def void activateIterators(List<Symbol> ls, int dimension) {
		for (Symbol s : varMap.keySet()) {
			setVisibility(s, false)
		}
		for (Symbol s : ls) {
			if (!varMap.containsKey(s)) {
				var ScopDimension i = ScopUserFactory.dimSym(s)
				index.put(dimension, i)
				varMap.put(s, i)
			}
			setVisibility(s, true)
		}
	}

	def void removeIterator(Symbol iterator) {
		visible.remove(iterator)
	}

	def void removeIterators(List<Symbol> iterators) {
		for (Symbol s : iterators) {
			removeIterator(s)
		}
	}

	def void addParameters(List<Symbol> symbols) {
		for (Symbol symbol : symbols) {
			if (parMap.containsKey(symbol)) {
				throw new RuntimeException("Error")
			} else {
				if (symbol !== null) {
					var ScopParameter gscopParameter = ScopUserFactory.param(symbol)
					parMap.put(symbol, gscopParameter)
				} else {
					throw new RuntimeException("Something really went wrong here")
				}
			}
		}
	}

	static package int existentialId = 0

	def void setVisibility(Symbol s, boolean b) {
		visible.put(s, b)
	}

	def void makeAllSymbolVisible() {
		for (Entry<Symbol, Boolean> entry : visible.entrySet()) {
			entry.setValue(true)
		}
	}

	def ScopDimension lookUp(Symbol a) {
		if (varMap.containsKey(a)) {
			if (visible.get(a)) {
				return varMap.get(a)
			} else {
				if (VERBOSE)
					System.out.println('''-Symbol «a» is not an iterator at this program level''')
			}
		} else if (parMap.containsKey(a)) {
			return parMap.get(a)
		}
		return null
	}

	def Collection<ScopDimension> getAllIterators() {
		return varMap.values()
	}

	def Collection<ScopParameter> getAllParameters() {
		return parMap.values()
	} // public ScopDimension getDimension(int dimension) {
	// return index.get(dimension);
	// }
	// public ScopDimension search(int dimension, Symbol iterator) {
	// index
	// }
}
