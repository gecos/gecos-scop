package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractBlockTransformation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager;
import gecos.blocks.Block;
import gecos.blocks.SimpleForBlock;

abstract public class AbstractScopDomainAnalyser extends AbstractBlockTransformation{

	protected static final boolean VERBOSE = false;// && ScopExtractorPass.VERBOSE;
	
	public static void debug(String mess) {
		if(VERBOSE) {
			System.out.println(mess);
			//ScriptLog.getInstance().printText(mess);
		}
	}

	protected static Block rootBlock;

	protected static VariableContextManager context;
	protected Map<Block,ScopNode> map;
	
	public AbstractScopDomainAnalyser(VariableContextManager context) {
		this.context=context;
		map = new HashMap<Block,ScopNode>();
	}
  
	protected ScopNode get(Block blk) {
		if(map.containsKey(blk)) {
			return map.get(blk);
		} else {
			throw new SCoPException(blk,"Block "+blk+" has no SCoP counterpart in map ");
		}
	} 

	protected void put(Block blk,ScopNode node) {
		if (blk != null && node != null) {
			debug("\t\t- Adding "+blk.getClass().getSimpleName()+":"+blk+"  in Scop map");
			map.put(blk, node);
		}
	}
	/* FIXME : maxime */
	protected static int findDimension(Block block) {
		if (rootBlock.equals(block)) {
			if (block instanceof SimpleForBlock) {
				return 0;
			} else {
				return -1;
			}
		} else {
			if (block instanceof SimpleForBlock) {
				return findDimension(block.getParent())+1;
			} else {
				return findDimension(block.getParent());
			}
		}
	}
	

}
