/** 
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 * Contributors:
 * DERRIEN Steven - initial API and implementation
 * MORVAN Antoine - initial API and implementation
 * NAULLET Maxime - initial API and implementation
 */
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom

import java.util.List
import org.eclipse.emf.common.util.BasicEList

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException
import gecos.blocks.BasicBlock
import gecos.blocks.Block
import gecos.blocks.ForBlock
import gecos.blocks.SimpleForBlock
import gecos.core.Symbol
import gecos.instrs.ArrayInstruction
import gecos.instrs.Instruction
import gecos.instrs.SymbolInstruction
import tom.library.sl.VisitFailure
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.LoopCounterAnalyzer
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager

@SuppressWarnings(#["all"]) 

abstract class BaseLinearizer extends BlocksDefaultSwitch {



	protected VariableContextManager context
	protected LoopCounterAnalyzer loopCounterAnalyzer
	protected List<Symbol> parameters
	protected List<Symbol> iterators
	protected int dimension
	//package boolean isRoot = false
	protected static final boolean VERBOSE = true	// &&  ScopExtractorPass.VERBOSE;

	protected static  boolean touched = false

	def protected static void debug(String mess) {
		if (VERBOSE) {
			System::out.println('''[Linearizer] «mess»'''.toString)
		}
	}

	new() {
		dimension = -1
	}

	def public VariableContextManager transform(Block b) {
		var fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ParametersAnalyzer paramAnalyzer = new fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ParametersAnalyzer()
		parameters = paramAnalyzer.findParametersIn(b)
		debug('''Linearizing expressions in block «b» with parameters «parameters»'''.toString)
		iterators = new BasicEList<Symbol>()
		loopCounterAnalyzer = new fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.LoopCounterAnalyzer(b)
		context = new fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager()
		context.addParameters(parameters)
		doSwitch(b)
		return context
	}

	override Object caseBasicBlock(BasicBlock b) {
		for (var int i = 0; i < b.getInstructionCount(); i++) {
			var Instruction orig = b.getInstruction(i)
			var Instruction trans
			try {
				debug('''Linearizing «orig»'''.toString)
				touched = false
				var Instruction copyAnnotatedElement = ScopExtractorPass::copyAnnotatedElement(orig)
				trans = apply(copyAnnotatedElement)
				if (touched) {
					// FIXME : should add a compare method between instruction
					// if(!trans.toString().equals(orig.toString())) {
					debug('''Replacing «orig» by «trans»'''.toString)
					orig.substituteWith(trans)
				}
			} catch (VisitFailure e) {
				e.printStackTrace()
				throw new RuntimeException('''Couln't linearize «orig» : «e.getLocalizedMessage()»'''.toString)
			}

		}
		return true
	}

	override Object caseForBlock(ForBlock b) {
		try {
			iterators = loopCounterAnalyzer.findSurroundingLoopCounters(b)
			debug('''Entering forloop «b» iterators = «iterators»'''.toString)
			context.activateIterators(iterators, dimension)
			super.caseForBlock(b)
			if(b instanceof SimpleForBlock) context.removeIterator(((b as SimpleForBlock)).getIterator())
			return true
		} catch (VisitFailure e) {
			e.printStackTrace()
			throw new RuntimeException('''Couln't find iterators in «b» : «e.getLocalizedMessage()»'''.toString)
		}

	}

	override Object caseSimpleForBlock(SimpleForBlock b) {
		dimension++
		var Object object = caseForBlock(b)
		try {
			b.setLb(apply(b.getLb().copy()))
			b.setUb(apply(b.getUb().copy()))
			b.setStride(apply(b.getStride().copy()))
		} catch (VisitFailure e) {
			e.printStackTrace()
			throw new RuntimeException('''Couln't linearize  : «e.getLocalizedMessage()»'''.toString)
		}

		dimension--
		return object
	}

	def Object caseBlock(Block b) {
		// debug("Visiting "+b+" :"+b.getClass().getSimpleName());
		return true
	}

	def protected static void checkNoNull(Object expr) {
		if(expr === null) throw new NullPointerException();
	}

	def protected Instruction apply(Instruction instruction) throws VisitFailure {
		try {
			debug(
				'''Linearizing «instruction.getClass().getSimpleName()»:«instruction» using «iterators» and «parameters»'''.
					toString)
			var Instruction res = tom_rewrite(instruction);
			debug('''-> «res.getClass().getSimpleName()»:«res»'''.toString)
			checkNoNull(res)
			return res
		} catch (RuntimeException e) {
			e.printStackTrace()
			throw new SCoPException(instruction.getBasicBlock(),
				'''«instruction» is not Scopable due to runtime exception «e.getMessage()»'''.toString)
		}

	}
	
	def protected Instruction tom_rewrite(Instruction instruction) 
	

	def protected static boolean isScalarSymbol(Instruction s) {
		if (s instanceof SymbolInstruction) {
			if (s.getParent() instanceof ArrayInstruction) {
				var ArrayInstruction array = (s.getParent() as ArrayInstruction)
				if (array.getDest() === s) {
					return false
				}
			}
			return true
		}
		return false
	}
}
