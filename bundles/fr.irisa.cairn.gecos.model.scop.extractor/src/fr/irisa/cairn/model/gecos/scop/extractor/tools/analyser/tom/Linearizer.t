/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;
import org.polymodel.algebra.tom.ArithmeticOperations;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.BaseLinearizer;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
  
@SuppressWarnings({"all"})
public class Linearizer extends BaseLinearizer {


 	%include { sl.tom }  
	%include { List.tom }
	%include { algebra_common.tom }
	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }    
  
	%include { algebra_quasiaffine.tom }
	%include { algebra_domains.tom }   
	%include { gecos_terminals.tom } 
	%include { scop_terminals.tom } 
	%include { extractor_extraction.tom } 
 
	%include { gecos_basic.tom }
	%include { scop_nodes.tom }
 

	static final boolean TOM_VERBOSE = true	; // &&  ScopExtractorPass.VERBOSE;

	private static void traceTom(String mess) {
		if(TOM_VERBOSE) {
			System.out.println("[Linearizer/Tom]\t"+mess);
		}
	}


	protected Instruction tom_rewrite(Instruction instruction) throws SCoPException {
		try {
			debug("Linearizing "+instruction.getClass().getSimpleName()+":"+instruction+" using "+iterators+" and "+parameters);
			Instruction res = `InnermostId(BuildLinearExpression(context)).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
			debug("-> "+res.getClass().getSimpleName()+":"+res);
			checkNoNull(res);
			return res;
		} catch (Exception e) {  
			e.printStackTrace();
			throw new SCoPException(instruction.getBasicBlock(), instruction + " is not Scopable due to runtime exception "+e.getMessage());
		}
	}

	%typeterm Context {
		implement 		{VariableContextManager}
		is_sort(t) 		{$t instanceof VariableContextManager}
		equals(l1,l2) 	{$l1==$l2}
	}
	
	%strategy BuildLinearExpression(c : Context) extends Identity() {
		visit Inst {
			s@generic("mul",InstL(symref(a),ival(b))) -> {
				touched=true;
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 1 (ignored since instruction is used to define a type) ");
					return `s;
				}
				// s@imul(InstL(symref(a),ival(b)))
				ScopDimension v = `c.lookUp(`a);
				if(v!=null) {
					Instruction res= `sexpr(affine(terms(term(b,v))));
					res.setType((`s).getType());
					traceTom("Rule 1 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					traceTom("Rule 1 "+(`s)+" => "+(`s));
					return `s;  
				} 
			}
			s@generic("mul",InstL(ival(b),symref(a))) -> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 2 (ignored since instruction is used to define a type) ");
					return `s;
				}
				// s@imul(InstL(symref(a),ival(b)))
				ScopDimension v = `c.lookUp(`a);
				if(v!=null) {
					touched=true;
					Instruction res= `sexpr(affine(terms(term(b,v))));
					res.setType((`s).getType());
					traceTom("Rule 2 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					return `s;  
				}  
			}
	
			s@generic("shr",InstL(symref(a),ival(b))) -> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 3 (ignored since instruction is used to define a type) ");
					return `s;
				}
				// s@shr(InstL(symref(a),ival(b))) 
				ScopDimension v = `c.lookUp(`a);
				if(v!=null) {
					touched=true;
					Instruction res= `sexpr(affine(terms(term(1<<b,v))));
					res.setType((`s).getType());
					traceTom("Rule 3 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					return `s;
				} 
			}

			s@generic("shl",InstL(symref(a),ival(b))) -> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 4 (ignored since instruction is used to define a type) ");
					return `s;
				}
				// s@shl(InstL(symref(a),ival(b)))
				ScopDimension v = `c.lookUp(`a);
				if(v!=null) {
					touched=true;
					Instruction res= `sexpr(qaffine(terms(floor(
							affine(terms(term(1L,v))),((long)1<<b)))));
					res.setType((`s).getType());
					traceTom("Rule 4 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					return `s;
				}
			}

			s@symref(a) -> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 5 (ignored since instruction is used to define a type) ");
					return `s;
				}
				ScopDimension v = `c.lookUp(`a);
/*				
				if((`v).getSymbol().eContainer()==null) {
					///debug("Houston, we have a problem with "+(`v)+ " symbol-> "+ (`v).getSymbol());
				}
*/				
				if(v!=null) {
					touched=true;
					Instruction res= `sexpr(affine(terms(term(1L,v))));
					res.setType((`s).getType());
					traceTom("Rule 5 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					traceTom("Rule 5 (ignored since "+(`a)+" is neither a parameter or an iterator) ");
					return `s;
				} 
			}

			conv@convert(s@symref(a))-> {
				EObject container = `conv.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 6 (ignored since instruction is used to define a type) ");
					return `conv;
				}
				ScopDimension v = `c.lookUp(`a);
				if(v!=null) {
					touched=true;
					Instruction res= `sexpr(affine(terms(term(1L,v))));
					res.setType((`s).getType());
					traceTom("Rule 6 "+(`s)+" => "+res);
					res.getAnnotations().putAll(`s.getAnnotations());
					return res;
				} else {
					traceTom("Rule 6 (ignored since "+(`a)+" is neither a parameter or an iterator) ");
					return `s;
				} 
			}

			valueInstr@ival(a) ->{
				EObject container = `valueInstr.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 6b (ignored since instruction "+(`valueInstr)+" is used within type/symbol definition "+container+") ");
					return `valueInstr;
				}
				touched=true;
				Instruction instr = `sexpr(affine(terms(constant(a))));
				instr.setType((`valueInstr).getType());
				instr.getAnnotations().putAll(`valueInstr.getAnnotations());
				return instr;
			}

			in@generic("add",InstL(sexpr(a),sexpr(b)))-> {
				EObject container = `in.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 7 (ignored since instruction is used to define a type/symbol) ");
					return `in;
				}
				IntExpression expr = ArithmeticOperations.add(`a,`b).simplify();
				Instruction res = `sexpr(expr);
				res.setType((`in).getType());
				checkNoNull(res);  
				traceTom("Rule 7 : "+(`in)+"=>"+res);
				res.getAnnotations().putAll(`in.getAnnotations());
				touched=true;
				return res;
			}

			in@generic("sub",InstL(sexpr(a),sexpr(b)))-> {
				EObject container = `in.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 8 (ignored since instruction is used to define a type/symbol) ");
					return `in;
				}
				touched=true;
				IntExpression expr = ArithmeticOperations.sub((`a),(`b)).simplify();
				checkNoNull(expr);
				Instruction res = `sexpr(expr); 
				res.setType((`in).getType());
				traceTom("Rule 8 : "+(`in)+"=>"+res);
				res.getAnnotations().putAll(`in.getAnnotations());
				return res;
			}
			
			s@generic("mul",InstL(sexpr(affine(terms(constant(a)))),sexpr(b)))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 9 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(ArithmeticOperations.scale(b,a));
				res.setType((`s).getType());
				traceTom("Rule 9 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}
			
			s@generic("mul",InstL(sexpr(b),sexpr(affine(terms(constant(a))))))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 10 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(ArithmeticOperations.scale(b,a));
				res.setType((`s).getType());
				traceTom("Rule 10 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}

			s@generic("mul",InstL(sexpr(a),ival(c)))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 11 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(ArithmeticOperations.scale(a,c));
				res.setType((`s).getType());
				traceTom("Rule 11 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}
			
			s@generic("mul",InstL(ival(c), sexpr(a)))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 12 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(ArithmeticOperations.scale(a,c));
				res.setType((`s).getType());
				traceTom("Rule 12 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}
			 
			s@generic("div",InstL(sexpr(b),sexpr(affine(terms(constant(a))))))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 13 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(qaffine(terms(div(b,a))));
				res.setType((`s).getType());
				traceTom("Rule 13 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}  
			
			s@generic("mod",InstL(sexpr(b),sexpr(affine(terms(constant(a))))))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 13.1 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(qaffine(terms(mod(b,a))));
				res.setType((`s).getType());
				traceTom("Rule 13.1 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}
			
			s@generic("neg",InstL(sexpr(affine(terms(term(a,b))))))-> {
				EObject container = `s.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 14 (ignored since instruction is used to define a type) ");
					return `s;
				}
				touched=true;
				Instruction res= `sexpr(affine(terms(term(a * (-1), b))));
				res.setType((`s).getType());
				traceTom("Rule 14 "+(`s)+" => "+res);
				res.getAnnotations().putAll(`s.getAnnotations());
				return res;
			}
			

			op@generic("lt",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 15 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(lt(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("gt",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 16 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(gt(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("le",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 17 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(le(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("ge",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 18 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(ge(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("eq",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 19 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(eq(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("ne",InstL(sexpr(a),sexpr(b))) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 20 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res = `sconstraintSys(system(CL(ne(a,b))));
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}


			op@generic("boolconjunction", 
					InstL(	_*, 
							sconstraintSys(system(CL(a))),
							_*,
							sconstraintSys(system(CL(b))),
							_*)
					) -> {  
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 21 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res= `generic("boolconjunction",InstL(sconstraintSys(system(CL(a,b)))));
				res.setType((`op).getType());
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
			op@generic("land",
					InstL(	_*,
							sconstraintSys(system(CL(a*))),
							_*,
							sconstraintSys(system(CL(b*))),
							_*)
					) -> {
				EObject container = `op.getRoot().eContainer();
				if (container instanceof Type || container instanceof Symbol) {
					traceTom("Rule 22 (ignored since instruction is used to define a type) ");
					return `op;
				}
				touched=true;
				Instruction res= `sconstraintSys(system(CL(a*,b*)));
				res.setType((`op).getType());
				res.getAnnotations().putAll(`op.getAnnotations());
				return res;
			}
/*
			b@sarray(InstL(symref(_array),indices*)) -> {
				boolean notAffine=false;
				List<IntExpression> exprs =  new BasicEList<IntExpression>();
				for(Instruction instr : `indices) {
					if(instr instanceof GScopIntExpressionInstr) {
						exprs.add(((GScopIntExpressionInstr)instr).getExpression());
						notAffine=true;
						break;
					}
				}	
				if (!notAffine) {
					// FIXME : need to check if it is a read or a write 
					debug("Found affine array access :"+(`b));
					return `b;
				} else {
					return `b;
				}
			}
*/			

		} 
	}  
  


}