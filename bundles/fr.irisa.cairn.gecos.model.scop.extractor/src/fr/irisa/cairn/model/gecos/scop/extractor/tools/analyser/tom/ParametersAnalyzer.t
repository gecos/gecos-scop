/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.blocks.util.BlocksSwitch;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ParametersAnalyzer{

	%include { sl.tom }
	%include { List.tom }
	

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_control.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }


 
	private EList<Symbol> parameters;
	private EList<Symbol> variables;   
	private static boolean VERBOSE= ScopExtractorPass.VERBOSE;

	private static void debug(String mess) {
		if(VERBOSE) {
			System.out.println("[ParametersAnalyzer] "+mess);
		}
	}

	public EList<Symbol> findParametersIn(Block block) {
		parameters = new BasicEList<Symbol>();
		variables = new BasicEList<Symbol>();
		Traversal traversal = new Traversal();
		traversal.doSwitch(block);
		ScopeTraversal scopeTraversal = new ScopeTraversal();
		scopeTraversal.doSwitch(block);
		debug("Parameters "+parameters);
		parameters.removeAll(variables);
		debug("Parameters "+parameters);
		return parameters; 
	} 

	protected void updateParameters(Instruction instruction) throws VisitFailure {
		`InnermostId(AnalyseParameters(parameters,variables)).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
	}
	
	private class Traversal extends BlockInstructionSwitch<Object>{
		@Override
		public Object caseInstruction(Instruction object) {
			try {
				updateParameters(object);
			} catch (VisitFailure e) {
			}
			return new Object();
		}
	}
	
	private class ScopeTraversal extends BlocksSwitch<Object> {
		
		@Override
		public Object caseScopeContainer(ScopeContainer scopeContainer) {
			for (Symbol symbol : scopeContainer.getScope().getSymbols()) {
				variables.add(symbol);
				if(parameters.contains(symbol)) {
					debug("Discarding potential parameter +"+symbol+" as its scope is local : "+scopeContainer);
				}
			}
			return new Object();
		}
		
		@Override
		public Object caseCompositeBlock(CompositeBlock compositeBlock) {
			for (Block block : compositeBlock.getChildren())
				doSwitch(block);
			return null;
		}

		@Override
		public Object caseForBlock(ForBlock forBlock) {
			Block body = forBlock.getBodyBlock();
			if (body != null)
				doSwitch(body);
			return super.caseForBlock(forBlock);
		}
		
		@Override
		public Object caseForC99Block(ForC99Block forC99Block) {
			Block body = forC99Block.getBodyBlock();
			if (body != null)
				doSwitch(body);
			return super.caseForC99Block(forC99Block);
		}
		
		@Override
		public Object caseIfBlock(IfBlock ifBlock) {
			doSwitch(ifBlock.getThenBlock());
			if (ifBlock.getElseBlock() != null)
				doSwitch(ifBlock.getElseBlock());
			return super.caseIfBlock(ifBlock);
		}
		
		@Override
		public Object caseDoWhileBlock(DoWhileBlock loopBlock) {
			doSwitch(loopBlock.getBodyBlock());
			return super.caseDoWhileBlock(loopBlock);
		}
		
		@Override
		public Object caseSimpleForBlock(SimpleForBlock simpleForBlock) {
			Block body = simpleForBlock.getBodyBlock();
			if (body != null)
				doSwitch(body);
			return super.caseSimpleForBlock(simpleForBlock);
		}
		
		@Override
		public Object caseWhileBlock(WhileBlock whileBlock) {
			doSwitch(whileBlock.getBodyBlock());
			return super.caseWhileBlock(whileBlock);
		}
		
		@Override
		public Object caseSwitchBlock(SwitchBlock switchBlock) {
			doSwitch(switchBlock.getBodyBlock());
			return super.caseSwitchBlock(switchBlock);
		}
	}
	
	%strategy AnalyseParameters(params:SymL,vars:SymL) extends Identity() {
		visit Inst {			
			s@symref(a) -> {
				if(`a.getType().asBase() != null) {
					if(!params.contains(`a)) {
						params.add(`a);
						debug("Found potential parameter : "+`a+ " in "+`s.getRoot());
						debug("Parameters = "+params);
						debug("Variables = "+vars);
					}
				}
			}
			s@set(symref(a),_) -> {
				vars.add(`a);
				if(params.contains(`a)) {
					debug("Discarding potential parameter +"+`a+" as it is involved in assignment : "+`s);
						debug("Parameters = "+params);
						debug("Variables = "+vars);
				}
			}
			
			c@fcall(_,InstL(_)) -> {
				CallInstruction callInst = (CallInstruction) `c;
				List<Instruction> args = callInst.getArgs();
				for(Instruction arg: args){ 
					if(arg instanceof AddressInstruction){
						Instruction scalarInst = ((AddressInstruction)arg).getAddress();
						Symbol symAddr = null;
						if(scalarInst instanceof SymbolInstruction){
							symAddr = ((SymbolInstruction)scalarInst).getSymbol();
							if(params.contains(symAddr)) {
								debug("Discarding potential parameter +"+symAddr+" as it is involved in assignment : "+`c);
							}
							vars.add(symAddr);
							params.remove(symAddr);
						}
					}
				}				
			}
		} 
	}

}