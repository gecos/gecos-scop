/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;
  
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.*;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ScopAccessAnalyzer extends AbstractInstructionTransformation{
	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }

	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }
	%include { algebra_quasiaffine.tom }
	//%include { algebra_domains_ops.tom }
	%include { scop_terminals.tom } 
	  
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_control.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }

	%include { gecos_basic.tom } 
	%include { extractor_extraction.tom } 
	%include { scop_nodes.tom }   

	public ScopAccessAnalyzer() {
	}

	private static final boolean VERBOSE = true  & ScopExtractorPass.VERBOSE;

	private static void debug(String mess) {
		if(VERBOSE) {
			System.out.println(mess);
		}
	}
	
	@Override
	protected Instruction apply(Instruction i) {
		try {
			debug("Analyzing "+i+ " for Read/Write SCOP access");
			Instruction res = `InnermostId(AnalyseStatements()).visitLight(i, tom.mapping.GenericIntrospector.INSTANCE);
			debug("\t-Transformed into "+res.toString().replace("\n"," ")+ "\n");
			return res;
		} catch (VisitFailure e){
			throw new SCoPException(i.getBasicBlock(),"Visit failure "+e.getMessage());
		}
	}
	
	private static ScopWrite buildScopWriteAccess(SimpleArrayInstruction array) {
		EList<IntExpression>  expList = new BasicEList<IntExpression>();
		for(Instruction index : array.getIndex()) {
			if(index instanceof ScopIntExpression) {
				expList.add(((ScopIntExpression) index).getExpr());
			} else {
				throw new SCoPException(array.getBasicBlock(),"Non affine array access in "+array);
			}
		}
		ScopWrite swrite = (ScopWrite)(`iwrite(array.getSymbol(),expList));
		swrite.getAnnotations().putAll(array.getAnnotations());
		return swrite;
	}

	private static ScopRead buildScopReadAccess(SimpleArrayInstruction array) {
		EList<IntExpression>  expList = new BasicEList<IntExpression>();
		for(Instruction index : array.getIndex()) {
			if(index instanceof ScopIntExpression) {
				expList.add(((ScopIntExpression) index).getExpr());
			} else {
				throw new SCoPException(array.getBasicBlock(),"Non affine array access in "+array);
			}
		}
		ScopRead sread = (ScopRead)(`iread(array.getSymbol(),expList));
		sread.getAnnotations().putAll(array.getAnnotations());
		return sread;
	}
	
	private static ScopWrite buildScopWriteAccess(SymbolInstruction array) {
		EList<IntExpression>  expList = new BasicEList<IntExpression>();
		ScopWrite write = (ScopWrite)(`iwrite(array.getSymbol(),expList));
		write.getAnnotations().putAll(array.getAnnotations());
		return write;
	}
	
	private static ScopRead buildScopReadAccess(SymbolInstruction array) {
		EList<IntExpression>  expList = new BasicEList<IntExpression>();
		ScopRead read = (ScopRead)(`iread(array.getSymbol(),expList));
		read.getAnnotations().putAll(array.getAnnotations());
		return read;
	}

	%strategy AnalyseStatements() extends Identity() {
		visit Inst{

			// Array write operation  
			// and use the rule n°1 above to convert flase read into actual write operations
			statement@set(a@sarray(_,InstL(_*)),lhs) -> {
				Instruction stmt= `statement;
				//if ((`lhs) instanceof ScopIntExpression) {
					debug("\t\tValue to set : " + `lhs);
					debug("\t-Rule 1 : Array write operation "+(`a)+ "found in "+(`statement).getRoot());
					Instruction symbol = `buildScopWriteAccess((SimpleArrayInstruction)a);
					symbol.setType((`a).getType());
					Instruction setInstruction = `set(symbol, lhs); 
					setInstruction.setType(`statement.getType());
					String label= "S"; 
					ScopInstructionStatement res = (ScopInstructionStatement) (`stmt(label,InstL(setInstruction)));
					res.setType(`statement.getType()); 
					res.getAnnotations().putAll(`statement.getAnnotations());
					return res;
				/*} else {
					throw new SCoPException(stmt.getBasicBlock(),"Right hand side of the set is not a GScopExpression");
				}*/
			}

			// Implicit array write operation expressed as a function argument 
			statement@address(a@sarray(_,InstL(_*))) -> {
				Instruction stmt= `statement;
				if(isCallWriteArgument(`a)) {
					debug("\t-Rule 2 : Implicit array write operation "+(`a)+ " found  in call "+(`a).getRoot());
					Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)`a);
					symbol.setType((`a).getType());
					Instruction address = `address(symbol);
					address.setType(stmt.getType());
					return address;
				} else {
					throw new SCoPException(stmt.getBasicBlock(),"Illegal array pattern "+stmt.getRoot());
				}
			}

			// Array read operation 
			statement@sarray(_,InstL(_*)) -> {
				if(!(isWrite(`statement) || isCallWriteArgument(`statement))){
					debug("\t-Rule 3 : array read operation "+(`statement)+ " found  in "+(`statement).getRoot());
					Instruction symbol = buildScopReadAccess((SimpleArrayInstruction)`statement);
					symbol.setType((`statement).getType());
					return symbol;
				}
			}
 
			/* Scalar write operation */
			statement@set(sym@symref(var),lhs) -> {
				Symbol s = (`var);
				Type t = s.getType();
				
				// look for the base type when using aliases
				while (t instanceof gecos.types.AliasType)
					t = ((gecos.types.AliasType)t).getAlias();

				if(t instanceof gecos.types.BaseType) {
					debug("\t-Rule 4 : scalar write operation "+(`statement)+ " found  in "+(`statement).getRoot());
					Instruction symbol = `buildScopWriteAccess((SymbolInstruction)sym);
					symbol.setType((`sym).getType());
					Instruction setInstruction = `set(symbol, lhs);
					setInstruction.setType(`statement.getType());
					ScopInstructionStatement res = (ScopInstructionStatement) (`stmt("S",InstL(setInstruction)));
					res.setType(setInstruction.getType());
					res.getAnnotations().putAll(`statement.getAnnotations());
					return res; 
				} else {
					throw new SCoPException(`statement.getBasicBlock(),"Left hand side of the set has not a BaseType");
				}
			}	
			
			//Scalar reference
			statement@symref(var)-> {
				Symbol s = (`var);
				Type t = s.getType();
				// look for the base type when using aliases
				while (t instanceof gecos.types.AliasType)
					t = ((gecos.types.AliasType)t).getAlias();
				if(t instanceof gecos.types.BaseType) {
					//ScopWrite s = scalar(`var,`statement);
					if(!isWrite(`statement)){
						debug("\t-Rule 5 : scalar read operation "+(`statement)+ " found  in "+(`statement).getRoot());
						Instruction symbol = buildScopReadAccess((SymbolInstruction)`statement);
						symbol.setType((`statement).getType());
						return symbol;
					}
				}
			}
			 
			//candidate in call instruction
			c@fcall(_,InstL(_*)) -> {
				Instruction i = (`c);
				debug("\t-Rule 6 : function call "+i.getRoot());
				CallInstruction callInst = (CallInstruction) i;
				callInst.setType(`c.getType()); 
				return AnalyseCallStatement(callInst);
			}

			//candidate in indirection instruction
			c@indir(_) -> {
				throw new SCoPException((`c).getBasicBlock(),"Found indirection "+(`c));
			}

			// normalizing domain constraints guards
			inst@brcond(generic("booldisjunction",InstL(systems*))) -> {
				return (`inst);
			}
			// normalizing domain constraints guards
			inst@brcond(or@generic("lor",InstL(systems*))) -> {
				GenericInstruction copy = (GenericInstruction)(`or).copy();
				Instruction res = `brcond(generic("booldisjunction",copy.getChildren()));
				debug("\t-Rule 7 : normalizing constraints : "+(`inst)+"->"+res);
				return res ;
			}
			// normalizing domain constraints guards
			inst@brcond(s@sconstraintSys(system)) -> {
				Instruction res =  `brcond(generic("booldisjunction",InstL(s.copy())));  
				debug("\t-Rule 8 : normalizing constraints : "+(`inst)+"->"+res);
				return res ;
			}

			// normalizing domain constraints guards  
			inst@brcond(_) -> {
				if (!((`inst).getRoot() instanceof ScopInstructionStatement)) {
					Instruction copy = (`inst).copy();
					Instruction res =  (Instruction) (`stmt("S", InstL(copy)));  
					debug("\t-Rule 9 : non affine guard : "+(`inst)+"->"+res);
					return res ;
				} else {
					return (`inst);
				}
			}
			
		}  
	}
  

	private static Instruction AnalyseCallStatement(CallInstruction callInst) {
		Instruction rootIns = callInst.getRootInstuction();
		List<Instruction> args = callInst.getArgs();

		/* analyzing function call arguments, to determine if the call is supported 
			  as part of a SCoP */
		for(Instruction arg: args){
			boolean pass=false;
			%match (arg){
				sarray(_,InstL(_)) -> {
					debug("  - Argument "+arg + " is OK");
					pass=true;
				}
				convert(address(iwrite(_,_))) -> {
					debug("\t\t-Argument "+arg + " is OK");
					pass=true;
				}
				convert(address(sarray(_,InstL(_*)))) -> {
					debug("\t\t-Argument "+arg + " is OK");
					pass=true;
				}
				address(sarray(_,InstL(_))) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				convert(symref(_)) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				symref(_) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				} 
				sexpr(affine(_)) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				} 
				sexpr(qaffine(_)) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				convert(ival(_)) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				ival(_) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				convert(address(symref(_))) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				address(symref(_)) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				iread(_,_) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				generic("add", _) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				generic("sub", _) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				generic("mul", _) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
				generic("div", _) -> {
					debug("\t\t- Argument "+arg + " is OK");
					pass=true;
				}
			}
			if(!pass) {
				throw new SCoPException(callInst.getBasicBlock(), "Argument "+arg+ ":"+arg.getClass().getSimpleName()+" in " +callInst + " is not compatible with a pure function call");
			}
		}

		/* analyzing implicit Write operations (using pointers as arguments) */

		for(Instruction arg: args){ 
			%match (arg){
				convert(address(inst@sarray(_,InstL(_)))) -> {
					Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)`inst);
					symbol.setType((`inst).getType());
					(`inst).substituteWith(symbol);
				}
				address(inst@sarray(_,InstL(_))) -> { 
					Instruction symbol = buildScopWriteAccess((SimpleArrayInstruction)`inst);
					symbol.setType((`inst).getType());
					(`inst).substituteWith(symbol);
				}
				address(inst@symref(_)) -> { 
					Instruction symbol = buildScopWriteAccess((SymbolInstruction)`inst);
					symbol.setType((`inst).getType());
					(`inst).substituteWith(symbol);
				}
				convert(address(inst@symref(_))) -> { 
					Instruction symbol = buildScopWriteAccess((SymbolInstruction)`inst);
					symbol.setType((`inst).getType());
					(`inst).substituteWith(symbol);
				}
			}
		}
		return callInst;
	}

	private static boolean isWrite(Instruction i){
		if(i.getParent() instanceof SetInstruction){
			SetInstruction set = (SetInstruction)i.getParent();
			if(set.getDest()==i)
				return true;
		}
		return false;
	}

	private static boolean isCallWriteArgument(Instruction i){
		Instruction parent = i.getParent();
		if(!(parent instanceof AddressInstruction)) return false;
		while(parent!=null) {
			if(parent instanceof CallInstruction) return true;
			parent = parent.getParent();
		}
		return false;
	}
}