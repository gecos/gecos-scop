/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.*;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractBlockTransformation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.AbstractScopDomainAnalyser;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.ScopDomainBuilder;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.CoreFactory;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
  
@SuppressWarnings({"all"})
public class ScopDomainAnalyser extends ScopDomainBuilder {

	%include { sl.tom } 

	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }

	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }
	%include { algebra_quasiaffine.tom }
	%include { algebra_domains.tom }
	%include { scop_terminals.tom } 
	  
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_control.tom }
	%include { gecos_logical.tom }

	%include { gecos_basic.tom }  
	%include { scop_nodes.tom }     
	%include { extractor_extraction.tom }  
	
	%typeterm Context {
		implement 		{VariableContextManager}
		is_sort(t) 		{$t instanceof VariableContextManager}
		equals(l1,l2) 	{$l1==$l2}
	}

	%typeterm RootScopNode{
		implement 		{GecosScopBlock}
		is_sort(t) 		{$t instanceof GecosScopBlock}
		equals(l1,l2) 	{$l1==$l2}
	}

	%typeterm ScopDomainAnalyser {
		implement 		{ScopDomainAnalyser}
		is_sort(t) 		{$t instanceof ScopDomainAnalyser}
		equals(l1,l2) 	{$l1==$l2}
	}

	public ScopDomainAnalyser(VariableContextManager context) {
		super(context);
	}
	
	protected Block tom_rewrite(Block block) throws VisitFailure, SCoPException {
		
		try {
			return `InnermostId(DomainAnalyzer(context,this)).visitLight(block, tom.mapping.GenericIntrospector.INSTANCE);
		} catch(VisitFailure e) {
			throw new SCoPException(block, "Tom strategy failed");
		} catch(SCoPException e) {
			throw e;
		}
	}

	 

	%strategy DomainAnalyzer(_context : Context, scopDomainAnalyser : ScopDomainAnalyser) extends Identity() {
		visit Blk {
			a -> {
				debug("\nVisiting "+(`a).getClass().getSimpleName()+":"+(`a));
			}
  
			b@forBlock(_, _, _, _) -> {
				int dimension = findDimension(`b);
				debug("Dimension for "+(`b)+"="+dimension);
				SimpleForBlock simpleForBlock = (SimpleForBlock)(`b);
				List<Symbol> iterators = loopCounterAnalyzer.findSurroundingLoopCounters((`b));
				debug("Entering for loop "+(`b)+" iterators = "+iterators);
				context.activateIterators(iterators, dimension);
				ScopDimension iterator = context.lookUp(simpleForBlock.getIterator());
				context.removeIterators(iterators);
				
				Instruction lb = simpleForBlock.getLb();
				Instruction ub = simpleForBlock.getUb();
				Instruction stride = simpleForBlock.getStride();
				try {
					extractForInformation(scopDomainAnalyser,
						simpleForBlock,
						iterator,
						((ScopIntExpression)lb).getExpr(),
						((ScopIntExpression)ub).getExpr(),
						((ScopIntExpression)stride).getExpr(),
						simpleForBlock.getBodyBlock());
					
				} catch (ClassCastException e) {
					e.printStackTrace();
					throw new ClassCastException("Non affine loop : something went wrong ! \n ub="+ub+"\nlb="+lb+"\nstride="+stride);
				}		
			}
			
			b@ifThenElse(guardblk@BB(InstL(guards*)),_then,_else) -> {
				debug("\t- Checking empty If/Else guard "+(`b)); 
			}
			b@ifThenElse(guardblk@BB(InstL(guard)),_then,_else) -> {
				debug("\t- Checking If/Else guard "+(`guard)+":"+(`guard).getClass().getSimpleName());
				boolean affine =false;
				%match (guard){
					brcond(generic("booldisjunction",InstL(systems*))) -> {
						debug("\t\t- If/Else guard is a union of affine constraint systems");
						affine=true;
						buildIf(`b, `guard, `systems, `_then, `_else, scopDomainAnalyser);
					}
					brcond(generic("lor",InstL(systems*))) -> {
						debug("\t\t- If/Else guard is a union of affine constraint systems");
						affine=true;
						buildIf(`b, `guard, `systems, `_then, `_else, scopDomainAnalyser);
						throw new SCoPException((`b),"Should not happen !");
						
					}
					brcond(sconstraintSys(system)) -> {
						debug("\t\t- If/Else guard is an affine constraint systems");
						affine=true;
						if(`_else==null) { 
							try {
								ScopNode thenScopNode=scopDomainAnalyser.get((`_then));
								ScopNode newNode= (ScopNode)(`guard(ICSList(system),thenScopNode,null));
								scopDomainAnalyser.put((`b),newNode);
							} catch (SCoPException e) {
								throw new SCoPException((`b)," then branch is not a ScopNode ");
							}
						} else {
							scopDomainAnalyser.put((`b),(ScopNode)(`guard(ICSList(system),
							scopDomainAnalyser.get(_then),
							scopDomainAnalyser.get(_else))));
						}
						throw new SCoPException((`b),"Should not happen !");
					}
				}
				if(!affine) {
					Instruction guard = (`guard);  
					debug("\t\t- If/Else guard is *NOT* affine "+(`guard));
					if(`_else==null) { 
						try {
							ScopNode thenScopNode=scopDomainAnalyser.get((`_then));
							
							%match(thenScopNode) {
								s@stmt(label,_) -> {
									ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(`s), guard);
									scopDomainAnalyser.put((`b), stmt);
									
								}
								sblk(nodes(children*)) -> {    
									EList<ScopInstructionStatement> stmts = new BasicEList<ScopInstructionStatement>();
								
									for (ScopNode c : (`children)) {
										%match(c) {
											s@!stmt(_,_) -> { 
												throw new SCoPException((`b),"Non affine If/Else guard "+(`guard) +" combined with complex scop then branch");
											}
											s@stmt(_,_) -> {
												ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(`s), guard); 
												stmts.add((ScopInstructionStatement)stmt.copy());   
											}
										}
									}
									ScopBlock block = (ScopBlock)(`scopBlk((EList)stmts)); 
									scopDomainAnalyser.put((`b), block);
								}
								sblk(nodes(s@stmt(label,InstL(children*)))) -> {
									
									ScopInstructionStatement stmt = ifConvert((ScopInstructionStatement)(`s), guard);
									scopDomainAnalyser.put((`b), stmt);
								}
							}
						} catch (SCoPException e) {
							throw new SCoPException((`b),"Non affine If/Else guard "+(`guard) +" combined with non scopable then branch");
						}
					} else {
						throw new SCoPException((`b)," If/Else guard in "+(`b)+" is *NOT* affine "+(`guard));
					}

				}
			}

			b@BB(instrs) -> {
				Block parent = (`b).getParent();
				boolean skip = false;
				%match(parent) {
					ifThenElse(g,_then,_else) -> {
						if ((`g)==(`b)) {
							skip=true;
							debug("\t\t- Skipping guard basic block "+(`b)+" in if/else "+parent);
						}
					} 
					forBlock(i,t,s,_) -> {
						if ((`i)==(`b)) {
							skip=true;
							debug("\t- Skipping for-loop init in "+ parent);
						}
						if ((`t)==(`b)) {
							skip=true;
							debug("\t- Skipping for-loop test in "+ parent);
						}
						if ((`s)==(`b)) {
							skip=true;
							debug("\t- Skipping for-loop step in "+ parent);
						}
					} 
				}
				
				if(!skip) {
				
					/**
						Decorating Gecos Instruction as ScopInstructionStatement
					**/
					debug("\t- Analyzing BasicBlock "+(`b));
					List<Instruction> instrs = new BasicEList<Instruction>(`instrs);
					ScopNode resNode = null;
	
					EList<ScopInstructionStatement> stmts = new BasicEList<ScopInstructionStatement>();
					for(Instruction i : instrs) {
						ScopInstructionStatement node = analyzeInstruction(i);
						stmts.add((ScopInstructionStatement)node.copy());
						debug("\t\t- Analyzing instruction "+i.toString().replace('\n',' ')+" -> "+node.toString().replace('\n',' '));
					}
					ScopBlock block = (ScopBlock)(`scopBlk((EList)stmts));
					resNode = block;
					debug("\t\t- Building ScopBlock "+block.toString().replace('\n',' '));

					GecosUserAnnotationFactory.copyAnnotations((AnnotatedElement)`b, (AnnotatedElement) resNode);
					scopDomainAnalyser.put((`b), resNode);
					debug("\t\t- Added ScopBlock to map "+block.toString().replace('\n',' '));
				}
			}
 
			b@cblock(BlkL(children*)) -> {
				//processComposite((`b),(`children) )
				debug("\t- Analyzing CompositeBlock "+(`b));
				if (`children.size() > 1 || !(`b).getScope().getSymbols().isEmpty()) {
					EList<ScopNode> _nodes = new BasicEList<ScopNode>();
					for(Block child : `children) {
						debug("\t\tLooking for child "+child.toShortString() + " in map");
						try {
							ScopNode node = scopDomainAnalyser.get(child);
							_nodes.add(node); 
						} catch (SCoPException e) {
							throw new SCoPException((`b),"Composite block children without ScopNode mapping" );
						}
						//if (node instanceof ScopBlock && ((ScopBlock) node).getScope() != null && ((ScopBlock) node).getScope().getSymbols().size() == 0)
						//	_nodes.addAll(((ScopBlock) node).getChildren());
						//else
					} 
					ScopBlock gblk = (ScopBlock) (`scopBlk(_nodes));
					GecosUserAnnotationFactory.copyAnnotations(`b,gblk);
					scopDomainAnalyser.put((`b),(ScopNode)gblk);
				} else if (`children.size() == 1) {
					Block child = `children.get(0);
					try {
						ScopNode childScopNode = scopDomainAnalyser.get(child);
						scopDomainAnalyser.put((`b),childScopNode);
						GecosUserAnnotationFactory.copyAnnotations(`b,childScopNode);
					} catch (SCoPException e) {
						throw new SCoPException((`b),"children "+child.getClass().getSimpleName()+" of "+(`b)+" is not a Scop !") ;
					}
				}
			}
		}
	}


/*
	private static EList<IntConstraintSystem> analyzeGuard(List<Instruction> systems) {
		EList<IntConstraintSystem> res= new BasicEList<IntConstraintSystem>();
		for(int i=0 ; i < systems.size(); i++) {
			%match(systems.get(i)) {
				!sconstraintSys(_) -> {
					throw new SCoPException(systems.get(i).getBasicBlock(),("If/Else guard '"+systems+"' is not Scopable "));
				}
				sconstraintSys(_) -> {
					res.add(((ScopIntConstraintSystem)systems.get(i)).getSystem());
				}
			}
		}
		return res;
	}
	private static void buildIf(Block ifBlock, Instruction guard, List<Instruction> systems, Block thenBlock, Block elseBlock, ScopDomainAnalyser scopDomainAnalyser) {
		if (systems.size() > 0) {
			ScopNode thenScopNode = null;
			try {
				thenScopNode = scopDomainAnalyser.get(thenBlock);
			} catch (SCoPException e) {
				throw new SCoPException(ifBlock," then statement "+thenBlock+" is not a Scop !") ;
			}

			ScopNode elseScopNode = null;
			if(elseBlock != null) {
				elseScopNode =scopDomainAnalyser.get(elseBlock);
			}

			try {
				EList<IntConstraintSystem> predicate = analyzeGuard(systems);
				ScopGuard newNode = (ScopGuard)(`guard(predicate,thenScopNode,elseScopNode));
				GecosUserAnnotationFactory.copyAnnotations(ifBlock, newNode);
				scopDomainAnalyser.put(ifBlock,newNode); 
			} catch (SCoPException e) {
				%match(thenScopNode) { 
					s@stmt(id,InstL(children)) -> {
						debug("Fixing "+(`s));
						throw new SCoPException(ifBlock," data-dependant guard "+systems+" for "+(`s)) ;
					}
					scopBlk(nodes(s@stmt(id,InstL(children)))) -> {
						debug("Fixing "+(`s));
						throw new SCoPException(ifBlock," data-dependant guard "+systems+" for "+(`s)) ;
					}
				}
				throw new SCoPException(ifBlock," guard predicate "+systems+" is not polyhedral") ;
			}
		}
	}

	private static void extractForInformation(
			ScopDomainAnalyser scopDomainAnalyser, 
			SimpleForBlock simpleForBlock, 
			ScopDimension variable, 
			IntExpression lowerBound, 
			IntExpression upperBound, 
			IntExpression step, 
			Block body) {

		if (variable==null) throw new SCoPException(simpleForBlock, "null variable "+variable);
		if (lowerBound==null) throw new SCoPException(simpleForBlock,"null lower bound for "+simpleForBlock);
		if (upperBound==null) throw new SCoPException(simpleForBlock,"null upper bound for "+simpleForBlock);
		if (step==null) throw new SCoPException(simpleForBlock,"null stride for "+simpleForBlock); 
		
		try {

			ScopNode bodyAsScop= scopDomainAnalyser.get(body);
			ScopForLoop  res= (ScopForLoop) (`loop(variable,lowerBound.copy(),upperBound.copy(),step.copy(),bodyAsScop));
			GecosUserAnnotationFactory.copyAnnotations(simpleForBlock, res);
			//res.getAnnotations().putAll(simpleForBlock.getAnnotations());
			if (simpleForBlock.getScope() != null && !simpleForBlock.getScope().getSymbols().isEmpty()) {
				EList<ScopNode> nodes = new BasicEList<ScopNode>();
				nodes.add(res);
				ScopBlock gblk = (ScopBlock) (`scopBlk(nodes));
			//gblk.setScope(simpleForBlock.getScope());
				scopDomainAnalyser.put(simpleForBlock,((ScopNode)gblk));
			} else {
				scopDomainAnalyser.put(simpleForBlock,((ScopNode)res));
			}
		} catch (SCoPException e) {
			throw new SCoPException(simpleForBlock,"Loop body for "+simpleForBlock+" is not a Scop !") ;
		}
	}
*/
}