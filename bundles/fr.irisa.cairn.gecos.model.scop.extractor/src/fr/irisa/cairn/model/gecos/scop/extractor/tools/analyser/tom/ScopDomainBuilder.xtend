package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.polymodel.algebra.ComparisonOperator
import org.polymodel.algebra.FuzzyBoolean
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.polynomials.PolynomialVariable
import fr.irisa.cairn.gecos.model.scop.*
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.gecos.model.transforms.tools.AbstractBlockTransformation
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.AbstractScopDomainAnalyser
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.annotations.*

import gecos.blocks.*


import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ScopDomainAnalyser
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager
import gecos.instrs.Instruction
import gecos.instrs.LabelInstruction
import gecos.instrs.SetInstruction
import gecos.instrs.CallInstruction
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.LoopCounterAnalyzer
import gecos.blocks.Block
import tom.library.sl.VisitFailure

abstract class ScopDomainBuilder extends AbstractScopDomainAnalyser {
		
	protected static final boolean VERBOSE = true;
	//ScopExtractorPass.VERBOSE;
	
	def public static void debug(String mess) {
		if(VERBOSE) { 
			println("[ScopDomainAnalyzer] "+mess);
		}
	}

	new (VariableContextManager context) {
		super(context);
	}
	static protected LoopCounterAnalyzer loopCounterAnalyzer
  

	
	override protected Block apply(Block block)  {
		debug('''- Analyzing domains for «block»'''.toString)
		rootBlock = block
		loopCounterAnalyzer = new LoopCounterAnalyzer(block)
		var GecosScopBlock root = ScopUserFactory::root()
		root.setScope(GecosUserCoreFactory::scope())   
		var Block b = tom_rewrite(block)  
		if (b instanceof ScopBlock) {
			root.setRoot((b as ScopNode)) // root.setScope(b.getScope());
		} else {
			root.setRoot((get(b) as ScopNode)) // root.setScope(CoreFactory.eINSTANCE.createScope());
		}
		var VariableContextManager ctxt = context
		root.getIterators().addAll(ctxt.getAllIterators())
		root.getParameters().addAll(ctxt.getAllParameters())
		var List<ScopForLoop> children = EMFUtils::eAllContentsInstancesOf(root, typeof(ScopForLoop))
		if(children.isEmpty()) throw new SCoPException(block, "No for loop in the block");
		return root
	}
	
	def protected Block tom_rewrite(Block block) throws VisitFailure, SCoPException

	def protected static ScopInstructionStatement analyzeInstruction(Instruction instr) {
		if (instr === null) {
			throw new SCoPException(instr.getBasicBlock(), "Error in analyzeInstruction (instr==null)")
		}
		var ScopInstructionStatement resNode = null
		
		if (instr instanceof ScopInstructionStatement) {
			resNode = instr as ScopInstructionStatement
		} else if (instr instanceof CallInstruction) {
			var CallInstruction call = (instr as CallInstruction)
			var PragmaAnnotation pragma = call.getProcedureSymbol().getPragma()
			if (pragma !== null && pragma.getContent().contains("GCS_PURE_FUNCTION")) { 
				var ScopInstructionStatement gScopInstructionStatement = ScopUserFactory.scopStatement("Call",call)
				gScopInstructionStatement.setType(call.getType())
				gScopInstructionStatement.getAnnotations().putAll(call.getAnnotations())
				resNode = gScopInstructionStatement
			} else {
				throw new SCoPException(instr.getBasicBlock(), '''Function '«»«call»' is not Scopable '''.toString)
			}
		} else if (instr instanceof LabelInstruction) {
			var LabelInstruction label = (instr as LabelInstruction)
			var ScopInstructionStatement gScopInstructionStatement = ScopUserFactory.scopStatement(label.getName(),
				instr)
			gScopInstructionStatement.setType(instr.getType())
			gScopInstructionStatement.getAnnotations().putAll(instr.getAnnotations())
			resNode = gScopInstructionStatement
		}
		if (resNode === null) {
			throw new SCoPException(instr.getBasicBlock(),
				'''Error in analyzeInstruction for «instr»:«instr.getClass().getSimpleName()»'''.toString)
		}
		return resNode
	}

	def protected static ScopInstructionStatement ifConvert(ScopInstructionStatement scopStatement, Instruction guard) {
		debug('''ifConvert(«scopStatement»,«guard»)'''.toString)
		var EList<ScopWrite> listAllWriteAccess = scopStatement.listAllWriteAccess()
		if (scopStatement.getChild(0) instanceof LabelInstruction) {
			return scopStatement
		} else {
			var ScopWrite write = (listAllWriteAccess.get(0).copy() as ScopWrite)
			var BasicEList<IntExpression> index = new BasicEList<IntExpression>()
			for (IntExpression expr : write.getIndexExpressions()) {
				index.add(expr.copy())
			}
			var ScopRead read = ScopUserFactory.scopRead(write.getSym(), index)
			try {
				var ScopInstructionStatement guardStmt = (guard as ScopInstructionStatement)
				var Instruction predicate = guardStmt.getChildren().get(0).copy()
				var SetInstruction set = ((scopStatement.getChildren().get(0)) as SetInstruction)
				var Instruction select = GecosUserInstructionFactory.set(write,
					GecosUserInstructionFactory.generic("mux",write.type,#[predicate, set.getSource().copy(), read]))
				var ScopInstructionStatement ternary = ScopUserFactory.scopStatement("mux",select)
				debug('''	=>«ternary»'''.toString)
				return ternary
			} catch (ClassCastException e) {
				throw new RuntimeException(
					'''Unsupported instruction pattern «scopStatement» in if-conversion'''.toString)
			}

		}
	}
		
	def	protected static EList<IntConstraintSystem> analyzeGuard(List<Instruction> systems) {
		val EList<IntConstraintSystem> res= new BasicEList<IntConstraintSystem>();
		for(s:systems) {
			switch (s) {
				ScopIntConstraintSystem :{
					res.add(s.getSystem());
				}
				default:{
					throw new SCoPException(s.getBasicBlock(),("If/Else guard '"+systems+"' is not Scopable "));
				}	
			}
		}
		return res;
	}
		
	def protected static void buildIf(Block ifBlock, Instruction guard, List<Instruction> systems, Block thenBlock,
		Block elseBlock, ScopDomainAnalyser scopDomainAnalyser) {
		if (systems.size() > 0) {
			var ScopNode thenScopNode = null
			try {
				thenScopNode = scopDomainAnalyser.get(thenBlock)
			} catch (SCoPException e) {
				throw new SCoPException(ifBlock, ''' then statement «thenBlock» is not a Scop !'''.toString)
			}

			var ScopNode elseScopNode = null
			if (elseBlock !== null) {
				elseScopNode = scopDomainAnalyser.get(elseBlock)
			} try {
				var predicate = analyzeGuard(systems)
				var ScopGuard newNode = ScopUserFactory.scopGuard(predicate, thenScopNode, elseScopNode)
				GecosUserAnnotationFactory::copyAnnotations(ifBlock, newNode)
				scopDomainAnalyser.put(ifBlock, newNode)
			} catch (SCoPException e) {
				switch(thenScopNode) { 
					ScopStatement: {
						debug("Fixing "+thenScopNode);
						throw new SCoPException(ifBlock," data-dependant guard "+systems+" for statement "+thenScopNode) ;
					}
					ScopBlock:{ 
						debug("Fixing "+thenScopNode);
						if (thenScopNode.children.size==1) {
							val child =thenScopNode.children.get(0)
							switch(child) {
								ScopStatement: {
									throw new SCoPException(ifBlock," data-dependant guard "+systems+" for statement "+child) ;
								}					
							}
						}
					}
				}
				throw new SCoPException(ifBlock, ''' guard predicate «systems» is not polyhedral'''.toString)
			}
		}
	}

	def protected static void extractForInformation(ScopDomainAnalyser scopDomainAnalyser, SimpleForBlock simpleForBlock,
		ScopDimension variable, IntExpression lowerBound, IntExpression upperBound, IntExpression step, Block body) {
		
		
		if(variable === null) 
			throw new SCoPException(simpleForBlock, '''null variable «variable»'''.toString);
		if(lowerBound === null) 
			throw new SCoPException(simpleForBlock,'''null lower bound for «simpleForBlock»'''.toString);
		if(upperBound === null) 
			throw new SCoPException(simpleForBlock,'''null upper bound for «simpleForBlock»'''.toString);
		if(step === null) 
			throw new SCoPException(simpleForBlock, '''null stride for «simpleForBlock»'''.toString);

		try {
			var bodyAsScop = scopDomainAnalyser.get(body)
			var loop = ScopUserFactory.scopFor(variable, lowerBound.copy, upperBound.copy, step.copy, bodyAsScop) 
			GecosUserAnnotationFactory.copyAnnotations(simpleForBlock, loop)
			val localScope = simpleForBlock.getScope();
			if (localScope!== null && !localScope.symbols.isEmpty) {
				scopDomainAnalyser.put(simpleForBlock, ScopUserFactory.scopBlock(loop))
			} else {
				scopDomainAnalyser.put(simpleForBlock, loop)
			}
		} catch (SCoPException e) {
			e.printStackTrace
			throw new SCoPException(simpleForBlock, '''Loop body for «simpleForBlock» is not a Scop !'''.toString)
		}

	}
	
}