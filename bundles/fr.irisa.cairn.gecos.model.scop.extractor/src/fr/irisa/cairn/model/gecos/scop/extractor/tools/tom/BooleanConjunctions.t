package fr.irisa.cairn.model.gecos.scop.extractor.tools;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
   
@SuppressWarnings({"all"})
public class BooleanConjunctions extends GenericInstructionTransformation {
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }

	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction res = `InnermostId(Conjunctions()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
		return res;
	}  
   
	%op Inst boolconjunction(list: InstL) {
	    is_fsym(t)        	{ (t instanceof GenericInstruction) && ((GenericInstruction)t).getName().equals("boolconjunction")}
	    get_slot(list,t)  	{ enforce(((GenericInstruction)$t).getChildren()) }
	    make(l)         	{ fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric("boolconjunction",$l)  } 
	}
	
	%op Inst booldisjunction(list: InstL) {
	    is_fsym(t)        	{ (t instanceof GenericInstruction) && ((GenericInstruction)t).getName().equals("booldisjunction")}
	    get_slot(list,t)  	{ enforce(((GenericInstruction)$t).getChildren()) }
	    make(l)         	{ fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric("booldisjunction",$l)  } 
	}

	%strategy Conjunctions() extends Identity() {
		visit Inst {
				//Build rules
				and(InstL(c1@(gt|lt|ge|le|eq)(InstL(_)),c2@(gt|lt|ge|le|eq)(InstL(_)))) -> {return `boolconjunction(InstL(c1,c2));}
				and(InstL(c1@booldisjunction(_),c2@(gt|lt|ge|le|eq)(InstL(_)))) -> {return `boolconjunction(InstL(c1,c2));}
				and(InstL(c1@(gt|lt|ge|le|eq)(InstL(_)),c2@booldisjunction(_))) -> {return `boolconjunction(InstL(c1,c2));}
				and(InstL(c1@booldisjunction(_),c2@booldisjunction(_))) -> {return `boolconjunction(InstL(c1,c2));}
				
				//Expand rules
				and(InstL(c1@boolconjunction(_),c2@(gt|lt|ge|le|eq)(InstL(_,_)))) -> {return `boolconjunction(InstL(c1,c2));}
				and(InstL(c1@(gt|lt|ge|le|eq)(InstL(_)),c2@boolconjunction(_))) -> {return `boolconjunction(InstL(c1,c2));}
				and(InstL(c1@boolconjunction(_),c2@boolconjunction(_))) -> {return `boolconjunction(InstL(c1,c2));}
		} 
	}
}