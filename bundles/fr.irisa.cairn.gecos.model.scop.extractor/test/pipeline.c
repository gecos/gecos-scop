/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
//#pragma scop_pure_function
void main()
{
int N;
//#pragma scop_change_array_layout f[i] -> [i%2]
  int f[N];
//#pragma scop_contract_array t1
  int t1[N];
//#pragma scop_merge_arrays (f,t2)
  int t2[N];
  int i,result;

#pragma scop_schedule_dims (t:SEQUENTIAL:8, p:SEQUENTIAL:8)
#pragma scop_schedule_statement (0, 0)
  f[0] = 0;
#pragma scop_schedule_statement (1, 0)
  f[1] = 1;
#pragma	scop_definition scop_ex0 (N-9>=0, N>=0)
  for(i=2; i<N; i++)
    {
#pragma scop_schedule_statement (2,i)
      t1[i] = f[i-1];
#pragma scop_schedule_statement (2,i+1)
      t2[i] = f[i-2];
#pragma scop_schedule_statement (2,i+2)
      f[i] = t1[i] + t2[i];
    }
//
#pragma scop_schedule_statement (3, 0)
  result = f[N-1];

  //return result;
}
