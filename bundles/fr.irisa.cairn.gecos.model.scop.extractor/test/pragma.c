/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
void foobar(int N) {
	int i, j;
//#pragma GCS_CHANGE_ARRAY_LAYOUT x[i][j] -> [(i+j)%2]
	int x[12][12];
//#pragma GCS_CONTRACT_ARRAY y
	int y[12][12];
//#pragma GCS_CHANGE_ARRAY_LAYOUT z[i] -> [i%2]
	int z[12][12];
#pragma GCS_SCHEDULE_DIMS (t:SEQUENTIAL,pp,p,-)
//#pragma GCS_CLOOG_OPTIONS lastDepthToOptimize=2 firstDepthToOptimize=2
#pragma GCS_SCOP_ADD_CONTEXT (i>1)
	for (i = 0; i < N; i++) {
#pragma GCS_SCHEDULE_STMT (i,0,0,0)
		x[i][0]=0;
		for (j = i; j < N; j++) {
#pragma GCS_SCHEDULE_STMT (i-N, (j-1)/8,(j-1)%8,0)
			x[j][i] = x[i - j][i] + x[j][i - 1];
#pragma GCS_SCHEDULE_STMT (i,j/8,j%8,1)
			x[N - j][i] = x[i - j][i] + x[j][i - 1];
		}
	}
}


