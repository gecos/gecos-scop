/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
void foo(int N, int** x) {
	int i, j, a, d;
	a = 10;
	for(i = 0; i < N; i++) {
//		Lparam = {b, c, d}
//		Lindice = {i}
		a = i;
		for(j = 0; j < i; j++) {
			a = x[j-N];
			//foo(N, X);
			if((j+2*i+8>=0) && (N>0)) {
				x[i*7+2][i]=x[i][0]+a;
			}
			//Lparam = {b, c, d}
			//Lindice = {i, j}
		}
	}
}

void main() {
	int a = 1;
	int i;
	int j;
	int *p;
	a = (3 + 4) / 2;
	for(i = 0; i < 10; i++) {
		a *= i;
		p = &i;
		//p = &a;
		//i = a;
		for (j = 0; j < i; j++) {
			//i = 0;
		}
	}
}
