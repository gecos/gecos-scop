/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
/*void bar (int N, int* x, int a) {
	int i;
	int j;
	int k;
	int l;
	for (i=0; i < N; i++) {
		for (j=0; j < N; j++) {
			x[j] = a;
		}
		for (k=0; k < N; k++) {
			x[k] = a;
		}
		i = a;
		while (a) {
			a = 0;
		}
		for (l=0; l < N; l++) {
			x[l] = a;
		}
	}
}*/

int lu(int N, int *a)
{
	int i, j, k;

    /* pluto start (N) */
    for (k=0; k<N; k++) {
        for (j=k+1; j<N; j++)   {
            a[k][j] = a[k][j]/a[k][k];
        }
        for(i=k+1; i<N; i++)    {
            for (j=k+1; j<N; j++)   {
                a[i][j] = a[i][j] - a[i][k]*a[k][j];
            }
        }
    }
    /* pluto end */

    return 0;
}
