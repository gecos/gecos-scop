/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
void bar (int N, int* x, int a) {
	int i;
	int j;
	int k;
	int c = 0;
	for (i=0; i < N; i++) {
		i = 0;
		for (j=0; j < N; j++) {
			for (k=0; k < j; k++) {
				if ((j+2*k+8>=0) && (N>0)) {
					x[k] = a;
				}
			}
		}
	}
}
