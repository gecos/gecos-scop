

void gauss(int M, int N, int**in, int **out) {
	int i, j;
	int ** tmp;
#pragma scop gauss, dims = (i,j)
	for (i = 1; i < N - 1; i++)
		for (j = 0; j < M; j++)
#pragma  scop_schedule_statement (i,j)
			tmp[i][j] = 3 * in[i][j] - in[i - 1][j] - in[i + 1][j]; //S0
	for (i = 1; i < N - 1; i++)
		for (j = 1; j < M - 1; j++)
#pragma  scop_schedule_statement (i,j+2)
			out[i][j] = 3 * tmp[i][j] - tmp[i][j - 1] - tmp[i][j + 1]; //S1
}

#pragma GCS_PURE_FUNCTION
void pureFunction(int* out1, int* out2, int in1, int in2);
void testPureFunction(int N, int ** x, int ** y) {
	int i,j;
	for (i = 2; i < N; i++) {
		for (j = 0; j < N; j++) {
			pureFunction(&x[i][j],&y[i][j],x[i-1][j],y[i-2][j]);
		}
	}
}

void testSchedulePRDG(int M, int N, int**in, int **out) {
	int i, j;
	int ** tmp;
	for (i = 1; i < N - 1; i++) {
		tmp[i] = in[i][0];
		tmp[i] = tmp[i];
		out[i][0] = tmp[i];
	}
}

void testContract(int M, int N, int**in, int **out) {
	int i, j;
	int ** tmp;
#pragma scop test, context = (N > 0), dims = (i,j)
	for (i = 1; i < N; i++) {
		for (j = 0; j < N; j++) {
#pragma  scop_schedule_statement (i,j)
			tmp[i][j] = tmp[i-1][j];
		}
	}
}

void matmult(int** A, int **B, int **C, int N) {
	int i,j,k,s;
#pragma scop matmult,dims = (i:PAR,j,k)
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
//#pragma  scop_schedule_statement (0,i,j)
#pragma  scop_schedule_statement (i,j,0)
			s = 0;
			for (k = 0; k < N; k++) {
//#pragma  scop_schedule_statement (k,i,j)
#pragma  scop_schedule_statement (i,j,k+1)
				s += A[i][k]*B[k][j];
			}
//#pragma  scop_schedule_statement (P,i,j)
#pragma  scop_schedule_statement (i,j,N+1)
			C[i][j] = s;
		}
	}
}


//#define N 100
void durbin(int* r, int* out, int N) {
	int* alpha;
	int*beta;
	int** y;
	int ** sum;
	int k,i;

	y[0][0] = r[0];
	beta[0] = 1;
	alpha[0] = r[0];
	for (k = 1; k <= N - 1; k++) {
		beta[k] = beta[k - 1] - beta[k - 1] * (alpha[k - 1]) * (alpha[k - 1]);
		sum[0][k] = r[k];
		for (i = 0; i <= k - 1; i++)
			sum[i + 1][k] = sum[i][k] + r[k - i - 1] * y[i][k - 1];
		alpha[k] = -sum[k][k] * beta[k];
		for (i = 0; i <= k - 1; i++)
			y[i][k] = y[i][k - 1] + alpha[k] * y[k - i - 1][k - 1];
		y[k][k] = alpha[k];
	}
	for (i = 0; i <= N - 1; i++)
		out[i] = y[i][N - 1];
}


void test1(int T, int N, int**a) {
	int i, t;
#pragma scop test1, dims = (t,i)
	for (t = 1; t <= T; t++) {
		for (i = 1; i <= N-2; i++) {
#pragma  scop_schedule_statement (t,i)
			a[t][i] = a[t][i-1] + a[t-1][i] + a[t-1][i+1];
		}
	}
}
void test2(int T, int N, int**a) {
	int i, t;
#pragma scop test2, dims = (t,i)
	for (t = 1; t <= T; t++) {
		for (i = 1; i <= N-2; i++) {
#pragma  scop_schedule_statement (t,i)
			a[t][i] = a[t-1][i] + a[t-1][i-1] + a[t-1][i+1];
		}
	}
}

int N;
void forward_subst(int b[N], int L[N][N], int y[N]) {
  int i, j;
  for(i = 0; i < N; i++) {
    y[i] = b[i];
    for(j = 0; j < i; j++)
      y[i] = y[i] - L[i][j]*y[j];
    y[i] = y[i] / L[i][i];
  }
}
// void forward_substitution(int* x, int** L, int* b, int N) {
// 	int i,j;
// 	#pragma omp parallel for private(j)
// 	for (i = 0; i < N; i++) {
// 		x[i] = b[i];
// 		for (j = 0; j < i; j++) {
// 			x[i] = x[i] - L[i][j]*x[j];
// 		}
// 		x[i] = x[i]/L[i][i];
// 	}
// }


void matrix_transpose(int N, int A[N][N]) {
	int i,j,tmp;
	for (i = 0; i < N; i++) {
		for (j = 0; j < i; j++) {
			tmp = A[i][j]; //S0
			A[i][j] = A[j][i]; //S1
			A[j][i] = tmp; //S2
		}
	}
}
