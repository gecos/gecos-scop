package fr.irisa.cairn.model.gecos.scop.extractor;

import com.google.common.base.Objects;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.analysis.dataflow.liveness.LivenessAnalyser;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.IAnnotation;
import gecos.annotations.LivenessAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class LivenessAnalysisPass {
  private static boolean VERBOSE = false;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (LivenessAnalysisPass.VERBOSE) {
      _xifexpression = InputOutput.<String>println(string);
    }
    return _xifexpression;
  }
  
  GecosProject proj;
  
  public LivenessAnalysisPass(final GecosProject proj) {
    this.proj = proj;
  }
  
  public void compute(final GecosScopBlock blk) {
  }
  
  public void compute() {
    LivenessAnalysisPass.debug("Liveness analysis to determine live-in live-out array accesses in SCoPs");
    List<GecosScopBlock> roots = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    final Consumer<GecosScopBlock> _function = (GecosScopBlock r) -> {
      this.replaceScopByBasicBlock(r);
    };
    roots.forEach(_function);
    new BuildControlFlow(this.proj).compute();
    EList<Procedure> _listProcedures = this.proj.listProcedures();
    for (final Procedure proc : _listProcedures) {
      {
        final Function1<Symbol, Boolean> _function_1 = (Symbol it) -> {
          return Boolean.valueOf((!(it instanceof ProcedureSymbol)));
        };
        Symbol[] variables = ((Symbol[])Conversions.unwrapArray(IterableExtensions.<Symbol>filter(EMFUtils.<Symbol>getAllReferencesOfType(proc.getBody(), Symbol.class), _function_1), Symbol.class));
        final LivenessAnalyser live = new LivenessAnalyser(proc, variables);
        live.compute();
        LivenessAnalysisPass.debug("done");
        EList<BasicBlock> _basicBlocks = proc.getBasicBlocks();
        for (final BasicBlock b : _basicBlocks) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("BB ");
          int _number = b.getNumber();
          _builder.append(_number);
          _builder.append(" {");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t\t");
          CharSequence _pp = this.pp(b.getAnnotations().get(LivenessAnalyser.LIVEIN_ANNOTATION));
          _builder.append(_pp, "\t\t\t\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t\t");
          EList<Instruction> _instructions = b.getInstructions();
          _builder.append(_instructions, "\t\t\t\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t\t");
          _builder.append("} ");
          _builder.newLine();
          LivenessAnalysisPass.debug(_builder.toString());
        }
        this.removeUpdateInstructions(proc);
      }
    }
    this.replaceBasicBlockByScop();
    EList<Procedure> _listProcedures_1 = this.proj.listProcedures();
    for (final Procedure proc_1 : _listProcedures_1) {
      EList<BasicBlock> _basicBlocks = proc_1.getBasicBlocks();
      for (final BasicBlock b : _basicBlocks) {
        {
          b.getAnnotations().removeKey(LivenessAnalyser.LIVEIN_ANNOTATION);
          b.getAnnotations().removeKey(LivenessAnalyser.DEFINED_ANNOTATION);
          b.getAnnotations().removeKey(LivenessAnalyser.USED_ANNOTATION);
        }
      }
    }
    new ClearControlFlow(this.proj).compute();
    final Consumer<GecosScopBlock> _function_1 = (GecosScopBlock r) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("live in :");
      EList<Symbol> _liveInSymbols = r.getLiveInSymbols();
      _builder.append(_liveInSymbols);
      _builder.newLineIfNotEmpty();
      _builder.append("live out :  ");
      EList<Symbol> _liveOutSymbols = r.getLiveOutSymbols();
      _builder.append(_liveOutSymbols);
      _builder.newLineIfNotEmpty();
      InputOutput.<String>println(_builder.toString());
    };
    roots.forEach(_function_1);
  }
  
  private void removeUpdateInstructions(final Procedure proc) {
    final EList<SetInstruction> setInsts = EMFUtils.<SetInstruction>eAllContentsInstancesOf(proc, SetInstruction.class);
    for (final SetInstruction setInst : setInsts) {
      {
        final Instruction src = setInst.getSource();
        if ((src instanceof GenericInstruction)) {
          boolean _isNamed = ((GenericInstruction)src).isNamed("update");
          if (_isNamed) {
            final EList<Instruction> listChildren = ((GenericInstruction)src).listChildren();
            final List<Instruction> index = IterableExtensions.<Instruction>toList(IterableExtensions.<Instruction>drop(listChildren, 2));
            final Instruction setValue = listChildren.get(1);
            final ArrayInstruction dest = GecosUserInstructionFactory.array(setInst.getDest().copy(), index);
            setInst.setDest(dest);
            setInst.setSource(setValue);
          }
        }
      }
    }
  }
  
  private BiMap<CompositeBlock, GecosScopBlock> map = HashBiMap.<CompositeBlock, GecosScopBlock>create();
  
  /**
   * Goal : search for symbols that are live-in
   * /* Approach : arrays accessed in ScopRead instances that do have at least
   * /* one source/write outside of the scop are live-in
   */
  public boolean isLiveIn(final Symbol rsym, final ScopNode block) {
    final Function1<ScopRead, Boolean> _function = (ScopRead rd) -> {
      Symbol _sym = rd.getSym();
      return Boolean.valueOf(Objects.equal(_sym, rsym));
    };
    final Iterable<ScopRead> reads = IterableExtensions.<ScopRead>filter(block.listAllReadAccess(), _function);
    String _name = rsym.getName();
    String _plus = ("\t- Checking if " + _name);
    String _plus_1 = (_plus + " is live in Scop");
    LivenessAnalysisPass.debug(_plus_1);
    final JNIISLUnionMap rawDeps = ScopISLConverter.getRAWDepenceGraph(rsym, block);
    for (final ScopRead rd : reads) {
      {
        final ScopStatement rdStmt = rd.getEnclosingStatement();
        final JNIISLSet domS = ScopISLConverter.getDomain(rdStmt);
        LivenessAnalysisPass.debug(("\t\t- Read stmt " + rdStmt));
        LivenessAnalysisPass.debug(("\t\t\t- Read access " + rd));
        String _name_1 = rsym.getName();
        String _plus_2 = ("\t\t\t- RAW deps for " + _name_1);
        String _plus_3 = (_plus_2 + " = ");
        String _plus_4 = (_plus_3 + rawDeps);
        LivenessAnalysisPass.debug(_plus_4);
        JNIISLSet domR = null;
        final Function1<JNIISLMap, Boolean> _function_1 = (JNIISLMap e) -> {
          String _inputTupleName = e.getInputTupleName();
          String _id = rdStmt.getId();
          return Boolean.valueOf(Objects.equal(_inputTupleName, _id));
        };
        Iterable<JNIISLMap> _filter = IterableExtensions.<JNIISLMap>filter(rawDeps.copy().getMaps(), _function_1);
        for (final JNIISLMap edge : _filter) {
          {
            LivenessAnalysisPass.debug(("\t\t\t- Edge " + edge));
            JNIISLSet _copy = domS.copy();
            String _plus_5 = ("\t\t\t\t- Read stmt domain " + _copy);
            LivenessAnalysisPass.debug(_plus_5);
            if ((domR == null)) {
              domR = edge.getDomain();
            } else {
              domR = domR.union(edge.getDomain());
            }
            JNIISLSet _copy_1 = domR.copy();
            String _plus_6 = ("\t\t\t\t- RAW validity domain" + _copy_1);
            LivenessAnalysisPass.debug(_plus_6);
          }
        }
        if ((domR == null)) {
          LivenessAnalysisPass.debug("\t\t\t\t\t- No source statement");
          return true;
        } else {
          boolean _isStrictSubset = domR.isStrictSubset(domS);
          if (_isStrictSubset) {
            JNIISLSet _subtract = domS.copy().subtract(domR.copy());
            String _plus_5 = ("\t\t\t\t\t- No source on domain " + _subtract);
            LivenessAnalysisPass.debug(_plus_5);
            String _name_2 = rsym.getName();
            String _plus_6 = ("\t\t\t\t\t- *" + _name_2);
            String _plus_7 = (_plus_6 + " is live-in*");
            LivenessAnalysisPass.debug(_plus_7);
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public void replaceScopByBasicBlock(final GecosScopBlock block) {
    LivenessAnalysisPass.debug("Summarizing SCOP for liveness");
    Set<Symbol> liveInsSyms = IterableExtensions.<Symbol>toSet(ScopAccessQuery.getAllReadSymbols(block));
    final Function1<Symbol, String> _function = (Symbol s) -> {
      return s.getName();
    };
    Iterable<String> _map = IterableExtensions.<Symbol, String>map(liveInsSyms, _function);
    String _plus = ("\t-Read symbols " + _map);
    LivenessAnalysisPass.debug(_plus);
    final HashSet<Symbol> readOnlySyms = ScopAccessAnalyzer.getAllReadOnlyArrays(block);
    Iterables.removeAll(liveInsSyms, readOnlySyms);
    final Function1<Symbol, String> _function_1 = (Symbol s) -> {
      return s.getName();
    };
    Iterable<String> _map_1 = IterableExtensions.<Symbol, String>map(readOnlySyms, _function_1);
    String _plus_1 = ("\t-Read only symbols " + _map_1);
    LivenessAnalysisPass.debug(_plus_1);
    Instruction use = null;
    final Function1<Symbol, String> _function_2 = (Symbol s) -> {
      return s.getName();
    };
    Iterable<String> _map_2 = IterableExtensions.<Symbol, String>map(liveInsSyms, _function_2);
    String _plus_2 = ("\t-Iterate over symbols " + _map_2);
    LivenessAnalysisPass.debug(_plus_2);
    final Function1<Symbol, Boolean> _function_3 = (Symbol s) -> {
      return Boolean.valueOf(this.isLiveIn(s, block));
    };
    liveInsSyms = IterableExtensions.<Symbol>toSet(IterableExtensions.<Symbol>filter(liveInsSyms, _function_3));
    Iterables.<Symbol>addAll(liveInsSyms, readOnlySyms);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("**********************************************");
    _builder.newLine();
    _builder.append("Symbols ");
    final Function1<Symbol, String> _function_4 = (Symbol s) -> {
      return s.getName();
    };
    Iterable<String> _map_3 = IterableExtensions.<Symbol, String>map(liveInsSyms, _function_4);
    _builder.append(_map_3);
    _builder.append(" are live-in");
    _builder.newLineIfNotEmpty();
    _builder.append("***********************************************");
    InputOutput.<String>println(_builder.toString());
    final BasicBlock defuseBB = GecosUserBlockFactory.BBlock();
    final BasicBlock liveOutBB = GecosUserBlockFactory.BBlock();
    final CompositeBlock cb = GecosUserBlockFactory.CompositeBlock(defuseBB, liveOutBB);
    final Function1<Symbol, SymbolInstruction> _function_5 = (Symbol s) -> {
      return GecosUserInstructionFactory.symbref(s);
    };
    use = GecosUserInstructionFactory.generic("use", null, ((Instruction[])Conversions.unwrapArray(IterableExtensions.<SymbolInstruction>toList(IterableExtensions.<Symbol, SymbolInstruction>map(liveInsSyms, _function_5)), Instruction.class)));
    final Function1<ScopWrite, Symbol> _function_6 = (ScopWrite w) -> {
      return w.getSym();
    };
    Set<Symbol> defOutSyms = IterableExtensions.<Symbol>toSet(ListExtensions.<ScopWrite, Symbol>map(block.listAllWriteAccess(), _function_6));
    for (final Symbol wsym : defOutSyms) {
      EList<Instruction> _instructions = defuseBB.getInstructions();
      SetInstruction _set = GecosUserInstructionFactory.set(((Symbol) wsym), use.copy());
      _instructions.add(_set);
    }
    this.map.put(cb, block);
    EList<Instruction> _instructions_1 = defuseBB.getInstructions();
    for (final Instruction i : _instructions_1) {
      LivenessAnalysisPass.debug(i.toString());
    }
    block.getParent().replace(block, cb);
  }
  
  public void replaceBasicBlockByScop() {
    Set<CompositeBlock> _keySet = this.map.keySet();
    for (final CompositeBlock cb : _keySet) {
      {
        final Block defuse = cb.getChildren().get(0);
        final Block liveOut = cb.getChildren().get(1);
        IAnnotation _get = defuse.getAnnotations().get(LivenessAnalyser.LIVEIN_ANNOTATION);
        final LivenessAnnotation live_in = ((LivenessAnnotation) _get);
        IAnnotation _get_1 = liveOut.getAnnotations().get(LivenessAnalyser.LIVEIN_ANNOTATION);
        final LivenessAnnotation live_out = ((LivenessAnnotation) _get_1);
        final GecosScopBlock scop = this.map.get(cb);
        EList<Symbol> _liveInSymbols = scop.getLiveInSymbols();
        EList<Symbol> _refs = live_in.getRefs();
        Iterables.<Symbol>addAll(_liveInSymbols, _refs);
        EList<Symbol> _liveOutSymbols = scop.getLiveOutSymbols();
        EList<Symbol> _refs_1 = live_out.getRefs();
        Iterables.<Symbol>addAll(_liveOutSymbols, _refs_1);
        cb.getParent().replace(cb, scop);
      }
    }
  }
  
  protected CharSequence _pp(final IAnnotation annotation) {
    return null;
  }
  
  protected CharSequence _pp(final LivenessAnnotation annotation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("live : ");
    final Function1<Symbol, String> _function = (Symbol s) -> {
      return s.getName();
    };
    List<String> _map = ListExtensions.<Symbol, String>map(annotation.getRefs(), _function);
    _builder.append(_map);
    return _builder;
  }
  
  public CharSequence pp(final IAnnotation annotation) {
    if (annotation instanceof LivenessAnnotation) {
      return _pp((LivenessAnnotation)annotation);
    } else if (annotation != null) {
      return _pp(annotation);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(annotation).toString());
    }
  }
}
