package fr.irisa.cairn.model.gecos.scop.extractor;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;
import java.util.HashMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class ScopLabelFixer {
  private static boolean VERBOSE = false;
  
  public static String debug(final String mess) {
    String _xifexpression = null;
    if (ScopLabelFixer.VERBOSE) {
      _xifexpression = InputOutput.<String>println(mess);
    }
    return _xifexpression;
  }
  
  public static ScopStatement nextInBlock(final ScopStatement s) {
    Object _xblockexpression = null;
    {
      final ScopNode blk = s.getParentScop();
      if ((blk instanceof ScopBlock)) {
        final int pos = ((ScopBlock)blk).getChildren().indexOf(s);
        int _size = ((ScopBlock)blk).getChildren().size();
        boolean _lessThan = (pos < _size);
        if (_lessThan) {
          final ScopNode next = ((ScopBlock)blk).getChildren().get((pos + 1));
          if ((next instanceof ScopStatement)) {
            return ((ScopStatement)next);
          }
        }
      }
      _xblockexpression = null;
    }
    return ((ScopStatement)_xblockexpression);
  }
  
  public static void fixLabels(final GecosScopBlock root) {
    HashMap<String, ScopStatement> map = new HashMap<String, ScopStatement>();
    final EList<ScopStatement> stmts = root.listAllStatements();
    int pos = 0;
    for (final ScopStatement s : stmts) {
      {
        int _plusPlus = pos++;
        String _plus = ("S" + Integer.valueOf(_plusPlus));
        s.setId(_plus);
        map.put(s.getId(), s);
      }
    }
    Iterable<ScopInstructionStatement> _filter = Iterables.<ScopInstructionStatement>filter(stmts, ScopInstructionStatement.class);
    for (final ScopInstructionStatement s_1 : _filter) {
      {
        final Instruction instr = s_1.getChildren().get(0);
        boolean _matched = false;
        if (instr instanceof LabelInstruction) {
          _matched=true;
          final ScopStatement next = ScopLabelFixer.nextInBlock(s_1);
          if ((next != null)) {
            String _name = ((LabelInstruction)instr).getName();
            String _plus = ("Found Label instruction " + _name);
            String _plus_1 = (_plus + " to be associated to ");
            String _plus_2 = (_plus_1 + next);
            ScopLabelFixer.debug(_plus_2);
            String newName = ((LabelInstruction)instr).getName();
            if ((map.containsKey(newName) && (!Objects.equal(next, map.get(newName))))) {
              final ScopStatement stmt = map.get(newName);
              String _name_1 = ((LabelInstruction)instr).getName();
              String _plus_3 = ("\tLabel " + _name_1);
              String _plus_4 = (_plus_3 + " is already used by ");
              String _plus_5 = (_plus_4 + stmt);
              ScopLabelFixer.debug(_plus_5);
              map.remove(stmt.getId());
              ScopLabelFixer.debug(("\tRenaming " + stmt));
              String _id = stmt.getId();
              String _plus_6 = ("_" + _id);
              stmt.setId(_plus_6);
              while (map.containsKey(stmt.getId())) {
                String _id_1 = stmt.getId();
                String _plus_7 = ("_" + _id_1);
                stmt.setId(_plus_7);
              }
              map.put(stmt.getId(), stmt);
              ScopLabelFixer.debug(("\tInto " + stmt));
            }
            ScopLabelFixer.debug(("\tRenaming " + next));
            map.remove(next.getId());
            next.setId(newName);
            ScopLabelFixer.debug(("\tInto " + next));
            map.put(newName, next);
          }
          s_1.getParentScop().remove(s_1);
          String _name_2 = ((LabelInstruction)instr).getName();
          String _plus_7 = ("Removing LabelInstruction " + _name_2);
          String _plus_8 = (_plus_7 + " to be associated to ");
          String _plus_9 = (_plus_8 + s_1);
          ScopLabelFixer.debug(_plus_9);
        }
      }
    }
    EList<ScopStatement> _listAllStatements = root.listAllStatements();
    for (final ScopStatement s_2 : _listAllStatements) {
      ScopLabelFixer.debug(("Statement " + s_2));
    }
  }
}
