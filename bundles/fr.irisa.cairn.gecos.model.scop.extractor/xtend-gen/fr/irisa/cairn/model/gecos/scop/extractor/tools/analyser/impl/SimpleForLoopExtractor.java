package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.analysis.forloops.ModelNormalForInformation;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.IAnnotation;
import gecos.blocks.ForBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class SimpleForLoopExtractor {
  private GecosProject p;
  
  public SimpleForLoopExtractor(final GecosProject p) {
    this.p = p;
  }
  
  public void compute() {
    final EList<ForBlock> loops = EMFUtils.<ForBlock>eAllContentsFirstInstancesOf(this.p, ForBlock.class);
    for (final ForBlock l : loops) {
      {
        SimpleForBlock sfor = null;
        try {
          final ModelNormalForInformation analyzer = new ModelNormalForInformation(l);
          final Instruction lb = analyzer.getStartValue();
          final Instruction step = analyzer.getStepValue();
          final Instruction ub = analyzer.getStopValue();
          final Symbol iterator = analyzer.getIterationIndex();
          if (((((lb != null) && (ub != null)) && (iterator != null)) && (step != null))) {
            InputOutput.<String>print(("Replacing " + l));
            sfor = GecosUserBlockFactory.SimpleFor(l, iterator, lb, ub, step, GecosUserCoreFactory.scope());
            EMap<String, IAnnotation> _annotations = sfor.getAnnotations();
            EMap<String, IAnnotation> _annotations_1 = l.getAnnotations();
            Iterables.<Map.Entry<String, IAnnotation>>addAll(_annotations, _annotations_1);
            InputOutput.<String>println((" by " + sfor));
            l.getParent().replace(l, sfor);
          }
        } catch (final Throwable _t) {
          if (_t instanceof NullPointerException) {
            InputOutput.<String>println("not simple for block");
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
      }
    }
  }
}
