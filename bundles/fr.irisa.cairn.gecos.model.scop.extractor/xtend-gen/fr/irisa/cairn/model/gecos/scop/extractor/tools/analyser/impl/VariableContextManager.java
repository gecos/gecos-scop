/**
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 * Contributors:
 * DERRIEN Steven - initial API and implementation
 * MORVAN Antoine - initial API and implementation
 * NAULLET Maxime - initial API and implementation
 */
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopParameter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import gecos.core.Symbol;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class VariableContextManager {
  public static boolean VERBOSE = ScopExtractorPass.VERBOSE;
  
  private Map<Symbol, ScopDimension> varMap;
  
  private Map<Symbol, ScopParameter> parMap;
  
  private Map<Integer, ScopDimension> index;
  
  private Map<Symbol, Boolean> visible;
  
  public String debug(final String mess) {
    String _xifexpression = null;
    if (VariableContextManager.VERBOSE) {
      _xifexpression = InputOutput.<String>print(("[VariableContextManager] " + mess));
    }
    return _xifexpression;
  }
  
  public VariableContextManager() {
    HashMap<Symbol, ScopDimension> _hashMap = new HashMap<Symbol, ScopDimension>();
    this.varMap = _hashMap;
    HashMap<Symbol, ScopParameter> _hashMap_1 = new HashMap<Symbol, ScopParameter>();
    this.parMap = _hashMap_1;
    HashMap<Integer, ScopDimension> _hashMap_2 = new HashMap<Integer, ScopDimension>();
    this.index = _hashMap_2;
    HashMap<Symbol, Boolean> _hashMap_3 = new HashMap<Symbol, Boolean>();
    this.visible = _hashMap_3;
  }
  
  public void activateIterators(final List<Symbol> ls, final int dimension) {
    Set<Symbol> _keySet = this.varMap.keySet();
    for (final Symbol s : _keySet) {
      this.setVisibility(s, false);
    }
    for (final Symbol s_1 : ls) {
      {
        boolean _containsKey = this.varMap.containsKey(s_1);
        boolean _not = (!_containsKey);
        if (_not) {
          ScopDimension i = ScopUserFactory.dimSym(s_1);
          this.index.put(Integer.valueOf(dimension), i);
          this.varMap.put(s_1, i);
        }
        this.setVisibility(s_1, true);
      }
    }
  }
  
  public void removeIterator(final Symbol iterator) {
    this.visible.remove(iterator);
  }
  
  public void removeIterators(final List<Symbol> iterators) {
    for (final Symbol s : iterators) {
      this.removeIterator(s);
    }
  }
  
  public void addParameters(final List<Symbol> symbols) {
    for (final Symbol symbol : symbols) {
      boolean _containsKey = this.parMap.containsKey(symbol);
      if (_containsKey) {
        throw new RuntimeException("Error");
      } else {
        if ((symbol != null)) {
          ScopParameter gscopParameter = ScopUserFactory.param(symbol);
          this.parMap.put(symbol, gscopParameter);
        } else {
          throw new RuntimeException("Something really went wrong here");
        }
      }
    }
  }
  
  static int existentialId = 0;
  
  public void setVisibility(final Symbol s, final boolean b) {
    this.visible.put(s, Boolean.valueOf(b));
  }
  
  public void makeAllSymbolVisible() {
    Set<Map.Entry<Symbol, Boolean>> _entrySet = this.visible.entrySet();
    for (final Map.Entry<Symbol, Boolean> entry : _entrySet) {
      entry.setValue(Boolean.valueOf(true));
    }
  }
  
  public ScopDimension lookUp(final Symbol a) {
    boolean _containsKey = this.varMap.containsKey(a);
    if (_containsKey) {
      Boolean _get = this.visible.get(a);
      if ((_get).booleanValue()) {
        return this.varMap.get(a);
      } else {
        if (VariableContextManager.VERBOSE) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("-Symbol ");
          _builder.append(a);
          _builder.append(" is not an iterator at this program level");
          System.out.println(_builder);
        }
      }
    } else {
      boolean _containsKey_1 = this.parMap.containsKey(a);
      if (_containsKey_1) {
        return this.parMap.get(a);
      }
    }
    return null;
  }
  
  public Collection<ScopDimension> getAllIterators() {
    return this.varMap.values();
  }
  
  public Collection<ScopParameter> getAllParameters() {
    return this.parMap.values();
  }
}
