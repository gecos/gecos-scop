/**
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 * Contributors:
 * DERRIEN Steven - initial API and implementation
 * MORVAN Antoine - initial API and implementation
 * NAULLET Maxime - initial API and implementation
 */
package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.LoopCounterAnalyzer;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ParametersAnalyzer;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;
import java.util.List;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import tom.library.sl.VisitFailure;

@SuppressWarnings({ "all" })
public abstract class BaseLinearizer extends BlocksDefaultSwitch {
  protected VariableContextManager context;
  
  protected LoopCounterAnalyzer loopCounterAnalyzer;
  
  protected List<Symbol> parameters;
  
  protected List<Symbol> iterators;
  
  protected int dimension;
  
  protected final static boolean VERBOSE = true;
  
  protected static boolean touched = false;
  
  protected static void debug(final String mess) {
    if (BaseLinearizer.VERBOSE) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("[Linearizer] ");
      _builder.append(mess);
      System.out.println(_builder.toString());
    }
  }
  
  public BaseLinearizer() {
    this.dimension = (-1);
  }
  
  public VariableContextManager transform(final Block b) {
    ParametersAnalyzer paramAnalyzer = new ParametersAnalyzer();
    this.parameters = paramAnalyzer.findParametersIn(b);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Linearizing expressions in block ");
    _builder.append(b);
    _builder.append(" with parameters ");
    _builder.append(this.parameters);
    BaseLinearizer.debug(_builder.toString());
    BasicEList<Symbol> _basicEList = new BasicEList<Symbol>();
    this.iterators = _basicEList;
    LoopCounterAnalyzer _loopCounterAnalyzer = new LoopCounterAnalyzer(b);
    this.loopCounterAnalyzer = _loopCounterAnalyzer;
    VariableContextManager _variableContextManager = new VariableContextManager();
    this.context = _variableContextManager;
    this.context.addParameters(this.parameters);
    this.doSwitch(b);
    return this.context;
  }
  
  @Override
  public Object caseBasicBlock(final BasicBlock b) {
    for (int i = 0; (i < b.getInstructionCount()); i++) {
      {
        Instruction orig = b.getInstruction(i);
        Instruction trans = null;
        try {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Linearizing ");
          _builder.append(orig);
          BaseLinearizer.debug(_builder.toString());
          BaseLinearizer.touched = false;
          Instruction copyAnnotatedElement = ScopExtractorPass.<Instruction>copyAnnotatedElement(orig);
          trans = this.apply(copyAnnotatedElement);
          if (BaseLinearizer.touched) {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("Replacing ");
            _builder_1.append(orig);
            _builder_1.append(" by ");
            _builder_1.append(trans);
            BaseLinearizer.debug(_builder_1.toString());
            orig.substituteWith(trans);
          }
        } catch (final Throwable _t) {
          if (_t instanceof VisitFailure) {
            final VisitFailure e = (VisitFailure)_t;
            e.printStackTrace();
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("Couln\'t linearize ");
            _builder_2.append(orig);
            _builder_2.append(" : ");
            String _localizedMessage = e.getLocalizedMessage();
            _builder_2.append(_localizedMessage);
            String _string = _builder_2.toString();
            throw new RuntimeException(_string);
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
      }
    }
    return Boolean.valueOf(true);
  }
  
  @Override
  public Object caseForBlock(final ForBlock b) {
    try {
      this.iterators = this.loopCounterAnalyzer.findSurroundingLoopCounters(b);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Entering forloop ");
      _builder.append(b);
      _builder.append(" iterators = ");
      _builder.append(this.iterators);
      BaseLinearizer.debug(_builder.toString());
      this.context.activateIterators(this.iterators, this.dimension);
      super.caseForBlock(b);
      if ((b instanceof SimpleForBlock)) {
        this.context.removeIterator(((SimpleForBlock) b).getIterator());
      }
      return Boolean.valueOf(true);
    } catch (final Throwable _t) {
      if (_t instanceof VisitFailure) {
        final VisitFailure e = (VisitFailure)_t;
        e.printStackTrace();
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Couln\'t find iterators in ");
        _builder_1.append(b);
        _builder_1.append(" : ");
        String _localizedMessage = e.getLocalizedMessage();
        _builder_1.append(_localizedMessage);
        String _string = _builder_1.toString();
        throw new RuntimeException(_string);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  @Override
  public Object caseSimpleForBlock(final SimpleForBlock b) {
    this.dimension++;
    Object object = this.caseForBlock(b);
    try {
      b.setLb(this.apply(b.getLb().copy()));
      b.setUb(this.apply(b.getUb().copy()));
      b.setStride(this.apply(b.getStride().copy()));
    } catch (final Throwable _t) {
      if (_t instanceof VisitFailure) {
        final VisitFailure e = (VisitFailure)_t;
        e.printStackTrace();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Couln\'t linearize  : ");
        String _localizedMessage = e.getLocalizedMessage();
        _builder.append(_localizedMessage);
        String _string = _builder.toString();
        throw new RuntimeException(_string);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    this.dimension--;
    return object;
  }
  
  public Object caseBlock(final Block b) {
    return Boolean.valueOf(true);
  }
  
  protected static void checkNoNull(final Object expr) {
    if ((expr == null)) {
      throw new NullPointerException();
    }
  }
  
  protected Instruction apply(final Instruction instruction) throws VisitFailure {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Linearizing ");
      String _simpleName = instruction.getClass().getSimpleName();
      _builder.append(_simpleName);
      _builder.append(":");
      _builder.append(instruction);
      _builder.append(" using ");
      _builder.append(this.iterators);
      _builder.append(" and ");
      _builder.append(this.parameters);
      BaseLinearizer.debug(
        _builder.toString());
      Instruction res = this.tom_rewrite(instruction);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("-> ");
      String _simpleName_1 = res.getClass().getSimpleName();
      _builder_1.append(_simpleName_1);
      _builder_1.append(":");
      _builder_1.append(res);
      BaseLinearizer.debug(_builder_1.toString());
      BaseLinearizer.checkNoNull(res);
      return res;
    } catch (final Throwable _t) {
      if (_t instanceof RuntimeException) {
        final RuntimeException e = (RuntimeException)_t;
        e.printStackTrace();
        BasicBlock _basicBlock = instruction.getBasicBlock();
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(instruction);
        _builder_2.append(" is not Scopable due to runtime exception ");
        String _message = e.getMessage();
        _builder_2.append(_message);
        String _string = _builder_2.toString();
        throw new SCoPException(_basicBlock, _string);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  protected abstract Instruction tom_rewrite(final Instruction instruction);
  
  protected static boolean isScalarSymbol(final Instruction s) {
    if ((s instanceof SymbolInstruction)) {
      ComplexInstruction _parent = ((SymbolInstruction)s).getParent();
      if ((_parent instanceof ArrayInstruction)) {
        ComplexInstruction _parent_1 = ((SymbolInstruction)s).getParent();
        ArrayInstruction array = ((ArrayInstruction) _parent_1);
        Instruction _dest = array.getDest();
        boolean _tripleEquals = (_dest == s);
        if (_tripleEquals) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}
