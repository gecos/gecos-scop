package fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.model.gecos.scop.extractor.exception.SCoPException;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.LoopCounterAnalyzer;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.ScopDomainAnalyser;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.impl.VariableContextManager;
import fr.irisa.cairn.model.gecos.scop.extractor.tools.analyser.tom.AbstractScopDomainAnalyser;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.SimpleForBlock;
import gecos.core.Scope;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.SetInstruction;
import java.util.List;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import tom.library.sl.VisitFailure;

@SuppressWarnings("all")
public abstract class ScopDomainBuilder extends AbstractScopDomainAnalyser {
  protected final static boolean VERBOSE = true;
  
  public static void debug(final String mess) {
    if (ScopDomainBuilder.VERBOSE) {
      InputOutput.<String>println(("[ScopDomainAnalyzer] " + mess));
    }
  }
  
  public ScopDomainBuilder(final VariableContextManager context) {
    super(context);
  }
  
  protected static LoopCounterAnalyzer loopCounterAnalyzer;
  
  @Override
  protected Block apply(final Block block) {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("- Analyzing domains for ");
      _builder.append(block);
      ScopDomainBuilder.debug(_builder.toString());
      AbstractScopDomainAnalyser.rootBlock = block;
      LoopCounterAnalyzer _loopCounterAnalyzer = new LoopCounterAnalyzer(block);
      ScopDomainBuilder.loopCounterAnalyzer = _loopCounterAnalyzer;
      GecosScopBlock root = ScopUserFactory.root();
      root.setScope(GecosUserCoreFactory.scope());
      Block b = this.tom_rewrite(block);
      if ((b instanceof ScopBlock)) {
        root.setRoot(((ScopNode) ((ScopBlock)b)));
      } else {
        ScopNode _get = this.get(b);
        root.setRoot(((ScopNode) _get));
      }
      VariableContextManager ctxt = AbstractScopDomainAnalyser.context;
      root.getIterators().addAll(ctxt.getAllIterators());
      root.getParameters().addAll(ctxt.getAllParameters());
      List<ScopForLoop> children = EMFUtils.<ScopForLoop>eAllContentsInstancesOf(root, ScopForLoop.class);
      boolean _isEmpty = children.isEmpty();
      if (_isEmpty) {
        throw new SCoPException(block, "No for loop in the block");
      }
      return root;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected abstract Block tom_rewrite(final Block block) throws VisitFailure, SCoPException;
  
  protected static ScopInstructionStatement analyzeInstruction(final Instruction instr) {
    if ((instr == null)) {
      BasicBlock _basicBlock = instr.getBasicBlock();
      throw new SCoPException(_basicBlock, "Error in analyzeInstruction (instr==null)");
    }
    ScopInstructionStatement resNode = null;
    if ((instr instanceof ScopInstructionStatement)) {
      resNode = ((ScopInstructionStatement) instr);
    } else {
      if ((instr instanceof CallInstruction)) {
        CallInstruction call = ((CallInstruction) instr);
        PragmaAnnotation pragma = call.getProcedureSymbol().getPragma();
        if (((pragma != null) && pragma.getContent().contains("GCS_PURE_FUNCTION"))) {
          ScopInstructionStatement gScopInstructionStatement = ScopUserFactory.scopStatement("Call", call);
          gScopInstructionStatement.setType(call.getType());
          gScopInstructionStatement.getAnnotations().putAll(call.getAnnotations());
          resNode = gScopInstructionStatement;
        } else {
          BasicBlock _basicBlock_1 = ((CallInstruction)instr).getBasicBlock();
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Function \'");
          _builder.append(call);
          _builder.append("\' is not Scopable ");
          String _string = _builder.toString();
          throw new SCoPException(_basicBlock_1, _string);
        }
      } else {
        if ((instr instanceof LabelInstruction)) {
          LabelInstruction label = ((LabelInstruction) instr);
          ScopInstructionStatement gScopInstructionStatement_1 = ScopUserFactory.scopStatement(label.getName(), instr);
          gScopInstructionStatement_1.setType(((LabelInstruction)instr).getType());
          gScopInstructionStatement_1.getAnnotations().putAll(((LabelInstruction)instr).getAnnotations());
          resNode = gScopInstructionStatement_1;
        }
      }
    }
    if ((resNode == null)) {
      BasicBlock _basicBlock_2 = instr.getBasicBlock();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Error in analyzeInstruction for ");
      _builder_1.append(instr);
      _builder_1.append(":");
      String _simpleName = instr.getClass().getSimpleName();
      _builder_1.append(_simpleName);
      String _string_1 = _builder_1.toString();
      throw new SCoPException(_basicBlock_2, _string_1);
    }
    return resNode;
  }
  
  protected static ScopInstructionStatement ifConvert(final ScopInstructionStatement scopStatement, final Instruction guard) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ifConvert(");
    _builder.append(scopStatement);
    _builder.append(",");
    _builder.append(guard);
    _builder.append(")");
    ScopDomainBuilder.debug(_builder.toString());
    EList<ScopWrite> listAllWriteAccess = scopStatement.listAllWriteAccess();
    Instruction _child = scopStatement.getChild(0);
    if ((_child instanceof LabelInstruction)) {
      return scopStatement;
    } else {
      ScopAccess _copy = listAllWriteAccess.get(0).copy();
      ScopWrite write = ((ScopWrite) _copy);
      BasicEList<IntExpression> index = new BasicEList<IntExpression>();
      EList<IntExpression> _indexExpressions = write.getIndexExpressions();
      for (final IntExpression expr : _indexExpressions) {
        index.add(expr.<IntExpression>copy());
      }
      ScopRead read = ScopUserFactory.scopRead(write.getSym(), index);
      try {
        ScopInstructionStatement guardStmt = ((ScopInstructionStatement) guard);
        Instruction predicate = guardStmt.getChildren().get(0).copy();
        Instruction _get = scopStatement.getChildren().get(0);
        SetInstruction set = ((SetInstruction) _get);
        Instruction _copy_1 = set.getSource().copy();
        Instruction select = GecosUserInstructionFactory.set(write, 
          GecosUserInstructionFactory.generic("mux", write.getType(), new Instruction[] { predicate, _copy_1, read }));
        ScopInstructionStatement ternary = ScopUserFactory.scopStatement("mux", select);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("\t");
        _builder_1.append("=>");
        _builder_1.append(ternary, "\t");
        ScopDomainBuilder.debug(_builder_1.toString());
        return ternary;
      } catch (final Throwable _t) {
        if (_t instanceof ClassCastException) {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("Unsupported instruction pattern ");
          _builder_2.append(scopStatement);
          _builder_2.append(" in if-conversion");
          String _string = _builder_2.toString();
          throw new RuntimeException(_string);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
  }
  
  protected static EList<IntConstraintSystem> analyzeGuard(final List<Instruction> systems) {
    final EList<IntConstraintSystem> res = new BasicEList<IntConstraintSystem>();
    for (final Instruction s : systems) {
      boolean _matched = false;
      if (s instanceof ScopIntConstraintSystem) {
        _matched=true;
        res.add(((ScopIntConstraintSystem)s).getSystem());
      }
      if (!_matched) {
        BasicBlock _basicBlock = s.getBasicBlock();
        throw new SCoPException(_basicBlock, (("If/Else guard \'" + systems) + "\' is not Scopable "));
      }
    }
    return res;
  }
  
  protected static void buildIf(final Block ifBlock, final Instruction guard, final List<Instruction> systems, final Block thenBlock, final Block elseBlock, final ScopDomainAnalyser scopDomainAnalyser) {
    int _size = systems.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      ScopNode thenScopNode = null;
      try {
        thenScopNode = scopDomainAnalyser.get(thenBlock);
      } catch (final Throwable _t) {
        if (_t instanceof SCoPException) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append(" ");
          _builder.append("then statement ");
          _builder.append(thenBlock, " ");
          _builder.append(" is not a Scop !");
          String _string = _builder.toString();
          throw new SCoPException(ifBlock, _string);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
      ScopNode elseScopNode = null;
      if ((elseBlock != null)) {
        elseScopNode = scopDomainAnalyser.get(elseBlock);
      }
      try {
        EList<IntConstraintSystem> predicate = ScopDomainBuilder.analyzeGuard(systems);
        ScopGuard newNode = ScopUserFactory.scopGuard(predicate, thenScopNode, elseScopNode);
        GecosUserAnnotationFactory.copyAnnotations(ifBlock, newNode);
        scopDomainAnalyser.put(ifBlock, newNode);
      } catch (final Throwable _t_1) {
        if (_t_1 instanceof SCoPException) {
          boolean _matched = false;
          if (thenScopNode instanceof ScopStatement) {
            _matched=true;
            ScopDomainBuilder.debug(("Fixing " + thenScopNode));
            throw new SCoPException(ifBlock, (((" data-dependant guard " + systems) + " for statement ") + thenScopNode));
          }
          if (!_matched) {
            if (thenScopNode instanceof ScopBlock) {
              _matched=true;
              ScopDomainBuilder.debug(("Fixing " + thenScopNode));
              int _size_1 = ((ScopBlock)thenScopNode).getChildren().size();
              boolean _equals = (_size_1 == 1);
              if (_equals) {
                final ScopNode child = ((ScopBlock)thenScopNode).getChildren().get(0);
                boolean _matched_1 = false;
                if (child instanceof ScopStatement) {
                  _matched_1=true;
                  throw new SCoPException(ifBlock, (((" data-dependant guard " + systems) + " for statement ") + child));
                }
              }
            }
          }
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(" ");
          _builder_1.append("guard predicate ");
          _builder_1.append(systems, " ");
          _builder_1.append(" is not polyhedral");
          String _string_1 = _builder_1.toString();
          throw new SCoPException(ifBlock, _string_1);
        } else {
          throw Exceptions.sneakyThrow(_t_1);
        }
      }
    }
  }
  
  protected static void extractForInformation(final ScopDomainAnalyser scopDomainAnalyser, final SimpleForBlock simpleForBlock, final ScopDimension variable, final IntExpression lowerBound, final IntExpression upperBound, final IntExpression step, final Block body) {
    if ((variable == null)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("null variable ");
      _builder.append(variable);
      String _string = _builder.toString();
      throw new SCoPException(simpleForBlock, _string);
    }
    if ((lowerBound == null)) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("null lower bound for ");
      _builder_1.append(simpleForBlock);
      String _string_1 = _builder_1.toString();
      throw new SCoPException(simpleForBlock, _string_1);
    }
    if ((upperBound == null)) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("null upper bound for ");
      _builder_2.append(simpleForBlock);
      String _string_2 = _builder_2.toString();
      throw new SCoPException(simpleForBlock, _string_2);
    }
    if ((step == null)) {
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("null stride for ");
      _builder_3.append(simpleForBlock);
      String _string_3 = _builder_3.toString();
      throw new SCoPException(simpleForBlock, _string_3);
    }
    try {
      ScopNode bodyAsScop = scopDomainAnalyser.get(body);
      ScopForLoop loop = ScopUserFactory.scopFor(variable, lowerBound.<IntExpression>copy(), upperBound.<IntExpression>copy(), step.<IntExpression>copy(), bodyAsScop);
      GecosUserAnnotationFactory.copyAnnotations(simpleForBlock, loop);
      final Scope localScope = simpleForBlock.getScope();
      if (((localScope != null) && (!localScope.getSymbols().isEmpty()))) {
        scopDomainAnalyser.put(simpleForBlock, ScopUserFactory.scopBlock(loop));
      } else {
        scopDomainAnalyser.put(simpleForBlock, loop);
      }
    } catch (final Throwable _t) {
      if (_t instanceof SCoPException) {
        final SCoPException e = (SCoPException)_t;
        e.printStackTrace();
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append("Loop body for ");
        _builder_4.append(simpleForBlock);
        _builder_4.append(" is not a Scop !");
        String _string_4 = _builder_4.toString();
        throw new SCoPException(simpleForBlock, _string_4);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
}
