/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.cairn.gecos.model.scop.pragma.ui;

import com.google.inject.Injector;
import fr.irisa.cairn.gecos.model.scop.pragma.ui.internal.PragmaActivator;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class PragmasExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return PragmaActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return PragmaActivator.getInstance().getInjector(PragmaActivator.FR_IRISA_CAIRN_GECOS_MODEL_SCOP_PRAGMA_PRAGMAS);
	}
	
}
