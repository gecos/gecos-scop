package fr.irisa.cairn.gecos.model.scop.pragma.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.gecos.model.scop.pragma.services.PragmasGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPragmasParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INTTOKEN", "RULE_STRINGTOKEN", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'RLT_INSET'", "'gscop_cg_schedule'", "'numbertasks'", "'='", "'fixedtasks'", "','", "'tileSizes'", "'gscop_fg_schedule'", "'gscop_pure_function'", "'omp'", "'parallel'", "'for'", "'private'", "'('", "')'", "'shared'", "'reduction'", "'+'", "':'", "'gscop_unroll'", "'gscop_inline'", "'ignore_memory_dependency'", "'from'", "'to'", "'scop'", "'name'", "'context'", "'scheduling'", "'pluto-isl'", "'feautrier-isl'", "'scop_schedule_statement'", "'scop_schedule_block'", "'scop_cloog_options'", "'equalitySpreading'", "'stop'", "'firstDepthToOptimize'", "'firstDepthSpreading'", "'strides'", "'lastDepthToOptimize'", "'block'", "'constantSpreading'", "'onceTimeLoopElim'", "'coalescingDepth'", "'scop_duplicate'", "'scop_data_motion'", "'scop_slice'", "'sizes'", "'unrolls'", "'scop_merge_arrays'", "'scop_contract_array'", "'pipeline_loop'", "'latency'", "'hash'", "'-'", "'RAW'", "'WAR'", "'WAW'", "'%'", "'/'", "'<'", "'>'", "'>='", "'<='"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int RULE_STRINGTOKEN=6;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=7;
    public static final int RULE_INTTOKEN=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalPragmasParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPragmasParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPragmasParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPragmas.g"; }



     	private PragmasGrammarAccess grammarAccess;

        public InternalPragmasParser(TokenStream input, PragmasGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "AnnotatedProcedureSet";
       	}

       	@Override
       	protected PragmasGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAnnotatedProcedureSet"
    // InternalPragmas.g:65:1: entryRuleAnnotatedProcedureSet returns [EObject current=null] : iv_ruleAnnotatedProcedureSet= ruleAnnotatedProcedureSet EOF ;
    public final EObject entryRuleAnnotatedProcedureSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotatedProcedureSet = null;


        try {
            // InternalPragmas.g:65:62: (iv_ruleAnnotatedProcedureSet= ruleAnnotatedProcedureSet EOF )
            // InternalPragmas.g:66:2: iv_ruleAnnotatedProcedureSet= ruleAnnotatedProcedureSet EOF
            {
             newCompositeNode(grammarAccess.getAnnotatedProcedureSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotatedProcedureSet=ruleAnnotatedProcedureSet();

            state._fsp--;

             current =iv_ruleAnnotatedProcedureSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotatedProcedureSet"


    // $ANTLR start "ruleAnnotatedProcedureSet"
    // InternalPragmas.g:72:1: ruleAnnotatedProcedureSet returns [EObject current=null] : ( (lv_pragmas_0_0= rulePragmaAnnotation ) ) ;
    public final EObject ruleAnnotatedProcedureSet() throws RecognitionException {
        EObject current = null;

        EObject lv_pragmas_0_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:78:2: ( ( (lv_pragmas_0_0= rulePragmaAnnotation ) ) )
            // InternalPragmas.g:79:2: ( (lv_pragmas_0_0= rulePragmaAnnotation ) )
            {
            // InternalPragmas.g:79:2: ( (lv_pragmas_0_0= rulePragmaAnnotation ) )
            // InternalPragmas.g:80:3: (lv_pragmas_0_0= rulePragmaAnnotation )
            {
            // InternalPragmas.g:80:3: (lv_pragmas_0_0= rulePragmaAnnotation )
            // InternalPragmas.g:81:4: lv_pragmas_0_0= rulePragmaAnnotation
            {

            				newCompositeNode(grammarAccess.getAnnotatedProcedureSetAccess().getPragmasPragmaAnnotationParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_pragmas_0_0=rulePragmaAnnotation();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getAnnotatedProcedureSetRule());
            				}
            				add(
            					current,
            					"pragmas",
            					lv_pragmas_0_0,
            					"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.PragmaAnnotation");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotatedProcedureSet"


    // $ANTLR start "entryRulePragmaAnnotation"
    // InternalPragmas.g:101:1: entryRulePragmaAnnotation returns [EObject current=null] : iv_rulePragmaAnnotation= rulePragmaAnnotation EOF ;
    public final EObject entryRulePragmaAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePragmaAnnotation = null;


        try {
            // InternalPragmas.g:101:57: (iv_rulePragmaAnnotation= rulePragmaAnnotation EOF )
            // InternalPragmas.g:102:2: iv_rulePragmaAnnotation= rulePragmaAnnotation EOF
            {
             newCompositeNode(grammarAccess.getPragmaAnnotationRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePragmaAnnotation=rulePragmaAnnotation();

            state._fsp--;

             current =iv_rulePragmaAnnotation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePragmaAnnotation"


    // $ANTLR start "rulePragmaAnnotation"
    // InternalPragmas.g:108:1: rulePragmaAnnotation returns [EObject current=null] : ( () ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+ ) ;
    public final EObject rulePragmaAnnotation() throws RecognitionException {
        EObject current = null;

        EObject lv_directives_1_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:114:2: ( ( () ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+ ) )
            // InternalPragmas.g:115:2: ( () ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+ )
            {
            // InternalPragmas.g:115:2: ( () ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+ )
            // InternalPragmas.g:116:3: () ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+
            {
            // InternalPragmas.g:116:3: ()
            // InternalPragmas.g:117:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPragmaAnnotationAccess().getS2S4HLSPragmaAnnotationAction_0(),
            					current);
            			

            }

            // InternalPragmas.g:123:3: ( (lv_directives_1_0= ruleS2S4HLSDirective ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=12 && LA1_0<=13)||(LA1_0>=19 && LA1_0<=21)||(LA1_0>=31 && LA1_0<=33)||LA1_0==36||(LA1_0>=42 && LA1_0<=44)||(LA1_0>=55 && LA1_0<=57)||(LA1_0>=60 && LA1_0<=62)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPragmas.g:124:4: (lv_directives_1_0= ruleS2S4HLSDirective )
            	    {
            	    // InternalPragmas.g:124:4: (lv_directives_1_0= ruleS2S4HLSDirective )
            	    // InternalPragmas.g:125:5: lv_directives_1_0= ruleS2S4HLSDirective
            	    {

            	    					newCompositeNode(grammarAccess.getPragmaAnnotationAccess().getDirectivesS2S4HLSDirectiveParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_directives_1_0=ruleS2S4HLSDirective();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPragmaAnnotationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"directives",
            	    						lv_directives_1_0,
            	    						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.S2S4HLSDirective");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePragmaAnnotation"


    // $ANTLR start "entryRuleS2S4HLSDirective"
    // InternalPragmas.g:146:1: entryRuleS2S4HLSDirective returns [EObject current=null] : iv_ruleS2S4HLSDirective= ruleS2S4HLSDirective EOF ;
    public final EObject entryRuleS2S4HLSDirective() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleS2S4HLSDirective = null;


        try {
            // InternalPragmas.g:146:57: (iv_ruleS2S4HLSDirective= ruleS2S4HLSDirective EOF )
            // InternalPragmas.g:147:2: iv_ruleS2S4HLSDirective= ruleS2S4HLSDirective EOF
            {
             newCompositeNode(grammarAccess.getS2S4HLSDirectiveRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleS2S4HLSDirective=ruleS2S4HLSDirective();

            state._fsp--;

             current =iv_ruleS2S4HLSDirective; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleS2S4HLSDirective"


    // $ANTLR start "ruleS2S4HLSDirective"
    // InternalPragmas.g:153:1: ruleS2S4HLSDirective returns [EObject current=null] : (this_PureFunctionPragma_0= rulePureFunctionPragma | this_ScheduleStatementPragma_1= ruleScheduleStatementPragma | this_ScheduleBlockPragma_2= ruleScheduleBlockPragma | this_ScheduleContextPragma_3= ruleScheduleContextPragma | this_CloogOptionPragma_4= ruleCloogOptionPragma | this_UnrollPragma_5= ruleUnrollPragma | this_InlinePragma_6= ruleInlinePragma | this_openMpPragma_7= ruleopenMpPragma | this_IgnoreMemoryDep_8= ruleIgnoreMemoryDep | this_DuplicateNodePragma_9= ruleDuplicateNodePragma | this_DataMotionPragma_10= ruleDataMotionPragma | this_SlicePragma_11= ruleSlicePragma | this_MergeArraysPragma_12= ruleMergeArraysPragma | this_ArrayContractPragma_13= ruleArrayContractPragma | this_LoopPipelinePragma_14= ruleLoopPipelinePragma | this_RLTInsetPragma_15= ruleRLTInsetPragma | this_ALMACoarseGrainPragma_16= ruleALMACoarseGrainPragma | this_ALMAFineGrainPragma_17= ruleALMAFineGrainPragma ) ;
    public final EObject ruleS2S4HLSDirective() throws RecognitionException {
        EObject current = null;

        EObject this_PureFunctionPragma_0 = null;

        EObject this_ScheduleStatementPragma_1 = null;

        EObject this_ScheduleBlockPragma_2 = null;

        EObject this_ScheduleContextPragma_3 = null;

        EObject this_CloogOptionPragma_4 = null;

        EObject this_UnrollPragma_5 = null;

        EObject this_InlinePragma_6 = null;

        EObject this_openMpPragma_7 = null;

        EObject this_IgnoreMemoryDep_8 = null;

        EObject this_DuplicateNodePragma_9 = null;

        EObject this_DataMotionPragma_10 = null;

        EObject this_SlicePragma_11 = null;

        EObject this_MergeArraysPragma_12 = null;

        EObject this_ArrayContractPragma_13 = null;

        EObject this_LoopPipelinePragma_14 = null;

        EObject this_RLTInsetPragma_15 = null;

        EObject this_ALMACoarseGrainPragma_16 = null;

        EObject this_ALMAFineGrainPragma_17 = null;



        	enterRule();

        try {
            // InternalPragmas.g:159:2: ( (this_PureFunctionPragma_0= rulePureFunctionPragma | this_ScheduleStatementPragma_1= ruleScheduleStatementPragma | this_ScheduleBlockPragma_2= ruleScheduleBlockPragma | this_ScheduleContextPragma_3= ruleScheduleContextPragma | this_CloogOptionPragma_4= ruleCloogOptionPragma | this_UnrollPragma_5= ruleUnrollPragma | this_InlinePragma_6= ruleInlinePragma | this_openMpPragma_7= ruleopenMpPragma | this_IgnoreMemoryDep_8= ruleIgnoreMemoryDep | this_DuplicateNodePragma_9= ruleDuplicateNodePragma | this_DataMotionPragma_10= ruleDataMotionPragma | this_SlicePragma_11= ruleSlicePragma | this_MergeArraysPragma_12= ruleMergeArraysPragma | this_ArrayContractPragma_13= ruleArrayContractPragma | this_LoopPipelinePragma_14= ruleLoopPipelinePragma | this_RLTInsetPragma_15= ruleRLTInsetPragma | this_ALMACoarseGrainPragma_16= ruleALMACoarseGrainPragma | this_ALMAFineGrainPragma_17= ruleALMAFineGrainPragma ) )
            // InternalPragmas.g:160:2: (this_PureFunctionPragma_0= rulePureFunctionPragma | this_ScheduleStatementPragma_1= ruleScheduleStatementPragma | this_ScheduleBlockPragma_2= ruleScheduleBlockPragma | this_ScheduleContextPragma_3= ruleScheduleContextPragma | this_CloogOptionPragma_4= ruleCloogOptionPragma | this_UnrollPragma_5= ruleUnrollPragma | this_InlinePragma_6= ruleInlinePragma | this_openMpPragma_7= ruleopenMpPragma | this_IgnoreMemoryDep_8= ruleIgnoreMemoryDep | this_DuplicateNodePragma_9= ruleDuplicateNodePragma | this_DataMotionPragma_10= ruleDataMotionPragma | this_SlicePragma_11= ruleSlicePragma | this_MergeArraysPragma_12= ruleMergeArraysPragma | this_ArrayContractPragma_13= ruleArrayContractPragma | this_LoopPipelinePragma_14= ruleLoopPipelinePragma | this_RLTInsetPragma_15= ruleRLTInsetPragma | this_ALMACoarseGrainPragma_16= ruleALMACoarseGrainPragma | this_ALMAFineGrainPragma_17= ruleALMAFineGrainPragma )
            {
            // InternalPragmas.g:160:2: (this_PureFunctionPragma_0= rulePureFunctionPragma | this_ScheduleStatementPragma_1= ruleScheduleStatementPragma | this_ScheduleBlockPragma_2= ruleScheduleBlockPragma | this_ScheduleContextPragma_3= ruleScheduleContextPragma | this_CloogOptionPragma_4= ruleCloogOptionPragma | this_UnrollPragma_5= ruleUnrollPragma | this_InlinePragma_6= ruleInlinePragma | this_openMpPragma_7= ruleopenMpPragma | this_IgnoreMemoryDep_8= ruleIgnoreMemoryDep | this_DuplicateNodePragma_9= ruleDuplicateNodePragma | this_DataMotionPragma_10= ruleDataMotionPragma | this_SlicePragma_11= ruleSlicePragma | this_MergeArraysPragma_12= ruleMergeArraysPragma | this_ArrayContractPragma_13= ruleArrayContractPragma | this_LoopPipelinePragma_14= ruleLoopPipelinePragma | this_RLTInsetPragma_15= ruleRLTInsetPragma | this_ALMACoarseGrainPragma_16= ruleALMACoarseGrainPragma | this_ALMAFineGrainPragma_17= ruleALMAFineGrainPragma )
            int alt2=18;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt2=1;
                }
                break;
            case 42:
                {
                alt2=2;
                }
                break;
            case 43:
                {
                alt2=3;
                }
                break;
            case 36:
                {
                alt2=4;
                }
                break;
            case 44:
                {
                alt2=5;
                }
                break;
            case 31:
                {
                alt2=6;
                }
                break;
            case 32:
                {
                alt2=7;
                }
                break;
            case 21:
                {
                alt2=8;
                }
                break;
            case 33:
                {
                alt2=9;
                }
                break;
            case 55:
                {
                alt2=10;
                }
                break;
            case 56:
                {
                alt2=11;
                }
                break;
            case 57:
                {
                alt2=12;
                }
                break;
            case 60:
                {
                alt2=13;
                }
                break;
            case 61:
                {
                alt2=14;
                }
                break;
            case 62:
                {
                alt2=15;
                }
                break;
            case 12:
                {
                alt2=16;
                }
                break;
            case 13:
                {
                alt2=17;
                }
                break;
            case 19:
                {
                alt2=18;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalPragmas.g:161:3: this_PureFunctionPragma_0= rulePureFunctionPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getPureFunctionPragmaParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_PureFunctionPragma_0=rulePureFunctionPragma();

                    state._fsp--;


                    			current = this_PureFunctionPragma_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPragmas.g:170:3: this_ScheduleStatementPragma_1= ruleScheduleStatementPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getScheduleStatementPragmaParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ScheduleStatementPragma_1=ruleScheduleStatementPragma();

                    state._fsp--;


                    			current = this_ScheduleStatementPragma_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPragmas.g:179:3: this_ScheduleBlockPragma_2= ruleScheduleBlockPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getScheduleBlockPragmaParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ScheduleBlockPragma_2=ruleScheduleBlockPragma();

                    state._fsp--;


                    			current = this_ScheduleBlockPragma_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPragmas.g:188:3: this_ScheduleContextPragma_3= ruleScheduleContextPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getScheduleContextPragmaParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ScheduleContextPragma_3=ruleScheduleContextPragma();

                    state._fsp--;


                    			current = this_ScheduleContextPragma_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalPragmas.g:197:3: this_CloogOptionPragma_4= ruleCloogOptionPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getCloogOptionPragmaParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_CloogOptionPragma_4=ruleCloogOptionPragma();

                    state._fsp--;


                    			current = this_CloogOptionPragma_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalPragmas.g:206:3: this_UnrollPragma_5= ruleUnrollPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getUnrollPragmaParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_UnrollPragma_5=ruleUnrollPragma();

                    state._fsp--;


                    			current = this_UnrollPragma_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalPragmas.g:215:3: this_InlinePragma_6= ruleInlinePragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getInlinePragmaParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_InlinePragma_6=ruleInlinePragma();

                    state._fsp--;


                    			current = this_InlinePragma_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalPragmas.g:224:3: this_openMpPragma_7= ruleopenMpPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getOpenMpPragmaParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_openMpPragma_7=ruleopenMpPragma();

                    state._fsp--;


                    			current = this_openMpPragma_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalPragmas.g:233:3: this_IgnoreMemoryDep_8= ruleIgnoreMemoryDep
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getIgnoreMemoryDepParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_IgnoreMemoryDep_8=ruleIgnoreMemoryDep();

                    state._fsp--;


                    			current = this_IgnoreMemoryDep_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 10 :
                    // InternalPragmas.g:242:3: this_DuplicateNodePragma_9= ruleDuplicateNodePragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getDuplicateNodePragmaParserRuleCall_9());
                    		
                    pushFollow(FOLLOW_2);
                    this_DuplicateNodePragma_9=ruleDuplicateNodePragma();

                    state._fsp--;


                    			current = this_DuplicateNodePragma_9;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 11 :
                    // InternalPragmas.g:251:3: this_DataMotionPragma_10= ruleDataMotionPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getDataMotionPragmaParserRuleCall_10());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataMotionPragma_10=ruleDataMotionPragma();

                    state._fsp--;


                    			current = this_DataMotionPragma_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 12 :
                    // InternalPragmas.g:260:3: this_SlicePragma_11= ruleSlicePragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getSlicePragmaParserRuleCall_11());
                    		
                    pushFollow(FOLLOW_2);
                    this_SlicePragma_11=ruleSlicePragma();

                    state._fsp--;


                    			current = this_SlicePragma_11;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 13 :
                    // InternalPragmas.g:269:3: this_MergeArraysPragma_12= ruleMergeArraysPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getMergeArraysPragmaParserRuleCall_12());
                    		
                    pushFollow(FOLLOW_2);
                    this_MergeArraysPragma_12=ruleMergeArraysPragma();

                    state._fsp--;


                    			current = this_MergeArraysPragma_12;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 14 :
                    // InternalPragmas.g:278:3: this_ArrayContractPragma_13= ruleArrayContractPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getArrayContractPragmaParserRuleCall_13());
                    		
                    pushFollow(FOLLOW_2);
                    this_ArrayContractPragma_13=ruleArrayContractPragma();

                    state._fsp--;


                    			current = this_ArrayContractPragma_13;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 15 :
                    // InternalPragmas.g:287:3: this_LoopPipelinePragma_14= ruleLoopPipelinePragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getLoopPipelinePragmaParserRuleCall_14());
                    		
                    pushFollow(FOLLOW_2);
                    this_LoopPipelinePragma_14=ruleLoopPipelinePragma();

                    state._fsp--;


                    			current = this_LoopPipelinePragma_14;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 16 :
                    // InternalPragmas.g:296:3: this_RLTInsetPragma_15= ruleRLTInsetPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getRLTInsetPragmaParserRuleCall_15());
                    		
                    pushFollow(FOLLOW_2);
                    this_RLTInsetPragma_15=ruleRLTInsetPragma();

                    state._fsp--;


                    			current = this_RLTInsetPragma_15;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 17 :
                    // InternalPragmas.g:305:3: this_ALMACoarseGrainPragma_16= ruleALMACoarseGrainPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getALMACoarseGrainPragmaParserRuleCall_16());
                    		
                    pushFollow(FOLLOW_2);
                    this_ALMACoarseGrainPragma_16=ruleALMACoarseGrainPragma();

                    state._fsp--;


                    			current = this_ALMACoarseGrainPragma_16;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 18 :
                    // InternalPragmas.g:314:3: this_ALMAFineGrainPragma_17= ruleALMAFineGrainPragma
                    {

                    			newCompositeNode(grammarAccess.getS2S4HLSDirectiveAccess().getALMAFineGrainPragmaParserRuleCall_17());
                    		
                    pushFollow(FOLLOW_2);
                    this_ALMAFineGrainPragma_17=ruleALMAFineGrainPragma();

                    state._fsp--;


                    			current = this_ALMAFineGrainPragma_17;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleS2S4HLSDirective"


    // $ANTLR start "entryRuleRLTInsetPragma"
    // InternalPragmas.g:326:1: entryRuleRLTInsetPragma returns [EObject current=null] : iv_ruleRLTInsetPragma= ruleRLTInsetPragma EOF ;
    public final EObject entryRuleRLTInsetPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRLTInsetPragma = null;


        try {
            // InternalPragmas.g:326:55: (iv_ruleRLTInsetPragma= ruleRLTInsetPragma EOF )
            // InternalPragmas.g:327:2: iv_ruleRLTInsetPragma= ruleRLTInsetPragma EOF
            {
             newCompositeNode(grammarAccess.getRLTInsetPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRLTInsetPragma=ruleRLTInsetPragma();

            state._fsp--;

             current =iv_ruleRLTInsetPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRLTInsetPragma"


    // $ANTLR start "ruleRLTInsetPragma"
    // InternalPragmas.g:333:1: ruleRLTInsetPragma returns [EObject current=null] : otherlv_0= 'RLT_INSET' ;
    public final EObject ruleRLTInsetPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalPragmas.g:339:2: (otherlv_0= 'RLT_INSET' )
            // InternalPragmas.g:340:2: otherlv_0= 'RLT_INSET'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getRLTInsetPragmaAccess().getRLT_INSETKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRLTInsetPragma"


    // $ANTLR start "entryRuleALMACoarseGrainPragma"
    // InternalPragmas.g:347:1: entryRuleALMACoarseGrainPragma returns [EObject current=null] : iv_ruleALMACoarseGrainPragma= ruleALMACoarseGrainPragma EOF ;
    public final EObject entryRuleALMACoarseGrainPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleALMACoarseGrainPragma = null;


        try {
            // InternalPragmas.g:347:62: (iv_ruleALMACoarseGrainPragma= ruleALMACoarseGrainPragma EOF )
            // InternalPragmas.g:348:2: iv_ruleALMACoarseGrainPragma= ruleALMACoarseGrainPragma EOF
            {
             newCompositeNode(grammarAccess.getALMACoarseGrainPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleALMACoarseGrainPragma=ruleALMACoarseGrainPragma();

            state._fsp--;

             current =iv_ruleALMACoarseGrainPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleALMACoarseGrainPragma"


    // $ANTLR start "ruleALMACoarseGrainPragma"
    // InternalPragmas.g:354:1: ruleALMACoarseGrainPragma returns [EObject current=null] : (otherlv_0= 'gscop_cg_schedule' (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )? (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )? (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )? ) ;
    public final EObject ruleALMACoarseGrainPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_nbTasks_3_0 = null;

        AntlrDatatypeRuleToken lv_taskIds_6_0 = null;

        AntlrDatatypeRuleToken lv_taskIds_8_0 = null;

        AntlrDatatypeRuleToken lv_tileSizes_11_0 = null;

        AntlrDatatypeRuleToken lv_tileSizes_13_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:360:2: ( (otherlv_0= 'gscop_cg_schedule' (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )? (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )? (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )? ) )
            // InternalPragmas.g:361:2: (otherlv_0= 'gscop_cg_schedule' (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )? (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )? (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )? )
            {
            // InternalPragmas.g:361:2: (otherlv_0= 'gscop_cg_schedule' (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )? (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )? (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )? )
            // InternalPragmas.g:362:3: otherlv_0= 'gscop_cg_schedule' (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )? (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )? (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )?
            {
            otherlv_0=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getALMACoarseGrainPragmaAccess().getGscop_cg_scheduleKeyword_0());
            		
            // InternalPragmas.g:366:3: (otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalPragmas.g:367:4: otherlv_1= 'numbertasks' otherlv_2= '=' ( (lv_nbTasks_3_0= ruleINT_TYPE ) )
                    {
                    otherlv_1=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_1, grammarAccess.getALMACoarseGrainPragmaAccess().getNumbertasksKeyword_1_0());
                    			
                    otherlv_2=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_2, grammarAccess.getALMACoarseGrainPragmaAccess().getEqualsSignKeyword_1_1());
                    			
                    // InternalPragmas.g:375:4: ( (lv_nbTasks_3_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:376:5: (lv_nbTasks_3_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:376:5: (lv_nbTasks_3_0= ruleINT_TYPE )
                    // InternalPragmas.g:377:6: lv_nbTasks_3_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getALMACoarseGrainPragmaAccess().getNbTasksINT_TYPEParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_nbTasks_3_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getALMACoarseGrainPragmaRule());
                    						}
                    						set(
                    							current,
                    							"nbTasks",
                    							lv_nbTasks_3_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:395:3: (otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPragmas.g:396:4: otherlv_4= 'fixedtasks' otherlv_5= '=' ( (lv_taskIds_6_0= ruleINT_TYPE ) ) (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )*
                    {
                    otherlv_4=(Token)match(input,16,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getALMACoarseGrainPragmaAccess().getFixedtasksKeyword_2_0());
                    			
                    otherlv_5=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_5, grammarAccess.getALMACoarseGrainPragmaAccess().getEqualsSignKeyword_2_1());
                    			
                    // InternalPragmas.g:404:4: ( (lv_taskIds_6_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:405:5: (lv_taskIds_6_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:405:5: (lv_taskIds_6_0= ruleINT_TYPE )
                    // InternalPragmas.g:406:6: lv_taskIds_6_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getALMACoarseGrainPragmaAccess().getTaskIdsINT_TYPEParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_taskIds_6_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getALMACoarseGrainPragmaRule());
                    						}
                    						add(
                    							current,
                    							"taskIds",
                    							lv_taskIds_6_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPragmas.g:423:4: (otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==17) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalPragmas.g:424:5: otherlv_7= ',' ( (lv_taskIds_8_0= ruleINT_TYPE ) )
                    	    {
                    	    otherlv_7=(Token)match(input,17,FOLLOW_6); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getALMACoarseGrainPragmaAccess().getCommaKeyword_2_3_0());
                    	    				
                    	    // InternalPragmas.g:428:5: ( (lv_taskIds_8_0= ruleINT_TYPE ) )
                    	    // InternalPragmas.g:429:6: (lv_taskIds_8_0= ruleINT_TYPE )
                    	    {
                    	    // InternalPragmas.g:429:6: (lv_taskIds_8_0= ruleINT_TYPE )
                    	    // InternalPragmas.g:430:7: lv_taskIds_8_0= ruleINT_TYPE
                    	    {

                    	    							newCompositeNode(grammarAccess.getALMACoarseGrainPragmaAccess().getTaskIdsINT_TYPEParserRuleCall_2_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_taskIds_8_0=ruleINT_TYPE();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getALMACoarseGrainPragmaRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"taskIds",
                    	    								lv_taskIds_8_0,
                    	    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalPragmas.g:449:3: (otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalPragmas.g:450:4: otherlv_9= 'tileSizes' otherlv_10= '=' ( (lv_tileSizes_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )*
                    {
                    otherlv_9=(Token)match(input,18,FOLLOW_5); 

                    				newLeafNode(otherlv_9, grammarAccess.getALMACoarseGrainPragmaAccess().getTileSizesKeyword_3_0());
                    			
                    otherlv_10=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_10, grammarAccess.getALMACoarseGrainPragmaAccess().getEqualsSignKeyword_3_1());
                    			
                    // InternalPragmas.g:458:4: ( (lv_tileSizes_11_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:459:5: (lv_tileSizes_11_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:459:5: (lv_tileSizes_11_0= ruleINT_TYPE )
                    // InternalPragmas.g:460:6: lv_tileSizes_11_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getALMACoarseGrainPragmaAccess().getTileSizesINT_TYPEParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_tileSizes_11_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getALMACoarseGrainPragmaRule());
                    						}
                    						add(
                    							current,
                    							"tileSizes",
                    							lv_tileSizes_11_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPragmas.g:477:4: (otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==17) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalPragmas.g:478:5: otherlv_12= ',' ( (lv_tileSizes_13_0= ruleINT_TYPE ) )
                    	    {
                    	    otherlv_12=(Token)match(input,17,FOLLOW_6); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getALMACoarseGrainPragmaAccess().getCommaKeyword_3_3_0());
                    	    				
                    	    // InternalPragmas.g:482:5: ( (lv_tileSizes_13_0= ruleINT_TYPE ) )
                    	    // InternalPragmas.g:483:6: (lv_tileSizes_13_0= ruleINT_TYPE )
                    	    {
                    	    // InternalPragmas.g:483:6: (lv_tileSizes_13_0= ruleINT_TYPE )
                    	    // InternalPragmas.g:484:7: lv_tileSizes_13_0= ruleINT_TYPE
                    	    {

                    	    							newCompositeNode(grammarAccess.getALMACoarseGrainPragmaAccess().getTileSizesINT_TYPEParserRuleCall_3_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_tileSizes_13_0=ruleINT_TYPE();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getALMACoarseGrainPragmaRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"tileSizes",
                    	    								lv_tileSizes_13_0,
                    	    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleALMACoarseGrainPragma"


    // $ANTLR start "entryRuleALMAFineGrainPragma"
    // InternalPragmas.g:507:1: entryRuleALMAFineGrainPragma returns [EObject current=null] : iv_ruleALMAFineGrainPragma= ruleALMAFineGrainPragma EOF ;
    public final EObject entryRuleALMAFineGrainPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleALMAFineGrainPragma = null;


        try {
            // InternalPragmas.g:507:60: (iv_ruleALMAFineGrainPragma= ruleALMAFineGrainPragma EOF )
            // InternalPragmas.g:508:2: iv_ruleALMAFineGrainPragma= ruleALMAFineGrainPragma EOF
            {
             newCompositeNode(grammarAccess.getALMAFineGrainPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleALMAFineGrainPragma=ruleALMAFineGrainPragma();

            state._fsp--;

             current =iv_ruleALMAFineGrainPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleALMAFineGrainPragma"


    // $ANTLR start "ruleALMAFineGrainPragma"
    // InternalPragmas.g:514:1: ruleALMAFineGrainPragma returns [EObject current=null] : (otherlv_0= 'gscop_fg_schedule' otherlv_1= 'tileSizes' otherlv_2= '=' ( (lv_tileSizes_3_0= ruleINT_TYPE ) ) (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )* ) ;
    public final EObject ruleALMAFineGrainPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_tileSizes_3_0 = null;

        AntlrDatatypeRuleToken lv_tileSizes_5_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:520:2: ( (otherlv_0= 'gscop_fg_schedule' otherlv_1= 'tileSizes' otherlv_2= '=' ( (lv_tileSizes_3_0= ruleINT_TYPE ) ) (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )* ) )
            // InternalPragmas.g:521:2: (otherlv_0= 'gscop_fg_schedule' otherlv_1= 'tileSizes' otherlv_2= '=' ( (lv_tileSizes_3_0= ruleINT_TYPE ) ) (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )* )
            {
            // InternalPragmas.g:521:2: (otherlv_0= 'gscop_fg_schedule' otherlv_1= 'tileSizes' otherlv_2= '=' ( (lv_tileSizes_3_0= ruleINT_TYPE ) ) (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )* )
            // InternalPragmas.g:522:3: otherlv_0= 'gscop_fg_schedule' otherlv_1= 'tileSizes' otherlv_2= '=' ( (lv_tileSizes_3_0= ruleINT_TYPE ) ) (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )*
            {
            otherlv_0=(Token)match(input,19,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getALMAFineGrainPragmaAccess().getGscop_fg_scheduleKeyword_0());
            		
            otherlv_1=(Token)match(input,18,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getALMAFineGrainPragmaAccess().getTileSizesKeyword_1());
            		
            otherlv_2=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getALMAFineGrainPragmaAccess().getEqualsSignKeyword_2());
            		
            // InternalPragmas.g:534:3: ( (lv_tileSizes_3_0= ruleINT_TYPE ) )
            // InternalPragmas.g:535:4: (lv_tileSizes_3_0= ruleINT_TYPE )
            {
            // InternalPragmas.g:535:4: (lv_tileSizes_3_0= ruleINT_TYPE )
            // InternalPragmas.g:536:5: lv_tileSizes_3_0= ruleINT_TYPE
            {

            					newCompositeNode(grammarAccess.getALMAFineGrainPragmaAccess().getTileSizesINT_TYPEParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_9);
            lv_tileSizes_3_0=ruleINT_TYPE();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getALMAFineGrainPragmaRule());
            					}
            					add(
            						current,
            						"tileSizes",
            						lv_tileSizes_3_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:553:3: (otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==17) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalPragmas.g:554:4: otherlv_4= ',' ( (lv_tileSizes_5_0= ruleINT_TYPE ) )
            	    {
            	    otherlv_4=(Token)match(input,17,FOLLOW_6); 

            	    				newLeafNode(otherlv_4, grammarAccess.getALMAFineGrainPragmaAccess().getCommaKeyword_4_0());
            	    			
            	    // InternalPragmas.g:558:4: ( (lv_tileSizes_5_0= ruleINT_TYPE ) )
            	    // InternalPragmas.g:559:5: (lv_tileSizes_5_0= ruleINT_TYPE )
            	    {
            	    // InternalPragmas.g:559:5: (lv_tileSizes_5_0= ruleINT_TYPE )
            	    // InternalPragmas.g:560:6: lv_tileSizes_5_0= ruleINT_TYPE
            	    {

            	    						newCompositeNode(grammarAccess.getALMAFineGrainPragmaAccess().getTileSizesINT_TYPEParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_tileSizes_5_0=ruleINT_TYPE();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getALMAFineGrainPragmaRule());
            	    						}
            	    						add(
            	    							current,
            	    							"tileSizes",
            	    							lv_tileSizes_5_0,
            	    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleALMAFineGrainPragma"


    // $ANTLR start "entryRulePureFunctionPragma"
    // InternalPragmas.g:582:1: entryRulePureFunctionPragma returns [EObject current=null] : iv_rulePureFunctionPragma= rulePureFunctionPragma EOF ;
    public final EObject entryRulePureFunctionPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePureFunctionPragma = null;


        try {
            // InternalPragmas.g:582:59: (iv_rulePureFunctionPragma= rulePureFunctionPragma EOF )
            // InternalPragmas.g:583:2: iv_rulePureFunctionPragma= rulePureFunctionPragma EOF
            {
             newCompositeNode(grammarAccess.getPureFunctionPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePureFunctionPragma=rulePureFunctionPragma();

            state._fsp--;

             current =iv_rulePureFunctionPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePureFunctionPragma"


    // $ANTLR start "rulePureFunctionPragma"
    // InternalPragmas.g:589:1: rulePureFunctionPragma returns [EObject current=null] : otherlv_0= 'gscop_pure_function' ;
    public final EObject rulePureFunctionPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalPragmas.g:595:2: (otherlv_0= 'gscop_pure_function' )
            // InternalPragmas.g:596:2: otherlv_0= 'gscop_pure_function'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getPureFunctionPragmaAccess().getGscop_pure_functionKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePureFunctionPragma"


    // $ANTLR start "entryRuleopenMpPragma"
    // InternalPragmas.g:603:1: entryRuleopenMpPragma returns [EObject current=null] : iv_ruleopenMpPragma= ruleopenMpPragma EOF ;
    public final EObject entryRuleopenMpPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleopenMpPragma = null;


        try {
            // InternalPragmas.g:603:53: (iv_ruleopenMpPragma= ruleopenMpPragma EOF )
            // InternalPragmas.g:604:2: iv_ruleopenMpPragma= ruleopenMpPragma EOF
            {
             newCompositeNode(grammarAccess.getOpenMpPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleopenMpPragma=ruleopenMpPragma();

            state._fsp--;

             current =iv_ruleopenMpPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleopenMpPragma"


    // $ANTLR start "ruleopenMpPragma"
    // InternalPragmas.g:610:1: ruleopenMpPragma returns [EObject current=null] : (otherlv_0= 'omp' otherlv_1= 'parallel' ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )? (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )? ) ;
    public final EObject ruleopenMpPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_parallelFor_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;


        	enterRule();

        try {
            // InternalPragmas.g:616:2: ( (otherlv_0= 'omp' otherlv_1= 'parallel' ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )? (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )? ) )
            // InternalPragmas.g:617:2: (otherlv_0= 'omp' otherlv_1= 'parallel' ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )? (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )? )
            {
            // InternalPragmas.g:617:2: (otherlv_0= 'omp' otherlv_1= 'parallel' ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )? (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )? )
            // InternalPragmas.g:618:3: otherlv_0= 'omp' otherlv_1= 'parallel' ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )? (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )?
            {
            otherlv_0=(Token)match(input,21,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getOpenMpPragmaAccess().getOmpKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getOpenMpPragmaAccess().getParallelKeyword_1());
            		
            // InternalPragmas.g:626:3: ( ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )? )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==23) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalPragmas.g:627:4: ( (lv_parallelFor_2_0= 'for' ) ) (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )? (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )?
                    {
                    // InternalPragmas.g:627:4: ( (lv_parallelFor_2_0= 'for' ) )
                    // InternalPragmas.g:628:5: (lv_parallelFor_2_0= 'for' )
                    {
                    // InternalPragmas.g:628:5: (lv_parallelFor_2_0= 'for' )
                    // InternalPragmas.g:629:6: lv_parallelFor_2_0= 'for'
                    {
                    lv_parallelFor_2_0=(Token)match(input,23,FOLLOW_13); 

                    						newLeafNode(lv_parallelFor_2_0, grammarAccess.getOpenMpPragmaAccess().getParallelForForKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                    						}
                    						setWithLastConsumed(current, "parallelFor", true, "for");
                    					

                    }


                    }

                    // InternalPragmas.g:641:4: (otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==24) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalPragmas.g:642:5: otherlv_3= 'private' otherlv_4= '(' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* otherlv_8= ')'
                            {
                            otherlv_3=(Token)match(input,24,FOLLOW_14); 

                            					newLeafNode(otherlv_3, grammarAccess.getOpenMpPragmaAccess().getPrivateKeyword_2_1_0());
                            				
                            otherlv_4=(Token)match(input,25,FOLLOW_15); 

                            					newLeafNode(otherlv_4, grammarAccess.getOpenMpPragmaAccess().getLeftParenthesisKeyword_2_1_1());
                            				
                            // InternalPragmas.g:650:5: ( (otherlv_5= RULE_ID ) )
                            // InternalPragmas.g:651:6: (otherlv_5= RULE_ID )
                            {
                            // InternalPragmas.g:651:6: (otherlv_5= RULE_ID )
                            // InternalPragmas.g:652:7: otherlv_5= RULE_ID
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                            							}
                            						
                            otherlv_5=(Token)match(input,RULE_ID,FOLLOW_16); 

                            							newLeafNode(otherlv_5, grammarAccess.getOpenMpPragmaAccess().getPrivatesSymbolCrossReference_2_1_2_0());
                            						

                            }


                            }

                            // InternalPragmas.g:663:5: (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )*
                            loop9:
                            do {
                                int alt9=2;
                                int LA9_0 = input.LA(1);

                                if ( (LA9_0==17) ) {
                                    alt9=1;
                                }


                                switch (alt9) {
                            	case 1 :
                            	    // InternalPragmas.g:664:6: otherlv_6= ',' ( (otherlv_7= RULE_ID ) )
                            	    {
                            	    otherlv_6=(Token)match(input,17,FOLLOW_15); 

                            	    						newLeafNode(otherlv_6, grammarAccess.getOpenMpPragmaAccess().getCommaKeyword_2_1_3_0());
                            	    					
                            	    // InternalPragmas.g:668:6: ( (otherlv_7= RULE_ID ) )
                            	    // InternalPragmas.g:669:7: (otherlv_7= RULE_ID )
                            	    {
                            	    // InternalPragmas.g:669:7: (otherlv_7= RULE_ID )
                            	    // InternalPragmas.g:670:8: otherlv_7= RULE_ID
                            	    {

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                            	    								}
                            	    							
                            	    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_16); 

                            	    								newLeafNode(otherlv_7, grammarAccess.getOpenMpPragmaAccess().getPrivatesSymbolCrossReference_2_1_3_1_0());
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop9;
                                }
                            } while (true);

                            otherlv_8=(Token)match(input,26,FOLLOW_17); 

                            					newLeafNode(otherlv_8, grammarAccess.getOpenMpPragmaAccess().getRightParenthesisKeyword_2_1_4());
                            				

                            }
                            break;

                    }

                    // InternalPragmas.g:687:4: (otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')' )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==27) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalPragmas.g:688:5: otherlv_9= 'shared' otherlv_10= '(' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* otherlv_14= ')'
                            {
                            otherlv_9=(Token)match(input,27,FOLLOW_14); 

                            					newLeafNode(otherlv_9, grammarAccess.getOpenMpPragmaAccess().getSharedKeyword_2_2_0());
                            				
                            otherlv_10=(Token)match(input,25,FOLLOW_15); 

                            					newLeafNode(otherlv_10, grammarAccess.getOpenMpPragmaAccess().getLeftParenthesisKeyword_2_2_1());
                            				
                            // InternalPragmas.g:696:5: ( (otherlv_11= RULE_ID ) )
                            // InternalPragmas.g:697:6: (otherlv_11= RULE_ID )
                            {
                            // InternalPragmas.g:697:6: (otherlv_11= RULE_ID )
                            // InternalPragmas.g:698:7: otherlv_11= RULE_ID
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                            							}
                            						
                            otherlv_11=(Token)match(input,RULE_ID,FOLLOW_16); 

                            							newLeafNode(otherlv_11, grammarAccess.getOpenMpPragmaAccess().getSharedSymbolCrossReference_2_2_2_0());
                            						

                            }


                            }

                            // InternalPragmas.g:709:5: (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )*
                            loop11:
                            do {
                                int alt11=2;
                                int LA11_0 = input.LA(1);

                                if ( (LA11_0==17) ) {
                                    alt11=1;
                                }


                                switch (alt11) {
                            	case 1 :
                            	    // InternalPragmas.g:710:6: otherlv_12= ',' ( (otherlv_13= RULE_ID ) )
                            	    {
                            	    otherlv_12=(Token)match(input,17,FOLLOW_15); 

                            	    						newLeafNode(otherlv_12, grammarAccess.getOpenMpPragmaAccess().getCommaKeyword_2_2_3_0());
                            	    					
                            	    // InternalPragmas.g:714:6: ( (otherlv_13= RULE_ID ) )
                            	    // InternalPragmas.g:715:7: (otherlv_13= RULE_ID )
                            	    {
                            	    // InternalPragmas.g:715:7: (otherlv_13= RULE_ID )
                            	    // InternalPragmas.g:716:8: otherlv_13= RULE_ID
                            	    {

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                            	    								}
                            	    							
                            	    otherlv_13=(Token)match(input,RULE_ID,FOLLOW_16); 

                            	    								newLeafNode(otherlv_13, grammarAccess.getOpenMpPragmaAccess().getSharedSymbolCrossReference_2_2_3_1_0());
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop11;
                                }
                            } while (true);

                            otherlv_14=(Token)match(input,26,FOLLOW_18); 

                            					newLeafNode(otherlv_14, grammarAccess.getOpenMpPragmaAccess().getRightParenthesisKeyword_2_2_4());
                            				

                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalPragmas.g:734:3: (otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==28) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPragmas.g:735:4: otherlv_15= 'reduction' otherlv_16= '(' otherlv_17= '+' otherlv_18= ':' ( (otherlv_19= RULE_ID ) ) otherlv_20= ')'
                    {
                    otherlv_15=(Token)match(input,28,FOLLOW_14); 

                    				newLeafNode(otherlv_15, grammarAccess.getOpenMpPragmaAccess().getReductionKeyword_3_0());
                    			
                    otherlv_16=(Token)match(input,25,FOLLOW_19); 

                    				newLeafNode(otherlv_16, grammarAccess.getOpenMpPragmaAccess().getLeftParenthesisKeyword_3_1());
                    			
                    otherlv_17=(Token)match(input,29,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getOpenMpPragmaAccess().getPlusSignKeyword_3_2());
                    			
                    otherlv_18=(Token)match(input,30,FOLLOW_15); 

                    				newLeafNode(otherlv_18, grammarAccess.getOpenMpPragmaAccess().getColonKeyword_3_3());
                    			
                    // InternalPragmas.g:751:4: ( (otherlv_19= RULE_ID ) )
                    // InternalPragmas.g:752:5: (otherlv_19= RULE_ID )
                    {
                    // InternalPragmas.g:752:5: (otherlv_19= RULE_ID )
                    // InternalPragmas.g:753:6: otherlv_19= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOpenMpPragmaRule());
                    						}
                    					
                    otherlv_19=(Token)match(input,RULE_ID,FOLLOW_21); 

                    						newLeafNode(otherlv_19, grammarAccess.getOpenMpPragmaAccess().getReductionsSymbolCrossReference_3_4_0());
                    					

                    }


                    }

                    otherlv_20=(Token)match(input,26,FOLLOW_2); 

                    				newLeafNode(otherlv_20, grammarAccess.getOpenMpPragmaAccess().getRightParenthesisKeyword_3_5());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleopenMpPragma"


    // $ANTLR start "entryRuleUnrollPragma"
    // InternalPragmas.g:773:1: entryRuleUnrollPragma returns [EObject current=null] : iv_ruleUnrollPragma= ruleUnrollPragma EOF ;
    public final EObject entryRuleUnrollPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnrollPragma = null;


        try {
            // InternalPragmas.g:773:53: (iv_ruleUnrollPragma= ruleUnrollPragma EOF )
            // InternalPragmas.g:774:2: iv_ruleUnrollPragma= ruleUnrollPragma EOF
            {
             newCompositeNode(grammarAccess.getUnrollPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnrollPragma=ruleUnrollPragma();

            state._fsp--;

             current =iv_ruleUnrollPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnrollPragma"


    // $ANTLR start "ruleUnrollPragma"
    // InternalPragmas.g:780:1: ruleUnrollPragma returns [EObject current=null] : (otherlv_0= 'gscop_unroll' ( (lv_factor_1_0= ruleINT_TYPE ) )? ) ;
    public final EObject ruleUnrollPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_factor_1_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:786:2: ( (otherlv_0= 'gscop_unroll' ( (lv_factor_1_0= ruleINT_TYPE ) )? ) )
            // InternalPragmas.g:787:2: (otherlv_0= 'gscop_unroll' ( (lv_factor_1_0= ruleINT_TYPE ) )? )
            {
            // InternalPragmas.g:787:2: (otherlv_0= 'gscop_unroll' ( (lv_factor_1_0= ruleINT_TYPE ) )? )
            // InternalPragmas.g:788:3: otherlv_0= 'gscop_unroll' ( (lv_factor_1_0= ruleINT_TYPE ) )?
            {
            otherlv_0=(Token)match(input,31,FOLLOW_22); 

            			newLeafNode(otherlv_0, grammarAccess.getUnrollPragmaAccess().getGscop_unrollKeyword_0());
            		
            // InternalPragmas.g:792:3: ( (lv_factor_1_0= ruleINT_TYPE ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_INTTOKEN) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPragmas.g:793:4: (lv_factor_1_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:793:4: (lv_factor_1_0= ruleINT_TYPE )
                    // InternalPragmas.g:794:5: lv_factor_1_0= ruleINT_TYPE
                    {

                    					newCompositeNode(grammarAccess.getUnrollPragmaAccess().getFactorINT_TYPEParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_factor_1_0=ruleINT_TYPE();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getUnrollPragmaRule());
                    					}
                    					set(
                    						current,
                    						"factor",
                    						lv_factor_1_0,
                    						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnrollPragma"


    // $ANTLR start "entryRuleInlinePragma"
    // InternalPragmas.g:815:1: entryRuleInlinePragma returns [EObject current=null] : iv_ruleInlinePragma= ruleInlinePragma EOF ;
    public final EObject entryRuleInlinePragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInlinePragma = null;


        try {
            // InternalPragmas.g:815:53: (iv_ruleInlinePragma= ruleInlinePragma EOF )
            // InternalPragmas.g:816:2: iv_ruleInlinePragma= ruleInlinePragma EOF
            {
             newCompositeNode(grammarAccess.getInlinePragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInlinePragma=ruleInlinePragma();

            state._fsp--;

             current =iv_ruleInlinePragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInlinePragma"


    // $ANTLR start "ruleInlinePragma"
    // InternalPragmas.g:822:1: ruleInlinePragma returns [EObject current=null] : (otherlv_0= 'gscop_inline' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleInlinePragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPragmas.g:828:2: ( (otherlv_0= 'gscop_inline' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalPragmas.g:829:2: (otherlv_0= 'gscop_inline' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalPragmas.g:829:2: (otherlv_0= 'gscop_inline' ( (otherlv_1= RULE_ID ) ) )
            // InternalPragmas.g:830:3: otherlv_0= 'gscop_inline' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,32,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getInlinePragmaAccess().getGscop_inlineKeyword_0());
            		
            // InternalPragmas.g:834:3: ( (otherlv_1= RULE_ID ) )
            // InternalPragmas.g:835:4: (otherlv_1= RULE_ID )
            {
            // InternalPragmas.g:835:4: (otherlv_1= RULE_ID )
            // InternalPragmas.g:836:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInlinePragmaRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getInlinePragmaAccess().getProcProcedureSymbolCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInlinePragma"


    // $ANTLR start "entryRuleIgnoreMemoryDep"
    // InternalPragmas.g:851:1: entryRuleIgnoreMemoryDep returns [EObject current=null] : iv_ruleIgnoreMemoryDep= ruleIgnoreMemoryDep EOF ;
    public final EObject entryRuleIgnoreMemoryDep() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIgnoreMemoryDep = null;


        try {
            // InternalPragmas.g:851:56: (iv_ruleIgnoreMemoryDep= ruleIgnoreMemoryDep EOF )
            // InternalPragmas.g:852:2: iv_ruleIgnoreMemoryDep= ruleIgnoreMemoryDep EOF
            {
             newCompositeNode(grammarAccess.getIgnoreMemoryDepRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIgnoreMemoryDep=ruleIgnoreMemoryDep();

            state._fsp--;

             current =iv_ruleIgnoreMemoryDep; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIgnoreMemoryDep"


    // $ANTLR start "ruleIgnoreMemoryDep"
    // InternalPragmas.g:858:1: ruleIgnoreMemoryDep returns [EObject current=null] : (otherlv_0= 'ignore_memory_dependency' ( (lv_type_1_0= ruleDepType ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= 'from' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'to' ( (otherlv_9= RULE_ID ) ) ) ;
    public final EObject ruleIgnoreMemoryDep() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Enumerator lv_type_1_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:864:2: ( (otherlv_0= 'ignore_memory_dependency' ( (lv_type_1_0= ruleDepType ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= 'from' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'to' ( (otherlv_9= RULE_ID ) ) ) )
            // InternalPragmas.g:865:2: (otherlv_0= 'ignore_memory_dependency' ( (lv_type_1_0= ruleDepType ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= 'from' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'to' ( (otherlv_9= RULE_ID ) ) )
            {
            // InternalPragmas.g:865:2: (otherlv_0= 'ignore_memory_dependency' ( (lv_type_1_0= ruleDepType ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= 'from' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'to' ( (otherlv_9= RULE_ID ) ) )
            // InternalPragmas.g:866:3: otherlv_0= 'ignore_memory_dependency' ( (lv_type_1_0= ruleDepType ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= 'from' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'to' ( (otherlv_9= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,33,FOLLOW_23); 

            			newLeafNode(otherlv_0, grammarAccess.getIgnoreMemoryDepAccess().getIgnore_memory_dependencyKeyword_0());
            		
            // InternalPragmas.g:870:3: ( (lv_type_1_0= ruleDepType ) )
            // InternalPragmas.g:871:4: (lv_type_1_0= ruleDepType )
            {
            // InternalPragmas.g:871:4: (lv_type_1_0= ruleDepType )
            // InternalPragmas.g:872:5: lv_type_1_0= ruleDepType
            {

            					newCompositeNode(grammarAccess.getIgnoreMemoryDepAccess().getTypeDepTypeEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_type_1_0=ruleDepType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIgnoreMemoryDepRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_1_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.DepType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getIgnoreMemoryDepAccess().getForKeyword_2());
            		
            // InternalPragmas.g:893:3: ( (otherlv_3= RULE_ID ) )
            // InternalPragmas.g:894:4: (otherlv_3= RULE_ID )
            {
            // InternalPragmas.g:894:4: (otherlv_3= RULE_ID )
            // InternalPragmas.g:895:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIgnoreMemoryDepRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(otherlv_3, grammarAccess.getIgnoreMemoryDepAccess().getSymbolsSymbolCrossReference_3_0());
            				

            }


            }

            // InternalPragmas.g:906:3: (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==17) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalPragmas.g:907:4: otherlv_4= ',' ( (otherlv_5= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,17,FOLLOW_15); 

            	    				newLeafNode(otherlv_4, grammarAccess.getIgnoreMemoryDepAccess().getCommaKeyword_4_0());
            	    			
            	    // InternalPragmas.g:911:4: ( (otherlv_5= RULE_ID ) )
            	    // InternalPragmas.g:912:5: (otherlv_5= RULE_ID )
            	    {
            	    // InternalPragmas.g:912:5: (otherlv_5= RULE_ID )
            	    // InternalPragmas.g:913:6: otherlv_5= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getIgnoreMemoryDepRule());
            	    						}
            	    					
            	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_25); 

            	    						newLeafNode(otherlv_5, grammarAccess.getIgnoreMemoryDepAccess().getSymbolsSymbolCrossReference_4_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            otherlv_6=(Token)match(input,34,FOLLOW_15); 

            			newLeafNode(otherlv_6, grammarAccess.getIgnoreMemoryDepAccess().getFromKeyword_5());
            		
            // InternalPragmas.g:929:3: ( (otherlv_7= RULE_ID ) )
            // InternalPragmas.g:930:4: (otherlv_7= RULE_ID )
            {
            // InternalPragmas.g:930:4: (otherlv_7= RULE_ID )
            // InternalPragmas.g:931:5: otherlv_7= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIgnoreMemoryDepRule());
            					}
            				
            otherlv_7=(Token)match(input,RULE_ID,FOLLOW_26); 

            					newLeafNode(otherlv_7, grammarAccess.getIgnoreMemoryDepAccess().getFromInstructionCrossReference_6_0());
            				

            }


            }

            otherlv_8=(Token)match(input,35,FOLLOW_15); 

            			newLeafNode(otherlv_8, grammarAccess.getIgnoreMemoryDepAccess().getToKeyword_7());
            		
            // InternalPragmas.g:946:3: ( (otherlv_9= RULE_ID ) )
            // InternalPragmas.g:947:4: (otherlv_9= RULE_ID )
            {
            // InternalPragmas.g:947:4: (otherlv_9= RULE_ID )
            // InternalPragmas.g:948:5: otherlv_9= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIgnoreMemoryDepRule());
            					}
            				
            otherlv_9=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_9, grammarAccess.getIgnoreMemoryDepAccess().getToInstructionCrossReference_8_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIgnoreMemoryDep"


    // $ANTLR start "entryRuleScheduleContextPragma"
    // InternalPragmas.g:963:1: entryRuleScheduleContextPragma returns [EObject current=null] : iv_ruleScheduleContextPragma= ruleScheduleContextPragma EOF ;
    public final EObject entryRuleScheduleContextPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScheduleContextPragma = null;


        try {
            // InternalPragmas.g:963:62: (iv_ruleScheduleContextPragma= ruleScheduleContextPragma EOF )
            // InternalPragmas.g:964:2: iv_ruleScheduleContextPragma= ruleScheduleContextPragma EOF
            {
             newCompositeNode(grammarAccess.getScheduleContextPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScheduleContextPragma=ruleScheduleContextPragma();

            state._fsp--;

             current =iv_ruleScheduleContextPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScheduleContextPragma"


    // $ANTLR start "ruleScheduleContextPragma"
    // InternalPragmas.g:970:1: ruleScheduleContextPragma returns [EObject current=null] : (otherlv_0= 'scop' (otherlv_1= 'name' otherlv_2= '=' )? ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )? (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )? ) ;
    public final EObject ruleScheduleContextPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token lv_scheduling_15_1=null;
        Token lv_scheduling_15_2=null;
        EObject lv_constraints_8_0 = null;

        EObject lv_constraints_10_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:976:2: ( (otherlv_0= 'scop' (otherlv_1= 'name' otherlv_2= '=' )? ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )? (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )? ) )
            // InternalPragmas.g:977:2: (otherlv_0= 'scop' (otherlv_1= 'name' otherlv_2= '=' )? ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )? (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )? )
            {
            // InternalPragmas.g:977:2: (otherlv_0= 'scop' (otherlv_1= 'name' otherlv_2= '=' )? ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )? (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )? )
            // InternalPragmas.g:978:3: otherlv_0= 'scop' (otherlv_1= 'name' otherlv_2= '=' )? ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )? (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )?
            {
            otherlv_0=(Token)match(input,36,FOLLOW_27); 

            			newLeafNode(otherlv_0, grammarAccess.getScheduleContextPragmaAccess().getScopKeyword_0());
            		
            // InternalPragmas.g:982:3: (otherlv_1= 'name' otherlv_2= '=' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==37) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPragmas.g:983:4: otherlv_1= 'name' otherlv_2= '='
                    {
                    otherlv_1=(Token)match(input,37,FOLLOW_5); 

                    				newLeafNode(otherlv_1, grammarAccess.getScheduleContextPragmaAccess().getNameKeyword_1_0());
                    			
                    otherlv_2=(Token)match(input,15,FOLLOW_15); 

                    				newLeafNode(otherlv_2, grammarAccess.getScheduleContextPragmaAccess().getEqualsSignKeyword_1_1());
                    			

                    }
                    break;

            }

            // InternalPragmas.g:992:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalPragmas.g:993:4: (lv_name_3_0= RULE_ID )
            {
            // InternalPragmas.g:993:4: (lv_name_3_0= RULE_ID )
            // InternalPragmas.g:994:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_3_0, grammarAccess.getScheduleContextPragmaAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getScheduleContextPragmaRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_3_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.ID");
            				

            }


            }

            // InternalPragmas.g:1010:3: (otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==17) ) {
                int LA19_1 = input.LA(2);

                if ( (LA19_1==38) ) {
                    alt19=1;
                }
            }
            switch (alt19) {
                case 1 :
                    // InternalPragmas.g:1011:4: otherlv_4= ',' otherlv_5= 'context' otherlv_6= '=' otherlv_7= '(' ( (lv_constraints_8_0= ruleAffineConstraint ) ) (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )* otherlv_11= ')'
                    {
                    otherlv_4=(Token)match(input,17,FOLLOW_28); 

                    				newLeafNode(otherlv_4, grammarAccess.getScheduleContextPragmaAccess().getCommaKeyword_3_0());
                    			
                    otherlv_5=(Token)match(input,38,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getScheduleContextPragmaAccess().getContextKeyword_3_1());
                    			
                    otherlv_6=(Token)match(input,15,FOLLOW_14); 

                    				newLeafNode(otherlv_6, grammarAccess.getScheduleContextPragmaAccess().getEqualsSignKeyword_3_2());
                    			
                    otherlv_7=(Token)match(input,25,FOLLOW_29); 

                    				newLeafNode(otherlv_7, grammarAccess.getScheduleContextPragmaAccess().getLeftParenthesisKeyword_3_3());
                    			
                    // InternalPragmas.g:1027:4: ( (lv_constraints_8_0= ruleAffineConstraint ) )
                    // InternalPragmas.g:1028:5: (lv_constraints_8_0= ruleAffineConstraint )
                    {
                    // InternalPragmas.g:1028:5: (lv_constraints_8_0= ruleAffineConstraint )
                    // InternalPragmas.g:1029:6: lv_constraints_8_0= ruleAffineConstraint
                    {

                    						newCompositeNode(grammarAccess.getScheduleContextPragmaAccess().getConstraintsAffineConstraintParserRuleCall_3_4_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_constraints_8_0=ruleAffineConstraint();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScheduleContextPragmaRule());
                    						}
                    						add(
                    							current,
                    							"constraints",
                    							lv_constraints_8_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.AffineConstraint");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPragmas.g:1046:4: (otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==17) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalPragmas.g:1047:5: otherlv_9= ',' ( (lv_constraints_10_0= ruleAffineConstraint ) )
                    	    {
                    	    otherlv_9=(Token)match(input,17,FOLLOW_29); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getScheduleContextPragmaAccess().getCommaKeyword_3_5_0());
                    	    				
                    	    // InternalPragmas.g:1051:5: ( (lv_constraints_10_0= ruleAffineConstraint ) )
                    	    // InternalPragmas.g:1052:6: (lv_constraints_10_0= ruleAffineConstraint )
                    	    {
                    	    // InternalPragmas.g:1052:6: (lv_constraints_10_0= ruleAffineConstraint )
                    	    // InternalPragmas.g:1053:7: lv_constraints_10_0= ruleAffineConstraint
                    	    {

                    	    							newCompositeNode(grammarAccess.getScheduleContextPragmaAccess().getConstraintsAffineConstraintParserRuleCall_3_5_1_0());
                    	    						
                    	    pushFollow(FOLLOW_16);
                    	    lv_constraints_10_0=ruleAffineConstraint();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getScheduleContextPragmaRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"constraints",
                    	    								lv_constraints_10_0,
                    	    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.AffineConstraint");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,26,FOLLOW_9); 

                    				newLeafNode(otherlv_11, grammarAccess.getScheduleContextPragmaAccess().getRightParenthesisKeyword_3_6());
                    			

                    }
                    break;

            }

            // InternalPragmas.g:1076:3: (otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==17) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPragmas.g:1077:4: otherlv_12= ',' otherlv_13= 'scheduling' otherlv_14= '=' ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) )
                    {
                    otherlv_12=(Token)match(input,17,FOLLOW_30); 

                    				newLeafNode(otherlv_12, grammarAccess.getScheduleContextPragmaAccess().getCommaKeyword_4_0());
                    			
                    otherlv_13=(Token)match(input,39,FOLLOW_5); 

                    				newLeafNode(otherlv_13, grammarAccess.getScheduleContextPragmaAccess().getSchedulingKeyword_4_1());
                    			
                    otherlv_14=(Token)match(input,15,FOLLOW_31); 

                    				newLeafNode(otherlv_14, grammarAccess.getScheduleContextPragmaAccess().getEqualsSignKeyword_4_2());
                    			
                    // InternalPragmas.g:1089:4: ( ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) ) )
                    // InternalPragmas.g:1090:5: ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) )
                    {
                    // InternalPragmas.g:1090:5: ( (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' ) )
                    // InternalPragmas.g:1091:6: (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' )
                    {
                    // InternalPragmas.g:1091:6: (lv_scheduling_15_1= 'pluto-isl' | lv_scheduling_15_2= 'feautrier-isl' )
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==40) ) {
                        alt20=1;
                    }
                    else if ( (LA20_0==41) ) {
                        alt20=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 0, input);

                        throw nvae;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalPragmas.g:1092:7: lv_scheduling_15_1= 'pluto-isl'
                            {
                            lv_scheduling_15_1=(Token)match(input,40,FOLLOW_2); 

                            							newLeafNode(lv_scheduling_15_1, grammarAccess.getScheduleContextPragmaAccess().getSchedulingPlutoIslKeyword_4_3_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getScheduleContextPragmaRule());
                            							}
                            							setWithLastConsumed(current, "scheduling", lv_scheduling_15_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalPragmas.g:1103:7: lv_scheduling_15_2= 'feautrier-isl'
                            {
                            lv_scheduling_15_2=(Token)match(input,41,FOLLOW_2); 

                            							newLeafNode(lv_scheduling_15_2, grammarAccess.getScheduleContextPragmaAccess().getSchedulingFeautrierIslKeyword_4_3_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getScheduleContextPragmaRule());
                            							}
                            							setWithLastConsumed(current, "scheduling", lv_scheduling_15_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScheduleContextPragma"


    // $ANTLR start "entryRuleScheduleStatementPragma"
    // InternalPragmas.g:1121:1: entryRuleScheduleStatementPragma returns [EObject current=null] : iv_ruleScheduleStatementPragma= ruleScheduleStatementPragma EOF ;
    public final EObject entryRuleScheduleStatementPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScheduleStatementPragma = null;


        try {
            // InternalPragmas.g:1121:64: (iv_ruleScheduleStatementPragma= ruleScheduleStatementPragma EOF )
            // InternalPragmas.g:1122:2: iv_ruleScheduleStatementPragma= ruleScheduleStatementPragma EOF
            {
             newCompositeNode(grammarAccess.getScheduleStatementPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScheduleStatementPragma=ruleScheduleStatementPragma();

            state._fsp--;

             current =iv_ruleScheduleStatementPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScheduleStatementPragma"


    // $ANTLR start "ruleScheduleStatementPragma"
    // InternalPragmas.g:1128:1: ruleScheduleStatementPragma returns [EObject current=null] : (otherlv_0= 'scop_schedule_statement' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleScheduleStatementPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_exprs_2_0 = null;

        EObject lv_exprs_4_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1134:2: ( (otherlv_0= 'scop_schedule_statement' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalPragmas.g:1135:2: (otherlv_0= 'scop_schedule_statement' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalPragmas.g:1135:2: (otherlv_0= 'scop_schedule_statement' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalPragmas.g:1136:3: otherlv_0= 'scop_schedule_statement' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,42,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getScheduleStatementPragmaAccess().getScop_schedule_statementKeyword_0());
            		
            otherlv_1=(Token)match(input,25,FOLLOW_29); 

            			newLeafNode(otherlv_1, grammarAccess.getScheduleStatementPragmaAccess().getLeftParenthesisKeyword_1());
            		
            // InternalPragmas.g:1144:3: ( (lv_exprs_2_0= ruleExpression ) )
            // InternalPragmas.g:1145:4: (lv_exprs_2_0= ruleExpression )
            {
            // InternalPragmas.g:1145:4: (lv_exprs_2_0= ruleExpression )
            // InternalPragmas.g:1146:5: lv_exprs_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getScheduleStatementPragmaAccess().getExprsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_16);
            lv_exprs_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScheduleStatementPragmaRule());
            					}
            					add(
            						current,
            						"exprs",
            						lv_exprs_2_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:1163:3: (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==17) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalPragmas.g:1164:4: otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,17,FOLLOW_29); 

            	    				newLeafNode(otherlv_3, grammarAccess.getScheduleStatementPragmaAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalPragmas.g:1168:4: ( (lv_exprs_4_0= ruleExpression ) )
            	    // InternalPragmas.g:1169:5: (lv_exprs_4_0= ruleExpression )
            	    {
            	    // InternalPragmas.g:1169:5: (lv_exprs_4_0= ruleExpression )
            	    // InternalPragmas.g:1170:6: lv_exprs_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getScheduleStatementPragmaAccess().getExprsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_16);
            	    lv_exprs_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getScheduleStatementPragmaRule());
            	    						}
            	    						add(
            	    							current,
            	    							"exprs",
            	    							lv_exprs_4_0,
            	    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_5=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getScheduleStatementPragmaAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScheduleStatementPragma"


    // $ANTLR start "entryRuleScheduleBlockPragma"
    // InternalPragmas.g:1196:1: entryRuleScheduleBlockPragma returns [EObject current=null] : iv_ruleScheduleBlockPragma= ruleScheduleBlockPragma EOF ;
    public final EObject entryRuleScheduleBlockPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScheduleBlockPragma = null;


        try {
            // InternalPragmas.g:1196:60: (iv_ruleScheduleBlockPragma= ruleScheduleBlockPragma EOF )
            // InternalPragmas.g:1197:2: iv_ruleScheduleBlockPragma= ruleScheduleBlockPragma EOF
            {
             newCompositeNode(grammarAccess.getScheduleBlockPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScheduleBlockPragma=ruleScheduleBlockPragma();

            state._fsp--;

             current =iv_ruleScheduleBlockPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScheduleBlockPragma"


    // $ANTLR start "ruleScheduleBlockPragma"
    // InternalPragmas.g:1203:1: ruleScheduleBlockPragma returns [EObject current=null] : (otherlv_0= 'scop_schedule_block' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleScheduleBlockPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_exprs_2_0 = null;

        EObject lv_exprs_4_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1209:2: ( (otherlv_0= 'scop_schedule_block' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalPragmas.g:1210:2: (otherlv_0= 'scop_schedule_block' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalPragmas.g:1210:2: (otherlv_0= 'scop_schedule_block' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalPragmas.g:1211:3: otherlv_0= 'scop_schedule_block' otherlv_1= '(' ( (lv_exprs_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,43,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getScheduleBlockPragmaAccess().getScop_schedule_blockKeyword_0());
            		
            otherlv_1=(Token)match(input,25,FOLLOW_29); 

            			newLeafNode(otherlv_1, grammarAccess.getScheduleBlockPragmaAccess().getLeftParenthesisKeyword_1());
            		
            // InternalPragmas.g:1219:3: ( (lv_exprs_2_0= ruleExpression ) )
            // InternalPragmas.g:1220:4: (lv_exprs_2_0= ruleExpression )
            {
            // InternalPragmas.g:1220:4: (lv_exprs_2_0= ruleExpression )
            // InternalPragmas.g:1221:5: lv_exprs_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getScheduleBlockPragmaAccess().getExprsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_16);
            lv_exprs_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScheduleBlockPragmaRule());
            					}
            					add(
            						current,
            						"exprs",
            						lv_exprs_2_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:1238:3: (otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==17) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalPragmas.g:1239:4: otherlv_3= ',' ( (lv_exprs_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,17,FOLLOW_29); 

            	    				newLeafNode(otherlv_3, grammarAccess.getScheduleBlockPragmaAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalPragmas.g:1243:4: ( (lv_exprs_4_0= ruleExpression ) )
            	    // InternalPragmas.g:1244:5: (lv_exprs_4_0= ruleExpression )
            	    {
            	    // InternalPragmas.g:1244:5: (lv_exprs_4_0= ruleExpression )
            	    // InternalPragmas.g:1245:6: lv_exprs_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getScheduleBlockPragmaAccess().getExprsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_16);
            	    lv_exprs_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getScheduleBlockPragmaRule());
            	    						}
            	    						add(
            	    							current,
            	    							"exprs",
            	    							lv_exprs_4_0,
            	    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            otherlv_5=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getScheduleBlockPragmaAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScheduleBlockPragma"


    // $ANTLR start "entryRuleCloogOptionPragma"
    // InternalPragmas.g:1271:1: entryRuleCloogOptionPragma returns [EObject current=null] : iv_ruleCloogOptionPragma= ruleCloogOptionPragma EOF ;
    public final EObject entryRuleCloogOptionPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCloogOptionPragma = null;


        try {
            // InternalPragmas.g:1271:58: (iv_ruleCloogOptionPragma= ruleCloogOptionPragma EOF )
            // InternalPragmas.g:1272:2: iv_ruleCloogOptionPragma= ruleCloogOptionPragma EOF
            {
             newCompositeNode(grammarAccess.getCloogOptionPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCloogOptionPragma=ruleCloogOptionPragma();

            state._fsp--;

             current =iv_ruleCloogOptionPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCloogOptionPragma"


    // $ANTLR start "ruleCloogOptionPragma"
    // InternalPragmas.g:1278:1: ruleCloogOptionPragma returns [EObject current=null] : ( () otherlv_1= 'scop_cloog_options' (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )? (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )? (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )? (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )? (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )? (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )? (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )? (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )? (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )? (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )? ) ;
    public final EObject ruleCloogOptionPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        AntlrDatatypeRuleToken lv_equalitySpreading_4_0 = null;

        AntlrDatatypeRuleToken lv_stop_7_0 = null;

        AntlrDatatypeRuleToken lv_firstDepthToOptimize_10_0 = null;

        AntlrDatatypeRuleToken lv_firstDepthSpreading_13_0 = null;

        AntlrDatatypeRuleToken lv_strides_16_0 = null;

        AntlrDatatypeRuleToken lv_lastDepthToOptimize_19_0 = null;

        AntlrDatatypeRuleToken lv_block_22_0 = null;

        AntlrDatatypeRuleToken lv_constantSpreading_25_0 = null;

        AntlrDatatypeRuleToken lv_onceTimeLoopElim_28_0 = null;

        AntlrDatatypeRuleToken lv_coalescingDepth_31_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1284:2: ( ( () otherlv_1= 'scop_cloog_options' (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )? (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )? (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )? (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )? (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )? (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )? (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )? (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )? (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )? (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )? ) )
            // InternalPragmas.g:1285:2: ( () otherlv_1= 'scop_cloog_options' (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )? (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )? (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )? (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )? (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )? (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )? (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )? (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )? (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )? (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )? )
            {
            // InternalPragmas.g:1285:2: ( () otherlv_1= 'scop_cloog_options' (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )? (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )? (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )? (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )? (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )? (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )? (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )? (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )? (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )? (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )? )
            // InternalPragmas.g:1286:3: () otherlv_1= 'scop_cloog_options' (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )? (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )? (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )? (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )? (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )? (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )? (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )? (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )? (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )? (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )?
            {
            // InternalPragmas.g:1286:3: ()
            // InternalPragmas.g:1287:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCloogOptionPragmaAccess().getCloogOptionsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,44,FOLLOW_32); 

            			newLeafNode(otherlv_1, grammarAccess.getCloogOptionPragmaAccess().getScop_cloog_optionsKeyword_1());
            		
            // InternalPragmas.g:1297:3: (otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==45) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalPragmas.g:1298:4: otherlv_2= 'equalitySpreading' otherlv_3= '=' ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) )
                    {
                    otherlv_2=(Token)match(input,45,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getCloogOptionPragmaAccess().getEqualitySpreadingKeyword_2_0());
                    			
                    otherlv_3=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_3, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_2_1());
                    			
                    // InternalPragmas.g:1306:4: ( (lv_equalitySpreading_4_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1307:5: (lv_equalitySpreading_4_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1307:5: (lv_equalitySpreading_4_0= ruleINT_TYPE )
                    // InternalPragmas.g:1308:6: lv_equalitySpreading_4_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getEqualitySpreadingINT_TYPEParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_33);
                    lv_equalitySpreading_4_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"equalitySpreading",
                    							lv_equalitySpreading_4_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1326:3: (otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==46) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalPragmas.g:1327:4: otherlv_5= 'stop' otherlv_6= '=' ( (lv_stop_7_0= ruleINT_TYPE ) )
                    {
                    otherlv_5=(Token)match(input,46,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getCloogOptionPragmaAccess().getStopKeyword_3_0());
                    			
                    otherlv_6=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_6, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_3_1());
                    			
                    // InternalPragmas.g:1335:4: ( (lv_stop_7_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1336:5: (lv_stop_7_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1336:5: (lv_stop_7_0= ruleINT_TYPE )
                    // InternalPragmas.g:1337:6: lv_stop_7_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getStopINT_TYPEParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_34);
                    lv_stop_7_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"stop",
                    							lv_stop_7_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1355:3: (otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==47) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalPragmas.g:1356:4: otherlv_8= 'firstDepthToOptimize' otherlv_9= '=' ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) )
                    {
                    otherlv_8=(Token)match(input,47,FOLLOW_5); 

                    				newLeafNode(otherlv_8, grammarAccess.getCloogOptionPragmaAccess().getFirstDepthToOptimizeKeyword_4_0());
                    			
                    otherlv_9=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_9, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_4_1());
                    			
                    // InternalPragmas.g:1364:4: ( (lv_firstDepthToOptimize_10_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1365:5: (lv_firstDepthToOptimize_10_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1365:5: (lv_firstDepthToOptimize_10_0= ruleINT_TYPE )
                    // InternalPragmas.g:1366:6: lv_firstDepthToOptimize_10_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getFirstDepthToOptimizeINT_TYPEParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_35);
                    lv_firstDepthToOptimize_10_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"firstDepthToOptimize",
                    							lv_firstDepthToOptimize_10_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1384:3: (otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==48) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalPragmas.g:1385:4: otherlv_11= 'firstDepthSpreading' otherlv_12= '=' ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) )
                    {
                    otherlv_11=(Token)match(input,48,FOLLOW_5); 

                    				newLeafNode(otherlv_11, grammarAccess.getCloogOptionPragmaAccess().getFirstDepthSpreadingKeyword_5_0());
                    			
                    otherlv_12=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_12, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_5_1());
                    			
                    // InternalPragmas.g:1393:4: ( (lv_firstDepthSpreading_13_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1394:5: (lv_firstDepthSpreading_13_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1394:5: (lv_firstDepthSpreading_13_0= ruleINT_TYPE )
                    // InternalPragmas.g:1395:6: lv_firstDepthSpreading_13_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getFirstDepthSpreadingINT_TYPEParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_firstDepthSpreading_13_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"firstDepthSpreading",
                    							lv_firstDepthSpreading_13_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1413:3: (otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==49) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalPragmas.g:1414:4: otherlv_14= 'strides' otherlv_15= '=' ( (lv_strides_16_0= ruleINT_TYPE ) )
                    {
                    otherlv_14=(Token)match(input,49,FOLLOW_5); 

                    				newLeafNode(otherlv_14, grammarAccess.getCloogOptionPragmaAccess().getStridesKeyword_6_0());
                    			
                    otherlv_15=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_15, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_6_1());
                    			
                    // InternalPragmas.g:1422:4: ( (lv_strides_16_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1423:5: (lv_strides_16_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1423:5: (lv_strides_16_0= ruleINT_TYPE )
                    // InternalPragmas.g:1424:6: lv_strides_16_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getStridesINT_TYPEParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_strides_16_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"strides",
                    							lv_strides_16_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1442:3: (otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==50) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalPragmas.g:1443:4: otherlv_17= 'lastDepthToOptimize' otherlv_18= '=' ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) )
                    {
                    otherlv_17=(Token)match(input,50,FOLLOW_5); 

                    				newLeafNode(otherlv_17, grammarAccess.getCloogOptionPragmaAccess().getLastDepthToOptimizeKeyword_7_0());
                    			
                    otherlv_18=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_18, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_7_1());
                    			
                    // InternalPragmas.g:1451:4: ( (lv_lastDepthToOptimize_19_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1452:5: (lv_lastDepthToOptimize_19_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1452:5: (lv_lastDepthToOptimize_19_0= ruleINT_TYPE )
                    // InternalPragmas.g:1453:6: lv_lastDepthToOptimize_19_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getLastDepthToOptimizeINT_TYPEParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_38);
                    lv_lastDepthToOptimize_19_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"lastDepthToOptimize",
                    							lv_lastDepthToOptimize_19_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1471:3: (otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==51) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalPragmas.g:1472:4: otherlv_20= 'block' otherlv_21= '=' ( (lv_block_22_0= ruleINT_TYPE ) )
                    {
                    otherlv_20=(Token)match(input,51,FOLLOW_5); 

                    				newLeafNode(otherlv_20, grammarAccess.getCloogOptionPragmaAccess().getBlockKeyword_8_0());
                    			
                    otherlv_21=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_21, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_8_1());
                    			
                    // InternalPragmas.g:1480:4: ( (lv_block_22_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1481:5: (lv_block_22_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1481:5: (lv_block_22_0= ruleINT_TYPE )
                    // InternalPragmas.g:1482:6: lv_block_22_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getBlockINT_TYPEParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_block_22_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"block",
                    							lv_block_22_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1500:3: (otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==52) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalPragmas.g:1501:4: otherlv_23= 'constantSpreading' otherlv_24= '=' ( (lv_constantSpreading_25_0= ruleINT_TYPE ) )
                    {
                    otherlv_23=(Token)match(input,52,FOLLOW_5); 

                    				newLeafNode(otherlv_23, grammarAccess.getCloogOptionPragmaAccess().getConstantSpreadingKeyword_9_0());
                    			
                    otherlv_24=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_24, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_9_1());
                    			
                    // InternalPragmas.g:1509:4: ( (lv_constantSpreading_25_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1510:5: (lv_constantSpreading_25_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1510:5: (lv_constantSpreading_25_0= ruleINT_TYPE )
                    // InternalPragmas.g:1511:6: lv_constantSpreading_25_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getConstantSpreadingINT_TYPEParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_constantSpreading_25_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"constantSpreading",
                    							lv_constantSpreading_25_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1529:3: (otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==53) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalPragmas.g:1530:4: otherlv_26= 'onceTimeLoopElim' otherlv_27= '=' ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) )
                    {
                    otherlv_26=(Token)match(input,53,FOLLOW_5); 

                    				newLeafNode(otherlv_26, grammarAccess.getCloogOptionPragmaAccess().getOnceTimeLoopElimKeyword_10_0());
                    			
                    otherlv_27=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_27, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_10_1());
                    			
                    // InternalPragmas.g:1538:4: ( (lv_onceTimeLoopElim_28_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1539:5: (lv_onceTimeLoopElim_28_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1539:5: (lv_onceTimeLoopElim_28_0= ruleINT_TYPE )
                    // InternalPragmas.g:1540:6: lv_onceTimeLoopElim_28_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getOnceTimeLoopElimINT_TYPEParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_onceTimeLoopElim_28_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"onceTimeLoopElim",
                    							lv_onceTimeLoopElim_28_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPragmas.g:1558:3: (otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==54) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalPragmas.g:1559:4: otherlv_29= 'coalescingDepth' otherlv_30= '=' ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) )
                    {
                    otherlv_29=(Token)match(input,54,FOLLOW_5); 

                    				newLeafNode(otherlv_29, grammarAccess.getCloogOptionPragmaAccess().getCoalescingDepthKeyword_11_0());
                    			
                    otherlv_30=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_30, grammarAccess.getCloogOptionPragmaAccess().getEqualsSignKeyword_11_1());
                    			
                    // InternalPragmas.g:1567:4: ( (lv_coalescingDepth_31_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1568:5: (lv_coalescingDepth_31_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1568:5: (lv_coalescingDepth_31_0= ruleINT_TYPE )
                    // InternalPragmas.g:1569:6: lv_coalescingDepth_31_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getCloogOptionPragmaAccess().getCoalescingDepthINT_TYPEParserRuleCall_11_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_coalescingDepth_31_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCloogOptionPragmaRule());
                    						}
                    						set(
                    							current,
                    							"coalescingDepth",
                    							lv_coalescingDepth_31_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCloogOptionPragma"


    // $ANTLR start "entryRuleDuplicateNodePragma"
    // InternalPragmas.g:1591:1: entryRuleDuplicateNodePragma returns [EObject current=null] : iv_ruleDuplicateNodePragma= ruleDuplicateNodePragma EOF ;
    public final EObject entryRuleDuplicateNodePragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDuplicateNodePragma = null;


        try {
            // InternalPragmas.g:1591:60: (iv_ruleDuplicateNodePragma= ruleDuplicateNodePragma EOF )
            // InternalPragmas.g:1592:2: iv_ruleDuplicateNodePragma= ruleDuplicateNodePragma EOF
            {
             newCompositeNode(grammarAccess.getDuplicateNodePragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDuplicateNodePragma=ruleDuplicateNodePragma();

            state._fsp--;

             current =iv_ruleDuplicateNodePragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDuplicateNodePragma"


    // $ANTLR start "ruleDuplicateNodePragma"
    // InternalPragmas.g:1598:1: ruleDuplicateNodePragma returns [EObject current=null] : (otherlv_0= 'scop_duplicate' ( (otherlv_1= RULE_ID ) ) ( (lv_factor_2_0= ruleINT_TYPE ) ) ) ;
    public final EObject ruleDuplicateNodePragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_factor_2_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1604:2: ( (otherlv_0= 'scop_duplicate' ( (otherlv_1= RULE_ID ) ) ( (lv_factor_2_0= ruleINT_TYPE ) ) ) )
            // InternalPragmas.g:1605:2: (otherlv_0= 'scop_duplicate' ( (otherlv_1= RULE_ID ) ) ( (lv_factor_2_0= ruleINT_TYPE ) ) )
            {
            // InternalPragmas.g:1605:2: (otherlv_0= 'scop_duplicate' ( (otherlv_1= RULE_ID ) ) ( (lv_factor_2_0= ruleINT_TYPE ) ) )
            // InternalPragmas.g:1606:3: otherlv_0= 'scop_duplicate' ( (otherlv_1= RULE_ID ) ) ( (lv_factor_2_0= ruleINT_TYPE ) )
            {
            otherlv_0=(Token)match(input,55,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getDuplicateNodePragmaAccess().getScop_duplicateKeyword_0());
            		
            // InternalPragmas.g:1610:3: ( (otherlv_1= RULE_ID ) )
            // InternalPragmas.g:1611:4: (otherlv_1= RULE_ID )
            {
            // InternalPragmas.g:1611:4: (otherlv_1= RULE_ID )
            // InternalPragmas.g:1612:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDuplicateNodePragmaRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(otherlv_1, grammarAccess.getDuplicateNodePragmaAccess().getSymbolSymbolCrossReference_1_0());
            				

            }


            }

            // InternalPragmas.g:1623:3: ( (lv_factor_2_0= ruleINT_TYPE ) )
            // InternalPragmas.g:1624:4: (lv_factor_2_0= ruleINT_TYPE )
            {
            // InternalPragmas.g:1624:4: (lv_factor_2_0= ruleINT_TYPE )
            // InternalPragmas.g:1625:5: lv_factor_2_0= ruleINT_TYPE
            {

            					newCompositeNode(grammarAccess.getDuplicateNodePragmaAccess().getFactorINT_TYPEParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_factor_2_0=ruleINT_TYPE();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDuplicateNodePragmaRule());
            					}
            					set(
            						current,
            						"factor",
            						lv_factor_2_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDuplicateNodePragma"


    // $ANTLR start "entryRuleDataMotionPragma"
    // InternalPragmas.g:1646:1: entryRuleDataMotionPragma returns [EObject current=null] : iv_ruleDataMotionPragma= ruleDataMotionPragma EOF ;
    public final EObject entryRuleDataMotionPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataMotionPragma = null;


        try {
            // InternalPragmas.g:1646:57: (iv_ruleDataMotionPragma= ruleDataMotionPragma EOF )
            // InternalPragmas.g:1647:2: iv_ruleDataMotionPragma= ruleDataMotionPragma EOF
            {
             newCompositeNode(grammarAccess.getDataMotionPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataMotionPragma=ruleDataMotionPragma();

            state._fsp--;

             current =iv_ruleDataMotionPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataMotionPragma"


    // $ANTLR start "ruleDataMotionPragma"
    // InternalPragmas.g:1653:1: ruleDataMotionPragma returns [EObject current=null] : (otherlv_0= 'scop_data_motion' (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )? ) ;
    public final EObject ruleDataMotionPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalPragmas.g:1659:2: ( (otherlv_0= 'scop_data_motion' (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )? ) )
            // InternalPragmas.g:1660:2: (otherlv_0= 'scop_data_motion' (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )? )
            {
            // InternalPragmas.g:1660:2: (otherlv_0= 'scop_data_motion' (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )? )
            // InternalPragmas.g:1661:3: otherlv_0= 'scop_data_motion' (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )?
            {
            otherlv_0=(Token)match(input,56,FOLLOW_42); 

            			newLeafNode(otherlv_0, grammarAccess.getDataMotionPragmaAccess().getScop_data_motionKeyword_0());
            		
            // InternalPragmas.g:1665:3: (otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==25) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalPragmas.g:1666:4: otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= ')'
                    {
                    otherlv_1=(Token)match(input,25,FOLLOW_15); 

                    				newLeafNode(otherlv_1, grammarAccess.getDataMotionPragmaAccess().getLeftParenthesisKeyword_1_0());
                    			
                    // InternalPragmas.g:1670:4: ( (otherlv_2= RULE_ID ) )
                    // InternalPragmas.g:1671:5: (otherlv_2= RULE_ID )
                    {
                    // InternalPragmas.g:1671:5: (otherlv_2= RULE_ID )
                    // InternalPragmas.g:1672:6: otherlv_2= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDataMotionPragmaRule());
                    						}
                    					
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_16); 

                    						newLeafNode(otherlv_2, grammarAccess.getDataMotionPragmaAccess().getSymbolsSymbolCrossReference_1_1_0());
                    					

                    }


                    }

                    // InternalPragmas.g:1683:4: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==17) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalPragmas.g:1684:5: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
                    	    {
                    	    otherlv_3=(Token)match(input,17,FOLLOW_15); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getDataMotionPragmaAccess().getCommaKeyword_1_2_0());
                    	    				
                    	    // InternalPragmas.g:1688:5: ( (otherlv_4= RULE_ID ) )
                    	    // InternalPragmas.g:1689:6: (otherlv_4= RULE_ID )
                    	    {
                    	    // InternalPragmas.g:1689:6: (otherlv_4= RULE_ID )
                    	    // InternalPragmas.g:1690:7: otherlv_4= RULE_ID
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getDataMotionPragmaRule());
                    	    							}
                    	    						
                    	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_16); 

                    	    							newLeafNode(otherlv_4, grammarAccess.getDataMotionPragmaAccess().getSymbolsSymbolCrossReference_1_2_1_0());
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,26,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getDataMotionPragmaAccess().getRightParenthesisKeyword_1_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataMotionPragma"


    // $ANTLR start "entryRuleSlicePragma"
    // InternalPragmas.g:1711:1: entryRuleSlicePragma returns [EObject current=null] : iv_ruleSlicePragma= ruleSlicePragma EOF ;
    public final EObject entryRuleSlicePragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSlicePragma = null;


        try {
            // InternalPragmas.g:1711:52: (iv_ruleSlicePragma= ruleSlicePragma EOF )
            // InternalPragmas.g:1712:2: iv_ruleSlicePragma= ruleSlicePragma EOF
            {
             newCompositeNode(grammarAccess.getSlicePragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSlicePragma=ruleSlicePragma();

            state._fsp--;

             current =iv_ruleSlicePragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSlicePragma"


    // $ANTLR start "ruleSlicePragma"
    // InternalPragmas.g:1718:1: ruleSlicePragma returns [EObject current=null] : (otherlv_0= 'scop_slice' (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )? (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )? ) ;
    public final EObject ruleSlicePragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_sizes_4_0 = null;

        AntlrDatatypeRuleToken lv_sizes_6_0 = null;

        AntlrDatatypeRuleToken lv_unrolls_11_0 = null;

        AntlrDatatypeRuleToken lv_unrolls_13_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1724:2: ( (otherlv_0= 'scop_slice' (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )? (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )? ) )
            // InternalPragmas.g:1725:2: (otherlv_0= 'scop_slice' (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )? (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )? )
            {
            // InternalPragmas.g:1725:2: (otherlv_0= 'scop_slice' (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )? (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )? )
            // InternalPragmas.g:1726:3: otherlv_0= 'scop_slice' (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )? (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )?
            {
            otherlv_0=(Token)match(input,57,FOLLOW_43); 

            			newLeafNode(otherlv_0, grammarAccess.getSlicePragmaAccess().getScop_sliceKeyword_0());
            		
            // InternalPragmas.g:1730:3: (otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')' )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==58) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalPragmas.g:1731:4: otherlv_1= 'sizes' otherlv_2= '=' otherlv_3= '(' ( (lv_sizes_4_0= ruleINT_TYPE ) ) (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )* otherlv_7= ')'
                    {
                    otherlv_1=(Token)match(input,58,FOLLOW_5); 

                    				newLeafNode(otherlv_1, grammarAccess.getSlicePragmaAccess().getSizesKeyword_1_0());
                    			
                    otherlv_2=(Token)match(input,15,FOLLOW_14); 

                    				newLeafNode(otherlv_2, grammarAccess.getSlicePragmaAccess().getEqualsSignKeyword_1_1());
                    			
                    otherlv_3=(Token)match(input,25,FOLLOW_6); 

                    				newLeafNode(otherlv_3, grammarAccess.getSlicePragmaAccess().getLeftParenthesisKeyword_1_2());
                    			
                    // InternalPragmas.g:1743:4: ( (lv_sizes_4_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1744:5: (lv_sizes_4_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1744:5: (lv_sizes_4_0= ruleINT_TYPE )
                    // InternalPragmas.g:1745:6: lv_sizes_4_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getSlicePragmaAccess().getSizesINT_TYPEParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_sizes_4_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSlicePragmaRule());
                    						}
                    						add(
                    							current,
                    							"sizes",
                    							lv_sizes_4_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPragmas.g:1762:4: (otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) ) )*
                    loop36:
                    do {
                        int alt36=2;
                        int LA36_0 = input.LA(1);

                        if ( (LA36_0==17) ) {
                            alt36=1;
                        }


                        switch (alt36) {
                    	case 1 :
                    	    // InternalPragmas.g:1763:5: otherlv_5= ',' ( (lv_sizes_6_0= ruleINT_TYPE ) )
                    	    {
                    	    otherlv_5=(Token)match(input,17,FOLLOW_6); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getSlicePragmaAccess().getCommaKeyword_1_4_0());
                    	    				
                    	    // InternalPragmas.g:1767:5: ( (lv_sizes_6_0= ruleINT_TYPE ) )
                    	    // InternalPragmas.g:1768:6: (lv_sizes_6_0= ruleINT_TYPE )
                    	    {
                    	    // InternalPragmas.g:1768:6: (lv_sizes_6_0= ruleINT_TYPE )
                    	    // InternalPragmas.g:1769:7: lv_sizes_6_0= ruleINT_TYPE
                    	    {

                    	    							newCompositeNode(grammarAccess.getSlicePragmaAccess().getSizesINT_TYPEParserRuleCall_1_4_1_0());
                    	    						
                    	    pushFollow(FOLLOW_16);
                    	    lv_sizes_6_0=ruleINT_TYPE();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSlicePragmaRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"sizes",
                    	    								lv_sizes_6_0,
                    	    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop36;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,26,FOLLOW_44); 

                    				newLeafNode(otherlv_7, grammarAccess.getSlicePragmaAccess().getRightParenthesisKeyword_1_5());
                    			

                    }
                    break;

            }

            // InternalPragmas.g:1792:3: (otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')' )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==59) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalPragmas.g:1793:4: otherlv_8= 'unrolls' otherlv_9= '=' otherlv_10= '(' ( (lv_unrolls_11_0= ruleINT_TYPE ) ) (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )* otherlv_14= ')'
                    {
                    otherlv_8=(Token)match(input,59,FOLLOW_5); 

                    				newLeafNode(otherlv_8, grammarAccess.getSlicePragmaAccess().getUnrollsKeyword_2_0());
                    			
                    otherlv_9=(Token)match(input,15,FOLLOW_14); 

                    				newLeafNode(otherlv_9, grammarAccess.getSlicePragmaAccess().getEqualsSignKeyword_2_1());
                    			
                    otherlv_10=(Token)match(input,25,FOLLOW_6); 

                    				newLeafNode(otherlv_10, grammarAccess.getSlicePragmaAccess().getLeftParenthesisKeyword_2_2());
                    			
                    // InternalPragmas.g:1805:4: ( (lv_unrolls_11_0= ruleINT_TYPE ) )
                    // InternalPragmas.g:1806:5: (lv_unrolls_11_0= ruleINT_TYPE )
                    {
                    // InternalPragmas.g:1806:5: (lv_unrolls_11_0= ruleINT_TYPE )
                    // InternalPragmas.g:1807:6: lv_unrolls_11_0= ruleINT_TYPE
                    {

                    						newCompositeNode(grammarAccess.getSlicePragmaAccess().getUnrollsINT_TYPEParserRuleCall_2_3_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_unrolls_11_0=ruleINT_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSlicePragmaRule());
                    						}
                    						add(
                    							current,
                    							"unrolls",
                    							lv_unrolls_11_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPragmas.g:1824:4: (otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) ) )*
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( (LA38_0==17) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // InternalPragmas.g:1825:5: otherlv_12= ',' ( (lv_unrolls_13_0= ruleINT_TYPE ) )
                    	    {
                    	    otherlv_12=(Token)match(input,17,FOLLOW_6); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getSlicePragmaAccess().getCommaKeyword_2_4_0());
                    	    				
                    	    // InternalPragmas.g:1829:5: ( (lv_unrolls_13_0= ruleINT_TYPE ) )
                    	    // InternalPragmas.g:1830:6: (lv_unrolls_13_0= ruleINT_TYPE )
                    	    {
                    	    // InternalPragmas.g:1830:6: (lv_unrolls_13_0= ruleINT_TYPE )
                    	    // InternalPragmas.g:1831:7: lv_unrolls_13_0= ruleINT_TYPE
                    	    {

                    	    							newCompositeNode(grammarAccess.getSlicePragmaAccess().getUnrollsINT_TYPEParserRuleCall_2_4_1_0());
                    	    						
                    	    pushFollow(FOLLOW_16);
                    	    lv_unrolls_13_0=ruleINT_TYPE();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSlicePragmaRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"unrolls",
                    	    								lv_unrolls_13_0,
                    	    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop38;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,26,FOLLOW_2); 

                    				newLeafNode(otherlv_14, grammarAccess.getSlicePragmaAccess().getRightParenthesisKeyword_2_5());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSlicePragma"


    // $ANTLR start "entryRuleMergeArraysPragma"
    // InternalPragmas.g:1858:1: entryRuleMergeArraysPragma returns [EObject current=null] : iv_ruleMergeArraysPragma= ruleMergeArraysPragma EOF ;
    public final EObject entryRuleMergeArraysPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMergeArraysPragma = null;


        try {
            // InternalPragmas.g:1858:58: (iv_ruleMergeArraysPragma= ruleMergeArraysPragma EOF )
            // InternalPragmas.g:1859:2: iv_ruleMergeArraysPragma= ruleMergeArraysPragma EOF
            {
             newCompositeNode(grammarAccess.getMergeArraysPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMergeArraysPragma=ruleMergeArraysPragma();

            state._fsp--;

             current =iv_ruleMergeArraysPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMergeArraysPragma"


    // $ANTLR start "ruleMergeArraysPragma"
    // InternalPragmas.g:1865:1: ruleMergeArraysPragma returns [EObject current=null] : (otherlv_0= 'scop_merge_arrays' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) ) otherlv_5= ')' ) ;
    public final EObject ruleMergeArraysPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalPragmas.g:1871:2: ( (otherlv_0= 'scop_merge_arrays' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) ) otherlv_5= ')' ) )
            // InternalPragmas.g:1872:2: (otherlv_0= 'scop_merge_arrays' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) ) otherlv_5= ')' )
            {
            // InternalPragmas.g:1872:2: (otherlv_0= 'scop_merge_arrays' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) ) otherlv_5= ')' )
            // InternalPragmas.g:1873:3: otherlv_0= 'scop_merge_arrays' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) ) otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,60,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getMergeArraysPragmaAccess().getScop_merge_arraysKeyword_0());
            		
            otherlv_1=(Token)match(input,25,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getMergeArraysPragmaAccess().getLeftParenthesisKeyword_1());
            		
            // InternalPragmas.g:1881:3: ( (otherlv_2= RULE_ID ) )
            // InternalPragmas.g:1882:4: (otherlv_2= RULE_ID )
            {
            // InternalPragmas.g:1882:4: (otherlv_2= RULE_ID )
            // InternalPragmas.g:1883:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMergeArraysPragmaRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_45); 

            					newLeafNode(otherlv_2, grammarAccess.getMergeArraysPragmaAccess().getSymbolsSymbolCrossReference_2_0());
            				

            }


            }

            // InternalPragmas.g:1894:3: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )
            // InternalPragmas.g:1895:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            {
            otherlv_3=(Token)match(input,17,FOLLOW_15); 

            				newLeafNode(otherlv_3, grammarAccess.getMergeArraysPragmaAccess().getCommaKeyword_3_0());
            			
            // InternalPragmas.g:1899:4: ( (otherlv_4= RULE_ID ) )
            // InternalPragmas.g:1900:5: (otherlv_4= RULE_ID )
            {
            // InternalPragmas.g:1900:5: (otherlv_4= RULE_ID )
            // InternalPragmas.g:1901:6: otherlv_4= RULE_ID
            {

            						if (current==null) {
            							current = createModelElement(grammarAccess.getMergeArraysPragmaRule());
            						}
            					
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_21); 

            						newLeafNode(otherlv_4, grammarAccess.getMergeArraysPragmaAccess().getSymbolsSymbolCrossReference_3_1_0());
            					

            }


            }


            }

            otherlv_5=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getMergeArraysPragmaAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMergeArraysPragma"


    // $ANTLR start "entryRuleArrayContractPragma"
    // InternalPragmas.g:1921:1: entryRuleArrayContractPragma returns [EObject current=null] : iv_ruleArrayContractPragma= ruleArrayContractPragma EOF ;
    public final EObject entryRuleArrayContractPragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayContractPragma = null;


        try {
            // InternalPragmas.g:1921:60: (iv_ruleArrayContractPragma= ruleArrayContractPragma EOF )
            // InternalPragmas.g:1922:2: iv_ruleArrayContractPragma= ruleArrayContractPragma EOF
            {
             newCompositeNode(grammarAccess.getArrayContractPragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArrayContractPragma=ruleArrayContractPragma();

            state._fsp--;

             current =iv_ruleArrayContractPragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayContractPragma"


    // $ANTLR start "ruleArrayContractPragma"
    // InternalPragmas.g:1928:1: ruleArrayContractPragma returns [EObject current=null] : (otherlv_0= 'scop_contract_array' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleArrayContractPragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPragmas.g:1934:2: ( (otherlv_0= 'scop_contract_array' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalPragmas.g:1935:2: (otherlv_0= 'scop_contract_array' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalPragmas.g:1935:2: (otherlv_0= 'scop_contract_array' ( (otherlv_1= RULE_ID ) ) )
            // InternalPragmas.g:1936:3: otherlv_0= 'scop_contract_array' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,61,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getArrayContractPragmaAccess().getScop_contract_arrayKeyword_0());
            		
            // InternalPragmas.g:1940:3: ( (otherlv_1= RULE_ID ) )
            // InternalPragmas.g:1941:4: (otherlv_1= RULE_ID )
            {
            // InternalPragmas.g:1941:4: (otherlv_1= RULE_ID )
            // InternalPragmas.g:1942:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getArrayContractPragmaRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getArrayContractPragmaAccess().getSymbolSymbolCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayContractPragma"


    // $ANTLR start "entryRuleLoopPipelinePragma"
    // InternalPragmas.g:1957:1: entryRuleLoopPipelinePragma returns [EObject current=null] : iv_ruleLoopPipelinePragma= ruleLoopPipelinePragma EOF ;
    public final EObject entryRuleLoopPipelinePragma() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoopPipelinePragma = null;


        try {
            // InternalPragmas.g:1957:59: (iv_ruleLoopPipelinePragma= ruleLoopPipelinePragma EOF )
            // InternalPragmas.g:1958:2: iv_ruleLoopPipelinePragma= ruleLoopPipelinePragma EOF
            {
             newCompositeNode(grammarAccess.getLoopPipelinePragmaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLoopPipelinePragma=ruleLoopPipelinePragma();

            state._fsp--;

             current =iv_ruleLoopPipelinePragma; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoopPipelinePragma"


    // $ANTLR start "ruleLoopPipelinePragma"
    // InternalPragmas.g:1964:1: ruleLoopPipelinePragma returns [EObject current=null] : (otherlv_0= 'pipeline_loop' otherlv_1= 'latency' otherlv_2= '=' ( (lv_latency_3_0= ruleINT_TYPE ) ) (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )? ) ;
    public final EObject ruleLoopPipelinePragma() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_latency_3_0 = null;

        AntlrDatatypeRuleToken lv_hashFunction_6_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:1970:2: ( (otherlv_0= 'pipeline_loop' otherlv_1= 'latency' otherlv_2= '=' ( (lv_latency_3_0= ruleINT_TYPE ) ) (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )? ) )
            // InternalPragmas.g:1971:2: (otherlv_0= 'pipeline_loop' otherlv_1= 'latency' otherlv_2= '=' ( (lv_latency_3_0= ruleINT_TYPE ) ) (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )? )
            {
            // InternalPragmas.g:1971:2: (otherlv_0= 'pipeline_loop' otherlv_1= 'latency' otherlv_2= '=' ( (lv_latency_3_0= ruleINT_TYPE ) ) (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )? )
            // InternalPragmas.g:1972:3: otherlv_0= 'pipeline_loop' otherlv_1= 'latency' otherlv_2= '=' ( (lv_latency_3_0= ruleINT_TYPE ) ) (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )?
            {
            otherlv_0=(Token)match(input,62,FOLLOW_46); 

            			newLeafNode(otherlv_0, grammarAccess.getLoopPipelinePragmaAccess().getPipeline_loopKeyword_0());
            		
            otherlv_1=(Token)match(input,63,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getLoopPipelinePragmaAccess().getLatencyKeyword_1());
            		
            otherlv_2=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getLoopPipelinePragmaAccess().getEqualsSignKeyword_2());
            		
            // InternalPragmas.g:1984:3: ( (lv_latency_3_0= ruleINT_TYPE ) )
            // InternalPragmas.g:1985:4: (lv_latency_3_0= ruleINT_TYPE )
            {
            // InternalPragmas.g:1985:4: (lv_latency_3_0= ruleINT_TYPE )
            // InternalPragmas.g:1986:5: lv_latency_3_0= ruleINT_TYPE
            {

            					newCompositeNode(grammarAccess.getLoopPipelinePragmaAccess().getLatencyINT_TYPEParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_47);
            lv_latency_3_0=ruleINT_TYPE();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLoopPipelinePragmaRule());
            					}
            					set(
            						current,
            						"latency",
            						lv_latency_3_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_TYPE");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2003:3: (otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==64) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalPragmas.g:2004:4: otherlv_4= 'hash' otherlv_5= '=' ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) )
                    {
                    otherlv_4=(Token)match(input,64,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getLoopPipelinePragmaAccess().getHashKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,15,FOLLOW_48); 

                    				newLeafNode(otherlv_5, grammarAccess.getLoopPipelinePragmaAccess().getEqualsSignKeyword_4_1());
                    			
                    // InternalPragmas.g:2012:4: ( (lv_hashFunction_6_0= ruleSTRING_TYPE ) )
                    // InternalPragmas.g:2013:5: (lv_hashFunction_6_0= ruleSTRING_TYPE )
                    {
                    // InternalPragmas.g:2013:5: (lv_hashFunction_6_0= ruleSTRING_TYPE )
                    // InternalPragmas.g:2014:6: lv_hashFunction_6_0= ruleSTRING_TYPE
                    {

                    						newCompositeNode(grammarAccess.getLoopPipelinePragmaAccess().getHashFunctionSTRING_TYPEParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_hashFunction_6_0=ruleSTRING_TYPE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLoopPipelinePragmaRule());
                    						}
                    						set(
                    							current,
                    							"hashFunction",
                    							lv_hashFunction_6_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.STRING_TYPE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoopPipelinePragma"


    // $ANTLR start "entryRuleExpression"
    // InternalPragmas.g:2036:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalPragmas.g:2036:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalPragmas.g:2037:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalPragmas.g:2043:1: ruleExpression returns [EObject current=null] : (this_AffineExpression_0= ruleAffineExpression | this_QuasiAffineExpression_1= ruleQuasiAffineExpression ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AffineExpression_0 = null;

        EObject this_QuasiAffineExpression_1 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2049:2: ( (this_AffineExpression_0= ruleAffineExpression | this_QuasiAffineExpression_1= ruleQuasiAffineExpression ) )
            // InternalPragmas.g:2050:2: (this_AffineExpression_0= ruleAffineExpression | this_QuasiAffineExpression_1= ruleQuasiAffineExpression )
            {
            // InternalPragmas.g:2050:2: (this_AffineExpression_0= ruleAffineExpression | this_QuasiAffineExpression_1= ruleQuasiAffineExpression )
            int alt41=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA41_1 = input.LA(2);

                if ( ((LA41_1>=69 && LA41_1<=70)) ) {
                    alt41=2;
                }
                else if ( (LA41_1==EOF||LA41_1==15||LA41_1==17||LA41_1==26||LA41_1==29||LA41_1==65||(LA41_1>=71 && LA41_1<=74)) ) {
                    alt41=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 41, 1, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INTTOKEN:
                {
                switch ( input.LA(2) ) {
                case RULE_ID:
                    {
                    int LA41_6 = input.LA(3);

                    if ( ((LA41_6>=69 && LA41_6<=70)) ) {
                        alt41=2;
                    }
                    else if ( (LA41_6==EOF||LA41_6==15||LA41_6==17||LA41_6==26||LA41_6==29||LA41_6==65||(LA41_6>=71 && LA41_6<=74)) ) {
                        alt41=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 6, input);

                        throw nvae;
                    }
                    }
                    break;
                case 69:
                case 70:
                    {
                    alt41=2;
                    }
                    break;
                case EOF:
                case 15:
                case 17:
                case 26:
                case 29:
                case 65:
                case 71:
                case 72:
                case 73:
                case 74:
                    {
                    alt41=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 41, 2, input);

                    throw nvae;
                }

                }
                break;
            case 65:
                {
                int LA41_3 = input.LA(2);

                if ( (LA41_3==RULE_ID) ) {
                    int LA41_6 = input.LA(3);

                    if ( ((LA41_6>=69 && LA41_6<=70)) ) {
                        alt41=2;
                    }
                    else if ( (LA41_6==EOF||LA41_6==15||LA41_6==17||LA41_6==26||LA41_6==29||LA41_6==65||(LA41_6>=71 && LA41_6<=74)) ) {
                        alt41=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 6, input);

                        throw nvae;
                    }
                }
                else if ( (LA41_3==RULE_INTTOKEN) ) {
                    switch ( input.LA(3) ) {
                    case RULE_ID:
                        {
                        int LA41_6 = input.LA(4);

                        if ( ((LA41_6>=69 && LA41_6<=70)) ) {
                            alt41=2;
                        }
                        else if ( (LA41_6==EOF||LA41_6==15||LA41_6==17||LA41_6==26||LA41_6==29||LA41_6==65||(LA41_6>=71 && LA41_6<=74)) ) {
                            alt41=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 41, 6, input);

                            throw nvae;
                        }
                        }
                        break;
                    case 69:
                    case 70:
                        {
                        alt41=2;
                        }
                        break;
                    case EOF:
                    case 15:
                    case 17:
                    case 26:
                    case 29:
                    case 65:
                    case 71:
                    case 72:
                    case 73:
                    case 74:
                        {
                        alt41=1;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 7, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 41, 3, input);

                    throw nvae;
                }
                }
                break;
            case 25:
                {
                alt41=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }

            switch (alt41) {
                case 1 :
                    // InternalPragmas.g:2051:3: this_AffineExpression_0= ruleAffineExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getAffineExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AffineExpression_0=ruleAffineExpression();

                    state._fsp--;


                    			current = this_AffineExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2060:3: this_QuasiAffineExpression_1= ruleQuasiAffineExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getQuasiAffineExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuasiAffineExpression_1=ruleQuasiAffineExpression();

                    state._fsp--;


                    			current = this_QuasiAffineExpression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleAffineExpression"
    // InternalPragmas.g:2072:1: entryRuleAffineExpression returns [EObject current=null] : iv_ruleAffineExpression= ruleAffineExpression EOF ;
    public final EObject entryRuleAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAffineExpression = null;


        try {
            // InternalPragmas.g:2072:57: (iv_ruleAffineExpression= ruleAffineExpression EOF )
            // InternalPragmas.g:2073:2: iv_ruleAffineExpression= ruleAffineExpression EOF
            {
             newCompositeNode(grammarAccess.getAffineExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAffineExpression=ruleAffineExpression();

            state._fsp--;

             current =iv_ruleAffineExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAffineExpression"


    // $ANTLR start "ruleAffineExpression"
    // InternalPragmas.g:2079:1: ruleAffineExpression returns [EObject current=null] : ( ( (lv_terms_0_0= ruleFirstTerm ) ) ( (lv_terms_1_0= ruleNextTerm ) )* ) ;
    public final EObject ruleAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_terms_0_0 = null;

        EObject lv_terms_1_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2085:2: ( ( ( (lv_terms_0_0= ruleFirstTerm ) ) ( (lv_terms_1_0= ruleNextTerm ) )* ) )
            // InternalPragmas.g:2086:2: ( ( (lv_terms_0_0= ruleFirstTerm ) ) ( (lv_terms_1_0= ruleNextTerm ) )* )
            {
            // InternalPragmas.g:2086:2: ( ( (lv_terms_0_0= ruleFirstTerm ) ) ( (lv_terms_1_0= ruleNextTerm ) )* )
            // InternalPragmas.g:2087:3: ( (lv_terms_0_0= ruleFirstTerm ) ) ( (lv_terms_1_0= ruleNextTerm ) )*
            {
            // InternalPragmas.g:2087:3: ( (lv_terms_0_0= ruleFirstTerm ) )
            // InternalPragmas.g:2088:4: (lv_terms_0_0= ruleFirstTerm )
            {
            // InternalPragmas.g:2088:4: (lv_terms_0_0= ruleFirstTerm )
            // InternalPragmas.g:2089:5: lv_terms_0_0= ruleFirstTerm
            {

            					newCompositeNode(grammarAccess.getAffineExpressionAccess().getTermsFirstTermParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_49);
            lv_terms_0_0=ruleFirstTerm();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAffineExpressionRule());
            					}
            					add(
            						current,
            						"terms",
            						lv_terms_0_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.FirstTerm");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2106:3: ( (lv_terms_1_0= ruleNextTerm ) )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==29||LA42_0==65) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalPragmas.g:2107:4: (lv_terms_1_0= ruleNextTerm )
            	    {
            	    // InternalPragmas.g:2107:4: (lv_terms_1_0= ruleNextTerm )
            	    // InternalPragmas.g:2108:5: lv_terms_1_0= ruleNextTerm
            	    {

            	    					newCompositeNode(grammarAccess.getAffineExpressionAccess().getTermsNextTermParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_49);
            	    lv_terms_1_0=ruleNextTerm();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAffineExpressionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"terms",
            	    						lv_terms_1_0,
            	    						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NextTerm");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAffineExpression"


    // $ANTLR start "entryRuleQuasiAffineExpression"
    // InternalPragmas.g:2129:1: entryRuleQuasiAffineExpression returns [EObject current=null] : iv_ruleQuasiAffineExpression= ruleQuasiAffineExpression EOF ;
    public final EObject entryRuleQuasiAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuasiAffineExpression = null;


        try {
            // InternalPragmas.g:2129:62: (iv_ruleQuasiAffineExpression= ruleQuasiAffineExpression EOF )
            // InternalPragmas.g:2130:2: iv_ruleQuasiAffineExpression= ruleQuasiAffineExpression EOF
            {
             newCompositeNode(grammarAccess.getQuasiAffineExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuasiAffineExpression=ruleQuasiAffineExpression();

            state._fsp--;

             current =iv_ruleQuasiAffineExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuasiAffineExpression"


    // $ANTLR start "ruleQuasiAffineExpression"
    // InternalPragmas.g:2136:1: ruleQuasiAffineExpression returns [EObject current=null] : ( (lv_terms_0_0= ruleQuasiAffineTerm ) ) ;
    public final EObject ruleQuasiAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_terms_0_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2142:2: ( ( (lv_terms_0_0= ruleQuasiAffineTerm ) ) )
            // InternalPragmas.g:2143:2: ( (lv_terms_0_0= ruleQuasiAffineTerm ) )
            {
            // InternalPragmas.g:2143:2: ( (lv_terms_0_0= ruleQuasiAffineTerm ) )
            // InternalPragmas.g:2144:3: (lv_terms_0_0= ruleQuasiAffineTerm )
            {
            // InternalPragmas.g:2144:3: (lv_terms_0_0= ruleQuasiAffineTerm )
            // InternalPragmas.g:2145:4: lv_terms_0_0= ruleQuasiAffineTerm
            {

            				newCompositeNode(grammarAccess.getQuasiAffineExpressionAccess().getTermsQuasiAffineTermParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_terms_0_0=ruleQuasiAffineTerm();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getQuasiAffineExpressionRule());
            				}
            				add(
            					current,
            					"terms",
            					lv_terms_0_0,
            					"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.QuasiAffineTerm");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuasiAffineExpression"


    // $ANTLR start "entryRuleQuasiAffineTerm"
    // InternalPragmas.g:2165:1: entryRuleQuasiAffineTerm returns [EObject current=null] : iv_ruleQuasiAffineTerm= ruleQuasiAffineTerm EOF ;
    public final EObject entryRuleQuasiAffineTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuasiAffineTerm = null;


        try {
            // InternalPragmas.g:2165:56: (iv_ruleQuasiAffineTerm= ruleQuasiAffineTerm EOF )
            // InternalPragmas.g:2166:2: iv_ruleQuasiAffineTerm= ruleQuasiAffineTerm EOF
            {
             newCompositeNode(grammarAccess.getQuasiAffineTermRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuasiAffineTerm=ruleQuasiAffineTerm();

            state._fsp--;

             current =iv_ruleQuasiAffineTerm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuasiAffineTerm"


    // $ANTLR start "ruleQuasiAffineTerm"
    // InternalPragmas.g:2172:1: ruleQuasiAffineTerm returns [EObject current=null] : ( ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) ) ( (lv_operator_1_0= ruleDivModOperator ) ) ( (lv_coef_2_0= ruleINT_VAL ) ) ) ;
    public final EObject ruleQuasiAffineTerm() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_1 = null;

        EObject lv_expression_0_2 = null;

        Enumerator lv_operator_1_0 = null;

        AntlrDatatypeRuleToken lv_coef_2_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2178:2: ( ( ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) ) ( (lv_operator_1_0= ruleDivModOperator ) ) ( (lv_coef_2_0= ruleINT_VAL ) ) ) )
            // InternalPragmas.g:2179:2: ( ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) ) ( (lv_operator_1_0= ruleDivModOperator ) ) ( (lv_coef_2_0= ruleINT_VAL ) ) )
            {
            // InternalPragmas.g:2179:2: ( ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) ) ( (lv_operator_1_0= ruleDivModOperator ) ) ( (lv_coef_2_0= ruleINT_VAL ) ) )
            // InternalPragmas.g:2180:3: ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) ) ( (lv_operator_1_0= ruleDivModOperator ) ) ( (lv_coef_2_0= ruleINT_VAL ) )
            {
            // InternalPragmas.g:2180:3: ( ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) ) )
            // InternalPragmas.g:2181:4: ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) )
            {
            // InternalPragmas.g:2181:4: ( (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression ) )
            // InternalPragmas.g:2182:5: (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression )
            {
            // InternalPragmas.g:2182:5: (lv_expression_0_1= ruleParenthesizedAffineExpression | lv_expression_0_2= ruleSingleTermAffineExpression )
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==25) ) {
                alt43=1;
            }
            else if ( ((LA43_0>=RULE_ID && LA43_0<=RULE_INTTOKEN)||LA43_0==65) ) {
                alt43=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }
            switch (alt43) {
                case 1 :
                    // InternalPragmas.g:2183:6: lv_expression_0_1= ruleParenthesizedAffineExpression
                    {

                    						newCompositeNode(grammarAccess.getQuasiAffineTermAccess().getExpressionParenthesizedAffineExpressionParserRuleCall_0_0_0());
                    					
                    pushFollow(FOLLOW_50);
                    lv_expression_0_1=ruleParenthesizedAffineExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getQuasiAffineTermRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_0_1,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.ParenthesizedAffineExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2199:6: lv_expression_0_2= ruleSingleTermAffineExpression
                    {

                    						newCompositeNode(grammarAccess.getQuasiAffineTermAccess().getExpressionSingleTermAffineExpressionParserRuleCall_0_0_1());
                    					
                    pushFollow(FOLLOW_50);
                    lv_expression_0_2=ruleSingleTermAffineExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getQuasiAffineTermRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_0_2,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.SingleTermAffineExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }
                    break;

            }


            }


            }

            // InternalPragmas.g:2217:3: ( (lv_operator_1_0= ruleDivModOperator ) )
            // InternalPragmas.g:2218:4: (lv_operator_1_0= ruleDivModOperator )
            {
            // InternalPragmas.g:2218:4: (lv_operator_1_0= ruleDivModOperator )
            // InternalPragmas.g:2219:5: lv_operator_1_0= ruleDivModOperator
            {

            					newCompositeNode(grammarAccess.getQuasiAffineTermAccess().getOperatorDivModOperatorEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_operator_1_0=ruleDivModOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuasiAffineTermRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_1_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.DivModOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2236:3: ( (lv_coef_2_0= ruleINT_VAL ) )
            // InternalPragmas.g:2237:4: (lv_coef_2_0= ruleINT_VAL )
            {
            // InternalPragmas.g:2237:4: (lv_coef_2_0= ruleINT_VAL )
            // InternalPragmas.g:2238:5: lv_coef_2_0= ruleINT_VAL
            {

            					newCompositeNode(grammarAccess.getQuasiAffineTermAccess().getCoefINT_VALParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_coef_2_0=ruleINT_VAL();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuasiAffineTermRule());
            					}
            					set(
            						current,
            						"coef",
            						lv_coef_2_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_VAL");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuasiAffineTerm"


    // $ANTLR start "entryRuleParenthesizedAffineExpression"
    // InternalPragmas.g:2259:1: entryRuleParenthesizedAffineExpression returns [EObject current=null] : iv_ruleParenthesizedAffineExpression= ruleParenthesizedAffineExpression EOF ;
    public final EObject entryRuleParenthesizedAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParenthesizedAffineExpression = null;


        try {
            // InternalPragmas.g:2259:70: (iv_ruleParenthesizedAffineExpression= ruleParenthesizedAffineExpression EOF )
            // InternalPragmas.g:2260:2: iv_ruleParenthesizedAffineExpression= ruleParenthesizedAffineExpression EOF
            {
             newCompositeNode(grammarAccess.getParenthesizedAffineExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParenthesizedAffineExpression=ruleParenthesizedAffineExpression();

            state._fsp--;

             current =iv_ruleParenthesizedAffineExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParenthesizedAffineExpression"


    // $ANTLR start "ruleParenthesizedAffineExpression"
    // InternalPragmas.g:2266:1: ruleParenthesizedAffineExpression returns [EObject current=null] : (otherlv_0= '(' ( (lv_terms_1_0= ruleFirstTerm ) ) ( (lv_terms_2_0= ruleNextTerm ) )* otherlv_3= ')' ) ;
    public final EObject ruleParenthesizedAffineExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject lv_terms_1_0 = null;

        EObject lv_terms_2_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2272:2: ( (otherlv_0= '(' ( (lv_terms_1_0= ruleFirstTerm ) ) ( (lv_terms_2_0= ruleNextTerm ) )* otherlv_3= ')' ) )
            // InternalPragmas.g:2273:2: (otherlv_0= '(' ( (lv_terms_1_0= ruleFirstTerm ) ) ( (lv_terms_2_0= ruleNextTerm ) )* otherlv_3= ')' )
            {
            // InternalPragmas.g:2273:2: (otherlv_0= '(' ( (lv_terms_1_0= ruleFirstTerm ) ) ( (lv_terms_2_0= ruleNextTerm ) )* otherlv_3= ')' )
            // InternalPragmas.g:2274:3: otherlv_0= '(' ( (lv_terms_1_0= ruleFirstTerm ) ) ( (lv_terms_2_0= ruleNextTerm ) )* otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_51); 

            			newLeafNode(otherlv_0, grammarAccess.getParenthesizedAffineExpressionAccess().getLeftParenthesisKeyword_0());
            		
            // InternalPragmas.g:2278:3: ( (lv_terms_1_0= ruleFirstTerm ) )
            // InternalPragmas.g:2279:4: (lv_terms_1_0= ruleFirstTerm )
            {
            // InternalPragmas.g:2279:4: (lv_terms_1_0= ruleFirstTerm )
            // InternalPragmas.g:2280:5: lv_terms_1_0= ruleFirstTerm
            {

            					newCompositeNode(grammarAccess.getParenthesizedAffineExpressionAccess().getTermsFirstTermParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_52);
            lv_terms_1_0=ruleFirstTerm();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParenthesizedAffineExpressionRule());
            					}
            					add(
            						current,
            						"terms",
            						lv_terms_1_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.FirstTerm");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2297:3: ( (lv_terms_2_0= ruleNextTerm ) )*
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==29||LA44_0==65) ) {
                    alt44=1;
                }


                switch (alt44) {
            	case 1 :
            	    // InternalPragmas.g:2298:4: (lv_terms_2_0= ruleNextTerm )
            	    {
            	    // InternalPragmas.g:2298:4: (lv_terms_2_0= ruleNextTerm )
            	    // InternalPragmas.g:2299:5: lv_terms_2_0= ruleNextTerm
            	    {

            	    					newCompositeNode(grammarAccess.getParenthesizedAffineExpressionAccess().getTermsNextTermParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_52);
            	    lv_terms_2_0=ruleNextTerm();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getParenthesizedAffineExpressionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"terms",
            	    						lv_terms_2_0,
            	    						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NextTerm");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);

            otherlv_3=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getParenthesizedAffineExpressionAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParenthesizedAffineExpression"


    // $ANTLR start "entryRuleSingleTermAffineExpression"
    // InternalPragmas.g:2324:1: entryRuleSingleTermAffineExpression returns [EObject current=null] : iv_ruleSingleTermAffineExpression= ruleSingleTermAffineExpression EOF ;
    public final EObject entryRuleSingleTermAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleTermAffineExpression = null;


        try {
            // InternalPragmas.g:2324:67: (iv_ruleSingleTermAffineExpression= ruleSingleTermAffineExpression EOF )
            // InternalPragmas.g:2325:2: iv_ruleSingleTermAffineExpression= ruleSingleTermAffineExpression EOF
            {
             newCompositeNode(grammarAccess.getSingleTermAffineExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleTermAffineExpression=ruleSingleTermAffineExpression();

            state._fsp--;

             current =iv_ruleSingleTermAffineExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleTermAffineExpression"


    // $ANTLR start "ruleSingleTermAffineExpression"
    // InternalPragmas.g:2331:1: ruleSingleTermAffineExpression returns [EObject current=null] : ( (lv_terms_0_0= ruleFirstTerm ) ) ;
    public final EObject ruleSingleTermAffineExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_terms_0_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2337:2: ( ( (lv_terms_0_0= ruleFirstTerm ) ) )
            // InternalPragmas.g:2338:2: ( (lv_terms_0_0= ruleFirstTerm ) )
            {
            // InternalPragmas.g:2338:2: ( (lv_terms_0_0= ruleFirstTerm ) )
            // InternalPragmas.g:2339:3: (lv_terms_0_0= ruleFirstTerm )
            {
            // InternalPragmas.g:2339:3: (lv_terms_0_0= ruleFirstTerm )
            // InternalPragmas.g:2340:4: lv_terms_0_0= ruleFirstTerm
            {

            				newCompositeNode(grammarAccess.getSingleTermAffineExpressionAccess().getTermsFirstTermParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_terms_0_0=ruleFirstTerm();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getSingleTermAffineExpressionRule());
            				}
            				add(
            					current,
            					"terms",
            					lv_terms_0_0,
            					"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.FirstTerm");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleTermAffineExpression"


    // $ANTLR start "entryRuleAffineConstraint"
    // InternalPragmas.g:2360:1: entryRuleAffineConstraint returns [EObject current=null] : iv_ruleAffineConstraint= ruleAffineConstraint EOF ;
    public final EObject entryRuleAffineConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAffineConstraint = null;


        try {
            // InternalPragmas.g:2360:57: (iv_ruleAffineConstraint= ruleAffineConstraint EOF )
            // InternalPragmas.g:2361:2: iv_ruleAffineConstraint= ruleAffineConstraint EOF
            {
             newCompositeNode(grammarAccess.getAffineConstraintRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAffineConstraint=ruleAffineConstraint();

            state._fsp--;

             current =iv_ruleAffineConstraint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAffineConstraint"


    // $ANTLR start "ruleAffineConstraint"
    // InternalPragmas.g:2367:1: ruleAffineConstraint returns [EObject current=null] : ( ( (lv_lhs_0_0= ruleExpression ) ) ( (lv_comparisonOperator_1_0= ruleCompOperator ) ) ( (lv_rhs_2_0= ruleExpression ) ) ) ;
    public final EObject ruleAffineConstraint() throws RecognitionException {
        EObject current = null;

        EObject lv_lhs_0_0 = null;

        Enumerator lv_comparisonOperator_1_0 = null;

        EObject lv_rhs_2_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2373:2: ( ( ( (lv_lhs_0_0= ruleExpression ) ) ( (lv_comparisonOperator_1_0= ruleCompOperator ) ) ( (lv_rhs_2_0= ruleExpression ) ) ) )
            // InternalPragmas.g:2374:2: ( ( (lv_lhs_0_0= ruleExpression ) ) ( (lv_comparisonOperator_1_0= ruleCompOperator ) ) ( (lv_rhs_2_0= ruleExpression ) ) )
            {
            // InternalPragmas.g:2374:2: ( ( (lv_lhs_0_0= ruleExpression ) ) ( (lv_comparisonOperator_1_0= ruleCompOperator ) ) ( (lv_rhs_2_0= ruleExpression ) ) )
            // InternalPragmas.g:2375:3: ( (lv_lhs_0_0= ruleExpression ) ) ( (lv_comparisonOperator_1_0= ruleCompOperator ) ) ( (lv_rhs_2_0= ruleExpression ) )
            {
            // InternalPragmas.g:2375:3: ( (lv_lhs_0_0= ruleExpression ) )
            // InternalPragmas.g:2376:4: (lv_lhs_0_0= ruleExpression )
            {
            // InternalPragmas.g:2376:4: (lv_lhs_0_0= ruleExpression )
            // InternalPragmas.g:2377:5: lv_lhs_0_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getAffineConstraintAccess().getLhsExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_53);
            lv_lhs_0_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAffineConstraintRule());
            					}
            					set(
            						current,
            						"lhs",
            						lv_lhs_0_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2394:3: ( (lv_comparisonOperator_1_0= ruleCompOperator ) )
            // InternalPragmas.g:2395:4: (lv_comparisonOperator_1_0= ruleCompOperator )
            {
            // InternalPragmas.g:2395:4: (lv_comparisonOperator_1_0= ruleCompOperator )
            // InternalPragmas.g:2396:5: lv_comparisonOperator_1_0= ruleCompOperator
            {

            					newCompositeNode(grammarAccess.getAffineConstraintAccess().getComparisonOperatorCompOperatorEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_29);
            lv_comparisonOperator_1_0=ruleCompOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAffineConstraintRule());
            					}
            					set(
            						current,
            						"comparisonOperator",
            						lv_comparisonOperator_1_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.CompOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPragmas.g:2413:3: ( (lv_rhs_2_0= ruleExpression ) )
            // InternalPragmas.g:2414:4: (lv_rhs_2_0= ruleExpression )
            {
            // InternalPragmas.g:2414:4: (lv_rhs_2_0= ruleExpression )
            // InternalPragmas.g:2415:5: lv_rhs_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getAffineConstraintAccess().getRhsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_rhs_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAffineConstraintRule());
            					}
            					set(
            						current,
            						"rhs",
            						lv_rhs_2_0,
            						"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAffineConstraint"


    // $ANTLR start "entryRuleFirstTerm"
    // InternalPragmas.g:2436:1: entryRuleFirstTerm returns [EObject current=null] : iv_ruleFirstTerm= ruleFirstTerm EOF ;
    public final EObject entryRuleFirstTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFirstTerm = null;


        try {
            // InternalPragmas.g:2436:50: (iv_ruleFirstTerm= ruleFirstTerm EOF )
            // InternalPragmas.g:2437:2: iv_ruleFirstTerm= ruleFirstTerm EOF
            {
             newCompositeNode(grammarAccess.getFirstTermRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFirstTerm=ruleFirstTerm();

            state._fsp--;

             current =iv_ruleFirstTerm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFirstTerm"


    // $ANTLR start "ruleFirstTerm"
    // InternalPragmas.g:2443:1: ruleFirstTerm returns [EObject current=null] : ( ( () ( (otherlv_1= RULE_ID ) ) ) | ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) ) | ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) ) ) ;
    public final EObject ruleFirstTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_coef_3_1 = null;

        AntlrDatatypeRuleToken lv_coef_3_2 = null;

        AntlrDatatypeRuleToken lv_coef_3_3 = null;

        AntlrDatatypeRuleToken lv_coef_6_1 = null;

        AntlrDatatypeRuleToken lv_coef_6_2 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2449:2: ( ( ( () ( (otherlv_1= RULE_ID ) ) ) | ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) ) | ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) ) ) )
            // InternalPragmas.g:2450:2: ( ( () ( (otherlv_1= RULE_ID ) ) ) | ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) ) | ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) ) )
            {
            // InternalPragmas.g:2450:2: ( ( () ( (otherlv_1= RULE_ID ) ) ) | ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) ) | ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) ) )
            int alt47=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt47=1;
                }
                break;
            case RULE_INTTOKEN:
                {
                int LA47_2 = input.LA(2);

                if ( (LA47_2==RULE_ID) ) {
                    alt47=2;
                }
                else if ( (LA47_2==EOF||LA47_2==15||LA47_2==17||LA47_2==26||LA47_2==29||LA47_2==65||(LA47_2>=69 && LA47_2<=74)) ) {
                    alt47=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 2, input);

                    throw nvae;
                }
                }
                break;
            case 65:
                {
                int LA47_3 = input.LA(2);

                if ( (LA47_3==RULE_ID) ) {
                    alt47=2;
                }
                else if ( (LA47_3==RULE_INTTOKEN) ) {
                    int LA47_6 = input.LA(3);

                    if ( (LA47_6==RULE_ID) ) {
                        alt47=2;
                    }
                    else if ( (LA47_6==EOF||LA47_6==15||LA47_6==17||LA47_6==26||LA47_6==29||LA47_6==65||(LA47_6>=69 && LA47_6<=74)) ) {
                        alt47=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 47, 6, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }

            switch (alt47) {
                case 1 :
                    // InternalPragmas.g:2451:3: ( () ( (otherlv_1= RULE_ID ) ) )
                    {
                    // InternalPragmas.g:2451:3: ( () ( (otherlv_1= RULE_ID ) ) )
                    // InternalPragmas.g:2452:4: () ( (otherlv_1= RULE_ID ) )
                    {
                    // InternalPragmas.g:2452:4: ()
                    // InternalPragmas.g:2453:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getFirstTermAccess().getAffineTermHackAction_0_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2459:4: ( (otherlv_1= RULE_ID ) )
                    // InternalPragmas.g:2460:5: (otherlv_1= RULE_ID )
                    {
                    // InternalPragmas.g:2460:5: (otherlv_1= RULE_ID )
                    // InternalPragmas.g:2461:6: otherlv_1= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFirstTermRule());
                    						}
                    					
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_1, grammarAccess.getFirstTermAccess().getVariableVariableCrossReference_0_1_0());
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2474:3: ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) )
                    {
                    // InternalPragmas.g:2474:3: ( () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) ) )
                    // InternalPragmas.g:2475:4: () ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) )
                    {
                    // InternalPragmas.g:2475:4: ()
                    // InternalPragmas.g:2476:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getFirstTermAccess().getAffineTermAction_1_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2482:4: ( ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) ) )
                    // InternalPragmas.g:2483:5: ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) ) ( (otherlv_4= RULE_ID ) )
                    {
                    // InternalPragmas.g:2483:5: ( ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) ) )
                    // InternalPragmas.g:2484:6: ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) )
                    {
                    // InternalPragmas.g:2484:6: ( (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL ) )
                    // InternalPragmas.g:2485:7: (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL )
                    {
                    // InternalPragmas.g:2485:7: (lv_coef_3_1= ruleINT_VAL | lv_coef_3_2= ruleNEG_VAL | lv_coef_3_3= ruleNEG_INT_VAL )
                    int alt45=3;
                    int LA45_0 = input.LA(1);

                    if ( (LA45_0==RULE_INTTOKEN) ) {
                        alt45=1;
                    }
                    else if ( (LA45_0==65) ) {
                        int LA45_2 = input.LA(2);

                        if ( (LA45_2==RULE_ID) ) {
                            alt45=2;
                        }
                        else if ( (LA45_2==RULE_INTTOKEN) ) {
                            alt45=3;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 45, 2, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 45, 0, input);

                        throw nvae;
                    }
                    switch (alt45) {
                        case 1 :
                            // InternalPragmas.g:2486:8: lv_coef_3_1= ruleINT_VAL
                            {

                            								newCompositeNode(grammarAccess.getFirstTermAccess().getCoefINT_VALParserRuleCall_1_1_0_0_0());
                            							
                            pushFollow(FOLLOW_15);
                            lv_coef_3_1=ruleINT_VAL();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getFirstTermRule());
                            								}
                            								set(
                            									current,
                            									"coef",
                            									lv_coef_3_1,
                            									"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_VAL");
                            								afterParserOrEnumRuleCall();
                            							

                            }
                            break;
                        case 2 :
                            // InternalPragmas.g:2502:8: lv_coef_3_2= ruleNEG_VAL
                            {

                            								newCompositeNode(grammarAccess.getFirstTermAccess().getCoefNEG_VALParserRuleCall_1_1_0_0_1());
                            							
                            pushFollow(FOLLOW_15);
                            lv_coef_3_2=ruleNEG_VAL();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getFirstTermRule());
                            								}
                            								set(
                            									current,
                            									"coef",
                            									lv_coef_3_2,
                            									"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_VAL");
                            								afterParserOrEnumRuleCall();
                            							

                            }
                            break;
                        case 3 :
                            // InternalPragmas.g:2518:8: lv_coef_3_3= ruleNEG_INT_VAL
                            {

                            								newCompositeNode(grammarAccess.getFirstTermAccess().getCoefNEG_INT_VALParserRuleCall_1_1_0_0_2());
                            							
                            pushFollow(FOLLOW_15);
                            lv_coef_3_3=ruleNEG_INT_VAL();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getFirstTermRule());
                            								}
                            								set(
                            									current,
                            									"coef",
                            									lv_coef_3_3,
                            									"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_INT_VAL");
                            								afterParserOrEnumRuleCall();
                            							

                            }
                            break;

                    }


                    }


                    }

                    // InternalPragmas.g:2536:5: ( (otherlv_4= RULE_ID ) )
                    // InternalPragmas.g:2537:6: (otherlv_4= RULE_ID )
                    {
                    // InternalPragmas.g:2537:6: (otherlv_4= RULE_ID )
                    // InternalPragmas.g:2538:7: otherlv_4= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getFirstTermRule());
                    							}
                    						
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

                    							newLeafNode(otherlv_4, grammarAccess.getFirstTermAccess().getVariableVariableCrossReference_1_1_1_0());
                    						

                    }


                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPragmas.g:2552:3: ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) )
                    {
                    // InternalPragmas.g:2552:3: ( () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) ) )
                    // InternalPragmas.g:2553:4: () ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) )
                    {
                    // InternalPragmas.g:2553:4: ()
                    // InternalPragmas.g:2554:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getFirstTermAccess().getAffineTermAction_2_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2560:4: ( ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) ) )
                    // InternalPragmas.g:2561:5: ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) )
                    {
                    // InternalPragmas.g:2561:5: ( (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL ) )
                    // InternalPragmas.g:2562:6: (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL )
                    {
                    // InternalPragmas.g:2562:6: (lv_coef_6_1= ruleINT_VAL | lv_coef_6_2= ruleNEG_INT_VAL )
                    int alt46=2;
                    int LA46_0 = input.LA(1);

                    if ( (LA46_0==RULE_INTTOKEN) ) {
                        alt46=1;
                    }
                    else if ( (LA46_0==65) ) {
                        alt46=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 46, 0, input);

                        throw nvae;
                    }
                    switch (alt46) {
                        case 1 :
                            // InternalPragmas.g:2563:7: lv_coef_6_1= ruleINT_VAL
                            {

                            							newCompositeNode(grammarAccess.getFirstTermAccess().getCoefINT_VALParserRuleCall_2_1_0_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_coef_6_1=ruleINT_VAL();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getFirstTermRule());
                            							}
                            							set(
                            								current,
                            								"coef",
                            								lv_coef_6_1,
                            								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_VAL");
                            							afterParserOrEnumRuleCall();
                            						

                            }
                            break;
                        case 2 :
                            // InternalPragmas.g:2579:7: lv_coef_6_2= ruleNEG_INT_VAL
                            {

                            							newCompositeNode(grammarAccess.getFirstTermAccess().getCoefNEG_INT_VALParserRuleCall_2_1_0_1());
                            						
                            pushFollow(FOLLOW_2);
                            lv_coef_6_2=ruleNEG_INT_VAL();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getFirstTermRule());
                            							}
                            							set(
                            								current,
                            								"coef",
                            								lv_coef_6_2,
                            								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_INT_VAL");
                            							afterParserOrEnumRuleCall();
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFirstTerm"


    // $ANTLR start "entryRuleNextTerm"
    // InternalPragmas.g:2602:1: entryRuleNextTerm returns [EObject current=null] : iv_ruleNextTerm= ruleNextTerm EOF ;
    public final EObject entryRuleNextTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNextTerm = null;


        try {
            // InternalPragmas.g:2602:49: (iv_ruleNextTerm= ruleNextTerm EOF )
            // InternalPragmas.g:2603:2: iv_ruleNextTerm= ruleNextTerm EOF
            {
             newCompositeNode(grammarAccess.getNextTermRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNextTerm=ruleNextTerm();

            state._fsp--;

             current =iv_ruleNextTerm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNextTerm"


    // $ANTLR start "ruleNextTerm"
    // InternalPragmas.g:2609:1: ruleNextTerm returns [EObject current=null] : ( ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) ) | ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) ) | ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) ) | ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) ) | ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) ) ) ;
    public final EObject ruleNextTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_coef_1_1 = null;

        AntlrDatatypeRuleToken lv_coef_1_2 = null;

        AntlrDatatypeRuleToken lv_coef_4_0 = null;

        AntlrDatatypeRuleToken lv_coef_10_0 = null;

        AntlrDatatypeRuleToken lv_coef_14_0 = null;



        	enterRule();

        try {
            // InternalPragmas.g:2615:2: ( ( ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) ) | ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) ) | ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) ) | ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) ) | ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) ) ) )
            // InternalPragmas.g:2616:2: ( ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) ) | ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) ) | ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) ) | ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) ) | ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) ) )
            {
            // InternalPragmas.g:2616:2: ( ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) ) | ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) ) | ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) ) | ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) ) | ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) ) )
            int alt49=5;
            alt49 = dfa49.predict(input);
            switch (alt49) {
                case 1 :
                    // InternalPragmas.g:2617:3: ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) )
                    {
                    // InternalPragmas.g:2617:3: ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) )
                    // InternalPragmas.g:2618:4: () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) )
                    {
                    // InternalPragmas.g:2618:4: ()
                    // InternalPragmas.g:2619:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNextTermAccess().getAffineTermAction_0_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2625:4: ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) )
                    // InternalPragmas.g:2626:5: ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) )
                    {
                    // InternalPragmas.g:2626:5: ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) )
                    // InternalPragmas.g:2627:6: ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) )
                    {
                    // InternalPragmas.g:2627:6: ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) )
                    // InternalPragmas.g:2628:7: (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL )
                    {
                    // InternalPragmas.g:2628:7: (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL )
                    int alt48=2;
                    int LA48_0 = input.LA(1);

                    if ( (LA48_0==65) ) {
                        int LA48_1 = input.LA(2);

                        if ( (LA48_1==RULE_INTTOKEN) ) {
                            alt48=2;
                        }
                        else if ( (LA48_1==RULE_ID) ) {
                            alt48=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 48, 1, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 48, 0, input);

                        throw nvae;
                    }
                    switch (alt48) {
                        case 1 :
                            // InternalPragmas.g:2629:8: lv_coef_1_1= ruleNEG_VAL
                            {

                            								newCompositeNode(grammarAccess.getNextTermAccess().getCoefNEG_VALParserRuleCall_0_1_0_0_0());
                            							
                            pushFollow(FOLLOW_15);
                            lv_coef_1_1=ruleNEG_VAL();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getNextTermRule());
                            								}
                            								set(
                            									current,
                            									"coef",
                            									lv_coef_1_1,
                            									"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_VAL");
                            								afterParserOrEnumRuleCall();
                            							

                            }
                            break;
                        case 2 :
                            // InternalPragmas.g:2645:8: lv_coef_1_2= ruleNEG_INT_VAL
                            {

                            								newCompositeNode(grammarAccess.getNextTermAccess().getCoefNEG_INT_VALParserRuleCall_0_1_0_0_1());
                            							
                            pushFollow(FOLLOW_15);
                            lv_coef_1_2=ruleNEG_INT_VAL();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getNextTermRule());
                            								}
                            								set(
                            									current,
                            									"coef",
                            									lv_coef_1_2,
                            									"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_INT_VAL");
                            								afterParserOrEnumRuleCall();
                            							

                            }
                            break;

                    }


                    }


                    }

                    // InternalPragmas.g:2663:5: ( (otherlv_2= RULE_ID ) )
                    // InternalPragmas.g:2664:6: (otherlv_2= RULE_ID )
                    {
                    // InternalPragmas.g:2664:6: (otherlv_2= RULE_ID )
                    // InternalPragmas.g:2665:7: otherlv_2= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getNextTermRule());
                    							}
                    						
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

                    							newLeafNode(otherlv_2, grammarAccess.getNextTermAccess().getVariableVariableCrossReference_0_1_1_0());
                    						

                    }


                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2679:3: ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) )
                    {
                    // InternalPragmas.g:2679:3: ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) )
                    // InternalPragmas.g:2680:4: () ( (lv_coef_4_0= ruleNEG_INT_VAL ) )
                    {
                    // InternalPragmas.g:2680:4: ()
                    // InternalPragmas.g:2681:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNextTermAccess().getAffineTermAction_1_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2687:4: ( (lv_coef_4_0= ruleNEG_INT_VAL ) )
                    // InternalPragmas.g:2688:5: (lv_coef_4_0= ruleNEG_INT_VAL )
                    {
                    // InternalPragmas.g:2688:5: (lv_coef_4_0= ruleNEG_INT_VAL )
                    // InternalPragmas.g:2689:6: lv_coef_4_0= ruleNEG_INT_VAL
                    {

                    						newCompositeNode(grammarAccess.getNextTermAccess().getCoefNEG_INT_VALParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_coef_4_0=ruleNEG_INT_VAL();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNextTermRule());
                    						}
                    						set(
                    							current,
                    							"coef",
                    							lv_coef_4_0,
                    							"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.NEG_INT_VAL");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPragmas.g:2708:3: ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) )
                    {
                    // InternalPragmas.g:2708:3: ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) )
                    // InternalPragmas.g:2709:4: () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) )
                    {
                    // InternalPragmas.g:2709:4: ()
                    // InternalPragmas.g:2710:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNextTermAccess().getAffineTermHackAction_2_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2716:4: (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) )
                    // InternalPragmas.g:2717:5: otherlv_6= '+' ( (otherlv_7= RULE_ID ) )
                    {
                    otherlv_6=(Token)match(input,29,FOLLOW_15); 

                    					newLeafNode(otherlv_6, grammarAccess.getNextTermAccess().getPlusSignKeyword_2_1_0());
                    				
                    // InternalPragmas.g:2721:5: ( (otherlv_7= RULE_ID ) )
                    // InternalPragmas.g:2722:6: (otherlv_7= RULE_ID )
                    {
                    // InternalPragmas.g:2722:6: (otherlv_7= RULE_ID )
                    // InternalPragmas.g:2723:7: otherlv_7= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getNextTermRule());
                    							}
                    						
                    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_2); 

                    							newLeafNode(otherlv_7, grammarAccess.getNextTermAccess().getVariableVariableCrossReference_2_1_1_0());
                    						

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalPragmas.g:2737:3: ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) )
                    {
                    // InternalPragmas.g:2737:3: ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) )
                    // InternalPragmas.g:2738:4: () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) )
                    {
                    // InternalPragmas.g:2738:4: ()
                    // InternalPragmas.g:2739:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNextTermAccess().getAffineTermAction_3_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2745:4: (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) )
                    // InternalPragmas.g:2746:5: otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) )
                    {
                    otherlv_9=(Token)match(input,29,FOLLOW_6); 

                    					newLeafNode(otherlv_9, grammarAccess.getNextTermAccess().getPlusSignKeyword_3_1_0());
                    				
                    // InternalPragmas.g:2750:5: ( (lv_coef_10_0= ruleINT_VAL ) )
                    // InternalPragmas.g:2751:6: (lv_coef_10_0= ruleINT_VAL )
                    {
                    // InternalPragmas.g:2751:6: (lv_coef_10_0= ruleINT_VAL )
                    // InternalPragmas.g:2752:7: lv_coef_10_0= ruleINT_VAL
                    {

                    							newCompositeNode(grammarAccess.getNextTermAccess().getCoefINT_VALParserRuleCall_3_1_1_0());
                    						
                    pushFollow(FOLLOW_15);
                    lv_coef_10_0=ruleINT_VAL();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getNextTermRule());
                    							}
                    							set(
                    								current,
                    								"coef",
                    								lv_coef_10_0,
                    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_VAL");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalPragmas.g:2769:5: ( (otherlv_11= RULE_ID ) )
                    // InternalPragmas.g:2770:6: (otherlv_11= RULE_ID )
                    {
                    // InternalPragmas.g:2770:6: (otherlv_11= RULE_ID )
                    // InternalPragmas.g:2771:7: otherlv_11= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getNextTermRule());
                    							}
                    						
                    otherlv_11=(Token)match(input,RULE_ID,FOLLOW_2); 

                    							newLeafNode(otherlv_11, grammarAccess.getNextTermAccess().getVariableVariableCrossReference_3_1_2_0());
                    						

                    }


                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalPragmas.g:2785:3: ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) )
                    {
                    // InternalPragmas.g:2785:3: ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) )
                    // InternalPragmas.g:2786:4: () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) )
                    {
                    // InternalPragmas.g:2786:4: ()
                    // InternalPragmas.g:2787:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getNextTermAccess().getAffineTermAction_4_0(),
                    						current);
                    				

                    }

                    // InternalPragmas.g:2793:4: (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) )
                    // InternalPragmas.g:2794:5: otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) )
                    {
                    otherlv_13=(Token)match(input,29,FOLLOW_6); 

                    					newLeafNode(otherlv_13, grammarAccess.getNextTermAccess().getPlusSignKeyword_4_1_0());
                    				
                    // InternalPragmas.g:2798:5: ( (lv_coef_14_0= ruleINT_VAL ) )
                    // InternalPragmas.g:2799:6: (lv_coef_14_0= ruleINT_VAL )
                    {
                    // InternalPragmas.g:2799:6: (lv_coef_14_0= ruleINT_VAL )
                    // InternalPragmas.g:2800:7: lv_coef_14_0= ruleINT_VAL
                    {

                    							newCompositeNode(grammarAccess.getNextTermAccess().getCoefINT_VALParserRuleCall_4_1_1_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_coef_14_0=ruleINT_VAL();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getNextTermRule());
                    							}
                    							set(
                    								current,
                    								"coef",
                    								lv_coef_14_0,
                    								"fr.irisa.cairn.gecos.model.scop.pragma.Pragmas.INT_VAL");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNextTerm"


    // $ANTLR start "entryRuleNEG_VAL"
    // InternalPragmas.g:2823:1: entryRuleNEG_VAL returns [String current=null] : iv_ruleNEG_VAL= ruleNEG_VAL EOF ;
    public final String entryRuleNEG_VAL() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNEG_VAL = null;


        try {
            // InternalPragmas.g:2823:47: (iv_ruleNEG_VAL= ruleNEG_VAL EOF )
            // InternalPragmas.g:2824:2: iv_ruleNEG_VAL= ruleNEG_VAL EOF
            {
             newCompositeNode(grammarAccess.getNEG_VALRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNEG_VAL=ruleNEG_VAL();

            state._fsp--;

             current =iv_ruleNEG_VAL.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNEG_VAL"


    // $ANTLR start "ruleNEG_VAL"
    // InternalPragmas.g:2830:1: ruleNEG_VAL returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '-' ;
    public final AntlrDatatypeRuleToken ruleNEG_VAL() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPragmas.g:2836:2: (kw= '-' )
            // InternalPragmas.g:2837:2: kw= '-'
            {
            kw=(Token)match(input,65,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getNEG_VALAccess().getHyphenMinusKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNEG_VAL"


    // $ANTLR start "entryRuleNEG_INT_VAL"
    // InternalPragmas.g:2845:1: entryRuleNEG_INT_VAL returns [String current=null] : iv_ruleNEG_INT_VAL= ruleNEG_INT_VAL EOF ;
    public final String entryRuleNEG_INT_VAL() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNEG_INT_VAL = null;


        try {
            // InternalPragmas.g:2845:51: (iv_ruleNEG_INT_VAL= ruleNEG_INT_VAL EOF )
            // InternalPragmas.g:2846:2: iv_ruleNEG_INT_VAL= ruleNEG_INT_VAL EOF
            {
             newCompositeNode(grammarAccess.getNEG_INT_VALRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNEG_INT_VAL=ruleNEG_INT_VAL();

            state._fsp--;

             current =iv_ruleNEG_INT_VAL.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNEG_INT_VAL"


    // $ANTLR start "ruleNEG_INT_VAL"
    // InternalPragmas.g:2852:1: ruleNEG_INT_VAL returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '-' this_INTTOKEN_1= RULE_INTTOKEN ) ;
    public final AntlrDatatypeRuleToken ruleNEG_INT_VAL() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INTTOKEN_1=null;


        	enterRule();

        try {
            // InternalPragmas.g:2858:2: ( (kw= '-' this_INTTOKEN_1= RULE_INTTOKEN ) )
            // InternalPragmas.g:2859:2: (kw= '-' this_INTTOKEN_1= RULE_INTTOKEN )
            {
            // InternalPragmas.g:2859:2: (kw= '-' this_INTTOKEN_1= RULE_INTTOKEN )
            // InternalPragmas.g:2860:3: kw= '-' this_INTTOKEN_1= RULE_INTTOKEN
            {
            kw=(Token)match(input,65,FOLLOW_6); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getNEG_INT_VALAccess().getHyphenMinusKeyword_0());
            		
            this_INTTOKEN_1=(Token)match(input,RULE_INTTOKEN,FOLLOW_2); 

            			current.merge(this_INTTOKEN_1);
            		

            			newLeafNode(this_INTTOKEN_1, grammarAccess.getNEG_INT_VALAccess().getINTTOKENTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNEG_INT_VAL"


    // $ANTLR start "entryRuleINT_VAL"
    // InternalPragmas.g:2876:1: entryRuleINT_VAL returns [String current=null] : iv_ruleINT_VAL= ruleINT_VAL EOF ;
    public final String entryRuleINT_VAL() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleINT_VAL = null;


        try {
            // InternalPragmas.g:2876:47: (iv_ruleINT_VAL= ruleINT_VAL EOF )
            // InternalPragmas.g:2877:2: iv_ruleINT_VAL= ruleINT_VAL EOF
            {
             newCompositeNode(grammarAccess.getINT_VALRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleINT_VAL=ruleINT_VAL();

            state._fsp--;

             current =iv_ruleINT_VAL.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINT_VAL"


    // $ANTLR start "ruleINT_VAL"
    // InternalPragmas.g:2883:1: ruleINT_VAL returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INTTOKEN_0= RULE_INTTOKEN ;
    public final AntlrDatatypeRuleToken ruleINT_VAL() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INTTOKEN_0=null;


        	enterRule();

        try {
            // InternalPragmas.g:2889:2: (this_INTTOKEN_0= RULE_INTTOKEN )
            // InternalPragmas.g:2890:2: this_INTTOKEN_0= RULE_INTTOKEN
            {
            this_INTTOKEN_0=(Token)match(input,RULE_INTTOKEN,FOLLOW_2); 

            		current.merge(this_INTTOKEN_0);
            	

            		newLeafNode(this_INTTOKEN_0, grammarAccess.getINT_VALAccess().getINTTOKENTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINT_VAL"


    // $ANTLR start "entryRuleINT_TYPE"
    // InternalPragmas.g:2900:1: entryRuleINT_TYPE returns [String current=null] : iv_ruleINT_TYPE= ruleINT_TYPE EOF ;
    public final String entryRuleINT_TYPE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleINT_TYPE = null;


        try {
            // InternalPragmas.g:2900:48: (iv_ruleINT_TYPE= ruleINT_TYPE EOF )
            // InternalPragmas.g:2901:2: iv_ruleINT_TYPE= ruleINT_TYPE EOF
            {
             newCompositeNode(grammarAccess.getINT_TYPERule()); 
            pushFollow(FOLLOW_1);
            iv_ruleINT_TYPE=ruleINT_TYPE();

            state._fsp--;

             current =iv_ruleINT_TYPE.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINT_TYPE"


    // $ANTLR start "ruleINT_TYPE"
    // InternalPragmas.g:2907:1: ruleINT_TYPE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INTTOKEN_0= RULE_INTTOKEN ;
    public final AntlrDatatypeRuleToken ruleINT_TYPE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INTTOKEN_0=null;


        	enterRule();

        try {
            // InternalPragmas.g:2913:2: (this_INTTOKEN_0= RULE_INTTOKEN )
            // InternalPragmas.g:2914:2: this_INTTOKEN_0= RULE_INTTOKEN
            {
            this_INTTOKEN_0=(Token)match(input,RULE_INTTOKEN,FOLLOW_2); 

            		current.merge(this_INTTOKEN_0);
            	

            		newLeafNode(this_INTTOKEN_0, grammarAccess.getINT_TYPEAccess().getINTTOKENTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINT_TYPE"


    // $ANTLR start "entryRuleSTRING_TYPE"
    // InternalPragmas.g:2924:1: entryRuleSTRING_TYPE returns [String current=null] : iv_ruleSTRING_TYPE= ruleSTRING_TYPE EOF ;
    public final String entryRuleSTRING_TYPE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSTRING_TYPE = null;


        try {
            // InternalPragmas.g:2924:51: (iv_ruleSTRING_TYPE= ruleSTRING_TYPE EOF )
            // InternalPragmas.g:2925:2: iv_ruleSTRING_TYPE= ruleSTRING_TYPE EOF
            {
             newCompositeNode(grammarAccess.getSTRING_TYPERule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSTRING_TYPE=ruleSTRING_TYPE();

            state._fsp--;

             current =iv_ruleSTRING_TYPE.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSTRING_TYPE"


    // $ANTLR start "ruleSTRING_TYPE"
    // InternalPragmas.g:2931:1: ruleSTRING_TYPE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRINGTOKEN_0= RULE_STRINGTOKEN ;
    public final AntlrDatatypeRuleToken ruleSTRING_TYPE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRINGTOKEN_0=null;


        	enterRule();

        try {
            // InternalPragmas.g:2937:2: (this_STRINGTOKEN_0= RULE_STRINGTOKEN )
            // InternalPragmas.g:2938:2: this_STRINGTOKEN_0= RULE_STRINGTOKEN
            {
            this_STRINGTOKEN_0=(Token)match(input,RULE_STRINGTOKEN,FOLLOW_2); 

            		current.merge(this_STRINGTOKEN_0);
            	

            		newLeafNode(this_STRINGTOKEN_0, grammarAccess.getSTRING_TYPEAccess().getSTRINGTOKENTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSTRING_TYPE"


    // $ANTLR start "ruleDepType"
    // InternalPragmas.g:2948:1: ruleDepType returns [Enumerator current=null] : ( (enumLiteral_0= 'RAW' ) | (enumLiteral_1= 'WAR' ) | (enumLiteral_2= 'WAW' ) ) ;
    public final Enumerator ruleDepType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalPragmas.g:2954:2: ( ( (enumLiteral_0= 'RAW' ) | (enumLiteral_1= 'WAR' ) | (enumLiteral_2= 'WAW' ) ) )
            // InternalPragmas.g:2955:2: ( (enumLiteral_0= 'RAW' ) | (enumLiteral_1= 'WAR' ) | (enumLiteral_2= 'WAW' ) )
            {
            // InternalPragmas.g:2955:2: ( (enumLiteral_0= 'RAW' ) | (enumLiteral_1= 'WAR' ) | (enumLiteral_2= 'WAW' ) )
            int alt50=3;
            switch ( input.LA(1) ) {
            case 66:
                {
                alt50=1;
                }
                break;
            case 67:
                {
                alt50=2;
                }
                break;
            case 68:
                {
                alt50=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // InternalPragmas.g:2956:3: (enumLiteral_0= 'RAW' )
                    {
                    // InternalPragmas.g:2956:3: (enumLiteral_0= 'RAW' )
                    // InternalPragmas.g:2957:4: enumLiteral_0= 'RAW'
                    {
                    enumLiteral_0=(Token)match(input,66,FOLLOW_2); 

                    				current = grammarAccess.getDepTypeAccess().getRAWEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDepTypeAccess().getRAWEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2964:3: (enumLiteral_1= 'WAR' )
                    {
                    // InternalPragmas.g:2964:3: (enumLiteral_1= 'WAR' )
                    // InternalPragmas.g:2965:4: enumLiteral_1= 'WAR'
                    {
                    enumLiteral_1=(Token)match(input,67,FOLLOW_2); 

                    				current = grammarAccess.getDepTypeAccess().getWAREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDepTypeAccess().getWAREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPragmas.g:2972:3: (enumLiteral_2= 'WAW' )
                    {
                    // InternalPragmas.g:2972:3: (enumLiteral_2= 'WAW' )
                    // InternalPragmas.g:2973:4: enumLiteral_2= 'WAW'
                    {
                    enumLiteral_2=(Token)match(input,68,FOLLOW_2); 

                    				current = grammarAccess.getDepTypeAccess().getWAWEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDepTypeAccess().getWAWEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDepType"


    // $ANTLR start "ruleDivModOperator"
    // InternalPragmas.g:2983:1: ruleDivModOperator returns [Enumerator current=null] : ( (enumLiteral_0= '%' ) | (enumLiteral_1= '/' ) ) ;
    public final Enumerator ruleDivModOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalPragmas.g:2989:2: ( ( (enumLiteral_0= '%' ) | (enumLiteral_1= '/' ) ) )
            // InternalPragmas.g:2990:2: ( (enumLiteral_0= '%' ) | (enumLiteral_1= '/' ) )
            {
            // InternalPragmas.g:2990:2: ( (enumLiteral_0= '%' ) | (enumLiteral_1= '/' ) )
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==69) ) {
                alt51=1;
            }
            else if ( (LA51_0==70) ) {
                alt51=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }
            switch (alt51) {
                case 1 :
                    // InternalPragmas.g:2991:3: (enumLiteral_0= '%' )
                    {
                    // InternalPragmas.g:2991:3: (enumLiteral_0= '%' )
                    // InternalPragmas.g:2992:4: enumLiteral_0= '%'
                    {
                    enumLiteral_0=(Token)match(input,69,FOLLOW_2); 

                    				current = grammarAccess.getDivModOperatorAccess().getMODEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDivModOperatorAccess().getMODEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPragmas.g:2999:3: (enumLiteral_1= '/' )
                    {
                    // InternalPragmas.g:2999:3: (enumLiteral_1= '/' )
                    // InternalPragmas.g:3000:4: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,70,FOLLOW_2); 

                    				current = grammarAccess.getDivModOperatorAccess().getDIVEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDivModOperatorAccess().getDIVEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDivModOperator"


    // $ANTLR start "ruleCompOperator"
    // InternalPragmas.g:3010:1: ruleCompOperator returns [Enumerator current=null] : ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '=' ) ) ;
    public final Enumerator ruleCompOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalPragmas.g:3016:2: ( ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '=' ) ) )
            // InternalPragmas.g:3017:2: ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '=' ) )
            {
            // InternalPragmas.g:3017:2: ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '=' ) )
            int alt52=5;
            switch ( input.LA(1) ) {
            case 71:
                {
                alt52=1;
                }
                break;
            case 72:
                {
                alt52=2;
                }
                break;
            case 73:
                {
                alt52=3;
                }
                break;
            case 74:
                {
                alt52=4;
                }
                break;
            case 15:
                {
                alt52=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }

            switch (alt52) {
                case 1 :
                    // InternalPragmas.g:3018:3: (enumLiteral_0= '<' )
                    {
                    // InternalPragmas.g:3018:3: (enumLiteral_0= '<' )
                    // InternalPragmas.g:3019:4: enumLiteral_0= '<'
                    {
                    enumLiteral_0=(Token)match(input,71,FOLLOW_2); 

                    				current = grammarAccess.getCompOperatorAccess().getLTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getCompOperatorAccess().getLTEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPragmas.g:3026:3: (enumLiteral_1= '>' )
                    {
                    // InternalPragmas.g:3026:3: (enumLiteral_1= '>' )
                    // InternalPragmas.g:3027:4: enumLiteral_1= '>'
                    {
                    enumLiteral_1=(Token)match(input,72,FOLLOW_2); 

                    				current = grammarAccess.getCompOperatorAccess().getGTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getCompOperatorAccess().getGTEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPragmas.g:3034:3: (enumLiteral_2= '>=' )
                    {
                    // InternalPragmas.g:3034:3: (enumLiteral_2= '>=' )
                    // InternalPragmas.g:3035:4: enumLiteral_2= '>='
                    {
                    enumLiteral_2=(Token)match(input,73,FOLLOW_2); 

                    				current = grammarAccess.getCompOperatorAccess().getGEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getCompOperatorAccess().getGEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalPragmas.g:3042:3: (enumLiteral_3= '<=' )
                    {
                    // InternalPragmas.g:3042:3: (enumLiteral_3= '<=' )
                    // InternalPragmas.g:3043:4: enumLiteral_3= '<='
                    {
                    enumLiteral_3=(Token)match(input,74,FOLLOW_2); 

                    				current = grammarAccess.getCompOperatorAccess().getLEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getCompOperatorAccess().getLEEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalPragmas.g:3050:3: (enumLiteral_4= '=' )
                    {
                    // InternalPragmas.g:3050:3: (enumLiteral_4= '=' )
                    // InternalPragmas.g:3051:4: enumLiteral_4= '='
                    {
                    enumLiteral_4=(Token)match(input,15,FOLLOW_2); 

                    				current = grammarAccess.getCompOperatorAccess().getEQEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getCompOperatorAccess().getEQEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompOperator"

    // Delegated rules


    protected DFA49 dfa49 = new DFA49(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\3\uffff\1\7\1\uffff\1\10\4\uffff";
    static final String dfa_3s = "\1\35\3\4\1\uffff\1\4\4\uffff";
    static final String dfa_4s = "\1\101\2\5\1\112\1\uffff\1\112\4\uffff";
    static final String dfa_5s = "\4\uffff\1\1\1\uffff\1\3\1\2\1\5\1\4";
    static final String dfa_6s = "\12\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\43\uffff\1\1",
            "\1\4\1\3",
            "\1\6\1\5",
            "\1\4\12\uffff\1\7\1\uffff\1\7\10\uffff\1\7\2\uffff\1\7\43\uffff\1\7\5\uffff\4\7",
            "",
            "\1\11\12\uffff\1\10\1\uffff\1\10\10\uffff\1\10\2\uffff\1\10\43\uffff\1\10\5\uffff\4\10",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA49 extends DFA {

        public DFA49(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 49;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "2616:2: ( ( () ( ( ( (lv_coef_1_1= ruleNEG_VAL | lv_coef_1_2= ruleNEG_INT_VAL ) ) ) ( (otherlv_2= RULE_ID ) ) ) ) | ( () ( (lv_coef_4_0= ruleNEG_INT_VAL ) ) ) | ( () (otherlv_6= '+' ( (otherlv_7= RULE_ID ) ) ) ) | ( () (otherlv_9= '+' ( (lv_coef_10_0= ruleINT_VAL ) ) ( (otherlv_11= RULE_ID ) ) ) ) | ( () (otherlv_13= '+' ( (lv_coef_14_0= ruleINT_VAL ) ) ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x73801C1380383002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000054002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000050002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000010800002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000019000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000018000002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000000L,0x000000000000001CL});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000400020000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002000000010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000002000030L,0x0000000000000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000030000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x007FE00000000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x007FC00000000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x007F800000000002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x007F000000000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x007E000000000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x007C000000000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0078000000000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0070000000000002L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0060000000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0C00000000000002L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0800000000000002L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000020000022L,0x0000000000000002L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000060L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000030L,0x0000000000000002L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000024000020L,0x0000000000000002L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000780L});

}