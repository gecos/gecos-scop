package fr.irisa.cairn.gecos.model.scop.pragma;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractValueConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.util.Strings;

import com.google.inject.Inject;

public class PragmasTerminalConverters extends DefaultTerminalConverters {

	// NEG_VAL returns algebra::Value : '-';
	// NEG_INT_VAL returns algebra::Value : '-' INTTOKEN;
	// INT_VAL returns algebra::Value : INTTOKEN;
	// INT_TYPE returns ecore::EInt: INTTOKEN;

	public static class LongValueConverter extends AbstractValueConverter<Long> {

		public String toString(Long value) {
			if (value == null)
				throw new ValueConverterException(
						"INT-value may not be null. (null indeed, zero is ok)",
						null, null);
			return value.toString();
		}

		public Long toValue(String string, INode node) {
			if (Strings.isEmpty(string))
				throw new ValueConverterException(
						"Couldn't convert empty string to int.", node, null);
			if (string.equals("-")) {
				return (long) -1;
			}
			try {
				return Long.parseLong(string);
			} catch (NumberFormatException e) {
				throw new ValueConverterException("Couldn't convert '" + string
						+ "' to int.", node, e);
			}
		}
	}

	public static class IntValueConverter extends
			AbstractValueConverter<Integer> {

		public String toString(Integer value) {
			if (value == null)
				throw new ValueConverterException(
						"INT-value may not be null. (null indeed, zero is ok)",
						null, null);
			return value.toString();
		}

		public Integer toValue(String string, INode node) {
			if (Strings.isEmpty(string))
				throw new ValueConverterException(
						"Couldn't convert empty string to int.", node, null);
			if (string.equals("-")) {
				return (int) -1;
			}
			try {
				return Integer.parseInt(string);
			} catch (NumberFormatException e) {
				throw new ValueConverterException("Couldn't convert '" + string
						+ "' to int.", node, e);
			}
		}
	}


	@Inject
	private LongValueConverter longValueConverter;
	@Inject
	private IntValueConverter intValueConverter;


	@ValueConverter(rule = "NEG_VAL")
	public IValueConverter<Long> NEG_VAL() {
		return longValueConverter;
	}

	@ValueConverter(rule = "INT_VAL")
	public IValueConverter<Long> INT_VAL() {
		return longValueConverter;
	}

	@ValueConverter(rule = "NEG_INT_VAL")
	public IValueConverter<Long> NEG_INT_VAL() {
		return longValueConverter;
	}

	@ValueConverter(rule = "INT_TYPE")
	public IValueConverter<Integer> INT_TYPE() {
		return intValueConverter;
	}
}
