package fr.irisa.cairn.gecos.model.scop.pragma.extractor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.AlgebraFactory;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import gecos.core.Scope;
import gecos.core.Symbol;

public abstract class AbstractSCopTransformBuilder {

	private static boolean verbose = false;

	
	public abstract ScopTransform buildScopTransform(GecosScopBlock root);
	
	protected ScopTransform build(GecosScopBlock root, Map<ScopInstructionStatement,List<IntExpression>> sched) {
		if (sched.size() < 1) return TransformFactory.eINSTANCE.createScopTransform();
		
		int nbSchedDims = sched.values().iterator().next().size();
		List<ScopDimension> scheduleDims = generateScheduleDimensions(nbSchedDims, root, sched.values());
		
		ScopTransform res = build(root,sched,scheduleDims);
		//res.getDimensions().addAll(scheduleDims);
		return res;
	}
	
	
	protected EList<ScopDimension> buildDefaultScheduleDimensions(GecosScopBlock root, int size) {
		EList<ScopDimension> schedDims;
		
		// FIXME : 
		EList<ScopDimension> allIterators = root.listAllEnclosingIterators();
		
		schedDims = new BasicEList<ScopDimension>(size);
		for(int i =0;i<size;i++) {
			if((i%2)==0) {
				ScopDimension scalar = ScopFactory.eINSTANCE.createScopDimension();
				scalar.setName("s_"+i);
				if (verbose) System.out.println("  - New scalar dimension :"+scalar+" at depth ?");
				schedDims.add(scalar);
				
			} else {
				// loop dimension
				ScopDimension loop = ScopFactory.eINSTANCE.createScopDimension();
				ScopDimension loopIterator = null;
				loopIterator = allIterators.get(i/2);
				loop.setSymbol(loopIterator.getSymbol());
				loop.setName(loopIterator.getName());
				//data.getDimensions().add(loop);
				if (verbose) System.out.println(" - New loop dimension :"+loop+" at depth ?");
				schedDims.add(loop);
			}
			if (verbose) System.out.println("Schedules ");
		}
		return schedDims;
	}

	protected ScopTransform build(ScopNode root, Map<ScopInstructionStatement,List<IntExpression>> sched, List<ScopDimension> scheduleDims) {
		ScopTransform res = TransformFactory.eINSTANCE.createScopTransform();
//		res.setContext(AlgebraFactory.eINSTANCE
//				.createIntConstraintSystem());
		if (sched.size() < 1) return res;
		// FIXME res.getDimensions().addAll(scheduleDims);
		Set<Entry<ScopInstructionStatement, List<IntExpression>>> entrySet = sched.entrySet();

		
		for (Entry<ScopInstructionStatement, List<IntExpression>> e : entrySet) {
			ScheduleDirective toto = buildDirective(e.getKey(),e.getValue(), scheduleDims);
			res.getCommands().add(toto);
		}
		
		return res;
	}
	
	protected ScheduleDirective buildDirective(ScopInstructionStatement instr, List<IntExpression> sched, List<ScopDimension> scheduleDims) {

		String in = "S"+instr.getId();
		String out = null;
		
		ScheduleDirective scheduleDirective = TransformFactory.eINSTANCE.createScheduleDirective();
		scheduleDirective.setStatement(instr);
		scheduleDirective.setSchedule(instr.listAllEnclosingIterators()+"->"+sched.toString());
		return scheduleDirective;
	}
	
	protected List<ScopDimension> generateScheduleDimensions(int nbDims, GecosScopBlock scopRoot, Collection<List<IntExpression>> schedules) {
		List<ScopDimension> res = new ArrayList<ScopDimension>(nbDims);
		Scope scope = scopRoot.getScope();
		int origDimsCounter = 0;
		for (int i = 0; i < nbDims; i++) {
			
			boolean scalar = isScalar(i,schedules);
			ScopDimension dim;
			if (scalar) {
				ScopDimension sDim = ScopFactory.eINSTANCE.createScopDimension();
				String name = "s_"+i;
				Symbol sym = GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT());
				scope.makeUnique(sym);
				scope.getSymbols().add(sym);
				sDim.setName(name);
				sDim.setSymbol(sym);
				dim = sDim;
			} else {
				String name = null;
				Symbol sym = null;
				try {
					ScopDimension loopIterator = scopRoot.listAllEnclosingIterators().get(origDimsCounter);
					sym = loopIterator.getSymbol();
					name = loopIterator.getName();
					origDimsCounter++;
				} catch (Exception e) {
					name = "l_"+i;
					sym = GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT());
					scope.makeUnique(sym);
					scope.getSymbols().add(sym);
				}
				
				ScopDimension lDim = ScopFactory.eINSTANCE.createScopDimension();;
				lDim.setName(name);
				
				dim = lDim;
			}
			res.add(dim);
		}
		return res;
	}

	protected boolean isScalar(int i, Collection<List<IntExpression>> schedules) {
		for (List<IntExpression> schedule : schedules) {
			IntExpression expr = schedule.get(i);
			if (!(expr.isConstant() == FuzzyBoolean.YES)) return false;
		}
		return true;
	}
}
