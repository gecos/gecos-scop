package fr.irisa.cairn.gecos.model.scop.pragma.extractor

import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.EList
import org.polymodel.algebra.IntExpression
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.analysis.IdentityScheduleBuilder
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform

class DefaultTransformDataBuilder extends AbstractSCopTransformBuilder {
	static boolean verbose = false

	override ScopTransform buildScopTransform(GecosScopBlock root) {
		/*
		 * 
		 * 
		 * Build default Schedule ID
		 */
		if(verbose) System.out.println('''*** Default SCOP Transform data Builder for «root»''')
		var boolean first = true
		var EList<ScopDimension> schedDims = null
		var Map<ScopInstructionStatement, List<IntExpression>> map = null
		
		for (ScopInstructionStatement instr : root.listAllStatements().filter(ScopInstructionStatement)) {
			var List<IntExpression> idSched = IdentityScheduleBuilder.getSchedule(instr)
			var List<IntExpression> expressions = idSched
			if (first) {
				first = false
				var int size = expressions.size()
				if(size % 2 !== 1) throw new RuntimeException();
				map = new HashMap(size)
				schedDims = buildDefaultScheduleDimensions(root, size)
			}
			if(verbose) System.out.println('''  - ID Schedule for «instr» is «idSched»''')
			map.put(instr , expressions)
		}
		return build(root, map, schedDims)
	}
}
