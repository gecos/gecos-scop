package fr.irisa.cairn.gecos.model.scop.pragma.extractor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.polymodel.algebra.AlgebraFactory;
import org.polymodel.algebra.IntExpression;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.gecos.model.scop.transform.util.TransformSwitch;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotatedElement;

public class PragmaTransformDataBuilder extends AbstractSCopTransformBuilder{
	
	private static final boolean debug = false;
	
	

	ScopTransform data  ;
//	S2S4HLSPragmaCollector collector ;
	List<ScopTransformDirective> processedDirectives;

	private boolean foundSomething;

	private GecosScopBlock scopBlk;

	public PragmaTransformDataBuilder() {
		processedDirectives = new ArrayList<ScopTransformDirective>();
//		collector = new S2S4HLSPragmaCollector();
	}
	
	@Override
	public ScopTransform  buildScopTransform(GecosScopBlock scopBlk) {
		List<ScopTransformDirective> directives= EMFUtils.eAllContentsInstancesOf(scopBlk, ScopTransformDirective.class);
		this.scopBlk=scopBlk;
		data  = TransformFactory.eINSTANCE.createScopTransform();
		foundSomething=false;
		try {
			// Search
			for (ScopTransformDirective directive : new ArrayList<ScopTransformDirective>(directives)) {
				if (	   directive instanceof AlmaCoarseGrainPragma
						|| directive instanceof RLTInsetPragma
						|| directive instanceof AlmaFineGrainPragma) {
					ScopTransformDirective ctxt = (ScopTransformDirective) directive;
					directives.remove(directive);
					processedDirectives.add(ctxt);
					data.getCommands().add(ctxt);
					foundSomething = true;
				}
				if (directive instanceof ScheduleContextPragma) {
					ScheduleContextPragma ctxt = (ScheduleContextPragma) directive;
					directives.remove(directive);
					scopBlk.setName(ctxt.getName());
//					if (data.getContext() == null) {
//						data.setContext(AlgebraFactory.eINSTANCE
//								.createIntConstraintSystem());
//					}
//					EList<ScopDimension> dims = ctxt.getInfo();
//					if (dims.size() != 0) {
//						data.getDimensions().addAll(dims);
//					}
//					data.getContext().getConstraints().addAll(ctxt.getConstraints());
//					processedDirectives.add(ctxt);
					throw new RuntimeException("NYI");
				}
			}
			if(processedDirectives.size()==0) {
				// no context found ...
				// expects 2n+1 dimensions ...
				return null;
//				throw new UnsupportedOperationException("Not yet Implemented");
			}
			
			for(ScopTransformDirective directive : directives) {
				//collector.doSwitch(directive);
			}
			
			for(ScopTransformDirective directive : processedDirectives) {
				EObject eContainer = directive.eContainer();
				if(eContainer instanceof S2S4HLSPragmaAnnotation) {
					S2S4HLSPragmaAnnotation pragma= (S2S4HLSPragmaAnnotation) eContainer;
					
					pragma.getDirectives().remove(directive);
					if(pragma.getDirectives().size()==0) {
						AnnotatedElement annotatedElmt = (AnnotatedElement) pragma.eContainer().eContainer();
//						annotatedElmt.getAnnotations().removeKey(pragmaPackage.S2S4HLS_ANNOTATION_KEY);
						if (debug) System.out.println(annotatedElmt);
					}
				}
			}

			
			if(foundSomething) {
				//fix(data);
				return data;
			} else {
				return null;
			}
		} catch (UnsupportedOperationException e) {
			System.err.println("Error while building SCopTransform for "+scopBlk.getName()+" : " +e.getMessage());
			e.printStackTrace();
			return null;
		}
				
	}

//	private void fix(ScopTransform data) {
//		if(data.getDimensions().size()==0) {
//			
//		}
//		
//	}

//	private class S2S4HLSPragmaCollector extends pragmaSwitch<Object> {
//		
//		
//		ScopTransformDirectiveCollector collector ;
//		
//		public S2S4HLSPragmaCollector() {
//			collector = new ScopTransformDirectiveCollector();
//		}
//		
//
//		@Override
//		public Object caseScheduleStatementPragma(ScheduleStatementPragma object) {
//			
//			/** defensive checks */
//			EList<IntExpression> exprs = object.getExprs();
//
//			S2S4HLSPragmaAnnotation annotation = (S2S4HLSPragmaAnnotation) object.eContainer();
//			ScopStatement  statement =null;
//			for(ScopStatement stmt : scopBlk.listAllStatements()) {
//				AnnotatedElement target = annotation.getTarget();
//				if(stmt == target) {
//					statement=stmt;
//					break;
//				}
//			}
//			if(statement==null) {
//				throw new UnsupportedOperationException("Schedule Statement Directive "+object+" has no corresponding statement");
//			}
//
//			/**
//			 * Building input and output dimension to user the Relation class in org.polymodel 
//			 */
//			
//			EList<ScopDimension> dimensions = data.getDimensions();
//			if(exprs.size()!=dimensions.size()) {
//				throw new UnsupportedOperationException("Schedule Directive "+object+" has an inconsistent number of dimension "+dimensions+" expected, but "+exprs+" found in "+object);
//			}
//			
//			IASTNodeReference ref = IASTNodeReference.getNodeReference(object.eContainer());
//			ScheduleDirective schedule = buildDirective(statement, exprs, dimensions, scopBlk.getDimensionsManager());
//			data.getSchedule().add(schedule);
//			if (ref != null)
//				schedule.eAdapters().add(ref);
//			else
//				if (debug) System.out.println("No pragma : " + schedule);
//			
//			processedDirectives.add(object);
//			foundSomething=true;
//			return true;
//		}
//
//
//		@Override
//		public Object caseScheduleBlockPragma(ScheduleBlockPragma object) {
//			throw new UnsupportedOperationException("Schedule Block Pragma "+object+" is not supported");
//		}
//
//		@Override
//		public Object defaultCase(EObject object) {
//			if(object instanceof ScopTransformDirective)
//				collector.doSwitch(object);
//			return true;
//		}
//
//	}
//	



}
