package fr.irisa.cairn.gecos.model.scop.pragma.extractor;

import org.eclipse.emf.ecore.EObject;

public class S2S4HLSPragmaParseException extends RuntimeException {

	private static final long serialVersionUID = 6864726185949303804L;
	
//	public final AnnotatedElement annotatedElement;
	public final EObject errorSource;
	
	public S2S4HLSPragmaParseException(String message, Throwable originalException, EObject annotation) {
		super(message, originalException);
		this.errorSource = annotation;
	}
	
}
