package fr.irisa.cairn.gecos.model.scop.pragma.extractor

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.common.notify.Adapter
import org.eclipse.emf.common.util.URI 
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.Resource.Diagnostic
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.IResourceFactory
import org.eclipse.xtext.resource.IResourceServiceProvider
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.util.StringInputStream
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator
import org.eclipse.xtext.validation.Issue
import org.polymodel.algebra.affine.AffineTerm
import org.polymodel.algebra.factory.IntExpressionBuilder
import com.google.inject.Injector
import fr.irisa.cairn.gecos.model.cdtfrontend.IASTNodeReference
import fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack
import fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage
import fr.irisa.cairn.gecos.model.scop.pragma.PragmasStandaloneSetup
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.S2S4HLSPragmaParseException
import fr.irisa.cairn.gecos.model.scop.pragma.scoping.PragmasScopeProvider
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.annotations.AnnotatedElement
import gecos.annotations.AnnotationKeys
import gecos.annotations.PragmaAnnotation
import gecos.blocks.Block
import gecos.core.Procedure
import gecos.core.ProcedureSet
import gecos.gecosproject.GecosProject
import gecos.gecosproject.GecosSourceFile
import fr.irisa.cairn.gecos.model.scop.pragma.scoping.PragmaLinkingService

class ScopPragmaParser {
	static final String PREFIX = "[PRAGMA PARSER] " 
	static final boolean debug = true
	static final boolean VALIDATE = false
	package IResourceFactory factory
	package XtextResourceSet rs
	package Resource remf
	package Map<EObject, List<Diagnostic>> errorMap
	// Map<Integer,AnnotatedElement> map ;
	package List<Block> procedures
	static GecosProject p;
	private new() {
		errorMap = new HashMap<EObject, List<Diagnostic>>()
	}

	new(GecosProject proj) {
		this()
		p=proj;
		this.procedures = new ArrayList()
		for (ProcedureSet ps : proj.listProcedureSets()) {
			for (Procedure proc : ps.listProcedures()) {
				this.procedures.add(proc.getBody())
			}
		}
	}

	new(ProcedureSet ps) {
		this()
		this.procedures = new ArrayList()
		for (Procedure proc : ps.listProcedures()) {
			this.procedures.add(proc.getBody())
		}
	}

	new(Block... blocks) {
		this()
		this.procedures = new ArrayList(blocks.length)
		for (Block b : blocks)
			this.procedures.add(b)
	}

	def private void addError(Diagnostic error) {
		var AnnotatedElement context = PragmaLinkingService.getContext()
		var List<Diagnostic> list
		if (errorMap.containsKey(context)) {
			list = errorMap.get(context)
		} else {
			list = new ArrayList<Diagnostic>()
		}
		list.add(error)
	}

	def void compute() {
		debug('''«PREFIX»start''')
		var GecosSourceFile container = null
		for (Block block : this.procedures) { 
			var ProcedureSet ps = block.getContainingProcedureSet()
			if (ps.eContainer() instanceof GecosSourceFile) {
				container = ps.eContainer() as GecosSourceFile
			}
			var List<PragmaAnnotation> pragmas = EMFUtils.eAllContentsInstancesOf(block, PragmaAnnotation)
			PragmaLinkingService.setRoot(ps)
			for (PragmaAnnotation pragma : pragmas) {
				parsePragma(pragma)
			}
			if (container !== null) {
				container.setModel(ps)
				container = null
			}
		}
		debug('''«PREFIX»end''')
		return;
	}

	def Map<EObject, List<Diagnostic>> getErrorMap() {
		return errorMap
	}

	static int i = 0
	
	static final String GECOS_ANNOTATION_KEY = "GCS"

	def debug(String mess) {
		if (debug) println (mess);
	}
	def private void parsePragma(PragmaAnnotation pragma) {
		debug('''«PREFIX» o)  parsing pragma [«pragma»]''')
		var S2S4HLSPragmaAnnotation merged = TransformFactory.eINSTANCE.createS2S4HLSPragmaAnnotation()
		var ISetup instance = new PragmasStandaloneSetup()
		var Injector injector = instance.createInjectorAndDoEMFRegistration()
		rs = injector.getInstance(XtextResourceSet)
		factory = injector.getInstance(IResourceFactory)
		var AnnotatedElement annotatedElement = pragma.getAnnotatedElement()
		PragmaLinkingService.setContext((annotatedElement as AnnotatedElement))
		var List<String> processedEntries = new ArrayList<String>()
		var List<String> entries = new ArrayList<String>()
		entries.addAll(pragma.getContent())

		for (String entry : entries) {
			debug('''«PREFIX»    > entry [«entry»]''')
			if (entry.contains("scop") || entry.contains("gcs") || entry.contains("omp")) {
				debug('''«PREFIX»       - contains filter key''')
				try {
					// FIXME Find better URI than "entry + "_" + i++"
					var XtextResource r = (factory.createResource(URI.createURI('''«entry»_«i++»''')) as XtextResource)
					rs.getResources().add(r)
					r.load(new StringInputStream(entry), null)
					debug('''«PREFIX»       - XText prepared''')
					var AnnotatedProcSet root = (r.getParseResult().getRootASTElement() as AnnotatedProcSet)
					EcoreUtil.resolveAll(root)
					debug('''«PREFIX»       - XText computed''')
					if (r.getErrors().size() === 0) {
						debug('''#pragma "«entry»" successfully analyzed ''')
						pragma.getContent().remove(entry)
						processedEntries.add(entry)
						debug('''Removing pragma entry «entry»''')
					} else {
						debug('''«PREFIX»       - Error found''')
						for (Diagnostic error : r.getErrors()) {
							addError(error)
							debug('''«PREFIX»           >«error.getMessage()»''')
							// ignore pragmas that the xtext parser should not support
							if (!error.getMessage().startsWith("required")) {
								throw new S2S4HLSPragmaParseException(error.getMessage(), null, pragma)
							}
						}
					}
					// Call Xtext Validator (causes some exception)
					if (VALIDATE) {
						var IResourceServiceProvider serviceProvider = injector.getInstance(IResourceServiceProvider)
						var IResourceValidator resourceValidator = serviceProvider.getResourceValidator()
						var List<Issue> validate = resourceValidator.validate(r, CheckMode.ALL, null)
						for (Issue issue : validate) {
							System.err.println('''«issue.getLineNumber()»: «issue.getMessage()»''')
						}
					}
					var S2S4HLSPragmaAnnotation toplevel = (root.getPragmas().get(0) as S2S4HLSPragmaAnnotation)
					debug('''«PREFIX»       - created [«toplevel»] of type [«toplevel.getClass().getSimpleName()»]''')
					// annotatedElement.getAnnotations()
					// toplevel.setTarget(annotatedElement);
					toplevel.eAdapters().addAll(pragma.eAdapters())
					merged.getDirectives().addAll(toplevel.getDirectives())
					for (Adapter a : pragma.eAdapters()) {
						if (a instanceof IASTNodeReference) {
							merged.eAdapters().add(a)
						}
					}
				} catch (S2S4HLSPragmaParseException e) {
					debug('''could not parse «entry»''')
					throw new S2S4HLSPragmaParseException('''Exception in parsing pragma "«entry»" : «e.getLocalizedMessage()»''',
						e, pragma)
				} catch (Exception e) {
					debug('''could not parse «entry»''')
					throw new S2S4HLSPragmaParseException('''Exception in parsing pragma "«entry»" : «e.getLocalizedMessage()»''',e, pragma)
				}

			} else {
				debug('''«PREFIX»       - does not contain filter key >> ignore''')
			}
		}
		if (processedEntries.size() > 0) {
			debug('''«PREFIX»    >> post process''')
			annotatedElement.setAnnotation(GECOS_ANNOTATION_KEY, merged);
			if (pragma.getContent().size() === 0) {
				debug('''«PREFIX»       - pragma is empty >> removing''')
				annotatedElement.getAnnotations().remove(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral())
			}
			//
			var PragmaAnnotation copy = (new GecosCopier().copy(pragma) as PragmaAnnotation)
			copy.getContent().clear()
			copy.getContent().addAll(processedEntries);
			merged.setOriginalPragma(copy)
			var List<AffineTermHack> affineTermHacks = EMFUtils.eAllContentsInstancesOf(merged, AffineTermHack)
			for (AffineTermHack hack : affineTermHacks) {
				var AffineTerm nt = IntExpressionBuilder.term(hack.getVariable())
				EMFUtils.substituteByNewObjectInContainer(hack, nt)
			}
		} else {
			debug('''«PREFIX»    >> no entry processed''')
		}
	}
}
