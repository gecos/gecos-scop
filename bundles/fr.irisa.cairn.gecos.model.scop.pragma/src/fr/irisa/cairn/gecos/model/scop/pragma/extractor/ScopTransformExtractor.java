package fr.irisa.cairn.gecos.model.scop.pragma.extractor;


import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.ScopPragmaParser;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;


public class ScopTransformExtractor {

	public boolean throwExceptions = false;
	List<ProcedureSet> procedures;

	public ScopTransformExtractor(GecosProject proj) {
		this.procedures=new ArrayList<>(proj.getSources().size());
		for (GecosSourceFile f : proj.getSources()) {
			this.procedures.add(f.getModel());
		}
	}

	public ScopTransformExtractor(ProcedureSet ps) {
		this.procedures=new ArrayList<>(1);
		this.procedures.add(ps);
	}
	
	public void compute()  {
		GecosSourceFile container = null;
		for (ProcedureSet ps : this.procedures) {
			if (ps.eContainer() instanceof GecosSourceFile)
				container = (GecosSourceFile)ps.eContainer();
			List<GecosScopBlock> scopRoots= EMFUtils.eAllContentsInstancesOf(ps,GecosScopBlock.class);
			
			for(GecosScopBlock scopBlk : scopRoots) {
				ScopPragmaParser pragmaAnalyzer = new ScopPragmaParser(scopBlk);
				S2S4HLSPragmaParseException ex = null;
				try {
					pragmaAnalyzer.compute();
				} catch (S2S4HLSPragmaParseException e) {
					if (throwExceptions && ex == null) ex = e;
				}
				PragmaTransformDataBuilder pragmaBuilder = new PragmaTransformDataBuilder();
				DefaultTransformDataBuilder defaultBuilder = new DefaultTransformDataBuilder();
				ScopTransform data = null;
				try {
					data = pragmaBuilder.buildScopTransform(scopBlk);
				} catch (S2S4HLSPragmaParseException e) {
					if (throwExceptions && ex == null) ex = e;
				}
				
				if(data==null) {
					data = defaultBuilder.buildScopTransform(scopBlk);
				} else if (!validation(data, scopBlk)) {
					// If inconsistent data is found, we fall back to default data 
					ScopTransform newdata = defaultBuilder.buildScopTransform(scopBlk);
					
					//and update commands
//					if (data.getCommands() != null)
//						for (S2S4HLSDirective dir : data.getCommands().toArray(new S2S4HLSDirective[data.getCommands().size()]))
//							newdata.getCommands().add(dir);
//					if (data.getContext() != null)
//						newdata.setContext(data.getContext());
//					if (data.getLayout() != null)
//						for (LayoutDirective dir : data.getLayout().toArray(new LayoutDirective[data.getLayout().size()]))
//							newdata.getLayout().add(dir);
//					if (data.getOptions() != null)
//						newdata.setOptions(data.getOptions());
					
					data = newdata;
				}
				
				scopBlk.setTransform(data);
				if (throwExceptions && ex != null) 
					throw ex;

				if (container != null && container.getModel() == null)
					throw new RuntimeException("S2S4HLSPragmaParser altered the model in an irrecuperable way.");
				
			}
		}
	}

	private boolean validation(ScopTransform data, GecosScopBlock scopBlk) {
		EList<ScopStatement> schedules = scopBlk.listAllStatements();
		int nbStatement = schedules.size();
		if (data.listAllScheduleDirectives().size() != nbStatement) {
			EObject target = null;
			for (ScopStatement i : schedules) {
				throw new S2S4HLSPragmaParseException("NYI",null,target);
//				//ISLMap scheduleFor = data.getScheduleFor((GScopInstruction)i);
//				if(scheduleFor == null) {
//					target = i;
//					break;
//				}
			}
			if (target != null) {
				if (throwExceptions)
					throw new S2S4HLSPragmaParseException("The schedule has "+schedules.size()+" directives whereas the Scop has "+nbStatement+" statements.",null,target);
				return false;
			}
		}
		
		//return data.getDimensions().size()>0;
		return true;
	}
		

}
