package fr.irisa.cairn.gecos.model.scop.pragma.scoping

import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma
import gecos.core.CoreFactory
import gecos.core.ProcedureSet
import gecos.core.Scope
import gecos.core.ScopeContainer
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.linking.impl.DefaultLinkingService
import org.eclipse.xtext.linking.impl.IllegalNodeException
import org.eclipse.xtext.nodemodel.INode
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective
import gecos.annotations.AnnotatedElement
import fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma
import fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma

/** 
 * This class implements a custom Linking Service to be able to handle references to symbols 
 * that are not declared statically. Whenever a reference to a Symbol is expected (and not found), 
 * we create a new symbol with the corresponding name and add it to the adequate scope.  
 * @author steven
 */
class PragmaLinkingService extends DefaultLinkingService {

	private static final boolean debug = false;
	static AnnotatedElement element;
	static ProcedureSet root;
	
	public static def AnnotatedElement getContext() {
		return fr.irisa.cairn.gecos.model.scop.pragma.scoping.PragmaLinkingService.element;
	}
	
	public static def ProcedureSet getRoot() {
		return root;
	}

	public static def void setContext(AnnotatedElement context) {
		element = context;
	}
	
	public static def void setRoot(ProcedureSet root) {
		if(PragmaLinkingService.root != root) {
			PragmaLinkingService.root = root;
		}
	}

	def Scope getScope(EObject current_finalParam_) {
		var  current=current_finalParam_ 
		while (current !== null) {
			if (current instanceof ProcedureSet) {
				var ScopeContainer module=(current as ScopeContainer) 
				if (module.getScope() === null) {
					module.setScope(CoreFactory.eINSTANCE.createScope()) 
				}
				return module.getScope() 
			}
			current=current.eContainer() 
		}
		return null 
	}

	def dispatch List<EObject> findLinkedObjects(EObject context, EReference ref, INode node) {
		super.getLinkedObjects(context, ref, node) 
	}	


	def getSymbolScope(EObject obj) {
		var current =obj
		while (current!==null) {
			switch(current) {
				ScopeContainer :{
					return current.scope.lookupAllSymbols
				}
			}
			current=current.eContainer;
		}
		return null;
	}
 
	def dispatch List<EObject> findLinkedObjects(ContractArrayDirective obj, EReference ref, INode node) {
		getSymbolScope(element).filter[s|s.name==node.text].toList as List
	}	
	def dispatch List<EObject> findLinkedObjects(DuplicateNodePragma obj, EReference ref, INode node) {
		getSymbolScope(element).filter[s|s.name==node.text].toList as List
	}	
	def dispatch List<EObject> findLinkedObjects(DataMotionPragma obj, EReference ref, INode node) {
		getSymbolScope(element).filter[s|s.name==node.text].toList as List
	}	
	def dispatch List<EObject> findLinkedObjects(MergeArraysPragma obj, EReference ref, INode node) {
		getSymbolScope(element).filter[s|s.name==node.text].toList as List
	}	
	
	override  List<EObject> getLinkedObjects(EObject context, EReference ref, INode node) throws IllegalNodeException {
		
		findLinkedObjects(context, ref, node);
	}

	
		
//	def List<EObject> oldgetLinkedObjects(EObject context, EReference ref, INode node) throws IllegalNodeException {
//		var List<EObject> res=super.getLinkedObjects(context, ref, node) 
//		var String text=node.getText() 
//		if (context instanceof TypeInfo) {
//			if (VERBOSE) System.err.println('''Warning : fixing linking issue for TypeInfo pragma symbol «text»''') 
//			if (res.size() === 0) {
//				var Symbol newSym=createVariable(text, getScope(context)) 
//				return Collections.singletonList((newSym as EObject)) 
//			}
//		}
//		if (context instanceof FieldInstruction) {
//			if (VERBOSE) System.err.println('''Warning : fixing linking issue for Field Info «text»''') 
//			if (res.size() === 0) {
//				var Field newSym=createField(text, getScope(context)) 
//				return Collections.singletonList((newSym as EObject)) 
//			}
//		}
//		if ((context instanceof SymbolInstruction) || (context instanceof ScilabForBlock)) {
//			if (VERBOSE) System.err.println('''Warning : fixing linking issue for variable symbol «text»''') 
//			if (res.size() === 0) {
//				var Symbol newSym=createVariable(text, getScope(context)) 
//				return Collections.singletonList((newSym as EObject)) 
//			}
//		}
//		if (context instanceof ScilabProcedure) {
//			var ScilabProcedure mproc=(context as ScilabProcedure) 
//			
//			switch (ref.getFeatureID()) {//case ScilabPackage.SCILAB_PROCEDURE__ARGS:
//				
//				case ScilabPackage.SCILAB_PROCEDURE__RESULTS:/* FIXME unsupported fall-through */{
//					if (mproc.getScope() === null) {
//						mproc.setScope(GecosUserCoreFactory.scope()) 
//					}if (VERBOSE) System.err.println('''Warning : fixing linking issue for function symbol «text»''') if (res.size() === 0) {
//						var Symbol newSym=createParameter(text, mproc.getScope()) 
//						return Collections.singletonList((newSym as EObject)) 
//					}/* FIXME Unsupported BreakStatement */ break
//				}
//				default :{
//					
//				}
//			}
//		}
//		if (context instanceof ScilabForBlock) {
//			
//			switch (ref.getFeatureID()) {
//				case ScilabPackage.SCILAB_FOR_BLOCK__ITERATOR:{
//					if (VERBOSE) System.err.println('''Warning : fixing linking issue for iterator symbol «text»''') var Symbol newSym=createIntVariable(text, getScope(context)) return Collections.singletonList((newSym as EObject)) 
//				}
//				default :{
//					
//				}
//			}
//		}
//		return res 
//	}
}