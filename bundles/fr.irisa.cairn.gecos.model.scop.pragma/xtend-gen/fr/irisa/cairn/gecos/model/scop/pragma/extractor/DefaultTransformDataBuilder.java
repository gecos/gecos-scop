package fr.irisa.cairn.gecos.model.scop.pragma.extractor;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.IdentityScheduleBuilder;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.AbstractSCopTransformBuilder;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class DefaultTransformDataBuilder extends AbstractSCopTransformBuilder {
  private static boolean verbose = false;
  
  @Override
  public ScopTransform buildScopTransform(final GecosScopBlock root) {
    if (DefaultTransformDataBuilder.verbose) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("*** Default SCOP Transform data Builder for ");
      _builder.append(root);
      System.out.println(_builder);
    }
    boolean first = true;
    EList<ScopDimension> schedDims = null;
    Map<ScopInstructionStatement, List<IntExpression>> map = null;
    Iterable<ScopInstructionStatement> _filter = Iterables.<ScopInstructionStatement>filter(root.listAllStatements(), ScopInstructionStatement.class);
    for (final ScopInstructionStatement instr : _filter) {
      {
        List<IntExpression> idSched = IdentityScheduleBuilder.getSchedule(instr);
        List<IntExpression> expressions = idSched;
        if (first) {
          first = false;
          int size = expressions.size();
          if (((size % 2) != 1)) {
            throw new RuntimeException();
          }
          HashMap<ScopInstructionStatement, List<IntExpression>> _hashMap = new HashMap<ScopInstructionStatement, List<IntExpression>>(size);
          map = _hashMap;
          schedDims = this.buildDefaultScheduleDimensions(root, size);
        }
        if (DefaultTransformDataBuilder.verbose) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("  ");
          _builder_1.append("- ID Schedule for ");
          _builder_1.append(instr, "  ");
          _builder_1.append(" is ");
          _builder_1.append(idSched, "  ");
          System.out.println(_builder_1);
        }
        map.put(instr, expressions);
      }
    }
    return this.build(root, map, schedDims);
  }
}
