package fr.irisa.cairn.gecos.model.scop.pragma.extractor;

import com.google.inject.Injector;
import fr.irisa.cairn.gecos.model.cdtfrontend.IASTNodeReference;
import fr.irisa.cairn.gecos.model.scop.pragma.PragmasStandaloneSetup;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.S2S4HLSPragmaParseException;
import fr.irisa.cairn.gecos.model.scop.pragma.scoping.PragmaLinkingService;
import fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack;
import fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.IResourceServiceProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.StringInputStream;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.polymodel.algebra.affine.AffineTerm;
import org.polymodel.algebra.factory.IntExpressionBuilder;

@SuppressWarnings("all")
public class ScopPragmaParser {
  private final static String PREFIX = "[PRAGMA PARSER] ";
  
  private final static boolean debug = true;
  
  private final static boolean VALIDATE = false;
  
  IResourceFactory factory;
  
  XtextResourceSet rs;
  
  Resource remf;
  
  Map<EObject, List<Resource.Diagnostic>> errorMap;
  
  List<Block> procedures;
  
  private static GecosProject p;
  
  private ScopPragmaParser() {
    HashMap<EObject, List<Resource.Diagnostic>> _hashMap = new HashMap<EObject, List<Resource.Diagnostic>>();
    this.errorMap = _hashMap;
  }
  
  public ScopPragmaParser(final GecosProject proj) {
    this();
    ScopPragmaParser.p = proj;
    ArrayList<Block> _arrayList = new ArrayList<Block>();
    this.procedures = _arrayList;
    EList<ProcedureSet> _listProcedureSets = proj.listProcedureSets();
    for (final ProcedureSet ps : _listProcedureSets) {
      EList<Procedure> _listProcedures = ps.listProcedures();
      for (final Procedure proc : _listProcedures) {
        this.procedures.add(proc.getBody());
      }
    }
  }
  
  public ScopPragmaParser(final ProcedureSet ps) {
    this();
    ArrayList<Block> _arrayList = new ArrayList<Block>();
    this.procedures = _arrayList;
    EList<Procedure> _listProcedures = ps.listProcedures();
    for (final Procedure proc : _listProcedures) {
      this.procedures.add(proc.getBody());
    }
  }
  
  public ScopPragmaParser(final Block... blocks) {
    this();
    int _length = blocks.length;
    ArrayList<Block> _arrayList = new ArrayList<Block>(_length);
    this.procedures = _arrayList;
    for (final Block b : blocks) {
      this.procedures.add(b);
    }
  }
  
  private void addError(final Resource.Diagnostic error) {
    AnnotatedElement context = PragmaLinkingService.getContext();
    List<Resource.Diagnostic> list = null;
    boolean _containsKey = this.errorMap.containsKey(context);
    if (_containsKey) {
      list = this.errorMap.get(context);
    } else {
      ArrayList<Resource.Diagnostic> _arrayList = new ArrayList<Resource.Diagnostic>();
      list = _arrayList;
    }
    list.add(error);
  }
  
  public void compute() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(ScopPragmaParser.PREFIX);
    _builder.append("start");
    this.debug(_builder.toString());
    GecosSourceFile container = null;
    for (final Block block : this.procedures) {
      {
        ProcedureSet ps = block.getContainingProcedureSet();
        EObject _eContainer = ps.eContainer();
        if ((_eContainer instanceof GecosSourceFile)) {
          EObject _eContainer_1 = ps.eContainer();
          container = ((GecosSourceFile) _eContainer_1);
        }
        List<PragmaAnnotation> pragmas = EMFUtils.<PragmaAnnotation>eAllContentsInstancesOf(block, PragmaAnnotation.class);
        PragmaLinkingService.setRoot(ps);
        for (final PragmaAnnotation pragma : pragmas) {
          this.parsePragma(pragma);
        }
        if ((container != null)) {
          container.setModel(ps);
          container = null;
        }
      }
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append(ScopPragmaParser.PREFIX);
    _builder_1.append("end");
    this.debug(_builder_1.toString());
    return;
  }
  
  public Map<EObject, List<Resource.Diagnostic>> getErrorMap() {
    return this.errorMap;
  }
  
  private static int i = 0;
  
  private final static String GECOS_ANNOTATION_KEY = "GCS";
  
  public String debug(final String mess) {
    String _xifexpression = null;
    if (ScopPragmaParser.debug) {
      _xifexpression = InputOutput.<String>println(mess);
    }
    return _xifexpression;
  }
  
  private void parsePragma(final PragmaAnnotation pragma) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(ScopPragmaParser.PREFIX);
    _builder.append(" o)  parsing pragma [");
    _builder.append(pragma);
    _builder.append("]");
    this.debug(_builder.toString());
    S2S4HLSPragmaAnnotation merged = TransformFactory.eINSTANCE.createS2S4HLSPragmaAnnotation();
    ISetup instance = new PragmasStandaloneSetup();
    Injector injector = instance.createInjectorAndDoEMFRegistration();
    this.rs = injector.<XtextResourceSet>getInstance(XtextResourceSet.class);
    this.factory = injector.<IResourceFactory>getInstance(IResourceFactory.class);
    AnnotatedElement annotatedElement = pragma.getAnnotatedElement();
    PragmaLinkingService.setContext(((AnnotatedElement) annotatedElement));
    List<String> processedEntries = new ArrayList<String>();
    List<String> entries = new ArrayList<String>();
    entries.addAll(pragma.getContent());
    for (final String entry : entries) {
      {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(ScopPragmaParser.PREFIX);
        _builder_1.append("    > entry [");
        _builder_1.append(entry);
        _builder_1.append("]");
        this.debug(_builder_1.toString());
        if (((entry.contains("scop") || entry.contains("gcs")) || entry.contains("omp"))) {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append(ScopPragmaParser.PREFIX);
          _builder_2.append("       - contains filter key");
          this.debug(_builder_2.toString());
          try {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append(entry);
            _builder_3.append("_");
            int _plusPlus = ScopPragmaParser.i++;
            _builder_3.append(_plusPlus);
            Resource _createResource = this.factory.createResource(URI.createURI(_builder_3.toString()));
            XtextResource r = ((XtextResource) _createResource);
            this.rs.getResources().add(r);
            StringInputStream _stringInputStream = new StringInputStream(entry);
            r.load(_stringInputStream, null);
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append(ScopPragmaParser.PREFIX);
            _builder_4.append("       - XText prepared");
            this.debug(_builder_4.toString());
            EObject _rootASTElement = r.getParseResult().getRootASTElement();
            AnnotatedProcSet root = ((AnnotatedProcSet) _rootASTElement);
            EcoreUtil.resolveAll(root);
            StringConcatenation _builder_5 = new StringConcatenation();
            _builder_5.append(ScopPragmaParser.PREFIX);
            _builder_5.append("       - XText computed");
            this.debug(_builder_5.toString());
            int _size = r.getErrors().size();
            boolean _tripleEquals = (_size == 0);
            if (_tripleEquals) {
              StringConcatenation _builder_6 = new StringConcatenation();
              _builder_6.append("#pragma \"");
              _builder_6.append(entry);
              _builder_6.append("\" successfully analyzed ");
              this.debug(_builder_6.toString());
              pragma.getContent().remove(entry);
              processedEntries.add(entry);
              StringConcatenation _builder_7 = new StringConcatenation();
              _builder_7.append("Removing pragma entry ");
              _builder_7.append(entry);
              this.debug(_builder_7.toString());
            } else {
              StringConcatenation _builder_8 = new StringConcatenation();
              _builder_8.append(ScopPragmaParser.PREFIX);
              _builder_8.append("       - Error found");
              this.debug(_builder_8.toString());
              EList<Resource.Diagnostic> _errors = r.getErrors();
              for (final Resource.Diagnostic error : _errors) {
                {
                  this.addError(error);
                  StringConcatenation _builder_9 = new StringConcatenation();
                  _builder_9.append(ScopPragmaParser.PREFIX);
                  _builder_9.append("           >");
                  String _message = error.getMessage();
                  _builder_9.append(_message);
                  this.debug(_builder_9.toString());
                  boolean _startsWith = error.getMessage().startsWith("required");
                  boolean _not = (!_startsWith);
                  if (_not) {
                    String _message_1 = error.getMessage();
                    throw new S2S4HLSPragmaParseException(_message_1, null, pragma);
                  }
                }
              }
            }
            if (ScopPragmaParser.VALIDATE) {
              IResourceServiceProvider serviceProvider = injector.<IResourceServiceProvider>getInstance(IResourceServiceProvider.class);
              IResourceValidator resourceValidator = serviceProvider.getResourceValidator();
              List<Issue> validate = resourceValidator.validate(r, CheckMode.ALL, null);
              for (final Issue issue : validate) {
                StringConcatenation _builder_9 = new StringConcatenation();
                Integer _lineNumber = issue.getLineNumber();
                _builder_9.append(_lineNumber);
                _builder_9.append(": ");
                String _message = issue.getMessage();
                _builder_9.append(_message);
                System.err.println(_builder_9);
              }
            }
            S2S4HLSPragmaAnnotation _get = root.getPragmas().get(0);
            S2S4HLSPragmaAnnotation toplevel = ((S2S4HLSPragmaAnnotation) _get);
            StringConcatenation _builder_10 = new StringConcatenation();
            _builder_10.append(ScopPragmaParser.PREFIX);
            _builder_10.append("       - created [");
            _builder_10.append(toplevel);
            _builder_10.append("] of type [");
            String _simpleName = toplevel.getClass().getSimpleName();
            _builder_10.append(_simpleName);
            _builder_10.append("]");
            this.debug(_builder_10.toString());
            toplevel.eAdapters().addAll(pragma.eAdapters());
            merged.getDirectives().addAll(toplevel.getDirectives());
            EList<Adapter> _eAdapters = pragma.eAdapters();
            for (final Adapter a : _eAdapters) {
              if ((a instanceof IASTNodeReference)) {
                merged.eAdapters().add(a);
              }
            }
          } catch (final Throwable _t) {
            if (_t instanceof S2S4HLSPragmaParseException) {
              final S2S4HLSPragmaParseException e = (S2S4HLSPragmaParseException)_t;
              StringConcatenation _builder_11 = new StringConcatenation();
              _builder_11.append("could not parse ");
              _builder_11.append(entry);
              this.debug(_builder_11.toString());
              StringConcatenation _builder_12 = new StringConcatenation();
              _builder_12.append("Exception in parsing pragma \"");
              _builder_12.append(entry);
              _builder_12.append("\" : ");
              String _localizedMessage = e.getLocalizedMessage();
              _builder_12.append(_localizedMessage);
              throw new S2S4HLSPragmaParseException(_builder_12.toString(), e, pragma);
            } else if (_t instanceof Exception) {
              final Exception e_1 = (Exception)_t;
              StringConcatenation _builder_13 = new StringConcatenation();
              _builder_13.append("could not parse ");
              _builder_13.append(entry);
              this.debug(_builder_13.toString());
              StringConcatenation _builder_14 = new StringConcatenation();
              _builder_14.append("Exception in parsing pragma \"");
              _builder_14.append(entry);
              _builder_14.append("\" : ");
              String _localizedMessage_1 = e_1.getLocalizedMessage();
              _builder_14.append(_localizedMessage_1);
              throw new S2S4HLSPragmaParseException(_builder_14.toString(), e_1, pragma);
            } else {
              throw Exceptions.sneakyThrow(_t);
            }
          }
        } else {
          StringConcatenation _builder_15 = new StringConcatenation();
          _builder_15.append(ScopPragmaParser.PREFIX);
          _builder_15.append("       - does not contain filter key >> ignore");
          this.debug(_builder_15.toString());
        }
      }
    }
    int _size = processedEntries.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(ScopPragmaParser.PREFIX);
      _builder_1.append("    >> post process");
      this.debug(_builder_1.toString());
      annotatedElement.setAnnotation(ScopPragmaParser.GECOS_ANNOTATION_KEY, merged);
      int _size_1 = pragma.getContent().size();
      boolean _tripleEquals = (_size_1 == 0);
      if (_tripleEquals) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(ScopPragmaParser.PREFIX);
        _builder_2.append("       - pragma is empty >> removing");
        this.debug(_builder_2.toString());
        annotatedElement.getAnnotations().remove(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
      }
      EObject _copy = new GecosCopier().copy(pragma);
      PragmaAnnotation copy = ((PragmaAnnotation) _copy);
      copy.getContent().clear();
      copy.getContent().addAll(processedEntries);
      merged.setOriginalPragma(copy);
      List<AffineTermHack> affineTermHacks = EMFUtils.<AffineTermHack>eAllContentsInstancesOf(merged, AffineTermHack.class);
      for (final AffineTermHack hack : affineTermHacks) {
        {
          AffineTerm nt = IntExpressionBuilder.term(hack.getVariable());
          EMFUtils.substituteByNewObjectInContainer(hack, nt);
        }
      }
    } else {
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(ScopPragmaParser.PREFIX);
      _builder_3.append("    >> no entry processed");
      this.debug(_builder_3.toString());
    }
  }
}
