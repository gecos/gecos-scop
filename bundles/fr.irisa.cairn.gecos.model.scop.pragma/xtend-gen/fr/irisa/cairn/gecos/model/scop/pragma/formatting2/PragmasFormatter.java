/**
 * generated by Xtext 2.12.0
 */
package fr.irisa.cairn.gecos.model.scop.pragma.formatting2;

import com.google.inject.Inject;
import fr.irisa.cairn.gecos.model.scop.pragma.services.PragmasGrammarAccess;
import fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class PragmasFormatter extends AbstractFormatter2 {
  @Inject
  @Extension
  private PragmasGrammarAccess _pragmasGrammarAccess;
  
  protected void _format(final AnnotatedProcSet annotatedProcSet, @Extension final IFormattableDocument document) {
    EList<S2S4HLSPragmaAnnotation> _pragmas = annotatedProcSet.getPragmas();
    for (final S2S4HLSPragmaAnnotation s2S4HLSPragmaAnnotation : _pragmas) {
      document.<S2S4HLSPragmaAnnotation>format(s2S4HLSPragmaAnnotation);
    }
  }
  
  protected void _format(final S2S4HLSPragmaAnnotation s2S4HLSPragmaAnnotation, @Extension final IFormattableDocument document) {
    EList<ScopTransformDirective> _directives = s2S4HLSPragmaAnnotation.getDirectives();
    for (final ScopTransformDirective scopTransformDirective : _directives) {
      document.<ScopTransformDirective>format(scopTransformDirective);
    }
  }
  
  public void format(final Object s2S4HLSPragmaAnnotation, final IFormattableDocument document) {
    if (s2S4HLSPragmaAnnotation instanceof S2S4HLSPragmaAnnotation) {
      _format((S2S4HLSPragmaAnnotation)s2S4HLSPragmaAnnotation, document);
      return;
    } else if (s2S4HLSPragmaAnnotation instanceof XtextResource) {
      _format((XtextResource)s2S4HLSPragmaAnnotation, document);
      return;
    } else if (s2S4HLSPragmaAnnotation instanceof AnnotatedProcSet) {
      _format((AnnotatedProcSet)s2S4HLSPragmaAnnotation, document);
      return;
    } else if (s2S4HLSPragmaAnnotation instanceof EObject) {
      _format((EObject)s2S4HLSPragmaAnnotation, document);
      return;
    } else if (s2S4HLSPragmaAnnotation == null) {
      _format((Void)null, document);
      return;
    } else if (s2S4HLSPragmaAnnotation != null) {
      _format(s2S4HLSPragmaAnnotation, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(s2S4HLSPragmaAnnotation, document).toString());
    }
  }
}
