package fr.irisa.cairn.gecos.model.scop.pragma.scoping;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective;
import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma;
import fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma;
import gecos.annotations.AnnotatedElement;
import gecos.core.CoreFactory;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * This class implements a custom Linking Service to be able to handle references to symbols
 * that are not declared statically. Whenever a reference to a Symbol is expected (and not found),
 * we create a new symbol with the corresponding name and add it to the adequate scope.
 * @author steven
 */
@SuppressWarnings("all")
public class PragmaLinkingService extends DefaultLinkingService {
  private final static boolean debug = false;
  
  private static AnnotatedElement element;
  
  private static ProcedureSet root;
  
  public static AnnotatedElement getContext() {
    return PragmaLinkingService.element;
  }
  
  public static ProcedureSet getRoot() {
    return PragmaLinkingService.root;
  }
  
  public static void setContext(final AnnotatedElement context) {
    PragmaLinkingService.element = context;
  }
  
  public static void setRoot(final ProcedureSet root) {
    boolean _notEquals = (!Objects.equal(PragmaLinkingService.root, root));
    if (_notEquals) {
      PragmaLinkingService.root = root;
    }
  }
  
  public Scope getScope(final EObject current_finalParam_) {
    EObject current = current_finalParam_;
    while ((current != null)) {
      {
        if ((current instanceof ProcedureSet)) {
          ScopeContainer module = ((ScopeContainer) current);
          Scope _scope = module.getScope();
          boolean _tripleEquals = (_scope == null);
          if (_tripleEquals) {
            module.setScope(CoreFactory.eINSTANCE.createScope());
          }
          return module.getScope();
        }
        current = current.eContainer();
      }
    }
    return null;
  }
  
  protected List<EObject> _findLinkedObjects(final EObject context, final EReference ref, final INode node) {
    return super.getLinkedObjects(context, ref, node);
  }
  
  public EList<Symbol> getSymbolScope(final EObject obj) {
    EObject current = obj;
    while ((current != null)) {
      {
        boolean _matched = false;
        if (current instanceof ScopeContainer) {
          _matched=true;
          return ((ScopeContainer)current).getScope().lookupAllSymbols();
        }
        current = current.eContainer();
      }
    }
    return null;
  }
  
  protected List<EObject> _findLinkedObjects(final ContractArrayDirective obj, final EReference ref, final INode node) {
    final Function1<Symbol, Boolean> _function = (Symbol s) -> {
      String _name = s.getName();
      String _text = node.getText();
      return Boolean.valueOf(Objects.equal(_name, _text));
    };
    List<Symbol> _list = IterableExtensions.<Symbol>toList(IterableExtensions.<Symbol>filter(this.getSymbolScope(PragmaLinkingService.element), _function));
    return ((List) _list);
  }
  
  protected List<EObject> _findLinkedObjects(final DuplicateNodePragma obj, final EReference ref, final INode node) {
    final Function1<Symbol, Boolean> _function = (Symbol s) -> {
      String _name = s.getName();
      String _text = node.getText();
      return Boolean.valueOf(Objects.equal(_name, _text));
    };
    List<Symbol> _list = IterableExtensions.<Symbol>toList(IterableExtensions.<Symbol>filter(this.getSymbolScope(PragmaLinkingService.element), _function));
    return ((List) _list);
  }
  
  protected List<EObject> _findLinkedObjects(final DataMotionPragma obj, final EReference ref, final INode node) {
    final Function1<Symbol, Boolean> _function = (Symbol s) -> {
      String _name = s.getName();
      String _text = node.getText();
      return Boolean.valueOf(Objects.equal(_name, _text));
    };
    List<Symbol> _list = IterableExtensions.<Symbol>toList(IterableExtensions.<Symbol>filter(this.getSymbolScope(PragmaLinkingService.element), _function));
    return ((List) _list);
  }
  
  protected List<EObject> _findLinkedObjects(final MergeArraysPragma obj, final EReference ref, final INode node) {
    final Function1<Symbol, Boolean> _function = (Symbol s) -> {
      String _name = s.getName();
      String _text = node.getText();
      return Boolean.valueOf(Objects.equal(_name, _text));
    };
    List<Symbol> _list = IterableExtensions.<Symbol>toList(IterableExtensions.<Symbol>filter(this.getSymbolScope(PragmaLinkingService.element), _function));
    return ((List) _list);
  }
  
  @Override
  public List<EObject> getLinkedObjects(final EObject context, final EReference ref, final INode node) throws IllegalNodeException {
    return this.findLinkedObjects(context, ref, node);
  }
  
  public List<EObject> findLinkedObjects(final EObject obj, final EReference ref, final INode node) {
    if (obj instanceof ContractArrayDirective) {
      return _findLinkedObjects((ContractArrayDirective)obj, ref, node);
    } else if (obj instanceof DataMotionPragma) {
      return _findLinkedObjects((DataMotionPragma)obj, ref, node);
    } else if (obj instanceof DuplicateNodePragma) {
      return _findLinkedObjects((DuplicateNodePragma)obj, ref, node);
    } else if (obj instanceof MergeArraysPragma) {
      return _findLinkedObjects((MergeArraysPragma)obj, ref, node);
    } else if (obj != null) {
      return _findLinkedObjects(obj, ref, node);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj, ref, node).toString());
    }
  }
}
