package fr.irisa.cairn.model.gecos.scop.remove;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.CompositeOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import gecos.blocks.Block;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
* Helper class for "RemoveScop" allowing the conversion of some polyhedral 
* expressions to a Gecos Instruction.
*
* -"scope" must contains all the symbols which names are involved in the 
* polymodel expressions to be converted (typicaly it can be the scope of 
* the GScopRoot object containing the input)
*
* N.B: no new Symbols are created 
*
* @author aelmouss
*
*/
@SuppressWarnings("all")
public class PolymodelConvertor{ 


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_List(Object l1, Object l2) {
return  l1.equals(l2) ;
}
private static boolean tom_is_sort_List(Object t) {
return  t instanceof java.util.List ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_org_polymodel_algebra_FuzzyBoolean(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_FuzzyBoolean(Object t) {
return t instanceof org.polymodel.algebra.FuzzyBoolean;
}
private static boolean tom_equal_term_org_polymodel_algebra_OUTPUT_FORMAT(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_OUTPUT_FORMAT(Object t) {
return t instanceof org.polymodel.algebra.OUTPUT_FORMAT;
}
private static boolean tom_equal_term_org_polymodel_algebra_Value(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_Value(Object t) {
return true;
}
private static boolean tom_equal_term_org_polymodel_algebra_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_ComparisonOperator(Object t) {
return t instanceof org.polymodel.algebra.ComparisonOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_CompositeOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_CompositeOperator(Object t) {
return t instanceof org.polymodel.algebra.CompositeOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_reductions_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_reductions_ReductionOperator(Object t) {
return t instanceof org.polymodel.algebra.reductions.ReductionOperator;
}
private static boolean tom_equal_term_ICS(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICS(Object t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static boolean tom_equal_term_ICSL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_equal_term_E(Object l1, Object l2) {
return (l1!=null && l2 instanceof IntExpression && ((IntExpression)l1).isEquivalent((IntExpression)l2) == FuzzyBoolean.YES) || l1==l2;
}
private static boolean tom_is_sort_E(Object t) {
return t instanceof IntExpression;
}
private static boolean tom_equal_term_EL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_EL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntExpression>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntExpression>)t).size()>0 && ((EList<org.polymodel.algebra.IntExpression>)t).get(0) instanceof org.polymodel.algebra.IntExpression));
}
private static boolean tom_equal_term_V(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_V(Object t) {
return t instanceof org.polymodel.algebra.Variable;
}
private static boolean tom_equal_term_vars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_vars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.Variable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.Variable>)t).size()>0 && ((EList<org.polymodel.algebra.Variable>)t).get(0) instanceof org.polymodel.algebra.Variable));
}
private static boolean tom_equal_term_T(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_T(Object t) {
return t instanceof org.polymodel.algebra.IntTerm;
}
private static boolean tom_equal_term_terms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_terms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static boolean tom_equal_term_C(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_C(Object t) {
return t instanceof org.polymodel.algebra.IntConstraint;
}
private static boolean tom_equal_term_CL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_CL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static boolean tom_equal_term_pterm(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterm(Object t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialTerm;
}
private static boolean tom_equal_term_pterms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialTerm));
}
private static boolean tom_equal_term_pvar(Object l1, Object l2) {
return (l1!=null && l2 instanceof PolynomialVariable && ((PolynomialVariable)l1).isEquivalent((PolynomialVariable)l2)) || l1==l2;
}
private static boolean tom_is_sort_pvar(Object t) {
return t instanceof PolynomialVariable;
}
private static boolean tom_equal_term_pvars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pvars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialVariable));
}
private static boolean tom_is_fun_sym_affine(IntExpression t) {
return t instanceof org.polymodel.algebra.affine.AffineExpression;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_affine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.affine.AffineExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_term(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.affine.AffineTerm;
}
private static  long  tom_get_slot_term_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getCoef();
}
private static org.polymodel.algebra.Variable tom_get_slot_term_variable(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getVariable();
}
private static boolean tom_is_fun_sym_qaffine(IntExpression t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_qaffine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.quasiAffine.QuasiAffineExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_mul(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.MUL);
}
private static IntExpression tom_get_slot_mul_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_mul_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_mod(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.MOD);
}
private static IntExpression tom_get_slot_mod_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_mod_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_ceil(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.CEIL);
}
private static IntExpression tom_get_slot_ceil_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_ceil_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_floor(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.FLOOR);
}
private static IntExpression tom_get_slot_floor_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_floor_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_div(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.DIV);
}
private static IntExpression tom_get_slot_div_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_div_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_nmul(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.MUL);
}
private static IntExpression tom_get_slot_nmul_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_nmul_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_nmod(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.MOD);
}
private static IntExpression tom_get_slot_nmod_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_nmod_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_nceil(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.CEIL);
}
private static IntExpression tom_get_slot_nceil_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_nceil_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_nfloor(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.FLOOR);
}
private static IntExpression tom_get_slot_nfloor_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_nfloor_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_ndiv(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm && ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getOperator().equals(org.polymodel.algebra.quasiAffine.QuasiAffineOperator.DIV);
}
private static IntExpression tom_get_slot_ndiv_expression(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getExpression();
}
private static  long  tom_get_slot_ndiv_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm)t).getCoef();
}
private static boolean tom_is_fun_sym_composite(IntExpression t) {
return t instanceof org.polymodel.algebra.CompositeIntExpression;
}
private static org.polymodel.algebra.CompositeOperator tom_get_slot_composite_operator(IntExpression t) {
return ((org.polymodel.algebra.CompositeIntExpression)t).getOperator();
}
private static IntExpression tom_get_slot_composite_left(IntExpression t) {
return ((org.polymodel.algebra.CompositeIntExpression)t).getLeft();
}
private static IntExpression tom_get_slot_composite_right(IntExpression t) {
return ((org.polymodel.algebra.CompositeIntExpression)t).getRight();
}
private static boolean tom_is_fun_sym_select(IntExpression t) {
return t instanceof org.polymodel.algebra.SelectExpression;
}
private static  EList<org.polymodel.algebra.IntConstraintSystem>  tom_get_slot_select_condition(IntExpression t) {
return enforce(((org.polymodel.algebra.SelectExpression)t).getCondition());
}
private static IntExpression tom_get_slot_select_then(IntExpression t) {
return ((org.polymodel.algebra.SelectExpression)t).getThen();
}
private static IntExpression tom_get_slot_select_else(IntExpression t) {
return ((org.polymodel.algebra.SelectExpression)t).getElse();
}
private static boolean tom_is_fun_sym_polynomial(IntExpression t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialExpression;
}
private static  EList<org.polymodel.algebra.polynomials.PolynomialTerm>  tom_get_slot_polynomial_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.polynomials.PolynomialExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_prod(IntExpression t) {
return t instanceof org.polymodel.algebra.reductions.ReductionExpression && ((org.polymodel.algebra.reductions.ReductionExpression)t).getOperator().equals(org.polymodel.algebra.reductions.ReductionOperator.PROD);
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_prod_expressions(IntExpression t) {
return enforce(((org.polymodel.algebra.reductions.ReductionExpression)t).getExpressions());
}
private static boolean tom_is_fun_sym_sum(IntExpression t) {
return t instanceof org.polymodel.algebra.reductions.ReductionExpression && ((org.polymodel.algebra.reductions.ReductionExpression)t).getOperator().equals(org.polymodel.algebra.reductions.ReductionOperator.SUM);
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_sum_expressions(IntExpression t) {
return enforce(((org.polymodel.algebra.reductions.ReductionExpression)t).getExpressions());
}
private static boolean tom_is_fun_sym_max(IntExpression t) {
return t instanceof org.polymodel.algebra.reductions.ReductionExpression && ((org.polymodel.algebra.reductions.ReductionExpression)t).getOperator().equals(org.polymodel.algebra.reductions.ReductionOperator.MAX);
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_max_expressions(IntExpression t) {
return enforce(((org.polymodel.algebra.reductions.ReductionExpression)t).getExpressions());
}
private static boolean tom_is_fun_sym_min(IntExpression t) {
return t instanceof org.polymodel.algebra.reductions.ReductionExpression && ((org.polymodel.algebra.reductions.ReductionExpression)t).getOperator().equals(org.polymodel.algebra.reductions.ReductionOperator.MIN);
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_min_expressions(IntExpression t) {
return enforce(((org.polymodel.algebra.reductions.ReductionExpression)t).getExpressions());
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static Instruction tom_make_symref(Symbol _symbol) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createSymref(_symbol);
}
private static Instruction tom_make_ival( long  _value) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createIval(_value);
}
private static Instruction tom_make_generic( String  _name,  EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGeneric(_name, _children);
}


private Scope scope;
private static boolean DEBUG = false; 


public PolymodelConvertor(Scope scope) {
this.scope = scope;
}

private static Instruction debug(String mess, Instruction out) {
if(DEBUG) {
System.out.println(mess+(out==null?"":out));
} 
return out;
}

public Instruction convertListOfConstraintSystem(List<IntConstraintSystem> consSys) {
debug("Converting List of IntConstraintSystem to Instruction: "+consSys, null);
EList<Instruction> instrs = new BasicEList();
for(IntConstraintSystem c : consSys) 
instrs.add(convertConstraintSystem(c));

if(instrs.size() == 1)
return debug("", instrs.get(0));
return debug("", 
tom_make_generic("lor",instrs)); 
}

public Instruction convertConstraintSystem(IntConstraintSystem consSys) {
debug("Converting IntConstraintSystem to Instruction: "+consSys, null);
EList<Instruction> instrs = new BasicEList();
for(IntConstraint c : consSys.getConstraints()) 
instrs.add(convertConstraint(c));

if(instrs.size() == 1)
return debug("", instrs.get(0));
return debug("", 
tom_make_generic("land",instrs)); 
}

public Instruction convertConstraint(IntConstraint cons) {
debug("  Converting IntConstraint to Instruction: "+cons, null);
String op;
switch(cons.getComparisonOperator()) {
case EQ:
op = ComparisonOperator.EQ.getLiteral();
break;
case NE:
op = ComparisonOperator.NE.getLiteral();
break;
case GE:
op = ComparisonOperator.GE.getLiteral();
break;
case GT:
op = ComparisonOperator.GT.getLiteral();
break;
case LE:
op = ComparisonOperator.LE.getLiteral();
break;
case LT:
op = ComparisonOperator.LT.getLiteral();
break;
default:
op = "unknown";
break;
}

return debug("  ", 
tom_make_generic(op,tom_cons_array_InstL(convertExpression(cons.getRhs()),tom_cons_array_InstL(convertExpression(cons.getLhs()),tom_empty_array_InstL(2)))));//TODO be carfull about the order of operands
}


public Instruction createGenericAdd(EList<Instruction> children) {
if(children.size() == 1)
return debug("    ", children.get(0));
Instruction res = children.get(0);
for(int i=1; i<children.size(); i++) {
Instruction child = children.get(i);
if (child instanceof IntInstruction) {
IntInstruction ii = (IntInstruction) child;
if (ii.getValue() < 0) {
ii.setValue(-ii.getValue());
res = GecosUserInstructionFactory.sub(res, child);
continue;
}
} else if (child instanceof GenericInstruction) {
GenericInstruction gi = (GenericInstruction) child;
if (gi.getName().equals(ArithmeticOperator.NEG.getLiteral())) {
res = GecosUserInstructionFactory.sub(res, gi.getOperand(0));
continue;
} else if (gi.getName().equals(ArithmeticOperator.MUL.getLiteral())) {
if (gi.getOperand(0) instanceof IntInstruction) {
IntInstruction ii = (IntInstruction) gi.getOperand(0);
if (ii.getValue() < 0) {
ii.setValue(-ii.getValue());
if (ii.getValue() == 1) 
child = gi.getOperand(1);
res = GecosUserInstructionFactory.sub(res, child);
continue;
}
}
} else if (gi.getName().equals(ArithmeticOperator.MOD.getLiteral())
|| gi.getName().equals(ArithmeticOperator.DIV.getLiteral())) {
if (gi.getOperand(1) instanceof IntInstruction) {
IntInstruction ii = (IntInstruction) gi.getOperand(1);
if (ii.getValue() < 0) {
ii.setValue(-ii.getValue());
res = GecosUserInstructionFactory.sub(res, child);
continue;
}
}
}
}
res = GecosUserInstructionFactory.add(res, child);
}
return debug("    ", res);
}

public Instruction convertExpression(IntExpression expression) {
debug("    Converting IntExpression to Instruction: "+expression, null);

{
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_affine(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntTerm t : 
tom_get_slot_affine_terms(((IntExpression)expression))) 
instrs.add(convertIntTerm(t));

return createGenericAdd(instrs); 

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_qaffine(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntTerm t : 
tom_get_slot_qaffine_terms(((IntExpression)expression))) 
instrs.add(convertIntTerm(t));

return createGenericAdd(instrs); 

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_composite(((IntExpression)((IntExpression)expression)))) {
IntExpression tom_a=tom_get_slot_composite_left(((IntExpression)expression));
IntExpression tom_b=tom_get_slot_composite_right(((IntExpression)expression));

switch(((CompositeOperator)
tom_get_slot_composite_operator(((IntExpression)expression)))) {
case MOD:
{ return 
tom_make_generic("mod",tom_cons_array_InstL(convertExpression(tom_b),tom_cons_array_InstL(convertExpression(tom_a),tom_empty_array_InstL(2)))); } 
case DIV:
{ return 
tom_make_generic("div",tom_cons_array_InstL(convertExpression(tom_b),tom_cons_array_InstL(convertExpression(tom_a),tom_empty_array_InstL(2)))); } 
case CEIL:
{ return 
tom_make_generic("ceil",tom_cons_array_InstL(convertExpression(tom_b),tom_cons_array_InstL(convertExpression(tom_a),tom_empty_array_InstL(2)))); } 
case FLOOR:
{ return 
tom_make_generic("floor",tom_cons_array_InstL(convertExpression(tom_b),tom_cons_array_InstL(convertExpression(tom_a),tom_empty_array_InstL(2)))); } 
default:
throw new RuntimeException("Unhandled operator in : " + 
((IntExpression)expression));
} 


}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_sum(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntExpression e : 
tom_get_slot_sum_expressions(((IntExpression)expression)))
instrs.add(convertExpression(e));

return createGenericAdd(instrs);

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_prod(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntExpression e : 
tom_get_slot_prod_expressions(((IntExpression)expression)))
instrs.add(convertExpression(e));

if(instrs.size() == 1)
return debug("    ", instrs.get(0));	
return debug("    ", 
tom_make_generic("mul",instrs));

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_min(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntExpression e : 
tom_get_slot_min_expressions(((IntExpression)expression)))
instrs.add(convertExpression(e));

if(instrs.size() == 1)
return debug("    ", instrs.get(0));	
return debug("    ", 
tom_make_generic("min",instrs));

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_max(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
for(IntExpression e : 
tom_get_slot_max_expressions(((IntExpression)expression)))
instrs.add(convertExpression(e));

if(instrs.size() == 1)
return debug("    ", instrs.get(0));	
return debug("    ", 
tom_make_generic("max",instrs));

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_polynomial(((IntExpression)((IntExpression)expression)))) {

throw new RuntimeException("polynomial not yet implemented");

}
}
}
}
{
if (tom_is_sort_E(expression)) {
if (tom_is_sort_E(((IntExpression)expression))) {
if (tom_is_fun_sym_select(((IntExpression)((IntExpression)expression)))) {

EList<Instruction> instrs = new BasicEList();
instrs.add(convertListOfConstraintSystem(
tom_get_slot_select_condition(((IntExpression)expression))));
instrs.add(convertExpression(
tom_get_slot_select_then(((IntExpression)expression))));
instrs.add(convertExpression(
tom_get_slot_select_else(((IntExpression)expression))));
return debug("    ", 
tom_make_generic("mux",instrs));

}
}
}
}
}

throw new RuntimeException("Conversion for "+expression+"not implemented yet!");
}

private Symbol getSymbol(Variable var) {
Symbol varSymbol = scope.lookup(var.getName());
if(varSymbol == null)
throw new RuntimeException("Symbol "+var.getName()+" was not found in scope "+scope);
return varSymbol;
}

public Instruction convertIntTerm(IntTerm t) {
debug("      Converting IntTerm to Instruction: "+t, null);

{
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_term(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
 long  tom_coef=tom_get_slot_term_coef(((org.polymodel.algebra.IntTerm)t));
org.polymodel.algebra.Variable tom_var=tom_get_slot_term_variable(((org.polymodel.algebra.IntTerm)t));

if(
tom_var== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", 
tom_make_symref(getSymbol(tom_var))); //TODO choose the Type carefully
return debug("      ", 
tom_make_generic("mul",tom_cons_array_InstL(tom_make_symref(getSymbol(tom_var)),tom_cons_array_InstL(tom_make_ival(tom_coef),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_mul(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_mul_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_mul_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("mul",tom_cons_array_InstL(convertExpression(tom_exp),tom_cons_array_InstL(tom_make_ival(tom_coef),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_div(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_div_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_div_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("div",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_mod(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_mod_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_mod_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("mod",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_ceil(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_ceil_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_ceil_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("ceil",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_floor(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_floor_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_floor_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("floor",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_nmul(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_nmul_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_nmul_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("mul",tom_cons_array_InstL(convertExpression(tom_exp),tom_cons_array_InstL(tom_make_ival(tom_coef),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_ndiv(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_ndiv_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_ndiv_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("div",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_nmod(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_nmod_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_nmod_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("mmod",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_nceil(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_nceil_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_nceil_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("ceil",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
{
if (tom_is_sort_T(t)) {
if (tom_is_sort_T(((org.polymodel.algebra.IntTerm)t))) {
if (tom_is_fun_sym_nfloor(((org.polymodel.algebra.IntTerm)((org.polymodel.algebra.IntTerm)t)))) {
IntExpression tom_exp=tom_get_slot_nfloor_expression(((org.polymodel.algebra.IntTerm)t));
 long  tom_coef=tom_get_slot_nfloor_coef(((org.polymodel.algebra.IntTerm)t));

if(
tom_exp== null)
return debug("      ", 
tom_make_ival(tom_coef));
if(
tom_coef== 1)
return debug("      ", convertExpression(
tom_exp)); 
return debug("      ",
tom_make_generic("floor",tom_cons_array_InstL(tom_make_ival(tom_coef),tom_cons_array_InstL(convertExpression(tom_exp),tom_empty_array_InstL(2)))));

}
}
}
}
}

throw new RuntimeException("not implemented yet!");  
}
}
