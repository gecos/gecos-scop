package fr.irisa.cairn.model.gecos.scop.remove;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.*;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.*;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.*;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlockStatement;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter;
import fr.irisa.cairn.gecos.model.scop.util.ScopSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Type;

/**
 *
 * @author aelmouss
 *
 */
public class RemoveScop extends ScopSwitch<EObject> {
	private GecosScopBlock Scop;
	private Scope scope;
	private static int doneID;
	
	private static final boolean DEBUG = false;
	private static String prefix;
	
	
	public RemoveScop(Scope scope) {
		this.scope = scope;
		
		debug("Scope: "+scope);
		if(DEBUG) prefix = "";
	}

	private void debug(String string) {
		if(DEBUG) System.out.println(prefix + string);
	}
	
	public Block compute(GecosScopBlock scop) {
		this.Scop = scop;
		
		debug("**********************************\n*  Removing Scop:\n**********************************\n"+ Scop);
		return (Block) doSwitch(Scop);
	}
	
	@Override
	public Block caseScopBlock(ScopBlock object) {
		debug("Root: ");

		List<Block> lBlk = new ArrayList<>();
		EList<ScopNode> statements = object.getChildren();
		for(ScopNode node : statements) {
			if(DEBUG) prefix += "  ";
			lBlk.add((Block) doSwitch(node));
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
		
		CompositeBlock res = GecosUserBlockFactory.CompositeBlock(lBlk);
		if ( object.getAnnotations() != null )
			res.getAnnotations().addAll(object.getAnnotations());
		debug("res: "+res);
		return res;
	}

	@Override
	public Block caseScopForLoop(ScopForLoop object) {
		debug("FOR: ");
		if(DEBUG) prefix += "  "; 

			Symbol iterator = getSymbol(object.getIterator());//TODO carefully choose the Type. where shall the symbol be?	

		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		
		IntExpression stride = object.getStride();
		if (stride.isConstant() != FuzzyBoolean.YES) {
			throw new RuntimeException("Non constant stride " + stride + " unsupported in RemoveScop");
		}
		
		boolean increment = stride.toAffine().getConstantTerm().getCoef() > 0;
		IntExpression init = increment ? object.getLB() : object.getUB();
		IntExpression test = increment ? object.getUB() : object.getLB();
		
		Instruction initInst = polyConvertor.convertExpression(init);
		initInst.setType(iterator.getType()); //XXX make sure value won't be casted when creating setInstruction 
		BasicBlock initBlk = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(iterator, initInst)); 
		
		Instruction testInst = polyConvertor.convertExpression(test);
		BasicBlock testBlk = increment ? GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.le(iterator, testInst))
				                       : GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.ge(GecosUserInstructionFactory.symbref(iterator), testInst));
		
		Instruction setInst = GecosUserInstructionFactory.add(GecosUserInstructionFactory.symbref(iterator), polyConvertor.convertExpression(object.getStride()));
		setInst.setType(iterator.getType()); //XXX make sure value won't be casted when creating setInstruction 
		BasicBlock stepBlk = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(iterator, setInst));
		
		Block bodyBlk = (Block) doSwitch(object.getBody());
		
		//wrap the body in a compositeblock. 
		if (!(bodyBlk instanceof CompositeBlock))
			bodyBlk = GecosUserBlockFactory.CompositeBlock(bodyBlk);
		
		if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		
		ForBlock res = GecosUserBlockFactory.For(initBlk, testBlk, stepBlk, bodyBlk);
		if  ( object.getAnnotations() != null ) {
			res.getAnnotations().addAll(object.getAnnotations());
		}
		debug("res: "+res);
		return res;
	}
	
	private Symbol getSymbol(Variable var) {
		Symbol varSymbol = scope.lookup(var.getName());
		if(varSymbol == null)
			throw new RuntimeException("Symbol "+var.getName()+" was not found in scope "+scope);
		return varSymbol;
	}

	@Override
	public Block caseScopGuard(ScopGuard object) {
		debug("IF: ");
		
//		PragmaAnnotation pragmas = object.getPragma();
//		if(pragmas!=null) doSwitch(pragmas); //TODO
		
		EList<IntConstraintSystem> constraintSystems = object.getPredicate();
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		BasicBlock condBlk = GecosUserBlockFactory.BBlock(polyConvertor.convertListOfConstraintSystem(constraintSystems));
		
		ScopNode then = object.getThenNode();
		ScopNode elsenode = object.getElseNode();
		
		Block thenBlk = null;
		if(then!=null) {
			debug("then: ");
			if(DEBUG) prefix += "  ";
			thenBlk = (Block) doSwitch(then);
			
			//wrap the branches in a compositeblock. 
			if (!(thenBlk instanceof CompositeBlock))
				thenBlk = GecosUserBlockFactory.CompositeBlock(thenBlk);
			
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
		else 
			throw new RuntimeException("Guard with empty then block: "+object);
		
		Block elseBlk = null;
		if(elsenode!=null) {
			debug("else: ");
			if(DEBUG)prefix += "  ";
			elseBlk = (Block) doSwitch(elsenode);

			//wrap the branches in a compositeblock. 
			if (!(elseBlk instanceof CompositeBlock))
				elseBlk = GecosUserBlockFactory.CompositeBlock(elseBlk);
			
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
		
		IfBlock res = null;
		if(elseBlk != null)
			res = GecosUserBlockFactory.IfThenElse(condBlk, thenBlk, elseBlk);
		else
			res = GecosUserBlockFactory.IfThen(condBlk, thenBlk);
		
		if  ( object.getAnnotations() != null ) {
			res.getAnnotations().addAll(object.getAnnotations());
		}
		
		debug("res: "+res);
		return res;
	}

	@Override
	public Block caseGecosScopBlock(GecosScopBlock gecosScopBlock) {
		debug("BLOCK :");
		ProcedureSet ps =  gecosScopBlock.getContainingProcedure().getContainingProcedureSet();
		GecosUserTypeFactory.setScope(ps.getScope());
		//push scope
		Scope previousScope = this.scope;
		this.scope = gecosScopBlock.getScope();
		List<Block> lblk = new ArrayList<Block>();
		List<ScopNode> nodes = new ArrayList<>();
		if(gecosScopBlock.getRoot()!=null) {
			nodes.add(gecosScopBlock.getRoot());
			
			for(ScopNode n : nodes) {
				if(DEBUG) prefix += "  ";
				lblk.add((Block) doSwitch(n));
				if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
			}
		}
		
		CompositeBlock cb = GecosUserBlockFactory.CompositeBlock(lblk);
		cb.mergeChildren();
		Block res =cb;

		EList<Block> children = cb.getChildren();
		if((children.size() == 1) && (children.get(0) != null) && (children.get(0) instanceof BasicBlock)) {
			BasicBlock child = (BasicBlock) children.get(0);
			res = child;
		}
		
		///move symbols
		if (gecosScopBlock.getScope()!=null) {
			EList<Symbol> symbols = gecosScopBlock.getScope().getSymbols();
			for (Symbol s: symbols) {
				if(s.getValue()!=null) {
					Instruction instr = (Instruction)doSwitch(s.getValue());
					s.setValue(instr);
				}
			}
			Scope resScope = res.getScope();
			if (resScope==null && symbols.size()!=0) {
				throw new UnsupportedOperationException("Not yet implemented");
			}
			resScope.getSymbols().addAll(symbols);
		}
		
		//copy annotations
		if  ( gecosScopBlock.getAnnotations() != null )
			res.getAnnotations().addAll(gecosScopBlock.getAnnotations());
		
		//pop scope
		this.scope = previousScope;
		
		debug("res: "+res);
		return res;
	}
	
	@Override
	public Block caseScopUnexpandedStatement(ScopUnexpandedStatement object) {
		throw new UnsupportedOperationException("Unexpanded nodes are not supported in RemoveScop");
	}
	
	@Override
	public Block caseScopBlockStatement(ScopBlockStatement object) {
		throw new UnsupportedOperationException("ScopBlockStatement not supported"+ScopPrettyPrinter.print(object)+" is not an instruction");
		
	}
	@Override
	public Block caseScopInstructionStatement(ScopInstructionStatement object) {
		debug("INST :"+object);

		EList<Instruction> linst = new BasicEList<Instruction>();
		for(Instruction i : object.getChildren()) {
			if(DEBUG) prefix += "  ";
			
			EObject res = doSwitch(i);
			if (res instanceof Instruction) {
				linst.add((Instruction) res);
			} else {
				throw new RuntimeException("ScopStatement "+ScopPrettyPrinter.print(object)+" is not an instruction");
			}
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
			
		BasicBlock res = GecosUserBlockFactory.BBlock(linst);
//		if(object.getAnnotations().containsKey(RegisterTiler.RLT_INSET_ANNOTATION)) {
////			GecosUserAnnotationFactory.pragma(res, RegisterTiler.RLT_INSET_ANNOTATION);
////			debug(res + "has been annotated as \"RegisterTiler.RLT_INSET_ANNOTATION\"");
//		}
		if ( object.getAnnotations() != null )
			res.getAnnotations().addAll(object.getAnnotations());
		debug("res: "+res);
		return res;
	}
	
	
//	public Block processScopAssignment(ScopAssignment obj) {
//		debug("ScopAssignment");
//		Instruction sym = GecosUserInstructionFactory.symbref(getSymbol(obj
//				.getLHS()));
//		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
//		Instruction value = polyConvertor.convertExpression(obj.getRHS());
//		Instruction inst = GecosUserInstructionFactory.set(sym, value);
//
//		BasicBlock res = GecosUserBlockFactory.BBlock(inst);
//		debug("result: " + res);
//		return res;
//	 }
//	
	
	@Override
	public Instruction defaultCase(EObject object) {
		if (object instanceof Instruction) {
			if(DEBUG) prefix += "  ";
			Instruction res =new ScopInstructionConvertor().doSwitch(object);
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
			return res;
		} else {
			throw new UnsupportedOperationException(object.eClass().getName()+" Not supported in "+this.getClass().getSimpleName());
		}
	};
	
	
	public Instruction convertScopInst(Instruction object) {
		if(DEBUG) prefix += "  ";
		Instruction res = (Instruction) doSwitch(object);
		if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		return res;
	}
	
	@Override
	public Instruction caseScopRead(ScopRead object) {
		debug("ScopRead :"+object);
		
		EList<Instruction> linst = new BasicEList<Instruction>();
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		for(IntExpression exp : object.getIndexExpressions())
			linst.add(polyConvertor.convertExpression(exp));
		
		Instruction res;
		if(linst.size() == 0) {
			//it's actually  a scalar
			res = GecosUserInstructionFactory.symbref(object.getSym());
		}
		else
			res = GecosUserInstructionFactory.array(object.getSym(), linst.toArray(new Instruction[1]));
		
		debug("res: "+res);
		return res;
	}

	@Override
	public Instruction caseScopWrite(ScopWrite object) {
		debug("ScopWrite :"+object);
		
		List<Instruction> linst = new ArrayList<>();
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		for(IntExpression exp : object.getIndexExpressions())
			linst.add(polyConvertor.convertExpression(exp));
		
		
		Instruction res;
		if(linst.size() == 0) {
			//it's actually  a scalar
			res = GecosUserInstructionFactory.symbref(object.getSym());
		}
		else
			res = GecosUserInstructionFactory.array(object.getSym(), linst.toArray(new Instruction[1]));
		
		debug("res: "+res);
		return res;
	}


	public Block buildNextTransition(ScopFSMTransition object, Symbol done, HashMap<Symbol,Symbol> map) {
		debug("ScopFSMTransition:"+object);
		EList<IntConstraintSystem> constraintSystems = object.getDomain();
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		BasicBlock condBlk = GecosUserBlockFactory.BBlock(polyConvertor.convertListOfConstraintSystem(constraintSystems));
		
		ScopFSMBlock fsm = (ScopFSMBlock) object.eContainer();
		EList<ScopDimension> iterators = fsm.getIterators();
		EList<IntExpression> exprs= object.getNextIteration();
		int offset = 0;

		BasicBlock bodyBlk = GecosUserBlockFactory.BBlock();
		for (IntExpression e : exprs) {
			Instruction rhs =polyConvertor.convertExpression(e);
			Instruction lhs =symbref(map.get(iterators.get(offset).getSymbol()));
			Instruction assign = set(lhs,rhs);
			bodyBlk.getInstructions().add(assign);
			offset++;

		}
		Instruction assign = set(done,Int(1l));
		bodyBlk.getInstructions().add(assign);
		return GecosUserBlockFactory.IfThen(condBlk, bodyBlk);
	}

	@Override
	public Block caseScopFSMBlock(ScopFSMBlock object) {
		debug("ScopFSMBlock:"+object);
		CompositeBlock res = GecosUserBlockFactory.CompositeBlock();
		res.setScope(GecosUserCoreFactory.scope());
		GecosUserTypeFactory.setScope(res.getScope());
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		Type intType = INT();
		Symbol doneSym=  GecosUserCoreFactory.symbol("done_"+doneID++,intType);
		res.getScope().addSymbol(doneSym);

		HashMap<Symbol,Symbol> nextSymMap = new HashMap<Symbol,Symbol>();
		for (ScopDimension d : object.getIterators()) {
			Symbol nextIteratorSym=  d.getSymbol().copy();
			nextIteratorSym.setName("_"+d.getSymbolName());
			nextSymMap.put(d.getSymbol(), nextIteratorSym);
			res.getScope().addSymbol(nextIteratorSym);
		}

		for (ScopFSMTransition t: object.getStart()) {
			if (t.getDomain().size()==0) {
				BasicBlock initBB = BBlock();
				int offset = 0;
				for (IntExpression e: t.getNextIteration()) {
					Instruction rhs=  (Instruction) polyConvertor.convertExpression(e);
					Instruction lhs = symbref(object.getIterators().get(offset).getSymbol());
					initBB.getInstructions().add(set(lhs,rhs));
					offset++;
				}
				res.getChildren().add(initBB);
			} else {
				throw new UnsupportedOperationException("Not Yet Implemented");
			}
		}
		res.getChildren().add(GecosUserBlockFactory.BBlock(set(symbref(doneSym),Int(1l))));

		CompositeBlock whileBody  = CompositeBlock();
		Instruction cond = condBranch(eq(symbref(doneSym),Int(1)), "whileLoop");
		WhileBlock _while = GecosUserBlockFactory.While(cond, whileBody);
		res.getChildren().add(_while);

		/*
		 * done=0 update i=i_next
		 */
		
		whileBody.getChildren().add(GecosUserBlockFactory.BBlock(set(symbref(doneSym),Int(0l))));

		/*
		 * Commands (statements)
		 */
		EList<ScopNode> statements = object.getCommands();
		for(ScopNode node : statements) {
			if(DEBUG) prefix += "  ";
			whileBody.getChildren().add((Block) doSwitch(node));
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
		
		/*
		 * Next state guards
		 */
		for(ScopFSMTransition node : object.getNext()) {
			if(DEBUG) prefix += "  ";
			whileBody.getChildren().add(buildNextTransition(node, doneSym, nextSymMap));
			if(DEBUG) prefix = prefix.substring(0, prefix.length()-2);
		}
		
		/*
		 * State update i=i_next
		 */
		BasicBlock bb = GecosUserBlockFactory.BBlock();
		for(Symbol sym : nextSymMap.keySet()) {
			Instruction assign = set(symbref(sym),symbref(nextSymMap.get(sym)));
			bb.getInstructions().add(assign);
		}
		whileBody.getChildren().add(bb);

		if ( object.getAnnotations() != null )
			res.getAnnotations().addAll(object.getAnnotations());
		debug("res: "+res);
		return res;
	}

	@Override
	public Instruction caseScopIntExpression(ScopIntExpression object) {
		debug("ScopIntExpr :"+object);
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		Instruction res = polyConvertor.convertExpression(object.getExpr());
		debug("res: "+res);
		return res;
	}

	@Override
	public Instruction caseScopIntConstraintSystem(ScopIntConstraintSystem object) {
		debug("ScopIntConstraintSys :"+object);
		PolymodelConvertor polyConvertor = new PolymodelConvertor(scope);
		Instruction res = polyConvertor.convertConstraintSystem(object.getSystem());
		debug("res: "+res);
		return res;
	}
	
	public class ScopInstructionConvertor extends DefaultInstructionSwitch<Instruction> {
		@Override
		public Instruction caseSetInstruction(SetInstruction object) {
			debug("SetInstruction: "+object);	
			
			Instruction symbol = convertScopInst(object.getDest());
			Instruction value = convertScopInst(object.getSource());
			value.setType(symbol.getType()); //XXX make sure value won't be casted when creating setInstruction
			Instruction res = GecosUserInstructionFactory.set(symbol, value);
			debug("res: "+res);
			return res;
		}	
		
		@Override
		public Instruction caseGenericInstruction(GenericInstruction object) {
			debug("GenericInstruction: "+object);
			List<Instruction> linst = new ArrayList<>();
			for(Instruction i : object.getChildren())
				linst.add(convertScopInst(i));		
			Instruction res = GecosUserInstructionFactory.generic(object.getName(), object.getType(), linst.toArray(new Instruction[1]));
			debug("res: "+res);
			return res;
		}
		
		@Override
		public Instruction caseCallInstruction(CallInstruction object) {
			debug("CallInstruction: "+object);
			List<Instruction> linst = new ArrayList<>();
			for(Instruction i : object.getArgs())
				linst.add(convertScopInst(i));		
			CallInstruction res = GecosUserInstructionFactory.call(convertScopInst(object.getAddress()),linst.toArray(new Instruction[linst.size()]));
			debug("res: "+res);
			return res;
		}
		
		@Override
		public Instruction caseAddressInstruction(AddressInstruction object) {
			debug("AddressInstruction: "+object);
			Instruction res = GecosUserInstructionFactory.address(convertScopInst(object.getAddress()));
			return res;
		}
		
		@Override
		public Instruction caseIndirInstruction(IndirInstruction object) {
			debug("IndirInstruction: "+object);
			Instruction res = GecosUserInstructionFactory.indir(convertScopInst(object.getAddress()));
			return res;
		}
		
		
		@Override
		public Instruction caseFloatInstruction(FloatInstruction object) {
			debug("FloatInstruction: "+object);
			return object;
		}
		
		@Override
		public Instruction caseIntInstruction(IntInstruction object) {
			debug("IntInstruction: "+object);
			return object;
		}
		
		@Override
		public Instruction caseConvertInstruction(ConvertInstruction object) {
			debug("ConvertInstruction: " + object);
			Instruction exp = convertScopInst(object.getExpr());
			
			Instruction res = GecosUserInstructionFactory.cast(object.getType(), exp);
			debug("res: " + res);
			return res;
		}

			
		@Override
		public Instruction caseInstruction(Instruction object) {
			return object.copy();
		}
		
		@Override
		public Instruction caseSymbolInstruction(SymbolInstruction object) {
			debug("SymbolInstruction: " + object);
			return object;
		}
		
		
	}	
}
