package fr.irisa.cairn.model.gecos.scop.remove;

import java.util.List;
import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopIndexExpression;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeCompositeBlocks;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.SymbolDefinitionAnnotation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.Type;

/**
 *
 * @author aelmouss
 *
 */
public class RemoveScopModule {
	
	private EObject object;
	private List<GecosScopBlock> scopRoots;
	
	public RemoveScopModule(GecosProject proj) {
		this.object = proj;
	}
	
	public RemoveScopModule(ProcedureSet ps) {
		this.object = ps;
	}
	
	public RemoveScopModule(Procedure proc) {
		this.object = proc;
	}

	public void compute() {
		scopRoots = EMFUtils.eAllContentsInstancesOf(this.object, GecosScopBlock.class);
	
		for(GecosScopBlock scop : this.scopRoots) {
			remove(scop);
		}
		
		/* Convert types */
		for (Scope s: EMFUtils.eAllContentsInstancesOf(object, Scope.class))
			convertTypes(s);

		new ClearControlFlow((GecosProject) object).compute();
		new BuildControlFlow((GecosProject) object).compute();
		new MergeCompositeBlocks((GecosProject) object).compute();
	//	new SimpleLoop(object).compute();
	//	new ReplaceLoopByMemcpy((GecosProject)object).compute();
	}

	public static void remove(GecosScopBlock scop) {
		try{
			Block parentBlk = scop.getParent();
			
			/* convert scop to CDFG */
			Block res = new RemoveScop(scop.getScope()).compute(scop);
			
			//XXX redundant with RemoveScop pass?
			if ( scop.getAnnotations() != null )
				res.getAnnotations().addAll(scop.getAnnotations());
			
			/* Replace scop with new Block */
			parentBlk.replace(scop, res);
			
			/* move symbols and types to main scope */
			Scope scope = res.getScope();
			if (scop.getScope()!=null) {
				List<Symbol> symList = scop.getScope().getSymbols();
				for ( Symbol sym : symList ) {
					scope.makeUnique(sym);
				}
				scope.getSymbols().addAll(symList);
				scope.getTypes().addAll(scop.getScope().getTypes());
			}
			
			
//				/* Convert types */
			convertTypes(scope);
			
		} catch(Exception e) {
			throw new RuntimeException("RemoveScop Failed for scop: "+scop,e);
		}
	}

	private static void convertTypes(Scope scopScope) {
		//XXX no longer necessary; covered below ?
		for(Type t : scopScope.getTypes()) {
			while(t instanceof ArrayType) {
				ArrayType tmp = (ArrayType)t;
				if(tmp.getSizeExpr() instanceof ScopIntExpression) {
					ScopIntExpression expr = (ScopIntExpression) tmp.getSizeExpr();
					tmp.setSizeExpr(new PolymodelConvertor(scopScope).convertExpression(expr.getExpr()));
				}
				t = tmp.getBase();
			}
		}
		
		EMFUtils.eAllContentsFirstInstancesOf(scopScope, ScopIntExpression.class).stream()
			.filter(Objects::nonNull)
			.forEach(n -> EcoreUtil.replace(n, new RemoveScop(scopScope).doSwitch(n)));
		
		EMFUtils.eAllContentsFirstInstancesOf(scopScope, ScopIndexExpression.class).stream()
			.filter(Objects::nonNull)
			.forEach(n -> EcoreUtil.replace(n, new RemoveScop(scopScope).doSwitch(n)));
	}
	
}
