package fr.irisa.cairn.model.gecos.scop.remove

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory
import gecos.instrs.AddressInstruction
import gecos.instrs.CallInstruction
import gecos.instrs.ConvertInstruction
import gecos.instrs.FloatInstruction
import gecos.instrs.GenericInstruction
import gecos.instrs.IndirInstruction
import gecos.instrs.Instruction
import gecos.instrs.IntInstruction
import gecos.instrs.SetInstruction
import gecos.instrs.SymbolInstruction
import java.util.ArrayList
import java.util.List

class ScopInstructionConvertor  {
	
	
	def dispatch convert(SetInstruction object) {
		debug('''SetInstruction: «object»'''.toString)
		var Instruction symbol = convertScopInst(object.getDest())
		var Instruction value = convertScopInst(object.getSource())
		value.setType(symbol.getType())
		// XXX make sure value won't be casted when creating setInstruction
		var Instruction res = GecosUserInstructionFactory::set(symbol, value)
		debug('''res: «res»'''.toString)
		return res
	}

	def private Instruction convertScopInst(Instruction dest) {
		// TODO Auto-generated method stub
		return null
	}

	def private void debug(String string) { // TODO Auto-generated method stub
	}

	def dispatch convert(GenericInstruction object) {
		debug('''GenericInstruction: «object»'''.toString)
		var List<Instruction> linst = new ArrayList()
		for (Instruction i : object.getChildren())
			linst.add(convertScopInst(i))
		var Instruction res = GecosUserInstructionFactory::generic(object.getName(), object.getType(),
			linst.toArray(newArrayOfSize(1)))
		debug('''res: «res»'''.toString)
		return res
	}

	def dispatch convert(CallInstruction object) {
		debug('''CallInstruction: «object»'''.toString)
		var List<Instruction> linst = new ArrayList()
		for (Instruction i : object.getArgs())
			linst.add(convertScopInst(i))
		var CallInstruction res = GecosUserInstructionFactory::call(convertScopInst(object.getAddress()),
			linst.toArray(newArrayOfSize(linst.size())))
		debug('''res: «res»'''.toString)
		return res
	}

	def dispatch convert(AddressInstruction object) {
		debug('''AddressInstruction: «object»'''.toString)
		var Instruction res = GecosUserInstructionFactory::address(convertScopInst(object.getAddress()))
		return res
	}

	def dispatch convert(IndirInstruction object) {
		debug('''IndirInstruction: «object»'''.toString)
		var Instruction res = GecosUserInstructionFactory::indir(convertScopInst(object.getAddress()))
		return res
	}

	def dispatch convert(FloatInstruction object) {
		debug('''FloatInstruction: «object»'''.toString)
		return object
	}

	def dispatch convert(IntInstruction object) {
		debug('''IntInstruction: «object»'''.toString)
		return object
	}

	def dispatch convert(ConvertInstruction object) {
		debug('''ConvertInstruction: «object»'''.toString)
		var Instruction exp = convertScopInst(object.getExpr())
		var Instruction res = GecosUserInstructionFactory::cast(object.getType(), exp)
		debug('''res: «res»'''.toString)
		return res
	}

	def dispatch convert(Instruction object) {
		throw new RuntimeException('''not yet implemented for type «object.getClass()»'''.toString)
	}

	def dispatch convert(SymbolInstruction object) {
		debug('''SymbolInstruction: «object»'''.toString)
		return object
	}
}
