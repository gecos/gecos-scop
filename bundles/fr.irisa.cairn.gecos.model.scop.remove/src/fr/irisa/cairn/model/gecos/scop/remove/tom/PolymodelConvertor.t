package fr.irisa.cairn.model.gecos.scop.remove;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.CompositeOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import gecos.blocks.Block;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
 * Helper class for "RemoveScop" allowing the conversion of some polyhedral 
 * expressions to a Gecos Instruction.
 *
 * -"scope" must contains all the symbols which names are involved in the 
 * polymodel expressions to be converted (typicaly it can be the scope of 
 * the GScopRoot object containing the input)
 *
 * N.B: no new Symbols are created 
 *
 * @author aelmouss
 *
 */
@SuppressWarnings("all")
public class PolymodelConvertor{ 

	%include { sl.tom }  
	%include { List.tom }
	
	%include { algebra_common.tom }
	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }
	%include { algebra_quasiaffine.tom }
	%include { algebra_all_ops.tom }
	%include { algebra_quasiaffine.tom }
	%include { algebra_domains.tom }   
	%include { gecos_terminals.tom } 
 
	%include { gecos_basic.tom }

	private Scope scope;
	private static boolean DEBUG = false; 


	public PolymodelConvertor(Scope scope) {
		this.scope = scope;
	}

	private static Instruction debug(String mess, Instruction out) {
		if(DEBUG) {
			System.out.println(mess+(out==null?"":out));
		} 
		return out;
	}

	public Instruction convertListOfConstraintSystem(List<IntConstraintSystem> consSys) {
		debug("Converting List of IntConstraintSystem to Instruction: "+consSys, null);
		EList<Instruction> instrs = new BasicEList();
		for(IntConstraintSystem c : consSys) 
			instrs.add(convertConstraintSystem(c));
		
		if(instrs.size() == 1)
			return debug("", instrs.get(0));
		return debug("", `generic("lor", instrs)); 
	}

	public Instruction convertConstraintSystem(IntConstraintSystem consSys) {
		debug("Converting IntConstraintSystem to Instruction: "+consSys, null);
		EList<Instruction> instrs = new BasicEList();
		for(IntConstraint c : consSys.getConstraints()) 
			instrs.add(convertConstraint(c));
		
		if(instrs.size() == 1)
			return debug("", instrs.get(0));
		return debug("", `generic("land", instrs)); 
	}
 
	public Instruction convertConstraint(IntConstraint cons) {
		debug("  Converting IntConstraint to Instruction: "+cons, null);
		String op;
		switch(cons.getComparisonOperator()) {
			case EQ:
				op = ComparisonOperator.EQ.getLiteral();
				break;
			case NE:
				op = ComparisonOperator.NE.getLiteral();
				break;
			case GE:
				op = ComparisonOperator.GE.getLiteral();
				break;
			case GT:
				op = ComparisonOperator.GT.getLiteral();
				break;
			case LE:
				op = ComparisonOperator.LE.getLiteral();
				break;
			case LT:
				op = ComparisonOperator.LT.getLiteral();
				break;
			default:
				op = "unknown";
				break;
		}

		return debug("  ", `generic(op, InstL(convertExpression(cons.getLhs()), convertExpression(cons.getRhs()))));//TODO be carfull about the order of operands
	}


	public Instruction createGenericAdd(EList<Instruction> children) {
		if(children.size() == 1)
			return debug("    ", children.get(0));
		Instruction res = children.get(0);
		for(int i=1; i<children.size(); i++) {
			Instruction child = children.get(i);
			if (child instanceof IntInstruction) {
				IntInstruction ii = (IntInstruction) child;
				if (ii.getValue() < 0) {
					ii.setValue(-ii.getValue());
					res = GecosUserInstructionFactory.sub(res, child);
					continue;
				}
			} else if (child instanceof GenericInstruction) {
				GenericInstruction gi = (GenericInstruction) child;
				if (gi.getName().equals(ArithmeticOperator.NEG.getLiteral())) {
					res = GecosUserInstructionFactory.sub(res, gi.getOperand(0));
					continue;
				} else if (gi.getName().equals(ArithmeticOperator.MUL.getLiteral())) {
					if (gi.getOperand(0) instanceof IntInstruction) {
						IntInstruction ii = (IntInstruction) gi.getOperand(0);
						if (ii.getValue() < 0) {
							ii.setValue(-ii.getValue());
							if (ii.getValue() == 1) 
								child = gi.getOperand(1);
							res = GecosUserInstructionFactory.sub(res, child);
							continue;
						}
					}
				} else if (gi.getName().equals(ArithmeticOperator.MOD.getLiteral())
						|| gi.getName().equals(ArithmeticOperator.DIV.getLiteral())) {
					if (gi.getOperand(1) instanceof IntInstruction) {
						IntInstruction ii = (IntInstruction) gi.getOperand(1);
						if (ii.getValue() < 0) {
							ii.setValue(-ii.getValue());
							res = GecosUserInstructionFactory.sub(res, child);
							continue;
						}
					}
				}
			}
			res = GecosUserInstructionFactory.add(res, child);
		}
		return debug("    ", res);
	}

	public Instruction convertExpression(IntExpression expression) {
		debug("    Converting IntExpression to Instruction: "+expression, null);
		%match (expression) {
			affine(trms) -> {	
				EList<Instruction> instrs = new BasicEList();
				for(IntTerm t : `trms) 
					instrs.add(convertIntTerm(t));
				
				return createGenericAdd(instrs); 
			}
			
			qaffine(trms) -> {
				EList<Instruction> instrs = new BasicEList();
				for(IntTerm t : `trms) 
					instrs.add(convertIntTerm(t));
				
				return createGenericAdd(instrs); 
			}
		
			in@composite(op,a,b) -> {
				switch(((CompositeOperator)`op)) {
					case MOD:
						{ return `generic("mod", InstL(convertExpression(a),convertExpression(b))); } 
					case DIV:
						{ return `generic("div", InstL(convertExpression(a),convertExpression(b))); } 
					case CEIL:
						{ return `generic("ceil", InstL(convertExpression(a),convertExpression(b))); } 
					case FLOOR:
						{ return `generic("floor", InstL(convertExpression(a),convertExpression(b))); } 
					default:
						throw new RuntimeException("Unhandled operator in : " + `in);
					} 

			}

			sum(expl) -> {
				EList<Instruction> instrs = new BasicEList();
				for(IntExpression e : `expl)
					instrs.add(convertExpression(e));
					
				return createGenericAdd(instrs);
			}
			prod(expl) -> {
				EList<Instruction> instrs = new BasicEList();
				for(IntExpression e : `expl)
					instrs.add(convertExpression(e));
					
				if(instrs.size() == 1)
					return debug("    ", instrs.get(0));	
				return debug("    ", `generic("mul", instrs));
			}
			min(expl) -> {
				EList<Instruction> instrs = new BasicEList();
				for(IntExpression e : `expl)
					instrs.add(convertExpression(e));
					
				if(instrs.size() == 1)
					return debug("    ", instrs.get(0));	
				return debug("    ", `generic("min", instrs));
			}
			max(expl) -> {
				EList<Instruction> instrs = new BasicEList();
				for(IntExpression e : `expl)
					instrs.add(convertExpression(e));
				
				if(instrs.size() == 1)
					return debug("    ", instrs.get(0));	
				return debug("    ", `generic("max", instrs));
			}
			
			polynomial(_) -> {
				throw new RuntimeException("polynomial not yet implemented");
			}
			select(cond,t,e) -> {
				EList<Instruction> instrs = new BasicEList();
				instrs.add(convertListOfConstraintSystem(`cond));
				instrs.add(convertExpression(`t));
				instrs.add(convertExpression(`e));
				return debug("    ", `generic("mux", instrs));
			}
			
		}
		throw new RuntimeException("Conversion for "+expression+"not implemented yet!");
	}

	private Symbol getSymbol(Variable var) {
		Symbol varSymbol = scope.lookup(var.getName());
		if(varSymbol == null)
			throw new RuntimeException("Symbol "+var.getName()+" was not found in scope "+scope);
		return varSymbol;
	}

	public Instruction convertIntTerm(IntTerm t) {
		debug("      Converting IntTerm to Instruction: "+t, null);
		%match(t) {
			term(coef, var) -> {
				if(`var == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", `symref(getSymbol(var))); //TODO choose the Type carefully
				return debug("      ", `generic("mul", InstL(ival(coef), symref(getSymbol(var)))));
			} 
			
			mul(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("mul", InstL(ival(coef), convertExpression(exp))));
			}
			div(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("div", InstL(convertExpression(exp), ival(coef))));
			}
			mod(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("mod", InstL(convertExpression(exp), ival(coef))));
			}
			ceil(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("ceil", InstL(convertExpression(exp), ival(coef))));
			}
			floor(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("floor", InstL(convertExpression(exp), ival(coef))));
			}
			
			nmul(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("mul", InstL(ival(coef), convertExpression(exp))));
			}
			ndiv(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("div", InstL(convertExpression(exp), ival(coef))));
			}
			nmod(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("mmod", InstL(convertExpression(exp), ival(coef))));
			}
			nceil(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("ceil", InstL(convertExpression(exp), ival(coef))));
			}
			nfloor(exp, coef) -> {
				if(`exp == null)
					return debug("      ", `ival(coef));
				if(`coef == 1)
					return debug("      ", convertExpression(`exp)); 
				return debug("      ",`generic("floor", InstL(convertExpression(exp), ival(coef))));
			} 
			
//			pterm(num, den, pvars) -> {
//				throw new RuntimeException("pterm not implemented yet !");
//			}  
		} 
		throw new RuntimeException("not implemented yet!");  
	}
}