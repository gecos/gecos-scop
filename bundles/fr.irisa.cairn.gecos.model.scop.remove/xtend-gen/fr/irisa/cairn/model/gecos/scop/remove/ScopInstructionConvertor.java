package fr.irisa.cairn.model.gecos.scop.remove;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.instrs.AddressInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class ScopInstructionConvertor {
  protected Instruction _convert(final SetInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("SetInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    Instruction symbol = this.convertScopInst(object.getDest());
    Instruction value = this.convertScopInst(object.getSource());
    value.setType(symbol.getType());
    Instruction res = GecosUserInstructionFactory.set(symbol, value);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("res: ");
    _builder_1.append(res);
    this.debug(_builder_1.toString());
    return res;
  }
  
  private Instruction convertScopInst(final Instruction dest) {
    return null;
  }
  
  private void debug(final String string) {
  }
  
  protected Instruction _convert(final GenericInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("GenericInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    List<Instruction> linst = new ArrayList<Instruction>();
    EList<Instruction> _children = object.getChildren();
    for (final Instruction i : _children) {
      linst.add(this.convertScopInst(i));
    }
    Instruction res = GecosUserInstructionFactory.generic(object.getName(), object.getType(), 
      linst.<Instruction>toArray(new Instruction[1]));
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("res: ");
    _builder_1.append(res);
    this.debug(_builder_1.toString());
    return res;
  }
  
  protected Instruction _convert(final CallInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("CallInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    List<Instruction> linst = new ArrayList<Instruction>();
    EList<Instruction> _args = object.getArgs();
    for (final Instruction i : _args) {
      linst.add(this.convertScopInst(i));
    }
    CallInstruction res = GecosUserInstructionFactory.call(this.convertScopInst(object.getAddress()), 
      linst.<Instruction>toArray(new Instruction[linst.size()]));
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("res: ");
    _builder_1.append(res);
    this.debug(_builder_1.toString());
    return res;
  }
  
  protected Instruction _convert(final AddressInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("AddressInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    Instruction res = GecosUserInstructionFactory.address(this.convertScopInst(object.getAddress()));
    return res;
  }
  
  protected Instruction _convert(final IndirInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("IndirInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    Instruction res = GecosUserInstructionFactory.indir(this.convertScopInst(object.getAddress()));
    return res;
  }
  
  protected Instruction _convert(final FloatInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("FloatInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    return object;
  }
  
  protected Instruction _convert(final IntInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("IntInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    return object;
  }
  
  protected Instruction _convert(final ConvertInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ConvertInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    Instruction exp = this.convertScopInst(object.getExpr());
    Instruction res = GecosUserInstructionFactory.cast(object.getType(), exp);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("res: ");
    _builder_1.append(res);
    this.debug(_builder_1.toString());
    return res;
  }
  
  protected Instruction _convert(final Instruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("not yet implemented for type ");
    Class<? extends Instruction> _class = object.getClass();
    _builder.append(_class);
    String _string = _builder.toString();
    throw new RuntimeException(_string);
  }
  
  protected Instruction _convert(final SymbolInstruction object) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("SymbolInstruction: ");
    _builder.append(object);
    this.debug(_builder.toString());
    return object;
  }
  
  public Instruction convert(final Instruction object) {
    if (object instanceof AddressInstruction) {
      return _convert((AddressInstruction)object);
    } else if (object instanceof CallInstruction) {
      return _convert((CallInstruction)object);
    } else if (object instanceof ConvertInstruction) {
      return _convert((ConvertInstruction)object);
    } else if (object instanceof IndirInstruction) {
      return _convert((IndirInstruction)object);
    } else if (object instanceof GenericInstruction) {
      return _convert((GenericInstruction)object);
    } else if (object instanceof SetInstruction) {
      return _convert((SetInstruction)object);
    } else if (object instanceof FloatInstruction) {
      return _convert((FloatInstruction)object);
    } else if (object instanceof IntInstruction) {
      return _convert((IntInstruction)object);
    } else if (object instanceof SymbolInstruction) {
      return _convert((SymbolInstruction)object);
    } else if (object != null) {
      return _convert(object);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(object).toString());
    }
  }
}
