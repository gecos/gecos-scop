extern output(void *t);
void* input();

#define M 512
#define H 256
void stencil1d() {
	int in[256];
	int tab[256][256];
	int out[256];
	int i,j;
	{
		for (i=0;i<M-1;i++) {
			tab[i][0]=in[i];
		}
		for (i=1;i<M-1;i++) {
			for (j=0;j<M;j++) {
				if (i<1 || i>=M-1)
					tab[i][j] =tab[i][j-1];
				if (i>=1 && i<M-1)
					tab[i][j]=0.5*(tab[i-1][j-1]+tab[i][j-1]+tab[i+1][j-1]);
			}
		}
		for (i=1;i<M-1;i++) {
			out[i][j]=tab[i][M-1];
		}
		output(out);
	}
}
