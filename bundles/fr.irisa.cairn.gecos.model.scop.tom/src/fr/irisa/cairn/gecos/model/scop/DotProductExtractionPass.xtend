package fr.irisa.cairn.gecos.model.scop

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.gecos.model.tools.queries.GecosQuery
import fr.irisa.r2d2.gecos.framework.GSModule

@GSModule(value="This is a test")
class DotProductExtractionPass extends DotProductExtraction {
	
	GecosProject p
	
	new(GecosProject p) {
		this.p=p;
	}
	
	def compute() {
		val bbs= GecosQuery.findAllBasicBlocksIn(p);
		for (bb : bbs) {
			for (i : bb.instructions) {
				val newInst = apply(i)
				println("before :"+i)
				println("after :"+newInst)
				
			}
		} 
	}
}
