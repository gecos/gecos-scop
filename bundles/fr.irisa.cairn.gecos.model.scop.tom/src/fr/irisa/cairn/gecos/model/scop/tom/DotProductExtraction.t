package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.*;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class DotProductExtraction extends GenericInstructionTransformation {  
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }
 	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }

	%include { algebra_terminals.tom }
  
	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction adapter = `InnermostId(SumOfProductExtraction()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
		return adapter;
	}

	public static GenericInstruction generic(String name, EList<Instruction> args) {
		return (GenericInstruction)(`generic(name,args));
	}

	public static boolean isGeneric(String name, Instruction i) {
		if (i instanceof GenericInstruction) {
			return ((GenericInstruction)i).getName().equals(name);
		}
		return false;
	}
	
	%op Inst vector(children : InstL) {
		is_fsym(t) {isGeneric("vector", $t)}
		get_slot(children,t) {enforce(((GenericInstruction)$t).getChildren())}
		make(_children) { generic("vector",$_children)}
	}

	%op Inst sum(children : InstL) {
		is_fsym(t) {isGeneric("sum", $t)}
		get_slot(children,t) {enforce(((GenericInstruction)$t).getChildren())}
		make(_children) { generic("sum",$_children)}
	}

	%op Inst sop(children : InstL) {
		is_fsym(t) {isGeneric("sop", $t)}
		get_slot(children,t) {enforce(((GenericInstruction)$t).getChildren())}
		make(_children) { generic("sop",$_children)}
	}
  
	%op Inst sread(sym : Sym, indexExpressions : EL) {
		is_fsym(t) {$t instanceof fr.irisa.cairn.gecos.model.scop.ScopRead}
		get_slot(sym,t) {((fr.irisa.cairn.gecos.model.scop.ScopRead)$t).getSym()}
		get_slot(indexExpressions,t) {enforce(((fr.irisa.cairn.gecos.model.scop.ScopRead)$t).getIndexExpressions())}
		make(_sym,_indexExpressions) {fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory.createRead($_sym, $_indexExpressions)}
	}  
	
	%strategy SumOfProductExtraction() extends Identity() {
		visit Inst {
				sum(InstL(a*,
						m0@mul(InstL(c0,r0@sread(sym,_))),
						b*,
						m1@mul(InstL(c1,r1@sread(sym,_))),
						c*)) -> {   
					return `sum(InstL(a*,b*,c*,
									sop(InstL(
											vector(InstL(c0,c1)),	
											vector(InstL(r0,r1)))
										)
									));	
				}
				sum(InstL(a*,
						m0@mul(InstL(c0,r0@sread(sym,_))),
						b*,
						s@sop(InstL(
								vector(InstL(w*)),
								vector(l@InstL(sread(sym,_),z*)))),
						c*)) -> { 
						
					return `sum(InstL(a*,b*,c*,
									sop(InstL(
											vector(InstL(c0,w*)),	
											vector(InstL(r0,l*)))
										)
									));	
				}
			}
	} 

}