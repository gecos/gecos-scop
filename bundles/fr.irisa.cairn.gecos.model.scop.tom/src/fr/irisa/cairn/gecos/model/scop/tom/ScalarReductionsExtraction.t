package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.*;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ScalarReductionsExtraction extends GenericInstructionTransformation {
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }
 	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }

	%include { algebra_terminals.tom }

	%strategy ScalarReductions() extends Identity() {
		visit Inst {
				add(InstL(add(InstL(x, y)), z)) -> { return `sum(InstL(x,y,z)); }
				add(InstL(x,add(InstL(y, z)))) -> {  return `sum(InstL(x,y,z)); }
				sum(InstL(x*,add(InstL(y, z)),w*)) -> { return `sum(InstL(x*,y,z,w*)); }
				sum(InstL(x*,sum(InstL(y*)),z*)) -> { return `sum(InstL(x*,y*,z*)); }
		}
	}


	%op Inst sum(children : InstL) {
		is_fsym(t) {isGeneric("sum", $t)}
		get_slot(children,t) {enforce(((GenericInstruction)$t).getChildren())}
		make(_children) { generic("sum",$_children)}
	}

	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction adapter = `InnermostId(ScalarReductions()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
		return adapter;
	}
 
 	public static GenericInstruction generic(String name, EList<Instruction> args) {
		return (GenericInstruction)(`generic(name,args));
	}

	public static boolean isGeneric(String name, Instruction i) {
		if (i instanceof GenericInstruction) {
			return ((GenericInstruction)i).getName().equals(name);
		}
		return false;
	}

	
}