package fr.irisa.cairn.gecos.model.scop;

import fr.irisa.cairn.gecos.model.scop.ScalarReductionsExtraction;
import fr.irisa.cairn.gecos.model.tools.queries.GecosQuery;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;

@GSModule(value = "This is a test")
@SuppressWarnings("all")
public class ScalarReductionsExtractionPass extends ScalarReductionsExtraction {
  private GecosProject p;
  
  public ScalarReductionsExtractionPass(final GecosProject p) {
    this.p = p;
  }
  
  public void compute() {
    try {
      final EList<BasicBlock> bbs = GecosQuery.findAllBasicBlocksIn(this.p);
      for (final BasicBlock bb : bbs) {
        EList<Instruction> _instructions = bb.getInstructions();
        for (final Instruction i : _instructions) {
          {
            final Instruction newInst = this.apply(i);
            InputOutput.<String>println(("before :" + i));
            InputOutput.<String>println(("after :" + newInst));
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
