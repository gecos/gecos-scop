debug(1);
p = CreateGecosProject("test");
AddSourceToGecosProject(p, "./src-c/slice1d.c");
CDTFrontend(p);
ScopExtractor(p);
RemoveScop(p);
CGenerator(p, "./src-regen-extract/");

p = CreateGecosProject("test");
AddSourceToGecosProject(p, "./src-c/slice1d.c");
CDTFrontend(p);
ScopExtractor(p);
ScopSlicer(p);
RemoveScop(p);
CGenerator(p, "./src-regen-slice/");

p = CreateGecosProject("test");
AddSourceToGecosProject(p, "./src-c/slice1d.c");
CDTFrontend(p);
ScopExtractor(p);
ScopSlicer(p);
ScopISLCodeGen(p);
RemoveScop(p);
CGenerator(p, "./src-regen-codegen/");



extracted = CopyGecosProject(p);
ScopSlicer(p);
sliced = CopyGecosProject(p);

RemoveScop(p);
CGenerator(p, "./src-regen-extracted/");

extracted = CopyGecosProject(p);


ScopISLCodeGen(sliced);
RemoveScop(sliced);
CGenerator(sliced, "./src-regen-sliced/");

ScopISLCodeGen(sliced);

#ScopIteratorNormalizer(p);
RemoveScop(p);
CGenerator(p, "./src-regen/");

