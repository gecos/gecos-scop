package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopNode
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.javatuples.Pair
import org.polymodel.algebra.AlgebraUserFactory
import org.polymodel.algebra.FuzzyBoolean
import org.polymodel.algebra.IntConstraint
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression

import static org.polymodel.algebra.factory.IntExpressionBuilder.*
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLExpressionConverter
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.AlgebraFactory
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator
import org.polymodel.algebra.affine.AffineExpression

class DomainConstraintsCollector {
 
	Map<ScopForLoop,IntConstraintSystem> icsMap = new HashMap<ScopForLoop,IntConstraintSystem>()
	val List<IntConstraintSystem> list = new ArrayList()
	val List<IntConstraintSystem> nlist	= new ArrayList()
	val ScopNode root
	
	new (ScopNode root) {
		this.root = root
	}
		
	def static compute(ScopNode node, ScopNode root) {
		if (root===null) throw new UnsupportedOperationException("Cannot compute contraints when root is null "+node);
		if (node===null) throw new UnsupportedOperationException("Cannot compute contraints when node is null ");
		var ScopNode validRoot = null
		if (root.parentScop === null) 
			validRoot = node.scopRoot 
		else 
			validRoot = root.parentScop
		
		if (validRoot===null) throw new UnsupportedOperationException("Cannot compute valid root "+validRoot);
		val collector = new DomainConstraintsCollector(validRoot)
		if (node != root)
			collector.collectConstraints(node, null);
		new Pair(collector.list, collector.nlist)
	}

	def dispatch void collectConstraints(ScopNode node, ScopNode prev) {
		val parent = node.parentScop
		if (parent != root)
			collectConstraints(parent, node)
	}
	

	def dispatch void collectConstraints(GecosScopBlock node, ScopNode prev) {
		if(node?.context?.constraints.size != 0)
			list.add(node.context);
	}
	
	
	
	def dispatch void collectConstraints(ScopGuard node, ScopNode prev) {
		// Negate list to pass from union to intersection of constraints
		// i.e. x || y = !(!x && !y)
		if (node.predicate.size > 1) {
			if (prev == node.thenNode) {
				nlist.addAll(IntExpressionBuilder.negate(node.predicate))
			} else {
				list.addAll(IntExpressionBuilder.negate(node.predicate))
			}
		} else {
			if (prev == node.thenNode) {
				list.addAll(node.predicate)
			} else {
				nlist.addAll(node.predicate)
			}
		}

		val parent = node.parentScop
		if (parent != root)
			collectConstraints(parent, node)
	}
	
	
	def dispatch void collectConstraints(ScopForLoop node, ScopNode prev) {
		if (!icsMap.containsKey(node)){
			val IntExpression stride = node.getStride();
			if (!(stride.isConstant() == FuzzyBoolean.YES))
				throw new UnsupportedOperationException("Stride is not constant. This results in non-affine constraints and cannot be handled by the polyhedral model.");
			
			val long strideValue = stride.toAffine().getConstantTerm().getCoef();
			
			if ((node.getLB()===null)||(node.getUB()===null)) {
				throw new UnsupportedOperationException("Inconsistent ScopForNode node :"+node);
			}
			var IntExpression lb2 = node.getLB().copy();
			var IntExpression ub2 = node.getUB().copy();

			var IntConstraint eq = null;
			val iterator = node.getIterator()
			if (stride.isEquivalent(constant(1)) != FuzzyBoolean.YES
			    && stride.isEquivalent(constant(-1)) != FuzzyBoolean.YES) {
				if (lb2 instanceof AffineExpression) {
				val expr = sum(prod(affine(term(-1)),lb2),affine(iterator)).simplify 
				eq = eq(qaffine(mod(expr as AffineExpression, strideValue)), affine(term(0)))				}
			}
			
			val IntConstraint lb = ge(affine(term(iterator)),lb2);
			val IntConstraint ub = le(affine(term(iterator)),ub2);
			
			val intConstraintSystem = AlgebraUserFactory.intConstraintSystem();
			val List<IntConstraint> l = intConstraintSystem.constraints
			l.add(lb);
			l.add(ub);
			if (eq !== null)
				l.add(eq);
				
			icsMap.put(node,intConstraintSystem)
		}
		list.add(icsMap.get(node));
		val parent = node.parentScop
		if (parent != root)
			collectConstraints(parent, node)
	}
}