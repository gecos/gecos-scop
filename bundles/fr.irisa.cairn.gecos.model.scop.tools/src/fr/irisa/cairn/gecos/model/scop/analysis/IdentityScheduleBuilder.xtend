package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopAccess
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import java.util.ArrayList
import java.util.List
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression
import org.polymodel.algebra.quasiAffine.QuasiAffineFactory
import org.polymodel.algebra.FuzzyBoolean

/** 
 * Compute the original schedule expressions for a given node in a GecosScop.
 * @author amorvan
 */
class IdentityScheduleBuilder {
  
	int maxDepth
	List<IntExpression> schedule
	ScopNode g
	ScopNode root

	new(ScopNode g) {
		this.g = g
		this.root = g.getScopRoot()
		this.maxDepth = computeMaxDepth()
	}

	new(ScopNode g, ScopNode root) {
		this.g = g
		this.root = root
		this.maxDepth = computeMaxDepth()
	}



	def static List<IntExpression> getSchedule(ScopNode g) {
		return (new IdentityScheduleBuilder(g)).getSchedule()
	}

	def static List<IntExpression> getSchedule(ScopNode g, ScopNode root) {
		return (new IdentityScheduleBuilder(g)).getSchedule()
	}

	def private int computeMaxDepth() {
		var int maxDepth = 0
		for (ScopStatement stmt : root.listAllStatements()) {
			maxDepth = Math::max(maxDepth, stmt.getNumberOfEnclosingDimension())
		}
		var int nbSchedDimMax = 2 * maxDepth + 1
		return nbSchedDimMax
	}


	def List<IntExpression> getSchedule() {
		if (this.schedule === null) {
			var IDScheduleBuilderVisitor privateVisitor = new IDScheduleBuilderVisitor(maxDepth)
			privateVisitor.visit(g)
			this.schedule = privateVisitor.getSchedule()
		}
		return this.schedule
	}



	static class IDScheduleBuilderVisitor {
		int cpt
		protected int currentDepth
		ScopNode previousNode
		protected boolean bottomUp = true
		List<IntExpression> schedule
		int maxDeth

		def List<IntExpression> getSchedule() {
			return schedule
		}

		new(int maxDeth) {
			this.maxDeth = maxDeth
			schedule = new ArrayList<IntExpression>(maxDeth)
		}

		def dispatch void visit(ScopAccess g) {
			val s = g.parentScop;
			visit(s)
			
		}
		def dispatch void visit(ScopStatement g) {
			if (this.bottomUp) {
				var int depth = g.numberOfEnclosingDimension * 2
				if(depth > maxDeth) throw new RuntimeException();
				if (this.schedule.isEmpty()) {
					// init schedule
					for (var int i = 0; i < depth + 1; i++) {
						this.schedule.add(i, IntExpressionBuilder::constant(-1))
					}
					// fill with 0 when depth is not maxDepth
					for (var int i = depth + 1; i < maxDeth; i++) {
						this.schedule.add(i, IntExpressionBuilder::constant(0))
					}
				}
				// start visit
				this.cpt = 0
				this.currentDepth = depth
				this.previousNode = g
				visit(g.getParentScop())
			} else {
				this.cpt++
			}
		}

		def dispatch void visit(ScopBlock g) {
			if (this.bottomUp) {
				var int indexOf = g.children.indexOf(this.previousNode)
				if(indexOf === -1) throw new RuntimeException();
				this.bottomUp = false
				for (var int i = 0; i < indexOf; i++) {
					var ScopNode node = g.children.get(i)
					visit(node)
				}
				this.bottomUp = true
				this.previousNode = g
				visit(g.getParentScop())
			} else {
				for (ScopNode n : g.listAllStatements()) {
					visit(n)
				}
			}
		}

		// @Override
		def dispatch void visit(ScopGuard g) {
			if (this.bottomUp) {
				if (g.getElseNode() === this.previousNode) {
					this.bottomUp = false
					visit(g.parentScop)
					this.bottomUp = true
				}
				this.previousNode = g
				visit(g.parentScop)
			} else {
				visit(g.thenNode)
				if(g.elseNode !== null) {
					visit(g.elseNode)
				}
			}
		}

		// @Override
		def dispatch void visit(ScopForLoop g) {
			if (this.bottomUp) {
				this.schedule.set(this.currentDepth, IntExpressionBuilder::constant(this.cpt))
				var QuasiAffineExpression expr = QuasiAffineFactory::eINSTANCE.createQuasiAffineExpression()
				if (g.stride.isConstant != FuzzyBoolean.YES)
					throw new UnsupportedOperationException("Expecting constant stride instead of " + g.stride)
				val sign = if (g.stride.toAffine.constantTerm.coef > 0) 1 else -1
				expr.getTerms().add(IntExpressionBuilder::mul(IntExpressionBuilder::affine(g.getIterator()), sign))
				this.schedule.set(this.currentDepth - 1, expr.simplify())
				this.currentDepth -= 2
				this.cpt = 0
				this.previousNode = g
				visit(g.getParentScop())
			} else {
				this.cpt++
			}
		}

		// @Override
		def dispatch void  visit(GecosScopBlock g) {
			// done 
			this.schedule.set(this.currentDepth, IntExpressionBuilder::constant(this.cpt)) // System.out.println("done");
		}
	}
}
