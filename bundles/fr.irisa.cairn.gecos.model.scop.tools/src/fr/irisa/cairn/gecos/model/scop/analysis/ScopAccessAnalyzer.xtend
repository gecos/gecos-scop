package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.ScopNode
import java.util.HashSet
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopAccess
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet
import gecos.types.Type
import gecos.types.ArrayType
import java.util.ArrayList
import gecos.instrs.IntInstruction
import gecos.types.BaseType
import java.util.Map
import java.util.HashMap
import fr.irisa.cairn.gecos.model.scop.codegen.ScopISLCodegen
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType

class ScopAccessAnalyzer {

	static final boolean VERBOSE = true
	
	def static dispatch JNIISLSet  getTypeFootprint(Type t) {
		throw new UnsupportedOperationException("Cannot derive shape domain for type "+t);
	}
	def static  dispatch JNIISLSet getTypeFootprint(BaseType t) {
		return JNIISLSet.buildFromString('''{[]}''') 
	}
	def static  dispatch JNIISLSet getTypeFootprint(ArrayType t) {
		var header = new ArrayList<String>(); 
		var constraint =  new ArrayList<String>();
		var int index=0;
		for (s: t.sizes) {
			switch (s) {
				IntInstruction : { 
					val dim = 'i'+index
					header+=dim;
					constraint+= "0<="+dim+"<"+s.value
				}
				default : {throw new UnsupportedOperationException("Cannot derive size of array type "+t)}
			}
			index++
		}
		return JNIISLSet.buildFromString('''{[«FOR id:header SEPARATOR ","»«id»«ENDFOR»]:«FOR id:constraint SEPARATOR " and "»«id»«ENDFOR»}''') 
	}
    
	/*
	 * compute the access foot print (all array cells accessed in that Scop)
	 */
	def static getArrayAccessFootprint(Symbol sym, ScopNode n) {
		var JNIISLSet set = null;
		for (s:n.listAllStatements) {
			val symrefs = s.listAllAccesses.filter[a|a.sym==sym]
			for (a:symrefs) {
				val map = ScopISLConverter.getAccessMap(a,n) 
				if(set===null) {
					set = map.getRange();
				} else {
					set = set.union(map.getRange());
				}
			}
		}
		set
	}
	
	/*
	 * compute the access foot print (all array cells accessed in that Scop)
	 */
	def static getArrayWriteFootprint(Symbol sym, ScopNode n) {
		ScopISLConverter.getAllWriteAccessMaps(n,sym)
	}

	/* Checks is an given array has all its cell overwritten when executing the 
	 * SCoP given a argument.
	 */
	def static isFullyOverwritten(Symbol sym, ScopNode n) {
		val footprint = getArrayAccessFootprint(sym,n)
		var declShape = getTypeFootprint(sym.type)
		declShape = declShape.tupleName=sym.name
		println(declShape)
		println(footprint)
		val sub = declShape.subtract(footprint.copy)
		sub.isEmpty		
	}

	def static getAllReadOnlyArrays(ScopNode n) {
		var HashSet<Symbol> used = new HashSet<Symbol>
		var HashSet<Symbol> writes = new HashSet<Symbol>
		for (s:n.listAllStatements) {
			for (a:s.listAllAccesses) {
				used+=a.sym
				switch(a) {
					ScopWrite :{
						writes+=a.sym
					}
				}
			}
		}
		used.removeAll(writes)
		used
	}

	def static getAllWriteOnlyArrays(ScopNode n) {
		var HashSet<Symbol> used = new HashSet<Symbol>
		var HashSet<Symbol> reads = new HashSet<Symbol>
		for (s:n.listAllStatements) {
			for (a:s.listAllAccesses) {
				used+=a.sym
				switch(a) {
					ScopRead :{
						reads+=a.sym
					}
				}
			}
		}
		used.removeAll(reads)
		used
	}

	def static computeFlowIn(Symbol sym, ScopNode root) {
		/* Goal : search for symbols that are live-in 
		/* Approach : arrays accessed in ScopRead instances that do not 
		 * have a source in the scop correspond to the flow-in */
		var JNIISLUnionSet flowIn
		
		val reads = ScopISLConverter.getAllReadAccessMaps(root, sym)
		if (reads.isEmpty) {
			flowIn = reads.range()
		} else {
			val rawDeps = ScopISLConverter.getRAWDepenceGraph(sym, root)
			val readsSourcedInScop = reads.copy.intersectDomain(rawDeps.domain())
			flowIn = reads.subtract(readsSourcedInScop).range()			
		}
		
		if(flowIn.nbSets>1) {
			throw new UnsupportedOperationException("Inconsistent input flow information")
		} else if(flowIn.nbSets==0) {
			return JNIISLSet.buildFromString("{ : 1 = 0 }")
		}
		return flowIn.sets.get(0)
	}

	def static computeFlowOut(Symbol sym, ScopNode node) {
		val flowOut = computeFlowOutMap(sym, node).range()

		if (flowOut.nbSets > 1) {
			throw new UnsupportedOperationException("Inconsistent output flow information")
		} else if (flowOut.nbSets == 0) {
			return JNIISLSet.buildFromString("{ : 1 = 0 }")
		}
		return flowOut.sets.get(0)
	}
	
	def static computeFlowOutMap(Symbol sym, ScopNode node) {
		var JNIISLUnionMap flowOut
		val root = node.scopRoot as GecosScopBlock

		if (root.liveOutSymbols.contains(sym)) {
			flowOut = ScopAccessAnalyzer.getArrayWriteFootprint(sym, node)
		} else {
			val writes = ScopISLConverter.getAllWriteAccessMaps(node, sym)
			if (writes.isEmpty) {
				flowOut = writes
			} else {
				val rawInDeps = ScopISLConverter.getRAWDepenceGraph(sym, node).range()
				var rawAllDeps = ScopISLConverter.getRAWDepenceGraph(sym, root).range()
				rawAllDeps = ScopISLConverter.moveDimsToParams(rawAllDeps, node.listAllEnclosingIterators)
				val rawOutDeps = rawAllDeps.subtract(rawInDeps)
				flowOut = writes.intersectDomain(rawOutDeps)
			}
		}

		return flowOut
	}
	
	def static debug(String string) {
		if (VERBOSE) {
			println("[ScopAccessAnalyzer] "+string)

		}
	}
		
	// FIXME : untested
	def static getDefUses(ScopNode root) {
		val HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> useDefs= getUseDefs(root)
		var res = new HashMap<ScopWrite, Map<ScopRead, JNIISLMap>>();
		
	   for (use: useDefs.keySet) {
	   		if (use===null) throw new RuntimeException("NYI")
	   		val defs = useDefs.get(use);
	   		for (def : defs.keySet) {
	   			if (def===null) throw new RuntimeException("NYI")
				if (!res.containsKey(def)) {
					res.put(def, new HashMap<ScopRead, JNIISLMap>)
				}
				res.get(def).put(use, defs.get(def).reverse)
	   		}
	   }
	  res
	}
	

//	// FIXME : untested
//	def static getDefUses(ScopNode root) {
//		try {
//			var res = new HashMap<ScopWrite, Map<ScopRead, JNIISLMap>>();
//			val stmts = root.listAllStatements
//			val prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(root);
//			val allEdges = prdg.reverse
//			for (s : stmts) {
//				var offset=0
//				debug("Statement "+s)
//				for (read : s.listAllWriteAccess) {
//					val targetSym = read.sym
//					val label =s.id+"_RD"+(offset++);
//					debug("- write "+read+" in statement "+s)
//					val edge =allEdges.copy.maps.filter[e|e.outputTupleName==label];
//					for (e:edge) {
//						val dstLabel=e.inputTupleName
//						val dstId =dstLabel.split("_RD").get(0)
//						val srcStmt = stmts.filter[src|src.id==dstId].get(0)
//						debug("  - use statement :"+srcStmt)
//						if (!res.containsKey(read)) {
//							res.put(read, new HashMap<ScopRead, JNIISLMap>)
//						}
//						for(_write : srcStmt.listAllReadAccess.filter[w|w.sym==targetSym]) {
//							res.get(read).put(_write, e)
//						}
//					}	
//				}	
//			}
//			return res
//		} catch (Exception e ) {
//			throw e;
//		}
//	}
	
	// FIXME : untested
	def static getUseDefs(ScopNode root) {
		try {
			var res = new HashMap<ScopRead, Map<ScopWrite, JNIISLMap>>();
			var readMap = new HashMap<String, ScopRead>();
			var writeMap = new HashMap<String, ScopWrite>();
			val stmts = root.listAllStatements
			val stntNmes = stmts.map[s|s.id]
			val prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(root);
			
			for (s : stmts) {
				var offset=0
				debug("Statement "+s)
				for (read : s.listAllReadAccess) {
					val targetSym = read.sym
					val label =s.id+"_RD"+(offset++);
					debug("- read "+read+" in statement "+s)
					val edge =prdg.maps.filter[e|e.inputTupleName==label];
					for (e:edge) {
						val srcLabel=e.outputTupleName
						val srcId =srcLabel.split("_WR").get(0)
						//debug("  - source :"+srcId)
						val srcStmt = stmts.filter[src|src.id==srcId].get(0)
						debug("  - source statement :"+srcStmt)
						if (!res.containsKey(read)) {
							res.put(read, new HashMap<ScopWrite, JNIISLMap>)
						}
						for(write : srcStmt.listAllWriteAccess.filter[w|w.sym==targetSym]) {
					   		if (write===null) throw new RuntimeException("NYI")
							res.get(read).put(write, e)
						}
					}	
				}	
			}
			return res
		} catch (Exception e ) {
			throw e;
		}
	}
	// FIXME : untested
	def static getUses(Symbol sym, ScopNode root) {
		try {
			var res = new HashMap<ScopWrite, Map<ScopRead, JNIISLMap>>();
			val prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(root);
			var flow = prdg.reverse;
			for (edge : flow.maps) {
				val wrStmt= root.getStatement(edge.inputTupleName);
				if (wrStmt.listAllWriteAccess.size>1) 
					throw new UnsupportedOperationException("MultiWrite statement are not (yet) supported in getUses")
				val wr = wrStmt.listAllWriteAccess.get(0)
				if (!res.containsKey(wr)) {
					res.put(wr, new HashMap<ScopRead, JNIISLMap>)
				}
				val rdStmt = root.getStatement(edge.outputTupleName);
				for (rd : rdStmt.listAllReadAccess) {
					res.get(wr).put(rd, edge.copy)
				}
			}
			return res
		} catch (Exception e ) {
			throw e;
		}
	}

	// FIXME : untested
	def static getUsesfor(ScopWrite wr, ScopNode root) {
		println("Use maps for "+wr+" in "+wr.enclosingStatement)
		getUses(wr.sym,root).get(wr);
	}

	// FIXME : untested
	def static getLastUsefor(ScopWrite wr, ScopNode root) {
		println("Last use map for "+wr+" in "+wr.enclosingStatement)
		
		var res = new HashMap<ScopRead, JNIISLMap>();
		val uses =getUses(wr.sym,root).get(wr);
		if (uses!==null)
			for (use :uses.entrySet) {
				val useMap = use.value.lexMax;
				res.put(use.key,useMap);
			}
		res
	}


	// FIXME : untested
	def static getFirstUsefor(ScopWrite wr, ScopNode root) {
		println("First use map for "+wr+" in "+wr.enclosingStatement)
		var res = new HashMap<ScopRead, JNIISLMap>();
		val uses =getUses(wr.sym,root).get(wr);
		if (uses!==null)
			for (use :uses.entrySet) {
				val useMap = use.value.lexMin;
				res.put(use.key,useMap);
			}
		res
	}
	

}
