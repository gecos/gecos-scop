package fr.irisa.cairn.gecos.model.scop.analysis

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement

class ScopCollapser {
	
	GecosProject proj
	
	new(GecosProject p) {
		proj=p	
	}

	def compute() {
		val scops = EMFUtils.eAllContentsFirstInstancesOf(proj,typeof(GecosScopBlock))	
		for (scop : scops) collapse(scop)
	}
	
	def dispatch ScopUnexpandedStatement collapseStmt(ScopInstructionStatement stmt, ScopNode n) {
		val dom  = ScopISLConverter.getDomain(stmt,n)
		val sched = ScopISLConverter.getIdSchedule(stmt,n);
		var res = ScopUserFactory.unexpandedStmt(stmt.listAllEnclosingIterators(n), dom.toString, sched.toString)
		res.id=stmt.id
		res.children+=stmt.children
		res
	}
	
	def dispatch ScopUnexpandedStatement collapseStmt(ScopUnexpandedStatement stmt, ScopNode n) {
		throw new UnsupportedOperationException("Not yet implemented")
	}
   
	def collapse(ScopNode n) {
		val stmts = n.listAllStatements.filter(ScopInstructionStatement)
		var res = ScopUserFactory.scopBlock()
		Substitute.replace(n,res);
		for(s:stmts) {
			res.children+=collapseStmt(s,n)
		}
	}
}