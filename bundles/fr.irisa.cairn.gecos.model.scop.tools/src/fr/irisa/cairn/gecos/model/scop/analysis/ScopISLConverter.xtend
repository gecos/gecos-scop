package fr.irisa.cairn.gecos.model.scop.analysis

import com.google.common.collect.Lists
import fr.irisa.cairn.gecos.model.scop.ScopAccess
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDataflowAnalysis
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet
import gecos.core.Symbol
import gecos.instrs.IntInstruction
import gecos.types.ArrayType
import gecos.types.Type
import java.util.List
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.prettyprinter.algebra.ISLPrettyPrinter
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead

class ScopISLConverter {

	// ScopNode root
	static ISLPrettyPrinter pp = new ISLPrettyPrinter

	static def JNIISLSet getContext(ScopNode node) {
		getDomain(node, node).paramSet()
	}

	static def JNIISLUnionMap getMemoryBasedDepenceGraph(ScopNode root) {
		val schedules = getAllIdSchedules(root);
		val domains = getAllStatementDomains(root);
		val reads = getAllReadAccessMaps(root);
		val writes = getAllWriteAccessMaps(root);
		val prdg = JNIISLDataflowAnalysis.computeMemoryBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getMemoryBasedDepenceGraph(Symbol s, ScopNode root) {
		val schedules = getAllIdSchedules(s, root);
		val domains = getAllStatementDomains(s, root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeMemoryBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getWAWDepenceGraph(Symbol s, ScopNode root) {
		val schedules = getAllIdSchedules(s, root);
		val domains = getAllStatementDomains(s, root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeWAWMemoryBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getWARDepenceGraph(Symbol s, ScopNode root) {
		val schedules = getAllIdSchedules(s, root);
		val domains = getAllStatementDomains(s, root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeWARMemoryBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getRAWDepenceGraph(Symbol s, ScopNode root) {
		val schedules = getAllIdSchedules(s, root);
		val domains = getAllStatementDomains(s, root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeRAWMemoryBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getValueBasedDepenceGraph(Symbol s, ScopNode root) {
		val schedules = getAllIdSchedules(s, root);
		val domains = getAllStatementDomains(s, root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getValueBasedDepenceGraph(ScopNode root) {
		val schedules = getAllIdSchedules(root);
		val domains = getAllStatementDomains(root);
		val reads = getAllReadAccessMaps(root);
		val writes = getAllWriteAccessMaps(root);
		val prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}
	static def JNIISLUnionMap getValueBasedDepenceGraphWithExplicitRead(ScopNode root) {
		val schedules = getAllIdSchedulesWithExplicitRead(null,root);
		val domains = getAllStatementDomainsWithExplicitRead(root);
		val reads = getAllReadAccessMapsWithExplicitRead(root,null);
		val writes = getAllWriteAccessMapsWithExplicitRead(root,null);
		val prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLUnionMap getValueBasedDepenceGraphBySymbol(ScopNode root, Symbol s) {
		val schedules = getAllIdSchedules(root);
		val domains = getAllStatementDomains(root);
		val reads = getAllReadAccessMaps(root, s);
		val writes = getAllWriteAccessMaps(root, s);
		val prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
		prdg
	}

	static def JNIISLMap getIdSchedule(ScopNode node, ScopNode root) {
		val indices = node.listAllEnclosingIterators(root).map[i|i.symName];
		val params = root.listAllParameters().map[i|i.symName];
		val exprs = IdentityScheduleBuilder.getSchedule(node, root)
		val str = '''«params» -> { «indices» -> «exprs.map[e|("o"+(exprs.indexOf(e)))]»: «FOR e : exprs SEPARATOR " and "»«"o"+exprs.indexOf(e)+"="+pp.print(e)»«ENDFOR»}'''
		var schedule = JNIISLMap.buildFromString(str);
		val domain = getDomain(node, root)
		schedule = schedule.intersectDomain(domain)
		schedule
	}

	static def JNIISLUnionMap getAllIdSchedules(ScopNode root) {
		getAllIdSchedules(root,root.listAllStatements) 
	}

	static def JNIISLUnionMap getAllIdSchedules(ScopNode root, ScopStatement ...stmts) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");

		for (ScopStatement stmt : stmts) {
			var scheduleMap = getIdSchedule(stmt, root)
			scheduleMap = scheduleMap.setInputTupleName(stmt.id)
			res = res.addMap(scheduleMap)
		}
		res
	}

	static def JNIISLUnionMap getAllIdSchedulesWithExplicitRead(Symbol s,ScopNode root) {

		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			var refSymbol = stmt.listAllAccesses.exists[a|a.sym == s]
			if (refSymbol || s===null) {
				var scheduleMap = getIdSchedule(stmt, root)
				scheduleMap=scheduleMap.addOutputDimensions(#{"o"})
				var schedule_wr = scheduleMap.copy.setInputTupleName(stmt.id+"_WR")
				val newOffset = scheduleMap.nbOuts-1
				schedule_wr = schedule_wr.sliceFixed(JNIISLDimType.isl_dim_out,newOffset,1)
				res = res.addMap(schedule_wr)
				var id=0
				for(read : stmt.listAllReadAccess) {
					var domain_rd = scheduleMap.copy.setInputTupleName(stmt.id+"_RD"+(id++))
					domain_rd = domain_rd.sliceFixed(JNIISLDimType.isl_dim_out,newOffset,0)
					res = res.addMap(domain_rd)
				}
				
			}
		}
		res
	}
	
	static def JNIISLUnionMap getAllIdSchedules(Symbol s, ScopNode root) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			var refSymbol = stmt.listAllAccesses.exists[a|a.sym == s]
			if (refSymbol) {
				var scheduleMap = getIdSchedule(stmt, root)
				scheduleMap = scheduleMap.setInputTupleName(stmt.id)
				res = res.addMap(scheduleMap)
			}
		}
		res
	}

	static def JNIISLUnionSet getAllDomains(ScopNode root) {
		var JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			var domain = getDomain(stmt, root)
			domain = domain.tupleName = stmt.id
			res = res.addSet(domain)
		}
		res
	}

	static def JNIISLUnionMap getAllReadAccessMaps(ScopNode root) {
		getAllReadAccessMaps(root, null);
	}
	

	static def JNIISLUnionMap getAllReadAccessMapsWithExplicitRead(ScopNode root, Symbol symfilter) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			val dom = getDomain(stmt, root)
			var map=  dom.buildIdentityMap;
			val wraccess = map.copy
			var id=0
			for (ScopRead read : stmt.listAllReadAccess) {
				if (symfilter === null || symfilter == read.sym) {
					var explRead =  wraccess.copy
					explRead = explRead.outputTupleName=stmt.id+"_tmp_"+(id)
					explRead = explRead.setTupleName(JNIISLDimType.isl_dim_in, stmt.id+"_WR")
					res = res.addMap(explRead)
			
					var scheduleMap = getAccessMap(read, root)
					scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.id+"_RD"+(id++))
					res = res.addMap(scheduleMap)
				}
			}
		}
		res
	}
	
	static def JNIISLUnionMap getAllReadAccessMaps(ScopNode root, Symbol symfilter) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			for (ScopRead read : stmt.listAllReadAccess) {
				if (symfilter === null || symfilter == read.sym) {
			
					var scheduleMap = getAccessMap(read, root)
					scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.id)
					res = res.addMap(scheduleMap)
				}
			}
		}
		res
	}
  
	static def JNIISLUnionMap getAllWriteAccessMaps(ScopNode root) {
		getAllWriteAccessMaps(root, null);
	}
	static def JNIISLUnionMap getAllWriteAccessMapsWithExplicitRead(ScopNode root, Symbol s) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			val dom = getDomain(stmt, root)
			var map=  dom.buildIdentityMap;
			val wraccess = map.copy
			var id=0
			for (ScopWrite write : stmt.listAllWriteAccess) {
				var scheduleMap = getAccessMap(write, root)
				scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.id+"_WR")
				res = res.addMap(scheduleMap)
			}
			for (ScopRead read : stmt.listAllReadAccess) {
				if (s === null || s == read.sym) {
					var explWrite =  wraccess.copy
					explWrite = explWrite.outputTupleName=stmt.id+"_tmp_"+(id)
					explWrite = explWrite.setTupleName(JNIISLDimType.isl_dim_in, stmt.id+"_RD"+(id++))
					res = res.addMap(explWrite)
			
				}
			}
		}
		res
	}

 	static def JNIISLUnionMap getAllWriteAccessMaps(ScopNode root, Symbol symfilter) {
		var JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			for (ScopWrite write : stmt.listAllWriteAccess) {
				if (symfilter === null || symfilter == write.sym) {
					var scheduleMap = getAccessMap(write, root)
					scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.id)
					res = res.addMap(scheduleMap)
				}
			}
		}
		res
	}

	static def JNIISLUnionSet getAllStatementDomains(Symbol s, ScopNode root) {
		var JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");

		for (ScopStatement stmt : root.listAllStatements.filter[stmt|stmt.useSymbol(s)]) {
			var domain = stmt.getDomain(root)
			domain = domain.setTupleName(stmt.id)
			res = res.addSet(domain)
		}
		res
	}

	static def JNIISLUnionSet getAllStatementDomains(ScopNode root) {
		var JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			var domain = stmt.getDomain(root)
			domain = domain.setTupleName(stmt.id)
			res = res.addSet(domain)
		}
		res
	}
	static def JNIISLUnionSet getAllStatementDomainsWithExplicitRead(ScopNode root) {
		var JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
		for (ScopStatement stmt : root.listAllStatements) {
			var domain = stmt.getDomain(root)
			val domain_wr = domain.copy.setTupleName(stmt.id+"_WR")
			res = res.addSet(domain_wr)
			var id=0
			for(read : stmt.listAllReadAccess) {
				val domain_rd = domain.copy.setTupleName(stmt.id+"_RD"+(id++))
				res = res.addSet(domain_rd)
			}
		}
		res
	}

	static def dispatch JNIISLMap getAccessMap(ScopAccess rd, ScopNode root) {
		val indices = rd.listAllEnclosingIterators(root);
		buildIslMap(rd.symName, rd, root, indices, rd.indexExpressions).inputTupleName = rd.enclosingStatement.id;
	}

	static def dispatch  JNIISLMap getAccessMap(ScopRegionRead rd, ScopNode root) {
		throw new UnsupportedOperationException("NYI");
	}

	static def JNIISLMap getAccessMap(ScopAccess wr) {
		getAccessMap(wr, (wr.root as ScopStatement).scopRoot);
	}

	static def JNIISLMap buildIslMap(String label, ScopNode node, ScopNode root, Iterable<ScopDimension> indices, List<IntExpression> exprs) {
		if (indices === null) {
			println("This should not happen")
			throw new RuntimeException('''no indices in buildIslMap'(«label», «node», «root», «exprs»)''')
		}
		val params = root.listAllParameters().map[p|p.name];
		val str = '''«params» -> {«indices.map[n|n.name]» -> «label»«exprs.map[e|"o"+exprs.indexOf(e)]»: «FOR e : exprs SEPARATOR " and "»«"o"+exprs.indexOf(e)+"="+pp.print(e)»«ENDFOR»}'''
		var access = JNIISLMap.buildFromString(str);
		val domain = getDomain(node, root)
		access = access.intersectDomain(domain)
		access
	}

	static def dispatch JNIISLSet getDomain(ScopNode node) {
		getDomain(node, node.scopRoot)
	}

	static def dispatch JNIISLSet getDomain(ScopStatement node) {
		getDomain(node, node.scopRoot).tupleName = node.id
	}

	static def dispatch JNIISLSet getDomain(Type s) {
	}

	static def dispatch JNIISLSet getDomain(ArrayType s) {
		val dimSizes = s.sizes.map[d|(d as IntInstruction).value]
		var indices = Lists.newArrayList
		var constraints = Lists.newArrayList
		for (i : 0 .. (dimSizes.size - 1)) {
			indices.add(('c' + i))
			constraints.add(''' 0<=«indices.get(i)»<«dimSizes.get(i)»''')
		}
		val str = '''[] -> { «indices» : «FOR cs : constraints SEPARATOR " and "»«cs»«ENDFOR»}'''
		JNIISLSet.buildFromString(str)
	}

	static def JNIISLSet getDomain(ScopNode node, ScopNode root) {
		val collected = DomainConstraintsCollector.compute(node, node.scopRoot)

		val ics = (collected.get(0) as List<IntConstraintSystem>).filter[t|t.constraints.size > 0];
		val nics = (collected.get(1) as List<IntConstraintSystem>).filter[t|t.constraints.size > 0];
		var dims = node.listAllEnclosingIterators(root);
		
		var indices = dims.map[i|i.name];
		val params = root.listAllParameters().map[p|p.name];

		val strParamsIndices = '''«IF !params.empty»«params» -> «ENDIF»{ «indices» : '''
		val str = '''«strParamsIndices»«FOR cs : ics SEPARATOR " and "»«pp.print(cs.simplify)»«ENDFOR»}'''
		try {
			val res = JNIISLSet.buildFromString(str)
			if (nics.size > 0) {
				var elseRes = res.copy
				for (nic : nics) {
					val elseSet = JNIISLSet.buildFromString('''«strParamsIndices»«pp.print(nic)»}''');
					elseRes = elseRes.intersect(elseSet.complement)
					if (res.isEmpty) {
						println("Empty domain !");
					}
				}
				return elseRes
			}
			return res
		} catch(RuntimeException e) {
			throw new RuntimeException("Cannot construct isl_set from "+str+" for node "+node);		
		}
	}
	
	static def JNIISLUnionSet moveDimsToParams(JNIISLUnionSet origUnionSet, List<ScopDimension> dims) {
		var unionSet = origUnionSet
		for(dim: dims) {
			var relUnionSet = JNIISLUnionSet.buildEmpty(origUnionSet.space)
			for (set: unionSet.sets) {
				val paramPos = set.nbParams
				val name = set.tupleName
				val dimPos = set.findDimByName(JNIISLDimType.isl_dim_set, dim.symName)
				var relSet = set.moveDims(JNIISLDimType.isl_dim_param, paramPos, JNIISLDimType.isl_dim_set, dimPos, 1);
				relSet = relSet.setTupleName(name)
				relUnionSet = relUnionSet.addSet(relSet)
			}
			unionSet = relUnionSet
		}
		
		unionSet
	}
}
