package fr.irisa.cairn.gecos.model.scop.analysis

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import org.polymodel.algebra.AlgebraUserFactory
import java.awt.geom.AffineTransform
import tom.mapping.introspectors.affine.AffineIntrospector
import org.polymodel.algebra.affine.AffineExpression
import org.polymodel.algebra.tom.ArithmeticOperations
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import java.util.Map
import fr.irisa.cairn.gecos.model.scop.ScopRead
import java.util.HashMap
import fr.irisa.cairn.gecos.model.scop.transforms.ScopCopyIdentifier
import fr.irisa.cairn.gecos.model.scop.ScopStatement

class ScopReductionIdentifier {

	GecosProject p;
	
	boolean DEBUG=true
	boolean VERBOSE=true
	
	def debug(String string) {
		if(DEBUG) {
			println("[ScopReductionIdentifier] "+string)
		}
	}

	def log(String string) {
		if(VERBOSE) {
			println("[ScopReductionIdentifier] "+string)
		}
	}

	new(GecosProject p) {
		this.p=p
		
	}
	
	def getTripCount(ScopForLoop loop) {
		val r = ArithmeticOperations.add(loop.UB.copy,loop.LB.copy).simplify;
		try {
			(r as AffineExpression).constantTerm.coef
		} catch (Exception e) {
			return null;
		}
	}
	def compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(p,GecosScopBlock)
		for (scop:scops) {
			val defuses = ScopAccessAnalyzer.getDefUses(scop);
			val HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> useDefs= ScopAccessAnalyzer.getUseDefs(scop);
			val defs = defuses.keySet
			for (def : defs) {
				debug("*** def "+def); 
				val stmt = def.enclosingStatement
				val uses = defuses.get(def)
				if(ScopCopyIdentifier.isCopy(stmt)) {
					debug("Copy statement "+stmt);
					for (use : uses.keySet) {
						debug("  used by "+use.root)
						// check whether def cell is still live  at use
						for (srcRead : def.enclosingStatement.listAllReadAccess) {
							val srcWrite = useDefs.get(srcRead)
							debug("      - "+srcWrite)
							
						}
					}															
				} else {
					
				}
				
				if (uses.size==1) {
					val use = uses.keySet.get(0);
					debug("*** def "+def+" has single use "+use); 
					if (useDefs.containsKey(use)) {
						val sources = useDefs.get(use)
						debug("  *** use "+def+" has sources "+sources.keySet()); 
						if (sources.size==1) {
							debug(" Bijection def/use "+def+"<=>"+use); 
						}
					}
				}
			}

//			for (node : scop.listAllChildredScopNodes) {
//				val reduction = new ScopReductionPatternMatching(node);
//				if (reduction.sym!==null) {
//					debug("Found reduction "+reduction.stmt+" on symbol "+reduction.sym)
//					val loop = reduction.loop
//					if (getTripCount(loop)!==null && getTripCount(loop)<4) {
//						val copy = reduction.stmt.copy
//					}
//				}
//				
//			}
		}
	}
	
	
}