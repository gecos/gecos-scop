/*******************************************************************************
* Copyright (c) 2012 Universite de Rennes 1 / Inria.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the FreeBSD License v1.0
* which accompanies this distribution, and is available at
* http://www.freebsd.org/copyright/freebsd-license.html
*
* Contributors:
*    DERRIEN Steven - initial API and implementation
*    MORVAN Antoine - initial API and implementation
*    NAULLET Maxime - initial API and implementation
*******************************************************************************/
package fr.irisa.cairn.gecos.model.scop.analysis;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.*;


import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.*;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import java.util.ArrayList;
@SuppressWarnings({"all"})
public class ScopReductionPatternMatching {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_List(Object l1, Object l2) {
return  l1.equals(l2) ;
}
private static boolean tom_is_sort_List(Object t) {
return  t instanceof java.util.List ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_equal_term_org_polymodel_algebra_FuzzyBoolean(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_FuzzyBoolean(Object t) {
return t instanceof org.polymodel.algebra.FuzzyBoolean;
}
private static boolean tom_equal_term_org_polymodel_algebra_OUTPUT_FORMAT(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_OUTPUT_FORMAT(Object t) {
return t instanceof org.polymodel.algebra.OUTPUT_FORMAT;
}
private static boolean tom_equal_term_org_polymodel_algebra_Value(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_Value(Object t) {
return true;
}
private static boolean tom_equal_term_org_polymodel_algebra_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_ComparisonOperator(Object t) {
return t instanceof org.polymodel.algebra.ComparisonOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_CompositeOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_CompositeOperator(Object t) {
return t instanceof org.polymodel.algebra.CompositeOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_quasiAffine_QuasiAffineOperator(Object t) {
return t instanceof org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
}
private static boolean tom_equal_term_org_polymodel_algebra_reductions_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_org_polymodel_algebra_reductions_ReductionOperator(Object t) {
return t instanceof org.polymodel.algebra.reductions.ReductionOperator;
}
private static boolean tom_equal_term_ICS(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICS(Object t) {
return t instanceof org.polymodel.algebra.IntConstraintSystem;
}
private static boolean tom_equal_term_ICSL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_equal_term_E(Object l1, Object l2) {
return (l1!=null && l2 instanceof IntExpression && ((IntExpression)l1).isEquivalent((IntExpression)l2) == FuzzyBoolean.YES) || l1==l2;
}
private static boolean tom_is_sort_E(Object t) {
return t instanceof IntExpression;
}
private static boolean tom_equal_term_EL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_EL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntExpression>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntExpression>)t).size()>0 && ((EList<org.polymodel.algebra.IntExpression>)t).get(0) instanceof org.polymodel.algebra.IntExpression));
}
private static boolean tom_equal_term_V(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_V(Object t) {
return t instanceof org.polymodel.algebra.Variable;
}
private static boolean tom_equal_term_vars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_vars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.Variable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.Variable>)t).size()>0 && ((EList<org.polymodel.algebra.Variable>)t).get(0) instanceof org.polymodel.algebra.Variable));
}
private static boolean tom_equal_term_T(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_T(Object t) {
return t instanceof org.polymodel.algebra.IntTerm;
}
private static boolean tom_equal_term_terms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_terms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static boolean tom_is_fun_sym_terms( EList<org.polymodel.algebra.IntTerm>  t) {
return  t instanceof EList<?> &&
 		(((EList<org.polymodel.algebra.IntTerm>)t).size() == 0 
 		|| (((EList<org.polymodel.algebra.IntTerm>)t).size()>0 && ((EList<org.polymodel.algebra.IntTerm>)t).get(0) instanceof org.polymodel.algebra.IntTerm));
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_empty_array_terms(int n) { 
return  new BasicEList<org.polymodel.algebra.IntTerm>(n) ;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_cons_array_terms(org.polymodel.algebra.IntTerm e,  EList<org.polymodel.algebra.IntTerm>  l) { 
return  append(e,l) ;
}
private static org.polymodel.algebra.IntTerm tom_get_element_terms_terms( EList<org.polymodel.algebra.IntTerm>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_terms_terms( EList<org.polymodel.algebra.IntTerm>  l) {
return  l.size() ;
}

  private static   EList<org.polymodel.algebra.IntTerm>  tom_get_slice_terms( EList<org.polymodel.algebra.IntTerm>  subject, int begin, int end) {
     EList<org.polymodel.algebra.IntTerm>  result =  new BasicEList<org.polymodel.algebra.IntTerm>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<org.polymodel.algebra.IntTerm>  tom_append_array_terms( EList<org.polymodel.algebra.IntTerm>  l2,  EList<org.polymodel.algebra.IntTerm>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<org.polymodel.algebra.IntTerm>  result =  new BasicEList<org.polymodel.algebra.IntTerm>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_C(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_C(Object t) {
return t instanceof org.polymodel.algebra.IntConstraint;
}
private static boolean tom_equal_term_CL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_CL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraint>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraint>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraint>)t).get(0) instanceof org.polymodel.algebra.IntConstraint));
}
private static boolean tom_equal_term_pterm(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterm(Object t) {
return t instanceof org.polymodel.algebra.polynomials.PolynomialTerm;
}
private static boolean tom_equal_term_pterms(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pterms(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialTerm>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialTerm));
}
private static boolean tom_equal_term_pvar(Object l1, Object l2) {
return (l1!=null && l2 instanceof PolynomialVariable && ((PolynomialVariable)l1).isEquivalent((PolynomialVariable)l2)) || l1==l2;
}
private static boolean tom_is_sort_pvar(Object t) {
return t instanceof PolynomialVariable;
}
private static boolean tom_equal_term_pvars(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_pvars(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).size()>0 && ((EList<org.polymodel.algebra.polynomials.PolynomialVariable>)t).get(0) instanceof org.polymodel.algebra.polynomials.PolynomialVariable));
}
private static boolean tom_is_fun_sym_affine(IntExpression t) {
return t instanceof org.polymodel.algebra.affine.AffineExpression;
}
private static  EList<org.polymodel.algebra.IntTerm>  tom_get_slot_affine_terms(IntExpression t) {
return enforce(((org.polymodel.algebra.affine.AffineExpression)t).getTerms());
}
private static boolean tom_is_fun_sym_constant(org.polymodel.algebra.IntTerm t) {
return t instanceof org.polymodel.algebra.affine.AffineTerm && ((org.polymodel.algebra.affine.AffineTerm)t).getVariable() == null;
}
private static  long  tom_get_slot_constant_coef(org.polymodel.algebra.IntTerm t) {
return ((org.polymodel.algebra.affine.AffineTerm)t).getCoef();
}

//	%include { algebra_quasiaffine.tom }

private static boolean tom_equal_term_nodes(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_nodes(Object t) {
return  t instanceof EList<?> &&
    	(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
    	|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static boolean tom_is_fun_sym_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  t) {
return  t instanceof EList<?> &&
 		(((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size() == 0 
 		|| (((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).size()>0 && ((EList<fr.irisa.cairn.gecos.model.scop.ScopNode>)t).get(0) instanceof fr.irisa.cairn.gecos.model.scop.ScopNode));
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_empty_array_nodes(int n) { 
return  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(n) ;
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_cons_array_nodes(fr.irisa.cairn.gecos.model.scop.ScopNode e,  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l) { 
return  append(e,l) ;
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_get_element_nodes_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_nodes_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l) {
return  l.size() ;
}

  private static   EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_get_slice_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  subject, int begin, int end) {
     EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  result =  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_append_array_nodes( EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l2,  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  result =  new BasicEList<fr.irisa.cairn.gecos.model.scop.ScopNode>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_node(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopNode;
}
private static boolean tom_equal_term_SymList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<gecos.core.Symbol>)t).size() == 0 
    	|| (((EList<gecos.core.Symbol>)t).size()>0 && ((EList<gecos.core.Symbol>)t).get(0) instanceof gecos.core.Symbol));
}
private static boolean tom_equal_term_ICSList(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_ICSList(Object t) {
return  t instanceof EList<?> &&
    	(((EList<org.polymodel.algebra.IntConstraintSystem>)t).size() == 0 
    	|| (((EList<org.polymodel.algebra.IntConstraintSystem>)t).size()>0 && ((EList<org.polymodel.algebra.IntConstraintSystem>)t).get(0) instanceof org.polymodel.algebra.IntConstraintSystem));
}
private static boolean tom_is_fun_sym_symbol(Symbol t) {
return t instanceof Symbol;
}
private static  String  tom_get_slot_symbol_name(Symbol t) {
return ((Symbol)t).getName();
}
private static boolean tom_is_fun_sym_set(Instruction t) {
return t instanceof SetInstruction;
}
private static Instruction tom_get_slot_set_dest(Instruction t) {
return ((SetInstruction)t).getDest();
}
private static Instruction tom_get_slot_set_source(Instruction t) {
return ((SetInstruction)t).getSource();
}
private static boolean tom_is_fun_sym_add(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("add");
}
private static  EList<Instruction>  tom_get_slot_add_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}


//%include { gecos_basic.tom } 

private static boolean tom_is_fun_sym_sblk(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopBlock;
}
private static  EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tom_get_slot_sblk_children(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopBlock)t).getChildren());
}
private static boolean tom_is_fun_sym_loop(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopForLoop;
}
private static org.polymodel.algebra.Variable tom_get_slot_loop_iterator(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopForLoop)t).getIterator();
}
private static IntExpression tom_get_slot_loop_LB(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopForLoop)t).getLB();
}
private static IntExpression tom_get_slot_loop_UB(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopForLoop)t).getUB();
}
private static IntExpression tom_get_slot_loop_stride(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopForLoop)t).getStride();
}
private static fr.irisa.cairn.gecos.model.scop.ScopNode tom_get_slot_loop_body(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopForLoop)t).getBody();
}
private static boolean tom_is_fun_sym_stmt(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
}
private static  String  tom_get_slot_stmt_name(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement)t).getName();
}
private static  EList<Instruction>  tom_get_slot_stmt_children(fr.irisa.cairn.gecos.model.scop.ScopNode t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement)t).getChildren());
}
private static boolean tom_is_fun_sym_iread(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopRead;
}
private static Symbol tom_get_slot_iread_sym(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopRead)t).getSym();
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_iread_indexExpressions(Instruction t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopRead)t).getIndexExpressions());
}
private static boolean tom_is_fun_sym_iwrite(Instruction t) {
return t instanceof fr.irisa.cairn.gecos.model.scop.ScopWrite;
}
private static Symbol tom_get_slot_iwrite_sym(Instruction t) {
return ((fr.irisa.cairn.gecos.model.scop.ScopWrite)t).getSym();
}
private static  EList<org.polymodel.algebra.IntExpression>  tom_get_slot_iwrite_indexExpressions(Instruction t) {
return enforce(((fr.irisa.cairn.gecos.model.scop.ScopWrite)t).getIndexExpressions());
}


/*	public static boolean isReduction(Instruction i) {
%match (i){ 
//			add(InstL(_*,iread(sym,wrindex),_*)) -> {return true;}
//		}
return false;
}  
*/

protected ScopStatement stmt; 
protected List<Instruction> values = new ArrayList<Instruction>(); 
protected Symbol sym;
protected ScopForLoop loop;
protected List<IntExpression> index;

public ScopReductionPatternMatching(ScopNode node) {

{
{
if (tom_is_sort_node(node)) {
if (tom_is_sort_node(((fr.irisa.cairn.gecos.model.scop.ScopNode)node))) {
if (tom_is_fun_sym_loop(((fr.irisa.cairn.gecos.model.scop.ScopNode)((fr.irisa.cairn.gecos.model.scop.ScopNode)node)))) {
IntExpression tomMatch1_4=tom_get_slot_loop_stride(((fr.irisa.cairn.gecos.model.scop.ScopNode)node));
fr.irisa.cairn.gecos.model.scop.ScopNode tomMatch1_5=tom_get_slot_loop_body(((fr.irisa.cairn.gecos.model.scop.ScopNode)node));
if (tom_is_sort_E(tomMatch1_4)) {
if (tom_is_fun_sym_affine(((IntExpression)tomMatch1_4))) {
 EList<org.polymodel.algebra.IntTerm>  tomMatch1_8=tom_get_slot_affine_terms(tomMatch1_4);
if (tom_is_fun_sym_terms((( EList<org.polymodel.algebra.IntTerm> )tomMatch1_8))) {
int tomMatch1_15=0;
if (!(tomMatch1_15 >= tom_get_size_terms_terms(tomMatch1_8))) {
org.polymodel.algebra.IntTerm tomMatch1_25=tom_get_element_terms_terms(tomMatch1_8,tomMatch1_15);
if (tom_is_sort_T(tomMatch1_25)) {
if (tom_is_fun_sym_constant(((org.polymodel.algebra.IntTerm)tomMatch1_25))) {
if (tomMatch1_15 + 1 >= tom_get_size_terms_terms(tomMatch1_8)) {
if (tom_is_sort_node(tomMatch1_5)) {
if (tom_is_fun_sym_sblk(((fr.irisa.cairn.gecos.model.scop.ScopNode)tomMatch1_5))) {
 EList<fr.irisa.cairn.gecos.model.scop.ScopNode>  tomMatch1_11=tom_get_slot_sblk_children(tomMatch1_5);
if (tom_is_fun_sym_nodes((( EList<fr.irisa.cairn.gecos.model.scop.ScopNode> )tomMatch1_11))) {
int tomMatch1_end_21=0;
do {
{
if (!(tomMatch1_end_21 >= tom_get_size_nodes_nodes(tomMatch1_11))) {
fr.irisa.cairn.gecos.model.scop.ScopNode tomMatch1_29=tom_get_element_nodes_nodes(tomMatch1_11,tomMatch1_end_21);
if (tom_is_sort_node(tomMatch1_29)) {
if (tom_is_fun_sym_stmt(((fr.irisa.cairn.gecos.model.scop.ScopNode)tomMatch1_29))) {
 EList<Instruction>  tomMatch1_28=tom_get_slot_stmt_children(tomMatch1_29);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_28))) {
int tomMatch1_32=0;
if (!(tomMatch1_32 >= tom_get_size_InstL_InstL(tomMatch1_28))) {
Instruction tomMatch1_36=tom_get_element_InstL_InstL(tomMatch1_28,tomMatch1_32);
if (tom_is_sort_Inst(tomMatch1_36)) {
if (tom_is_fun_sym_set(((Instruction)tomMatch1_36))) {
Instruction tomMatch1_34=tom_get_slot_set_dest(tomMatch1_36);
Instruction tomMatch1_35=tom_get_slot_set_source(tomMatch1_36);
if (tom_is_sort_Inst(tomMatch1_34)) {
if (tom_is_fun_sym_iwrite(((Instruction)tomMatch1_34))) {
Symbol tomMatch1_38=tom_get_slot_iwrite_sym(tomMatch1_34);
 EList<org.polymodel.algebra.IntExpression>  tomMatch1_39=tom_get_slot_iwrite_indexExpressions(tomMatch1_34);
if (tom_is_sort_Sym(tomMatch1_38)) {
if (tom_is_fun_sym_symbol(((Symbol)tomMatch1_38))) {
if (tom_is_sort_Inst(tomMatch1_35)) {
if (tom_is_fun_sym_add(((Instruction)tomMatch1_35))) {
 EList<Instruction>  tomMatch1_45=tom_get_slot_add_children(tomMatch1_35);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_45))) {
int tomMatch1_49=0;
int tomMatch1_end_52=tomMatch1_49;
do {
{
if (!(tomMatch1_end_52 >= tom_get_size_InstL_InstL(tomMatch1_45))) {
Instruction tomMatch1_59=tom_get_element_InstL_InstL(tomMatch1_45,tomMatch1_end_52);
if (tom_is_sort_Inst(tomMatch1_59)) {
if (tom_is_fun_sym_iread(((Instruction)tomMatch1_59))) {
Symbol tomMatch1_57=tom_get_slot_iread_sym(tomMatch1_59);
if (tom_is_sort_Sym(tomMatch1_57)) {
if (tom_is_fun_sym_symbol(((Symbol)tomMatch1_57))) {
if (tom_equal_term_String(tom_get_slot_symbol_name(tomMatch1_38), tom_get_slot_symbol_name(tomMatch1_57))) {
if (tom_equal_term_EL(tomMatch1_39, tom_get_slot_iread_indexExpressions(tomMatch1_59))) {
if (tomMatch1_32 + 1 >= tom_get_size_InstL_InstL(tomMatch1_28)) {

sym=(
tomMatch1_38);
index=(
tomMatch1_39);
stmt=(ScopStatement)(
tom_get_element_nodes_nodes(tomMatch1_11,tomMatch1_end_21));
for (Instruction v: (
tom_get_slice_InstL(tomMatch1_45,tomMatch1_49,tomMatch1_end_52))) {values.add(v.copy());}
for (Instruction v: (
tom_get_slice_InstL(tomMatch1_45,tomMatch1_end_52 + 1,tom_get_size_InstL_InstL(tomMatch1_45)))) {values.add(v.copy());}

}
}
}
}
}
}
}
}
tomMatch1_end_52=tomMatch1_end_52 + 1;
}
} while(!(tomMatch1_end_52 > tom_get_size_InstL_InstL(tomMatch1_45)));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
tomMatch1_end_21=tomMatch1_end_21 + 1;
}
} while(!(tomMatch1_end_21 > tom_get_size_nodes_nodes(tomMatch1_11)));
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

}  


}
