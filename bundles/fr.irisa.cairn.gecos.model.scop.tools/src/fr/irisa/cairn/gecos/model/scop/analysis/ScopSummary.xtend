package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject

public class ScopSummary {

	GecosProject proj;
	
	new(GecosProject p) {
		proj = p;	
	}

	def public compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(proj, typeof(GecosScopBlock));
		for (scop : scops) {
			summarize(scop);
		}	
	}
	
	public static def String summarize(GecosScopBlock scop) {
		println('''
		Scop «scop.number» «scop.parameters»{
			iterators : «scop.iterators»
			live-in : «scop.liveInSymbols»
			live-out : «scop.liveOutSymbols»
			«FOR stmt: scop.listAllStatements» 
			statement «stmt.id» {
				domain : «ScopISLConverter.getDomain(stmt,scop)»	
				schedule : «ScopISLConverter.getIdSchedule(stmt,scop)»	
				instruction : «stmt» {
					writes : «stmt.listAllWriteAccess»
					reads : : «stmt.listAllReadAccess»
				}	
			}
			«ENDFOR»
		}
		'''
		)
	}
	
	
}
