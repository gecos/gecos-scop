package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import java.util.HashSet
import java.util.List
import fr.irisa.cairn.gecos.model.scop.ScopNode

class ScopTransformQuery {
  
	public static def JNIISLUnionMap buildISLSchedule(ScopNode  block) {
		val ScopTransform t = block.transform
		if(t===null) {
			ScopISLConverter.getAllIdSchedules(block)				
		} else {
			val List<ScopStatement> stmts = block.listAllStatements
			val scheds = t.listAllScheduleDirectives
			if(scheds.size==0) {
				ScopISLConverter.getAllIdSchedules(block)				
			} else if(scheds.size==stmts.size) {
				var HashSet<String> map = new HashSet<String>();
				for (s : stmts) map.add(s.id)
				var JNIISLUnionMap umap = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(),"{}" );
				for (s : scheds) {
					if (!map.contains(s.statement.id)) {
						throw new UnsupportedOperationException("No schedule provided for statement "+s)				
					}
					umap =umap.addMap(JNIISLMap.buildFromString(s.schedule))
				}
				return umap 
			}
		}
		
	}

}