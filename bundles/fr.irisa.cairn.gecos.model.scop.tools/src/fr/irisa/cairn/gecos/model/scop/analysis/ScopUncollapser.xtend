package fr.irisa.cairn.gecos.model.scop.analysis

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.codegen.ScopISLCodegen

class ScopUncollapser extends ScopISLCodegen{
	
	
	new(GecosProject p) {
		super(p)
	}

	override compute() {
		val scops = EMFUtils.eAllContentsFirstInstancesOf(proj,typeof(GecosScopBlock))	
		for (scop : scops) collapse(scop)
	}
	
	def dispatch ScopUnexpandedStatement uncollapseStmt(ScopStatement stmt, ScopNode n) {
	}
	
	def dispatch ScopUnexpandedStatement uncollapseStmt(ScopUnexpandedStatement stmt, ScopNode n) {
		
	}
   
	def collapse(ScopNode n) {
		val stmts = n.listAllStatements
		var res = ScopUserFactory.scopBlock()
		Substitute.replace(n,res);
		for(s:stmts) {
			res.children+=uncollapseStmt(s,n)
		}
	}
}