package fr.irisa.cairn.gecos.model.scop.analysis

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopGuard

public class Substitute {
	
	static def replace(ScopNode old, ScopNode newnode) {
		if (old.parentScop!==null)
			replace(old, old.parentScop, newnode)
		else
			throw new UnsupportedOperationException("Cannot replace in root ")
	}
	
	static def dispatch replace(ScopNode old,ScopBlock parent, ScopNode newnode) {
		val index =parent.children.indexOf(old) 
		parent.children.set(index,newnode)
	}
	
	static def dispatch replace(ScopNode old,ScopGuard parent, ScopNode newnode) {
		if (old===parent.thenNode) {
			parent.thenNode=newnode
		} else if (old===parent.elseNode) {
			parent.thenNode=newnode
		} else {
			throw new UnsupportedOperationException("Node "+old+" not in parentScope")
			
		}
	}
	static def dispatch replace(ScopNode old,ScopForLoop parent, ScopNode newnode) {
		if (old===parent.body) {
			parent.body=newnode
		} else {
			throw new UnsupportedOperationException("Node "+old+" not in parentScope")
		}
	}
	
	static def dispatch replace(ScopNode old,GecosScopBlock parent, ScopNode newnode) {
		if (old===parent.root) {
			parent.root=newnode
		} else {
			throw new UnsupportedOperationException("Node "+old+" not in parentScope")
		}
	}
	
}