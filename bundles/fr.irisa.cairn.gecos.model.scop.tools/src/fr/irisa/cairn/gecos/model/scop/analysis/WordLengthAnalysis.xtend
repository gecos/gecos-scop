package fr.irisa.cairn.gecos.model.scop.analysis

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import org.polymodel.algebra.IntExpression
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import java.util.List
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import org.eclipse.emf.common.util.BasicEList

class WordLengthAnalysis {

	GecosProject p;
	
	boolean DEBUG=true
	boolean VERBOSE=true
	
	def debug(String string) {
		if(DEBUG) {
			println("[WordLengthAnalyzer] "+string)
		}
	}

	def log(String string) {
		if(VERBOSE) {
			println("[WordLengthAnalyzer] "+string)
		}
	}

	new(GecosProject p) {
		this.p=p
		
	}

	def dispatch List<IntExpression> collectExpressions(ScopNode node) {
		val res =  new BasicEList<IntExpression>();
		res
	}
	def dispatch List<IntExpression> collectExpressions(GecosScopBlock node) {
		val res =  new BasicEList<IntExpression>();
		res+=collectExpressions(node.root)
		res
	}

	def dispatch List<IntExpression> collectExpressions(ScopForLoop node) {
		val res =  new BasicEList<IntExpression>();
		res+=node.LB
		res+=node.UB
		res+=collectExpressions(node.body)
		res
	}

	def dispatch List<IntExpression> collectExpressions(ScopBlock node) {
		val res =  new BasicEList<IntExpression>();
		for(c:node.children) res+=collectExpressions(c)
		res
	}

	def dispatch List<IntExpression> collectExpressions(ScopGuard node) {
		val res =  new BasicEList<IntExpression>();
		if (node.thenNode!==null) res+=collectExpressions(node.thenNode)
		if (node.elseNode!==null) res+=collectExpressions(node.thenNode)
		for(c:node.predicate) res+=EMFUtils.eAllContentsInstancesOf(c,IntExpression)
		res
	}

	def dispatch List<IntExpression> collectExpressions(ScopInstructionStatement node) {
		return EMFUtils.eAllContentsInstancesOf(node,IntExpression)
	}
	
	def void compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(p,GecosScopBlock)
		for (scop:scops) {
			val exprs = collectExpressions(scop)
			for (e:exprs) {
				var node = getContainingScopNode(e)
				switch(node) {
					ScopForLoop:{node=node.body}
				}				
				val domain = ScopISLConverter.getDomain(node);
				
				val str = '''«node.listRootParameters»->{ «domain.tupleName»«node.listAllEnclosingIterators.map[i|i.name]»->[e] : e=«e»}'''
				val function = JNIISLMap.buildFromString(str)
				var map = function.intersectDomain(domain);
				var range = map.getRange
				range=range.projectOut(JNIISLDimType.isl_dim_param, 0, map.nbParams)
				log(e+"->["+range.copy.lexMin+":"+range.copy.lexMax+"]")
			}
		}
	
	}
	
	def ScopNode getContainingScopNode(IntExpression expression) {
		var EObject o= expression;
		while(o!==null && !(o instanceof ScopNode)) {
			o=o.eContainer
		}
		if (o!==null) 
			return o as ScopNode
		else
			null
	}
	
}
