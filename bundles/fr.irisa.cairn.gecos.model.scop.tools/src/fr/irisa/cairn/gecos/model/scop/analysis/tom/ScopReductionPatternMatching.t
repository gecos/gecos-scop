/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.scop.analysis;
  
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.*;


import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.*;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import java.util.ArrayList;
@SuppressWarnings({"all"})
public class ScopReductionPatternMatching {

	%include { sl.tom } 

	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }  
	%include { gecos_terminals.tom }

	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }
//	%include { algebra_quasiaffine.tom }
	%include { algebra_domains.tom }
	%include { scop_terminals.tom } 
	  
	%include { gecos_core.tom }
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_control.tom }
	%include { gecos_logical.tom }
	%include { gecos_arithmetic.tom }
  
	//%include { gecos_basic.tom } 
	%include { scop_nodes.tom }     
	%include { extractor_extraction.tom }  

/*	public static boolean isReduction(Instruction i) {
		%match (i){ 
//			add(InstL(_*,iread(sym,wrindex),_*)) -> {return true;}
//		}
		return false;
	}  
*/
	
	protected ScopStatement stmt; 
	protected List<Instruction> values = new ArrayList<Instruction>(); 
	protected Symbol sym;
	protected ScopForLoop loop;
	protected List<IntExpression> index;

	public ScopReductionPatternMatching(ScopNode node) {
		%match (node){ 
			loop(iterator, lb, ub, affine(terms(constant(stride))), sblk(
				nodes(  
					_*,
					s@stmt(name, InstL(
						set(w@iwrite(sym@symbol(sn),idx@index),
								add(InstL(v0@_*,iread(symbol(sn),index),v1@_*)
 	 							))) 
					),
					_*
				))) -> { 
					sym=(`sym);
					index=(`idx);
					stmt=(ScopStatement)(`s);
					for (Instruction v: (`v0)) {values.add(v.copy());}
					for (Instruction v: (`v1)) {values.add(v.copy());}
				}
		}
	}  


}