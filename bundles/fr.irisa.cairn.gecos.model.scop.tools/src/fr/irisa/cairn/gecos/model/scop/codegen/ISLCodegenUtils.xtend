package fr.irisa.cairn.gecos.model.scop.codegen

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import gecos.core.Symbol
import java.util.List
import org.polymodel.algebra.factory.IntExpressionBuilder
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead
import static  fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory.*
import static extension fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter.*
import static extension fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter.*
import fr.irisa.cairn.gecos.model.scop.ScopRead

class ISLCodegenUtils {

	def static encloseWithLoop(ScopNode node, JNIISLSet set, List<ScopDimension> iterators) {
		if (set.tupleName === null)
			throw new UnsupportedOperationException("Domain "+set+" must be named");
		
		var schedule = JNIISLUnionMap.buildEmpty(set.copy.identity.space)
		schedule = schedule.addMap(set.copy.identity)
		
		val loopUnexpanded = ScopUserFactory.unexpandedStmt(iterators, set.toString, schedule.toString)
		loopUnexpanded.name = set.tupleName
		node.parentScop.replace(node, loopUnexpanded)		
		
		val loop = JNIISLASTToScopConverter.adapt(loopUnexpanded);
		for (s : loop.listAllStatements) {
			s.parentScop.replace(s, node.copy)
		}
		
		loop
	}
	
	def static expand(ScopRegionRead read, List<ScopDimension> iterators) {
		val set = read.accessMap.moveInputDimsAsParameters().toSet
		var schedule = JNIISLUnionMap.buildEmpty(set.copy.identity.space)
		schedule = schedule.addMap(set.copy.identity)
		
		val loopUnexpanded = ScopUserFactory.unexpandedStmt(iterators, set.toString, schedule.toString)
		loopUnexpanded.name = set.tupleName

	//		node.parentScop.replace(node, loopUnexpanded)		
		
		val loop = JNIISLASTToScopConverter.adapt(loopUnexpanded);
		for (s : loop.listAllStatements) {
			print(s)
		}
//		read.parent.replaceChild(read.generic("vector", ScopReadsopce)))
//		loop
	}
	
	def static ScopNode dataMover(JNIISLSet domain, ScopBlock scopContainer, Symbol src, Symbol dst) {
		if (domain.tupleName.length==0)
			throw new UnsupportedOperationException("Domain "+domain+" must be named");

		val symIndices = ScopIteratorGenerator.freeIterators(scopContainer, domain.nbDims).toList

		val dstInstr = ScopUserFactory.scopWrite(dst, symIndices.map[i| IntExpressionBuilder.affine(i)])	
		val srcInstr = ScopUserFactory.scopRead(src, symIndices.map[i| IntExpressionBuilder.affine(i)])	
		val setInstr  = GecosUserInstructionFactory.set(dstInstr, srcInstr)

		var schedule = JNIISLUnionMap.buildEmpty(domain.copy.identity.space)
		schedule = schedule.addMap(domain.copy.identity)
		 
		val stmt = ScopUserFactory.unexpandedStmt(symIndices, domain.toString, schedule.toString)
		stmt.children.add(setInstr)
		stmt.name = domain.tupleName
		scopContainer.children.add(stmt)
		
		val res = JNIISLASTToScopConverter.adapt(stmt);
		println(res);	
		res
	}
}