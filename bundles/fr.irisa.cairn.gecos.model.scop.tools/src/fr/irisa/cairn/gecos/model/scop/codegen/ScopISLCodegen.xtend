package fr.irisa.cairn.gecos.model.scop.codegen

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.analysis.ScopTransformQuery
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject

import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.*
import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter
import fr.irisa.cairn.jnimap.isl.jni.ISLNative
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap

class ScopISLCodegen {

	protected GecosProject proj;
	int depth =-1;
	 
	new(GecosProject project) {
		proj=project	
	}
	   
	new(GecosProject project, Integer d) {
		proj=project	
		depth=d
	}
	 
	def compute() {
		for (ps : proj.listProcedureSets ) {
			GecosUserTypeFactory.scope=ps.scope
			val scops = EMFUtils::eAllContentsInstancesOf(ps, typeof(GecosScopBlock))
			for (scop : scops) {
				val newRoot = codegen(scop,depth)
				var id=0;
				for (stmt : newRoot.listAllStatements) {
					stmt.id= "S"+ (id++)
				}
			}	
		}
	}
	
	def public static codegen(ScopNode block, int _depth) {
		var JNIISLUnionMap schedule = ScopTransformQuery.buildISLSchedule(block).copy
		val ctxt = ScopISLConverter.getContext(block)
		JNIISLASTBuild.setDetectMinMax(JNIISLContext.getCtx(),1);
		val JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(ctxt.copy);  
		var JNIISLASTNode root =null

		if(_depth>=0) {  
			var JNIISLUnionMap newSched= JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(),"{}" );
			for (map : schedule.maps) {
				val nbDims = map.getNbDims(JNIISLDimType.isl_dim_out)
				val projectedScheduling = map.projectOut(JNIISLDimType.isl_dim_out,_depth, nbDims-_depth)
				newSched = newSched.addMap(projectedScheduling)
			}
			root = build.generateWithExpansionNodes(newSched, "EX");
		} else {
			root = JNIISLASTNode.buildFromSchedule(build, schedule);
			//print(root);
		}
		val res= JNIISLASTToScopConverter.adapt(block, root);
		for (unExp : res.listAllStatements.filter(ScopUnexpandedStatement)) {
			val depth = unExp.listAllEnclosingIterators.size			
			val map = JNIISLMap.buildFromString(JNIISLContext.ctx,unExp.schedule)
			map.moveDims(JNIISLDimType.isl_dim_param,map.nbParams-1,JNIISLDimType.isl_dim_param,0,depth)
			unExp.schedule=map.toString
		}
		res		
		
	}
	
}
 