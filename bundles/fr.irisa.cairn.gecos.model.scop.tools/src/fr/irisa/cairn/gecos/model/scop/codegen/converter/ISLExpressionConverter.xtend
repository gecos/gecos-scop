package fr.irisa.cairn.gecos.model.scop.codegen.converter

import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTExpression
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIdentifier
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTLiteral
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation
import fr.irisa.cairn.jnimap.isl.jni.JNIISLIdentifier
import org.eclipse.emf.common.util.BasicEList
import org.polymodel.algebra.IntConstraint
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.Variable
import org.polymodel.algebra.affine.AffineExpression
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression
import org.polymodel.algebra.tom.ArithmeticOperations

import static fr.irisa.cairn.jnimap.isl.jni.JNIISLAstOpType.*
import org.eclipse.emf.common.util.EList
import java.util.ArrayList

interface ISLVariableConverter {
	def ScopDimension buildVariable(String identifier)
}

class ISLExpressionConverter {

	var ISLVariableConverter adapter ;
	
	new(ISLVariableConverter adapter ) {
		this.adapter=adapter
	}
	
	def ScopDimension getVariable(JNIISLIdentifier identifier) { 
		adapter.buildVariable(identifier.name)
	}


	def dispatch IntExpression buildDiv(IntExpression obj, JNIISLASTExpression e) {
		throw new RuntimeException("Cannot build Div("+obj+","+e+")")
	}
	def dispatch IntExpression buildDiv(AffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::div(obj,e.value))
	}
	def dispatch IntExpression buildDiv(QuasiAffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::div(obj,e.value))
	}
	
	def dispatch IntExpression buildFloord(IntExpression obj, JNIISLASTExpression e) {
		throw new RuntimeException("Cannot build Floord("+obj+","+e+")")
	}
	def dispatch IntExpression buildFloord(AffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::div(obj,e.value))
	}
	def dispatch IntExpression buildFloord(QuasiAffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::div(obj,e.value))
	}
	
	def dispatch IntExpression buildMod(IntExpression obj, JNIISLASTExpression e) {
		throw new RuntimeException("Cannot build Mod("+obj+","+e+")")
	}
	def dispatch IntExpression buildMod(AffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::mod(obj,e.value))
	}
	def dispatch IntExpression buildMod(QuasiAffineExpression obj, JNIISLASTLiteral e) {
		return IntExpressionBuilder::qaffine(IntExpressionBuilder::mod(obj,e.value))
	}
	
	
	def dispatch IntExpression buildExpr(JNIISLASTLiteral obj) {
		val value =obj.value;
		IntExpressionBuilder::affine(IntExpressionBuilder::term(value));
	}
	
	def dispatch IntExpression buildExpr(JNIISLASTIdentifier obj) {
		val Variable v = getVariable(obj.getID)
		val res = IntExpressionBuilder::affine(IntExpressionBuilder::term(v));
		if(res===null) {
			throw new RuntimeException("Cannot build IntExpression object from "+obj)
		}
		res
	}
	def dispatch IntExpression buildExpr(JNIISLASTOperation obj) {
		val nbArgs =obj.nbArgs
		
		val args = (0..(nbArgs-1)).map[i | obj.getArgument(i)]
		val exprs = args.map[arg|buildExpr(arg)]
		
		
		val opType = obj.getOpType()
		if(opType===null) {
			println(obj);
			throw new RuntimeException("Obvious NPE "+obj)
		}
		val res= switch(opType.getValue()) {
		
		case ISL_AST_OP_MINUS :
			ArithmeticOperations::scale((exprs.get(0)), -1)
		case ISL_AST_OP_ADD :
			if (exprs.size>2) {
//				ArithmeticOperations::sum(exprs.toList as List)
				throw new RuntimeException("TODO: add support for sum")
			} else {
				ArithmeticOperations::add((exprs.get(0)), (exprs.get(1)))
			}
			
		case ISL_AST_OP_SUB :
			ArithmeticOperations::sub((exprs.get(0)), (exprs.get(1)))
		case ISL_AST_OP_MUL:
			if(args.get(0) instanceof JNIISLASTLiteral) {
				ArithmeticOperations::scale((exprs.get(1)),(args.get(0) as JNIISLASTLiteral).value)
			} else if (args.get(1) instanceof JNIISLASTLiteral) {
				ArithmeticOperations::scale((exprs.get(0)),(args.get(1) as JNIISLASTLiteral).value)
			} else {
				throw new RuntimeException("Not supported");
			}
		case ISL_AST_OP_MIN:
			IntExpressionBuilder::min((exprs.get(0)),(exprs.get(1)))
		case ISL_AST_OP_MAX:
			IntExpressionBuilder::max((exprs.get(0)),(exprs.get(1)))
					
		case ISL_AST_OP_DIV: 		//exact division i.e the result is known to be integer 
			buildDiv((exprs.get(0)),args.get(1))
		case ISL_AST_OP_FDIV_Q : 	
			buildFloord((exprs.get(0)),args.get(1)) 
		case ISL_AST_OP_PDIV_Q :	//result of integer division used when op1 is known to be non-negative
			buildFloord((exprs.get(0)),args.get(1)) //TODO: make sure it's ok even when op2 is negative
		case ISL_AST_OP_PDIV_R :	//remainder of integer division used when p is known to be non-negative
			buildMod((exprs.get(0)), args.get(1))
		case ISL_AST_OP_ZDIV_R :	//remainder of integer division used when p is known to be non-negative
			buildMod((exprs.get(0)), args.get(1))
			
		default:
				throw new RuntimeException(obj.getOpType().name+" not supported")
			
		}
		res
	}
	
	def dispatch IntConstraint buildConstraint(JNIISLASTExpression obj) {
		throw new RuntimeException("Cannot build IntConstraint object from "+obj+":"+obj.class.simpleName )
	}
	def dispatch IntConstraint buildConstraint(JNIISLASTOperation obj) {
		val op1 = buildExpr(obj.getArgument(0))
		val op2 = buildExpr(obj.getArgument(1))
		if (op1===null || op2===null) throw new RuntimeException("Cannot build IntConstraint object from "+obj)
		
		var IntConstraint res = 
		switch(obj.getOpType().getValue()) {
			 case ISL_AST_OP_EQ :
				IntExpressionBuilder::eq((op1),(op2))
			 case ISL_AST_OP_LE :
				IntExpressionBuilder::le((op1),(op2))
			 case ISL_AST_OP_LT :
				IntExpressionBuilder::lt((op1),(op2))
			 case ISL_AST_OP_GE :
				IntExpressionBuilder::ge((op1),(op2))
			 case ISL_AST_OP_GT :
				IntExpressionBuilder::gt((op1),(op2))
			default:
				throw new RuntimeException("Cannot build IntConstraint object from "+obj)
		}
		return res
	}
	
	def EList<IntConstraintSystem> buildUnionOfConstraintSystem(JNIISLASTOperation obj) {
		val res = new BasicEList<IntConstraintSystem>();
		switch (obj.getOpType().getValue()) {
			case ISL_AST_OP_OR: {
				for (i : 0 .. obj.getNbArgs - 1) {
					res.addAll(buildUnionOfConstraintSystem(obj.getArgument(i) as JNIISLASTOperation));
				}				
			}
			default: {
				res.add(buildConstraintSystem(obj))
			}
		}
		res;
	}
	
	def dispatch IntConstraintSystem buildConstraintSystem(JNIISLASTExpression obj) {
		throw new RuntimeException("Cannot build IntConstraintSystem object from "+obj)
	}

	def dispatch IntConstraintSystem buildConstraintSystem(JNIISLASTOperation obj) {
		switch (obj.getOpType().getValue()) {
			case ISL_AST_OP_AND: {
				val constraintSystems = new ArrayList<IntConstraintSystem>()
				for (i : 0 .. obj.nbArgs - 1) {
					constraintSystems.add(buildConstraintSystem(obj.getArgument(i)))
				}
				val constraints = new ArrayList<IntConstraint>()
				constraintSystems.forEach[constraints.addAll(it.constraints)]
				return IntExpressionBuilder.constraintSystem(constraints)
			}
			case ISL_AST_OP_MIN: {
				return IntExpressionBuilder.constraintSystem(getArguments(obj))
			}
			case ISL_AST_OP_EQ,
			case ISL_AST_OP_LE,
			case ISL_AST_OP_LT,
			case ISL_AST_OP_GE,
			case ISL_AST_OP_GT: {
				return IntExpressionBuilder.constraintSystem(buildConstraint(obj));
			}
		}
		throw new RuntimeException("Cannot build IntConstraint object from " + obj)
	}

	def BasicEList<IntConstraint> getArguments(JNIISLASTOperation operation) {
		var res  = new BasicEList<IntConstraint>(); 
		for (i : 0..operation.nbArgs-1 ) {
			res.add(buildConstraint(operation.getArgument(i)))
		} 
		res
	}

	
	def ScopStatement buildStatement(JNIISLASTOperation obj) {
		var stmt =ScopFactory::eINSTANCE.createScopInstructionStatement
		val op1 = obj.getArgument(0)
		switch(obj.getOpType().getValue()) {
			 case ISL_AST_OP_CALL: {
 				stmt.id = (op1 as JNIISLASTIdentifier).getID.name
 				for(i : 0..obj.getNbArgs-1) {
 					stmt.children+=ScopUserFactory.sexpr(buildExpr(obj.getArgument(i)))
 				}
 				return stmt
 				}
		}
		throw new RuntimeException("Cannot build ScopStatementMacro object from "+obj)
	}
	

}
