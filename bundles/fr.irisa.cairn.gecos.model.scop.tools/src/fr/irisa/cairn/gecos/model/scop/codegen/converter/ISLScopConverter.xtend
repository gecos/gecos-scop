package fr.irisa.cairn.gecos.model.scop.codegen.converter

import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.jnimap.isl.jni.IISLASTNodeVisitor
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBlockNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTForNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIfNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNodeList
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUnscannedNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUserNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLAff
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLVal
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.EList
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.Variable
import org.polymodel.algebra.affine.AffineTerm
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression
import org.polymodel.algebra.quasiAffine.QuasiAffineFactory
import org.polymodel.algebra.quasiAffine.QuasiAffineOperator
import org.polymodel.algebra.quasiAffine.QuasiAffineTerm
import org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicSet
import javax.management.RuntimeErrorException
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils

class ISLScopConverter implements IISLASTNodeVisitor, ISLVariableConverter {
	protected Map<String, ScopDimension> varMap = new HashMap<String, ScopDimension>()
	protected ISLExpressionConverter exprConverter = new ISLExpressionConverter(this)

	def static List<IntConstraintSystem> convert(JNIISLSet set, List<ScopDimension> parms, List<ScopDimension> idxs) {
		return new ISLScopConverter()._convert(set, parms, idxs)
	}
	def static IntConstraintSystem convert(JNIISLBasicSet set, List<ScopDimension> parms, List<ScopDimension> idxs) {
		val res = convert(set.copy.toSet, parms, idxs)
		if (res.size==1) {
			res.get(0)
		} else {
			throw new RuntimeException("Basic set should be converted to a single IntConstraintSystem object, not "+res.size);
		}
		
	}

	// FIXME : untested
	def static guard(ScopNode node,JNIISLSet set) {
		
		val block = ScopUserFactory.scopBlock()
		node.parentScop.replace(node,block)
		block.children+=node;
		val idxs = node.listAllEnclosingIterators();
		val parms = node.listRootParameters();
		var domain = set
		for (i: (0..<idxs.size)) {
			domain = domain.setDimName(JNIISLDimType.isl_dim_set, i, idxs.get(i).symName)
		}
		val ics = new ISLScopConverter()._convert(domain, parms, idxs);
		val g = ScopUserFactory.scopGuard(ics, node, null);
		block.children+=g;
		g
	}

	def static List<IntExpression> convert(JNIISLBasicMap bmap, List<ScopDimension> parms, List<ScopDimension> idxs) {
		return new ISLScopConverter()._convert(bmap, parms, idxs)
	}

	/** 
	 * Builds a list of expressions from given isl_multi_affine. The expressions
	 * in the multi_aff should be function of given inputs/params.
	 * @param maff
	 * @param inputs
	 * @param params
	 * @return
	 */
	@Deprecated def static List<IntExpression> exprList(JNIISLMultiAff maff, List<? extends Variable> inputs,
		List<? extends Variable> params) {
		var List<IntExpression> list = new ArrayList<IntExpression>()
		for (JNIISLAff aff : maff.getAffs()) {
			var IntExpression expr = aff(aff, inputs, params).simplify()
			list.add(expr.simplify())
		}
		return list
	}

	def static QuasiAffineExpression aff(JNIISLAff aff, List<? extends Variable> variables, // List<? extends Variable> outputs,
	List<? extends Variable> parameters) {
		var QuasiAffineExpression res = QuasiAffineFactory.eINSTANCE.createQuasiAffineExpression()
		// 1. inspect divs
		var int nbDivs = aff.getNbDims(JNIISLDimType.isl_dim_div)
		for (var int i = 0; i < nbDivs; i++) {
			var long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_div, i).asLong()
			if (c !== 0) {
				var JNIISLAff child = aff.getDiv(i)
				var QuasiAffineTerm divTerm = div(child, variables, // outputs,
				parameters)
				// if coef != 1, embed term in an MUL QuasiAffineExpression
				if (c !== 1) {
					var QuasiAffineExpression expr = IntExpressionBuilder.qaffine(divTerm)
					var NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm()
					tmp.setCoef(c)
					tmp.setOperator(QuasiAffineOperator.MUL)
					tmp.setExpression(expr)
					divTerm = tmp
				}
				res.getTerms().add(divTerm)
			}
		}
		// 2.1. inspect variables "in"
		var int nbVarsIn = aff.getNbDims(JNIISLDimType.isl_dim_in)
		for (var int i = 0; i < nbVarsIn; i++) {
			var long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_in, i).asLong()
			if (c !== 0) {
				var Variable ^var = variables.get(i)
				var QuasiAffineTerm term = IntExpressionBuilder.mul(
					IntExpressionBuilder.affine(IntExpressionBuilder.term(c, ^var)), 1)
				res.getTerms().add(term)
			}
		}
		// 2.2. inspect variables "out"
		// int nbVarsOut = aff.getNbDim(JNIISLDimType.isl_dim_out);
		// for (int i = 0; i < nbVarsOut; i++) {
		// int c = aff.getCoefficientAt(JNIISLDimType.isl_dim_out, i);
		// if (c != 0) {
		// Variable var = outputs.get(i);
		// QuasiAffineTerm term = IntExpressionBuilder.mul(
		// IntExpressionBuilder.affine(IntExpressionBuilder.term(
		// c, var)), 1);
		// res.getTerms().add(term);
		// }
		// }
		// 3. inspect parameters
		var int nbParams = aff.getNbDims(JNIISLDimType.isl_dim_param)
		for (var int i = 0; i < nbParams; i++) {
			var long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_param, i).asLong()
			if (c !== 0) {
				var Variable ^var = parameters.get(i)
				var QuasiAffineTerm term = IntExpressionBuilder.mul(
					IntExpressionBuilder.affine(IntExpressionBuilder.term(c, ^var)), 1)
				res.getTerms().add(term)
			}
		}
		// 4. get constant
		var long cst = aff.getConstant()
		if (cst !== 0 || res.getTerms().size() === 0) {
			var QuasiAffineTerm cstTerm = IntExpressionBuilder.mul(
				IntExpressionBuilder.affine(IntExpressionBuilder.term(cst)), 1)
			res.getTerms().add(cstTerm)
		}
		// 5. simplify
		// res = res.simplify();
		return res
	}

	def private static long getValidCoefficient(JNIISLAff div, long denom, JNIISLVal cval) {
		var long c = 0
		if (cval.isRational() && (cval.getNumerator() !== 0)) {
			var long denominator = cval.getDenominator()
			if (denominator === denom) {
				c = cval.getNumerator()
			} else {
				if ((denom % denominator) === 0) {
					c = cval.getNumerator() * (denom / denominator)
				} else {
					throw new UnsupportedOperationException('''JNIISLAff «div» with several distinct denominator values''')
				}
			}
		} else {
			c = cval.asLong()
		}
		return c
	}

	def static QuasiAffineTerm div(JNIISLAff div, List<? extends Variable> variables, // List<? extends Variable> outputs,
	List<? extends Variable> parameters) {
		if (variables.size() === 0) {
			System.err.println("problem")
			System.err.println(div)
			System.err.println(variables)
			throw new RuntimeException('''«div»''')
		}
		var QuasiAffineTerm res
		var List<AffineTerm> list = new ArrayList<AffineTerm>()
		// 0. get denominator
		var long denom = div.getDenominator()
		// 1. get constant
		var long cst = div.getConstantVal().getNumerator()
		if (cst !== 0) {
			var AffineTerm cstTerm = IntExpressionBuilder.term(cst)
			list.add(cstTerm)
		}
		// 2.1. inspect variables "out"
		// int n = div.getNbDim(JNIISLDimType.isl_dim_out);
		// for (int i = 0; i < n; i++) {
		// int c = div.getCoefficientAt(JNIISLDimType.isl_dim_out, i);
		// if (c != 0) {
		// Variable var = outputs.get(i);
		// AffineTerm term = IntExpressionBuilder.term(c, var);
		// list.add(term);
		// }
		// }
		// 2.2. inspect variables "in"
		var int n = div.getNbDims(JNIISLDimType.isl_dim_in)
		for (var int i = 0; i < n; i++) {
			var JNIISLVal cval = div.getCoefficientVal(JNIISLDimType.isl_dim_in, i)
			var long c = getValidCoefficient(div, denom, cval)
			if (c !== 0) {
				var Variable ^var = variables.get(i)
				var AffineTerm term = IntExpressionBuilder.term(c, ^var)
				list.add(term)
			}
		}
		// 3. inspect parameters
		n = div.getNbDims(JNIISLDimType.isl_dim_param)
		for (var int i = 0; i < n; i++) {
			var JNIISLVal cval = div.getCoefficientVal(JNIISLDimType.isl_dim_param, i)
			var long c = getValidCoefficient(div, denom, cval)
			if (c !== 0) {
				var Variable ^var = parameters.get(i)
				var AffineTerm term = IntExpressionBuilder.term(c, ^var)
				list.add(term)
			}
		}
		// 4. inspect divs
		n = div.getNbDims(JNIISLDimType.isl_dim_div)
		var int nbDivs = n
		var List<QuasiAffineTerm> qlist = new ArrayList<QuasiAffineTerm>(n)
		for (var int i = 0; i < n; i++) {
			var long c = div.getCoefficientVal(JNIISLDimType.isl_dim_div, i).asLong()
			if (c !== 0) {
				var JNIISLAff child = div.copy().getDiv(i)
				var QuasiAffineTerm divTerm = div(child, variables, // outputs,
				parameters)
				// if coef != 1, embed term in an MUL QuasiAffineExpression
				if (c !== 1) {
					var QuasiAffineExpression expr = IntExpressionBuilder.qaffine(divTerm)
					var NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm()
					tmp.setCoef(c)
					tmp.setOperator(QuasiAffineOperator.MUL)
					tmp.setExpression(expr)
					divTerm = tmp
				}
				qlist.add(divTerm)
			} else {
				nbDivs--
			}
		}
		// 5. get denominator
		var long denum = div.getDenominator()
		// 6. build term depending on the number of divs
		if (nbDivs > 0) {
			var NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm()
			tmp.setOperator(QuasiAffineOperator.FLOOR)
			tmp.setCoef(denum)
			var QuasiAffineExpression expr = IntExpressionBuilder.qaffine(qlist)
			for (AffineTerm t : list) {
				expr.getTerms().add(IntExpressionBuilder.qterm(t))
			}
			tmp.setExpression(expr)
			res = tmp
		} else {
			var SimpleQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createSimpleQuasiAffineTerm()
			tmp.setOperator(QuasiAffineOperator.FLOOR)
			tmp.setCoef(denum)
			tmp.setExpression(IntExpressionBuilder.affine(list))
			res = tmp
		}
		return res
	}

	def List<IntExpression> _convert(JNIISLBasicMap bmap, List<ScopDimension> parms, List<ScopDimension> idxs) {
		varMap.clear()
		for (ScopDimension p : parms) {
			varMap.put(p.getName(), p)
		}
		for (ScopDimension i : idxs) {
			varMap.put(i.getName(), i)
		}
		if (!bmap.isSingleValued()) {
			throw new RuntimeException('''Cannot convert the non single valued relation «bmap» as IntExpression''')
		}
		if (!bmap.getDomain().isUniverse()) {
			throw new RuntimeException('''Cannot convert «bmap» as IntExpression since it has a domain, use projectOut before calling the conversion''')
		}
		var int nbOut = bmap.getNbDims(JNIISLDimType.isl_dim_param)
		for (var int i = 0; i < nbOut; i++) {
			var JNIISLBasicMap res = bmap.copy()
			if(i > 0) res = res.projectOut(JNIISLDimType.isl_dim_param, 0, i - 1)
			if(i < nbOut - 2) res = res.projectOut(JNIISLDimType.isl_dim_param, i + 1, nbOut - 1) // //val buildEquality = JNIISLConstraint.dimEquConstant(rev.space.copy,JNIISLDimType.isl_dim_param, 0, )
		}
		// map.copy().projectOut(JNIISLDimType.isl_dim_param, 0, nbOut-1)
		// map.copy().projectOut(JNIISLDimType.isl_dim_param, 0, nbOut-1)
		// JNIISLMap rev = map.copy().reverse();
		// rev.addDims(JNIISLDimType, n)getSpace().addDims(JNIISLDimType.isl_dim_param, 0);
		// rev= rev.computeDivs();
		// space.addDims(JNIISLDimType.isl_dim_param, 0);
		/*
		 * 
		 * P := [n,m] -> { S0[i] -> [i', j'] : 0< i < n and i'=i mod 2 and j'=i};
		 * codegen P^-1;
		 */
		return null
	}

	def List<IntConstraintSystem> _convert(JNIISLSet set, List<ScopDimension> parms, List<ScopDimension> idxs) {
		varMap.clear()
		for (ScopDimension p : parms) {
			varMap.put(p.getName(), p)
		}
		for (ScopDimension i : idxs) {
			varMap.put(i.getName(), i)
		}
		var int nbdims = set.getNbDims(JNIISLDimType.isl_dim_set)
		var JNIISLSet domain = set.copy()
		domain = set.copy().moveDims(JNIISLDimType.isl_dim_param, 0, JNIISLDimType.isl_dim_set, 0, nbdims)
		var JNIISLSpace space = domain.copy().getSpace()
		var int nParams = space.copy().getNbDims(JNIISLDimType.isl_dim_param)
		var JNIISLSpace allocParams = JNIISLSpace.allocParams(set.copy().getContext(), nParams)
		for (var int i = 0; i < domain.getNbParams(); i++) {
			var String paramName = domain.getDimName(JNIISLDimType.isl_dim_param, i)
			allocParams = allocParams.setName(JNIISLDimType.isl_dim_param, i, paramName)
		}
		var JNIISLSet ctxt = JNIISLSet.buildUniverse(allocParams.copy())
		var JNIISLUnionMap scheduleUMap = domain.copy().toUnionSet().identity()
		var JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(ctxt.copy())
		var JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, scheduleUMap.copy())
		//System.out.println(root)
		var List<IntConstraintSystem> res = (root.accept(this, null) as List)
		return res
	}

	override Object visitJNIISLASTBlockNode(JNIISLASTBlockNode obj, Object arg) {
		var JNIISLASTNodeList c = obj.getChildren()
		var ScopBlock res = ScopFactory::eINSTANCE.createScopBlock()
		for (var int i = 0; i < c.getNumberOfChildren(); i++) {
			var JNIISLASTNode child = c.getChildrenAt(i)
			var ScopNode scopNode = (child.accept(this, arg) as ScopNode)
			res.getChildren().add(scopNode)
		}
		return res
	}

	override Object visitJNIISLASTForNode(JNIISLASTForNode obj, Object arg) {
		throw new RuntimeException("ForBloop should not appear here")
	}

	override Object visitJNIISLASTIfNode(JNIISLASTIfNode obj, Object arg) {
		var JNIISLASTOperation cond = (obj.getCond() as JNIISLASTOperation)
		var EList<IntConstraintSystem> res = exprConverter.buildUnionOfConstraintSystem(cond)
		obj.getThen().accept(this, null)
		if (obj.hasElse() !== 0) {
			throw new RuntimeException("Else should not appear here")
		}
		return res
	}

	override Object visitJNIISLASTUserNode(JNIISLASTUserNode obj, Object arg) {
		// FIXME : collect scheduling expression
		var List<IntExpression> args = new ArrayList<IntExpression>()
		var JNIISLASTOperation r = (obj.getExpression() as JNIISLASTOperation)
		for (var int i = 1; i < r.getNbArgs(); i++) {
			args.add(exprConverter.buildExpr(r.getArgument(i)))
		}
		return null
	}
	
	override Object visitJNIISLASTUnscannedNode(JNIISLASTUnscannedNode obj, Object arg) {
		throw new UnsupportedOperationException('''NYI'''.toString)
	}
	
	override Object visitJNIISLASTNode(JNIISLASTNode obj, Object arg) {
		throw new UnsupportedOperationException('''visitJNIISLASTNode not yet Implemented for «obj»'''.toString)
	}
	
	override buildVariable(String id) {
		 if (varMap.containsKey(id)) {
		 	return varMap.get(id);
		 } else {
			throw new UnsupportedOperationException("Unknown dimension : "+id+ " not found in "+varMap.keySet)
		}
	}

}
