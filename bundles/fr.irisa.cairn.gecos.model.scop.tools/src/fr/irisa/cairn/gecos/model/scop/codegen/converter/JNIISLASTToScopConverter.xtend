package fr.irisa.cairn.gecos.model.scop.codegen.converter

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlockStatement
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBlockNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTExpression
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTForNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIdentifier
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIfNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUserNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import org.polymodel.algebra.IntExpression
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUnscannedNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType

class JNIISLASTToScopConverter implements ISLVariableConverter {
	val dimMap = new HashMap<String, ScopDimension>()
	val stmtMap = new HashMap<String, ScopNode>()
	val iteratorList = new ArrayList<ScopDimension>()
	var iteratorDepth = 0
	
	val exprConverter = new ISLExpressionConverter(this)
	val ScopNode relativeRoot
	
	static final boolean DEBUG =false;
	
	def  debug(Object mess) {
		if (DEBUG) println('''[«this.class.simpleName»] «mess»''')
	}
	static def dispatch adapt(ScopNode original, JNIISLASTNode ast) {
		new JNIISLASTToScopConverter(original).compute(ast)
	}

	static def dispatch adapt(ScopUnexpandedNode original, JNIISLASTNode ast) {
		new JNIISLASTToScopConverter(original, original.existentials).compute(ast)
	}
	
	static def adapt(ScopUnexpandedNode original) {
		val domain = JNIISLSet.buildFromString(original.domain)
		val allocParams = JNIISLSpace.allocParams(domain.copy.getContext(), 0)
		val context = JNIISLSet.buildUniverse(allocParams)
		val build = JNIISLASTBuild.buildFromContext(context)
		var schedule = JNIISLUnionMap.buildEmpty(domain.copy.identity.space)
		schedule = schedule.addMap(domain.copy.identity)
		val ast = JNIISLASTNode.buildFromSchedule(build, schedule)
		
		adapt(original, ast)
	}
	
	private new(ScopNode original) {
		relativeRoot = original
		original.listAllParameters.forEach[dimMap.put(it.symName, it)]
		original.listAllStatements.forEach[stmtMap.put(it.id, it)]
	}

	private new(ScopNode original, List<ScopDimension> iterators) {
		relativeRoot = original
		original.listAllParameters.forEach[dimMap.put(it.symName, it)]
		iterators.forEach[d|addNewIteratorToScheduledSpace(d)]
		original.listAllStatements.forEach[stmtMap.put(it.id, it)]
	}
		
	private def compute(JNIISLASTNode ast) {
		val newNode = visit(ast)
		if (relativeRoot.parentScop !== null) {
			relativeRoot.parentScop.replace(relativeRoot, newNode)
		} else {
			(relativeRoot as GecosScopBlock).root=newNode
		}
		newNode
	}
	
	def addNewIteratorToScheduledSpace(ScopDimension d) {
		iteratorList+=d
	}
	
	def getIteratorInScheduledSpace(int depth) {
		while (iteratorList.size<=depth) {
			iteratorList+=ScopIteratorGenerator.newIterator(relativeRoot)
		}
		iteratorList.get(depth)
	}
	private def dispatch ScopNode visit(JNIISLASTForNode forNode) {
		val iteratorId = getIdentifierId(forNode.iterator)
//		if (iteratorList.size == iteratorDepth)
//			iteratorList.add(ScopIteratorGenerator.newIterator(relativeRoot))
		
		val iterator = getIteratorInScheduledSpace(iteratorDepth)
		dimMap.put(iteratorId, iterator)
		
		val increment = exprConverter.buildExpr(forNode.inc)
		val ub = exprConverter.buildConstraint(forNode.cond).getUB(iterator)
		val lb = exprConverter.buildExpr(forNode.init)
		
		iteratorDepth++
		val body = visit(forNode.body)
		iteratorDepth--
		
		ScopUserFactory.scopFor(iterator, lb, ub, increment, body)
	}
	
	private def dispatch ScopNode visit(JNIISLASTIfNode ifNode) {
		val cond = exprConverter.buildUnionOfConstraintSystem(ifNode.cond as JNIISLASTOperation)
		val then = visit(ifNode.then)
		var ScopNode ^else = null
		if (ifNode.hasElse != 0)
			^else = visit(ifNode.^else)
		
		ScopUserFactory.scopGuard(cond, then, ^else)
	}
	
	private def dispatch ScopNode visit(JNIISLASTBlockNode blockNode) {
		val newBlock = ScopFactory.eINSTANCE.createScopBlock
		val children = blockNode.children
		for (i:0..<children.numberOfChildren)
			newBlock.children.add(visit(children.getChildrenAt(i)))
		
		newBlock
	}
	
	private def dispatch ScopNode visit(JNIISLASTUserNode userNode) {
		val op = (userNode.expression as JNIISLASTOperation)
		val stmt = stmtMap.get(getIdentifierId(op.getArgument(0)))
		buildStatement(stmt, userNode)
	}

	private def dispatch ScopNode visit(JNIISLASTUnscannedNode unscannedNode) {
		val schedule = unscannedNode.schedule
		val newBlock = ScopFactory.eINSTANCE.createScopBlock
		for (map : schedule.maps) {
			val stmt = stmtMap.get(map.inputTupleName) as ScopInstructionStatement
			val existentials = stmt.listAllEnclosingIterators
			println("stmt : "+stmt)
			val unexpStmt = ScopFactory.eINSTANCE.createScopUnexpandedStatement
			unexpStmt.id = map.inputTupleName
			unexpStmt.children+= (stmt ).copy.children
			unexpStmt.schedule = map.toString
			unexpStmt.existentials+=existentials
			newBlock.children+=unexpStmt
		}
		newBlock
	}
	

	private def dispatch buildStatement(ScopNode stmt, JNIISLASTUserNode userNode) {
		throw new UnsupportedOperationException(stmt+" is not a statement !")		
	}

	private def dispatch buildStatement(ScopBlockStatement stmt, JNIISLASTUserNode userNode) {
		throw new UnsupportedOperationException("[FIXME] ScopBlockStatement are not yet supported !")		
	}
	
	private def dispatch buildStatement(ScopInstructionStatement stmt, JNIISLASTUserNode userNode) {
	
		val op = (userNode.expression as JNIISLASTOperation)
		if (stmt===null) 
			throw new UnsupportedOperationException('''Statement «getIdentifierId(op.getArgument(0))» is not registered in «stmtMap.keySet»''')
	
		val surroundingIndices = stmt.listAllEnclosingIterators(relativeRoot)
		if (stmt instanceof ScopUnexpandedStatement)
			surroundingIndices.addAll(stmt.existentials)
		
		val nodeArgs = new ArrayList<IntExpression>()
		debug("AstNode "+op+" for "+stmt)
		debug("- ScopRoot iterators "+(stmt.scopRoot as GecosScopBlock).iterators)
		debug("- ScopNode enclosing iterators "+(stmt.listAllEnclosingIterators))
		debug("- Schedule space iterators "+iteratorList)
		debug("- Conversion map "+dimMap)
	
		for (i:1..<op.nbArgs) {
			val e = exprConverter.buildExpr(op.getArgument(i))
			debug("- "+ op.getArgument(i)+" converted to "+e)
			nodeArgs.add(e)
		}
		
		if (nodeArgs.size != surroundingIndices.size)
			throw new UnsupportedOperationException('''Inconsistency on Scop. The number of expected surrounding Iterators «surroundingIndices» does not match the number of arguments in the statement «userNode»''')
		
		var ScopNode newStmt
		if (stmt instanceof ScopUnexpandedStatement)
			newStmt = ScopUserFactory.scopStatement(stmt.name, stmt.children.map[it.copy])
		else
			newStmt = stmt.copy
		
		var offset = 0;
		for (dim : surroundingIndices) {
			
			val res= nodeArgs.get(offset++)
			debug(" Transforming "+dim+" into "+res)
			val exprList = EMFUtils.eAllContentsInstancesOf(newStmt, IntExpression)
			for (expr: exprList) {
				var IntExpression newExpr = expr.simplify()
				newExpr = newExpr.substitute(dim, res.copy)
				EMFUtils.substituteByNewObjectInContainer(expr, newExpr)
			}
		}
		println("\t- new Statement is "+newStmt)  
		newStmt
		
	}
	
	
	
	private def getIdentifierId(JNIISLASTExpression expr) {
		if (!(expr instanceof JNIISLASTIdentifier))
			throw new UnsupportedOperationException('''Cannot get identifier of «expr»''')
		(expr as JNIISLASTIdentifier).ID.name
	}
	
	override buildVariable(String identifier) {
		dimMap.get(identifier)
	}
	
}