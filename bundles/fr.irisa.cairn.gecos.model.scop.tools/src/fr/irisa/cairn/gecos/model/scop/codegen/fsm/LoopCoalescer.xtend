package fr.irisa.cairn.gecos.model.scop.codegen.fsm

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import org.eclipse.emf.common.util.BasicEList
import org.polymodel.algebra.IntExpression
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition
import org.eclipse.emf.common.util.EList
import org.polymodel.algebra.IntConstraintSystem
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import java.util.List
import java.util.Map
import java.util.Map.Entry
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.jnimap.isl.jni.codegen.BouletFeautrierScanning
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.*

class LoopCoalescer {
	
	ScopNode root; 
	ScopNode old;
	
	GecosProject p
	
	new(GecosProject p) {
		this.p=p
	}
	
	def compute() {
		val scops = EMFUtils.eAllContentsFirstInstancesOf(p,GecosScopBlock);
		for (s : scops) {
			coalesce(s.root)
		}
	}
	
	def dropDomainConstraints(JNIISLBasicMap m) {
		m.closedFormRelation	
	}
	
	def coalesce(ScopNode node) {
		old=node
		root=node.parentScop
		val scope = (root.scopRoot as GecosScopBlock).scope
		val parameters = root.listAllParameters
		val schedule = ScopISLConverter.getAllIdSchedules(old)
		if (schedule.copy.maps.size==0) {
			throw new UnsupportedOperationException("Empty schedule (ScopNode may be dead code ?)");
		}
		val schedDim = schedule.copy.maps.get(0).outputDimensionNames
		val intType = root.listRootIterators.get(0).symbol.type 
		val fsm = ScopUserFactory.scopFSMBlock
		for (name :schedDim) {
			val dim = ScopUserFactory.dimSym(GecosUserCoreFactory.symbol(name, intType, scope))
			(node.scopRoot as GecosScopBlock).iterators+=dim
			fsm.iterators+=dim
		}
		val scannedDomain =schedule.copy.getRange 
		val mergedScannedDomain =scannedDomain.copy.mergeAsSingleSet
		val next = BouletFeautrierScanning.buildNextMap(mergedScannedDomain.copy.toUnionSet);
		val first = mergedScannedDomain.copy.lexMin;
		println("Boulet & Feautrier ")
		if (first.nbBasicSets>1) 
			throw new UnsupportedOperationException("piecewise FSM initialisation not yet implemented")

		if (first.copy.toPWMultiAff.pieces.size>1) 
			throw new UnsupportedOperationException("Unexpected domain for initialization [piecewise FSM initialization not yet supported]")
		
		val p = first.copy.toPWMultiAff.pieces.get(0) 
		val exprs = ISLScopConverter.exprList(p.maff,fsm.iterators, parameters)
		fsm.start+=ScopUserFactory.scopFSMTransition(#[], exprs, null);
		
		// build transitions 
		val explicit = next.closedFormRelation
		for (set : explicit.keySet) { 
			val dom = ISLScopConverter.convert(set, fsm.iterators, parameters)
			val expList= ISLScopConverter.exprList(explicit.get(set), fsm.iterators, parameters)
			fsm.next+=ScopUserFactory.scopFSMTransition(dom, expList, null);
		}	
		
		for (stmt : node.listAllStatements) {
			val sched = ScopISLConverter.getIdSchedule(stmt,node)
			if (!sched.isEmpty) {
				val gistDomain = sched.getRange().copy.gist(mergedScannedDomain.copy)
				val constraints= ISLScopConverter.convert(gistDomain.copy, fsm.iterators, parameters)
				var newStmt = stmt.copy
				var offset = 0; 
				for (d: stmt.listAllEnclosingIterators(node).reverseView) {
					val newDim = fsm.iterators.get(2*offset+1)
					println("substitute "+d+" with "+newDim)
					newStmt.substitute(d, newDim)
					offset++
				}
				if (constraints!==null) {
					val guardNode = ScopUserFactory.scopGuard(constraints, newStmt,null)
					fsm.commands+=guardNode
				} else {
					fsm.commands+=newStmt
				}
			}
			
		}
		print(fsm)

		node.parentScop.replace(node,fsm);		
	}
	

	
	
}