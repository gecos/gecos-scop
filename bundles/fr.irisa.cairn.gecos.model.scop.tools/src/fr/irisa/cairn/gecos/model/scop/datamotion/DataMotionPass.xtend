package fr.irisa.cairn.gecos.model.scop.datamotion

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.r2d2.gecos.framework.GSModule
import gecos.gecosproject.GecosProject
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation

@GSModule("Create copies of symbols used in a block annotated with a scop_data_motion (symbol) pragma.")
class DataMotionPass {
	static boolean VERBOSE = false

	static def debug(String string) {
		if(VERBOSE) println(string)
	}
			
	var GecosProject proj 

	public new(GecosProject proj) {
		this.proj = proj
	}
	
	def compute() {
		debug("DataMotionPass")
		for (scop: EMFUtils.eAllContentsInstancesOf(proj, ScopNode)) {
			val commands = (scop.annotations?.get("GCS") as S2S4HLSPragmaAnnotation)?.directives?.filter(DataMotionPragma)
			if (commands !== null) {
				for (command: commands) {
					DataMotionTransformer.insertDataMotion(scop, command.symbols)
			    	debug("GecosScopBlock after copies\n" + scop)
				}				
			}
		}
	}
}