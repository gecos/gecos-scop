package fr.irisa.cairn.gecos.model.scop.datamotion

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.layout.ApplyMemoryLayout
import fr.irisa.cairn.gecos.model.scop.layout.FindArrayContractionCandidates
import fr.irisa.cairn.gecos.model.scop.layout.ScopArrayContract
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import gecos.core.Symbol
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import fr.irisa.cairn.gecos.model.scop.layout.InferSliceSlindingWindow

class DataMotionTransformer {

	static boolean VERBOSE = true
	
	static boolean CONTRACT = true

	static def debug(String string) {
		if(VERBOSE) println(string)
	}

	def static insertDataMotion(ScopNode node) {
		insertDataMotion(node, node.listAllReferencedSymbols)
	}

	def static insertDataMotion(ScopNode node, List<Symbol> symbolsToInspect) {
		debug("Move local data for node:\n" + node)
		val symbolsToCopy = symbolsToInspect.toSet
	
		// Inspect each symbol for needed input and output copies
		val inputCopies = new HashMap<Symbol,JNIISLSet>()
		val outputCopies = new HashMap<Symbol,JNIISLSet>()
		for (symbol: symbolsToCopy) {
			val flowIn = ScopAccessAnalyzer.computeFlowIn(symbol, node).coalesce.convexHull.toSet
			if (!flowIn.isEmpty) {
				debug("Flow In for " + symbol + " " + flowIn)
				inputCopies.put(symbol, flowIn)
			}
			
			val flowOut = ScopAccessAnalyzer.computeFlowOut(symbol, node).coalesce
			if (!flowOut.isEmpty) {
				debug("Flow Out for " + symbol + " " + flowOut)
				outputCopies.put(symbol, flowOut)
			}
		}
		
		// Create Copy blocks
		val containingBlock = ScopUserFactory.scopBlock()
		node.parentScop.replace(node, containingBlock)
		containingBlock.annotations.addAll(node.annotations)
		
		var ScopBlock inputBlock		
		if (!inputCopies.isEmpty) {
			inputBlock = ScopUserFactory.scopBlock()
			containingBlock.children.add(inputBlock)
		}
		
		containingBlock.children.add(node)
		
		var ScopBlock outputBlock
		if (!outputCopies.isEmpty) {
			outputBlock = ScopUserFactory.scopBlock()
			containingBlock.children.add(outputBlock)
		}
						
		// Create copies
		val scope = (node.scopRoot as GecosScopBlock).scope
		val copySymbols = new ArrayList<Symbol>()
		for (symbol: symbolsToCopy) {
			var nb = 0
			while (scope.lookup(symbol.name + "_copy" + nb) !== null) {
				nb++
			}
			val copySymbol = GecosUserCoreFactory.symbol(symbol.name + "_copy" + nb, symbol.type, scope)
			if (copySymbol.type.constant) {
				
			}
			copySymbols.add(copySymbol)
			if (inputCopies.containsKey(symbol))
				ISLCodegenUtils.dataMover(inputCopies.get(symbol), inputBlock, symbol, copySymbol)
			if (outputCopies.containsKey(symbol))
				ISLCodegenUtils.dataMover(outputCopies.get(symbol), outputBlock, copySymbol, symbol)	
			
			node.listAllAccesses.filter[it.sym == symbol].forEach[it.sym = copySymbol]
		}
		 		
		if (CONTRACT) {
			for (copySymbol: copySymbols.filter[s|s.type.array&& !s.containingScope.global]) {
				// contract all local arrays
				FindArrayContractionCandidates.createDirective(containingBlock, copySymbol)
			}
			new ScopArrayContract(containingBlock).compute
		} else {
			for (copySymbol: copySymbols) {
				InferSliceSlindingWindow.createDirective(containingBlock, copySymbol)
			}
			
		}
		new ApplyMemoryLayout(containingBlock).compute
		
		debug("Moved local data for Scop " + containingBlock)
		debug("Statements : " + node.listAllStatements)
		
		containingBlock
	}
}
