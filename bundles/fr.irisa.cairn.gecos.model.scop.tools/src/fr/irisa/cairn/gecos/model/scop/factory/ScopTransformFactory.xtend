package fr.irisa.cairn.gecos.model.scop.factory

import fr.irisa.cairn.gecos.model.scop.transform.internal.TransformTomFactory
import java.util.List
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
import gecos.core.Symbol

class ScopTransformFactory extends TransformTomFactory {

	// CreateOperatorWithParameters layout
	def public static DataLayoutDirective createLayout(Symbol sym) {
		createLayout(#[],#[],sym)		
	}
	
	
}