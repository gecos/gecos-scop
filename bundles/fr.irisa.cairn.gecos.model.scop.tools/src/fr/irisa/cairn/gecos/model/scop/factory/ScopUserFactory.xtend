package fr.irisa.cairn.gecos.model.scop.factory

import fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopParameter
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory
import fr.irisa.cairn.gecos.model.scop.transform.internal.TransformTomFactory
import fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import gecos.core.CoreFactory
import gecos.core.Scope
import gecos.core.Symbol
import gecos.instrs.Instruction
import gecos.types.IntegerTypes
import gecos.types.SignModifiers
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.common.util.EList
import org.polymodel.algebra.IntConstraintSystem
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.Variable
import fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective
import org.polymodel.algebra.AlgebraUserFactory
import gecos.types.ArrayType
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock
import fr.irisa.cairn.gecos.model.scop.ScopFSMState
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedBlock
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead

class ScopUserFactory { 
  
    static Scope scope;
    
    def static setScope(Scope s) {
    		scope=s  
    }

	def static ScopUnexpandedStatement unexpandedStmt(List<ScopDimension> iterators, String d, String sched) {
		ScopTomFactory.createUnexpanded(iterators as List, d, sched)
	} 
	
//	def static ScopUnexpandedBlock unexpandedBlock(List<ScopDimension> iterators, String d, String sched) {
//		
//		ScopTomFactory.createUnexpandedBlk(iterators as List, d, sched)
//	} 
 
	def static GecosScopBlock root() {
		ScopFactory.eINSTANCE.createGecosScopBlock()
	} 

	def static GecosScopBlock root(ScopNode n) {
		val g = ScopFactory.eINSTANCE.createGecosScopBlock()
		g.scope = CoreFactory.eINSTANCE.createScope
		g.root=n; 
		g
	}

	def static ScopDimension dimSym(Symbol s) {
		var dim =ScopFactory.eINSTANCE.createScopDimension
		dim.symbol = s as Symbol
		dim.name=s.name
		dim  
	}
	
	def static ScopDimension dim(String name) {
		var dim = ScopFactory.eINSTANCE.createScopDimension
		GecosUserTypeFactory.scope = scope
		dim.symbol=scope.lookup(name)
		if (dim.symbol===null) {
			dim.symbol = GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT(), scope)
		}
		dim.name = name
		dim
	}

	def static ScopParameter param(Symbol p) {
		var dim = ScopFactory.eINSTANCE.createScopParameter
		dim.symbol= p
		dim.name = p.name
		dim
	}

	def static ScopParameter param(String name) {
		var param = ScopFactory.eINSTANCE.createScopParameter
		GecosUserTypeFactory.scope = scope
		param.symbol = GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT(), scope)
		param.name = name
		param
	}

	def static ScopForLoop scopFor(Variable i, IntExpression lb, IntExpression ub, IntExpression s, ScopNode body) {
		ScopTomFactory.createLoop(i,lb,ub,s,body);
	}

	def static ScopForLoop scopFor(ScopDimension i, int lb, int ub, int s, ScopNode body) {
		ScopTomFactory.createLoop(i,
			AlgebraUserFactory.affine(#[AlgebraUserFactory.constant(lb as long)]),
			AlgebraUserFactory.affine(#[AlgebraUserFactory.constant(ub as long)]),
			AlgebraUserFactory.affine(#[AlgebraUserFactory.constant(s as long)]),
			body
		);
	}

	def static ScopBlock scopBlock(ScopNode c) {
		ScopTomFactory.createScopBlk(#[c])
	}

	def static ScopBlock scopBlock(ScopNode... c) {
		ScopTomFactory.createScopBlk(c.toList)
	}	

	def static ScopGuard scopGuard(List<IntConstraintSystem> res, ScopNode scopBlock, ScopNode object) {
		ScopTomFactory.createGuard(res,scopBlock,object);
	}

	def static ScopIntExpression sexpr(IntExpression arg) {
		ExtractorTomFactory.createSexpr(arg);
	}

	def static ScopIntExpression sexpr(ScopDimension i) {
		ExtractorTomFactory.createSexpr(AlgebraUserFactory.affine(#[AlgebraUserFactory.term(1, i)]))
	}

	def static ScopIntExpression sexpr(long coef, ScopDimension i) {
		ExtractorTomFactory.createSexpr(AlgebraUserFactory.affine(#[AlgebraUserFactory.term(coef, i)]))
	}


	def static ScopRegionRead regionRead(Symbol array, List<ScopDimension> existentials, IntConstraintSystem ics) {
		val regionRead = ScopTomFactory.createRegionRead(array, existentials, ics)
		existentials.forEach[e|regionRead.indexExpressions+=AlgebraUserFactory.affine(e)]
		regionRead
	}


	def static ScopInstructionStatement scopStatement(String name, Instruction ...args) {
		ScopTomFactory.createStmt(name,new ArrayList<Instruction>(args))
	}  

	def static ScopInstructionStatement scopStatement(String name, List<Instruction> args) {
		ScopTomFactory.createStmt(name,new ArrayList<Instruction>(args))
	}  

	def static ScopStatement scopStatement(String name) {
		ScopTomFactory.createStmt(name,#[])
	}

	def static ScopWrite scopWrite(Symbol sym, List<IntExpression> index) {
		val atype = sym.type as ArrayType
		if (index.size!=atype.nbDims) {
			throw new UnsupportedOperationException("inconsistent scalar array type")
		} else {
			val res = ScopTomFactory.createWrite(sym,index)
			res.type = atype.innermostBase
			res
		}
	}  

	def static ScopRead scopRead(Symbol sym, List<IntExpression> index) {
		val atype = sym.type as ArrayType
		if (index.size!=atype.nbDims) {
			throw new UnsupportedOperationException("inconsistent scalar array type")
		} else {
			val res = ScopTomFactory.createRead(sym,index)
			res.type = atype.innermostBase
			res
		}
	}  
	def static ScopFSMBlock scopFSMBlock() {
		fr.irisa.cairn.gecos.model.scop.ScopFactory.eINSTANCE.createScopFSMBlock();
	}  

	def static ScopFSMState scopFSMState() {
		fr.irisa.cairn.gecos.model.scop.ScopFactory.eINSTANCE.createScopFSMState();
	}  

	def static ScopFSMTransition scopFSMTransition() {
		fr.irisa.cairn.gecos.model.scop.ScopFactory.eINSTANCE.createScopFSMTransition();
	}  

	def static ScopFSMTransition scopFSMTransition(IntConstraintSystem[] domain, IntExpression[] nextIteration, ScopFSMState next) {
		val r = scopFSMTransition();
		r.domain+=domain
		r.nextIteration+=nextIteration
		r
	}  
	
	def static ScopTransform scopTransform(ScopNode node, ScopTransformDirective... a ) {
		var res = TransformFactory.eINSTANCE.createScopTransform
		node.transform=res
		res.commands+=a
		res
	}

	def static LivenessDirective livenessDirective(Symbol s) {
		var res = TransformFactory.eINSTANCE.createLivenessDirective
		res.symbol=s
		res.liveIn=true
		res
	}
	
	def static ScheduleDirective scheduleDirective(ScopNode node, JNIISLMap schedule) {
		TransformTomFactory.createSchedule(node as Instruction, schedule.toString);
	}

	def static DataLayoutDirective layoutDirective(Symbol s, List<ScopDimension> vars, List<IntExpression> exprs) {
		TransformTomFactory.createLayout(exprs, vars as List,s);
	}

}
