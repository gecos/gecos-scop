package fr.irisa.cairn.gecos.model.scop.layout;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory
import fr.irisa.cairn.gecos.model.scop.ScopAccess
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.core.Symbol
import gecos.types.ArrayType
import gecos.types.BaseType
import gecos.types.Type
import java.util.HashMap
import java.util.List
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.polymodel.algebra.FuzzyBoolean
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.tom.SimplifyIntExpression

class ApplyMemoryLayout {

	static boolean debug = false;
	EObject target;
	
	public new(EObject target) {
		this.target=target;
	}

	def void compute() {
		var List<ScopNode > roots;
		if (target instanceof ScopNode)
			roots = newArrayList(target)
		else
			roots = EMFUtils.eAllContentsFirstInstancesOf(target, ScopNode)
		
		for (ScopNode root : roots) {
			
			val workList = root.transform?.commands?.filter(DataLayoutDirective)?.toList
			if (workList !== null)
				apply(workList, root)
		}
	}
	
	def private void apply(List<DataLayoutDirective> newCommands, ScopNode root) {
		//utility map
		val mapping = new HashMap<Symbol, DataLayoutDirective>(newCommands.size());
		for (DataLayoutDirective dir : newCommands) {
			val Symbol sym = dir.getSymbol();
			mapping.put(sym, dir);

			//replace array initialization with new size
			val EList<IntExpression> addresses = dir.getAddresses();
			GecosUserTypeFactory.setScope(sym.getContainingScope());
			val Type newType = createType(addresses,sym.getType());
			sym.setType(newType);
		}
		
		
		for (ScopStatement stmt : root.listAllStatements()) {
			for (ScopWrite write : stmt.listAllWriteAccess()) {
				
				if (mapping.containsKey(write.getSym())) {
					if (debug) System.out.println("updating "+write);
					update(write,mapping.get(write.getSym()));
					if (debug) System.out.println(" >> "+write);
				}
			}
			for (ScopRead read  : stmt.listAllReadAccess()) {

				if (mapping.containsKey(read.getSym())) {
					if (debug) System.out.println("updating "+read);
					update(read,mapping.get(read.getSym()));
					if (debug) System.out.println(" >> "+read);
				}
			}
		}
	}
	
	def private Type createType(EList<IntExpression> sizes, Type oldType) {
		var Type basetype = null;
		if (oldType instanceof BaseType)
			basetype = oldType
		else if (oldType instanceof ArrayType) {
			basetype = oldType.getInnermostBase();
		} else throw new UnsupportedOperationException();
		
		
		for (var i = sizes.size() - 1; i >= 0; i--) {
			val IntExpression size = sizes.get(i);
			if (isTrueDimension(size)) {
				val ScopIntExpression  sizeInstr = ScopFactory.eINSTANCE.createScopIntExpression();
				sizeInstr.setExpr(size.copy());
				sizeInstr.setType(GecosUserTypeFactory.INT());
				
				val ArrayType arrayType = GecosUserTypeFactory.ARRAY(basetype, sizeInstr);
				basetype = arrayType;
			}
			
		}
		return basetype;
	}
	
	private def boolean isTrueDimension(IntExpression size) {
		!(size.isConstant() == FuzzyBoolean.YES && size.toAffine().getConstantTerm().getCoef() == 1)
	}
	
	def void update(ScopAccess scopAccess, DataLayoutDirective dir) {
		val origIndexes = scopAccess.indexExpressions
		val newIndexes = new BasicEList<IntExpression>()
		val addresses = dir.getAddresses()
		
		val parameters = scopAccess.listRootParameters.map[it.symName].toString
		val range = ScopISLConverter.getAccessMap(scopAccess).range().clearTupleName
		
		for (var i = 0; i < origIndexes.size(); i++) {
			val exp = origIndexes.get(i);
			val size = addresses.get(i);
			if (isTrueDimension(size)) {
				val rangeDim = range.copy.projectIn(JNIISLDimType.isl_dim_set, i)
								
				val exclusionRange = JNIISLSet.buildFromString('''«parameters» -> { [exclude] : «size.toString.replace('[','(').replace(']',')')» - 1 < exclude }''')
				val outOfBoundAccess = rangeDim.intersect(exclusionRange)
				
				if (!outOfBoundAccess.isEmpty) {
					val mod = IntExpressionBuilder.symbolicMod(exp.copy(), size.copy())
					newIndexes.add(SimplifyIntExpression.simplify(mod))
				} else {
					newIndexes.add(exp.copy)
				}
			}
		}
		origIndexes.clear();
		origIndexes.addAll(newIndexes);
	}
}
