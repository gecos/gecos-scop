package fr.irisa.cairn.gecos.model.scop.layout

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.core.Symbol
import gecos.gecosproject.GecosProject
import java.util.List
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter

class FindArrayContractionCandidates {

	GecosProject proj

	new(GecosProject proj) {
		this.proj = proj
	}

	def void compute() {
		var List<GecosScopBlock> roots = EMFUtils.eAllContentsInstancesOf(proj, GecosScopBlock);
		roots.forEach[r|compute(r)];
	}

	def void compute(GecosScopBlock root) {
		var syms = ScopAccessQuery.getAllUsedSymbols(root);
		syms -= root.liveInSymbols
		syms -= root.liveOutSymbols
		for (s : syms.filter[it.type.isArray])
			createDirective(root, s)
	}

	static def createDirective(ScopNode node, Symbol s) {
		val writes = ScopISLConverter.getAllWriteAccessMaps(node, s).range()
		val reads = ScopISLConverter.getAllReadAccessMaps(node, s).range()
		if (writes.isEqual(reads)) {
			println("Symbol " + s.name + " is a candidate for contraction");
			val dir = TransformFactory.eINSTANCE.createContractArrayDirective
			dir.symbol = s
			if (node.transform === null) {
				node.transform = TransformFactory.eINSTANCE.createScopTransform
			}
			node.transform.commands.add(dir)
		} else {
			println("Symbol " + s.name + " reads do no match writes (possible dead array cells), do not contract")
		}
	}
}
