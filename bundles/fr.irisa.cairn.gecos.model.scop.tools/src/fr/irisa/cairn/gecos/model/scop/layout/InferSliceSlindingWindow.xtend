package fr.irisa.cairn.gecos.model.scop.layout

import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
import fr.irisa.cairn.gecos.model.scop.factory.ScopTransformFactory
import java.util.List
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace

class InferSliceSlindingWindow {
	
	
	
	def public static createDirective(ScopNode node, Symbol s) {
		val footprint =ScopAccessAnalyzer.getArrayAccessFootprint(s,node);
		val nbdims = footprint.nbDims
		var map= JNIISLMap.fromDomain(footprint).simpleHull.toMap; 
		var nbLocalParams = node.listAllParameters.size
		var nbRootParams = node.listRootParameters.size

		map=map.moveDims(JNIISLDimType.isl_dim_out,0,JNIISLDimType.isl_dim_in,0,nbdims)
		map=map.moveDims(JNIISLDimType.isl_dim_in,0,JNIISLDimType.isl_dim_param,nbRootParams,nbLocalParams-nbRootParams)

		val layout = ScopTransformFactory.createLayout(s)

		val params = (0..map.nbParams).map[v|ScopUserFactory.dim('p'+v)].toList	
		val index = (0..map.nbIns).map[v|ScopUserFactory.dim('i'+v)].toList

		layout.vars+=params
		layout.vars+=index
		
		var JNIISLMultiAff res  =null
		for (i:0..map.nbIns-1) {
			println("dimension "+map.getDimName(JNIISLDimType.isl_dim_out,i)+" for "+map);
			var newMap = map.copy.projectOutAllBut(JNIISLDimType.isl_dim_out,i)
			val _min = newMap.copy.lexMin
			val _max = newMap.copy.lexMax
			
			// compute the difference between max and min piecewise functions 
			var width = _min.copy.reverse.applyRange(_max).deltas
			//
			// If width expression depends on parameters (if not, the expression is already a constant)
			if (width.involvesDims(JNIISLDimType.isl_dim_param, 0, width.getNbParams())) {
				// Check if the parameter space is bounded 
				var JNIISLSet noParamwidth = width.copy().projectOut(JNIISLDimType.isl_dim_param, 0, width.nbParams);
				println("\t - after projecting parameters = "+noParamwidth);
				if (noParamwidth.copy().hasAnyUpperBound(JNIISLDimType.isl_dim_set, 0)) {
					println("\t - you are lucky ! there exists a constant array folding factor "+noParamwidth.copy().lexMax());
					noParamwidth = noParamwidth.copy().lexMax();
					noParamwidth = noParamwidth.alignParams(width.getSpace());
					println("\t - aligned "+noParamwidth);
					width =  noParamwidth;
				} else {
					println("\t - you're screwed : width is unbounded = "+noParamwidth);
				}
			} 
			var lwidth = width.toPWMultiAff.pieces.get(0).maff.getAff(0).getConstant;

			//
			// Build reindexing function			
			//
			val _extmin = _min.addInputDimensions(map.outputDimensionNames)
			val Id = map.getDomain.flatProduct(map.getRange).buildIdentityMap
			val idPieces= Id.copy.toPWMultiAff.pieces
			val minPieces= _extmin.copy.toPWMultiAff.pieces
			var JNIISLMap rmap = null
			val lexGt = _extmin.copy.getRange().space.lexLE
			if (minPieces.size>1) {
				for (p:minPieces) {
					if (rmap===null) {
						rmap= p.maff.toMap
					} else {
						rmap = rmap.union(p.maff.toMap) 
					}
				}
			}
			var valDom = _extmin.getDomain
			var simExtMin= rmap.copy
			simExtMin= simExtMin.applyRange(lexGt.copy)
			simExtMin= simExtMin.intersectDomain(valDom.copy)
			for (bmap1:simExtMin.copy.basicMaps) {
				for (bmap2:simExtMin.copy.basicMaps) {
					if (bmap1!==bmap2) {
						if (bmap1.isSubset(bmap2)) {
							println(bmap1+"is subset of "+bmap2)
						}
					}
				}
			}
			//simExtMin= simExtMin.lexMin.simplify
			println(simExtMin)
			val lhs = idPieces.get(0).maff.affs.get(2)
			val rhs = minPieces.get(0).maff.affs.get(0)
			val offset =lhs.sub(rhs.copy).mod(lwidth);
			val expr = ISLScopConverter.aff(offset,index,params)
			layout.addresses+=expr
		}
		res
	}
	
}