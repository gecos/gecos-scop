package fr.irisa.cairn.gecos.model.scop.layout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;

import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopTransformQuery;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.memorylayout.ISLMemoryLayoutTools;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;

@GSModule("Compute the contracted size for each candidate array.")
public class ScopArrayContract {
	
	static boolean debug = true;
	EObject target;
	private boolean usePowerOfTwo = false;
	private double contractThreshold = 0.9;
	
	@GSModuleConstructor(args = { @GSArg(name = "target", info = "Target to inspect for array contraction.") })
	public ScopArrayContract(EObject target) {
		this.target=target;
	}
	
	@GSModuleConstructor(args = {
		@GSArg(name = "target", info = "Target to inspect for array contraction."),
		@GSArg(name = "usePowerOfTwo", info = "Wether to round array dimension to the next power of two.")
	})
	public ScopArrayContract(EObject target, boolean usePowerOfTwo) { 
		this.target=target;
		this.usePowerOfTwo= usePowerOfTwo;
	}
	
	@GSModuleConstructor(args = {
		@GSArg(name = "target", info = "Target to inspect for array contraction."),
		@GSArg(name = "contractThreshold", info = "Ratio of contraction below which the array dimension is contracted. Default is 0.9.")
	})
	public ScopArrayContract(EObject target, double contractThreshold) {
		if (contractThreshold < 0 || contractThreshold > 1) {
			throw new IllegalArgumentException("Contraction threshold must be between 0 and 1");
		}
		
		this.target = target;
		this.contractThreshold = contractThreshold;
	}

	public void compute() {
		if (debug) System.out.println("*-------------------");
		List<ScopNode > roots = EMFUtils.eAllContentsInstancesOf(target, ScopNode.class);
		if (target instanceof ScopNode)
			roots.add((ScopNode) target);
		for (ScopNode  root : roots) {
			ScopTransform transform = root.getTransform();
			if (transform != null) {
				List<DataLayoutDirective> newCommands = computeAllocation(root, transform);
				if (debug) System.out.println(newCommands);
	
				for (DataLayoutDirective command : newCommands) 
					root.getTransform().getCommands().add(command);
			}
			
		}
	}

	private List<DataLayoutDirective> computeAllocation(ScopNode node, ScopTransform transform) {
		ScopNode root = node.getScopRoot();
		List<DataLayoutDirective> newCommands = new ArrayList<>();
		for (ScopTransformDirective command : transform.getCommands()) {
			if (command instanceof ContractArrayDirective) {
				ContractArrayDirective c = ((ContractArrayDirective)command);
				
				Symbol sym = c.getSymbol();
				checkSymbolScope(sym);
 
				String arrayName = sym.getName();
				
		
				JNIISLUnionSet domains = ScopISLConverter.getAllDomains(root);
				JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root);
				JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root);
				JNIISLUnionMap islPRDG = ScopISLConverter.getValueBasedDepenceGraph(root);
							
				JNIISLSet islContext = ScopISLConverter.getContext(root);

				JNIISLUnionMap schedule = ScopTransformQuery.buildISLSchedule(root);
				
				ISLMemoryLayoutTools allocator = new ISLMemoryLayoutTools();
				List<Map<JNIISLSet, JNIISLMultiAff>> alloc = 
						allocator.contractSuccessiveModuloBasic(arrayName, islContext, domains, writes, reads, islPRDG, schedule);
				
				DataLayoutDirective newdir = TransformFactory.eINSTANCE.createDataLayoutDirective();
				newCommands.add(newdir);
				newdir.setSymbol(sym);
				newdir.getAddresses();

				List<Instruction> arrayDims = sym.getType().asArray().getSizes();
				int dimIndex = 0;
				
				//iterate per dimension
				List<IntExpression> memAlloc = new ArrayList<>(alloc.size());
				for (Map<JNIISLSet, JNIISLMultiAff> a : alloc) {
					Set<Entry<JNIISLSet, JNIISLMultiAff>> entrySet = a.entrySet();
					
					List<IntExpression> partialSizes = new ArrayList<>(entrySet.size());
					//iterate per solution (max)
					for (Entry<JNIISLSet, JNIISLMultiAff> e : entrySet) {
						JNIISLMultiAff value = e.getValue();
						if (value.getNbAff() != 1)
							throw new RuntimeException();
						List<IntExpression> exprList = ISLScopConverter.exprList(value, new ArrayList<Variable>(), root.listAllParameters());
						if (exprList.size() != 1)
							throw new RuntimeException();
						
						IntExpression partialSize = exprList.get(0);
						partialSizes.add(partialSize);
					}
					IntExpression res;
					if (partialSizes.size() > 1)
						res = IntExpressionBuilder.max(partialSizes);
					else 
						res = partialSizes.get(0);
					
					// Apply threshold to contracted sizes
					if (res.isConstant() == FuzzyBoolean.YES && arrayDims.get(dimIndex).isConstant()) {
						long contracted = ((AffineExpression) res).getConstantTerm().getCoef();
						long original = ((IntInstruction) arrayDims.get(dimIndex)).getValue();
						if (((double) contracted) / original >= contractThreshold) {
							res = IntExpressionBuilder.constant((int) original);
						}
					}
					
					// Apply next power of two
					if (usePowerOfTwo) {
						if (res.isConstant() == FuzzyBoolean.YES) {
							AffineExpression e = (AffineExpression) res;
							long v = e.getConstantTerm().getCoef();
							// nextPower of 2
							v = Math.max(1, Long.highestOneBit(v - 1) << 1);
							res = IntExpressionBuilder.constant((int) v);
						}
					}
					
					// If original size was a constant, don't introduce parameters which may be iterators in disguise
					if (res.isConstant() != FuzzyBoolean.YES && arrayDims.get(dimIndex).isConstant()) {
						long original = ((IntInstruction) arrayDims.get(dimIndex)).getValue();
						res = IntExpressionBuilder.constant((int) original);
					}
					
					memAlloc.add(res);
					newdir.getAddresses().add(res);
					dimIndex++;
				}
			}
		}
		return newCommands;
	}

	/**
	 * TODO : Checks that the array to contract is declared as a local symbol.
	 * TODO : checks that the array is used in the SCoP only.
	 * 
	 * @param symbol
	 */
	private void checkSymbolScope(Symbol symbol) {
		
	}
}
