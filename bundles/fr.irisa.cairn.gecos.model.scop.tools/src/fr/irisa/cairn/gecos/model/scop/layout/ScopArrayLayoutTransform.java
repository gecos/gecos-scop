package fr.irisa.cairn.gecos.model.scop.layout;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.IntExpression;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;


import fr.irisa.cairn.tools.ecore.query.EMFUtils;

public class ScopArrayLayoutTransform {
	
	private EObject data;

	public ScopArrayLayoutTransform(EObject targer) {
		this.data=targer;
	}
	
	public void compute() {
		EList<GecosScopBlock> list = EMFUtils.eAllContentsInstancesOf(data, GecosScopBlock.class);
		for(GecosScopBlock data : list) {
			ScopTransform transform = data.getTransform();
			if (transform!=null) process(data, transform);  
		}
	}
  
	public void process(GecosScopBlock scop, ScopTransform data) {
		System.out.println("**********************************************");
		System.out.println("* Applying Layout transform on Scop "+scop);
		System.out.println("**********************************************");

		EList<DataLayoutDirective> list = EMFUtils.eAllContentsFirstInstancesOf(data, DataLayoutDirective.class);
		for(DataLayoutDirective layout : list) {
			System.out.println("- Applying "+layout+ " directive");
			Set<ScopRead> reads = new HashSet<>(EMFUtils.eAllContentsFirstInstancesOf(scop, ScopRead.class));
			for(ScopRead read  : reads) {
				System.out.println("  - Tranforming read "+read+":"+read.hashCode());
				if(layout.getSymbol()==read.getSym()) {
					System.out.print("\t- Tranforming read "+read);
					transformIndexingFunction(layout, read);
					System.out.println(" into "+read);
				}
			}

			Set<ScopWrite> writes = new HashSet<>(EMFUtils.eAllContentsFirstInstancesOf(scop, ScopWrite.class));
			for(ScopWrite write  : writes) {
				if(layout.getSymbol()==write.getSym()) {
					System.out.print("\t- Transforming "+write+ " using "+layout.getVars()+"->"+layout.getAddresses());
					transformIndexingFunction(layout, write);
					System.out.println(" into "+write);
				}
				
			}

		}
	}

	private void transformIndexingFunction(DataLayoutDirective layout,ScopAccess access) {
		
		EList<IntExpression> index = access.getIndexExpressions();
		EList<IntExpression> newIndex = new BasicEList<IntExpression>();
		for(int i=0;i<layout.getAddresses().size();i++) {
			IntExpression transformationExpr = layout.getAddresses().get(i).copy();
			for(int k=0;k<index.size();k++) {
				IntExpression expr = index.get(k);
				ScopDimension var = layout.getVars().get(k);
				IntExpression transformed =SubstitutionHelper.substitute(var,transformationExpr,expr).simplify();
				transformationExpr =transformed;
			}
			newIndex.add(transformationExpr.simplify());
		}
		access.getIndexExpressions().clear();
		
		access.getIndexExpressions().addAll(newIndex);
		
	}
	
	
}
