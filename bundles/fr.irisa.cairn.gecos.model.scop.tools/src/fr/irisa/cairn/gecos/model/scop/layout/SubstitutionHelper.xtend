package fr.irisa.cairn.gecos.model.scop.layout

import org.eclipse.emf.ecore.util.EcoreUtil
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.Variable
import org.polymodel.algebra.affine.AffineExpression
import org.polymodel.algebra.affine.AffineTerm
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression
import org.polymodel.algebra.quasiAffine.QuasiAffineOperator
import org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm

/**
 * TODO : This should be mode to algebra, but there is an issue with the x!tend code generator 
 */
public class SubstitutionHelper {

	def static IntExpression substitute(Variable v, IntExpression target, IntExpression value) {
		return dispatchSubstitute(v,target,value);
	}

	def static dispatch IntExpression dispatchSubstitute(Variable v, IntExpression target, IntExpression value) {
		throw new UnsupportedOperationException("Method substitute("+value.getClass.simpleName+" in "+target.getClass.simpleName+"");
	}

	def static dispatch IntExpression dispatchSubstitute(Variable v, AffineExpression target, AffineExpression value) {
		val res = IntExpressionBuilder::affine()
		for(AffineTerm t : target.terms) {
			if(t.variable==v) {
				/* we substiture the variable by the scaled affine expression*/
				val coef = t.coef  
				var copy = value.copy as AffineExpression  
				for(AffineTerm e : copy.terms) { 
					e.setCoef(((coef as int)*(e.coef as int)))
				}
				res.terms.addAll(copy.terms)
			} else {
				res.terms.add(EcoreUtil::copy(t))
			}
		}
		return res.simplify;
	}

	def static dispatch IntExpression dispatchSubstitute(Variable v, QuasiAffineExpression _t, AffineExpression value) {
		val target = _t.copy() as QuasiAffineExpression		
		for(SimpleQuasiAffineTerm t : target.terms.filter(typeof(SimpleQuasiAffineTerm))) {
			t.setExpression(dispatchSubstitute(v,t.expression,value) as AffineExpression);
		}
		for(NestedQuasiAffineTerm t : target.terms.filter(typeof(NestedQuasiAffineTerm))) {
			t.setExpression(dispatchSubstitute(v,t.expression,value) as QuasiAffineExpression);
		}
		return target
	}

	def static dispatch IntExpression dispatchSubstitute(Variable v, AffineExpression target, QuasiAffineExpression value) {
		val res = IntExpressionBuilder::qaffine()
		val resAff = IntExpressionBuilder::affine()
		var long scale =-1
		var boolean found = false
		for(AffineTerm t : target.terms) {
			if(t.variable==v) {
				found = true
				scale = t.coef
			} else {
				resAff.terms.add(EcoreUtil::copy(t))
			}
		}  
		if(found) {
			val copy = EcoreUtil::copy(value) as QuasiAffineExpression
			res.terms.add(IntExpressionBuilder::mul(copy,scale))
			res.terms.add(IntExpressionBuilder::mul(resAff,1));
			return res.simplify;
		} else {
			return target;
		}
	}

	def static dispatch IntExpression dispatchSubstitute(Variable v, QuasiAffineExpression _t, QuasiAffineExpression value) {
		val target = IntExpressionBuilder::qaffine()		
		for(SimpleQuasiAffineTerm t : _t.terms.filter(typeof(SimpleQuasiAffineTerm))) {
			val r= dispatchSubstitute(v,t.expression,value);
			target.terms.add(IntExpressionBuilder::qterm(QuasiAffineOperator::MUL,r as QuasiAffineExpression,1))
		}
		for(NestedQuasiAffineTerm t : _t.terms.filter(typeof(NestedQuasiAffineTerm))) {
			
			t.setExpression(dispatchSubstitute(v,t.expression,value) as QuasiAffineExpression);
		}
		return target.simplify
	}
}
		