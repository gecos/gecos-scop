package fr.irisa.cairn.gecos.model.scop.query

import java.util.HashMap
import java.util.List
import java.util.ArrayList

class MapList<K,V> {

	HashMap<K,List<V>> map = new HashMap<K,List<V>>();
	
	new() {
		
	}
	
	def put(K key, V value) {
		if (!map.containsKey(key)) {
			map.put(key, new ArrayList<V>())	
		}
		map.get(key)+=value
	}

	def get(K key) {
		if (!map.containsKey(key)) {
			map.put(key, new ArrayList<V>())	
		}
		map.get(key)
	}
	
	def containsKey(K key) {
		map.containsKey(key)
	}
	
	def keySet() {
		map.keySet
	}

	def List<V> allValues() {
		map.values.flatten.toList
	}
}
