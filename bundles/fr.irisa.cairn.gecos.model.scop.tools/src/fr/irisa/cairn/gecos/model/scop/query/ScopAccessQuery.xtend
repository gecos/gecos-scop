package fr.irisa.cairn.gecos.model.scop.query

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopRead
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopAccess
import java.util.HashSet
import java.util.ArrayList

class ScopAccessQuery {
	
	
	def public static getAllGecosScopBlocks(EObject obj) {
		obj.eAllContents.filter(typeof(GecosScopBlock)).toList
	}
	
	def public static getAllUsedSymbols(ScopNode n) {
		new HashSet(n.eAllContents.filter(typeof(ScopAccess)).map[a| a.sym].toList)
	}
	
	def public static getAllWrittenSymbols(ScopNode n) {
		n.listAllStatements.map[s|s.listAllWriteAccess].flatten.map[a| a.sym].toList
	}

	def public static getAllReadSymbols(ScopNode n) {
		n.listAllStatements.map[s|s.listAllReadAccess].flatten.map[a| a.sym].toList
	}

	def public static getReadSymbolsMap(ScopNode n) {
		var map = new MapList<Symbol,ScopRead>();
		for (s : n.listAllStatements) {
			for (read : s.listAllReadAccess) {
				map.put(read.sym, read);
			}
		}
		map
	}

	def public static getAccessSymbolsMap(ScopNode n) {
		var map = new MapList<Symbol,ScopAccess>();
		for (s : n.listAllStatements) {
			for (access: s.listAllAccesses) {
				map.put(access.sym, access);
			}
		}
		map
	}

	def public static getWrittenSymbolsMap(ScopNode n) {
		var map = new MapList<Symbol,ScopWrite>();
		for (s : n.listAllStatements) {
			for (write : s.listAllWriteAccess) {
				map.put(write.sym, write);
			}
		}
		map
	}

	def static getAllReadsTo(Symbol s, ScopNode root) {
		var list = new ArrayList<ScopRead>();
		for (stmt : root.listAllStatements) {
			for (access: stmt.listAllReadAccess) {
				if(access.sym==s) list.add(access);
			}
		}
		list
	}

	def static getAllWritesTo(Symbol s, ScopNode root) {
		var list = new ArrayList<ScopWrite>();
		for (stmt : root.listAllStatements) {
			for (access: stmt.listAllWriteAccess) {
				if(access.sym==s) list.add(access);
			}
		}
		list
	}

	def static getAllAccessesTo(Symbol s, ScopNode root) {
		var list = new ArrayList<ScopAccess>();
		for (stmt : root.listAllStatements) {
			for (access: stmt.listAllAccesses) {
				if(access.sym==s) list.add(access);
			}
		}
		list
	}


}
