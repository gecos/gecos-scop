package fr.irisa.cairn.gecos.model.scop.sheduling;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule.JNIISLSchedulingOptions;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;

/**
 * Builds a schedule for the given GScopRoot, using the Pluto algorithm
 * from ISL. The ISL object representing the computed schedule is accessible
 * using .getSchedule(). 
 * 
 * The ScopTransformData returned references dummy PRDG nodes, and the PRDG 
 * does not contain any edge.
 * 
 * 
 * @author amorvan
 *
 */
public class ScopISLScheduler {


	public enum ScheduleAlgorithm {FEAUTRIER,PLUTO_ISL} ;
	public static final ScheduleAlgorithm DEFAULT_ALGORITHM = ScheduleAlgorithm.PLUTO_ISL;
	public static final Map<String,ScheduleAlgorithm> algos = new HashMap<String, ScopISLScheduler.ScheduleAlgorithm>() {
		private static final long serialVersionUID = 1L;
		{
			put("FEAUTRIER",ScheduleAlgorithm.FEAUTRIER);
			put("PLUTO_ISL",ScheduleAlgorithm.PLUTO_ISL);
		}
	};
	

	private Collection<GecosScopBlock> scops;
	private ScheduleAlgorithm algo;

	public ScopISLScheduler(GecosScopBlock root) {
		this (root,DEFAULT_ALGORITHM);
	}
	
	public ScopISLScheduler(GecosScopBlock root, ScheduleAlgorithm algo) {
		this(Arrays.asList(new GecosScopBlock[] {root}), algo);
	}

	public ScopISLScheduler(GecosScopBlock root, String algo) {
		this(root, algos.get(algo));
	}
	
	public ScopISLScheduler(GecosProject project) {
		this (project,DEFAULT_ALGORITHM);
	}
	
	public ScopISLScheduler(GecosProject project, String algo) {
		this(project, algos.get(algo));
	}

	public ScopISLScheduler(GecosProject project, ScheduleAlgorithm algo) {
		this(EMFUtils.eAllContentsInstancesOf(project, GecosScopBlock.class),algo);
	}
	
	public ScopISLScheduler(Collection<GecosScopBlock> scops) {
		this(scops,DEFAULT_ALGORITHM);
	}
	
	public ScopISLScheduler(Collection<GecosScopBlock> scops, String algo) {
		this(scops,algos.get(algo));
	}
	
	public ScopISLScheduler(Collection<GecosScopBlock> scops, ScheduleAlgorithm algo) {
		this.scops = scops;
		this.algo = algo;
	}
	
	public void compute() {
		for (GecosScopBlock root : scops) {
		
			
			JNIISLSchedulingOptions options = new JNIISLSchedulingOptions();
			switch (algo) {
			case FEAUTRIER:
				options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_FEAUTRIER);
				break;
			case PLUTO_ISL:
				options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_ISL);
				break; 
			default:
				throw new UnsupportedOperationException();
			}
			options.setMax_coefficient(1);
			options.setMax_constant_term(3);
			options.setFuse(1);
			JNIISLUnionMap prdg = ScopISLConverter.getMemoryBasedDepenceGraph(root);
			JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(root);
			JNIISLSchedule islSchedule = null;
			islSchedule = JNIISLSchedule.computeSchedule(domains.copy(), prdg.copy(), options);
			
			JNIISLUnionMap islScheduleMap = islSchedule.getMap();
			islScheduleMap = islScheduleMap.intersectDomain(domains.copy());
			if (root.getTransform()== null) {
				root.setTransform(TransformFactory.eINSTANCE.createScopTransform());
			}
			ScopTransform transform = root.getTransform();
			for (ScopStatement stmt : root.listAllStatements()) {
				ScheduleDirective directive = TransformFactory.eINSTANCE.createScheduleDirective();
				for (JNIISLMap map : islScheduleMap.getMaps()) {
					if (map.getInputTupleName().equals(stmt.getId())) {
						directive.setSchedule(map.toString());
						directive.setStatement(stmt);
					}
				}
				transform.getCommands().add(directive);
				
			}

			root.setTransform(transform);
		}
	}
}
