package fr.irisa.cairn.gecos.model.scop.sheduling

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule.JNIISLSchedulingOptions
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject
import java.util.Collection

/** 
 * Builds a schedule for the given GScopRoot, using the Pluto algorithm
 * from ISL. The ISL object representing the computed schedule is accessible
 * using .getSchedule(). 
 * The ScopTransformData returned references dummy PRDG nodes, and the PRDG 
 * does not contain any edge.
 * @author amorvan
 */
class ScopPlutoTilingScheduler {
	static final boolean VERBOSE = true
	GecosProject p
	Collection<GecosScopBlock> scops

	def private void debug(String string) {
		if (VERBOSE) {
			System.out.println(string)
		}
	}

	new(GecosScopBlock root) {
		scops = EMFUtils.eAllContentsInstancesOf(root, GecosScopBlock)
	}

//	def private JNIISLSchedule tile_schedule(JNIISLSchedule schedule, List<Integer> tsize) {
//		var JNIISLVector vec = JNIISLVector.buildFrom(ISLFactory.getContext(), tsize)
//		var JNIISLBandList bandForest = schedule.getBandForest()
//		var List<JNIISLBand> band_forest = bandForest.asJavaList()
//		for (var int i = 0; i < band_forest.size(); i++) {
//			var JNIISLBand b = band_forest.get(i)
//			debug('''band «i»: ''')
//			for (JNIISLMap m : b.getPrefixSchedule().getMaps())
//				debug('''   prefix: «m»''')
//			for (JNIISLMap m : b.getPartialSchedule().getMaps())
//				debug('''   partial: «m»''')
//			for (JNIISLMap m : b.getSuffixSchedule().getMaps())
//				debug('''   suffix: «m»''')
//			b.tile(vec.copy())
//			debug('''   band «i» after : «b»''')
//		}
//		return schedule
//	}
 
	def void compute() {
		for (GecosScopBlock root : scops) {
			var JNIISLSchedulingOptions options = new JNIISLSchedulingOptions()
			options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_ISL)
			options.max_constant_term= root.listAllStatements.size;
			options.fuse = 1;
			var prdg = ScopISLConverter.getMemoryBasedDepenceGraph(root)
			var domains = ScopISLConverter.getAllStatementDomains(root)
			var islSchedule = JNIISLSchedule.computeSchedule(domains.copy(), prdg.copy(), options)
			
			var islScheduleMap = islSchedule.getMap()
			islScheduleMap = islScheduleMap.intersectDomain(domains.copy())
			
			if (root.getTransform() === null) {
				root.setTransform(TransformFactory.eINSTANCE.createScopTransform())
			}
			val transform = root.getTransform()
			for (stmt : root.listAllStatements()) {
				var directive = TransformFactory.eINSTANCE.createScheduleDirective()
				for (map : islScheduleMap.maps) {
					if (map.inputTupleName==stmt.id) {
						directive.schedule=map.toString()
						directive.statement = stmt
					}
				}
				transform.commands+=directive
			}
			root.transform = transform
		}
	}
}
