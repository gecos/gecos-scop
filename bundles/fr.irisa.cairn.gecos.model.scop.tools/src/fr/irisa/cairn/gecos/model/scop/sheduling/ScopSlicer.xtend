package fr.irisa.cairn.gecos.model.scop.sheduling

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation
import fr.irisa.cairn.gecos.model.scop.transform.SlicePragma
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.r2d2.gecos.framework.GSModule
import gecos.core.Symbol
import gecos.gecosproject.GecosProject
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.factory.IntExpressionBuilder
import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionTransformer
import fr.irisa.cairn.gecos.model.scop.transforms.ScopDuplicateNode

@GSModule("Slice each scop into tiles")
class ScopSlicerPass {
	val GecosProject proj
		
	new(GecosProject proj) {
		this.proj = proj
	}
	
	def compute() {
		for (node: EMFUtils.eAllContentsInstancesOf(proj, ScopNode)) {
			val commands = (node.annotations?.get("GCS") as S2S4HLSPragmaAnnotation)?.directives?.filter(SlicePragma)
			if (commands !== null) {
				for (command: commands) {
					new ScopSlicer(node, command.sizes, command.unrolls).compute
				}				
			}
		}
	}
}

class ScopSlicer {
	static final boolean VERBOSE = true
	
	def private static void debug(String string) {
		if (VERBOSE) println('''[ScopSlicer] «string»''')
	}

	var ScopNode node
	val List<Integer> tileSizes
	val List<Integer> unrollSizes
	var Symbol outputSym

	new (ScopNode node, List<Integer> tileSizes, List<Integer> unrollSizes) {
		this.node = node
		this.tileSizes = tileSizes
		this.unrollSizes = unrollSizes
	}

	def compute() {
		val scopRoot = node.scopRoot as GecosScopBlock
		if (node.parentScop==scopRoot) {
			scopRoot.root=ScopUserFactory.scopBlock(node);
		}
		ScopUserFactory.scope = scopRoot.scope
			
	
		val tileParameters = createTileIterators(scopRoot)
		
		val outputSyms = node.listAllReferencedSymbols.filter[!ScopAccessAnalyzer.computeFlowOut(it, node).isEmpty]
		if (outputSyms.size == 0) {
			throw new UnsupportedOperationException("Found no output to slice")
		}
		if (outputSyms.size != 1) {
			throw new UnsupportedOperationException("Cannot slice with more than 1 output. Output symbols found:" + outputSyms)
		}
		outputSym = outputSyms.get(0)
		debug('''Target symbol is «outputSyms»''')

		// Slice and insert guards on each statement
		// Those guards should be optimized using ScopIslCodegen
		val prdg = tilePrdg(tileParameters.map[it.symName])
		val sliceMap = slice(prdg, outputSym.name)
		for (kvp : sliceMap.entrySet) {
			val stmt = node.getStatement(kvp.key)
			val stmtDomain = ScopISLConverter.getDomain(stmt, node)
			val guard = kvp.value.copy.clearTupleName.gist(stmtDomain)
			debug('''Guarding statement «stmt» with «kvp.value.copy»''')
			ISLScopConverter.guard(stmt, guard)
		}
		
		node = DataMotionTransformer.insertDataMotion(node)
		
		val outTiledDomain = computeOuterDomain(tileParameters.map[it.symName])
		node.setAnnotation("SliceToUnroll", null)
		tileOuterDomain(tileParameters, outTiledDomain)
		
		for(candidate: EMFUtils.eAllContentsInstancesOf(node, ScopNode).filter[it.annotations.containsKey("SliceToUnroll")]) {
			candidate.annotations.removeKey("SliceToUnroll")
			var current = candidate
			for (pos : 0..<Math.min(tileParameters.size, unrollSizes.size)) {
				current = ScopDuplicateNode.duplicate(current, tileParameters.get(pos).symbol, unrollSizes.get(pos))				
			}
		}
		
		scopRoot.relabelStatements
	}
	
	def private JNIISLUnionMap tilePrdg(List<String> tileParameters) {
		val flowOutMap = ScopAccessAnalyzer.computeFlowOutMap(outputSym, node)
		val flowOut = ScopAccessAnalyzer.computeFlowOut(outputSym, node)

		val iterators = flowOut.indicesNames
		if (iterators.size > tileSizes.size) {
			throw new UnsupportedOperationException("Not enough sizes to tile iterators " + iterators)
		}
		
		val inequations = (0..<iterators.size).map[i|'''«tileSizes.get(i)»«tileParameters.get(i)»<=«iterators.get(i)»<=«tileSizes.get(i)»«tileParameters.get(i)»+«tileSizes.get(i)-1»''']
		var str = '''«flowOut.parametersNames» -> { «outputSym.name»«iterators» : «inequations.reduce[p1, p2|p1 + " and " + p2]»}'''
		val tileDomain = JNIISLUnionSet.buildFromString(JNIISLContext.ctx, str)

		debug(''' - Tile «outputSym» on domain «tileDomain»''')
		val flowOutMapTiled = flowOutMap.intersectRange(tileDomain)
		
		var prdgTiled = ScopISLConverter.getMemoryBasedDepenceGraph(node)
		for (revFlow: flowOutMapTiled.reverse.maps)
			prdgTiled = prdgTiled.addMap(revFlow)
		
		prdgTiled
	}
	
	def private createTileIterators(GecosScopBlock scop) {
		val tileIterators = new ArrayList<ScopDimension>()
		for (i : scop.iterators) {
			// TODO unique names
			val tileIterator = ScopUserFactory.dim(i.symName + i.symName)
			scop.parameters.add(tileIterator)
			tileIterators.add(tileIterator)
		}
		
		tileIterators
	}
	
	def private slice(JNIISLUnionMap prdg, String target) {
		var sliceMap = new HashMap<String, JNIISLSet>()
		val closurePrdg = prdg.copy.simpleHull.transitiveClosure(new JNIPtrBoolean).simpleHull.coalesce
		val targetPrdg = closurePrdg.maps.filter[m|m.inputTupleName == target]
				
		debug('''Slice symbol «target» on closure PRDG:''')
		targetPrdg.forEach[debug('''«it»''')]

		for (stmtMap : targetPrdg) {
			val domain = stmtMap.getRange
			val tupleName = domain.tupleName
			debug(''' - Slicing iteration domain of statement «tupleName»''')
			debug(''' - Sliced domain «domain»''');
			if (!sliceMap.containsKey(tupleName)) {
				sliceMap.put(tupleName, domain.simpleHull.toSet)
			} else {
				sliceMap.put(tupleName, sliceMap.get(tupleName).union(domain).simpleHull.toSet)
			}
		}
		sliceMap
	}
	
	def private computeOuterDomain(List<String> tileParameters) {
		val flowOut = ScopAccessAnalyzer.computeFlowOut(outputSym, node)
		val nbIn = flowOut.nbDims
		var flowOutTiled = flowOut
		for (i : 0..<nbIn) {
			val paramPos = flowOutTiled.findDimByName(JNIISLDimType.isl_dim_param, tileParameters.get(i))
			flowOutTiled = flowOutTiled.moveDims(JNIISLDimType.isl_dim_set, nbIn + i, JNIISLDimType.isl_dim_param, paramPos, 1) 
		}
		var outTiledDomain = flowOutTiled.projectOut(JNIISLDimType.isl_dim_set, 0, nbIn).simplify
		outTiledDomain = outTiledDomain.setTupleName("T")
		debug('''Tile iteration domain «outTiledDomain»''')
		outTiledDomain
	}
	
	def private tileOuterDomain(List<ScopDimension> tileParameters, JNIISLSet outTiledDomain) {
		val scopRoot = node.scopRoot as GecosScopBlock
		var reducedTileDomain = outTiledDomain
		val tileIterators = new ArrayList<ScopDimension>()
		for (p : tileParameters) {
			val pos = reducedTileDomain.findDimByName(JNIISLDimType.isl_dim_set, p.symName)
			if (pos >= 0) {
				val set = reducedTileDomain.copy.projectIn(JNIISLDimType.isl_dim_set, pos)
				if (set.singleton) {
					debug('''Single tile on dimension «pos», removing iterator''')
					for (expr : EMFUtils.eAllContentsInstancesOf(node, IntExpression)) {
						val newExpr = expr.substitute(p, IntExpressionBuilder.constant(0))
						EMFUtils.substituteByNewObjectInContainer(expr, newExpr)
					}
					scopRoot.parameters.remove(p)
					p.symbol.containingScope.removeSymbol(p.symbol)
					reducedTileDomain = reducedTileDomain.projectOut(JNIISLDimType.isl_dim_set, pos, 1)
					reducedTileDomain = reducedTileDomain.setTupleName("T")
				} else {
					tileIterators.add(p)
				}			
			}
		}
		
		scopRoot.iterators.addAll(tileIterators)
		node = ISLCodegenUtils.encloseWithLoop(node, reducedTileDomain, tileIterators)
	}
}
