package fr.irisa.cairn.gecos.model.scop.sheduling

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject
import java.util.ArrayList
import java.util.Collection
import java.util.List
import com.google.common.collect.Lists
import java.util.HashMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopNode
import java.util.Map
import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionTransformer
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory

class ScopSlicingScheduler {
	
	
	Collection<GecosScopBlock> scops
	
	new (Collection<GecosScopBlock> scops) {
		this.scops = scops;
	}

	new (GecosProject p) {
		this.scops = EMFUtils.eAllContentsInstancesOf(p, GecosScopBlock);
	}

	List<String> parNames
	
	int TILESIZE = 32
	int UNROLL=2
		
	def buildOutputTileSlice(Iterable<JNIISLMap> deps, String name, int size) {
		var JNIISLMap res = null
		for (dep:deps ) {
			val nbIn = dep.getNbDim(JNIISLDimType.isl_dim_in)
			parNames = dep.domainNames.map[d | "P_"+d].toList
			val domNames = dep.domainNames
			var str = new ArrayList<String>()
			for (i:0..nbIn-1) {
				str+=''' «size»«parNames.get(i)» <= «domNames.get(i)»< «size»«parNames.get(i)»+«size»'''
				//str+=''' «parNames.get(i)» = «domNames.get(i)» '''
			}	
			val mapstr = '''
				«parNames» -> { 
					«dep.inputTupleName»«domNames»  : 
					«FOR s:str SEPARATOR "and"»«s»«ENDFOR»
					}
			'''
			 val map =JNIISLMap.buildFromString(JNIISLContext.ctx, mapstr)
			 if (res===null) {
				 res = map
			 } else { 
				res=  res.union(map)
			 }
		 }
		 res
	}

	def  insertSlicedOutput(JNIISLUnionMap prdg, String name, int size) {
		var outputs = prdg.copy.maps.filter[m | m.inputTupleName==name]
		
		val outSlice = buildOutputTileSlice(outputs, name, size) 
		return prdg.copy.addMap(outSlice);
	}
	

	def slice(JNIISLUnionMap prdg, String target) {
		
		println("** slicing for :"+target)
		println("Original PRDG :\n"+pp(prdg.copy))
		var extPrdg = prdg.copy
		println("PRDG with additional node :\n"+pp(extPrdg.copy))
		extPrdg= extPrdg.simpleHull
		println("Simplified PRDG :\n"+pp(extPrdg.copy))
		var exact = new JNIPtrBoolean
		val closurePrdg = extPrdg.transitiveClosure(exact).simpleHull.coalesce
		println("Closure on PRDG with additional node :\n"+pp(closurePrdg.copy))

		var sliceMap = new HashMap<String,JNIISLSet>
		val filtered = closurePrdg.copy.maps.filter[m|m.inputTupleName==target];
		println("Filtered PRDG with additional node :\n");
		filtered.forEach[m| println(pp(m))]
		for (fmap : filtered) {
			val domain = fmap.copy.getRange
			val tupleName = domain.copy.tupleName
			println('''   - Slicing iteration domain of statement '''+tupleName)
			for(edge :prdg.copy.maps) {
				val outputTupleName = edge.copy.outputTupleName
				if (outputTupleName==tupleName) {
					println('''        - Matching edge «edge.inputTupleName»-> «edge.outputTupleName» ''')
					val slice = fmap.copy.getRange().intersect(domain.copy).simpleHull.toSet
					println('''           - Matching edge «edge.inputTupleName»-> «edge.outputTupleName» ''')
					if (!sliceMap.containsKey(tupleName)) {
						sliceMap.put(tupleName,slice)
					} else {
						val previous = sliceMap.get(tupleName).union(slice)
						sliceMap.put(tupleName,previous)
					}
					println('''           - Sliced domain«sliceMap.get(tupleName)»''');

				}
			}
		}
		var slicedDomain = JNIISLUnionSet.buildEmpty(closurePrdg.copy.space)
		for (slice : sliceMap.values) {
			slicedDomain= slicedDomain.addSet(slice)
			
		}
		println("Sliced domain :\n"+pp(slicedDomain))
		slicedDomain 
	}

	def tileInnerDomain(JNIISLSet s, int tileSize) {
		val nbIn = s.copy.nbDims
		//var set= s.copy.addDims(JNIISLDimType.isl_dim_param,nbIn);
		var par = s.copy.parametersNames
		//par+=s.copy.indicesNames.map[d | "P_"+d].toList
		val iter = s.copy.indicesNames
		val str = '''«par» -> { «s.tupleName»«iter» : «iter.map[i| '''«tileSize»P_«i»<=«i»<=«tileSize»P_«i»+«tileSize-1»'''].reduce[p1,p2|p1+" and "+p2]»}'''
		JNIISLSet.buildFromString(JNIISLContext.ctx, str)
	}

	def tileOuterDomain(JNIISLSet s, int tileSizem, int unroll) {
		val nbIn = s.copy.nbDims
		val nbParam = s.copy.nbParams
		val startParam = nbParam - nbIn
		var newS = s.copy
		
		//for (offser:startParam..(nbParam-1)) {
			newS= newS.moveDims(JNIISLDimType.isl_dim_set,newS.copy.nbDims, JNIISLDimType.isl_dim_param,startParam,nbIn)
		//}
//		val par = s.copy.parametersNames.subList(0, Math.max(0,nbParam-nbIn))
//		val sched = s.copy.parametersNames.subList(nbParam-nbIn,nbParam)
//		var setiter = s.copy.indicesNames
//		setiter+=sched
//		var iter = s.copy.indicesNames
//		val str = '''«par» -> { «setiter» : «iter.map[i| '''«tileSize»P_«i»<«i»<=«tileSize»P_«i»+«tileSize-1»'''].reduce[p1,p2|p1+" and "+p2]»}'''
//		var res = JNIISLSet.buildFromString(JNIISLContext.ctx, str)
		println(newS)
		var res= newS.projectOut(JNIISLDimType.isl_dim_set, 0, nbIn)
		res = res.tupleName="T"
		res
	}
	
	def computeOutputFlowTile(Symbol s, GecosScopBlock root, List<String> names) {
		var flowOut = ScopAccessAnalyzer.computeFlowOut(s,root) 
		println("flowOut for "+s.name+" "+flowOut.copy)
		if(!flowOut.isEmpty) {
			val shadow_name="O_"+s.name
			flowOut = flowOut.tupleName =shadow_name
			
			names+= flowOut.tupleName 
			// tile flowOout
			val nbDims= flowOut.copy.nbDims-1
			for (dimension : 0..nbDims) {
				val name = root.iterators.get(dimension).name
				flowOut = flowOut.copy.setDimName(JNIISLDimType.isl_dim_set,dimension,name);
			}	
		}
		flowOut
	}
	
	

	Map<String,Symbol> map = new HashMap<String,Symbol>();
	

	def makeSymbolsUnique(ScopNode node) {
		val List<Symbol> syms = 	EMFUtils.eAllContentsInstancesOf(node, typeof(Symbol))	
		
		for (s : syms) {
			while (map.containsKey(s.name)) {
				s.name="_"+s.name
			}
			map.put(s.name, s);
		}
	}

	def void compute() {
		for (GecosScopBlock root : scops) {
			
			val liveOut =root.liveOutSymbols;
			var sliceOutputArrayNames = #[].toList 
			
			// need to add dummy reads
			val block = ScopUserFactory.scopBlock();
			block.children+=root.root
			root.root = block
			
			ScopUserFactory.scope=root.scope
			
			val sliceIterators = Lists.newArrayList(root.iterators.map[i|ScopUserFactory.dim("P_"+i.name)])
			root.parameters+=sliceIterators
			
			
			/*
			 * Add fake statements that copy a tile of the output array into a 
			 * dummy fake temporary which will be used as output for the 
			 * slicing stage 
			 */
			val params = Lists.newArrayList(root.parameters)
			val iterators = Lists.newArrayList(root.iterators)
			
			/* iterate over live-out symbols  */
			for (s : liveOut.filter[s|s.type.isArray] ) {
				// Build tile domain		
				println("Slicing array symbol \"+s.name"+"\"\n")	
				val flowOut = computeOutputFlowTile(s, root, sliceOutputArrayNames)
				if (!flowOut.copy.isEmpty) {
					val tiledOutput = tileInnerDomain(flowOut.copy, TILESIZE)
					
					// Add artifact copy statement to isolate tile output
					var shadow = s.copy
					shadow.name=tiledOutput.tupleName
					ISLCodegenUtils.dataMover(tiledOutput, block, s, shadow)
				}
			}
		
				

			println('''
			********************************
			2) Building PRDG 
			********************************
			''')
			val prdg = ScopISLConverter.getValueBasedDepenceGraph(root)   
			
			
			/*  
			 *  Insert guards into scop to represent slice domain restriction. 
			 *  This leads to inefficient SCoP, but we call isl codegen to 
			 *  simplify it later on. FIXME : should use unexpanded node for this.
			 */
			for (n:sliceOutputArrayNames) {
				val slicedSet =  slice(prdg,n)
				for (set: slicedSet.sets) {
					val stmt = root.getStatement(set.tupleName)				
					ISLScopConverter.guard(stmt, set)
				}
			}
			/*  Remove artifact copy statements introduced in first steps
			 */
			val artifactStmts = root.listAllStatements.filter[s|s.id.startsWith("O_")]
			for (s : artifactStmts) {
				s.parentScop.remove(s)
			}
			
			
		println('''
			***************************************************
			4) Insert Data motion code and contract local arrays
			****************************************************
			''')
			//
			DataMotionTransformer.insertDataMotion(root.root, root.root.listAllReferencedSymbols)
			//root.root.transform= TransformFactory.eINSTANCE.createScopTransform
				
	
			/* 
			 *  Add data motion code
			 */
			//DataMotionTransformer.insertReadCopy(root)
			
			/* 
			 *  
			 */
			val flowOut = computeOutputFlowTile(root.liveOutSymbols.get(0), root, sliceOutputArrayNames)
			println('''
			********************************
			5) Output tile 
			********************************
			''')
			val outTiledDomain = tileOuterDomain(flowOut,TILESIZE, UNROLL)
			println (outTiledDomain)
			
			println('''
			********************************
			7) Generate outer loop with «sliceIterators» as index
			********************************
			''')
			
			ISLCodegenUtils.encloseWithLoop(root.root, outTiledDomain, sliceIterators)
			println(root)
		}
	}
	
		
	
	
		def static pp(JNIISLMap m) {
		'''«m.parametersNames» -> {
		«
		FOR bmap:m.copy.basicMaps»«bmap»«
		ENDFOR
		»
		}
		'''		
	}

	def static pp(JNIISLUnionMap m) {
		'''
			«m.copy.space.getNameList(JNIISLDimType.isl_dim_param)» -> {
			«FOR map:m.copy.maps»
				«FOR bmap:map.copy.basicMaps»	«bmap»
				«ENDFOR»
			«ENDFOR»
			}
		'''		
		
	}

	def static pp(JNIISLUnionSet m) {
		'''
			«m.copy.space.getNameList(JNIISLDimType.isl_dim_param)» -> {
			«FOR map:m.copy.sets»
				«FOR bmap:map.copy.basicSets»	«bmap»
				«ENDFOR»
			«ENDFOR»
			}
		'''		
		
	}
	
		
}