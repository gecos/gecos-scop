package fr.irisa.cairn.gecos.model.scop.sheduling

import gecos.gecosproject.GecosProject
import java.util.Collection
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean
import fr.irisa.cairn.gecos.model.scop.ScopWrite

class ScopStatementClustering {

	static final boolean VERBOSE = true
	
	GecosProject p
	
	new(GecosProject p) {
		this.p=p
	}
	
	def private void debug(String string) {
		if (VERBOSE) {
			println("[ScopStatementClustering] "+ string)
		}
	}
	
	def overlap(ScopWrite w0, ScopWrite w1) {
	}
	
	def compute() {
		for (scop : EMFUtils.eAllContentsInstancesOf(p, GecosScopBlock)) {
			val prdg = ScopISLConverter.getValueBasedDepenceGraph(scop) .transitiveClosure(new JNIPtrBoolean)
			val syms = scop.listAllReferencedSymbols;
			for (sym : syms) {
				val writeStmts= scop.listAllStatements.filter[s| s.listAllWriteAccess.map[w|w.sym].contains(sym)]			 
				for (wrStmt : writeStmts) {
					val seed =wrStmt 
					val remainder = writeStmts.filter[s| s!=wrStmt].toList  
					
//					for (wr1 : remainder) {
//						ScopAccessAnalyzer.getArrayWriteFootprint()
//						if (wr1.listAllWriteAccess)
//					}
				}
			}
			 
		}
		// build transitive dep graph
		// find independant (and writing to different locations) statements 
		// with same target array symbol
		//  
	}
}