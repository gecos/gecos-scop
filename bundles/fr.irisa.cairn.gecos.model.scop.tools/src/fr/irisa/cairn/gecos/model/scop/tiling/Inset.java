package fr.irisa.cairn.gecos.model.scop.tiling;

import java.util.List;

import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLConstraint;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;

public class Inset {
		static JNIISLSet compute_inset(JNIISLSet domain, List<Integer> tsize) {
			RegisterTiler.debug("########################");
			RegisterTiler.debug("Computing inset for:");
			RegisterTiler.debug("-domain: " + domain);
			
			JNIISLSet inset = null;
			
			/*
			 * for each (outermost first) dim do:
			 *   -eliminate all innermost dims (from dim+1)
			 *   -compute_inset_internal
			 */
			for(int dim=0; dim<domain.getNbDims(); dim++) {
				JNIISLSet proj = domain.copy().eliminateDims(dim+1, domain.getNbDims()-dim-1);
				RegisterTiler.debug("   -eliminate(dim: "+ dim +"): " + proj);
				proj = compute_inset_1dim(proj, dim, tsize);
				RegisterTiler.debug("   -inset("+ dim +"): " + proj);
				if(inset==null) inset = proj;
				else inset = inset.intersect(proj);
			}
			
			RegisterTiler.debug("-inset: " + inset);
			RegisterTiler.debug("########################");
			
			return inset;
		}
	
		/**
		 * @author aelmouss
		 * @param domain
		 * @param dim
		 * @param tsize
		 * @return JNIISLSet representing the inset of {@link domain} along dimension {@link dim}.
		 */
		private static JNIISLSet compute_inset_1dim(JNIISLSet domain, int dim, List<Integer> tsize){
			JNIISLSet tmpset = JNIISLSet.buildEmptyLike(domain);
			
			for(JNIISLBasicSet bs : domain.getBasicSets()){
				JNIISLBasicSet tmpbs = JNIISLBasicSet.buildUniverse(bs.getSpace());
				RegisterTiler.debug("      -bs before : " + bs);
				for(JNIISLConstraint c : bs.getConstraints()) {
					RegisterTiler.debug("         -constraint: " + c);
					if(c.isEquality())
						RegisterTiler.debug("            ignored!!! is equality");
					//we only consider constraints(lb or ub) over the specified dim 
					else if(c.isLowerBound(JNIISLDimType.isl_dim_set, dim)) {
						c = shift_up(c, dim, tsize);
						RegisterTiler.debug("            Lower bound >>> : " + c);
					}
					else if(c.isUpperBound(JNIISLDimType.isl_dim_set, dim)) {
						c = shift_down(c, dim, tsize);
						RegisterTiler.debug("            Upper bound <<< : " + c);
					}
					else
						RegisterTiler.debug("            ignored!!! Not an UB nor LB for dim " + domain.getIndicesNames().get(dim));
					tmpbs = tmpbs.addConstraint(c);
				}
//				debug("      -bs after  : " + tmpbs);
				tmpset = tmpset.union(tmpbs.toSet());
			}
			
			return tmpset;
		}
		
		/**
		 * @author aelmouss
		 * @param lb
		 * @param dim
		 * @param tsize
		 * @return the constraint shifted up.
		 * 
		 * Assumes that JNIISLConstraint is always like ax+by+c >= 0
		 * and that Constraint {@link lb} is an LB of dim
		 */
		public static JNIISLConstraint shift_up(JNIISLConstraint lb, int dim, List<Integer> tsize) {
			JNIISLConstraint res = lb.copy();
			
			/* foreach outer index i in lb */
			for(int i=0; i<dim; i++) {
				long coef = lb.getCoefficient(JNIISLDimType.isl_dim_set, i);
				if(-coef > 0) { //we take the negation as c is an UB (e.g. i >= 1  <=>  i -1 >= 0) 
					res = res.setConstant(res.getConstant() + coef*(tsize.get(i)-1)); //! we consider that tile size are constant (not parameters) 
				}
			}  
			return res;
		}
		
		/**
		 * @author aelmouss
		 * @param ub
		 * @param dim
		 * @param tsize
		 * @return the constraint shifted up.
		 * 
		 * Assumes that JNIISLConstraint is always like ax+by+c >= 0
		 * and that Constraint {@link c} is an UB of dim
		 */
		public static JNIISLConstraint shift_down(JNIISLConstraint ub, int dim, List<Integer> tsize) {
			JNIISLConstraint res = ub.copy();
			
			/* foreach outer index i in ub */
			for(int i=0; i<dim; i++) {
				long coef = ub.getCoefficient(JNIISLDimType.isl_dim_set, i);
				if(coef < 0) {
					res = res.setConstant(res.getConstant() + coef*(tsize.get(i)-1)); //! we consider that tile size are constant (not parameters) 
				}
			} 
			res = res.setConstant(res.getConstant() - (tsize.get(dim)-1)); //! we consider that tile size are constant (not parameters) 
			return res;
		}
	}