package fr.irisa.cairn.gecos.model.scop.tiling;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import com.google.common.collect.HashMultimap;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter;
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLConstraint;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDataflowAnalysis;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule.JNIISLSchedulingOptions;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLVector;
import gecos.blocks.BasicBlock;

public class RegisterTiler {
	
	public static final String RLT_INSET_ANNOTATION = "__RLT_INSET__";
	private static final int DEFAULT_TILE_SIZE = 2;
	private static final boolean ANNOTATE_RLTKERNELS = false;
	private static final boolean DEBUG = true;
	
	private GecosScopBlock gscop;
	private List<Integer> tileSize;
	private JNIISLUnionSet originalDomain;
	private JNIISLUnionMap proximity;
	private JNIISLUnionMap validity;
	private JNIISLSet paramSet;
	
	static final void debug(String str) {
		if(DEBUG)
			System.out.println(str);
	}
	
	
	public RegisterTiler(GecosScopBlock scop, List<Integer> tileSize) {
		this.gscop = scop;
		this.tileSize = tileSize;
		
		
		debug("***********************************\n" +
			  "*  Computing Register Tiling for: *\n" +
			  "***********************************\n"+ gscop);

		originalDomain = ScopISLConverter.getAllDomains(scop);
		if(originalDomain == null) throw  new RuntimeException("domain is empty from scop: "+scop);
		JNIISLUnionMap _schedules = ScopISLConverter.getAllIdSchedules(scop);
		JNIISLUnionMap _writes = ScopISLConverter.getAllWriteAccessMaps(scop);
		JNIISLUnionMap _reads = ScopISLConverter.getAllReadAccessMaps(scop);
		
		validity = JNIISLDataflowAnalysis.computeMemoryBasedADA(originalDomain.copy(), _writes.copy(), _reads.copy(), _schedules.copy());
		if(validity == null) validity = JNIISLUnionMap.buildEmpty(originalDomain.getSpace().copy());
		proximity = JNIISLUnionMap.buildEmpty(originalDomain.getSpace().copy());
		paramSet = originalDomain.copy().getSetAt(0).paramSet();
		
		debug("\n-Original domain   : " + originalDomain);
		debug("-Original schedules: " + _schedules);
		debug("-Original writes   : " + _writes);
		debug("-Original reads    : " + _reads);
		debug("-Validity Map      : " + validity);
//		for(JNIISLMap m : validity.getMaps())
//			System.out.println(  m.simplify());
		debug("-Proximity Map     : " + proximity);
		debug("-Parameters        : " + paramSet);
		debug("---------------------");
	}
	
//	public RegisterTiler(GecosScopBlock scop, ISLUnionMap schedule_map, List<Integer> tileSize) {
//		this.gscop = scop;
//		this.tileSize = tileSize;
//	}
	
	/**
	 * inputs: 	JNIISLUnionSet originalDomain
	 *         	JNIISLUnionMap proximity
	 *			JNIISLUnionMap validity
	 *			JNIISLSet      paramSet
	 */
	public GecosScopBlock compute() {
		
		/* compute pluto schedule */
		JNIISLSchedulingOptions opts = new JNIISLSchedulingOptions();
		opts.setMax_coefficient(10);
		JNIISLSchedule schedule = JNIISLSchedule.computeSchedule(originalDomain.copy(), validity, proximity, opts);
								//JNIISLSchedule.computePlutoSchedule(originalDomain.copy(), validity, proximity);	
		debug("\n-Pluto schedule: " + schedule.getMap().copy());		

		debug("Generating code (Original)...");
		if(DEBUG) generate_ast(schedule.getMap().copy().intersectDomain(originalDomain.copy()), paramSet.copy(), null);
		
		/* get the transformed domain */
		JNIISLUnionMap sched_map = null;
		for(JNIISLMap m : schedule.getMap().copy().getMaps()) {
			m = m.setOutputTupleName(m.getInputTupleName());
			if(sched_map == null){
				sched_map = m.toUnionMap();
			}
			else sched_map = sched_map.addMap(m);
		}
		JNIISLUnionSet trans_domain = originalDomain.copy().apply(sched_map.copy());
		debug("-Transformed domain: "+trans_domain);
		debug("---------------------");
		
		/* Tile all bands */
		long nb_tile_loops = 0; //= max(domain.getSets().getNDim())
		for(JNIISLSet s : originalDomain.getSets()) {
			//XXX assuming that original domain has no scalar dimensions, otherwise we shouldn't count scalar dims !!
			//TODO use trans_domain instead and do not count scalar dimensions
			int nd = (int)s.getNbDims(); 
			if(nd > nb_tile_loops)
				nb_tile_loops = nd;
		}
		List<Integer> tsize = new ArrayList<Integer>(); 
		for(int i=0; i<nb_tile_loops; i++) {
			if(i<tileSize.size())
				tsize.add(tileSize.get(i));
			else
				tsize.add(DEFAULT_TILE_SIZE);
		}
		debug("Tiling bands with tiles size: "+tsize);
		schedule = tile_schedule(schedule, tsize);
		JNIISLUnionMap tiled_schedule_map = schedule.getMap().copy().intersectDomain(originalDomain.copy()); //XXX
		debug("-Tiled Schedule map (intersected with original domain) : " + tiled_schedule_map);
		
		debug("Generating code (Tiled)...");
		if(DEBUG) generate_ast(tiled_schedule_map.copy(), paramSet.copy(), null);
		debug("---------------------");
	
		return compute1(trans_domain, tiled_schedule_map, nb_tile_loops, tsize);
	}

	
	private GecosScopBlock compute1(JNIISLUnionSet domain, JNIISLUnionMap tiled_schedule_map, long nb_tile_loops, List<Integer> tsize) {
//		TODO get disjoint groups and for each group do the steps below
//		XXX use 2d+1 schedule makes the separation into groups easier XXX
//		debug("Extracting groups...");
//		List<JNIISLUnionSet> groups = get_groups(trans_domain.copy(), 0, (int)trans_domain.getSetAt(0).getNDim()-1);
//		for(JNIISLUnionSet group : groups) {
			JNIISLUnionSet group = domain.copy();//XXX
			debug("-group: "+group);
			
			/* Compute Domain Slice to be considered for full tile separation */
			debug("Extracting group slice...");
			JNIISLSet slice = get_domain_slice(group);
			if(slice == null)
				throw  new RuntimeException("domain slice is empty for group: "+group);
			debug("-Domain Slice: "+slice);
			
			/* Compute the inset of the domain slice */
			debug("Computing Inset...");
			JNIISLSet inset = Inset.compute_inset(slice, tsize);
			if(inset.isEmpty()) {
				System.err.println("[RLT]: ** inset is empty!");
			}
			JNIISLSet inset_init = inset.copy();//XXX
			inset = inset.insertDims(JNIISLDimType.isl_dim_set, inset.getNbDims(), tiled_schedule_map.getMapAt(0).getNbOuts() - inset.getNbDims());
			debug("-Inset: "+inset);
			debug("---------------------");		
//		}
			
			
		/* Compute option map */
		JNIISLUnionMap ASToptions = null;
		/* FIXME This won't work if the first nb_tile_loops dimensions contain a scalar dim 
		 * !!!! we shouldn't consider scalar dim !!!!
		 */
		for(long dim=nb_tile_loops; dim<inset.getNbDims(); dim++) { 
			JNIISLUnionMap unionMap = option_map_template(tiled_schedule_map.getMapAt(0), 
					String.format("{[] -> unroll[%d] }", dim)).intersectDomain(
					inset.copy().simplify()).toUnionMap();
			if(ASToptions == null)ASToptions = unionMap;
			else ASToptions = ASToptions.union(unionMap);
		}
		
//		for(long dim=0; dim<nb_tile_loops; dim++) { 
//			JNIISLUnionMap unionMap = option_map_template(tiled_schedule_map.getMapAt(0),
//					String.format("{[] -> separation_class[[%d]->[0]] }", dim)).intersectDomain(
//					inset.copy().eliminateDims(dim+1, inset.getNDim()-dim-1)).toUnionMap();
//			if(ASToptions == null)ASToptions = unionMap;
//			else ASToptions = ASToptions.union(unionMap);
//		}
		
//		for(long dim=0; dim<nb_tile_loops; dim++) { 
//			JNIISLUnionMap unionMap = option_map_template(tiled_schedule_map.getMapAt(0), String.format("{[] -> separate[%d] }", dim)).intersectDomain(inset.copy().simplify()).toUnionMap();
//			if(ASToptions == null)ASToptions = unionMap;
//			else ASToptions = ASToptions.union(unionMap);
//		}
		
//		ASToptions = ISLFactory.islUnionMap("[M,N] -> { " +
////				"[i0, i1, i2, i3, i4] -> unroll[4] : i0 <= -3 + M and i1 <= -3 + N and i0 >= 2 and i1 >= 2;" +
//				"[i0, i1, i2, i3, i4] -> unroll[3] : i0 <= -3 + M and i1 <= -3 + N and i0 >= 2 and i1 >= 2;" +
//				"[i0, i1, i2, i3, i4] -> unroll[2] : i0 <= -3 + M and i1 <= -3 + N and i0 >= 2 and i1 >= 2;" +
//				"[i0, i1, i2, i3, i4] -> separate[0];" +
//				"[i0, i1, i2, i3, i4] -> separate[1];" +
//				"[i0, i1, i2, i3, i4] -> separate[2];" +
//				"[i0, i1, i2, i3, i4] -> separate[3];" +
//				"}");

		debug("Generating Final Code...");
		JNIISLASTNode ast = generate_ast(tiled_schedule_map, paramSet, ASToptions);

		debug("AST to Scop Conversion...");
		JNIISLASTToScopConverter.adapt(gscop.getRoot(), ast);
		
		if(ANNOTATE_RLTKERNELS) {
			/* Annotate kernel */
			//FIXME problem when parameters have known size (i.e. no parameters) :
			// ScopStatement.getDomain(): domain is not correct when any of the 
			// surrounding loops has stride > 1. FIX: ScopNodeConstrintsBuilder.visitScopFor() 
			// the upper bound constraint must take the loop stride into account
			debug("Annotating kernel...");
			try {
				inset_init = inset_init.simplify().setTupleName("COM");
				long insetNDim = inset_init.getNbDims();
				debug("inset: "+inset_init);
				
				for(ScopStatement stmt : gscop.listAllStatements()) {
					
					JNIISLSet stmtDom = ScopISLConverter.getDomain(stmt).setTupleName("COM");
					if(stmtDom.getNbDims(JNIISLDimType.isl_dim_set)== insetNDim) {
						debug("->"+stmtDom);
						if(stmtDom.isSubset(inset_init)) {
							debug(stmt+ " is Annotated as \"RLT_INSET_ANNOTATION\"");
							stmt.setAnnotation(RLT_INSET_ANNOTATION, null);
						}
					}
				}
			} catch (Exception e) {
				System.err.println("[RegisterTiler] Failed to annotate kernel!");
				e.printStackTrace();
			}
		}
		
		debug("done");
		return gscop;
	}
	


//	JNIISLUnionSet prdgNodes2UnionSet(List<PRDGNode> nodes) {
//		JNIISLUnionSet domain = null;
//		for(PRDGNode n : nodes) {
////			debug("==="+i.getDomain());
//			if(domain == null)
//				domain = JNIISLUnionSet.fromSet(ISLNativeBinder.jniIslSetNoString(n.getDomain()));
//			else
//				domain = domain.union(JNIISLUnionSet.fromSet(ISLNativeBinder.jniIslSetNoString(n.getDomain())));
//		}
//		return domain;
//	}
//	
//	JNIISLUnionMap prdgEdges2UnionMap(List<PRDGEdge> edges) {
//		JNIISLUnionMap umap = null;
//		for(PRDGEdge e : edges) {
//			JNIISLMap map = ISLNativeBinder.jniIslMapNoString(e.getFunction().getMapping()).intersectDomain(ISLNativeBinder.jniIslSetNoString(e.getDomain()));
////			debug("==="+map);
//			if(umap == null)
//				umap = map.toUnionMap();
//			else
//				umap = umap.union(map.toUnionMap());
//		}
//		return umap;
//	}
//		
	
	/**
	 * Construct the list of all the possible embedded loop nests
	 * Assume that all sets in domain have the same number of dimensions
	 * and they contain only 1 basic set
	 */
	List<JNIISLUnionSet> get_groups(JNIISLUnionSet trans_domain, int first_dim, int last_dim) {
		if(first_dim >= last_dim)
			return null;
		
		Set<JNIISLSet> group = new HashSet<JNIISLSet>();
		for(JNIISLSet s : trans_domain.getSets()) {
			group.add(s);
		}
		
		HashMultimap<String , JNIISLSet> groups = get_groups_internal(group, "", first_dim, last_dim);
		
		List<JNIISLUnionSet> groups_list = new ArrayList<JNIISLUnionSet>();
		for(String key : groups.keySet()) {
			Set<JNIISLSet> gr = groups.get(key);
			debug("group("+key+"): "+gr);
			JNIISLUnionSet uset = null;
			for(JNIISLSet s : gr) {
				if(uset == null) uset = JNIISLUnionSet.fromSet(s);
				else uset = uset.addSet(s);
			}
			groups_list.add(uset);
		}
//		debug("groups_list: "+groups_list);
		
		return groups_list;
	}

	/**
	 * FIXME if dim is scalar and all the next dims are also scalar => s is common to all the groups 
	 * at the current level and shouldn't be considered in further separation steps
	 */
	private HashMultimap<String , JNIISLSet> get_groups_internal(Set<JNIISLSet> group, String key_prefix, int dim, int last_dim) {
		if(dim >= last_dim)
			return null;
		
		HashMultimap<String , JNIISLSet> groups = HashMultimap.create();
		for(JNIISLSet s : group) {
			JNIISLBasicSet bset = s.getBasicSetAt(0);
			Long scalar = getScalar(bset, dim);
			if(scalar != null) {
				//TODO if dim is scalar and all the next dims are also scalar => s is commun to all the groups at this level and shouldn't be considered in further separation steps  
				groups.put(key_prefix+scalar.toString(), s);
			}
			else {//one set doesn't start with a scalar dim => others group
				groups.put(key_prefix+"X", s);
			}
		}	
		dim ++;
		
		HashMultimap<String, JNIISLSet> tmp = HashMultimap.create(groups);
		for(String key : tmp.keySet()) {
			Set<JNIISLSet> gr = tmp.get(key);
			debug("***group("+key+"): "+gr);
			if(gr.size() <= 1)
				continue;
			HashMultimap<String, JNIISLSet> subGroups = get_groups_internal(gr, key, dim, last_dim);
			if(subGroups != null) {
				groups.removeAll(key);
				groups.putAll(subGroups);
			}
		}
		
		return groups;
	}

	/*
	 * Assumes that:
	 * (1) All domains in "group":
	 *   - have the same number of dimensions 
	 *   - belong to the same group as described by "get_groups()",
	 *    so that the outermost scalar dimensions are similar for all domains in the group
	 * 
	 * (2) Each ISLSet in "group" contains only 1 BasicSet (as if a statement appears in 2 
	 * disjoint domains then it should be considered as 2 different statements!)
	 * 
	 * @return: 
	 * ISLSet where only "the statements" at the deepest are executed, as:
	 *   Union(max_level(Intersect(max_level(domains)) - Union(remainder domains))) where
	 *   max_level get the set of domains containing the maximal number of independent dimensions  
	 * Note: all domains are brought to the same space where all the scalar dimensions (present in a 
	 *   max_level domain) have been projected out.
	 */
	private JNIISLSet get_domain_slice(JNIISLUnionSet group) {
		HashMultimap<Integer , JNIISLSet> levels = HashMultimap.create();
		int max_level = 0;
		
		/* group domains according to the nb of their independent dimensions */ 
		for(JNIISLSet s : group.getSets()) {
			//TODO we can also consider excluding dims which has a (small) constant nb of points (e.g. [i,j]: 0<=i<=2 and 0<=j<=N => do not consider i as independent dim)
			int nb_dim = get_nb_independent_dims(s.getBasicSetAt(0));
			levels.put(nb_dim, s);
			if(nb_dim > max_level)
				max_level = nb_dim;
			debug("   set: "+s+" has "+nb_dim+" independent dimensions");
		}
		 
		 /* "common_dom" is the intersection of all the domains at the max_level. 
		 * All domains are brought to the same space and all scalar dimensions have been removed
		 * before computing their intersection.
		 */
		JNIISLSet common_dom = null;
		Set<JNIISLSet> level = levels.get(max_level);
		debug("   ***Deepest level("+max_level+"): "+level);
		List<Integer> scalar_dims = new ArrayList<Integer>();
		boolean first = true;
		for(JNIISLSet set : level) {
			if(first) {
				first = false;
				for(int dim=0; dim<set.getNbDims(); dim++)
					if(isScalar(set, dim)) 
						scalar_dims.add(dim);
			}
			//as all sets at the same level in the same group have the same nb of scalar dims at the same positions 
			//TODO if this is not the case through an exception
			for(int dim : scalar_dims)
				set = set.projectOut(JNIISLDimType.isl_dim_set, dim, 1);
			set = set.setTupleName("COM");
			if(common_dom == null) common_dom = set;
			else common_dom = common_dom.intersect(set);
		}
		if(common_dom == null)
			return null;
		debug("   -common_dom: "+common_dom);
		
		/* "others_dom" is the union of all domains in the group which don't belong to max_level.
		 * All domains are brought to the same space as "common_dom" (all scalar dimensions similar 
		 * to "common_dom" have been removed) before computing their union.
		 */
		JNIISLSet others_dom = null;
		for(Integer key : levels.keySet()) {
			if(key == max_level) 
				continue;
			level = levels.get(key);
			debug("   ***level("+key+"): "+level);
			for(JNIISLSet set : level) {
				for(int dim : scalar_dims)
					set = set.projectOut(JNIISLDimType.isl_dim_set, dim, 1);
				set = set.setTupleName("COM");
				if(others_dom == null) others_dom = set;
				else others_dom = others_dom.union(set);
			}	
		}
		debug("   -others_dom: "+others_dom);
		
		/* exclude "others_dom" from "common_dom" in order to get the domain slice where
		 * only the statements at max_level are executed 
		 */	
		if(others_dom != null) {
			common_dom = common_dom.subtract(others_dom);
			debug("   -Common - others: "+common_dom);
			
			/* eliminate basic sets which have less independent dimensions */
			HashMultimap<Integer , JNIISLBasicSet> tmp = HashMultimap.create();
			max_level = 0;
			for(JNIISLBasicSet bs : common_dom.copy().getBasicSets()) {
				int nb_dim = get_nb_independent_dims(bs);
				tmp.put(nb_dim, bs);
				if(nb_dim > max_level)
					max_level = nb_dim;
				debug("      basic set: "+bs+" has "+nb_dim+" independent dimensions");
			}
			JNIISLSet slice = null;
			for(JNIISLBasicSet bs : tmp.get(max_level)) {
				if(slice == null) 
					slice = bs.toSet();
				else
					slice = slice.union(bs.toSet());
			}
			common_dom = slice;
		}
		
		return common_dom;
	}
	
	/* return null if the set dimension is not scalar, otherwise it return its value */
	private Long getScalar(JNIISLBasicSet bs, int dim) {
		for(JNIISLConstraint c : bs.getConstraints()) {
			if((c.isEquality()) && (c.getCoefficientAt(JNIISLDimType.isl_dim_set, dim) != 0)) { //equality involving dim
				for(int i=0; i<bs.getNbDims(JNIISLDimType.isl_dim_set); i++) {
					if(i == dim) continue;
					if(c.getCoefficientAt(JNIISLDimType.isl_dim_set, i) != 0) // another dim is involved by c => dim is not scalar
						return null;
				}
				return new Long(-c.getConstant());
			}
		}
		return null;
	}
	
	/* only the first basic set of the set is considered !! */
	private boolean isScalar(JNIISLSet set, int dim) {
		JNIISLBasicSet bset = set.getBasicSetAt(0);
		Long scalar = getScalar(bset,dim);
		if(scalar != null) {
			debug("dim "+dim+" scalar: "+scalar);
			return true;
		}
		debug("dim "+dim);
		return false;
	}

	/* 
	 * @return true if exists equality in "bset", involving "dim" but any of the outermost dimensions 
	 */
	private boolean isNewEquality(JNIISLBasicSet bset, int dim) {
		for(JNIISLConstraint c : bset.getConstraints()) {
			//TODO what about isl_dim_param ?? shall we consider it in involveDims ?
			if((c.isEquality()) && (c.getCoefficientAt(JNIISLDimType.isl_dim_set, dim) != 0) && !(c.involvesDims(JNIISLDimType.isl_dim_set, 0, dim))) {		
				return true;
			}
		}
		return false;	
	}
	
	/*
	 * return the nb of independent non-scalar dimensions
	 */
	private int get_nb_independent_dims(JNIISLBasicSet bset) {
		int nb_dim = 0;
		for(int dim=0; dim<bset.getNbDims(JNIISLDimType.isl_dim_set); dim++)
			if(!isNewEquality(bset, dim))
				nb_dim ++;
		return nb_dim;
	}
	

	private JNIISLSchedule tile_schedule(JNIISLSchedule schedule, List<Integer> tsize) {
		JNIISLVector vec = new JNIISLVector(ISLFactory.getContext(), tsize.size());
		for(int i=0; i<tsize.size(); i++) 
			vec = vec.setElement(i, tsize.get(i));
		
		return schedule;
	}

	private JNIISLMap option_map_template(JNIISLMap sched, String templ) {
		String str = String.format(templ);
		JNIISLMap option_tmp = ISLFactory.islMap(str);
		option_tmp = option_tmp.insertDims(JNIISLDimType.isl_dim_in, 0, sched.getNbOuts());
		option_tmp = option_tmp.insertDims(JNIISLDimType.isl_dim_param, 0, sched.getNbParams());
		for(int i=0; i<sched.getNbParams(); i++)
			option_tmp = option_tmp.setDimName(JNIISLDimType.isl_dim_param, i, sched.getSpace().getName(JNIISLDimType.isl_dim_param, i));

		return option_tmp;
	}
	
	private JNIISLASTNode generate_ast(JNIISLUnionMap sched_map, JNIISLSet params, JNIISLUnionMap options) {
		JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(params);
		JNIISLASTBuild.setAtomicUpperBound(build.getContext(), 1);//=>use min/max for upper bounds
		JNIISLASTBuild.setExploitNestedBounds(build.getContext(), 1);//=>try to eliminate unnecessary guards
		JNIISLASTBuild.setSeparationBounds(build.getContext(), 0); //ISL_AST_BUILD_SEPARATION_BOUNDS_IMPLICIT=1 ; _EXPLICIT=0
		if(options != null){
			debug("setting ast options as: "+options);
			build = build.setOptions(options);
		}
		JNIISLASTNode ast = JNIISLASTNode.buildFromSchedule(build, sched_map);
		
		debug(""+ast);
		return ast;
	}		
	
	public static boolean isRLTKernel(BasicBlock bb) {
		if(bb.getPragma() != null && bb.getPragma().getContent().contains(RLT_INSET_ANNOTATION))
			return true;
		else if(bb.getParent().getPragma() != null && bb.getParent().getPragma().getContent().contains(RLT_INSET_ANNOTATION)) {
//			AnnotationBuilder.pragma(bb, RegisterTiler.RLT_INSET_ANNOTATION);
			return true;
		}
		return false;
	}
	
	
//	private JNIISLUnionSet scop2UnionSet(GecosScopBlock scop) {
//		JNIISLUnionSet domain = null;
//		for(ScopStatement i : scop.getAllStatements()) {
////			debug("==="+i.getDomain());
//			if(domain == null)
//				domain = JNIISLUnionSet.fromSet(ISLNativeBinder.jniIslSetNoString(i.getDomain()));
//			else
//				domain = domain.union(JNIISLUnionSet.fromSet(ISLNativeBinder.jniIslSetNoString(i.getDomain())));
//		}
//		return domain;
//	}
	
	
//	private JNIISLUnionMap full_tiles_separation_and_unroll_map(JNIISLSet domain, JNIISLMap sched, int nb_tile_loops, int nb_loops, List<Integer> tsize, int class_num) {
////		JNIISLSet domain = sched.getDomain();
//		debug("domain: " + domain);
//		
//		JNIISLUnionMap option_map = JNIISLMap.islMapEmpty(option_map_template(sched.copy(), String.format("{[] -> separation_class[[%d]->[%d]] }", 0, class_num)).getSpace()).toUnionMap();//  option_tmp.copy().toUnionMap();
//		debug("option map before: " + option_map);
//		
//		//JNIISLUnionSet inset = JNIISLUnionSet.fromSet(JNIISLSet.buildEmptyLike(range));
//		JNIISLSet inset = JNIISLSet.buildUniverseLike(domain);
//		inset = inset.insertDim(JNIISLDimType.isl_dim_set, inset.getNDim(), sched.getNbOut() - inset.getNDim());//!
//		debug("inset before: " + inset);
//		debug("----------------------------");
//		
//		for(int dim=0; dim<domain.getNDim(); dim++) {
////			JNIISLSet proj = domain.copy().projectOut(JNIISLDimType.isl_dim_set, dim+1, domain.getNDim()-dim-1);
//			JNIISLSet proj = domain.copy().eliminateDims(dim+1, domain.getNDim()-dim-1); 
//
//			debug("   elim("+ dim +"): " + proj);
//			proj = compute_inset_internal(proj, dim, tsize);
//			proj = proj.insertDim(JNIISLDimType.isl_dim_set, proj.getNDim(), sched.getNbOut() - proj.getNDim());
//			debug("   inset("+ dim +"): " + proj);
//			
//			option_map = option_map.union(option_map_template(sched.copy(), String.format("{[] -> separation_class[[%d]->[%d]] }", dim, class_num)).intersectDomain(proj.copy()).toUnionMap());
//		
//			inset = inset.intersect(proj);
//		}
//		
////		inset = inset.insertDim(JNIISLDimType.isl_dim_set, inset.getNDim(), sched.getNbOut() - inset.getNDim());
//		debug("   inset: " + inset);
//		
//		for(int dim=0; dim<nb_loops; dim++) {
//			if(dim < nb_tile_loops) {
////				option_map = option_map.union(option_map_template(sched.copy(), String.format("{[] -> separation_class[[%d]->[%d]] }", dim, class_num)).intersectDomain(inset.copy()/*.removeDivs()*/).toUnionMap());
//			}
//			else {
////				option_map = option_map.union(option_map_template(sched.copy(), String.format("{[] -> unroll[%d] }", dim)).intersectDomain(inset.copy()/*.removeDivs()*/).toUnionMap());
//			}
//		}
//		
//		debug("----------------------------");
//		debug("option map after: " + option_map);
//		debug("inset after: " + inset.removeDivs());
//		return option_map;
//	}
	
}