package fr.irisa.cairn.gecos.model.scop.tiling;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;


/**
 * GeCos module applying Register Tiling on all SCoPs in a Gecos Project
 * 
 * @author aelmouss
 */
public class RegisterTilingModule {
	private EObject object;
	private List<GecosScopBlock> scopRoots;
	private List<Integer> tileSize;
	
	private static final boolean DEBUG = false;
	private static final void debug(String str) {
		if(DEBUG)
			System.out.println(str);
	}
	
	public RegisterTilingModule(GecosProject obj, List<Integer> tileSize) {
		this.object = obj;
		this.tileSize = tileSize;
	}
	
	public RegisterTilingModule(ProcedureSet obj, List<Integer> tileSize) {
		this.object = obj;
		this.tileSize = tileSize;
	}

	public void compute() {
		scopRoots = EMFUtils.eAllContentsInstancesOf(this.object, GecosScopBlock.class);
		
		for (fr.irisa.cairn.gecos.model.scop.GecosScopBlock scop : scopRoots) {
			try{
				if(DEBUG) debug("BEFORE: "+ScopPrettyPrinter.print(scop));
				
				/* apply register Tiling */
				GecosScopBlock GecosScopBlock = new RegisterTiler(scop, tileSize).compute();
				if(DEBUG) debug("AFTER: "+ScopPrettyPrinter.print(GecosScopBlock));
				
				/* replace old scop with the new one */
				scop.getParent().replace(scop, GecosScopBlock);
			}
			catch(Exception e) {
				throw new RuntimeException("Register Tiling failed for :"+scop, e);
			}
		}
	}
}