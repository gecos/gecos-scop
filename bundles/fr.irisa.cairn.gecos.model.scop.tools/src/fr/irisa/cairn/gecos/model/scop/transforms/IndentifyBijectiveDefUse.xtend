package fr.irisa.cairn.gecos.model.scop.transforms

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.analysis.*
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery

class IndentifyBijectiveDefUse {

	GecosProject proj;

	new(GecosProject p) {
		proj=p;
	}

	def compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(proj, typeof(GecosScopBlock))	
		for (scop : scops) {  
			val readmap = ScopAccessQuery.getReadSymbolsMap(scop.root)
			val writemap = ScopAccessQuery.getWrittenSymbolsMap(scop.root)
			
			for (s: writemap.keySet) {
				for (wr : writemap.get(s)) {
//					ScopISLConverter.getAccessMap()
				}
//				if (readmap.get(s).size==1 && writemap.get(s).size==1) {
//										
//				}
			}
		}
	}	
}
