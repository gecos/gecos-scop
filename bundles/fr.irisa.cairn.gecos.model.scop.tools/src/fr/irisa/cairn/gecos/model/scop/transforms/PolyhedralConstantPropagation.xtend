package fr.irisa.cairn.gecos.model.scop.transforms

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.analysis.*
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery
import javax.management.RuntimeErrorException
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import gecos.instrs.Instruction
import java.util.HashMap

class PolyhedralConstantPropagation {

	GecosProject proj;

	static boolean VERBOSE = true;

	new(GecosProject p) {
		proj = p;
	}

	static def debug(String string) {
		if(VERBOSE) println(string)
	}

	def extractDefSource(ScopStatement defStmt, Symbol target) {
		val defSources = defStmt.listAllWriteAccess.filter[wr|wr.sym == target]
		if(defSources.size > 1) throw new RuntimeException("Unsupported (multi write) SCOP copy " + defStmt);
		defSources.get(0)
	}

	def extractMapping(ScopStatement defStmt, ScopWrite defSource) {
		val ScopRead useAccess = defStmt.listAllReadAccess.get(0)
		val defMap = ScopISLConverter.getAccessMap(defSource)
		val useMap = ScopISLConverter.getAccessMap(useAccess)
		val copyMapping = useMap.copy.reverse.applyRange(defMap.copy).reverse
		copyMapping
	}

	def replaceUse(ScopRead use, Symbol defSym, JNIISLMap remapping) {
		debug("\t\t- ScopRead " + use);
		val readMap = ScopISLConverter.getAccessMap(use);
		debug("\t\t\t- read map" + readMap);
		val src = readMap.copy.applyRange(remapping.copy)
		val entries = src.copy.closedFormRelation
		debug("\t\t\t- true source " + src);

		if (entries.size == 1) {
			use.sym = defSym
			val funcs = entries.values.get(0)

			val exprs = ISLScopConverter.exprList(funcs, use.listAllEnclosingIterators, use.listAllParameters);
			use.indexExpressions.clear
			use.indexExpressions += exprs
			debug("\t\t\t- Changed into :" + use);
		} else if (entries.size > 1) {
			throw new RuntimeException("Cannot substitute access map (>1 basic map)");
		}

	}

	def compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(proj, typeof(GecosScopBlock))
		for (scop : scops) {
			var map = new HashMap<ScopStatement,Instruction> 
			for (stmt :scop.listAllStatements) {
				val cst =  ScopCopyIdentifier.isConstantValue(stmt);
				if (cst!==null) {
					map.put(stmt,cst	)		
				}
			}
			val prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(scop);
			//val revPrdg =  prdg.reverse;
			for (islmap :prdg.maps) {
				print(islmap)
				val defStmt = scop.getStatement(islmap.inputTupleName)
				val useStmt = scop.getStatement(islmap.outputTupleName)
				if (map.containsKey(defStmt)) {
					
				}
			}		
			
			
		}

	}


}
