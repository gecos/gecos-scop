package fr.irisa.cairn.gecos.model.scop.transforms

import gecos.gecosproject.GecosProject
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter

import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import gecos.core.Symbol
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.r2d2.gecos.framework.GSModule
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter

@GSModule(value="Remove Copy Statements")
class RemoveCopyStatements {

	GecosProject proj;

	static boolean VERBOSE = true;
	static boolean DEBUG = true;

	static def debug(String string) {
		if(DEBUG) println("[ScopCopyRemover] " + string)
	}

	static def log(String string) {
		if(VERBOSE) println("[ScopCopyRemover] " + string)
	}

	new(GecosProject p) {
		proj = p;
	}

	def extractDefSource(ScopStatement defStmt, Symbol target) {
		val defSources = defStmt.listAllWriteAccess.filter[wr|wr.sym == target]
		if(defSources.size > 1) throw new RuntimeException("Unsupported (multi write) SCOP copy " + defStmt);
		defSources.get(0)
	}

	def extractMapping(ScopStatement defStmt, ScopWrite defSource) {
		val ScopRead useAccess = defStmt.listAllReadAccess.get(0)
		val defMap = ScopISLConverter.getAccessMap(defSource)
		val useMap = ScopISLConverter.getAccessMap(useAccess)
		val copyMapping = useMap.copy.reverse.applyRange(defMap.copy).reverse
		copyMapping
	}

	def replaceUse(ScopRead use, Symbol defSym, JNIISLMap remapping) {
		debug("\t\t- ScopRead " + use);
		val old= use.root.toString
		val readMap = ScopISLConverter.getAccessMap(use);
		debug("\t\t\t- read map" + readMap);
		val src = readMap.copy.applyRange(remapping.copy)
		val entries = src.copy.closedFormRelation
		debug("\t\t\t- true source " + src);

		if (entries.size == 1) {
			use.sym = defSym
			val funcs = entries.values.get(0)

			val exprs = ISLScopConverter.exprList(funcs, use.listAllEnclosingIterators, use.listAllParameters);
			use.indexExpressions.clear
			use.indexExpressions += exprs
			log(old+" changed into :" + use.root);
		} else if (entries.size > 1) {
			throw new RuntimeException("Cannot substitute access map (>1 basic map)");
		}

	}

	def compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(proj, typeof(GecosScopBlock))
		for (scop : scops) {
			var id = 0;
			for (s : scop.listAllStatements) {
				s.id = "S" + (id++)
			}

			val stmts = scop.listAllStatements
			val copyStmts = stmts.filter[s|ScopCopyIdentifier.isCopy(s)].toList

			for (stmt : copyStmts) {
				log("Analyzing "+stmt)
				val readSym = stmt.listAllReadSymbols.get(0);
				val copySym = stmt.listAllWriteSymbols.get(0);
				val writes = scop.listAllWriteAccess.filter[wr|wr.sym == readSym]
				if (writes.size > 1) {
					debug("Cannot remove copy because of possible WAW on " + readSym);
					writes.map[s|s.root].toList.forEach[w|debug("\t" + w)];
					checkNoOverlapping(writes)
				} else {
					/*
					 * Even there we need to make sure the source is lexicographically 
					 * before the use we substitute. We use the PRDG for that.
					 */
					val defSource = extractDefSource(stmt, copySym)
					val copyMapping = extractMapping(stmt, defSource)

					val prdgSrcSym = ScopISLConverter.getWARDepenceGraph(readSym, scop);
					// we proceed only if there are no WAR dependency on the array 
					if (prdgSrcSym.copy.maps.size > 0) {
						// Here we should check if the WAR dependency does not intersects with 
						// the def-use relation we want to inline
						debug(" Cannot eliminate copy due to WAR dependency on "+ copySym);
						for (edge : prdgSrcSym.copy.maps) {
							if (edge.copy.domainIsWrapping) {
								val curry = edge.copy.curry
								debug("\t- WAR edge " + edge.copy);
								debug("\t- PRDG edge " + curry.copy);
								debug("\t- PRDG edge " + curry.copy.getRange.unwrap);
							}
							debug("\t\t- PRDG edge " + edge.copy.domainIsWrapping);
						}
					} else {
						debug("- No WAR dependency on" + copySym+ " for statement "+stmt);
						val prdgCopySym = ScopISLConverter.getValueBasedDepenceGraph(copySym,scop);
						//val prdgCopy2 = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(scop)
						var remove = true
						for (writeMap : prdgCopySym.copy.maps) {
							val useStmt = scop.getStatement(writeMap.inputTupleName)
							// FIXME
							debug("\t- " + copySym + " Used in : " + useStmt);
							val uses = useStmt.listAllReadAccess.filter[rd|rd.sym == copySym]
							if(uses.size>1) {
								log("- Multiple access to the same array " + uses+ " in "+useStmt)
								remove=false
							} else if(uses.size==1) {
								val use = uses.get(0)
								//FIXME check that read domain is included within write domain
								replaceUse(use, readSym, copyMapping)
								log("Removed copy " + stmt)
							}
						}
						if(remove) {
							// FIXME : make sure the symbol is not live-out
							stmt.parentScop.remove(stmt)
						}
					}
				}
			}
			id=0
			scop.relabelStatements
		}

	}

	def checkNoOverlapping(Iterable<ScopWrite> writes) {
		val footPrints = writes.map[w|ScopISLConverter.getAccessMap(w).getRange]
		for (x : footPrints) {
			for (y : footPrints) {
				if (x != y) {
					debug("Comparing " + x + " and " + y)
					if(!x.copy.isDisjoint(y.copy)) return true
				}
			}
		}
	}

}
