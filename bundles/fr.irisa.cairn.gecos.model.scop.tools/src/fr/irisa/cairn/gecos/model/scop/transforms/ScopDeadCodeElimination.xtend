package fr.irisa.cairn.gecos.model.scop.transforms

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject
import java.util.ArrayList
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopForLoop

class ScopDeadCodeElimination {

	static boolean VERBOSE = true;
	static boolean DEBUG= false;

	static def debug(String string) {
		if(DEBUG) println("[ScopDeadcodeElimination] "+string)
	}

	static def log(String string) {
		if(VERBOSE) println("[ScopDeadcodeElimination] "+string)
	}

	GecosProject p
	
	new(GecosProject p) {
		this.p=p;
	}

	def compute() {
		val scops= EMFUtils.eAllContentsInstancesOf(p, GecosScopBlock);
		for(scop : scops) {
			deadCodeElimination(scop);
		}		
	}
	
	def deadCodeElimination(GecosScopBlock root) {
		var deadStatements = new ArrayList<ScopStatement>()
		var vprdg = ScopISLConverter.getValueBasedDepenceGraph(root);
		var vclosure = vprdg.reverse.transitiveClosure(new JNIPtrBoolean)
		val liveOutSymbols = root.liveOutSymbols
		
		
		for(stmtName: root.listAllStatements.map[a|a.id]) {
			val wrStmt = root.getStatement(stmtName);
			if (wrStmt.listAllWriteAccess.size>1) 
				throw new UnsupportedOperationException("Multi-Write statements are not supported");
			val wr = wrStmt.listAllWriteAccess.get(0)
			val wrDomain = ScopISLConverter.getAccessMap(wr).getDomain
			var JNIISLSet useDomain = null; 
			debug("Analyzing statement "+wrStmt)
			for (edge : vclosure.copy.maps.filter[e|e.inputTupleName==stmtName]) {
				val rdStmt = root.getStatement(edge.outputTupleName);
				//debug("\t- "+edge.copy+ " with "+rdStmt)
				// sinking a live-out array/scalar
				var hasSink = false
				for (sink : rdStmt.listAllWriteAccess) {
					if ((liveOutSymbols).contains(sink.sym)) {
						hasSink =true
					}
				}
				if (hasSink) {
					val dom = edge.getDomain
					if (useDomain===null) {
						useDomain = dom;
					} else {
						useDomain = useDomain.union(dom)		
					}
					debug("-> current live  domain :"+useDomain.copy)
				}
			}
			if (useDomain===null) {
				if (!root.liveOutSymbols.contains(wr.sym)) {
					log("Statement "+wrStmt+ " is dead code ")
					deadStatements+=(wrStmt)
				}
			} else if (useDomain!==null && useDomain.copy.isStrictSubset(wrDomain)) {
				val deadDom = wrDomain.copy.subtract(useDomain.copy).gist(wrDomain.copy)
				log("Statement "+wrStmt+ " result is not used when "+deadDom+"" )
				ISLScopConverter.guard(wrStmt, useDomain.copy.gist(wrDomain.copy))	
			}
			
		}
		
		for(s : deadStatements) {
			s.parentScop.remove(s)
		}		
		
		removeEmptyBlocks(root)
		root.relabelStatements			
		
	}
	
	def dispatch removeEmptyBlocks(ScopNode node) {
		throw new UnsupportedOperationException("Not yet implemented")
	}
	def dispatch removeEmptyBlocks(ScopStatement node) {
		
	}

	def dispatch removeEmptyBlocks(GecosScopBlock node) {
		removeEmptyBlocks(node.root)
	}
	
	def dispatch removeEmptyBlocks(ScopGuard node) {
		if (node.thenNode!==null) removeEmptyBlocks(node.thenNode)
		if (node.elseNode!==null) removeEmptyBlocks(node.elseNode)
		if (node.thenNode===null && node.elseNode===null) {
			node.parentScop.remove(node)
			debug("Dead if block ")
		}
	}
	def dispatch removeEmptyBlocks(ScopBlock node) {
		if (node.children.size!=0) {
			val children = new ArrayList(node.children)
			for (c:children) {
				removeEmptyBlocks(c)	
			}
		}
		if (node.children.size==0) {
			debug("Dead block ")
			node.parentScop.remove(node)
		}
	}

	def dispatch removeEmptyBlocks(ScopForLoop node) {
		if (node.body!==null) {
			removeEmptyBlocks(node.body)
		}
		if (node.body===null) {
			debug("Loop with dead body ")
			
			//node.parentScop.remove(node)
		}
	}
//	def deadCodeEliminationAttic(GecosScopBlock root) {
//		var internalArrays =  root.listAllAccesses.map[a|a.sym].toSet
//		internalArrays-=root.liveInSymbols
//		internalArrays-= root.liveOutSymbols;
//		for (s : internalArrays) {
//			debug("Target symbol "+s)
//			val uses = ScopAccessAnalyzer.getUses(s, root);
//			debug("\t- Uses "+uses)
//			for (wr : uses.keySet) {
//				// refining at the array cell level
//				val wrStmt = wr.enclosingStatement
//				val reads = uses.get(wr)
//				val wrMap = ScopISLConverter.getAccessMap(wr)	
//				val wrDomain= wrMap.copy.getDomain;
//				debug("Write map "+wrMap)
//				debug("Write dom "+wrDomain)
//				for (r : reads.keySet) {
//					val map = reads.get(r)
//					val range = map.getDomain
//					debug("Read Map "+map)
//					debug("Read Dom "+range)
//				}
//				val domList = reads.keySet.map[m|reads.get(m).getDomain]
//				var rdRange = domList.reduce[p1, p2|p1.union(p2)]
//				debug("Read dom "+rdRange)
//			
//				if (rdRange===null) {
//					throw new UnsupportedOperationException("Domain is null !") 
//				} else {
//					if (rdRange.copy.isStrictSubset(wrDomain)) {
//						val deadDom = wrDomain.copy.subtract(rdRange.copy)
//						debug("Dead code lying here "+deadDom)
//						val block = ScopUserFactory.scopBlock()
//						wrStmt.parentScop.replace(wrStmt,block)
//						block.children+=wrStmt;
//						val g = ISLScopConverter.guard(wrStmt, rdRange.copy.gist(wrDomain.copy))	
//						block.children+=g;
//						debug("Guarding "+wrStmt)
//						debug("\t"+g)
//					}
//				}
//				
//			}
//			
//		}
//	}
	
}