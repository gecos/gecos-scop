package fr.irisa.cairn.gecos.model.scop.transforms

import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.r2d2.gecos.framework.GSModule
import gecos.core.Symbol
import org.eclipse.emf.ecore.EObject
import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.factory.IntExpressionBuilder
import org.polymodel.algebra.FuzzyBoolean
import org.polymodel.algebra.affine.AffineExpression
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import java.util.List

@GSModule("Create copies of scop nodes annotated with a scop_duplicate (index) pragma on the index dimension.")
class ScopDuplicateNode {
	static boolean VERBOSE = false

	static def debug(String string) {
		if(VERBOSE) println(string)
	}

	static boolean UNIQUEFY_LOCAL_SYMBOLS = true

	var EObject obj

	new(EObject obj) {
		this.obj = obj
	}

	def compute() {
		debug("ScopDuplicateNode")
		for (scop : EMFUtils.eAllContentsInstancesOf(obj, ScopNode)) {
			val commands = (scop.annotations?.get("GCS") as S2S4HLSPragmaAnnotation)?.directives?.filter(
				DuplicateNodePragma)
			if (commands !== null) {
				for (command : commands) {
					duplicate(scop, command.symbol, command.factor)
				}
			}
		}
	}

	def static duplicate(ScopNode node, Symbol index, int factor) {
		debug("Duplicating ScopNode: " + node)
		val originalDomain = ScopISLConverter.getDomain(node, node.scopRoot)
		val iterator = findAndStrideForLoop(node, index, factor)

		// Insert a block to contain the copies	
		val block = ScopUserFactory.scopBlock()
		node.parentScop.replace(node, block)
		block.children.add(node)
		
		var List<Symbol> localSymbols
		if (UNIQUEFY_LOCAL_SYMBOLS) {
			localSymbols = node.listAllReferencedSymbols
							   .filter[ScopAccessAnalyzer.computeFlowIn(it, node).isEmpty]
							   .filter[ScopAccessAnalyzer.computeFlowOut(it, node).isEmpty]
							   .toList
		}
		
		val strideDomain = ScopISLConverter.getDomain(node, node.scopRoot)
		val iteratorPos = node.listAllEnclosingIterators().indexOf(iterator)
		
		// Duplicate the statements
		for (var i = 1; i < factor; i++) {
			var duplicate = node.copy
			block.children.add(duplicate)

			shiftIterator(duplicate, iterator, i)
			if (verifyShiftValidity(duplicate, originalDomain, strideDomain, iteratorPos, i)) {
				if (UNIQUEFY_LOCAL_SYMBOLS) {
					uniquefyLocalSymbols(duplicate, localSymbols)
				}
			}
		}

		debug("Duplicated ScopNode: " + block)
		block
	}
	
	def private static findAndStrideForLoop(ScopNode node, Symbol index, int factor) {
		var forLoop = node.enclosingForLoop
		var ScopDimension iterator = null
		while (iterator === null) {
			if (forLoop === null) {
				throw new UnsupportedOperationException("Cannot find enclosing for loop with index " + index +
					" for duplication")
			}
			if (forLoop.iterator.symbol == index) {
				iterator = forLoop.iterator
			} else {
				forLoop = forLoop.enclosingForLoop
			}
		}
		if (forLoop.stride.constant != FuzzyBoolean.YES && (forLoop.stride as AffineExpression).constantTerm.coef != 1) {
			// TODO add support for stride by including it in shift iterator and guard conditions
			throw new UnsupportedOperationException("ScopDuplicate do not support duplication on strided loop " + forLoop)
		}
		
		forLoop.stride = IntExpressionBuilder.prod(forLoop.stride, IntExpressionBuilder.constant(factor))
		
		iterator
	}
	
	def private static shiftIterator(ScopNode node, ScopDimension iterator, int shift) {
		val newIterator = IntExpressionBuilder.add(IntExpressionBuilder.affine(iterator), shift)
		val exprList = EMFUtils.eAllContentsInstancesOf(node, IntExpression)
		for (expr : exprList) {
			val newExpr = expr.substitute(iterator, newIterator.copy)
			EMFUtils.substituteByNewObjectInContainer(expr, newExpr)
		}
	}
	
	def private static verifyShiftValidity(ScopNode node, JNIISLSet originalDomain, JNIISLSet strideDomain, int iteratorPos, int shift) {
		var iterStr = '''«IF strideDomain.nbParams > 0»«strideDomain.parametersNames» -> «ENDIF» { «(0..<strideDomain.nbDims).map['''c«it»''']» -> '''
		val str = '''«iterStr»«(0..<strideDomain.nbDims).map['''c«it»«IF it == iteratorPos» + «shift»«ENDIF»''']»:}'''
		val func = JNIISLMap.buildFromString(JNIISLContext.ctx, str)
		
		val shiftedDomain = strideDomain.copy.apply(func.copy)
		val validshiftedDomain = shiftedDomain.copy.intersect(originalDomain.copy)
		
		if (validshiftedDomain.isEmpty) {
			node.parentScop.remove(node)
			return false
		}
		
		var reducedGuard = validshiftedDomain.copy.gist(shiftedDomain.copy)
		if (!reducedGuard.plainIsUniverse) {
			reducedGuard = reducedGuard.copy.apply(func.copy.reverse)
			ISLScopConverter.guard(node, reducedGuard)
		}
		
		true
	}
	
	def private static uniquefyLocalSymbols(ScopNode node, List<Symbol> localSymbols) {
		val scope = (node.scopRoot as GecosScopBlock).scope
		for (sym : localSymbols) {			
			var nb = 0
			while (scope.lookup(sym.name + "_dup" + nb) !== null) {
				nb++
			}
			val copy = GecosUserCoreFactory.symbol(sym.name + "_dup" + nb, sym.type, scope)			
			
			EMFUtils.replaceAllRefsInContainement(node, sym, copy)
		}
	}
}
