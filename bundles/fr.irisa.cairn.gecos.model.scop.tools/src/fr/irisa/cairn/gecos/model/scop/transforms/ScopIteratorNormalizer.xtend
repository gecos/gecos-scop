package fr.irisa.cairn.gecos.model.scop.transforms

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.gecosproject.GecosProject
import java.util.ArrayList
import fr.irisa.r2d2.gecos.framework.GSModule

@GSModule("Normalize Scop iterators before new transformations")
class ScopIteratorNormalizerPass {
	GecosProject proj;

	new(GecosProject proj) {
		this.proj = proj;
	}

	def compute() {
		val scops = EMFUtils.eAllContentsInstancesOf(proj, typeof(GecosScopBlock))
		for (scop : scops) {
			new ScopIteratorNormalizer(scop).compute()
		}
	}
}

class ScopIteratorNormalizer {
	val GecosScopBlock scop
	val oldIterators = new ArrayList<ScopDimension>()
	val newIterators = new ArrayList<ScopDimension>()
	var depth = 0

	new(GecosScopBlock scop) {
		this.scop = scop
		oldIterators.addAll(scop.iterators)
	}

	def void compute() {
		scop.iterators.removeAll(oldIterators)
		scop.scope.symbols.removeAll(oldIterators.map[it.symbol])
		for (i : oldIterators) {
			newIterators.add(ScopIteratorGenerator.newIterator(scop))
		}

		normalize(scop.root)
	}

	def dispatch void normalize(ScopNode node) {}

	def dispatch void normalize(ScopBlock block) {
		block.children.forEach[normalize]
	}

	def dispatch void normalize(ScopForLoop loop) {
		loop.substitute(loop.iterator, newIterators.get(depth))
		depth++
		normalize(loop.body)
		depth--
	}
}
