/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.scop.transforms;
  
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;


import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.*;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class ScopCopyIdentifier {

	%include { sl.tom } 

	%include { sl.tom }
	%include { List.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 

	%include { algebra_terminals.tom }
	%include { algebra_affine.tom }
	%include { algebra_quasiaffine.tom }
	%include { algebra_domains.tom }
	%include { scop_terminals.tom } 
	  
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_control.tom }
	%include { gecos_logical.tom }

	//%include { gecos_basic.tom } 
	%include { scop_nodes.tom }     
	%include { extractor_extraction.tom }  

	public static boolean isCopy(ScopStatement _stmt) {
		%match (_stmt){ 
			stmt(name, InstL(set(iwrite(wsym,rdindex), iread(rsym,wrindex)))) -> {
				return (`wsym)!=(`rsym);
			}
		}
		return false;
	}

	public static Instruction isConstantValue(ScopStatement _stmt) {
		%match (_stmt){ 
			stmt(name, InstL(set(iwrite(wsym,rdindex), v@ival(_)))) -> {
				return (`v);
			}
			stmt(name, InstL(set(iwrite(wsym,rdindex), v@fval(_)))) -> {
				return (`v);
			}
		}   
		return null;
	}

}