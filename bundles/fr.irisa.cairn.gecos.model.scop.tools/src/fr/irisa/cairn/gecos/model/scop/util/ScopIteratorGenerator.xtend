package fr.irisa.cairn.gecos.model.scop.util

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory

class ScopIteratorGenerator {

	/** Creates a new iterator and adds it to the scop root.
	 *  Requires node to be rooted.
	 * 
	 * @param node Scop in which to create the iterator
 	 * @return created iterator
	 */
	def static newIterator(ScopNode node) {
		val root = node.scopRoot as GecosScopBlock
		val scope = root.scope
		
		var depth = root.iterators.size
		while (scope.lookup(getName(depth)) !== null)
			depth++
		
		ScopUserFactory.scope = scope
		val iterator = ScopUserFactory.dim(getName(depth))
		root.iterators.add(iterator)
		
		iterator
	}
	
	/** Find or create n available iterators relative to node.
	 *  Requires node to be rooted.
	 * 
	 * @param node Scop used as relative root to find or create iterators
	 * @param n number of required iterators
 	 * @return list of free iterators
	 */
	def static freeIterators(ScopNode node, int n) {
		val iterators = node.listAllFreeIterators
		if (iterators.size >= n)
			return iterators.take(n)
				
		while (iterators.size < n)
			iterators.add(ScopIteratorGenerator.newIterator(node))
		
		return iterators
	}
	
	val static names = #['i', 'j', 'k', 'l', 'm', 'n']
	
	def private static getName(int depth) {
		if (names.size > depth) names.get(depth) else 'c' + depth
	}
}