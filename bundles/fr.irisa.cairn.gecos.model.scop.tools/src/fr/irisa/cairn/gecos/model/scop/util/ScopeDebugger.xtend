package fr.irisa.cairn.gecos.model.scop.util

import fr.irisa.cairn.tools.ecore.query.EMFUtils
import gecos.blocks.BasicBlock
import gecos.blocks.CompositeBlock
import gecos.blocks.ForBlock
import gecos.blocks.IfBlock
import gecos.blocks.SimpleForBlock
import gecos.core.ISymbolUse
import gecos.core.ITypedElement
import gecos.core.Scope
import gecos.core.Symbol
import gecos.gecosproject.GecosProject
import gecos.types.Type
import java.io.PrintStream
import java.util.HashSet
import org.eclipse.emf.ecore.EObject

class ScopeDebugger {

	GecosProject proj
	
	PrintStream outfile
	
	new(GecosProject p, String filename) {
		proj=p
		outfile= new PrintStream(filename);
	}

	def compute() {
		outfile.append('''
		«FOR ps : proj.listProcedureSets»
		«visit(ps.scope)»
		«FOR p : ps.listProcedures»
		function «p.symbolName» {
			«visit(p.body)»
		}
		«ENDFOR»
		«ENDFOR»
		''');
		outfile.close
	}
	
	def dispatch visit(Scope i) {
		'''
		Scope «i.hashCode» {
			«IF i.symbols.size>0»
			symbols {
				«FOR s:i.symbols SEPARATOR ";"»
					«visit(s)»
				«ENDFOR»	
			} 
			«ENDIF»
			«IF i.types.size>0» 
			types {
				«FOR s:i.types SEPARATOR ";"»
					«visit(s)»
				«ENDFOR»	
			}
			«ENDIF»
		}
		'''
	}

	def dispatch visit(EObject o) {
		'''
		«o.eClass.name» {
			uses { 
				«FOR s : o.eCrossReferences.filter(Symbol) SEPARATOR ","» «visit(s)» «ENDFOR»
				«FOR s : o.eCrossReferences.filter(Type) SEPARATOR ","» «visit(s)» «ENDFOR»
			}
			«FOR s : o.eContents»
			«visit(s)»
			«ENDFOR»
		}
		'''	
	}


	def dispatch visit(IfBlock  i) {
		'''
		IfBlock {
			«visit(i.testBlock)»
			«IF i.elseBlock!=null»«visit(i.elseBlock)»«ENDIF»
			«visit(i.thenBlock)»
		}
		'''	
	}
	def dispatch visit(ForBlock  i) {
		'''
		ForBlock«i.number» {
			«visit(i.initBlock)»
			«visit(i.testBlock)»
			«visit(i.stepBlock)»
			«visit(i.bodyBlock)»
		}
		'''	
	}
	
	def dispatch visit(SimpleForBlock  i) {
		'''
		SimpleForBlock«i.number» [«i.iterator»] {
			«visit(i.initBlock)»
			«visit(i.testBlock)»
			«visit(i.stepBlock)»
			«visit(i.bodyBlock)»
		}
		'''	
	}
	
	def dispatch visit(BasicBlock  i) {
		var smap = new HashSet<String>;	
		for (suse : EMFUtils.eAllContentsInstancesOf(i, typeof(ISymbolUse))) {
			smap.add(visit(suse.usedSymbol).toString)			
		}
		var tmap = new HashSet<String>;	
		for (tuse : EMFUtils.eAllContentsInstancesOf(i, typeof(ITypedElement))) {
			if(tuse.type!==null)
				tmap.add(visit(tuse.type).toString)			
		}
		'''
		BB«i.number»{
			used_symbols = «smap.toString»
			used_types = «tmap.toString»		
		}
		'''
	}

	def dispatch visit(CompositeBlock  i) {
		if(i.scope!=null) {
			'''
			CompositeBlock«i.number» {
				«visit(i.scope)»
				«FOR s : i.children»«visit(s)»«ENDFOR»
			}
			'''
		} else {
			'''
			CompositeBlock«i.number» {
				«FOR s : i.children»«visit(s)»«ENDFOR»
			}
			'''
		}
	}

	def dispatch String visit(Type i) {
		'''«i»_«i.hashCode%256»'''
	}

	def dispatch String visit(Symbol i) {
		'''«i.name»_«i.hashCode%256»:«visit(i.type)»'''
	}
	
}