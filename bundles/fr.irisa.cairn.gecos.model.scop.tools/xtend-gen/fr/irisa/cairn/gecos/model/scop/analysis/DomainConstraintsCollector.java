package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.javatuples.Pair;
import org.polymodel.algebra.AlgebraUserFactory;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;

@SuppressWarnings("all")
public class DomainConstraintsCollector {
  private Map<ScopForLoop, IntConstraintSystem> icsMap = new HashMap<ScopForLoop, IntConstraintSystem>();
  
  private final List<IntConstraintSystem> list = new ArrayList<IntConstraintSystem>();
  
  private final List<IntConstraintSystem> nlist = new ArrayList<IntConstraintSystem>();
  
  private final ScopNode root;
  
  public DomainConstraintsCollector(final ScopNode root) {
    this.root = root;
  }
  
  public static Pair<List<IntConstraintSystem>, List<IntConstraintSystem>> compute(final ScopNode node, final ScopNode root) {
    Pair<List<IntConstraintSystem>, List<IntConstraintSystem>> _xblockexpression = null;
    {
      if ((root == null)) {
        throw new UnsupportedOperationException(("Cannot compute contraints when root is null " + node));
      }
      if ((node == null)) {
        throw new UnsupportedOperationException("Cannot compute contraints when node is null ");
      }
      ScopNode validRoot = null;
      ScopNode _parentScop = root.getParentScop();
      boolean _tripleEquals = (_parentScop == null);
      if (_tripleEquals) {
        validRoot = node.getScopRoot();
      } else {
        validRoot = root.getParentScop();
      }
      if ((validRoot == null)) {
        throw new UnsupportedOperationException(("Cannot compute valid root " + validRoot));
      }
      final DomainConstraintsCollector collector = new DomainConstraintsCollector(validRoot);
      boolean _notEquals = (!Objects.equal(node, root));
      if (_notEquals) {
        collector.collectConstraints(node, null);
      }
      _xblockexpression = new Pair<List<IntConstraintSystem>, List<IntConstraintSystem>>(collector.list, collector.nlist);
    }
    return _xblockexpression;
  }
  
  protected void _collectConstraints(final ScopNode node, final ScopNode prev) {
    final ScopNode parent = node.getParentScop();
    boolean _notEquals = (!Objects.equal(parent, this.root));
    if (_notEquals) {
      this.collectConstraints(parent, node);
    }
  }
  
  protected void _collectConstraints(final GecosScopBlock node, final ScopNode prev) {
    IntConstraintSystem _context = null;
    if (node!=null) {
      _context=node.getContext();
    }
    EList<IntConstraint> _constraints = null;
    if (_context!=null) {
      _constraints=_context.getConstraints();
    }
    int _size = _constraints.size();
    boolean _notEquals = (_size != 0);
    if (_notEquals) {
      this.list.add(node.getContext());
    }
  }
  
  protected void _collectConstraints(final ScopGuard node, final ScopNode prev) {
    int _size = node.getPredicate().size();
    boolean _greaterThan = (_size > 1);
    if (_greaterThan) {
      ScopNode _thenNode = node.getThenNode();
      boolean _equals = Objects.equal(prev, _thenNode);
      if (_equals) {
        this.nlist.addAll(IntExpressionBuilder.negate(node.getPredicate()));
      } else {
        this.list.addAll(IntExpressionBuilder.negate(node.getPredicate()));
      }
    } else {
      ScopNode _thenNode_1 = node.getThenNode();
      boolean _equals_1 = Objects.equal(prev, _thenNode_1);
      if (_equals_1) {
        this.list.addAll(node.getPredicate());
      } else {
        this.nlist.addAll(node.getPredicate());
      }
    }
    final ScopNode parent = node.getParentScop();
    boolean _notEquals = (!Objects.equal(parent, this.root));
    if (_notEquals) {
      this.collectConstraints(parent, node);
    }
  }
  
  protected void _collectConstraints(final ScopForLoop node, final ScopNode prev) {
    boolean _containsKey = this.icsMap.containsKey(node);
    boolean _not = (!_containsKey);
    if (_not) {
      final IntExpression stride = node.getStride();
      FuzzyBoolean _isConstant = stride.isConstant();
      boolean _equals = Objects.equal(_isConstant, FuzzyBoolean.YES);
      boolean _not_1 = (!_equals);
      if (_not_1) {
        throw new UnsupportedOperationException("Stride is not constant. This results in non-affine constraints and cannot be handled by the polyhedral model.");
      }
      final long strideValue = stride.toAffine().getConstantTerm().getCoef();
      if (((node.getLB() == null) || (node.getUB() == null))) {
        throw new UnsupportedOperationException(("Inconsistent ScopForNode node :" + node));
      }
      IntExpression lb2 = node.getLB().<IntExpression>copy();
      IntExpression ub2 = node.getUB().<IntExpression>copy();
      IntConstraint eq = null;
      final ScopDimension iterator = node.getIterator();
      if (((!Objects.equal(stride.isEquivalent(IntExpressionBuilder.constant(1)), FuzzyBoolean.YES)) && (!Objects.equal(stride.isEquivalent(IntExpressionBuilder.constant((-1))), FuzzyBoolean.YES)))) {
        if ((lb2 instanceof AffineExpression)) {
          final IntExpression expr = IntExpressionBuilder.sum(IntExpressionBuilder.prod(IntExpressionBuilder.affine(IntExpressionBuilder.term((-1))), lb2), IntExpressionBuilder.affine(iterator)).simplify();
          eq = IntExpressionBuilder.eq(IntExpressionBuilder.qaffine(IntExpressionBuilder.mod(((AffineExpression) expr), strideValue)), IntExpressionBuilder.affine(IntExpressionBuilder.term(0)));
        }
      }
      final IntConstraint lb = IntExpressionBuilder.ge(IntExpressionBuilder.affine(IntExpressionBuilder.term(iterator)), lb2);
      final IntConstraint ub = IntExpressionBuilder.le(IntExpressionBuilder.affine(IntExpressionBuilder.term(iterator)), ub2);
      final IntConstraintSystem intConstraintSystem = AlgebraUserFactory.intConstraintSystem();
      final List<IntConstraint> l = intConstraintSystem.getConstraints();
      l.add(lb);
      l.add(ub);
      if ((eq != null)) {
        l.add(eq);
      }
      this.icsMap.put(node, intConstraintSystem);
    }
    this.list.add(this.icsMap.get(node));
    final ScopNode parent = node.getParentScop();
    boolean _notEquals = (!Objects.equal(parent, this.root));
    if (_notEquals) {
      this.collectConstraints(parent, node);
    }
  }
  
  public void collectConstraints(final ScopNode node, final ScopNode prev) {
    if (node instanceof GecosScopBlock) {
      _collectConstraints((GecosScopBlock)node, prev);
      return;
    } else if (node instanceof ScopForLoop) {
      _collectConstraints((ScopForLoop)node, prev);
      return;
    } else if (node instanceof ScopGuard) {
      _collectConstraints((ScopGuard)node, prev);
      return;
    } else if (node != null) {
      _collectConstraints(node, prev);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(node, prev).toString());
    }
  }
}
