package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineFactory;

/**
 * Compute the original schedule expressions for a given node in a GecosScop.
 * @author amorvan
 */
@SuppressWarnings("all")
public class IdentityScheduleBuilder {
  public static class IDScheduleBuilderVisitor {
    private int cpt;
    
    protected int currentDepth;
    
    private ScopNode previousNode;
    
    protected boolean bottomUp = true;
    
    private List<IntExpression> schedule;
    
    private int maxDeth;
    
    public List<IntExpression> getSchedule() {
      return this.schedule;
    }
    
    public IDScheduleBuilderVisitor(final int maxDeth) {
      this.maxDeth = maxDeth;
      ArrayList<IntExpression> _arrayList = new ArrayList<IntExpression>(maxDeth);
      this.schedule = _arrayList;
    }
    
    protected void _visit(final ScopAccess g) {
      final ScopNode s = g.getParentScop();
      this.visit(s);
    }
    
    protected void _visit(final ScopStatement g) {
      if (this.bottomUp) {
        int _numberOfEnclosingDimension = g.getNumberOfEnclosingDimension();
        int depth = (_numberOfEnclosingDimension * 2);
        if ((depth > this.maxDeth)) {
          throw new RuntimeException();
        }
        boolean _isEmpty = this.schedule.isEmpty();
        if (_isEmpty) {
          for (int i = 0; (i < (depth + 1)); i++) {
            this.schedule.add(i, IntExpressionBuilder.constant((-1)));
          }
          for (int i = (depth + 1); (i < this.maxDeth); i++) {
            this.schedule.add(i, IntExpressionBuilder.constant(0));
          }
        }
        this.cpt = 0;
        this.currentDepth = depth;
        this.previousNode = g;
        this.visit(g.getParentScop());
      } else {
        this.cpt++;
      }
    }
    
    protected void _visit(final ScopBlock g) {
      if (this.bottomUp) {
        int indexOf = g.getChildren().indexOf(this.previousNode);
        if ((indexOf == (-1))) {
          throw new RuntimeException();
        }
        this.bottomUp = false;
        for (int i = 0; (i < indexOf); i++) {
          {
            ScopNode node = g.getChildren().get(i);
            this.visit(node);
          }
        }
        this.bottomUp = true;
        this.previousNode = g;
        this.visit(g.getParentScop());
      } else {
        EList<ScopStatement> _listAllStatements = g.listAllStatements();
        for (final ScopNode n : _listAllStatements) {
          this.visit(n);
        }
      }
    }
    
    protected void _visit(final ScopGuard g) {
      if (this.bottomUp) {
        ScopNode _elseNode = g.getElseNode();
        boolean _tripleEquals = (_elseNode == this.previousNode);
        if (_tripleEquals) {
          this.bottomUp = false;
          this.visit(g.getParentScop());
          this.bottomUp = true;
        }
        this.previousNode = g;
        this.visit(g.getParentScop());
      } else {
        this.visit(g.getThenNode());
        ScopNode _elseNode_1 = g.getElseNode();
        boolean _tripleNotEquals = (_elseNode_1 != null);
        if (_tripleNotEquals) {
          this.visit(g.getElseNode());
        }
      }
    }
    
    protected void _visit(final ScopForLoop g) {
      if (this.bottomUp) {
        this.schedule.set(this.currentDepth, IntExpressionBuilder.constant(this.cpt));
        QuasiAffineExpression expr = QuasiAffineFactory.eINSTANCE.createQuasiAffineExpression();
        FuzzyBoolean _isConstant = g.getStride().isConstant();
        boolean _notEquals = (!Objects.equal(_isConstant, FuzzyBoolean.YES));
        if (_notEquals) {
          IntExpression _stride = g.getStride();
          String _plus = ("Expecting constant stride instead of " + _stride);
          throw new UnsupportedOperationException(_plus);
        }
        int _xifexpression = (int) 0;
        long _coef = g.getStride().toAffine().getConstantTerm().getCoef();
        boolean _greaterThan = (_coef > 0);
        if (_greaterThan) {
          _xifexpression = 1;
        } else {
          _xifexpression = (-1);
        }
        final int sign = _xifexpression;
        expr.getTerms().add(IntExpressionBuilder.mul(IntExpressionBuilder.affine(g.getIterator()), sign));
        this.schedule.set((this.currentDepth - 1), expr.simplify());
        int _currentDepth = this.currentDepth;
        this.currentDepth = (_currentDepth - 2);
        this.cpt = 0;
        this.previousNode = g;
        this.visit(g.getParentScop());
      } else {
        this.cpt++;
      }
    }
    
    protected void _visit(final GecosScopBlock g) {
      this.schedule.set(this.currentDepth, IntExpressionBuilder.constant(this.cpt));
    }
    
    public void visit(final ScopNode g) {
      if (g instanceof GecosScopBlock) {
        _visit((GecosScopBlock)g);
        return;
      } else if (g instanceof ScopAccess) {
        _visit((ScopAccess)g);
        return;
      } else if (g instanceof ScopBlock) {
        _visit((ScopBlock)g);
        return;
      } else if (g instanceof ScopForLoop) {
        _visit((ScopForLoop)g);
        return;
      } else if (g instanceof ScopGuard) {
        _visit((ScopGuard)g);
        return;
      } else if (g instanceof ScopStatement) {
        _visit((ScopStatement)g);
        return;
      } else {
        throw new IllegalArgumentException("Unhandled parameter types: " +
          Arrays.<Object>asList(g).toString());
      }
    }
  }
  
  private int maxDepth;
  
  private List<IntExpression> schedule;
  
  private ScopNode g;
  
  private ScopNode root;
  
  public IdentityScheduleBuilder(final ScopNode g) {
    this.g = g;
    this.root = g.getScopRoot();
    this.maxDepth = this.computeMaxDepth();
  }
  
  public IdentityScheduleBuilder(final ScopNode g, final ScopNode root) {
    this.g = g;
    this.root = root;
    this.maxDepth = this.computeMaxDepth();
  }
  
  public static List<IntExpression> getSchedule(final ScopNode g) {
    return new IdentityScheduleBuilder(g).getSchedule();
  }
  
  public static List<IntExpression> getSchedule(final ScopNode g, final ScopNode root) {
    return new IdentityScheduleBuilder(g).getSchedule();
  }
  
  private int computeMaxDepth() {
    int maxDepth = 0;
    EList<ScopStatement> _listAllStatements = this.root.listAllStatements();
    for (final ScopStatement stmt : _listAllStatements) {
      maxDepth = Math.max(maxDepth, stmt.getNumberOfEnclosingDimension());
    }
    int nbSchedDimMax = ((2 * maxDepth) + 1);
    return nbSchedDimMax;
  }
  
  public List<IntExpression> getSchedule() {
    if ((this.schedule == null)) {
      IdentityScheduleBuilder.IDScheduleBuilderVisitor privateVisitor = new IdentityScheduleBuilder.IDScheduleBuilderVisitor(this.maxDepth);
      privateVisitor.visit(this.g);
      this.schedule = privateVisitor.getSchedule();
    }
    return this.schedule;
  }
}
