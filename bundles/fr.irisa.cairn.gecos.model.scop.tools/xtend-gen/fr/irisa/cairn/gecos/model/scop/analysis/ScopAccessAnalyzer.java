package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopAccessAnalyzer {
  private final static boolean VERBOSE = true;
  
  protected static JNIISLSet _getTypeFootprint(final Type t) {
    throw new UnsupportedOperationException(("Cannot derive shape domain for type " + t));
  }
  
  protected static JNIISLSet _getTypeFootprint(final BaseType t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{[]}");
    return JNIISLSet.buildFromString(_builder.toString());
  }
  
  protected static JNIISLSet _getTypeFootprint(final ArrayType t) {
    ArrayList<String> header = new ArrayList<String>();
    ArrayList<String> constraint = new ArrayList<String>();
    int index = 0;
    EList<Instruction> _sizes = t.getSizes();
    for (final Instruction s : _sizes) {
      {
        boolean _matched = false;
        if (s instanceof IntInstruction) {
          _matched=true;
          final String dim = ("i" + Integer.valueOf(index));
          header.add(dim);
          long _value = ((IntInstruction)s).getValue();
          String _plus = ((("0<=" + dim) + "<") + Long.valueOf(_value));
          constraint.add(_plus);
        }
        if (!_matched) {
          throw new UnsupportedOperationException(("Cannot derive size of array type " + t));
        }
        index++;
      }
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{[");
    {
      boolean _hasElements = false;
      for(final String id : header) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        _builder.append(id);
      }
    }
    _builder.append("]:");
    {
      boolean _hasElements_1 = false;
      for(final String id_1 : constraint) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(" and ", "");
        }
        _builder.append(id_1);
      }
    }
    _builder.append("}");
    return JNIISLSet.buildFromString(_builder.toString());
  }
  
  /**
   * compute the access foot print (all array cells accessed in that Scop)
   */
  public static JNIISLSet getArrayAccessFootprint(final Symbol sym, final ScopNode n) {
    JNIISLSet _xblockexpression = null;
    {
      JNIISLSet set = null;
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        {
          final Function1<ScopAccess, Boolean> _function = (ScopAccess a) -> {
            Symbol _sym = a.getSym();
            return Boolean.valueOf(Objects.equal(_sym, sym));
          };
          final Iterable<ScopAccess> symrefs = IterableExtensions.<ScopAccess>filter(s.listAllAccesses(), _function);
          for (final ScopAccess a : symrefs) {
            {
              final JNIISLMap map = ScopISLConverter.getAccessMap(a, n);
              if ((set == null)) {
                set = map.getRange();
              } else {
                set = set.union(map.getRange());
              }
            }
          }
        }
      }
      _xblockexpression = set;
    }
    return _xblockexpression;
  }
  
  /**
   * compute the access foot print (all array cells accessed in that Scop)
   */
  public static JNIISLUnionMap getArrayWriteFootprint(final Symbol sym, final ScopNode n) {
    return ScopISLConverter.getAllWriteAccessMaps(n, sym);
  }
  
  /**
   * Checks is an given array has all its cell overwritten when executing the
   * SCoP given a argument.
   */
  public static boolean isFullyOverwritten(final Symbol sym, final ScopNode n) {
    boolean _xblockexpression = false;
    {
      final JNIISLSet footprint = ScopAccessAnalyzer.getArrayAccessFootprint(sym, n);
      JNIISLSet declShape = ScopAccessAnalyzer.getTypeFootprint(sym.getType());
      declShape = declShape.setTupleName(sym.getName());
      InputOutput.<JNIISLSet>println(declShape);
      InputOutput.<JNIISLSet>println(footprint);
      final JNIISLSet sub = declShape.subtract(footprint.copy());
      _xblockexpression = sub.isEmpty();
    }
    return _xblockexpression;
  }
  
  public static HashSet<Symbol> getAllReadOnlyArrays(final ScopNode n) {
    HashSet<Symbol> _xblockexpression = null;
    {
      HashSet<Symbol> used = new HashSet<Symbol>();
      HashSet<Symbol> writes = new HashSet<Symbol>();
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        EList<ScopAccess> _listAllAccesses = s.listAllAccesses();
        for (final ScopAccess a : _listAllAccesses) {
          {
            Symbol _sym = a.getSym();
            used.add(_sym);
            boolean _matched = false;
            if (a instanceof ScopWrite) {
              _matched=true;
              Symbol _sym_1 = ((ScopWrite)a).getSym();
              writes.add(_sym_1);
            }
          }
        }
      }
      used.removeAll(writes);
      _xblockexpression = used;
    }
    return _xblockexpression;
  }
  
  public static HashSet<Symbol> getAllWriteOnlyArrays(final ScopNode n) {
    HashSet<Symbol> _xblockexpression = null;
    {
      HashSet<Symbol> used = new HashSet<Symbol>();
      HashSet<Symbol> reads = new HashSet<Symbol>();
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        EList<ScopAccess> _listAllAccesses = s.listAllAccesses();
        for (final ScopAccess a : _listAllAccesses) {
          {
            Symbol _sym = a.getSym();
            used.add(_sym);
            boolean _matched = false;
            if (a instanceof ScopRead) {
              _matched=true;
              Symbol _sym_1 = ((ScopRead)a).getSym();
              reads.add(_sym_1);
            }
          }
        }
      }
      used.removeAll(reads);
      _xblockexpression = used;
    }
    return _xblockexpression;
  }
  
  public static JNIISLSet computeFlowIn(final Symbol sym, final ScopNode root) {
    JNIISLUnionSet flowIn = null;
    final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, sym);
    boolean _isEmpty = reads.isEmpty();
    if (_isEmpty) {
      flowIn = reads.range();
    } else {
      final JNIISLUnionMap rawDeps = ScopISLConverter.getRAWDepenceGraph(sym, root);
      final JNIISLUnionMap readsSourcedInScop = reads.copy().intersectDomain(rawDeps.domain());
      flowIn = reads.subtract(readsSourcedInScop).range();
    }
    int _nbSets = flowIn.getNbSets();
    boolean _greaterThan = (_nbSets > 1);
    if (_greaterThan) {
      throw new UnsupportedOperationException("Inconsistent input flow information");
    } else {
      int _nbSets_1 = flowIn.getNbSets();
      boolean _equals = (_nbSets_1 == 0);
      if (_equals) {
        return JNIISLSet.buildFromString("{ : 1 = 0 }");
      }
    }
    return flowIn.getSets().get(0);
  }
  
  public static JNIISLSet computeFlowOut(final Symbol sym, final ScopNode node) {
    final JNIISLUnionSet flowOut = ScopAccessAnalyzer.computeFlowOutMap(sym, node).range();
    int _nbSets = flowOut.getNbSets();
    boolean _greaterThan = (_nbSets > 1);
    if (_greaterThan) {
      throw new UnsupportedOperationException("Inconsistent output flow information");
    } else {
      int _nbSets_1 = flowOut.getNbSets();
      boolean _equals = (_nbSets_1 == 0);
      if (_equals) {
        return JNIISLSet.buildFromString("{ : 1 = 0 }");
      }
    }
    return flowOut.getSets().get(0);
  }
  
  public static JNIISLUnionMap computeFlowOutMap(final Symbol sym, final ScopNode node) {
    JNIISLUnionMap flowOut = null;
    ScopNode _scopRoot = node.getScopRoot();
    final GecosScopBlock root = ((GecosScopBlock) _scopRoot);
    boolean _contains = root.getLiveOutSymbols().contains(sym);
    if (_contains) {
      flowOut = ScopAccessAnalyzer.getArrayWriteFootprint(sym, node);
    } else {
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(node, sym);
      boolean _isEmpty = writes.isEmpty();
      if (_isEmpty) {
        flowOut = writes;
      } else {
        final JNIISLUnionSet rawInDeps = ScopISLConverter.getRAWDepenceGraph(sym, node).range();
        JNIISLUnionSet rawAllDeps = ScopISLConverter.getRAWDepenceGraph(sym, root).range();
        rawAllDeps = ScopISLConverter.moveDimsToParams(rawAllDeps, node.listAllEnclosingIterators());
        final JNIISLUnionSet rawOutDeps = rawAllDeps.subtract(rawInDeps);
        flowOut = writes.intersectDomain(rawOutDeps);
      }
    }
    return flowOut;
  }
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (ScopAccessAnalyzer.VERBOSE) {
      _xifexpression = InputOutput.<String>println(("[ScopAccessAnalyzer] " + string));
    }
    return _xifexpression;
  }
  
  public static HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> getDefUses(final ScopNode root) {
    HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> _xblockexpression = null;
    {
      final HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> useDefs = ScopAccessAnalyzer.getUseDefs(root);
      HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> res = new HashMap<ScopWrite, Map<ScopRead, JNIISLMap>>();
      Set<ScopRead> _keySet = useDefs.keySet();
      for (final ScopRead use : _keySet) {
        {
          if ((use == null)) {
            throw new RuntimeException("NYI");
          }
          final Map<ScopWrite, JNIISLMap> defs = useDefs.get(use);
          Set<ScopWrite> _keySet_1 = defs.keySet();
          for (final ScopWrite def : _keySet_1) {
            {
              if ((def == null)) {
                throw new RuntimeException("NYI");
              }
              boolean _containsKey = res.containsKey(def);
              boolean _not = (!_containsKey);
              if (_not) {
                HashMap<ScopRead, JNIISLMap> _hashMap = new HashMap<ScopRead, JNIISLMap>();
                res.put(def, _hashMap);
              }
              res.get(def).put(use, defs.get(def).reverse());
            }
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> getUseDefs(final ScopNode root) {
    try {
      try {
        HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> res = new HashMap<ScopRead, Map<ScopWrite, JNIISLMap>>();
        HashMap<String, ScopRead> readMap = new HashMap<String, ScopRead>();
        HashMap<String, ScopWrite> writeMap = new HashMap<String, ScopWrite>();
        final EList<ScopStatement> stmts = root.listAllStatements();
        final Function1<ScopStatement, String> _function = (ScopStatement s) -> {
          return s.getId();
        };
        final List<String> stntNmes = ListExtensions.<ScopStatement, String>map(stmts, _function);
        final JNIISLUnionMap prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(root);
        for (final ScopStatement s : stmts) {
          {
            int offset = 0;
            ScopAccessAnalyzer.debug(("Statement " + s));
            EList<ScopRead> _listAllReadAccess = s.listAllReadAccess();
            for (final ScopRead read : _listAllReadAccess) {
              {
                final Symbol targetSym = read.getSym();
                String _id = s.getId();
                String _plus = (_id + "_RD");
                int _plusPlus = offset++;
                final String label = (_plus + Integer.valueOf(_plusPlus));
                ScopAccessAnalyzer.debug(((("- read " + read) + " in statement ") + s));
                final Function1<JNIISLMap, Boolean> _function_1 = (JNIISLMap e) -> {
                  String _inputTupleName = e.getInputTupleName();
                  return Boolean.valueOf(Objects.equal(_inputTupleName, label));
                };
                final Iterable<JNIISLMap> edge = IterableExtensions.<JNIISLMap>filter(prdg.getMaps(), _function_1);
                for (final JNIISLMap e : edge) {
                  {
                    final String srcLabel = e.getOutputTupleName();
                    final String srcId = srcLabel.split("_WR")[0];
                    final Function1<ScopStatement, Boolean> _function_2 = (ScopStatement src) -> {
                      String _id_1 = src.getId();
                      return Boolean.valueOf(Objects.equal(_id_1, srcId));
                    };
                    final ScopStatement srcStmt = ((ScopStatement[])Conversions.unwrapArray(IterableExtensions.<ScopStatement>filter(stmts, _function_2), ScopStatement.class))[0];
                    ScopAccessAnalyzer.debug(("  - source statement :" + srcStmt));
                    boolean _containsKey = res.containsKey(read);
                    boolean _not = (!_containsKey);
                    if (_not) {
                      HashMap<ScopWrite, JNIISLMap> _hashMap = new HashMap<ScopWrite, JNIISLMap>();
                      res.put(read, _hashMap);
                    }
                    final Function1<ScopWrite, Boolean> _function_3 = (ScopWrite w) -> {
                      Symbol _sym = w.getSym();
                      return Boolean.valueOf(Objects.equal(_sym, targetSym));
                    };
                    Iterable<ScopWrite> _filter = IterableExtensions.<ScopWrite>filter(srcStmt.listAllWriteAccess(), _function_3);
                    for (final ScopWrite write : _filter) {
                      {
                        if ((write == null)) {
                          throw new RuntimeException("NYI");
                        }
                        res.get(read).put(write, e);
                      }
                    }
                  }
                }
              }
            }
          }
        }
        return res;
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception e = (Exception)_t;
          throw e;
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> getUses(final Symbol sym, final ScopNode root) {
    try {
      try {
        HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> res = new HashMap<ScopWrite, Map<ScopRead, JNIISLMap>>();
        final JNIISLUnionMap prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(root);
        JNIISLUnionMap flow = prdg.reverse();
        List<JNIISLMap> _maps = flow.getMaps();
        for (final JNIISLMap edge : _maps) {
          {
            final ScopStatement wrStmt = root.getStatement(edge.getInputTupleName());
            int _size = wrStmt.listAllWriteAccess().size();
            boolean _greaterThan = (_size > 1);
            if (_greaterThan) {
              throw new UnsupportedOperationException("MultiWrite statement are not (yet) supported in getUses");
            }
            final ScopWrite wr = wrStmt.listAllWriteAccess().get(0);
            boolean _containsKey = res.containsKey(wr);
            boolean _not = (!_containsKey);
            if (_not) {
              HashMap<ScopRead, JNIISLMap> _hashMap = new HashMap<ScopRead, JNIISLMap>();
              res.put(wr, _hashMap);
            }
            final ScopStatement rdStmt = root.getStatement(edge.getOutputTupleName());
            EList<ScopRead> _listAllReadAccess = rdStmt.listAllReadAccess();
            for (final ScopRead rd : _listAllReadAccess) {
              res.get(wr).put(rd, edge.copy());
            }
          }
        }
        return res;
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception e = (Exception)_t;
          throw e;
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static Map<ScopRead, JNIISLMap> getUsesfor(final ScopWrite wr, final ScopNode root) {
    Map<ScopRead, JNIISLMap> _xblockexpression = null;
    {
      ScopStatement _enclosingStatement = wr.getEnclosingStatement();
      String _plus = ((("Use maps for " + wr) + " in ") + _enclosingStatement);
      InputOutput.<String>println(_plus);
      _xblockexpression = ScopAccessAnalyzer.getUses(wr.getSym(), root).get(wr);
    }
    return _xblockexpression;
  }
  
  public static HashMap<ScopRead, JNIISLMap> getLastUsefor(final ScopWrite wr, final ScopNode root) {
    HashMap<ScopRead, JNIISLMap> _xblockexpression = null;
    {
      ScopStatement _enclosingStatement = wr.getEnclosingStatement();
      String _plus = ((("Last use map for " + wr) + " in ") + _enclosingStatement);
      InputOutput.<String>println(_plus);
      HashMap<ScopRead, JNIISLMap> res = new HashMap<ScopRead, JNIISLMap>();
      final Map<ScopRead, JNIISLMap> uses = ScopAccessAnalyzer.getUses(wr.getSym(), root).get(wr);
      if ((uses != null)) {
        Set<Map.Entry<ScopRead, JNIISLMap>> _entrySet = uses.entrySet();
        for (final Map.Entry<ScopRead, JNIISLMap> use : _entrySet) {
          {
            final JNIISLMap useMap = use.getValue().lexMax();
            res.put(use.getKey(), useMap);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static HashMap<ScopRead, JNIISLMap> getFirstUsefor(final ScopWrite wr, final ScopNode root) {
    HashMap<ScopRead, JNIISLMap> _xblockexpression = null;
    {
      ScopStatement _enclosingStatement = wr.getEnclosingStatement();
      String _plus = ((("First use map for " + wr) + " in ") + _enclosingStatement);
      InputOutput.<String>println(_plus);
      HashMap<ScopRead, JNIISLMap> res = new HashMap<ScopRead, JNIISLMap>();
      final Map<ScopRead, JNIISLMap> uses = ScopAccessAnalyzer.getUses(wr.getSym(), root).get(wr);
      if ((uses != null)) {
        Set<Map.Entry<ScopRead, JNIISLMap>> _entrySet = uses.entrySet();
        for (final Map.Entry<ScopRead, JNIISLMap> use : _entrySet) {
          {
            final JNIISLMap useMap = use.getValue().lexMin();
            res.put(use.getKey(), useMap);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLSet getTypeFootprint(final Type t) {
    if (t instanceof BaseType) {
      return _getTypeFootprint((BaseType)t);
    } else if (t instanceof ArrayType) {
      return _getTypeFootprint((ArrayType)t);
    } else if (t != null) {
      return _getTypeFootprint(t);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t).toString());
    }
  }
}
