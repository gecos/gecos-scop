package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.analysis.Substitute;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ScopCollapser {
  private GecosProject proj;
  
  public ScopCollapser(final GecosProject p) {
    this.proj = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsFirstInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      this.collapse(scop);
    }
  }
  
  protected ScopUnexpandedStatement _collapseStmt(final ScopInstructionStatement stmt, final ScopNode n) {
    ScopUnexpandedStatement _xblockexpression = null;
    {
      final JNIISLSet dom = ScopISLConverter.getDomain(stmt, n);
      final JNIISLMap sched = ScopISLConverter.getIdSchedule(stmt, n);
      ScopUnexpandedStatement res = ScopUserFactory.unexpandedStmt(stmt.listAllEnclosingIterators(n), dom.toString(), sched.toString());
      res.setId(stmt.getId());
      EList<Instruction> _children = res.getChildren();
      EList<Instruction> _children_1 = stmt.getChildren();
      Iterables.<Instruction>addAll(_children, _children_1);
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected ScopUnexpandedStatement _collapseStmt(final ScopUnexpandedStatement stmt, final ScopNode n) {
    throw new UnsupportedOperationException("Not yet implemented");
  }
  
  public void collapse(final ScopNode n) {
    final Iterable<ScopInstructionStatement> stmts = Iterables.<ScopInstructionStatement>filter(n.listAllStatements(), ScopInstructionStatement.class);
    ScopBlock res = ScopUserFactory.scopBlock();
    Substitute.replace(n, res);
    for (final ScopInstructionStatement s : stmts) {
      EList<ScopNode> _children = res.getChildren();
      ScopUnexpandedStatement _collapseStmt = this.collapseStmt(s, n);
      _children.add(_collapseStmt);
    }
  }
  
  public ScopUnexpandedStatement collapseStmt(final ScopInstructionStatement stmt, final ScopNode n) {
    if (stmt instanceof ScopUnexpandedStatement) {
      return _collapseStmt((ScopUnexpandedStatement)stmt, n);
    } else if (stmt != null) {
      return _collapseStmt(stmt, n);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(stmt, n).toString());
    }
  }
}
