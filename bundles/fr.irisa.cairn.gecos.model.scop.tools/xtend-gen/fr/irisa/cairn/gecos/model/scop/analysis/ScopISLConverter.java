package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.DomainConstraintsCollector;
import fr.irisa.cairn.gecos.model.scop.analysis.IdentityScheduleBuilder;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDataflowAnalysis;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import gecos.annotations.AnnotatedElement;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ArrayType;
import gecos.types.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.javatuples.Pair;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.prettyprinter.algebra.ISLPrettyPrinter;

@SuppressWarnings("all")
public class ScopISLConverter {
  private static ISLPrettyPrinter pp = new ISLPrettyPrinter();
  
  public static JNIISLSet getContext(final ScopNode node) {
    return ScopISLConverter.getDomain(node, node).paramSet();
  }
  
  public static JNIISLUnionMap getMemoryBasedDepenceGraph(final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeMemoryBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getMemoryBasedDepenceGraph(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(s, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(s, root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeMemoryBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getWAWDepenceGraph(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(s, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(s, root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeWAWMemoryBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getWARDepenceGraph(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(s, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(s, root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeWARMemoryBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getRAWDepenceGraph(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(s, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(s, root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeRAWMemoryBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getValueBasedDepenceGraph(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(s, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(s, root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getValueBasedDepenceGraph(final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getValueBasedDepenceGraphWithExplicitRead(final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedulesWithExplicitRead(null, root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomainsWithExplicitRead(root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMapsWithExplicitRead(root, null);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMapsWithExplicitRead(root, null);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getValueBasedDepenceGraphBySymbol(final ScopNode root, final Symbol s) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap schedules = ScopISLConverter.getAllIdSchedules(root);
      final JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(root);
      final JNIISLUnionMap reads = ScopISLConverter.getAllReadAccessMaps(root, s);
      final JNIISLUnionMap writes = ScopISLConverter.getAllWriteAccessMaps(root, s);
      final JNIISLUnionMap prdg = JNIISLDataflowAnalysis.computeValueBasedADA(domains.copy(), writes, reads, schedules);
      _xblockexpression = prdg;
    }
    return _xblockexpression;
  }
  
  public static JNIISLMap getIdSchedule(final ScopNode node, final ScopNode root) {
    JNIISLMap _xblockexpression = null;
    {
      final Function1<ScopDimension, String> _function = (ScopDimension i) -> {
        return i.getSymName();
      };
      final List<String> indices = ListExtensions.<ScopDimension, String>map(node.listAllEnclosingIterators(root), _function);
      final Function1<ScopDimension, String> _function_1 = (ScopDimension i) -> {
        return i.getSymName();
      };
      final List<String> params = ListExtensions.<ScopDimension, String>map(root.listAllParameters(), _function_1);
      final List<IntExpression> exprs = IdentityScheduleBuilder.getSchedule(node, root);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(params);
      _builder.append(" -> { ");
      _builder.append(indices);
      _builder.append(" -> ");
      final Function1<IntExpression, String> _function_2 = (IntExpression e) -> {
        int _indexOf = exprs.indexOf(e);
        return ("o" + Integer.valueOf(_indexOf));
      };
      List<String> _map = ListExtensions.<IntExpression, String>map(exprs, _function_2);
      _builder.append(_map);
      _builder.append(": ");
      {
        boolean _hasElements = false;
        for(final IntExpression e : exprs) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(" and ", "");
          }
          int _indexOf = exprs.indexOf(e);
          String _plus = ("o" + Integer.valueOf(_indexOf));
          String _plus_1 = (_plus + "=");
          String _print = ScopISLConverter.pp.print(e);
          String _plus_2 = (_plus_1 + _print);
          _builder.append(_plus_2);
        }
      }
      _builder.append("}");
      final String str = _builder.toString();
      JNIISLMap schedule = JNIISLMap.buildFromString(str);
      final JNIISLSet domain = ScopISLConverter.getDomain(node, root);
      schedule = schedule.intersectDomain(domain);
      _xblockexpression = schedule;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllIdSchedules(final ScopNode root) {
    return ScopISLConverter.getAllIdSchedules(root, ((ScopStatement[])Conversions.unwrapArray(root.listAllStatements(), ScopStatement.class)));
  }
  
  public static JNIISLUnionMap getAllIdSchedules(final ScopNode root, final ScopStatement... stmts) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      for (final ScopStatement stmt : stmts) {
        {
          JNIISLMap scheduleMap = ScopISLConverter.getIdSchedule(stmt, root);
          scheduleMap = scheduleMap.setInputTupleName(stmt.getId());
          res = res.addMap(scheduleMap);
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllIdSchedulesWithExplicitRead(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          final Function1<ScopAccess, Boolean> _function = (ScopAccess a) -> {
            Symbol _sym = a.getSym();
            return Boolean.valueOf(Objects.equal(_sym, s));
          };
          boolean refSymbol = IterableExtensions.<ScopAccess>exists(stmt.listAllAccesses(), _function);
          if ((refSymbol || (s == null))) {
            JNIISLMap scheduleMap = ScopISLConverter.getIdSchedule(stmt, root);
            scheduleMap = scheduleMap.addOutputDimensions(Collections.<String>unmodifiableSet(CollectionLiterals.<String>newHashSet("o")));
            JNIISLMap _copy = scheduleMap.copy();
            String _id = stmt.getId();
            String _plus = (_id + "_WR");
            JNIISLMap schedule_wr = _copy.setInputTupleName(_plus);
            int _nbOuts = scheduleMap.getNbOuts();
            final int newOffset = (_nbOuts - 1);
            schedule_wr = schedule_wr.sliceFixed(JNIISLDimType.isl_dim_out, newOffset, 1);
            res = res.addMap(schedule_wr);
            int id = 0;
            EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
            for (final ScopRead read : _listAllReadAccess) {
              {
                JNIISLMap _copy_1 = scheduleMap.copy();
                String _id_1 = stmt.getId();
                String _plus_1 = (_id_1 + "_RD");
                int _plusPlus = id++;
                String _plus_2 = (_plus_1 + Integer.valueOf(_plusPlus));
                JNIISLMap domain_rd = _copy_1.setInputTupleName(_plus_2);
                domain_rd = domain_rd.sliceFixed(JNIISLDimType.isl_dim_out, newOffset, 0);
                res = res.addMap(domain_rd);
              }
            }
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllIdSchedules(final Symbol s, final ScopNode root) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          final Function1<ScopAccess, Boolean> _function = (ScopAccess a) -> {
            Symbol _sym = a.getSym();
            return Boolean.valueOf(Objects.equal(_sym, s));
          };
          boolean refSymbol = IterableExtensions.<ScopAccess>exists(stmt.listAllAccesses(), _function);
          if (refSymbol) {
            JNIISLMap scheduleMap = ScopISLConverter.getIdSchedule(stmt, root);
            scheduleMap = scheduleMap.setInputTupleName(stmt.getId());
            res = res.addMap(scheduleMap);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionSet getAllDomains(final ScopNode root) {
    JNIISLUnionSet _xblockexpression = null;
    {
      JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          JNIISLSet domain = ScopISLConverter.getDomain(stmt, root);
          domain = domain.setTupleName(stmt.getId());
          res = res.addSet(domain);
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllReadAccessMaps(final ScopNode root) {
    return ScopISLConverter.getAllReadAccessMaps(root, null);
  }
  
  public static JNIISLUnionMap getAllReadAccessMapsWithExplicitRead(final ScopNode root, final Symbol symfilter) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          final JNIISLSet dom = ScopISLConverter.getDomain(stmt, root);
          JNIISLMap map = dom.buildIdentityMap();
          final JNIISLMap wraccess = map.copy();
          int id = 0;
          EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
          for (final ScopRead read : _listAllReadAccess) {
            if (((symfilter == null) || Objects.equal(symfilter, read.getSym()))) {
              JNIISLMap explRead = wraccess.copy();
              String _id = stmt.getId();
              String _plus = (_id + "_tmp_");
              String _plus_1 = (_plus + Integer.valueOf(id));
              JNIISLMap _setOutputTupleName = explRead.setOutputTupleName(_plus_1);
              explRead = _setOutputTupleName;
              String _id_1 = stmt.getId();
              String _plus_2 = (_id_1 + "_WR");
              explRead = explRead.setTupleName(JNIISLDimType.isl_dim_in, _plus_2);
              res = res.addMap(explRead);
              JNIISLMap scheduleMap = ScopISLConverter.getAccessMap(read, root);
              String _id_2 = stmt.getId();
              String _plus_3 = (_id_2 + "_RD");
              int _plusPlus = id++;
              String _plus_4 = (_plus_3 + Integer.valueOf(_plusPlus));
              scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, _plus_4);
              res = res.addMap(scheduleMap);
            }
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllReadAccessMaps(final ScopNode root, final Symbol symfilter) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
        for (final ScopRead read : _listAllReadAccess) {
          if (((symfilter == null) || Objects.equal(symfilter, read.getSym()))) {
            JNIISLMap scheduleMap = ScopISLConverter.getAccessMap(read, root);
            scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.getId());
            res = res.addMap(scheduleMap);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllWriteAccessMaps(final ScopNode root) {
    return ScopISLConverter.getAllWriteAccessMaps(root, null);
  }
  
  public static JNIISLUnionMap getAllWriteAccessMapsWithExplicitRead(final ScopNode root, final Symbol s) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          final JNIISLSet dom = ScopISLConverter.getDomain(stmt, root);
          JNIISLMap map = dom.buildIdentityMap();
          final JNIISLMap wraccess = map.copy();
          int id = 0;
          EList<ScopWrite> _listAllWriteAccess = stmt.listAllWriteAccess();
          for (final ScopWrite write : _listAllWriteAccess) {
            {
              JNIISLMap scheduleMap = ScopISLConverter.getAccessMap(write, root);
              String _id = stmt.getId();
              String _plus = (_id + "_WR");
              scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, _plus);
              res = res.addMap(scheduleMap);
            }
          }
          EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
          for (final ScopRead read : _listAllReadAccess) {
            if (((s == null) || Objects.equal(s, read.getSym()))) {
              JNIISLMap explWrite = wraccess.copy();
              String _id = stmt.getId();
              String _plus = (_id + "_tmp_");
              String _plus_1 = (_plus + Integer.valueOf(id));
              JNIISLMap _setOutputTupleName = explWrite.setOutputTupleName(_plus_1);
              explWrite = _setOutputTupleName;
              String _id_1 = stmt.getId();
              String _plus_2 = (_id_1 + "_RD");
              int _plusPlus = id++;
              String _plus_3 = (_plus_2 + Integer.valueOf(_plusPlus));
              explWrite = explWrite.setTupleName(JNIISLDimType.isl_dim_in, _plus_3);
              res = res.addMap(explWrite);
            }
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionMap getAllWriteAccessMaps(final ScopNode root, final Symbol symfilter) {
    JNIISLUnionMap _xblockexpression = null;
    {
      JNIISLUnionMap res = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        EList<ScopWrite> _listAllWriteAccess = stmt.listAllWriteAccess();
        for (final ScopWrite write : _listAllWriteAccess) {
          if (((symfilter == null) || Objects.equal(symfilter, write.getSym()))) {
            JNIISLMap scheduleMap = ScopISLConverter.getAccessMap(write, root);
            scheduleMap = scheduleMap.setTupleName(JNIISLDimType.isl_dim_in, stmt.getId());
            res = res.addMap(scheduleMap);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionSet getAllStatementDomains(final Symbol s, final ScopNode root) {
    JNIISLUnionSet _xblockexpression = null;
    {
      JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
      final Function1<ScopStatement, Boolean> _function = (ScopStatement stmt) -> {
        return Boolean.valueOf(stmt.useSymbol(s));
      };
      Iterable<ScopStatement> _filter = IterableExtensions.<ScopStatement>filter(root.listAllStatements(), _function);
      for (final ScopStatement stmt : _filter) {
        {
          JNIISLSet domain = ScopISLConverter.getDomain(stmt, root);
          domain = domain.setTupleName(stmt.getId());
          res = res.addSet(domain);
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionSet getAllStatementDomains(final ScopNode root) {
    JNIISLUnionSet _xblockexpression = null;
    {
      JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          JNIISLSet domain = ScopISLConverter.getDomain(stmt, root);
          domain = domain.setTupleName(stmt.getId());
          res = res.addSet(domain);
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static JNIISLUnionSet getAllStatementDomainsWithExplicitRead(final ScopNode root) {
    JNIISLUnionSet _xblockexpression = null;
    {
      JNIISLUnionSet res = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), "{}");
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        {
          JNIISLSet domain = ScopISLConverter.getDomain(stmt, root);
          JNIISLSet _copy = domain.copy();
          String _id = stmt.getId();
          String _plus = (_id + "_WR");
          final JNIISLSet domain_wr = _copy.setTupleName(_plus);
          res = res.addSet(domain_wr);
          int id = 0;
          EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
          for (final ScopRead read : _listAllReadAccess) {
            {
              JNIISLSet _copy_1 = domain.copy();
              String _id_1 = stmt.getId();
              String _plus_1 = (_id_1 + "_RD");
              int _plusPlus = id++;
              String _plus_2 = (_plus_1 + Integer.valueOf(_plusPlus));
              final JNIISLSet domain_rd = _copy_1.setTupleName(_plus_2);
              res = res.addSet(domain_rd);
            }
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected static JNIISLMap _getAccessMap(final ScopAccess rd, final ScopNode root) {
    JNIISLMap _xblockexpression = null;
    {
      final EList<ScopDimension> indices = rd.listAllEnclosingIterators(root);
      JNIISLMap _buildIslMap = ScopISLConverter.buildIslMap(rd.getSymName(), rd, root, indices, rd.getIndexExpressions());
      _xblockexpression = _buildIslMap.setInputTupleName(rd.getEnclosingStatement().getId());
    }
    return _xblockexpression;
  }
  
  protected static JNIISLMap _getAccessMap(final ScopRegionRead rd, final ScopNode root) {
    throw new UnsupportedOperationException("NYI");
  }
  
  public static JNIISLMap getAccessMap(final ScopAccess wr) {
    Instruction _root = wr.getRoot();
    return ScopISLConverter.getAccessMap(wr, ((ScopStatement) _root).getScopRoot());
  }
  
  public static JNIISLMap buildIslMap(final String label, final ScopNode node, final ScopNode root, final Iterable<ScopDimension> indices, final List<IntExpression> exprs) {
    JNIISLMap _xblockexpression = null;
    {
      if ((indices == null)) {
        InputOutput.<String>println("This should not happen");
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("no indices in buildIslMap\'(");
        _builder.append(label);
        _builder.append(", ");
        _builder.append(node);
        _builder.append(", ");
        _builder.append(root);
        _builder.append(", ");
        _builder.append(exprs);
        _builder.append(")");
        throw new RuntimeException(_builder.toString());
      }
      final Function1<ScopDimension, String> _function = (ScopDimension p) -> {
        return p.getName();
      };
      final List<String> params = ListExtensions.<ScopDimension, String>map(root.listAllParameters(), _function);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(params);
      _builder_1.append(" -> {");
      final Function1<ScopDimension, String> _function_1 = (ScopDimension n) -> {
        return n.getName();
      };
      Iterable<String> _map = IterableExtensions.<ScopDimension, String>map(indices, _function_1);
      _builder_1.append(_map);
      _builder_1.append(" -> ");
      _builder_1.append(label);
      final Function1<IntExpression, String> _function_2 = (IntExpression e) -> {
        int _indexOf = exprs.indexOf(e);
        return ("o" + Integer.valueOf(_indexOf));
      };
      List<String> _map_1 = ListExtensions.<IntExpression, String>map(exprs, _function_2);
      _builder_1.append(_map_1);
      _builder_1.append(": ");
      {
        boolean _hasElements = false;
        for(final IntExpression e : exprs) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder_1.appendImmediate(" and ", "");
          }
          int _indexOf = exprs.indexOf(e);
          String _plus = ("o" + Integer.valueOf(_indexOf));
          String _plus_1 = (_plus + "=");
          String _print = ScopISLConverter.pp.print(e);
          String _plus_2 = (_plus_1 + _print);
          _builder_1.append(_plus_2);
        }
      }
      _builder_1.append("}");
      final String str = _builder_1.toString();
      JNIISLMap access = JNIISLMap.buildFromString(str);
      final JNIISLSet domain = ScopISLConverter.getDomain(node, root);
      access = access.intersectDomain(domain);
      _xblockexpression = access;
    }
    return _xblockexpression;
  }
  
  protected static JNIISLSet _getDomain(final ScopNode node) {
    return ScopISLConverter.getDomain(node, node.getScopRoot());
  }
  
  protected static JNIISLSet _getDomain(final ScopStatement node) {
    JNIISLSet _domain = ScopISLConverter.getDomain(node, node.getScopRoot());
    return _domain.setTupleName(node.getId());
  }
  
  protected static JNIISLSet _getDomain(final Type s) {
    return null;
  }
  
  protected static JNIISLSet _getDomain(final ArrayType s) {
    JNIISLSet _xblockexpression = null;
    {
      final Function1<Instruction, Long> _function = (Instruction d) -> {
        return Long.valueOf(((IntInstruction) d).getValue());
      };
      final List<Long> dimSizes = ListExtensions.<Instruction, Long>map(s.getSizes(), _function);
      ArrayList<String> indices = Lists.<String>newArrayList();
      ArrayList<String> constraints = Lists.<String>newArrayList();
      int _size = dimSizes.size();
      int _minus = (_size - 1);
      IntegerRange _upTo = new IntegerRange(0, _minus);
      for (final Integer i : _upTo) {
        {
          indices.add(("c" + i));
          StringConcatenation _builder = new StringConcatenation();
          _builder.append(" ");
          _builder.append("0<=");
          String _get = indices.get((i).intValue());
          _builder.append(_get, " ");
          _builder.append("<");
          Long _get_1 = dimSizes.get((i).intValue());
          _builder.append(_get_1, " ");
          constraints.add(_builder.toString());
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("[] -> { ");
      _builder.append(indices);
      _builder.append(" : ");
      {
        boolean _hasElements = false;
        for(final String cs : constraints) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(" and ", "");
          }
          _builder.append(cs);
        }
      }
      _builder.append("}");
      final String str = _builder.toString();
      _xblockexpression = JNIISLSet.buildFromString(str);
    }
    return _xblockexpression;
  }
  
  public static JNIISLSet getDomain(final ScopNode node, final ScopNode root) {
    final Pair<List<IntConstraintSystem>, List<IntConstraintSystem>> collected = DomainConstraintsCollector.compute(node, node.getScopRoot());
    Object _get = ((Object[])Conversions.unwrapArray(collected, Object.class))[0];
    final Function1<IntConstraintSystem, Boolean> _function = (IntConstraintSystem t) -> {
      int _size = t.getConstraints().size();
      return Boolean.valueOf((_size > 0));
    };
    final Iterable<IntConstraintSystem> ics = IterableExtensions.<IntConstraintSystem>filter(((List<IntConstraintSystem>) _get), _function);
    Object _get_1 = ((Object[])Conversions.unwrapArray(collected, Object.class))[1];
    final Function1<IntConstraintSystem, Boolean> _function_1 = (IntConstraintSystem t) -> {
      int _size = t.getConstraints().size();
      return Boolean.valueOf((_size > 0));
    };
    final Iterable<IntConstraintSystem> nics = IterableExtensions.<IntConstraintSystem>filter(((List<IntConstraintSystem>) _get_1), _function_1);
    EList<ScopDimension> dims = node.listAllEnclosingIterators(root);
    final Function1<ScopDimension, String> _function_2 = (ScopDimension i) -> {
      return i.getName();
    };
    List<String> indices = ListExtensions.<ScopDimension, String>map(dims, _function_2);
    final Function1<ScopDimension, String> _function_3 = (ScopDimension p) -> {
      return p.getName();
    };
    final List<String> params = ListExtensions.<ScopDimension, String>map(root.listAllParameters(), _function_3);
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isEmpty = params.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        _builder.append(params);
        _builder.append(" -> ");
      }
    }
    _builder.append("{ ");
    _builder.append(indices);
    _builder.append(" : ");
    final String strParamsIndices = _builder.toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append(strParamsIndices);
    {
      boolean _hasElements = false;
      for(final IntConstraintSystem cs : ics) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder_1.appendImmediate(" and ", "");
        }
        String _print = ScopISLConverter.pp.print(cs.simplify());
        _builder_1.append(_print);
      }
    }
    _builder_1.append("}");
    final String str = _builder_1.toString();
    try {
      final JNIISLSet res = JNIISLSet.buildFromString(str);
      int _size = IterableExtensions.size(nics);
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        JNIISLSet elseRes = res.copy();
        for (final IntConstraintSystem nic : nics) {
          {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append(strParamsIndices);
            String _print_1 = ScopISLConverter.pp.print(nic);
            _builder_2.append(_print_1);
            _builder_2.append("}");
            final JNIISLSet elseSet = JNIISLSet.buildFromString(_builder_2.toString());
            elseRes = elseRes.intersect(elseSet.complement());
            boolean _isEmpty_1 = res.isEmpty();
            if (_isEmpty_1) {
              InputOutput.<String>println("Empty domain !");
            }
          }
        }
        return elseRes;
      }
      return res;
    } catch (final Throwable _t) {
      if (_t instanceof RuntimeException) {
        throw new RuntimeException(((("Cannot construct isl_set from " + str) + " for node ") + node));
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public static JNIISLUnionSet moveDimsToParams(final JNIISLUnionSet origUnionSet, final List<ScopDimension> dims) {
    JNIISLUnionSet _xblockexpression = null;
    {
      JNIISLUnionSet unionSet = origUnionSet;
      for (final ScopDimension dim : dims) {
        {
          JNIISLUnionSet relUnionSet = JNIISLUnionSet.buildEmpty(origUnionSet.getSpace());
          List<JNIISLSet> _sets = unionSet.getSets();
          for (final JNIISLSet set : _sets) {
            {
              final int paramPos = set.getNbParams();
              final String name = set.getTupleName();
              final int dimPos = set.findDimByName(JNIISLDimType.isl_dim_set, dim.getSymName());
              JNIISLSet relSet = set.moveDims(JNIISLDimType.isl_dim_param, paramPos, JNIISLDimType.isl_dim_set, dimPos, 1);
              relSet = relSet.setTupleName(name);
              relUnionSet = relUnionSet.addSet(relSet);
            }
          }
          unionSet = relUnionSet;
        }
      }
      _xblockexpression = unionSet;
    }
    return _xblockexpression;
  }
  
  public static JNIISLMap getAccessMap(final ScopAccess rd, final ScopNode root) {
    if (rd instanceof ScopRegionRead) {
      return _getAccessMap((ScopRegionRead)rd, root);
    } else if (rd != null) {
      return _getAccessMap(rd, root);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(rd, root).toString());
    }
  }
  
  public static JNIISLSet getDomain(final AnnotatedElement node) {
    if (node instanceof ScopStatement) {
      return _getDomain((ScopStatement)node);
    } else if (node instanceof ArrayType) {
      return _getDomain((ArrayType)node);
    } else if (node instanceof ScopNode) {
      return _getDomain((ScopNode)node);
    } else if (node instanceof Type) {
      return _getDomain((Type)node);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(node).toString());
    }
  }
}
