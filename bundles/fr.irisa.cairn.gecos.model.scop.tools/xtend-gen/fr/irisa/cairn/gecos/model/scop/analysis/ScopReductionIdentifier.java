package fr.irisa.cairn.gecos.model.scop.analysis;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopCopyIdentifier;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.tom.ArithmeticOperations;

@SuppressWarnings("all")
public class ScopReductionIdentifier {
  private GecosProject p;
  
  private boolean DEBUG = true;
  
  private boolean VERBOSE = true;
  
  public String debug(final String string) {
    String _xifexpression = null;
    if (this.DEBUG) {
      _xifexpression = InputOutput.<String>println(("[ScopReductionIdentifier] " + string));
    }
    return _xifexpression;
  }
  
  public String log(final String string) {
    String _xifexpression = null;
    if (this.VERBOSE) {
      _xifexpression = InputOutput.<String>println(("[ScopReductionIdentifier] " + string));
    }
    return _xifexpression;
  }
  
  public ScopReductionIdentifier(final GecosProject p) {
    this.p = p;
  }
  
  public Long getTripCount(final ScopForLoop loop) {
    long _xblockexpression = (long) 0;
    {
      final IntExpression r = ArithmeticOperations.add(loop.getUB().<AffineExpression>copy(), loop.getLB().<AffineExpression>copy()).simplify();
      long _xtrycatchfinallyexpression = (long) 0;
      try {
        _xtrycatchfinallyexpression = ((AffineExpression) r).getConstantTerm().getCoef();
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          return null;
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
      _xblockexpression = _xtrycatchfinallyexpression;
    }
    return Long.valueOf(_xblockexpression);
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      {
        final HashMap<ScopWrite, Map<ScopRead, JNIISLMap>> defuses = ScopAccessAnalyzer.getDefUses(scop);
        final HashMap<ScopRead, Map<ScopWrite, JNIISLMap>> useDefs = ScopAccessAnalyzer.getUseDefs(scop);
        final Set<ScopWrite> defs = defuses.keySet();
        for (final ScopWrite def : defs) {
          {
            this.debug(("*** def " + def));
            final ScopStatement stmt = def.getEnclosingStatement();
            final Map<ScopRead, JNIISLMap> uses = defuses.get(def);
            boolean _isCopy = ScopCopyIdentifier.isCopy(stmt);
            if (_isCopy) {
              this.debug(("Copy statement " + stmt));
              Set<ScopRead> _keySet = uses.keySet();
              for (final ScopRead use : _keySet) {
                {
                  Instruction _root = use.getRoot();
                  String _plus = ("  used by " + _root);
                  this.debug(_plus);
                  EList<ScopRead> _listAllReadAccess = def.getEnclosingStatement().listAllReadAccess();
                  for (final ScopRead srcRead : _listAllReadAccess) {
                    {
                      final Map<ScopWrite, JNIISLMap> srcWrite = useDefs.get(srcRead);
                      this.debug(("      - " + srcWrite));
                    }
                  }
                }
              }
            } else {
            }
            int _size = uses.size();
            boolean _equals = (_size == 1);
            if (_equals) {
              final ScopRead use_1 = ((ScopRead[])Conversions.unwrapArray(uses.keySet(), ScopRead.class))[0];
              this.debug(((("*** def " + def) + " has single use ") + use_1));
              boolean _containsKey = useDefs.containsKey(use_1);
              if (_containsKey) {
                final Map<ScopWrite, JNIISLMap> sources = useDefs.get(use_1);
                Set<ScopWrite> _keySet_1 = sources.keySet();
                String _plus = ((("  *** use " + def) + " has sources ") + _keySet_1);
                this.debug(_plus);
                int _size_1 = sources.size();
                boolean _equals_1 = (_size_1 == 1);
                if (_equals_1) {
                  this.debug((((" Bijection def/use " + def) + "<=>") + use_1));
                }
              }
            }
          }
        }
      }
    }
  }
}
