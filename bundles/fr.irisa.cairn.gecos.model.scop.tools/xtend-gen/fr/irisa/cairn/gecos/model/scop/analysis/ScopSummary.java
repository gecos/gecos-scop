package fr.irisa.cairn.gecos.model.scop.analysis;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class ScopSummary {
  private GecosProject proj;
  
  public ScopSummary(final GecosProject p) {
    this.proj = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      ScopSummary.summarize(scop);
    }
  }
  
  public static String summarize(final GecosScopBlock scop) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Scop ");
    int _number = scop.getNumber();
    _builder.append(_number);
    _builder.append(" ");
    EList<ScopDimension> _parameters = scop.getParameters();
    _builder.append(_parameters);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("iterators : ");
    EList<ScopDimension> _iterators = scop.getIterators();
    _builder.append(_iterators, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("live-in : ");
    EList<Symbol> _liveInSymbols = scop.getLiveInSymbols();
    _builder.append(_liveInSymbols, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("live-out : ");
    EList<Symbol> _liveOutSymbols = scop.getLiveOutSymbols();
    _builder.append(_liveOutSymbols, "\t");
    _builder.newLineIfNotEmpty();
    {
      EList<ScopStatement> _listAllStatements = scop.listAllStatements();
      for(final ScopStatement stmt : _listAllStatements) {
        _builder.append("\t");
        _builder.append("statement ");
        String _id = stmt.getId();
        _builder.append(_id, "\t");
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("domain : ");
        JNIISLSet _domain = ScopISLConverter.getDomain(stmt, scop);
        _builder.append(_domain, "\t\t");
        _builder.append("\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("schedule : ");
        JNIISLMap _idSchedule = ScopISLConverter.getIdSchedule(stmt, scop);
        _builder.append(_idSchedule, "\t\t");
        _builder.append("\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("instruction : ");
        _builder.append(stmt, "\t\t");
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("writes : ");
        EList<ScopWrite> _listAllWriteAccess = stmt.listAllWriteAccess();
        _builder.append(_listAllWriteAccess, "\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("reads : : ");
        EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
        _builder.append(_listAllReadAccess, "\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("}\t");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return InputOutput.<String>println(_builder.toString());
  }
}
