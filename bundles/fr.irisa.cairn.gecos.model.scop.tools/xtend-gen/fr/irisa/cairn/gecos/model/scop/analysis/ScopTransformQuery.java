package fr.irisa.cairn.gecos.model.scop.analysis;

import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import java.util.HashSet;
import java.util.List;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ScopTransformQuery {
  public static JNIISLUnionMap buildISLSchedule(final ScopNode block) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final ScopTransform t = block.getTransform();
      JNIISLUnionMap _xifexpression = null;
      if ((t == null)) {
        _xifexpression = ScopISLConverter.getAllIdSchedules(block);
      } else {
        JNIISLUnionMap _xblockexpression_1 = null;
        {
          final List<ScopStatement> stmts = block.listAllStatements();
          final EList<ScheduleDirective> scheds = t.listAllScheduleDirectives();
          JNIISLUnionMap _xifexpression_1 = null;
          int _size = scheds.size();
          boolean _equals = (_size == 0);
          if (_equals) {
            _xifexpression_1 = ScopISLConverter.getAllIdSchedules(block);
          } else {
            int _size_1 = scheds.size();
            int _size_2 = stmts.size();
            boolean _equals_1 = (_size_1 == _size_2);
            if (_equals_1) {
              HashSet<String> map = new HashSet<String>();
              for (final ScopStatement s : stmts) {
                map.add(s.getId());
              }
              JNIISLUnionMap umap = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
              for (final ScheduleDirective s_1 : scheds) {
                {
                  boolean _contains = map.contains(s_1.getStatement().getId());
                  boolean _not = (!_contains);
                  if (_not) {
                    throw new UnsupportedOperationException(("No schedule provided for statement " + s_1));
                  }
                  umap = umap.addMap(JNIISLMap.buildFromString(s_1.getSchedule()));
                }
              }
              return umap;
            }
          }
          _xblockexpression_1 = _xifexpression_1;
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
}
