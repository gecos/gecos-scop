package fr.irisa.cairn.gecos.model.scop.analysis;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.Substitute;
import fr.irisa.cairn.gecos.model.scop.codegen.ScopISLCodegen;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ScopUncollapser extends ScopISLCodegen {
  public ScopUncollapser(final GecosProject p) {
    super(p);
  }
  
  @Override
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsFirstInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      this.collapse(scop);
    }
  }
  
  protected ScopUnexpandedStatement _uncollapseStmt(final ScopStatement stmt, final ScopNode n) {
    return null;
  }
  
  protected ScopUnexpandedStatement _uncollapseStmt(final ScopUnexpandedStatement stmt, final ScopNode n) {
    return null;
  }
  
  public void collapse(final ScopNode n) {
    final EList<ScopStatement> stmts = n.listAllStatements();
    ScopBlock res = ScopUserFactory.scopBlock();
    Substitute.replace(n, res);
    for (final ScopStatement s : stmts) {
      EList<ScopNode> _children = res.getChildren();
      ScopUnexpandedStatement _uncollapseStmt = this.uncollapseStmt(s, n);
      _children.add(_uncollapseStmt);
    }
  }
  
  public ScopUnexpandedStatement uncollapseStmt(final ScopStatement stmt, final ScopNode n) {
    if (stmt instanceof ScopUnexpandedStatement) {
      return _uncollapseStmt((ScopUnexpandedStatement)stmt, n);
    } else if (stmt != null) {
      return _uncollapseStmt(stmt, n);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(stmt, n).toString());
    }
  }
}
