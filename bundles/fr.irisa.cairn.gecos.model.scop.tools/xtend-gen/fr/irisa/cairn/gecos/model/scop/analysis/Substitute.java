package fr.irisa.cairn.gecos.model.scop.analysis;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import java.util.Arrays;

@SuppressWarnings("all")
public class Substitute {
  public static ScopNode replace(final ScopNode old, final ScopNode newnode) {
    ScopNode _xifexpression = null;
    ScopNode _parentScop = old.getParentScop();
    boolean _tripleNotEquals = (_parentScop != null);
    if (_tripleNotEquals) {
      _xifexpression = Substitute.replace(old, old.getParentScop(), newnode);
    } else {
      throw new UnsupportedOperationException("Cannot replace in root ");
    }
    return _xifexpression;
  }
  
  protected static ScopNode _replace(final ScopNode old, final ScopBlock parent, final ScopNode newnode) {
    ScopNode _xblockexpression = null;
    {
      final int index = parent.getChildren().indexOf(old);
      _xblockexpression = parent.getChildren().set(index, newnode);
    }
    return _xblockexpression;
  }
  
  protected static ScopNode _replace(final ScopNode old, final ScopGuard parent, final ScopNode newnode) {
    ScopNode _thenNode = parent.getThenNode();
    boolean _tripleEquals = (old == _thenNode);
    if (_tripleEquals) {
      parent.setThenNode(newnode);
    } else {
      ScopNode _elseNode = parent.getElseNode();
      boolean _tripleEquals_1 = (old == _elseNode);
      if (_tripleEquals_1) {
        parent.setThenNode(newnode);
      } else {
        throw new UnsupportedOperationException((("Node " + old) + " not in parentScope"));
      }
    }
    return null;
  }
  
  protected static ScopNode _replace(final ScopNode old, final ScopForLoop parent, final ScopNode newnode) {
    ScopNode _body = parent.getBody();
    boolean _tripleEquals = (old == _body);
    if (_tripleEquals) {
      parent.setBody(newnode);
    } else {
      throw new UnsupportedOperationException((("Node " + old) + " not in parentScope"));
    }
    return null;
  }
  
  protected static ScopNode _replace(final ScopNode old, final GecosScopBlock parent, final ScopNode newnode) {
    ScopNode _root = parent.getRoot();
    boolean _tripleEquals = (old == _root);
    if (_tripleEquals) {
      parent.setRoot(newnode);
    } else {
      throw new UnsupportedOperationException((("Node " + old) + " not in parentScope"));
    }
    return null;
  }
  
  public static ScopNode replace(final ScopNode old, final ScopNode parent, final ScopNode newnode) {
    if (parent instanceof GecosScopBlock) {
      return _replace(old, (GecosScopBlock)parent, newnode);
    } else if (parent instanceof ScopBlock) {
      return _replace(old, (ScopBlock)parent, newnode);
    } else if (parent instanceof ScopForLoop) {
      return _replace(old, (ScopForLoop)parent, newnode);
    } else if (parent instanceof ScopGuard) {
      return _replace(old, (ScopGuard)parent, newnode);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(old, parent, newnode).toString());
    }
  }
}
