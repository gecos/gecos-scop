package fr.irisa.cairn.gecos.model.scop.analysis;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class WordLengthAnalysis {
  private GecosProject p;
  
  private boolean DEBUG = true;
  
  private boolean VERBOSE = true;
  
  public String debug(final String string) {
    String _xifexpression = null;
    if (this.DEBUG) {
      _xifexpression = InputOutput.<String>println(("[WordLengthAnalyzer] " + string));
    }
    return _xifexpression;
  }
  
  public String log(final String string) {
    String _xifexpression = null;
    if (this.VERBOSE) {
      _xifexpression = InputOutput.<String>println(("[WordLengthAnalyzer] " + string));
    }
    return _xifexpression;
  }
  
  public WordLengthAnalysis(final GecosProject p) {
    this.p = p;
  }
  
  protected List<IntExpression> _collectExpressions(final ScopNode node) {
    BasicEList<IntExpression> _xblockexpression = null;
    {
      final BasicEList<IntExpression> res = new BasicEList<IntExpression>();
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected List<IntExpression> _collectExpressions(final GecosScopBlock node) {
    BasicEList<IntExpression> _xblockexpression = null;
    {
      final BasicEList<IntExpression> res = new BasicEList<IntExpression>();
      List<IntExpression> _collectExpressions = this.collectExpressions(node.getRoot());
      Iterables.<IntExpression>addAll(res, _collectExpressions);
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected List<IntExpression> _collectExpressions(final ScopForLoop node) {
    BasicEList<IntExpression> _xblockexpression = null;
    {
      final BasicEList<IntExpression> res = new BasicEList<IntExpression>();
      IntExpression _lB = node.getLB();
      res.add(_lB);
      IntExpression _uB = node.getUB();
      res.add(_uB);
      List<IntExpression> _collectExpressions = this.collectExpressions(node.getBody());
      Iterables.<IntExpression>addAll(res, _collectExpressions);
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected List<IntExpression> _collectExpressions(final ScopBlock node) {
    BasicEList<IntExpression> _xblockexpression = null;
    {
      final BasicEList<IntExpression> res = new BasicEList<IntExpression>();
      EList<ScopNode> _children = node.getChildren();
      for (final ScopNode c : _children) {
        List<IntExpression> _collectExpressions = this.collectExpressions(c);
        Iterables.<IntExpression>addAll(res, _collectExpressions);
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected List<IntExpression> _collectExpressions(final ScopGuard node) {
    BasicEList<IntExpression> _xblockexpression = null;
    {
      final BasicEList<IntExpression> res = new BasicEList<IntExpression>();
      ScopNode _thenNode = node.getThenNode();
      boolean _tripleNotEquals = (_thenNode != null);
      if (_tripleNotEquals) {
        List<IntExpression> _collectExpressions = this.collectExpressions(node.getThenNode());
        Iterables.<IntExpression>addAll(res, _collectExpressions);
      }
      ScopNode _elseNode = node.getElseNode();
      boolean _tripleNotEquals_1 = (_elseNode != null);
      if (_tripleNotEquals_1) {
        List<IntExpression> _collectExpressions_1 = this.collectExpressions(node.getThenNode());
        Iterables.<IntExpression>addAll(res, _collectExpressions_1);
      }
      EList<IntConstraintSystem> _predicate = node.getPredicate();
      for (final IntConstraintSystem c : _predicate) {
        EList<IntExpression> _eAllContentsInstancesOf = EMFUtils.<IntExpression>eAllContentsInstancesOf(c, IntExpression.class);
        Iterables.<IntExpression>addAll(res, _eAllContentsInstancesOf);
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected List<IntExpression> _collectExpressions(final ScopInstructionStatement node) {
    return EMFUtils.<IntExpression>eAllContentsInstancesOf(node, IntExpression.class);
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      {
        final List<IntExpression> exprs = this.collectExpressions(scop);
        for (final IntExpression e : exprs) {
          {
            ScopNode node = this.getContainingScopNode(e);
            boolean _matched = false;
            if (node instanceof ScopForLoop) {
              _matched=true;
              node = ((ScopForLoop)node).getBody();
            }
            final JNIISLSet domain = ScopISLConverter.getDomain(node);
            StringConcatenation _builder = new StringConcatenation();
            EList<ScopDimension> _listRootParameters = node.listRootParameters();
            _builder.append(_listRootParameters);
            _builder.append("->{ ");
            String _tupleName = domain.getTupleName();
            _builder.append(_tupleName);
            final Function1<ScopDimension, String> _function = (ScopDimension i) -> {
              return i.getName();
            };
            List<String> _map = ListExtensions.<ScopDimension, String>map(node.listAllEnclosingIterators(), _function);
            _builder.append(_map);
            _builder.append("->[e] : e=");
            _builder.append(e);
            _builder.append("}");
            final String str = _builder.toString();
            final JNIISLMap function = JNIISLMap.buildFromString(str);
            JNIISLMap map = function.intersectDomain(domain);
            JNIISLSet range = map.getRange();
            range = range.projectOut(JNIISLDimType.isl_dim_param, 0, map.getNbParams());
            String _plus = (e + "->[");
            JNIISLSet _lexMin = range.copy().lexMin();
            String _plus_1 = (_plus + _lexMin);
            String _plus_2 = (_plus_1 + ":");
            JNIISLSet _lexMax = range.copy().lexMax();
            String _plus_3 = (_plus_2 + _lexMax);
            String _plus_4 = (_plus_3 + "]");
            this.log(_plus_4);
          }
        }
      }
    }
  }
  
  public ScopNode getContainingScopNode(final IntExpression expression) {
    Object _xblockexpression = null;
    {
      EObject o = expression;
      while (((o != null) && (!(o instanceof ScopNode)))) {
        o = o.eContainer();
      }
      Object _xifexpression = null;
      if ((o != null)) {
        return ((ScopNode) o);
      } else {
        _xifexpression = null;
      }
      _xblockexpression = _xifexpression;
    }
    return ((ScopNode)_xblockexpression);
  }
  
  public List<IntExpression> collectExpressions(final ScopNode node) {
    if (node instanceof ScopInstructionStatement) {
      return _collectExpressions((ScopInstructionStatement)node);
    } else if (node instanceof GecosScopBlock) {
      return _collectExpressions((GecosScopBlock)node);
    } else if (node instanceof ScopBlock) {
      return _collectExpressions((ScopBlock)node);
    } else if (node instanceof ScopForLoop) {
      return _collectExpressions((ScopForLoop)node);
    } else if (node instanceof ScopGuard) {
      return _collectExpressions((ScopGuard)node);
    } else if (node != null) {
      return _collectExpressions(node);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(node).toString());
    }
  }
}
