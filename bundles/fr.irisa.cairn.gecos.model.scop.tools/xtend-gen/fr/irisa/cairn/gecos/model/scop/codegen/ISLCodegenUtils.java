package fr.irisa.cairn.gecos.model.scop.codegen;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import gecos.core.Symbol;
import gecos.instrs.SetInstruction;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;

@SuppressWarnings("all")
public class ISLCodegenUtils {
  public static ScopNode encloseWithLoop(final ScopNode node, final JNIISLSet set, final List<ScopDimension> iterators) {
    ScopNode _xblockexpression = null;
    {
      String _tupleName = set.getTupleName();
      boolean _tripleEquals = (_tupleName == null);
      if (_tripleEquals) {
        throw new UnsupportedOperationException((("Domain " + set) + " must be named"));
      }
      JNIISLUnionMap schedule = JNIISLUnionMap.buildEmpty(set.copy().identity().getSpace());
      schedule = schedule.addMap(set.copy().identity());
      final ScopUnexpandedStatement loopUnexpanded = ScopUserFactory.unexpandedStmt(iterators, set.toString(), schedule.toString());
      loopUnexpanded.setName(set.getTupleName());
      node.getParentScop().replace(node, loopUnexpanded);
      final ScopNode loop = JNIISLASTToScopConverter.adapt(loopUnexpanded);
      EList<ScopStatement> _listAllStatements = loop.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        s.getParentScop().replace(s, node.copy());
      }
      _xblockexpression = loop;
    }
    return _xblockexpression;
  }
  
  public static void expand(final ScopRegionRead read, final List<ScopDimension> iterators) {
    final JNIISLSet set = ScopISLConverter.getAccessMap(read).moveInputDimsAsParameters().toSet();
    JNIISLUnionMap schedule = JNIISLUnionMap.buildEmpty(set.copy().identity().getSpace());
    schedule = schedule.addMap(set.copy().identity());
    final ScopUnexpandedStatement loopUnexpanded = ScopUserFactory.unexpandedStmt(iterators, set.toString(), schedule.toString());
    loopUnexpanded.setName(set.getTupleName());
    final ScopNode loop = JNIISLASTToScopConverter.adapt(loopUnexpanded);
    EList<ScopStatement> _listAllStatements = loop.listAllStatements();
    for (final ScopStatement s : _listAllStatements) {
      InputOutput.<ScopStatement>print(s);
    }
  }
  
  public static ScopNode dataMover(final JNIISLSet domain, final ScopBlock scopContainer, final Symbol src, final Symbol dst) {
    ScopNode _xblockexpression = null;
    {
      int _length = domain.getTupleName().length();
      boolean _equals = (_length == 0);
      if (_equals) {
        throw new UnsupportedOperationException((("Domain " + domain) + " must be named"));
      }
      final List<ScopDimension> symIndices = IterableExtensions.<ScopDimension>toList(ScopIteratorGenerator.freeIterators(scopContainer, domain.getNbDims()));
      final Function1<ScopDimension, IntExpression> _function = (ScopDimension i) -> {
        return IntExpressionBuilder.affine(i);
      };
      final ScopWrite dstInstr = ScopUserFactory.scopWrite(dst, ListExtensions.<ScopDimension, IntExpression>map(symIndices, _function));
      final Function1<ScopDimension, IntExpression> _function_1 = (ScopDimension i) -> {
        return IntExpressionBuilder.affine(i);
      };
      final ScopRead srcInstr = ScopUserFactory.scopRead(src, ListExtensions.<ScopDimension, IntExpression>map(symIndices, _function_1));
      final SetInstruction setInstr = GecosUserInstructionFactory.set(dstInstr, srcInstr);
      JNIISLUnionMap schedule = JNIISLUnionMap.buildEmpty(domain.copy().identity().getSpace());
      schedule = schedule.addMap(domain.copy().identity());
      final ScopUnexpandedStatement stmt = ScopUserFactory.unexpandedStmt(symIndices, domain.toString(), schedule.toString());
      stmt.getChildren().add(setInstr);
      stmt.setName(domain.getTupleName());
      scopContainer.getChildren().add(stmt);
      final ScopNode res = JNIISLASTToScopConverter.adapt(stmt);
      InputOutput.<ScopNode>println(res);
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
}
