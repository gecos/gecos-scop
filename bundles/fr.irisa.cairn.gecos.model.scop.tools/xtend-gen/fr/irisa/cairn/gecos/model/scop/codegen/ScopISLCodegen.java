package fr.irisa.cairn.gecos.model.scop.codegen;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopTransformQuery;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import java.util.List;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ScopISLCodegen {
  protected GecosProject proj;
  
  private int depth = (-1);
  
  public ScopISLCodegen(final GecosProject project) {
    this.proj = project;
  }
  
  public ScopISLCodegen(final GecosProject project, final Integer d) {
    this.proj = project;
    this.depth = (d).intValue();
  }
  
  public void compute() {
    EList<ProcedureSet> _listProcedureSets = this.proj.listProcedureSets();
    for (final ProcedureSet ps : _listProcedureSets) {
      {
        GecosUserTypeFactory.setScope(ps.getScope());
        final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(ps, GecosScopBlock.class);
        for (final GecosScopBlock scop : scops) {
          {
            final ScopNode newRoot = ScopISLCodegen.codegen(scop, this.depth);
            int id = 0;
            EList<ScopStatement> _listAllStatements = newRoot.listAllStatements();
            for (final ScopStatement stmt : _listAllStatements) {
              int _plusPlus = id++;
              String _plus = ("S" + Integer.valueOf(_plusPlus));
              stmt.setId(_plus);
            }
          }
        }
      }
    }
  }
  
  public static ScopNode codegen(final ScopNode block, final int _depth) {
    ScopNode _xblockexpression = null;
    {
      JNIISLUnionMap schedule = ScopTransformQuery.buildISLSchedule(block).copy();
      final JNIISLSet ctxt = ScopISLConverter.getContext(block);
      JNIISLASTBuild.setDetectMinMax(JNIISLContext.getCtx(), 1);
      final JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(ctxt.copy());
      JNIISLASTNode root = null;
      if ((_depth >= 0)) {
        JNIISLUnionMap newSched = JNIISLUnionMap.buildFromString(JNIISLContext.getCtx(), "{}");
        List<JNIISLMap> _maps = schedule.getMaps();
        for (final JNIISLMap map : _maps) {
          {
            final int nbDims = map.getNbDims(JNIISLDimType.isl_dim_out);
            final JNIISLMap projectedScheduling = map.projectOut(JNIISLDimType.isl_dim_out, _depth, (nbDims - _depth));
            newSched = newSched.addMap(projectedScheduling);
          }
        }
        root = build.generateWithExpansionNodes(newSched, "EX");
      } else {
        root = JNIISLASTNode.buildFromSchedule(build, schedule);
      }
      final ScopNode res = JNIISLASTToScopConverter.adapt(block, root);
      Iterable<ScopUnexpandedStatement> _filter = Iterables.<ScopUnexpandedStatement>filter(res.listAllStatements(), ScopUnexpandedStatement.class);
      for (final ScopUnexpandedStatement unExp : _filter) {
        {
          final int depth = unExp.listAllEnclosingIterators().size();
          final JNIISLMap map_1 = JNIISLMap.buildFromString(JNIISLContext.getCtx(), unExp.getSchedule());
          int _nbParams = map_1.getNbParams();
          int _minus = (_nbParams - 1);
          map_1.moveDims(JNIISLDimType.isl_dim_param, _minus, JNIISLDimType.isl_dim_param, 0, depth);
          unExp.setSchedule(map_1.toString());
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
}
