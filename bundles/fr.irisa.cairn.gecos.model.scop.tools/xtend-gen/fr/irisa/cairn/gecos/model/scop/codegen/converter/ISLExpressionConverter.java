package fr.irisa.cairn.gecos.model.scop.codegen.converter;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLVariableConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTExpression;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIdentifier;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTLiteral;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLAstOpType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLIdentifier;
import gecos.instrs.Instruction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.tom.ArithmeticOperations;

@SuppressWarnings("all")
public class ISLExpressionConverter {
  private ISLVariableConverter adapter;
  
  public ISLExpressionConverter(final ISLVariableConverter adapter) {
    this.adapter = adapter;
  }
  
  public ScopDimension getVariable(final JNIISLIdentifier identifier) {
    return this.adapter.buildVariable(identifier.getName());
  }
  
  protected IntExpression _buildDiv(final IntExpression obj, final JNIISLASTExpression e) {
    throw new RuntimeException((((("Cannot build Div(" + obj) + ",") + e) + ")"));
  }
  
  protected IntExpression _buildDiv(final AffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.div(obj, e.getValue()));
  }
  
  protected IntExpression _buildDiv(final QuasiAffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.div(obj, e.getValue()));
  }
  
  protected IntExpression _buildFloord(final IntExpression obj, final JNIISLASTExpression e) {
    throw new RuntimeException((((("Cannot build Floord(" + obj) + ",") + e) + ")"));
  }
  
  protected IntExpression _buildFloord(final AffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.div(obj, e.getValue()));
  }
  
  protected IntExpression _buildFloord(final QuasiAffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.div(obj, e.getValue()));
  }
  
  protected IntExpression _buildMod(final IntExpression obj, final JNIISLASTExpression e) {
    throw new RuntimeException((((("Cannot build Mod(" + obj) + ",") + e) + ")"));
  }
  
  protected IntExpression _buildMod(final AffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.mod(obj, e.getValue()));
  }
  
  protected IntExpression _buildMod(final QuasiAffineExpression obj, final JNIISLASTLiteral e) {
    return IntExpressionBuilder.qaffine(IntExpressionBuilder.mod(obj, e.getValue()));
  }
  
  protected IntExpression _buildExpr(final JNIISLASTLiteral obj) {
    AffineExpression _xblockexpression = null;
    {
      final long value = obj.getValue();
      _xblockexpression = IntExpressionBuilder.affine(IntExpressionBuilder.term(value));
    }
    return _xblockexpression;
  }
  
  protected IntExpression _buildExpr(final JNIISLASTIdentifier obj) {
    AffineExpression _xblockexpression = null;
    {
      final Variable v = this.getVariable(obj.getID());
      final AffineExpression res = IntExpressionBuilder.affine(IntExpressionBuilder.term(v));
      if ((res == null)) {
        throw new RuntimeException(("Cannot build IntExpression object from " + obj));
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected IntExpression _buildExpr(final JNIISLASTOperation obj) {
    IntExpression _xblockexpression = null;
    {
      final int nbArgs = obj.getNbArgs();
      final Function1<Integer, JNIISLASTExpression> _function = (Integer i) -> {
        return obj.getArgument((i).intValue());
      };
      final Iterable<JNIISLASTExpression> args = IterableExtensions.<Integer, JNIISLASTExpression>map(new IntegerRange(0, (nbArgs - 1)), _function);
      final Function1<JNIISLASTExpression, IntExpression> _function_1 = (JNIISLASTExpression arg) -> {
        return this.buildExpr(arg);
      };
      final Iterable<IntExpression> exprs = IterableExtensions.<JNIISLASTExpression, IntExpression>map(args, _function_1);
      final JNIISLAstOpType opType = obj.getOpType();
      if ((opType == null)) {
        InputOutput.<JNIISLASTOperation>println(obj);
        throw new RuntimeException(("Obvious NPE " + obj));
      }
      IntExpression _switchResult = null;
      int _value = opType.getValue();
      switch (_value) {
        case JNIISLAstOpType.ISL_AST_OP_MINUS:
          _switchResult = ArithmeticOperations.scale(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], (-1));
          break;
        case JNIISLAstOpType.ISL_AST_OP_ADD:
          IntExpression _xifexpression = null;
          int _size = IterableExtensions.size(exprs);
          boolean _greaterThan = (_size > 2);
          if (_greaterThan) {
            throw new RuntimeException("TODO: add support for sum");
          } else {
            _xifexpression = ArithmeticOperations.add(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[1]);
          }
          _switchResult = _xifexpression;
          break;
        case JNIISLAstOpType.ISL_AST_OP_SUB:
          _switchResult = ArithmeticOperations.sub(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_MUL:
          IntExpression _xifexpression_1 = null;
          Object _get = ((Object[])Conversions.unwrapArray(args, Object.class))[0];
          if ((_get instanceof JNIISLASTLiteral)) {
            JNIISLASTExpression _get_1 = ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[0];
            _xifexpression_1 = ArithmeticOperations.scale(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[1], ((JNIISLASTLiteral) _get_1).getValue());
          } else {
            IntExpression _xifexpression_2 = null;
            Object _get_2 = ((Object[])Conversions.unwrapArray(args, Object.class))[1];
            if ((_get_2 instanceof JNIISLASTLiteral)) {
              JNIISLASTExpression _get_3 = ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1];
              _xifexpression_2 = ArithmeticOperations.scale(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTLiteral) _get_3).getValue());
            } else {
              throw new RuntimeException("Not supported");
            }
            _xifexpression_1 = _xifexpression_2;
          }
          _switchResult = _xifexpression_1;
          break;
        case JNIISLAstOpType.ISL_AST_OP_MIN:
          _switchResult = IntExpressionBuilder.min(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_MAX:
          _switchResult = IntExpressionBuilder.max(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_DIV:
          _switchResult = this.buildDiv(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_FDIV_Q:
          _switchResult = this.buildFloord(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_PDIV_Q:
          _switchResult = this.buildFloord(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_PDIV_R:
          _switchResult = this.buildMod(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1]);
          break;
        case JNIISLAstOpType.ISL_AST_OP_ZDIV_R:
          _switchResult = this.buildMod(((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class))[0], ((JNIISLASTExpression[])Conversions.unwrapArray(args, JNIISLASTExpression.class))[1]);
          break;
        default:
          String _name = obj.getOpType().getName();
          String _plus = (_name + " not supported");
          throw new RuntimeException(_plus);
      }
      final IntExpression res = _switchResult;
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected IntConstraint _buildConstraint(final JNIISLASTExpression obj) {
    String _simpleName = obj.getClass().getSimpleName();
    String _plus = ((("Cannot build IntConstraint object from " + obj) + ":") + _simpleName);
    throw new RuntimeException(_plus);
  }
  
  protected IntConstraint _buildConstraint(final JNIISLASTOperation obj) {
    final IntExpression op1 = this.buildExpr(obj.getArgument(0));
    final IntExpression op2 = this.buildExpr(obj.getArgument(1));
    if (((op1 == null) || (op2 == null))) {
      throw new RuntimeException(("Cannot build IntConstraint object from " + obj));
    }
    IntConstraint _switchResult = null;
    int _value = obj.getOpType().getValue();
    switch (_value) {
      case JNIISLAstOpType.ISL_AST_OP_EQ:
        _switchResult = IntExpressionBuilder.eq(op1, op2);
        break;
      case JNIISLAstOpType.ISL_AST_OP_LE:
        _switchResult = IntExpressionBuilder.le(op1, op2);
        break;
      case JNIISLAstOpType.ISL_AST_OP_LT:
        _switchResult = IntExpressionBuilder.lt(op1, op2);
        break;
      case JNIISLAstOpType.ISL_AST_OP_GE:
        _switchResult = IntExpressionBuilder.ge(op1, op2);
        break;
      case JNIISLAstOpType.ISL_AST_OP_GT:
        _switchResult = IntExpressionBuilder.gt(op1, op2);
        break;
      default:
        throw new RuntimeException(("Cannot build IntConstraint object from " + obj));
    }
    IntConstraint res = _switchResult;
    return res;
  }
  
  public EList<IntConstraintSystem> buildUnionOfConstraintSystem(final JNIISLASTOperation obj) {
    BasicEList<IntConstraintSystem> _xblockexpression = null;
    {
      final BasicEList<IntConstraintSystem> res = new BasicEList<IntConstraintSystem>();
      int _value = obj.getOpType().getValue();
      switch (_value) {
        case JNIISLAstOpType.ISL_AST_OP_OR:
          int _nbArgs = obj.getNbArgs();
          int _minus = (_nbArgs - 1);
          IntegerRange _upTo = new IntegerRange(0, _minus);
          for (final Integer i : _upTo) {
            JNIISLASTExpression _argument = obj.getArgument((i).intValue());
            res.addAll(this.buildUnionOfConstraintSystem(((JNIISLASTOperation) _argument)));
          }
          break;
        default:
          res.add(this.buildConstraintSystem(obj));
          break;
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  protected IntConstraintSystem _buildConstraintSystem(final JNIISLASTExpression obj) {
    throw new RuntimeException(("Cannot build IntConstraintSystem object from " + obj));
  }
  
  protected IntConstraintSystem _buildConstraintSystem(final JNIISLASTOperation obj) {
    int _value = obj.getOpType().getValue();
    switch (_value) {
      case JNIISLAstOpType.ISL_AST_OP_AND:
        final ArrayList<IntConstraintSystem> constraintSystems = new ArrayList<IntConstraintSystem>();
        int _nbArgs = obj.getNbArgs();
        int _minus = (_nbArgs - 1);
        IntegerRange _upTo = new IntegerRange(0, _minus);
        for (final Integer i : _upTo) {
          constraintSystems.add(this.buildConstraintSystem(obj.getArgument((i).intValue())));
        }
        final ArrayList<IntConstraint> constraints = new ArrayList<IntConstraint>();
        final Consumer<IntConstraintSystem> _function = (IntConstraintSystem it) -> {
          constraints.addAll(it.getConstraints());
        };
        constraintSystems.forEach(_function);
        return IntExpressionBuilder.constraintSystem(constraints);
      case JNIISLAstOpType.ISL_AST_OP_MIN:
        return IntExpressionBuilder.constraintSystem(this.getArguments(obj));
      case JNIISLAstOpType.ISL_AST_OP_EQ:
      case JNIISLAstOpType.ISL_AST_OP_LE:
      case JNIISLAstOpType.ISL_AST_OP_LT:
      case JNIISLAstOpType.ISL_AST_OP_GE:
      case JNIISLAstOpType.ISL_AST_OP_GT:
        return IntExpressionBuilder.constraintSystem(this.buildConstraint(obj));
    }
    throw new RuntimeException(("Cannot build IntConstraint object from " + obj));
  }
  
  public BasicEList<IntConstraint> getArguments(final JNIISLASTOperation operation) {
    BasicEList<IntConstraint> _xblockexpression = null;
    {
      BasicEList<IntConstraint> res = new BasicEList<IntConstraint>();
      int _nbArgs = operation.getNbArgs();
      int _minus = (_nbArgs - 1);
      IntegerRange _upTo = new IntegerRange(0, _minus);
      for (final Integer i : _upTo) {
        res.add(this.buildConstraint(operation.getArgument((i).intValue())));
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public ScopStatement buildStatement(final JNIISLASTOperation obj) {
    ScopInstructionStatement stmt = ScopFactory.eINSTANCE.createScopInstructionStatement();
    final JNIISLASTExpression op1 = obj.getArgument(0);
    int _value = obj.getOpType().getValue();
    switch (_value) {
      case JNIISLAstOpType.ISL_AST_OP_CALL:
        stmt.setId(((JNIISLASTIdentifier) op1).getID().getName());
        int _nbArgs = obj.getNbArgs();
        int _minus = (_nbArgs - 1);
        IntegerRange _upTo = new IntegerRange(0, _minus);
        for (final Integer i : _upTo) {
          EList<Instruction> _children = stmt.getChildren();
          ScopIntExpression _sexpr = ScopUserFactory.sexpr(this.buildExpr(obj.getArgument((i).intValue())));
          _children.add(_sexpr);
        }
        return stmt;
    }
    throw new RuntimeException(("Cannot build ScopStatementMacro object from " + obj));
  }
  
  public IntExpression buildDiv(final IntExpression obj, final JNIISLASTExpression e) {
    if (obj instanceof AffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildDiv((AffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj instanceof QuasiAffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildDiv((QuasiAffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj != null
         && e != null) {
      return _buildDiv(obj, e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj, e).toString());
    }
  }
  
  public IntExpression buildFloord(final IntExpression obj, final JNIISLASTExpression e) {
    if (obj instanceof AffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildFloord((AffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj instanceof QuasiAffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildFloord((QuasiAffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj != null
         && e != null) {
      return _buildFloord(obj, e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj, e).toString());
    }
  }
  
  public IntExpression buildMod(final IntExpression obj, final JNIISLASTExpression e) {
    if (obj instanceof AffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildMod((AffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj instanceof QuasiAffineExpression
         && e instanceof JNIISLASTLiteral) {
      return _buildMod((QuasiAffineExpression)obj, (JNIISLASTLiteral)e);
    } else if (obj != null
         && e != null) {
      return _buildMod(obj, e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj, e).toString());
    }
  }
  
  public IntExpression buildExpr(final JNIISLASTExpression obj) {
    if (obj instanceof JNIISLASTIdentifier) {
      return _buildExpr((JNIISLASTIdentifier)obj);
    } else if (obj instanceof JNIISLASTLiteral) {
      return _buildExpr((JNIISLASTLiteral)obj);
    } else if (obj instanceof JNIISLASTOperation) {
      return _buildExpr((JNIISLASTOperation)obj);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
  
  public IntConstraint buildConstraint(final JNIISLASTExpression obj) {
    if (obj instanceof JNIISLASTOperation) {
      return _buildConstraint((JNIISLASTOperation)obj);
    } else if (obj != null) {
      return _buildConstraint(obj);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
  
  public IntConstraintSystem buildConstraintSystem(final JNIISLASTExpression obj) {
    if (obj instanceof JNIISLASTOperation) {
      return _buildConstraintSystem((JNIISLASTOperation)obj);
    } else if (obj != null) {
      return _buildConstraintSystem(obj);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
}
