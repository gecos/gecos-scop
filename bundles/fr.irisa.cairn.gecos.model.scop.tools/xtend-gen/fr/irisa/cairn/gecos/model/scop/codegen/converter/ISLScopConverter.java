package fr.irisa.cairn.gecos.model.scop.codegen.converter;

import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLExpressionConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLVariableConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.IISLASTNodeVisitor;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBlockNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTExpression;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTForNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIfNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNodeList;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUnscannedNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUserNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLVal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineTerm;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineFactory;
import org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
import org.polymodel.algebra.quasiAffine.QuasiAffineTerm;
import org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm;

@SuppressWarnings("all")
public class ISLScopConverter implements IISLASTNodeVisitor, ISLVariableConverter {
  protected Map<String, ScopDimension> varMap = new HashMap<String, ScopDimension>();
  
  protected ISLExpressionConverter exprConverter = new ISLExpressionConverter(this);
  
  public static List<IntConstraintSystem> convert(final JNIISLSet set, final List<ScopDimension> parms, final List<ScopDimension> idxs) {
    return new ISLScopConverter()._convert(set, parms, idxs);
  }
  
  public static IntConstraintSystem convert(final JNIISLBasicSet set, final List<ScopDimension> parms, final List<ScopDimension> idxs) {
    IntConstraintSystem _xblockexpression = null;
    {
      final List<IntConstraintSystem> res = ISLScopConverter.convert(set.copy().toSet(), parms, idxs);
      IntConstraintSystem _xifexpression = null;
      int _size = res.size();
      boolean _equals = (_size == 1);
      if (_equals) {
        _xifexpression = res.get(0);
      } else {
        int _size_1 = res.size();
        String _plus = ("Basic set should be converted to a single IntConstraintSystem object, not " + Integer.valueOf(_size_1));
        throw new RuntimeException(_plus);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public static ScopGuard guard(final ScopNode node, final JNIISLSet set) {
    ScopGuard _xblockexpression = null;
    {
      final ScopBlock block = ScopUserFactory.scopBlock();
      node.getParentScop().replace(node, block);
      EList<ScopNode> _children = block.getChildren();
      _children.add(node);
      final EList<ScopDimension> idxs = node.listAllEnclosingIterators();
      final EList<ScopDimension> parms = node.listRootParameters();
      JNIISLSet domain = set;
      int _size = idxs.size();
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size, true);
      for (final Integer i : _doubleDotLessThan) {
        domain = domain.setDimName(JNIISLDimType.isl_dim_set, (i).intValue(), idxs.get((i).intValue()).getSymName());
      }
      final List<IntConstraintSystem> ics = new ISLScopConverter()._convert(domain, parms, idxs);
      final ScopGuard g = ScopUserFactory.scopGuard(ics, node, null);
      EList<ScopNode> _children_1 = block.getChildren();
      _children_1.add(g);
      _xblockexpression = g;
    }
    return _xblockexpression;
  }
  
  public static List<IntExpression> convert(final JNIISLBasicMap bmap, final List<ScopDimension> parms, final List<ScopDimension> idxs) {
    return new ISLScopConverter()._convert(bmap, parms, idxs);
  }
  
  /**
   * Builds a list of expressions from given isl_multi_affine. The expressions
   * in the multi_aff should be function of given inputs/params.
   * @param maff
   * @param inputs
   * @param params
   * @return
   */
  @Deprecated
  public static List<IntExpression> exprList(final JNIISLMultiAff maff, final List<? extends Variable> inputs, final List<? extends Variable> params) {
    List<IntExpression> list = new ArrayList<IntExpression>();
    List<JNIISLAff> _affs = maff.getAffs();
    for (final JNIISLAff aff : _affs) {
      {
        IntExpression expr = ISLScopConverter.aff(aff, inputs, params).simplify();
        list.add(expr.simplify());
      }
    }
    return list;
  }
  
  public static QuasiAffineExpression aff(final JNIISLAff aff, final List<? extends Variable> variables, final List<? extends Variable> parameters) {
    QuasiAffineExpression res = QuasiAffineFactory.eINSTANCE.createQuasiAffineExpression();
    int nbDivs = aff.getNbDims(JNIISLDimType.isl_dim_div);
    for (int i = 0; (i < nbDivs); i++) {
      {
        long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_div, i).asLong();
        if ((c != 0)) {
          JNIISLAff child = aff.getDiv(i);
          QuasiAffineTerm divTerm = ISLScopConverter.div(child, variables, parameters);
          if ((c != 1)) {
            QuasiAffineExpression expr = IntExpressionBuilder.qaffine(divTerm);
            NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm();
            tmp.setCoef(c);
            tmp.setOperator(QuasiAffineOperator.MUL);
            tmp.setExpression(expr);
            divTerm = tmp;
          }
          res.getTerms().add(divTerm);
        }
      }
    }
    int nbVarsIn = aff.getNbDims(JNIISLDimType.isl_dim_in);
    for (int i = 0; (i < nbVarsIn); i++) {
      {
        long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_in, i).asLong();
        if ((c != 0)) {
          Variable var = variables.get(i);
          QuasiAffineTerm term = IntExpressionBuilder.mul(
            IntExpressionBuilder.affine(IntExpressionBuilder.term(c, var)), 1);
          res.getTerms().add(term);
        }
      }
    }
    int nbParams = aff.getNbDims(JNIISLDimType.isl_dim_param);
    for (int i = 0; (i < nbParams); i++) {
      {
        long c = aff.getCoefficientVal(JNIISLDimType.isl_dim_param, i).asLong();
        if ((c != 0)) {
          Variable var = parameters.get(i);
          QuasiAffineTerm term = IntExpressionBuilder.mul(
            IntExpressionBuilder.affine(IntExpressionBuilder.term(c, var)), 1);
          res.getTerms().add(term);
        }
      }
    }
    long cst = aff.getConstant();
    if (((cst != 0) || (res.getTerms().size() == 0))) {
      QuasiAffineTerm cstTerm = IntExpressionBuilder.mul(
        IntExpressionBuilder.affine(IntExpressionBuilder.term(cst)), 1);
      res.getTerms().add(cstTerm);
    }
    return res;
  }
  
  private static long getValidCoefficient(final JNIISLAff div, final long denom, final JNIISLVal cval) {
    long c = 0;
    if ((cval.isRational() && (cval.getNumerator() != 0))) {
      long denominator = cval.getDenominator();
      if ((denominator == denom)) {
        c = cval.getNumerator();
      } else {
        if (((denom % denominator) == 0)) {
          long _numerator = cval.getNumerator();
          long _multiply = (_numerator * (denom / denominator));
          c = _multiply;
        } else {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("JNIISLAff ");
          _builder.append(div);
          _builder.append(" with several distinct denominator values");
          throw new UnsupportedOperationException(_builder.toString());
        }
      }
    } else {
      c = cval.asLong();
    }
    return c;
  }
  
  public static QuasiAffineTerm div(final JNIISLAff div, final List<? extends Variable> variables, final List<? extends Variable> parameters) {
    int _size = variables.size();
    boolean _tripleEquals = (_size == 0);
    if (_tripleEquals) {
      System.err.println("problem");
      System.err.println(div);
      System.err.println(variables);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(div);
      throw new RuntimeException(_builder.toString());
    }
    QuasiAffineTerm res = null;
    List<AffineTerm> list = new ArrayList<AffineTerm>();
    long denom = div.getDenominator();
    long cst = div.getConstantVal().getNumerator();
    if ((cst != 0)) {
      AffineTerm cstTerm = IntExpressionBuilder.term(cst);
      list.add(cstTerm);
    }
    int n = div.getNbDims(JNIISLDimType.isl_dim_in);
    for (int i = 0; (i < n); i++) {
      {
        JNIISLVal cval = div.getCoefficientVal(JNIISLDimType.isl_dim_in, i);
        long c = ISLScopConverter.getValidCoefficient(div, denom, cval);
        if ((c != 0)) {
          Variable var = variables.get(i);
          AffineTerm term = IntExpressionBuilder.term(c, var);
          list.add(term);
        }
      }
    }
    n = div.getNbDims(JNIISLDimType.isl_dim_param);
    for (int i = 0; (i < n); i++) {
      {
        JNIISLVal cval = div.getCoefficientVal(JNIISLDimType.isl_dim_param, i);
        long c = ISLScopConverter.getValidCoefficient(div, denom, cval);
        if ((c != 0)) {
          Variable var = parameters.get(i);
          AffineTerm term = IntExpressionBuilder.term(c, var);
          list.add(term);
        }
      }
    }
    n = div.getNbDims(JNIISLDimType.isl_dim_div);
    int nbDivs = n;
    List<QuasiAffineTerm> qlist = new ArrayList<QuasiAffineTerm>(n);
    for (int i = 0; (i < n); i++) {
      {
        long c = div.getCoefficientVal(JNIISLDimType.isl_dim_div, i).asLong();
        if ((c != 0)) {
          JNIISLAff child = div.copy().getDiv(i);
          QuasiAffineTerm divTerm = ISLScopConverter.div(child, variables, parameters);
          if ((c != 1)) {
            QuasiAffineExpression expr = IntExpressionBuilder.qaffine(divTerm);
            NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm();
            tmp.setCoef(c);
            tmp.setOperator(QuasiAffineOperator.MUL);
            tmp.setExpression(expr);
            divTerm = tmp;
          }
          qlist.add(divTerm);
        } else {
          nbDivs--;
        }
      }
    }
    long denum = div.getDenominator();
    if ((nbDivs > 0)) {
      NestedQuasiAffineTerm tmp = QuasiAffineFactory.eINSTANCE.createNestedQuasiAffineTerm();
      tmp.setOperator(QuasiAffineOperator.FLOOR);
      tmp.setCoef(denum);
      QuasiAffineExpression expr = IntExpressionBuilder.qaffine(qlist);
      for (final AffineTerm t : list) {
        expr.getTerms().add(IntExpressionBuilder.qterm(t));
      }
      tmp.setExpression(expr);
      res = tmp;
    } else {
      SimpleQuasiAffineTerm tmp_1 = QuasiAffineFactory.eINSTANCE.createSimpleQuasiAffineTerm();
      tmp_1.setOperator(QuasiAffineOperator.FLOOR);
      tmp_1.setCoef(denum);
      tmp_1.setExpression(IntExpressionBuilder.affine(list));
      res = tmp_1;
    }
    return res;
  }
  
  public List<IntExpression> _convert(final JNIISLBasicMap bmap, final List<ScopDimension> parms, final List<ScopDimension> idxs) {
    this.varMap.clear();
    for (final ScopDimension p : parms) {
      this.varMap.put(p.getName(), p);
    }
    for (final ScopDimension i : idxs) {
      this.varMap.put(i.getName(), i);
    }
    boolean _isSingleValued = bmap.isSingleValued();
    boolean _not = (!_isSingleValued);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Cannot convert the non single valued relation ");
      _builder.append(bmap);
      _builder.append(" as IntExpression");
      throw new RuntimeException(_builder.toString());
    }
    boolean _isUniverse = bmap.getDomain().isUniverse();
    boolean _not_1 = (!_isUniverse);
    if (_not_1) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Cannot convert ");
      _builder_1.append(bmap);
      _builder_1.append(" as IntExpression since it has a domain, use projectOut before calling the conversion");
      throw new RuntimeException(_builder_1.toString());
    }
    int nbOut = bmap.getNbDims(JNIISLDimType.isl_dim_param);
    for (int i_1 = 0; (i_1 < nbOut); i_1++) {
      {
        JNIISLBasicMap res = bmap.copy();
        if ((i_1 > 0)) {
          res = res.projectOut(JNIISLDimType.isl_dim_param, 0, (i_1 - 1));
        }
        if ((i_1 < (nbOut - 2))) {
          res = res.projectOut(JNIISLDimType.isl_dim_param, (i_1 + 1), (nbOut - 1));
        }
      }
    }
    return null;
  }
  
  public List<IntConstraintSystem> _convert(final JNIISLSet set, final List<ScopDimension> parms, final List<ScopDimension> idxs) {
    this.varMap.clear();
    for (final ScopDimension p : parms) {
      this.varMap.put(p.getName(), p);
    }
    for (final ScopDimension i : idxs) {
      this.varMap.put(i.getName(), i);
    }
    int nbdims = set.getNbDims(JNIISLDimType.isl_dim_set);
    JNIISLSet domain = set.copy();
    domain = set.copy().moveDims(JNIISLDimType.isl_dim_param, 0, JNIISLDimType.isl_dim_set, 0, nbdims);
    JNIISLSpace space = domain.copy().getSpace();
    int nParams = space.copy().getNbDims(JNIISLDimType.isl_dim_param);
    JNIISLSpace allocParams = JNIISLSpace.allocParams(set.copy().getContext(), nParams);
    for (int i_1 = 0; (i_1 < domain.getNbParams()); i_1++) {
      {
        String paramName = domain.getDimName(JNIISLDimType.isl_dim_param, i_1);
        allocParams = allocParams.setName(JNIISLDimType.isl_dim_param, i_1, paramName);
      }
    }
    JNIISLSet ctxt = JNIISLSet.buildUniverse(allocParams.copy());
    JNIISLUnionMap scheduleUMap = domain.copy().toUnionSet().identity();
    JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(ctxt.copy());
    JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, scheduleUMap.copy());
    Object _accept = root.accept(this, null);
    List<IntConstraintSystem> res = ((List) _accept);
    return res;
  }
  
  @Override
  public Object visitJNIISLASTBlockNode(final JNIISLASTBlockNode obj, final Object arg) {
    JNIISLASTNodeList c = obj.getChildren();
    ScopBlock res = ScopFactory.eINSTANCE.createScopBlock();
    for (int i = 0; (i < c.getNumberOfChildren()); i++) {
      {
        JNIISLASTNode child = c.getChildrenAt(i);
        Object _accept = child.accept(this, arg);
        ScopNode scopNode = ((ScopNode) _accept);
        res.getChildren().add(scopNode);
      }
    }
    return res;
  }
  
  @Override
  public Object visitJNIISLASTForNode(final JNIISLASTForNode obj, final Object arg) {
    throw new RuntimeException("ForBloop should not appear here");
  }
  
  @Override
  public Object visitJNIISLASTIfNode(final JNIISLASTIfNode obj, final Object arg) {
    JNIISLASTExpression _cond = obj.getCond();
    JNIISLASTOperation cond = ((JNIISLASTOperation) _cond);
    EList<IntConstraintSystem> res = this.exprConverter.buildUnionOfConstraintSystem(cond);
    obj.getThen().accept(this, null);
    int _hasElse = obj.hasElse();
    boolean _tripleNotEquals = (_hasElse != 0);
    if (_tripleNotEquals) {
      throw new RuntimeException("Else should not appear here");
    }
    return res;
  }
  
  @Override
  public Object visitJNIISLASTUserNode(final JNIISLASTUserNode obj, final Object arg) {
    List<IntExpression> args = new ArrayList<IntExpression>();
    JNIISLASTExpression _expression = obj.getExpression();
    JNIISLASTOperation r = ((JNIISLASTOperation) _expression);
    for (int i = 1; (i < r.getNbArgs()); i++) {
      args.add(this.exprConverter.buildExpr(r.getArgument(i)));
    }
    return null;
  }
  
  @Override
  public Object visitJNIISLASTUnscannedNode(final JNIISLASTUnscannedNode obj, final Object arg) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("NYI");
    String _string = _builder.toString();
    throw new UnsupportedOperationException(_string);
  }
  
  @Override
  public Object visitJNIISLASTNode(final JNIISLASTNode obj, final Object arg) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("visitJNIISLASTNode not yet Implemented for ");
    _builder.append(obj);
    String _string = _builder.toString();
    throw new UnsupportedOperationException(_string);
  }
  
  @Override
  public ScopDimension buildVariable(final String id) {
    boolean _containsKey = this.varMap.containsKey(id);
    if (_containsKey) {
      return this.varMap.get(id);
    } else {
      Set<String> _keySet = this.varMap.keySet();
      String _plus = ((("Unknown dimension : " + id) + " not found in ") + _keySet);
      throw new UnsupportedOperationException(_plus);
    }
  }
}
