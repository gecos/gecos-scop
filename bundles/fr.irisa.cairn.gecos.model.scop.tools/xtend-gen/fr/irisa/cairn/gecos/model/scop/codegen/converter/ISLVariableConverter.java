package fr.irisa.cairn.gecos.model.scop.codegen.converter;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;

@SuppressWarnings("all")
public interface ISLVariableConverter {
  public abstract ScopDimension buildVariable(final String identifier);
}
