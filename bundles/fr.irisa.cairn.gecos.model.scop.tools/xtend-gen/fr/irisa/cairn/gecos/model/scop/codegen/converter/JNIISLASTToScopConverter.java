package fr.irisa.cairn.gecos.model.scop.codegen.converter;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlockStatement;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLExpressionConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLVariableConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBlockNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTExpression;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTForNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIdentifier;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTIfNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNodeList;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTOperation;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUnscannedNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTUserNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSpace;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.instrs.Instruction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class JNIISLASTToScopConverter implements ISLVariableConverter {
  private final HashMap<String, ScopDimension> dimMap = new HashMap<String, ScopDimension>();
  
  private final HashMap<String, ScopNode> stmtMap = new HashMap<String, ScopNode>();
  
  private final ArrayList<ScopDimension> iteratorList = new ArrayList<ScopDimension>();
  
  private int iteratorDepth = 0;
  
  private final ISLExpressionConverter exprConverter = new ISLExpressionConverter(this);
  
  private final ScopNode relativeRoot;
  
  private final static boolean DEBUG = false;
  
  public String debug(final Object mess) {
    String _xifexpression = null;
    if (JNIISLASTToScopConverter.DEBUG) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("[");
      String _simpleName = this.getClass().getSimpleName();
      _builder.append(_simpleName);
      _builder.append("] ");
      _builder.append(mess);
      _xifexpression = InputOutput.<String>println(_builder.toString());
    }
    return _xifexpression;
  }
  
  protected static ScopNode _adapt(final ScopNode original, final JNIISLASTNode ast) {
    return new JNIISLASTToScopConverter(original).compute(ast);
  }
  
  protected static ScopNode _adapt(final ScopUnexpandedNode original, final JNIISLASTNode ast) {
    EList<ScopDimension> _existentials = original.getExistentials();
    return new JNIISLASTToScopConverter(original, _existentials).compute(ast);
  }
  
  public static ScopNode adapt(final ScopUnexpandedNode original) {
    ScopNode _xblockexpression = null;
    {
      final JNIISLSet domain = JNIISLSet.buildFromString(original.getDomain());
      final JNIISLSpace allocParams = JNIISLSpace.allocParams(domain.copy().getContext(), 0);
      final JNIISLSet context = JNIISLSet.buildUniverse(allocParams);
      final JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context);
      JNIISLUnionMap schedule = JNIISLUnionMap.buildEmpty(domain.copy().identity().getSpace());
      schedule = schedule.addMap(domain.copy().identity());
      final JNIISLASTNode ast = JNIISLASTNode.buildFromSchedule(build, schedule);
      _xblockexpression = JNIISLASTToScopConverter.adapt(original, ast);
    }
    return _xblockexpression;
  }
  
  private JNIISLASTToScopConverter(final ScopNode original) {
    this.relativeRoot = original;
    final Consumer<ScopDimension> _function = (ScopDimension it) -> {
      this.dimMap.put(it.getSymName(), it);
    };
    original.listAllParameters().forEach(_function);
    final Consumer<ScopStatement> _function_1 = (ScopStatement it) -> {
      this.stmtMap.put(it.getId(), it);
    };
    original.listAllStatements().forEach(_function_1);
  }
  
  private JNIISLASTToScopConverter(final ScopNode original, final List<ScopDimension> iterators) {
    this.relativeRoot = original;
    final Consumer<ScopDimension> _function = (ScopDimension it) -> {
      this.dimMap.put(it.getSymName(), it);
    };
    original.listAllParameters().forEach(_function);
    final Consumer<ScopDimension> _function_1 = (ScopDimension d) -> {
      this.addNewIteratorToScheduledSpace(d);
    };
    iterators.forEach(_function_1);
    final Consumer<ScopStatement> _function_2 = (ScopStatement it) -> {
      this.stmtMap.put(it.getId(), it);
    };
    original.listAllStatements().forEach(_function_2);
  }
  
  private ScopNode compute(final JNIISLASTNode ast) {
    ScopNode _xblockexpression = null;
    {
      final ScopNode newNode = this.visit(ast);
      ScopNode _parentScop = this.relativeRoot.getParentScop();
      boolean _tripleNotEquals = (_parentScop != null);
      if (_tripleNotEquals) {
        this.relativeRoot.getParentScop().replace(this.relativeRoot, newNode);
      } else {
        ((GecosScopBlock) this.relativeRoot).setRoot(newNode);
      }
      _xblockexpression = newNode;
    }
    return _xblockexpression;
  }
  
  public boolean addNewIteratorToScheduledSpace(final ScopDimension d) {
    return this.iteratorList.add(d);
  }
  
  public ScopDimension getIteratorInScheduledSpace(final int depth) {
    ScopDimension _xblockexpression = null;
    {
      while ((this.iteratorList.size() <= depth)) {
        ScopDimension _newIterator = ScopIteratorGenerator.newIterator(this.relativeRoot);
        this.iteratorList.add(_newIterator);
      }
      _xblockexpression = this.iteratorList.get(depth);
    }
    return _xblockexpression;
  }
  
  private ScopNode _visit(final JNIISLASTForNode forNode) {
    ScopForLoop _xblockexpression = null;
    {
      final String iteratorId = this.getIdentifierId(forNode.getIterator());
      final ScopDimension iterator = this.getIteratorInScheduledSpace(this.iteratorDepth);
      this.dimMap.put(iteratorId, iterator);
      final IntExpression increment = this.exprConverter.buildExpr(forNode.getInc());
      final IntExpression ub = this.exprConverter.buildConstraint(forNode.getCond()).getUB(iterator);
      final IntExpression lb = this.exprConverter.buildExpr(forNode.getInit());
      this.iteratorDepth++;
      final ScopNode body = this.visit(forNode.getBody());
      this.iteratorDepth--;
      _xblockexpression = ScopUserFactory.scopFor(iterator, lb, ub, increment, body);
    }
    return _xblockexpression;
  }
  
  private ScopNode _visit(final JNIISLASTIfNode ifNode) {
    ScopGuard _xblockexpression = null;
    {
      JNIISLASTExpression _cond = ifNode.getCond();
      final EList<IntConstraintSystem> cond = this.exprConverter.buildUnionOfConstraintSystem(((JNIISLASTOperation) _cond));
      final ScopNode then = this.visit(ifNode.getThen());
      ScopNode else_ = null;
      int _hasElse = ifNode.hasElse();
      boolean _notEquals = (_hasElse != 0);
      if (_notEquals) {
        else_ = this.visit(ifNode.getElse());
      }
      _xblockexpression = ScopUserFactory.scopGuard(cond, then, else_);
    }
    return _xblockexpression;
  }
  
  private ScopNode _visit(final JNIISLASTBlockNode blockNode) {
    ScopBlock _xblockexpression = null;
    {
      final ScopBlock newBlock = ScopFactory.eINSTANCE.createScopBlock();
      final JNIISLASTNodeList children = blockNode.getChildren();
      int _numberOfChildren = children.getNumberOfChildren();
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _numberOfChildren, true);
      for (final Integer i : _doubleDotLessThan) {
        newBlock.getChildren().add(this.visit(children.getChildrenAt((i).intValue())));
      }
      _xblockexpression = newBlock;
    }
    return _xblockexpression;
  }
  
  private ScopNode _visit(final JNIISLASTUserNode userNode) {
    ScopNode _xblockexpression = null;
    {
      JNIISLASTExpression _expression = userNode.getExpression();
      final JNIISLASTOperation op = ((JNIISLASTOperation) _expression);
      final ScopNode stmt = this.stmtMap.get(this.getIdentifierId(op.getArgument(0)));
      _xblockexpression = this.buildStatement(stmt, userNode);
    }
    return _xblockexpression;
  }
  
  private ScopNode _visit(final JNIISLASTUnscannedNode unscannedNode) {
    ScopBlock _xblockexpression = null;
    {
      final JNIISLUnionMap schedule = unscannedNode.getSchedule();
      final ScopBlock newBlock = ScopFactory.eINSTANCE.createScopBlock();
      List<JNIISLMap> _maps = schedule.getMaps();
      for (final JNIISLMap map : _maps) {
        {
          ScopNode _get = this.stmtMap.get(map.getInputTupleName());
          final ScopInstructionStatement stmt = ((ScopInstructionStatement) _get);
          final EList<ScopDimension> existentials = stmt.listAllEnclosingIterators();
          InputOutput.<String>println(("stmt : " + stmt));
          final ScopUnexpandedStatement unexpStmt = ScopFactory.eINSTANCE.createScopUnexpandedStatement();
          unexpStmt.setId(map.getInputTupleName());
          EList<Instruction> _children = unexpStmt.getChildren();
          EList<Instruction> _children_1 = stmt.copy().getChildren();
          Iterables.<Instruction>addAll(_children, _children_1);
          unexpStmt.setSchedule(map.toString());
          EList<ScopDimension> _existentials = unexpStmt.getExistentials();
          Iterables.<ScopDimension>addAll(_existentials, existentials);
          EList<ScopNode> _children_2 = newBlock.getChildren();
          _children_2.add(unexpStmt);
        }
      }
      _xblockexpression = newBlock;
    }
    return _xblockexpression;
  }
  
  private ScopNode _buildStatement(final ScopNode stmt, final JNIISLASTUserNode userNode) {
    String _plus = (stmt + " is not a statement !");
    throw new UnsupportedOperationException(_plus);
  }
  
  private ScopNode _buildStatement(final ScopBlockStatement stmt, final JNIISLASTUserNode userNode) {
    throw new UnsupportedOperationException("[FIXME] ScopBlockStatement are not yet supported !");
  }
  
  private ScopNode _buildStatement(final ScopInstructionStatement stmt, final JNIISLASTUserNode userNode) {
    ScopNode _xblockexpression = null;
    {
      JNIISLASTExpression _expression = userNode.getExpression();
      final JNIISLASTOperation op = ((JNIISLASTOperation) _expression);
      if ((stmt == null)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Statement ");
        String _identifierId = this.getIdentifierId(op.getArgument(0));
        _builder.append(_identifierId);
        _builder.append(" is not registered in ");
        Set<String> _keySet = this.stmtMap.keySet();
        _builder.append(_keySet);
        throw new UnsupportedOperationException(_builder.toString());
      }
      final EList<ScopDimension> surroundingIndices = stmt.listAllEnclosingIterators(this.relativeRoot);
      if ((stmt instanceof ScopUnexpandedStatement)) {
        surroundingIndices.addAll(((ScopUnexpandedStatement)stmt).getExistentials());
      }
      final ArrayList<IntExpression> nodeArgs = new ArrayList<IntExpression>();
      this.debug(((("AstNode " + op) + " for ") + stmt));
      ScopNode _scopRoot = stmt.getScopRoot();
      EList<ScopDimension> _iterators = ((GecosScopBlock) _scopRoot).getIterators();
      String _plus = ("- ScopRoot iterators " + _iterators);
      this.debug(_plus);
      EList<ScopDimension> _listAllEnclosingIterators = stmt.listAllEnclosingIterators();
      String _plus_1 = ("- ScopNode enclosing iterators " + _listAllEnclosingIterators);
      this.debug(_plus_1);
      this.debug(("- Schedule space iterators " + this.iteratorList));
      this.debug(("- Conversion map " + this.dimMap));
      int _nbArgs = op.getNbArgs();
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(1, _nbArgs, true);
      for (final Integer i : _doubleDotLessThan) {
        {
          final IntExpression e = this.exprConverter.buildExpr(op.getArgument((i).intValue()));
          JNIISLASTExpression _argument = op.getArgument((i).intValue());
          String _plus_2 = ("- " + _argument);
          String _plus_3 = (_plus_2 + " converted to ");
          String _plus_4 = (_plus_3 + e);
          this.debug(_plus_4);
          nodeArgs.add(e);
        }
      }
      int _size = nodeArgs.size();
      int _size_1 = surroundingIndices.size();
      boolean _notEquals = (_size != _size_1);
      if (_notEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Inconsistency on Scop. The number of expected surrounding Iterators ");
        _builder_1.append(surroundingIndices);
        _builder_1.append(" does not match the number of arguments in the statement ");
        _builder_1.append(userNode);
        throw new UnsupportedOperationException(_builder_1.toString());
      }
      ScopNode newStmt = null;
      if ((stmt instanceof ScopUnexpandedStatement)) {
        final Function1<Instruction, Instruction> _function = (Instruction it) -> {
          return it.copy();
        };
        newStmt = ScopUserFactory.scopStatement(((ScopUnexpandedStatement)stmt).getName(), ListExtensions.<Instruction, Instruction>map(((ScopUnexpandedStatement)stmt).getChildren(), _function));
      } else {
        newStmt = stmt.copy();
      }
      int offset = 0;
      for (final ScopDimension dim : surroundingIndices) {
        {
          int _plusPlus = offset++;
          final IntExpression res = nodeArgs.get(_plusPlus);
          this.debug((((" Transforming " + dim) + " into ") + res));
          final EList<IntExpression> exprList = EMFUtils.<IntExpression>eAllContentsInstancesOf(newStmt, IntExpression.class);
          for (final IntExpression expr : exprList) {
            {
              IntExpression newExpr = expr.simplify();
              newExpr = newExpr.substitute(dim, res.<IntExpression>copy());
              EMFUtils.substituteByNewObjectInContainer(expr, newExpr);
            }
          }
        }
      }
      InputOutput.<String>println(("\t- new Statement is " + newStmt));
      _xblockexpression = newStmt;
    }
    return _xblockexpression;
  }
  
  private String getIdentifierId(final JNIISLASTExpression expr) {
    String _xblockexpression = null;
    {
      if ((!(expr instanceof JNIISLASTIdentifier))) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Cannot get identifier of ");
        _builder.append(expr);
        throw new UnsupportedOperationException(_builder.toString());
      }
      _xblockexpression = ((JNIISLASTIdentifier) expr).getID().getName();
    }
    return _xblockexpression;
  }
  
  @Override
  public ScopDimension buildVariable(final String identifier) {
    return this.dimMap.get(identifier);
  }
  
  public static ScopNode adapt(final ScopNode original, final JNIISLASTNode ast) {
    if (original instanceof ScopUnexpandedNode) {
      return _adapt((ScopUnexpandedNode)original, ast);
    } else if (original != null) {
      return _adapt(original, ast);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(original, ast).toString());
    }
  }
  
  private ScopNode visit(final JNIISLASTNode blockNode) {
    if (blockNode instanceof JNIISLASTBlockNode) {
      return _visit((JNIISLASTBlockNode)blockNode);
    } else if (blockNode instanceof JNIISLASTForNode) {
      return _visit((JNIISLASTForNode)blockNode);
    } else if (blockNode instanceof JNIISLASTIfNode) {
      return _visit((JNIISLASTIfNode)blockNode);
    } else if (blockNode instanceof JNIISLASTUnscannedNode) {
      return _visit((JNIISLASTUnscannedNode)blockNode);
    } else if (blockNode instanceof JNIISLASTUserNode) {
      return _visit((JNIISLASTUserNode)blockNode);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(blockNode).toString());
    }
  }
  
  private ScopNode buildStatement(final ScopNode stmt, final JNIISLASTUserNode userNode) {
    if (stmt instanceof ScopInstructionStatement) {
      return _buildStatement((ScopInstructionStatement)stmt, userNode);
    } else if (stmt instanceof ScopBlockStatement) {
      return _buildStatement((ScopBlockStatement)stmt, userNode);
    } else if (stmt != null) {
      return _buildStatement(stmt, userNode);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(stmt, userNode).toString());
    }
  }
}
