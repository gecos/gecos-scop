package fr.irisa.cairn.gecos.model.scop.codegen.fsm;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLPWMultiAffPiece;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.codegen.BouletFeautrierScanning;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Scope;
import gecos.gecosproject.GecosProject;
import gecos.types.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class LoopCoalescer {
  private ScopNode root;
  
  private ScopNode old;
  
  private GecosProject p;
  
  public LoopCoalescer(final GecosProject p) {
    this.p = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsFirstInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock s : scops) {
      this.coalesce(s.getRoot());
    }
  }
  
  public Map<JNIISLSet, JNIISLMultiAff> dropDomainConstraints(final JNIISLBasicMap m) {
    return m.getClosedFormRelation();
  }
  
  public void coalesce(final ScopNode node) {
    this.old = node;
    this.root = node.getParentScop();
    ScopNode _scopRoot = this.root.getScopRoot();
    final Scope scope = ((GecosScopBlock) _scopRoot).getScope();
    final EList<ScopDimension> parameters = this.root.listAllParameters();
    final JNIISLUnionMap schedule = ScopISLConverter.getAllIdSchedules(this.old);
    int _size = schedule.copy().getMaps().size();
    boolean _equals = (_size == 0);
    if (_equals) {
      throw new UnsupportedOperationException("Empty schedule (ScopNode may be dead code ?)");
    }
    final Iterable<String> schedDim = schedule.copy().getMaps().get(0).getOutputDimensionNames();
    final Type intType = this.root.listRootIterators().get(0).getSymbol().getType();
    final ScopFSMBlock fsm = ScopUserFactory.scopFSMBlock();
    for (final String name : schedDim) {
      {
        final ScopDimension dim = ScopUserFactory.dimSym(GecosUserCoreFactory.symbol(name, intType, scope));
        ScopNode _scopRoot_1 = node.getScopRoot();
        EList<ScopDimension> _iterators = ((GecosScopBlock) _scopRoot_1).getIterators();
        _iterators.add(dim);
        EList<ScopDimension> _iterators_1 = fsm.getIterators();
        _iterators_1.add(dim);
      }
    }
    final JNIISLUnionSet scannedDomain = schedule.copy().getRange();
    final JNIISLSet mergedScannedDomain = scannedDomain.copy().mergeAsSingleSet();
    final JNIISLMap next = BouletFeautrierScanning.buildNextMap(mergedScannedDomain.copy().toUnionSet());
    final JNIISLSet first = mergedScannedDomain.copy().lexMin();
    InputOutput.<String>println("Boulet & Feautrier ");
    int _nbBasicSets = first.getNbBasicSets();
    boolean _greaterThan = (_nbBasicSets > 1);
    if (_greaterThan) {
      throw new UnsupportedOperationException("piecewise FSM initialisation not yet implemented");
    }
    int _size_1 = first.copy().toPWMultiAff().getPieces().size();
    boolean _greaterThan_1 = (_size_1 > 1);
    if (_greaterThan_1) {
      throw new UnsupportedOperationException("Unexpected domain for initialization [piecewise FSM initialization not yet supported]");
    }
    final JNIISLPWMultiAffPiece p = first.copy().toPWMultiAff().getPieces().get(0);
    final List<IntExpression> exprs = ISLScopConverter.exprList(p.getMaff(), fsm.getIterators(), parameters);
    EList<ScopFSMTransition> _start = fsm.getStart();
    ScopFSMTransition _scopFSMTransition = ScopUserFactory.scopFSMTransition(new IntConstraintSystem[] {}, ((IntExpression[])Conversions.unwrapArray(exprs, IntExpression.class)), null);
    _start.add(_scopFSMTransition);
    final Map<JNIISLSet, JNIISLMultiAff> explicit = next.getClosedFormRelation();
    Set<JNIISLSet> _keySet = explicit.keySet();
    for (final JNIISLSet set : _keySet) {
      {
        final List<IntConstraintSystem> dom = ISLScopConverter.convert(set, fsm.getIterators(), parameters);
        final List<IntExpression> expList = ISLScopConverter.exprList(explicit.get(set), fsm.getIterators(), parameters);
        EList<ScopFSMTransition> _next = fsm.getNext();
        ScopFSMTransition _scopFSMTransition_1 = ScopUserFactory.scopFSMTransition(((IntConstraintSystem[])Conversions.unwrapArray(dom, IntConstraintSystem.class)), ((IntExpression[])Conversions.unwrapArray(expList, IntExpression.class)), null);
        _next.add(_scopFSMTransition_1);
      }
    }
    EList<ScopStatement> _listAllStatements = node.listAllStatements();
    for (final ScopStatement stmt : _listAllStatements) {
      {
        final JNIISLMap sched = ScopISLConverter.getIdSchedule(stmt, node);
        boolean _isEmpty = sched.isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          final JNIISLSet gistDomain = sched.getRange().copy().gist(mergedScannedDomain.copy());
          final List<IntConstraintSystem> constraints = ISLScopConverter.convert(gistDomain.copy(), fsm.getIterators(), parameters);
          ScopNode newStmt = stmt.copy();
          int offset = 0;
          List<ScopDimension> _reverseView = ListExtensions.<ScopDimension>reverseView(stmt.listAllEnclosingIterators(node));
          for (final ScopDimension d : _reverseView) {
            {
              final ScopDimension newDim = fsm.getIterators().get(((2 * offset) + 1));
              InputOutput.<String>println(((("substitute " + d) + " with ") + newDim));
              newStmt.substitute(d, newDim);
              offset++;
            }
          }
          if ((constraints != null)) {
            final ScopGuard guardNode = ScopUserFactory.scopGuard(constraints, newStmt, null);
            EList<ScopNode> _commands = fsm.getCommands();
            _commands.add(guardNode);
          } else {
            EList<ScopNode> _commands_1 = fsm.getCommands();
            _commands_1.add(newStmt);
          }
        }
      }
    }
    InputOutput.<ScopFSMBlock>print(fsm);
    node.getParentScop().replace(node, fsm);
  }
}
