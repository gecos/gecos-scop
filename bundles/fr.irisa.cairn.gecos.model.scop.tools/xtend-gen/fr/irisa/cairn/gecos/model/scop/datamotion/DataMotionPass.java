package fr.irisa.cairn.gecos.model.scop.datamotion;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionTransformer;
import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.annotations.IAnnotation;
import gecos.gecosproject.GecosProject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.xtext.xbase.lib.InputOutput;

@GSModule("Create copies of symbols used in a block annotated with a scop_data_motion (symbol) pragma.")
@SuppressWarnings("all")
public class DataMotionPass {
  private static boolean VERBOSE = false;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (DataMotionPass.VERBOSE) {
      _xifexpression = InputOutput.<String>println(string);
    }
    return _xifexpression;
  }
  
  private GecosProject proj;
  
  public DataMotionPass(final GecosProject proj) {
    this.proj = proj;
  }
  
  public void compute() {
    DataMotionPass.debug("DataMotionPass");
    EList<ScopNode> _eAllContentsInstancesOf = EMFUtils.<ScopNode>eAllContentsInstancesOf(this.proj, ScopNode.class);
    for (final ScopNode scop : _eAllContentsInstancesOf) {
      {
        EMap<String, IAnnotation> _annotations = scop.getAnnotations();
        IAnnotation _get = null;
        if (_annotations!=null) {
          _get=_annotations.get("GCS");
        }
        EList<ScopTransformDirective> _directives = null;
        if (((S2S4HLSPragmaAnnotation) _get)!=null) {
          _directives=((S2S4HLSPragmaAnnotation) _get).getDirectives();
        }
        Iterable<DataMotionPragma> _filter = null;
        if (_directives!=null) {
          _filter=Iterables.<DataMotionPragma>filter(_directives, DataMotionPragma.class);
        }
        final Iterable<DataMotionPragma> commands = _filter;
        if ((commands != null)) {
          for (final DataMotionPragma command : commands) {
            {
              DataMotionTransformer.insertDataMotion(scop, command.getSymbols());
              DataMotionPass.debug(("GecosScopBlock after copies\n" + scop));
            }
          }
        }
      }
    }
  }
}
