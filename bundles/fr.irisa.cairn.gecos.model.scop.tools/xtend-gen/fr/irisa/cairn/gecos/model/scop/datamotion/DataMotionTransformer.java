package fr.irisa.cairn.gecos.model.scop.datamotion;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.layout.ApplyMemoryLayout;
import fr.irisa.cairn.gecos.model.scop.layout.FindArrayContractionCandidates;
import fr.irisa.cairn.gecos.model.scop.layout.InferSliceSlindingWindow;
import fr.irisa.cairn.gecos.model.scop.layout.ScopArrayContract;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class DataMotionTransformer {
  private static boolean VERBOSE = true;
  
  private static boolean CONTRACT = true;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (DataMotionTransformer.VERBOSE) {
      _xifexpression = InputOutput.<String>println(string);
    }
    return _xifexpression;
  }
  
  public static ScopBlock insertDataMotion(final ScopNode node) {
    return DataMotionTransformer.insertDataMotion(node, node.listAllReferencedSymbols());
  }
  
  public static ScopBlock insertDataMotion(final ScopNode node, final List<Symbol> symbolsToInspect) {
    ScopBlock _xblockexpression = null;
    {
      DataMotionTransformer.debug(("Move local data for node:\n" + node));
      final Set<Symbol> symbolsToCopy = IterableExtensions.<Symbol>toSet(symbolsToInspect);
      final HashMap<Symbol, JNIISLSet> inputCopies = new HashMap<Symbol, JNIISLSet>();
      final HashMap<Symbol, JNIISLSet> outputCopies = new HashMap<Symbol, JNIISLSet>();
      for (final Symbol symbol : symbolsToCopy) {
        {
          final JNIISLSet flowIn = ScopAccessAnalyzer.computeFlowIn(symbol, node).coalesce().convexHull().toSet();
          boolean _isEmpty = flowIn.isEmpty();
          boolean _not = (!_isEmpty);
          if (_not) {
            DataMotionTransformer.debug(((("Flow In for " + symbol) + " ") + flowIn));
            inputCopies.put(symbol, flowIn);
          }
          final JNIISLSet flowOut = ScopAccessAnalyzer.computeFlowOut(symbol, node).coalesce();
          boolean _isEmpty_1 = flowOut.isEmpty();
          boolean _not_1 = (!_isEmpty_1);
          if (_not_1) {
            DataMotionTransformer.debug(((("Flow Out for " + symbol) + " ") + flowOut));
            outputCopies.put(symbol, flowOut);
          }
        }
      }
      final ScopBlock containingBlock = ScopUserFactory.scopBlock();
      node.getParentScop().replace(node, containingBlock);
      containingBlock.getAnnotations().addAll(node.getAnnotations());
      ScopBlock inputBlock = null;
      boolean _isEmpty = inputCopies.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        inputBlock = ScopUserFactory.scopBlock();
        containingBlock.getChildren().add(inputBlock);
      }
      containingBlock.getChildren().add(node);
      ScopBlock outputBlock = null;
      boolean _isEmpty_1 = outputCopies.isEmpty();
      boolean _not_1 = (!_isEmpty_1);
      if (_not_1) {
        outputBlock = ScopUserFactory.scopBlock();
        containingBlock.getChildren().add(outputBlock);
      }
      ScopNode _scopRoot = node.getScopRoot();
      final Scope scope = ((GecosScopBlock) _scopRoot).getScope();
      final ArrayList<Symbol> copySymbols = new ArrayList<Symbol>();
      for (final Symbol symbol_1 : symbolsToCopy) {
        {
          int nb = 0;
          while ((scope.lookup(((symbol_1.getName() + "_copy") + Integer.valueOf(nb))) != null)) {
            nb++;
          }
          String _name = symbol_1.getName();
          String _plus = (_name + "_copy");
          String _plus_1 = (_plus + Integer.valueOf(nb));
          final Symbol copySymbol = GecosUserCoreFactory.symbol(_plus_1, symbol_1.getType(), scope);
          boolean _isConstant = copySymbol.getType().isConstant();
          if (_isConstant) {
          }
          copySymbols.add(copySymbol);
          boolean _containsKey = inputCopies.containsKey(symbol_1);
          if (_containsKey) {
            ISLCodegenUtils.dataMover(inputCopies.get(symbol_1), inputBlock, symbol_1, copySymbol);
          }
          boolean _containsKey_1 = outputCopies.containsKey(symbol_1);
          if (_containsKey_1) {
            ISLCodegenUtils.dataMover(outputCopies.get(symbol_1), outputBlock, copySymbol, symbol_1);
          }
          final Function1<ScopAccess, Boolean> _function = (ScopAccess it) -> {
            Symbol _sym = it.getSym();
            return Boolean.valueOf(Objects.equal(_sym, symbol_1));
          };
          final Consumer<ScopAccess> _function_1 = (ScopAccess it) -> {
            it.setSym(copySymbol);
          };
          IterableExtensions.<ScopAccess>filter(node.listAllAccesses(), _function).forEach(_function_1);
        }
      }
      if (DataMotionTransformer.CONTRACT) {
        final Function1<Symbol, Boolean> _function = (Symbol s) -> {
          return Boolean.valueOf((s.getType().isArray() && (!s.getContainingScope().isGlobal())));
        };
        Iterable<Symbol> _filter = IterableExtensions.<Symbol>filter(copySymbols, _function);
        for (final Symbol copySymbol : _filter) {
          FindArrayContractionCandidates.createDirective(containingBlock, copySymbol);
        }
        new ScopArrayContract(containingBlock).compute();
      } else {
        for (final Symbol copySymbol_1 : copySymbols) {
          InferSliceSlindingWindow.createDirective(containingBlock, copySymbol_1);
        }
      }
      new ApplyMemoryLayout(containingBlock).compute();
      DataMotionTransformer.debug(("Moved local data for Scop " + containingBlock));
      EList<ScopStatement> _listAllStatements = node.listAllStatements();
      String _plus = ("Statements : " + _listAllStatements);
      DataMotionTransformer.debug(_plus);
      _xblockexpression = containingBlock;
    }
    return _xblockexpression;
  }
}
