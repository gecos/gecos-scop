package fr.irisa.cairn.gecos.model.scop.factory;

import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.internal.TransformTomFactory;
import gecos.core.Symbol;
import java.util.Collections;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;

@SuppressWarnings("all")
public class ScopTransformFactory extends TransformTomFactory {
  public static DataLayoutDirective createLayout(final Symbol sym) {
    return TransformTomFactory.createLayout(Collections.<IntExpression>unmodifiableList(CollectionLiterals.<IntExpression>newArrayList()), Collections.<Variable>unmodifiableList(CollectionLiterals.<Variable>newArrayList()), sym);
  }
}
