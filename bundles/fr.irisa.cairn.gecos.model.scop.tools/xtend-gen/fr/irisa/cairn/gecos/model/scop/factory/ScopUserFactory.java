package fr.irisa.cairn.gecos.model.scop.factory;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.extractor.internal.ExtractorTomFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMState;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopParameter;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.internal.ScopTomFactory;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.gecos.model.scop.transform.internal.TransformTomFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import gecos.core.CoreFactory;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.AlgebraUserFactory;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffineTerm;

@SuppressWarnings("all")
public class ScopUserFactory {
  private static Scope scope;
  
  public static Scope setScope(final Scope s) {
    return ScopUserFactory.scope = s;
  }
  
  public static ScopUnexpandedStatement unexpandedStmt(final List<ScopDimension> iterators, final String d, final String sched) {
    return ScopTomFactory.createUnexpanded(((List) iterators), d, sched);
  }
  
  public static GecosScopBlock root() {
    return ScopFactory.eINSTANCE.createGecosScopBlock();
  }
  
  public static GecosScopBlock root(final ScopNode n) {
    GecosScopBlock _xblockexpression = null;
    {
      final GecosScopBlock g = ScopFactory.eINSTANCE.createGecosScopBlock();
      g.setScope(CoreFactory.eINSTANCE.createScope());
      g.setRoot(n);
      _xblockexpression = g;
    }
    return _xblockexpression;
  }
  
  public static ScopDimension dimSym(final Symbol s) {
    ScopDimension _xblockexpression = null;
    {
      ScopDimension dim = ScopFactory.eINSTANCE.createScopDimension();
      dim.setSymbol(((Symbol) s));
      dim.setName(s.getName());
      _xblockexpression = dim;
    }
    return _xblockexpression;
  }
  
  public static ScopDimension dim(final String name) {
    ScopDimension _xblockexpression = null;
    {
      ScopDimension dim = ScopFactory.eINSTANCE.createScopDimension();
      GecosUserTypeFactory.setScope(ScopUserFactory.scope);
      dim.setSymbol(ScopUserFactory.scope.lookup(name));
      Symbol _symbol = dim.getSymbol();
      boolean _tripleEquals = (_symbol == null);
      if (_tripleEquals) {
        dim.setSymbol(GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT(), ScopUserFactory.scope));
      }
      dim.setName(name);
      _xblockexpression = dim;
    }
    return _xblockexpression;
  }
  
  public static ScopParameter param(final Symbol p) {
    ScopParameter _xblockexpression = null;
    {
      ScopParameter dim = ScopFactory.eINSTANCE.createScopParameter();
      dim.setSymbol(p);
      dim.setName(p.getName());
      _xblockexpression = dim;
    }
    return _xblockexpression;
  }
  
  public static ScopParameter param(final String name) {
    ScopParameter _xblockexpression = null;
    {
      ScopParameter param = ScopFactory.eINSTANCE.createScopParameter();
      GecosUserTypeFactory.setScope(ScopUserFactory.scope);
      param.setSymbol(GecosUserCoreFactory.symbol(name, GecosUserTypeFactory.INT(), ScopUserFactory.scope));
      param.setName(name);
      _xblockexpression = param;
    }
    return _xblockexpression;
  }
  
  public static ScopForLoop scopFor(final Variable i, final IntExpression lb, final IntExpression ub, final IntExpression s, final ScopNode body) {
    return ScopTomFactory.createLoop(i, lb, ub, s, body);
  }
  
  public static ScopForLoop scopFor(final ScopDimension i, final int lb, final int ub, final int s, final ScopNode body) {
    AffineTerm _constant = AlgebraUserFactory.constant(((long) lb));
    AffineTerm _constant_1 = AlgebraUserFactory.constant(((long) ub));
    AffineTerm _constant_2 = AlgebraUserFactory.constant(((long) s));
    return ScopTomFactory.createLoop(i, 
      AlgebraUserFactory.affine(Collections.<AffineTerm>unmodifiableList(CollectionLiterals.<AffineTerm>newArrayList(_constant))), 
      AlgebraUserFactory.affine(Collections.<AffineTerm>unmodifiableList(CollectionLiterals.<AffineTerm>newArrayList(_constant_1))), 
      AlgebraUserFactory.affine(Collections.<AffineTerm>unmodifiableList(CollectionLiterals.<AffineTerm>newArrayList(_constant_2))), body);
  }
  
  public static ScopBlock scopBlock(final ScopNode c) {
    return ScopTomFactory.createScopBlk(Collections.<ScopNode>unmodifiableList(CollectionLiterals.<ScopNode>newArrayList(c)));
  }
  
  public static ScopBlock scopBlock(final ScopNode... c) {
    return ScopTomFactory.createScopBlk(IterableExtensions.<ScopNode>toList(((Iterable<ScopNode>)Conversions.doWrapArray(c))));
  }
  
  public static ScopGuard scopGuard(final List<IntConstraintSystem> res, final ScopNode scopBlock, final ScopNode object) {
    return ScopTomFactory.createGuard(res, scopBlock, object);
  }
  
  public static ScopIntExpression sexpr(final IntExpression arg) {
    return ExtractorTomFactory.createSexpr(arg);
  }
  
  public static ScopIntExpression sexpr(final ScopDimension i) {
    AffineTerm _term = AlgebraUserFactory.term(1, i);
    return ExtractorTomFactory.createSexpr(AlgebraUserFactory.affine(Collections.<AffineTerm>unmodifiableList(CollectionLiterals.<AffineTerm>newArrayList(_term))));
  }
  
  public static ScopIntExpression sexpr(final long coef, final ScopDimension i) {
    AffineTerm _term = AlgebraUserFactory.term(coef, i);
    return ExtractorTomFactory.createSexpr(AlgebraUserFactory.affine(Collections.<AffineTerm>unmodifiableList(CollectionLiterals.<AffineTerm>newArrayList(_term))));
  }
  
  public static ScopRegionRead regionRead(final Symbol array, final List<ScopDimension> existentials, final IntConstraintSystem ics) {
    ScopRegionRead _xblockexpression = null;
    {
      final ScopRegionRead regionRead = ScopTomFactory.createRegionRead(array, existentials, ics);
      final Consumer<ScopDimension> _function = (ScopDimension e) -> {
        EList<IntExpression> _indexExpressions = regionRead.getIndexExpressions();
        AffineExpression _affine = AlgebraUserFactory.affine(e);
        _indexExpressions.add(_affine);
      };
      existentials.forEach(_function);
      _xblockexpression = regionRead;
    }
    return _xblockexpression;
  }
  
  public static ScopInstructionStatement scopStatement(final String name, final Instruction... args) {
    ArrayList<Instruction> _arrayList = new ArrayList<Instruction>((Collection<? extends Instruction>)Conversions.doWrapArray(args));
    return ScopTomFactory.createStmt(name, _arrayList);
  }
  
  public static ScopInstructionStatement scopStatement(final String name, final List<Instruction> args) {
    ArrayList<Instruction> _arrayList = new ArrayList<Instruction>(args);
    return ScopTomFactory.createStmt(name, _arrayList);
  }
  
  public static ScopStatement scopStatement(final String name) {
    return ScopTomFactory.createStmt(name, Collections.<Instruction>unmodifiableList(CollectionLiterals.<Instruction>newArrayList()));
  }
  
  public static ScopWrite scopWrite(final Symbol sym, final List<IntExpression> index) {
    ScopWrite _xblockexpression = null;
    {
      Type _type = sym.getType();
      final ArrayType atype = ((ArrayType) _type);
      ScopWrite _xifexpression = null;
      int _size = index.size();
      int _nbDims = atype.getNbDims();
      boolean _notEquals = (_size != _nbDims);
      if (_notEquals) {
        throw new UnsupportedOperationException("inconsistent scalar array type");
      } else {
        ScopWrite _xblockexpression_1 = null;
        {
          final ScopWrite res = ScopTomFactory.createWrite(sym, index);
          res.setType(atype.getInnermostBase());
          _xblockexpression_1 = res;
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public static ScopRead scopRead(final Symbol sym, final List<IntExpression> index) {
    ScopRead _xblockexpression = null;
    {
      Type _type = sym.getType();
      final ArrayType atype = ((ArrayType) _type);
      ScopRead _xifexpression = null;
      int _size = index.size();
      int _nbDims = atype.getNbDims();
      boolean _notEquals = (_size != _nbDims);
      if (_notEquals) {
        throw new UnsupportedOperationException("inconsistent scalar array type");
      } else {
        ScopRead _xblockexpression_1 = null;
        {
          final ScopRead res = ScopTomFactory.createRead(sym, index);
          res.setType(atype.getInnermostBase());
          _xblockexpression_1 = res;
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public static ScopFSMBlock scopFSMBlock() {
    return ScopFactory.eINSTANCE.createScopFSMBlock();
  }
  
  public static ScopFSMState scopFSMState() {
    return ScopFactory.eINSTANCE.createScopFSMState();
  }
  
  public static ScopFSMTransition scopFSMTransition() {
    return ScopFactory.eINSTANCE.createScopFSMTransition();
  }
  
  public static ScopFSMTransition scopFSMTransition(final IntConstraintSystem[] domain, final IntExpression[] nextIteration, final ScopFSMState next) {
    ScopFSMTransition _xblockexpression = null;
    {
      final ScopFSMTransition r = ScopUserFactory.scopFSMTransition();
      EList<IntConstraintSystem> _domain = r.getDomain();
      Iterables.<IntConstraintSystem>addAll(_domain, ((Iterable<? extends IntConstraintSystem>)Conversions.doWrapArray(domain)));
      EList<IntExpression> _nextIteration = r.getNextIteration();
      Iterables.<IntExpression>addAll(_nextIteration, ((Iterable<? extends IntExpression>)Conversions.doWrapArray(nextIteration)));
      _xblockexpression = r;
    }
    return _xblockexpression;
  }
  
  public static ScopTransform scopTransform(final ScopNode node, final ScopTransformDirective... a) {
    ScopTransform _xblockexpression = null;
    {
      ScopTransform res = TransformFactory.eINSTANCE.createScopTransform();
      node.setTransform(res);
      EList<ScopTransformDirective> _commands = res.getCommands();
      Iterables.<ScopTransformDirective>addAll(_commands, ((Iterable<? extends ScopTransformDirective>)Conversions.doWrapArray(a)));
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static LivenessDirective livenessDirective(final Symbol s) {
    LivenessDirective _xblockexpression = null;
    {
      LivenessDirective res = TransformFactory.eINSTANCE.createLivenessDirective();
      res.setSymbol(s);
      res.setLiveIn(true);
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public static ScheduleDirective scheduleDirective(final ScopNode node, final JNIISLMap schedule) {
    return TransformTomFactory.createSchedule(((Instruction) node), schedule.toString());
  }
  
  public static DataLayoutDirective layoutDirective(final Symbol s, final List<ScopDimension> vars, final List<IntExpression> exprs) {
    return TransformTomFactory.createLayout(exprs, ((List) vars), s);
  }
}
