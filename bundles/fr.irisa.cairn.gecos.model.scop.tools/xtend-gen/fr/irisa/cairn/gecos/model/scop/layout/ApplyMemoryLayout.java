package fr.irisa.cairn.gecos.model.scop.layout;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;
import java.util.HashMap;
import java.util.List;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.CompositeIntExpression;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.tom.SimplifyIntExpression;

@SuppressWarnings("all")
public class ApplyMemoryLayout {
  private static boolean debug = false;
  
  private EObject target;
  
  public ApplyMemoryLayout(final EObject target) {
    this.target = target;
  }
  
  public void compute() {
    List<ScopNode> roots = null;
    if ((this.target instanceof ScopNode)) {
      roots = CollectionLiterals.<ScopNode>newArrayList(((ScopNode)this.target));
    } else {
      roots = EMFUtils.<ScopNode>eAllContentsFirstInstancesOf(this.target, ScopNode.class);
    }
    for (final ScopNode root : roots) {
      {
        ScopTransform _transform = root.getTransform();
        EList<ScopTransformDirective> _commands = null;
        if (_transform!=null) {
          _commands=_transform.getCommands();
        }
        Iterable<DataLayoutDirective> _filter = null;
        if (_commands!=null) {
          _filter=Iterables.<DataLayoutDirective>filter(_commands, DataLayoutDirective.class);
        }
        List<DataLayoutDirective> _list = null;
        if (_filter!=null) {
          _list=IterableExtensions.<DataLayoutDirective>toList(_filter);
        }
        final List<DataLayoutDirective> workList = _list;
        if ((workList != null)) {
          this.apply(workList, root);
        }
      }
    }
  }
  
  private void apply(final List<DataLayoutDirective> newCommands, final ScopNode root) {
    int _size = newCommands.size();
    final HashMap<Symbol, DataLayoutDirective> mapping = new HashMap<Symbol, DataLayoutDirective>(_size);
    for (final DataLayoutDirective dir : newCommands) {
      {
        final Symbol sym = dir.getSymbol();
        mapping.put(sym, dir);
        final EList<IntExpression> addresses = dir.getAddresses();
        GecosUserTypeFactory.setScope(sym.getContainingScope());
        final Type newType = this.createType(addresses, sym.getType());
        sym.setType(newType);
      }
    }
    EList<ScopStatement> _listAllStatements = root.listAllStatements();
    for (final ScopStatement stmt : _listAllStatements) {
      {
        EList<ScopWrite> _listAllWriteAccess = stmt.listAllWriteAccess();
        for (final ScopWrite write : _listAllWriteAccess) {
          boolean _containsKey = mapping.containsKey(write.getSym());
          if (_containsKey) {
            if (ApplyMemoryLayout.debug) {
              System.out.println(("updating " + write));
            }
            this.update(write, mapping.get(write.getSym()));
            if (ApplyMemoryLayout.debug) {
              System.out.println((" >> " + write));
            }
          }
        }
        EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
        for (final ScopRead read : _listAllReadAccess) {
          boolean _containsKey_1 = mapping.containsKey(read.getSym());
          if (_containsKey_1) {
            if (ApplyMemoryLayout.debug) {
              System.out.println(("updating " + read));
            }
            this.update(read, mapping.get(read.getSym()));
            if (ApplyMemoryLayout.debug) {
              System.out.println((" >> " + read));
            }
          }
        }
      }
    }
  }
  
  private Type createType(final EList<IntExpression> sizes, final Type oldType) {
    Type basetype = null;
    if ((oldType instanceof BaseType)) {
      basetype = oldType;
    } else {
      if ((oldType instanceof ArrayType)) {
        basetype = ((ArrayType)oldType).getInnermostBase();
      } else {
        throw new UnsupportedOperationException();
      }
    }
    for (int i = (sizes.size() - 1); (i >= 0); i--) {
      {
        final IntExpression size = sizes.get(i);
        boolean _isTrueDimension = this.isTrueDimension(size);
        if (_isTrueDimension) {
          final ScopIntExpression sizeInstr = ScopFactory.eINSTANCE.createScopIntExpression();
          sizeInstr.setExpr(size.<IntExpression>copy());
          sizeInstr.setType(GecosUserTypeFactory.INT());
          final ArrayType arrayType = GecosUserTypeFactory.ARRAY(basetype, sizeInstr);
          basetype = arrayType;
        }
      }
    }
    return basetype;
  }
  
  private boolean isTrueDimension(final IntExpression size) {
    return (!(Objects.equal(size.isConstant(), FuzzyBoolean.YES) && (size.toAffine().getConstantTerm().getCoef() == 1)));
  }
  
  public void update(final ScopAccess scopAccess, final DataLayoutDirective dir) {
    final EList<IntExpression> origIndexes = scopAccess.getIndexExpressions();
    final BasicEList<IntExpression> newIndexes = new BasicEList<IntExpression>();
    final EList<IntExpression> addresses = dir.getAddresses();
    final Function1<ScopDimension, String> _function = (ScopDimension it) -> {
      return it.getSymName();
    };
    final String parameters = ListExtensions.<ScopDimension, String>map(scopAccess.listRootParameters(), _function).toString();
    final JNIISLSet range = ScopISLConverter.getAccessMap(scopAccess).range().clearTupleName();
    for (int i = 0; (i < origIndexes.size()); i++) {
      {
        final IntExpression exp = origIndexes.get(i);
        final IntExpression size = addresses.get(i);
        boolean _isTrueDimension = this.isTrueDimension(size);
        if (_isTrueDimension) {
          final JNIISLSet rangeDim = range.copy().projectIn(JNIISLDimType.isl_dim_set, i);
          StringConcatenation _builder = new StringConcatenation();
          _builder.append(parameters);
          _builder.append(" -> { [exclude] : ");
          String _replace = size.toString().replace("[", "(").replace("]", ")");
          _builder.append(_replace);
          _builder.append(" - 1 < exclude }");
          final JNIISLSet exclusionRange = JNIISLSet.buildFromString(_builder.toString());
          final JNIISLSet outOfBoundAccess = rangeDim.intersect(exclusionRange);
          boolean _isEmpty = outOfBoundAccess.isEmpty();
          boolean _not = (!_isEmpty);
          if (_not) {
            final CompositeIntExpression mod = IntExpressionBuilder.symbolicMod(exp.<IntExpression>copy(), size.<IntExpression>copy());
            newIndexes.add(SimplifyIntExpression.simplify(mod));
          } else {
            newIndexes.add(exp.<IntExpression>copy());
          }
        }
      }
    }
    origIndexes.clear();
    origIndexes.addAll(newIndexes);
  }
}
