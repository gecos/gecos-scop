package fr.irisa.cairn.gecos.model.scop.layout;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery;
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class FindArrayContractionCandidates {
  private GecosProject proj;
  
  public FindArrayContractionCandidates(final GecosProject proj) {
    this.proj = proj;
  }
  
  public void compute() {
    List<GecosScopBlock> roots = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    final Consumer<GecosScopBlock> _function = (GecosScopBlock r) -> {
      this.compute(r);
    };
    roots.forEach(_function);
  }
  
  public void compute(final GecosScopBlock root) {
    HashSet<Symbol> syms = ScopAccessQuery.getAllUsedSymbols(root);
    EList<Symbol> _liveInSymbols = root.getLiveInSymbols();
    Iterables.removeAll(syms, _liveInSymbols);
    EList<Symbol> _liveOutSymbols = root.getLiveOutSymbols();
    Iterables.removeAll(syms, _liveOutSymbols);
    final Function1<Symbol, Boolean> _function = (Symbol it) -> {
      return Boolean.valueOf(it.getType().isArray());
    };
    Iterable<Symbol> _filter = IterableExtensions.<Symbol>filter(syms, _function);
    for (final Symbol s : _filter) {
      FindArrayContractionCandidates.createDirective(root, s);
    }
  }
  
  public static Object createDirective(final ScopNode node, final Symbol s) {
    Object _xblockexpression = null;
    {
      final JNIISLUnionSet writes = ScopISLConverter.getAllWriteAccessMaps(node, s).range();
      final JNIISLUnionSet reads = ScopISLConverter.getAllReadAccessMaps(node, s).range();
      Object _xifexpression = null;
      boolean _isEqual = writes.isEqual(reads);
      if (_isEqual) {
        boolean _xblockexpression_1 = false;
        {
          String _name = s.getName();
          String _plus = ("Symbol " + _name);
          String _plus_1 = (_plus + " is a candidate for contraction");
          InputOutput.<String>println(_plus_1);
          final ContractArrayDirective dir = TransformFactory.eINSTANCE.createContractArrayDirective();
          dir.setSymbol(s);
          ScopTransform _transform = node.getTransform();
          boolean _tripleEquals = (_transform == null);
          if (_tripleEquals) {
            node.setTransform(TransformFactory.eINSTANCE.createScopTransform());
          }
          _xblockexpression_1 = node.getTransform().getCommands().add(dir);
        }
        _xifexpression = Boolean.valueOf(_xblockexpression_1);
      } else {
        String _name = s.getName();
        String _plus = ("Symbol " + _name);
        String _plus_1 = (_plus + " reads do no match writes (possible dead array cells), do not contract");
        _xifexpression = InputOutput.<String>println(_plus_1);
      }
      _xblockexpression = ((Object)_xifexpression);
    }
    return _xblockexpression;
  }
}
