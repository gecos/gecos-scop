package fr.irisa.cairn.gecos.model.scop.layout;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopTransformFactory;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLPWMultiAffPiece;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import gecos.core.Symbol;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;

@SuppressWarnings("all")
public class InferSliceSlindingWindow {
  public static JNIISLMultiAff createDirective(final ScopNode node, final Symbol s) {
    JNIISLMultiAff _xblockexpression = null;
    {
      final JNIISLSet footprint = ScopAccessAnalyzer.getArrayAccessFootprint(s, node);
      final int nbdims = footprint.getNbDims();
      JNIISLMap map = JNIISLMap.fromDomain(footprint).simpleHull().toMap();
      int nbLocalParams = node.listAllParameters().size();
      int nbRootParams = node.listRootParameters().size();
      map = map.moveDims(JNIISLDimType.isl_dim_out, 0, JNIISLDimType.isl_dim_in, 0, nbdims);
      map = map.moveDims(JNIISLDimType.isl_dim_in, 0, JNIISLDimType.isl_dim_param, nbRootParams, (nbLocalParams - nbRootParams));
      final DataLayoutDirective layout = ScopTransformFactory.createLayout(s);
      int _nbParams = map.getNbParams();
      final Function1<Integer, ScopDimension> _function = (Integer v) -> {
        return ScopUserFactory.dim(("p" + v));
      };
      final List<ScopDimension> params = IterableExtensions.<ScopDimension>toList(IterableExtensions.<Integer, ScopDimension>map(new IntegerRange(0, _nbParams), _function));
      int _nbIns = map.getNbIns();
      final Function1<Integer, ScopDimension> _function_1 = (Integer v) -> {
        return ScopUserFactory.dim(("i" + v));
      };
      final List<ScopDimension> index = IterableExtensions.<ScopDimension>toList(IterableExtensions.<Integer, ScopDimension>map(new IntegerRange(0, _nbIns), _function_1));
      EList<ScopDimension> _vars = layout.getVars();
      Iterables.<ScopDimension>addAll(_vars, params);
      EList<ScopDimension> _vars_1 = layout.getVars();
      Iterables.<ScopDimension>addAll(_vars_1, index);
      JNIISLMultiAff res = null;
      int _nbIns_1 = map.getNbIns();
      int _minus = (_nbIns_1 - 1);
      IntegerRange _upTo = new IntegerRange(0, _minus);
      for (final Integer i : _upTo) {
        {
          String _dimName = map.getDimName(JNIISLDimType.isl_dim_out, (i).intValue());
          String _plus = ("dimension " + _dimName);
          String _plus_1 = (_plus + " for ");
          String _plus_2 = (_plus_1 + map);
          InputOutput.<String>println(_plus_2);
          JNIISLMap newMap = map.copy().projectOutAllBut(JNIISLDimType.isl_dim_out, (i).intValue());
          final JNIISLMap _min = newMap.copy().lexMin();
          final JNIISLMap _max = newMap.copy().lexMax();
          JNIISLSet width = _min.copy().reverse().applyRange(_max).deltas();
          boolean _involvesDims = width.involvesDims(JNIISLDimType.isl_dim_param, 0, width.getNbParams());
          if (_involvesDims) {
            JNIISLSet noParamwidth = width.copy().projectOut(JNIISLDimType.isl_dim_param, 0, width.getNbParams());
            InputOutput.<String>println(("\t - after projecting parameters = " + noParamwidth));
            boolean _hasAnyUpperBound = noParamwidth.copy().hasAnyUpperBound(JNIISLDimType.isl_dim_set, 0);
            if (_hasAnyUpperBound) {
              JNIISLSet _lexMax = noParamwidth.copy().lexMax();
              String _plus_3 = ("\t - you are lucky ! there exists a constant array folding factor " + _lexMax);
              InputOutput.<String>println(_plus_3);
              noParamwidth = noParamwidth.copy().lexMax();
              noParamwidth = noParamwidth.alignParams(width.getSpace());
              InputOutput.<String>println(("\t - aligned " + noParamwidth));
              width = noParamwidth;
            } else {
              InputOutput.<String>println(("\t - you\'re screwed : width is unbounded = " + noParamwidth));
            }
          }
          long lwidth = width.toPWMultiAff().getPieces().get(0).getMaff().getAff(0).getConstant();
          final JNIISLMap _extmin = _min.addInputDimensions(map.getOutputDimensionNames());
          final JNIISLMap Id = map.getDomain().flatProduct(map.getRange()).buildIdentityMap();
          final List<JNIISLPWMultiAffPiece> idPieces = Id.copy().toPWMultiAff().getPieces();
          final List<JNIISLPWMultiAffPiece> minPieces = _extmin.copy().toPWMultiAff().getPieces();
          JNIISLMap rmap = null;
          final JNIISLMap lexGt = _extmin.copy().getRange().getSpace().lexLE();
          int _size = minPieces.size();
          boolean _greaterThan = (_size > 1);
          if (_greaterThan) {
            for (final JNIISLPWMultiAffPiece p : minPieces) {
              if ((rmap == null)) {
                rmap = p.getMaff().toMap();
              } else {
                rmap = rmap.union(p.getMaff().toMap());
              }
            }
          }
          JNIISLSet valDom = _extmin.getDomain();
          JNIISLMap simExtMin = rmap.copy();
          simExtMin = simExtMin.applyRange(lexGt.copy());
          simExtMin = simExtMin.intersectDomain(valDom.copy());
          List<JNIISLBasicMap> _basicMaps = simExtMin.copy().getBasicMaps();
          for (final JNIISLBasicMap bmap1 : _basicMaps) {
            List<JNIISLBasicMap> _basicMaps_1 = simExtMin.copy().getBasicMaps();
            for (final JNIISLBasicMap bmap2 : _basicMaps_1) {
              if ((bmap1 != bmap2)) {
                boolean _isSubset = bmap1.isSubset(bmap2);
                if (_isSubset) {
                  String _plus_4 = (bmap1 + "is subset of ");
                  String _plus_5 = (_plus_4 + bmap2);
                  InputOutput.<String>println(_plus_5);
                }
              }
            }
          }
          InputOutput.<JNIISLMap>println(simExtMin);
          final JNIISLAff lhs = idPieces.get(0).getMaff().getAffs().get(2);
          final JNIISLAff rhs = minPieces.get(0).getMaff().getAffs().get(0);
          final JNIISLAff offset = lhs.sub(rhs.copy()).mod(lwidth);
          final QuasiAffineExpression expr = ISLScopConverter.aff(offset, index, params);
          EList<IntExpression> _addresses = layout.getAddresses();
          _addresses.add(expr);
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
}
