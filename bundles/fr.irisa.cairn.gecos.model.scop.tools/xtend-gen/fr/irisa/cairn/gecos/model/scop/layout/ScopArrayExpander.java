package fr.irisa.cairn.gecos.model.scop.layout;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ScopArrayExpander {
  private GecosProject p;
  
  public ScopArrayExpander(final GecosProject p) {
    this.p = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
    }
  }
}
