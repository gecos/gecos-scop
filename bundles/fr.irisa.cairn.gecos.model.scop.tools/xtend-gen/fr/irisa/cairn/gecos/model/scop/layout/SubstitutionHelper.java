package fr.irisa.cairn.gecos.model.scop.layout;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffineTerm;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineOperator;
import org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm;

/**
 * TODO : This should be mode to algebra, but there is an issue with the x!tend code generator
 */
@SuppressWarnings("all")
public class SubstitutionHelper {
  public static IntExpression substitute(final Variable v, final IntExpression target, final IntExpression value) {
    return SubstitutionHelper.dispatchSubstitute(v, target, value);
  }
  
  protected static IntExpression _dispatchSubstitute(final Variable v, final IntExpression target, final IntExpression value) {
    String _simpleName = value.getClass().getSimpleName();
    String _plus = ("Method substitute(" + _simpleName);
    String _plus_1 = (_plus + " in ");
    String _simpleName_1 = target.getClass().getSimpleName();
    String _plus_2 = (_plus_1 + _simpleName_1);
    String _plus_3 = (_plus_2 + "");
    throw new UnsupportedOperationException(_plus_3);
  }
  
  protected static IntExpression _dispatchSubstitute(final Variable v, final AffineExpression target, final AffineExpression value) {
    final AffineExpression res = IntExpressionBuilder.affine();
    EList<AffineTerm> _terms = target.getTerms();
    for (final AffineTerm t : _terms) {
      Variable _variable = t.getVariable();
      boolean _equals = Objects.equal(_variable, v);
      if (_equals) {
        final long coef = t.getCoef();
        IntExpression _copy = value.<IntExpression>copy();
        AffineExpression copy = ((AffineExpression) _copy);
        EList<AffineTerm> _terms_1 = copy.getTerms();
        for (final AffineTerm e : _terms_1) {
          long _coef = e.getCoef();
          int _multiply = (((int) coef) * ((int) _coef));
          e.setCoef(_multiply);
        }
        res.getTerms().addAll(copy.getTerms());
      } else {
        res.getTerms().add(EcoreUtil.<AffineTerm>copy(t));
      }
    }
    return res.simplify();
  }
  
  protected static IntExpression _dispatchSubstitute(final Variable v, final QuasiAffineExpression _t, final AffineExpression value) {
    IntExpression _copy = _t.<IntExpression>copy();
    final QuasiAffineExpression target = ((QuasiAffineExpression) _copy);
    Iterable<SimpleQuasiAffineTerm> _filter = Iterables.<SimpleQuasiAffineTerm>filter(target.getTerms(), SimpleQuasiAffineTerm.class);
    for (final SimpleQuasiAffineTerm t : _filter) {
      IntExpression _dispatchSubstitute = SubstitutionHelper.dispatchSubstitute(v, t.getExpression(), value);
      t.setExpression(((AffineExpression) _dispatchSubstitute));
    }
    Iterable<NestedQuasiAffineTerm> _filter_1 = Iterables.<NestedQuasiAffineTerm>filter(target.getTerms(), NestedQuasiAffineTerm.class);
    for (final NestedQuasiAffineTerm t_1 : _filter_1) {
      IntExpression _dispatchSubstitute_1 = SubstitutionHelper.dispatchSubstitute(v, t_1.getExpression(), value);
      t_1.setExpression(((QuasiAffineExpression) _dispatchSubstitute_1));
    }
    return target;
  }
  
  protected static IntExpression _dispatchSubstitute(final Variable v, final AffineExpression target, final QuasiAffineExpression value) {
    final QuasiAffineExpression res = IntExpressionBuilder.qaffine();
    final AffineExpression resAff = IntExpressionBuilder.affine();
    long scale = (-1);
    boolean found = false;
    EList<AffineTerm> _terms = target.getTerms();
    for (final AffineTerm t : _terms) {
      Variable _variable = t.getVariable();
      boolean _equals = Objects.equal(_variable, v);
      if (_equals) {
        found = true;
        scale = t.getCoef();
      } else {
        resAff.getTerms().add(EcoreUtil.<AffineTerm>copy(t));
      }
    }
    if (found) {
      QuasiAffineExpression _copy = EcoreUtil.<QuasiAffineExpression>copy(value);
      final QuasiAffineExpression copy = ((QuasiAffineExpression) _copy);
      res.getTerms().add(IntExpressionBuilder.mul(copy, scale));
      res.getTerms().add(IntExpressionBuilder.mul(resAff, 1));
      return res.simplify();
    } else {
      return target;
    }
  }
  
  protected static IntExpression _dispatchSubstitute(final Variable v, final QuasiAffineExpression _t, final QuasiAffineExpression value) {
    final QuasiAffineExpression target = IntExpressionBuilder.qaffine();
    Iterable<SimpleQuasiAffineTerm> _filter = Iterables.<SimpleQuasiAffineTerm>filter(_t.getTerms(), SimpleQuasiAffineTerm.class);
    for (final SimpleQuasiAffineTerm t : _filter) {
      {
        final IntExpression r = SubstitutionHelper.dispatchSubstitute(v, t.getExpression(), value);
        target.getTerms().add(IntExpressionBuilder.qterm(QuasiAffineOperator.MUL, ((QuasiAffineExpression) r), 1));
      }
    }
    Iterable<NestedQuasiAffineTerm> _filter_1 = Iterables.<NestedQuasiAffineTerm>filter(_t.getTerms(), NestedQuasiAffineTerm.class);
    for (final NestedQuasiAffineTerm t_1 : _filter_1) {
      IntExpression _dispatchSubstitute = SubstitutionHelper.dispatchSubstitute(v, t_1.getExpression(), value);
      t_1.setExpression(((QuasiAffineExpression) _dispatchSubstitute));
    }
    return target.simplify();
  }
  
  public static IntExpression dispatchSubstitute(final Variable v, final IntExpression target, final IntExpression value) {
    if (target instanceof AffineExpression
         && value instanceof AffineExpression) {
      return _dispatchSubstitute(v, (AffineExpression)target, (AffineExpression)value);
    } else if (target instanceof AffineExpression
         && value instanceof QuasiAffineExpression) {
      return _dispatchSubstitute(v, (AffineExpression)target, (QuasiAffineExpression)value);
    } else if (target instanceof QuasiAffineExpression
         && value instanceof AffineExpression) {
      return _dispatchSubstitute(v, (QuasiAffineExpression)target, (AffineExpression)value);
    } else if (target instanceof QuasiAffineExpression
         && value instanceof QuasiAffineExpression) {
      return _dispatchSubstitute(v, (QuasiAffineExpression)target, (QuasiAffineExpression)value);
    } else if (target != null
         && value != null) {
      return _dispatchSubstitute(v, target, value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(v, target, value).toString());
    }
  }
}
