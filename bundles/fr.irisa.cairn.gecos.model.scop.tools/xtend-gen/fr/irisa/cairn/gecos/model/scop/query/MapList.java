package fr.irisa.cairn.gecos.model.scop.query;

import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class MapList<K extends Object, V extends Object> {
  private HashMap<K, List<V>> map = new HashMap<K, List<V>>();
  
  public MapList() {
  }
  
  public boolean put(final K key, final V value) {
    boolean _xblockexpression = false;
    {
      boolean _containsKey = this.map.containsKey(key);
      boolean _not = (!_containsKey);
      if (_not) {
        ArrayList<V> _arrayList = new ArrayList<V>();
        this.map.put(key, _arrayList);
      }
      List<V> _get = this.map.get(key);
      _xblockexpression = _get.add(value);
    }
    return _xblockexpression;
  }
  
  public List<V> get(final K key) {
    List<V> _xblockexpression = null;
    {
      boolean _containsKey = this.map.containsKey(key);
      boolean _not = (!_containsKey);
      if (_not) {
        ArrayList<V> _arrayList = new ArrayList<V>();
        this.map.put(key, _arrayList);
      }
      _xblockexpression = this.map.get(key);
    }
    return _xblockexpression;
  }
  
  public boolean containsKey(final K key) {
    return this.map.containsKey(key);
  }
  
  public Set<K> keySet() {
    return this.map.keySet();
  }
  
  public List<V> allValues() {
    return IterableExtensions.<V>toList(Iterables.<V>concat(this.map.values()));
  }
}
