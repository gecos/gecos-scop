package fr.irisa.cairn.gecos.model.scop.query;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.query.MapList;
import gecos.core.Symbol;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopAccessQuery {
  public static List<GecosScopBlock> getAllGecosScopBlocks(final EObject obj) {
    return IteratorExtensions.<GecosScopBlock>toList(Iterators.<GecosScopBlock>filter(obj.eAllContents(), GecosScopBlock.class));
  }
  
  public static HashSet<Symbol> getAllUsedSymbols(final ScopNode n) {
    final Function1<ScopAccess, Symbol> _function = (ScopAccess a) -> {
      return a.getSym();
    };
    List<Symbol> _list = IteratorExtensions.<Symbol>toList(IteratorExtensions.<ScopAccess, Symbol>map(Iterators.<ScopAccess>filter(n.eAllContents(), ScopAccess.class), _function));
    return new HashSet<Symbol>(_list);
  }
  
  public static List<Symbol> getAllWrittenSymbols(final ScopNode n) {
    final Function1<ScopStatement, EList<ScopWrite>> _function = (ScopStatement s) -> {
      return s.listAllWriteAccess();
    };
    final Function1<ScopWrite, Symbol> _function_1 = (ScopWrite a) -> {
      return a.getSym();
    };
    return IterableExtensions.<Symbol>toList(IterableExtensions.<ScopWrite, Symbol>map(Iterables.<ScopWrite>concat(ListExtensions.<ScopStatement, EList<ScopWrite>>map(n.listAllStatements(), _function)), _function_1));
  }
  
  public static List<Symbol> getAllReadSymbols(final ScopNode n) {
    final Function1<ScopStatement, EList<ScopRead>> _function = (ScopStatement s) -> {
      return s.listAllReadAccess();
    };
    final Function1<ScopRead, Symbol> _function_1 = (ScopRead a) -> {
      return a.getSym();
    };
    return IterableExtensions.<Symbol>toList(IterableExtensions.<ScopRead, Symbol>map(Iterables.<ScopRead>concat(ListExtensions.<ScopStatement, EList<ScopRead>>map(n.listAllStatements(), _function)), _function_1));
  }
  
  public static MapList<Symbol, ScopRead> getReadSymbolsMap(final ScopNode n) {
    MapList<Symbol, ScopRead> _xblockexpression = null;
    {
      MapList<Symbol, ScopRead> map = new MapList<Symbol, ScopRead>();
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        EList<ScopRead> _listAllReadAccess = s.listAllReadAccess();
        for (final ScopRead read : _listAllReadAccess) {
          map.put(read.getSym(), read);
        }
      }
      _xblockexpression = map;
    }
    return _xblockexpression;
  }
  
  public static MapList<Symbol, ScopAccess> getAccessSymbolsMap(final ScopNode n) {
    MapList<Symbol, ScopAccess> _xblockexpression = null;
    {
      MapList<Symbol, ScopAccess> map = new MapList<Symbol, ScopAccess>();
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        EList<ScopAccess> _listAllAccesses = s.listAllAccesses();
        for (final ScopAccess access : _listAllAccesses) {
          map.put(access.getSym(), access);
        }
      }
      _xblockexpression = map;
    }
    return _xblockexpression;
  }
  
  public static MapList<Symbol, ScopWrite> getWrittenSymbolsMap(final ScopNode n) {
    MapList<Symbol, ScopWrite> _xblockexpression = null;
    {
      MapList<Symbol, ScopWrite> map = new MapList<Symbol, ScopWrite>();
      EList<ScopStatement> _listAllStatements = n.listAllStatements();
      for (final ScopStatement s : _listAllStatements) {
        EList<ScopWrite> _listAllWriteAccess = s.listAllWriteAccess();
        for (final ScopWrite write : _listAllWriteAccess) {
          map.put(write.getSym(), write);
        }
      }
      _xblockexpression = map;
    }
    return _xblockexpression;
  }
  
  public static ArrayList<ScopRead> getAllReadsTo(final Symbol s, final ScopNode root) {
    ArrayList<ScopRead> _xblockexpression = null;
    {
      ArrayList<ScopRead> list = new ArrayList<ScopRead>();
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        EList<ScopRead> _listAllReadAccess = stmt.listAllReadAccess();
        for (final ScopRead access : _listAllReadAccess) {
          Symbol _sym = access.getSym();
          boolean _equals = Objects.equal(_sym, s);
          if (_equals) {
            list.add(access);
          }
        }
      }
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
  
  public static ArrayList<ScopWrite> getAllWritesTo(final Symbol s, final ScopNode root) {
    ArrayList<ScopWrite> _xblockexpression = null;
    {
      ArrayList<ScopWrite> list = new ArrayList<ScopWrite>();
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        EList<ScopWrite> _listAllWriteAccess = stmt.listAllWriteAccess();
        for (final ScopWrite access : _listAllWriteAccess) {
          Symbol _sym = access.getSym();
          boolean _equals = Objects.equal(_sym, s);
          if (_equals) {
            list.add(access);
          }
        }
      }
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
  
  public static ArrayList<ScopAccess> getAllAccessesTo(final Symbol s, final ScopNode root) {
    ArrayList<ScopAccess> _xblockexpression = null;
    {
      ArrayList<ScopAccess> list = new ArrayList<ScopAccess>();
      EList<ScopStatement> _listAllStatements = root.listAllStatements();
      for (final ScopStatement stmt : _listAllStatements) {
        EList<ScopAccess> _listAllAccesses = stmt.listAllAccesses();
        for (final ScopAccess access : _listAllAccesses) {
          Symbol _sym = access.getSym();
          boolean _equals = Objects.equal(_sym, s);
          if (_equals) {
            list.add(access);
          }
        }
      }
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
}
