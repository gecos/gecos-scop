package fr.irisa.cairn.gecos.model.scop.sheduling;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSchedule;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.util.EList;

/**
 * Builds a schedule for the given GScopRoot, using the Pluto algorithm
 * from ISL. The ISL object representing the computed schedule is accessible
 * using .getSchedule().
 * The ScopTransformData returned references dummy PRDG nodes, and the PRDG
 * does not contain any edge.
 * @author amorvan
 */
@SuppressWarnings("all")
public class ScopPlutoTilingScheduler {
  private final static boolean VERBOSE = true;
  
  private GecosProject p;
  
  private Collection<GecosScopBlock> scops;
  
  private void debug(final String string) {
    if (ScopPlutoTilingScheduler.VERBOSE) {
      System.out.println(string);
    }
  }
  
  public ScopPlutoTilingScheduler(final GecosScopBlock root) {
    this.scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(root, GecosScopBlock.class);
  }
  
  public void compute() {
    for (final GecosScopBlock root : this.scops) {
      {
        JNIISLSchedule.JNIISLSchedulingOptions options = new JNIISLSchedule.JNIISLSchedulingOptions();
        options.setAlgorithm(JNIISLSchedule.JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_ISL);
        options.setMax_constant_term(root.listAllStatements().size());
        options.setFuse(1);
        JNIISLUnionMap prdg = ScopISLConverter.getMemoryBasedDepenceGraph(root);
        JNIISLUnionSet domains = ScopISLConverter.getAllStatementDomains(root);
        JNIISLSchedule islSchedule = JNIISLSchedule.computeSchedule(domains.copy(), prdg.copy(), options);
        JNIISLUnionMap islScheduleMap = islSchedule.getMap();
        islScheduleMap = islScheduleMap.intersectDomain(domains.copy());
        ScopTransform _transform = root.getTransform();
        boolean _tripleEquals = (_transform == null);
        if (_tripleEquals) {
          root.setTransform(TransformFactory.eINSTANCE.createScopTransform());
        }
        final ScopTransform transform = root.getTransform();
        EList<ScopStatement> _listAllStatements = root.listAllStatements();
        for (final ScopStatement stmt : _listAllStatements) {
          {
            ScheduleDirective directive = TransformFactory.eINSTANCE.createScheduleDirective();
            List<JNIISLMap> _maps = islScheduleMap.getMaps();
            for (final JNIISLMap map : _maps) {
              String _inputTupleName = map.getInputTupleName();
              String _id = stmt.getId();
              boolean _equals = Objects.equal(_inputTupleName, _id);
              if (_equals) {
                directive.setSchedule(map.toString());
                directive.setStatement(stmt);
              }
            }
            EList<ScopTransformDirective> _commands = transform.getCommands();
            _commands.add(directive);
          }
        }
        root.setTransform(transform);
      }
    }
  }
}
