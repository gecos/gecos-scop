package fr.irisa.cairn.gecos.model.scop.sheduling;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionTransformer;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopDuplicateNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;

@SuppressWarnings("all")
public class ScopSlicer {
  private final static boolean VERBOSE = true;
  
  private static void debug(final String string) {
    if (ScopSlicer.VERBOSE) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("[ScopSlicer] ");
      _builder.append(string);
      InputOutput.<String>println(_builder.toString());
    }
  }
  
  private ScopNode node;
  
  private final List<Integer> tileSizes;
  
  private final List<Integer> unrollSizes;
  
  private Symbol outputSym;
  
  public ScopSlicer(final ScopNode node, final List<Integer> tileSizes, final List<Integer> unrollSizes) {
    this.node = node;
    this.tileSizes = tileSizes;
    this.unrollSizes = unrollSizes;
  }
  
  public void compute() {
    ScopNode _scopRoot = this.node.getScopRoot();
    final GecosScopBlock scopRoot = ((GecosScopBlock) _scopRoot);
    ScopNode _parentScop = this.node.getParentScop();
    boolean _equals = Objects.equal(_parentScop, scopRoot);
    if (_equals) {
      scopRoot.setRoot(ScopUserFactory.scopBlock(this.node));
    }
    ScopUserFactory.setScope(scopRoot.getScope());
    final ArrayList<ScopDimension> tileParameters = this.createTileIterators(scopRoot);
    final Function1<Symbol, Boolean> _function = (Symbol it) -> {
      boolean _isEmpty = ScopAccessAnalyzer.computeFlowOut(it, this.node).isEmpty();
      return Boolean.valueOf((!_isEmpty));
    };
    final Iterable<Symbol> outputSyms = IterableExtensions.<Symbol>filter(this.node.listAllReferencedSymbols(), _function);
    int _size = IterableExtensions.size(outputSyms);
    boolean _equals_1 = (_size == 0);
    if (_equals_1) {
      throw new UnsupportedOperationException("Found no output to slice");
    }
    int _size_1 = IterableExtensions.size(outputSyms);
    boolean _notEquals = (_size_1 != 1);
    if (_notEquals) {
      throw new UnsupportedOperationException(("Cannot slice with more than 1 output. Output symbols found:" + outputSyms));
    }
    this.outputSym = ((Symbol[])Conversions.unwrapArray(outputSyms, Symbol.class))[0];
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Target symbol is ");
    _builder.append(outputSyms);
    ScopSlicer.debug(_builder.toString());
    final Function1<ScopDimension, String> _function_1 = (ScopDimension it) -> {
      return it.getSymName();
    };
    final JNIISLUnionMap prdg = this.tilePrdg(ListExtensions.<ScopDimension, String>map(tileParameters, _function_1));
    final HashMap<String, JNIISLSet> sliceMap = this.slice(prdg, this.outputSym.getName());
    Set<Map.Entry<String, JNIISLSet>> _entrySet = sliceMap.entrySet();
    for (final Map.Entry<String, JNIISLSet> kvp : _entrySet) {
      {
        final ScopStatement stmt = this.node.getStatement(kvp.getKey());
        final JNIISLSet stmtDomain = ScopISLConverter.getDomain(stmt, this.node);
        final JNIISLSet guard = kvp.getValue().copy().clearTupleName().gist(stmtDomain);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Guarding statement ");
        _builder_1.append(stmt);
        _builder_1.append(" with ");
        JNIISLSet _copy = kvp.getValue().copy();
        _builder_1.append(_copy);
        ScopSlicer.debug(_builder_1.toString());
        ISLScopConverter.guard(stmt, guard);
      }
    }
    this.node = DataMotionTransformer.insertDataMotion(this.node);
    final Function1<ScopDimension, String> _function_2 = (ScopDimension it) -> {
      return it.getSymName();
    };
    final JNIISLSet outTiledDomain = this.computeOuterDomain(ListExtensions.<ScopDimension, String>map(tileParameters, _function_2));
    this.node.setAnnotation("SliceToUnroll", null);
    this.tileOuterDomain(tileParameters, outTiledDomain);
    final Function1<ScopNode, Boolean> _function_3 = (ScopNode it) -> {
      return Boolean.valueOf(it.getAnnotations().containsKey("SliceToUnroll"));
    };
    Iterable<ScopNode> _filter = IterableExtensions.<ScopNode>filter(EMFUtils.<ScopNode>eAllContentsInstancesOf(this.node, ScopNode.class), _function_3);
    for (final ScopNode candidate : _filter) {
      {
        candidate.getAnnotations().removeKey("SliceToUnroll");
        ScopNode current = candidate;
        int _min = Math.min(tileParameters.size(), this.unrollSizes.size());
        ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _min, true);
        for (final Integer pos : _doubleDotLessThan) {
          current = ScopDuplicateNode.duplicate(current, tileParameters.get((pos).intValue()).getSymbol(), (this.unrollSizes.get((pos).intValue())).intValue());
        }
      }
    }
    scopRoot.relabelStatements();
  }
  
  private JNIISLUnionMap tilePrdg(final List<String> tileParameters) {
    JNIISLUnionMap _xblockexpression = null;
    {
      final JNIISLUnionMap flowOutMap = ScopAccessAnalyzer.computeFlowOutMap(this.outputSym, this.node);
      final JNIISLSet flowOut = ScopAccessAnalyzer.computeFlowOut(this.outputSym, this.node);
      final List<String> iterators = flowOut.getIndicesNames();
      int _size = iterators.size();
      int _size_1 = this.tileSizes.size();
      boolean _greaterThan = (_size > _size_1);
      if (_greaterThan) {
        throw new UnsupportedOperationException(("Not enough sizes to tile iterators " + iterators));
      }
      int _size_2 = iterators.size();
      final Function1<Integer, String> _function = (Integer i) -> {
        StringConcatenation _builder = new StringConcatenation();
        Integer _get = this.tileSizes.get((i).intValue());
        _builder.append(_get);
        String _get_1 = tileParameters.get((i).intValue());
        _builder.append(_get_1);
        _builder.append("<=");
        String _get_2 = iterators.get((i).intValue());
        _builder.append(_get_2);
        _builder.append("<=");
        Integer _get_3 = this.tileSizes.get((i).intValue());
        _builder.append(_get_3);
        String _get_4 = tileParameters.get((i).intValue());
        _builder.append(_get_4);
        _builder.append("+");
        Integer _get_5 = this.tileSizes.get((i).intValue());
        int _minus = ((_get_5).intValue() - 1);
        _builder.append(_minus);
        return _builder.toString();
      };
      final Iterable<String> inequations = IterableExtensions.<Integer, String>map(new ExclusiveRange(0, _size_2, true), _function);
      StringConcatenation _builder = new StringConcatenation();
      List<String> _parametersNames = flowOut.getParametersNames();
      _builder.append(_parametersNames);
      _builder.append(" -> { ");
      String _name = this.outputSym.getName();
      _builder.append(_name);
      _builder.append(iterators);
      _builder.append(" : ");
      final Function2<String, String, String> _function_1 = (String p1, String p2) -> {
        return ((p1 + " and ") + p2);
      };
      String _reduce = IterableExtensions.<String>reduce(inequations, _function_1);
      _builder.append(_reduce);
      _builder.append("}");
      String str = _builder.toString();
      final JNIISLUnionSet tileDomain = JNIISLUnionSet.buildFromString(JNIISLContext.getCtx(), str);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(" ");
      _builder_1.append("- Tile ");
      _builder_1.append(this.outputSym, " ");
      _builder_1.append(" on domain ");
      _builder_1.append(tileDomain, " ");
      ScopSlicer.debug(_builder_1.toString());
      final JNIISLUnionMap flowOutMapTiled = flowOutMap.intersectRange(tileDomain);
      JNIISLUnionMap prdgTiled = ScopISLConverter.getMemoryBasedDepenceGraph(this.node);
      List<JNIISLMap> _maps = flowOutMapTiled.reverse().getMaps();
      for (final JNIISLMap revFlow : _maps) {
        prdgTiled = prdgTiled.addMap(revFlow);
      }
      _xblockexpression = prdgTiled;
    }
    return _xblockexpression;
  }
  
  private ArrayList<ScopDimension> createTileIterators(final GecosScopBlock scop) {
    ArrayList<ScopDimension> _xblockexpression = null;
    {
      final ArrayList<ScopDimension> tileIterators = new ArrayList<ScopDimension>();
      EList<ScopDimension> _iterators = scop.getIterators();
      for (final ScopDimension i : _iterators) {
        {
          String _symName = i.getSymName();
          String _symName_1 = i.getSymName();
          String _plus = (_symName + _symName_1);
          final ScopDimension tileIterator = ScopUserFactory.dim(_plus);
          scop.getParameters().add(tileIterator);
          tileIterators.add(tileIterator);
        }
      }
      _xblockexpression = tileIterators;
    }
    return _xblockexpression;
  }
  
  private HashMap<String, JNIISLSet> slice(final JNIISLUnionMap prdg, final String target) {
    HashMap<String, JNIISLSet> _xblockexpression = null;
    {
      HashMap<String, JNIISLSet> sliceMap = new HashMap<String, JNIISLSet>();
      JNIISLUnionMap _simpleHull = prdg.copy().simpleHull();
      JNIPtrBoolean _jNIPtrBoolean = new JNIPtrBoolean();
      final JNIISLUnionMap closurePrdg = _simpleHull.transitiveClosure(_jNIPtrBoolean).simpleHull().coalesce();
      final Function1<JNIISLMap, Boolean> _function = (JNIISLMap m) -> {
        String _inputTupleName = m.getInputTupleName();
        return Boolean.valueOf(Objects.equal(_inputTupleName, target));
      };
      final Iterable<JNIISLMap> targetPrdg = IterableExtensions.<JNIISLMap>filter(closurePrdg.getMaps(), _function);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Slice symbol ");
      _builder.append(target);
      _builder.append(" on closure PRDG:");
      ScopSlicer.debug(_builder.toString());
      final Consumer<JNIISLMap> _function_1 = (JNIISLMap it) -> {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(it);
        ScopSlicer.debug(_builder_1.toString());
      };
      targetPrdg.forEach(_function_1);
      for (final JNIISLMap stmtMap : targetPrdg) {
        {
          final JNIISLSet domain = stmtMap.getRange();
          final String tupleName = domain.getTupleName();
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(" ");
          _builder_1.append("- Slicing iteration domain of statement ");
          _builder_1.append(tupleName, " ");
          ScopSlicer.debug(_builder_1.toString());
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append(" ");
          _builder_2.append("- Sliced domain ");
          _builder_2.append(domain, " ");
          ScopSlicer.debug(_builder_2.toString());
          boolean _containsKey = sliceMap.containsKey(tupleName);
          boolean _not = (!_containsKey);
          if (_not) {
            sliceMap.put(tupleName, domain.simpleHull().toSet());
          } else {
            sliceMap.put(tupleName, sliceMap.get(tupleName).union(domain).simpleHull().toSet());
          }
        }
      }
      _xblockexpression = sliceMap;
    }
    return _xblockexpression;
  }
  
  private JNIISLSet computeOuterDomain(final List<String> tileParameters) {
    JNIISLSet _xblockexpression = null;
    {
      final JNIISLSet flowOut = ScopAccessAnalyzer.computeFlowOut(this.outputSym, this.node);
      final int nbIn = flowOut.getNbDims();
      JNIISLSet flowOutTiled = flowOut;
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, nbIn, true);
      for (final Integer i : _doubleDotLessThan) {
        {
          final int paramPos = flowOutTiled.findDimByName(JNIISLDimType.isl_dim_param, tileParameters.get((i).intValue()));
          flowOutTiled = flowOutTiled.moveDims(JNIISLDimType.isl_dim_set, (nbIn + (i).intValue()), JNIISLDimType.isl_dim_param, paramPos, 1);
        }
      }
      JNIISLSet outTiledDomain = flowOutTiled.projectOut(JNIISLDimType.isl_dim_set, 0, nbIn).simplify();
      outTiledDomain = outTiledDomain.setTupleName("T");
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Tile iteration domain ");
      _builder.append(outTiledDomain);
      ScopSlicer.debug(_builder.toString());
      _xblockexpression = outTiledDomain;
    }
    return _xblockexpression;
  }
  
  private ScopNode tileOuterDomain(final List<ScopDimension> tileParameters, final JNIISLSet outTiledDomain) {
    ScopNode _xblockexpression = null;
    {
      ScopNode _scopRoot = this.node.getScopRoot();
      final GecosScopBlock scopRoot = ((GecosScopBlock) _scopRoot);
      JNIISLSet reducedTileDomain = outTiledDomain;
      final ArrayList<ScopDimension> tileIterators = new ArrayList<ScopDimension>();
      for (final ScopDimension p : tileParameters) {
        {
          final int pos = reducedTileDomain.findDimByName(JNIISLDimType.isl_dim_set, p.getSymName());
          if ((pos >= 0)) {
            final JNIISLSet set = reducedTileDomain.copy().projectIn(JNIISLDimType.isl_dim_set, pos);
            boolean _isSingleton = set.isSingleton();
            if (_isSingleton) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("Single tile on dimension ");
              _builder.append(pos);
              _builder.append(", removing iterator");
              ScopSlicer.debug(_builder.toString());
              EList<IntExpression> _eAllContentsInstancesOf = EMFUtils.<IntExpression>eAllContentsInstancesOf(this.node, IntExpression.class);
              for (final IntExpression expr : _eAllContentsInstancesOf) {
                {
                  final IntExpression newExpr = expr.substitute(p, IntExpressionBuilder.constant(0));
                  EMFUtils.substituteByNewObjectInContainer(expr, newExpr);
                }
              }
              scopRoot.getParameters().remove(p);
              p.getSymbol().getContainingScope().removeSymbol(p.getSymbol());
              reducedTileDomain = reducedTileDomain.projectOut(JNIISLDimType.isl_dim_set, pos, 1);
              reducedTileDomain = reducedTileDomain.setTupleName("T");
            } else {
              tileIterators.add(p);
            }
          }
        }
      }
      scopRoot.getIterators().addAll(tileIterators);
      _xblockexpression = this.node = ISLCodegenUtils.encloseWithLoop(this.node, reducedTileDomain, tileIterators);
    }
    return _xblockexpression;
  }
}
