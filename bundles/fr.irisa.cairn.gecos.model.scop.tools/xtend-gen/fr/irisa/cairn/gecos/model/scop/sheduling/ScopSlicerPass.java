package fr.irisa.cairn.gecos.model.scop.sheduling;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.sheduling.ScopSlicer;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.SlicePragma;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.annotations.IAnnotation;
import gecos.gecosproject.GecosProject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

@GSModule("Slice each scop into tiles")
@SuppressWarnings("all")
public class ScopSlicerPass {
  private final GecosProject proj;
  
  public ScopSlicerPass(final GecosProject proj) {
    this.proj = proj;
  }
  
  public void compute() {
    EList<ScopNode> _eAllContentsInstancesOf = EMFUtils.<ScopNode>eAllContentsInstancesOf(this.proj, ScopNode.class);
    for (final ScopNode node : _eAllContentsInstancesOf) {
      {
        EMap<String, IAnnotation> _annotations = node.getAnnotations();
        IAnnotation _get = null;
        if (_annotations!=null) {
          _get=_annotations.get("GCS");
        }
        EList<ScopTransformDirective> _directives = null;
        if (((S2S4HLSPragmaAnnotation) _get)!=null) {
          _directives=((S2S4HLSPragmaAnnotation) _get).getDirectives();
        }
        Iterable<SlicePragma> _filter = null;
        if (_directives!=null) {
          _filter=Iterables.<SlicePragma>filter(_directives, SlicePragma.class);
        }
        final Iterable<SlicePragma> commands = _filter;
        if ((commands != null)) {
          for (final SlicePragma command : commands) {
            EList<Integer> _sizes = command.getSizes();
            EList<Integer> _unrolls = command.getUnrolls();
            new ScopSlicer(node, _sizes, _unrolls).compute();
          }
        }
      }
    }
  }
}
