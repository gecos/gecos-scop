package fr.irisa.cairn.gecos.model.scop.sheduling;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionTransformer;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLBasicSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopSlicingScheduler {
  private Collection<GecosScopBlock> scops;
  
  public ScopSlicingScheduler(final Collection<GecosScopBlock> scops) {
    this.scops = scops;
  }
  
  public ScopSlicingScheduler(final GecosProject p) {
    this.scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(p, GecosScopBlock.class);
  }
  
  private List<String> parNames;
  
  private int TILESIZE = 32;
  
  private int UNROLL = 2;
  
  public JNIISLMap buildOutputTileSlice(final Iterable<JNIISLMap> deps, final String name, final int size) {
    JNIISLMap _xblockexpression = null;
    {
      JNIISLMap res = null;
      for (final JNIISLMap dep : deps) {
        {
          final int nbIn = dep.getNbDim(JNIISLDimType.isl_dim_in);
          final Function1<String, String> _function = (String d) -> {
            return ("P_" + d);
          };
          this.parNames = IterableExtensions.<String>toList(ListExtensions.<String, String>map(dep.getDomainNames(), _function));
          final List<String> domNames = dep.getDomainNames();
          ArrayList<String> str = new ArrayList<String>();
          IntegerRange _upTo = new IntegerRange(0, (nbIn - 1));
          for (final Integer i : _upTo) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(" ");
            _builder.append(size, " ");
            String _get = this.parNames.get((i).intValue());
            _builder.append(_get, " ");
            _builder.append(" <= ");
            String _get_1 = domNames.get((i).intValue());
            _builder.append(_get_1, " ");
            _builder.append("< ");
            _builder.append(size, " ");
            String _get_2 = this.parNames.get((i).intValue());
            _builder.append(_get_2, " ");
            _builder.append("+");
            _builder.append(size, " ");
            str.add(_builder.toString());
          }
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(this.parNames);
          _builder_1.append(" -> { ");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t");
          String _inputTupleName = dep.getInputTupleName();
          _builder_1.append(_inputTupleName, "\t");
          _builder_1.append(domNames, "\t");
          _builder_1.append("  : ");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t");
          {
            boolean _hasElements = false;
            for(final String s : str) {
              if (!_hasElements) {
                _hasElements = true;
              } else {
                _builder_1.appendImmediate("and", "\t");
              }
              _builder_1.append(s, "\t");
            }
          }
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t");
          _builder_1.append("}");
          _builder_1.newLine();
          final String mapstr = _builder_1.toString();
          final JNIISLMap map = JNIISLMap.buildFromString(JNIISLContext.getCtx(), mapstr);
          if ((res == null)) {
            res = map;
          } else {
            res = res.union(map);
          }
        }
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public JNIISLUnionMap insertSlicedOutput(final JNIISLUnionMap prdg, final String name, final int size) {
    final Function1<JNIISLMap, Boolean> _function = (JNIISLMap m) -> {
      String _inputTupleName = m.getInputTupleName();
      return Boolean.valueOf(Objects.equal(_inputTupleName, name));
    };
    Iterable<JNIISLMap> outputs = IterableExtensions.<JNIISLMap>filter(prdg.copy().getMaps(), _function);
    final JNIISLMap outSlice = this.buildOutputTileSlice(outputs, name, size);
    return prdg.copy().addMap(outSlice);
  }
  
  public JNIISLUnionSet slice(final JNIISLUnionMap prdg, final String target) {
    JNIISLUnionSet _xblockexpression = null;
    {
      InputOutput.<String>println(("** slicing for :" + target));
      CharSequence _pp = ScopSlicingScheduler.pp(prdg.copy());
      String _plus = ("Original PRDG :\n" + _pp);
      InputOutput.<String>println(_plus);
      JNIISLUnionMap extPrdg = prdg.copy();
      CharSequence _pp_1 = ScopSlicingScheduler.pp(extPrdg.copy());
      String _plus_1 = ("PRDG with additional node :\n" + _pp_1);
      InputOutput.<String>println(_plus_1);
      extPrdg = extPrdg.simpleHull();
      CharSequence _pp_2 = ScopSlicingScheduler.pp(extPrdg.copy());
      String _plus_2 = ("Simplified PRDG :\n" + _pp_2);
      InputOutput.<String>println(_plus_2);
      JNIPtrBoolean exact = new JNIPtrBoolean();
      final JNIISLUnionMap closurePrdg = extPrdg.transitiveClosure(exact).simpleHull().coalesce();
      CharSequence _pp_3 = ScopSlicingScheduler.pp(closurePrdg.copy());
      String _plus_3 = ("Closure on PRDG with additional node :\n" + _pp_3);
      InputOutput.<String>println(_plus_3);
      HashMap<String, JNIISLSet> sliceMap = new HashMap<String, JNIISLSet>();
      final Function1<JNIISLMap, Boolean> _function = (JNIISLMap m) -> {
        String _inputTupleName = m.getInputTupleName();
        return Boolean.valueOf(Objects.equal(_inputTupleName, target));
      };
      final Iterable<JNIISLMap> filtered = IterableExtensions.<JNIISLMap>filter(closurePrdg.copy().getMaps(), _function);
      InputOutput.<String>println("Filtered PRDG with additional node :\n");
      final Consumer<JNIISLMap> _function_1 = (JNIISLMap m) -> {
        InputOutput.<CharSequence>println(ScopSlicingScheduler.pp(m));
      };
      filtered.forEach(_function_1);
      for (final JNIISLMap fmap : filtered) {
        {
          final JNIISLSet domain = fmap.copy().getRange();
          final String tupleName = domain.copy().getTupleName();
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("   ");
          _builder.append("- Slicing iteration domain of statement ");
          String _plus_4 = (_builder.toString() + tupleName);
          InputOutput.<String>println(_plus_4);
          List<JNIISLMap> _maps = prdg.copy().getMaps();
          for (final JNIISLMap edge : _maps) {
            {
              final String outputTupleName = edge.copy().getOutputTupleName();
              boolean _equals = Objects.equal(outputTupleName, tupleName);
              if (_equals) {
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("        ");
                _builder_1.append("- Matching edge ");
                String _inputTupleName = edge.getInputTupleName();
                _builder_1.append(_inputTupleName, "        ");
                _builder_1.append("-> ");
                String _outputTupleName = edge.getOutputTupleName();
                _builder_1.append(_outputTupleName, "        ");
                _builder_1.append(" ");
                InputOutput.<String>println(_builder_1.toString());
                final JNIISLSet slice = fmap.copy().getRange().intersect(domain.copy()).simpleHull().toSet();
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("           ");
                _builder_2.append("- Matching edge ");
                String _inputTupleName_1 = edge.getInputTupleName();
                _builder_2.append(_inputTupleName_1, "           ");
                _builder_2.append("-> ");
                String _outputTupleName_1 = edge.getOutputTupleName();
                _builder_2.append(_outputTupleName_1, "           ");
                _builder_2.append(" ");
                InputOutput.<String>println(_builder_2.toString());
                boolean _containsKey = sliceMap.containsKey(tupleName);
                boolean _not = (!_containsKey);
                if (_not) {
                  sliceMap.put(tupleName, slice);
                } else {
                  final JNIISLSet previous = sliceMap.get(tupleName).union(slice);
                  sliceMap.put(tupleName, previous);
                }
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("           ");
                _builder_3.append("- Sliced domain");
                JNIISLSet _get = sliceMap.get(tupleName);
                _builder_3.append(_get, "           ");
                InputOutput.<String>println(_builder_3.toString());
              }
            }
          }
        }
      }
      JNIISLUnionSet slicedDomain = JNIISLUnionSet.buildEmpty(closurePrdg.copy().getSpace());
      Collection<JNIISLSet> _values = sliceMap.values();
      for (final JNIISLSet slice : _values) {
        slicedDomain = slicedDomain.addSet(slice);
      }
      CharSequence _pp_4 = ScopSlicingScheduler.pp(slicedDomain);
      String _plus_4 = ("Sliced domain :\n" + _pp_4);
      InputOutput.<String>println(_plus_4);
      _xblockexpression = slicedDomain;
    }
    return _xblockexpression;
  }
  
  public JNIISLSet tileInnerDomain(final JNIISLSet s, final int tileSize) {
    JNIISLSet _xblockexpression = null;
    {
      final int nbIn = s.copy().getNbDims();
      List<String> par = s.copy().getParametersNames();
      final List<String> iter = s.copy().getIndicesNames();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(par);
      _builder.append(" -> { ");
      String _tupleName = s.getTupleName();
      _builder.append(_tupleName);
      _builder.append(iter);
      _builder.append(" : ");
      final Function1<String, String> _function = (String i) -> {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(tileSize);
        _builder_1.append("P_");
        _builder_1.append(i);
        _builder_1.append("<=");
        _builder_1.append(i);
        _builder_1.append("<=");
        _builder_1.append(tileSize);
        _builder_1.append("P_");
        _builder_1.append(i);
        _builder_1.append("+");
        _builder_1.append((tileSize - 1));
        return _builder_1.toString();
      };
      final Function2<String, String, String> _function_1 = (String p1, String p2) -> {
        return ((p1 + " and ") + p2);
      };
      String _reduce = IterableExtensions.<String>reduce(ListExtensions.<String, String>map(iter, _function), _function_1);
      _builder.append(_reduce);
      _builder.append("}");
      final String str = _builder.toString();
      _xblockexpression = JNIISLSet.buildFromString(JNIISLContext.getCtx(), str);
    }
    return _xblockexpression;
  }
  
  public JNIISLSet tileOuterDomain(final JNIISLSet s, final int tileSizem, final int unroll) {
    JNIISLSet _xblockexpression = null;
    {
      final int nbIn = s.copy().getNbDims();
      final int nbParam = s.copy().getNbParams();
      final int startParam = (nbParam - nbIn);
      JNIISLSet newS = s.copy();
      newS = newS.moveDims(JNIISLDimType.isl_dim_set, newS.copy().getNbDims(), JNIISLDimType.isl_dim_param, startParam, nbIn);
      InputOutput.<JNIISLSet>println(newS);
      JNIISLSet res = newS.projectOut(JNIISLDimType.isl_dim_set, 0, nbIn);
      res = res.setTupleName("T");
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public JNIISLSet computeOutputFlowTile(final Symbol s, final GecosScopBlock root, final List<String> names) {
    JNIISLSet _xblockexpression = null;
    {
      JNIISLSet flowOut = ScopAccessAnalyzer.computeFlowOut(s, root);
      String _name = s.getName();
      String _plus = ("flowOut for " + _name);
      String _plus_1 = (_plus + " ");
      JNIISLSet _copy = flowOut.copy();
      String _plus_2 = (_plus_1 + _copy);
      InputOutput.<String>println(_plus_2);
      boolean _isEmpty = flowOut.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        String _name_1 = s.getName();
        final String shadow_name = ("O_" + _name_1);
        flowOut = flowOut.setTupleName(shadow_name);
        String _tupleName = flowOut.getTupleName();
        names.add(_tupleName);
        int _nbDims = flowOut.copy().getNbDims();
        final int nbDims = (_nbDims - 1);
        IntegerRange _upTo = new IntegerRange(0, nbDims);
        for (final Integer dimension : _upTo) {
          {
            final String name = root.getIterators().get((dimension).intValue()).getName();
            flowOut = flowOut.copy().setDimName(JNIISLDimType.isl_dim_set, (dimension).intValue(), name);
          }
        }
      }
      _xblockexpression = flowOut;
    }
    return _xblockexpression;
  }
  
  private Map<String, Symbol> map = new HashMap<String, Symbol>();
  
  public void makeSymbolsUnique(final ScopNode node) {
    final List<Symbol> syms = EMFUtils.<Symbol>eAllContentsInstancesOf(node, Symbol.class);
    for (final Symbol s : syms) {
      {
        while (this.map.containsKey(s.getName())) {
          String _name = s.getName();
          String _plus = ("_" + _name);
          s.setName(_plus);
        }
        this.map.put(s.getName(), s);
      }
    }
  }
  
  public void compute() {
    for (final GecosScopBlock root : this.scops) {
      {
        final EList<Symbol> liveOut = root.getLiveOutSymbols();
        List<String> sliceOutputArrayNames = IterableExtensions.<String>toList(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList()));
        final ScopBlock block = ScopUserFactory.scopBlock();
        EList<ScopNode> _children = block.getChildren();
        ScopNode _root = root.getRoot();
        _children.add(_root);
        root.setRoot(block);
        ScopUserFactory.setScope(root.getScope());
        final Function1<ScopDimension, ScopDimension> _function = (ScopDimension i) -> {
          String _name = i.getName();
          String _plus = ("P_" + _name);
          return ScopUserFactory.dim(_plus);
        };
        final ArrayList<ScopDimension> sliceIterators = Lists.<ScopDimension>newArrayList(ListExtensions.<ScopDimension, ScopDimension>map(root.getIterators(), _function));
        EList<ScopDimension> _parameters = root.getParameters();
        Iterables.<ScopDimension>addAll(_parameters, sliceIterators);
        final ArrayList<ScopDimension> params = Lists.<ScopDimension>newArrayList(root.getParameters());
        final ArrayList<ScopDimension> iterators = Lists.<ScopDimension>newArrayList(root.getIterators());
        final Function1<Symbol, Boolean> _function_1 = (Symbol s) -> {
          return Boolean.valueOf(s.getType().isArray());
        };
        Iterable<Symbol> _filter = IterableExtensions.<Symbol>filter(liveOut, _function_1);
        for (final Symbol s : _filter) {
          {
            InputOutput.<String>println(("Slicing array symbol \"+s.name" + "\"\n"));
            final JNIISLSet flowOut = this.computeOutputFlowTile(s, root, sliceOutputArrayNames);
            boolean _isEmpty = flowOut.copy().isEmpty();
            boolean _not = (!_isEmpty);
            if (_not) {
              final JNIISLSet tiledOutput = this.tileInnerDomain(flowOut.copy(), this.TILESIZE);
              Symbol shadow = s.<Symbol>copy();
              shadow.setName(tiledOutput.getTupleName());
              ISLCodegenUtils.dataMover(tiledOutput, block, s, shadow);
            }
          }
        }
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("********************************");
        _builder.newLine();
        _builder.append("2) Building PRDG ");
        _builder.newLine();
        _builder.append("********************************");
        _builder.newLine();
        InputOutput.<String>println(_builder.toString());
        final JNIISLUnionMap prdg = ScopISLConverter.getValueBasedDepenceGraph(root);
        for (final String n : sliceOutputArrayNames) {
          {
            final JNIISLUnionSet slicedSet = this.slice(prdg, n);
            List<JNIISLSet> _sets = slicedSet.getSets();
            for (final JNIISLSet set : _sets) {
              {
                final ScopStatement stmt = root.getStatement(set.getTupleName());
                ISLScopConverter.guard(stmt, set);
              }
            }
          }
        }
        final Function1<ScopStatement, Boolean> _function_2 = (ScopStatement s_1) -> {
          return Boolean.valueOf(s_1.getId().startsWith("O_"));
        };
        final Iterable<ScopStatement> artifactStmts = IterableExtensions.<ScopStatement>filter(root.listAllStatements(), _function_2);
        for (final ScopStatement s_1 : artifactStmts) {
          s_1.getParentScop().remove(s_1);
        }
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("***************************************************");
        _builder_1.newLine();
        _builder_1.append("4) Insert Data motion code and contract local arrays");
        _builder_1.newLine();
        _builder_1.append("****************************************************");
        _builder_1.newLine();
        InputOutput.<String>println(_builder_1.toString());
        DataMotionTransformer.insertDataMotion(root.getRoot(), root.getRoot().listAllReferencedSymbols());
        final JNIISLSet flowOut = this.computeOutputFlowTile(root.getLiveOutSymbols().get(0), root, sliceOutputArrayNames);
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("********************************");
        _builder_2.newLine();
        _builder_2.append("5) Output tile ");
        _builder_2.newLine();
        _builder_2.append("********************************");
        _builder_2.newLine();
        InputOutput.<String>println(_builder_2.toString());
        final JNIISLSet outTiledDomain = this.tileOuterDomain(flowOut, this.TILESIZE, this.UNROLL);
        InputOutput.<JNIISLSet>println(outTiledDomain);
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append("********************************");
        _builder_3.newLine();
        _builder_3.append("7) Generate outer loop with ");
        _builder_3.append(sliceIterators);
        _builder_3.append(" as index");
        _builder_3.newLineIfNotEmpty();
        _builder_3.append("********************************");
        _builder_3.newLine();
        InputOutput.<String>println(_builder_3.toString());
        ISLCodegenUtils.encloseWithLoop(root.getRoot(), outTiledDomain, sliceIterators);
        InputOutput.<GecosScopBlock>println(root);
      }
    }
  }
  
  public static CharSequence pp(final JNIISLMap m) {
    StringConcatenation _builder = new StringConcatenation();
    List<String> _parametersNames = m.getParametersNames();
    _builder.append(_parametersNames);
    _builder.append(" -> {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    {
      List<JNIISLBasicMap> _basicMaps = m.copy().getBasicMaps();
      for(final JNIISLBasicMap bmap : _basicMaps) {
        _builder.append(bmap, "\t\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence pp(final JNIISLUnionMap m) {
    StringConcatenation _builder = new StringConcatenation();
    List<String> _nameList = m.copy().getSpace().getNameList(JNIISLDimType.isl_dim_param);
    _builder.append(_nameList);
    _builder.append(" -> {");
    _builder.newLineIfNotEmpty();
    {
      List<JNIISLMap> _maps = m.copy().getMaps();
      for(final JNIISLMap map : _maps) {
        {
          List<JNIISLBasicMap> _basicMaps = map.copy().getBasicMaps();
          for(final JNIISLBasicMap bmap : _basicMaps) {
            _builder.append("\t");
            _builder.append(bmap);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence pp(final JNIISLUnionSet m) {
    StringConcatenation _builder = new StringConcatenation();
    List<String> _nameList = m.copy().getSpace().getNameList(JNIISLDimType.isl_dim_param);
    _builder.append(_nameList);
    _builder.append(" -> {");
    _builder.newLineIfNotEmpty();
    {
      List<JNIISLSet> _sets = m.copy().getSets();
      for(final JNIISLSet map : _sets) {
        {
          List<JNIISLBasicSet> _basicSets = map.copy().getBasicSets();
          for(final JNIISLBasicSet bmap : _basicSets) {
            _builder.append("\t");
            _builder.append(bmap);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
}
