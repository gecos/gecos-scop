package fr.irisa.cairn.gecos.model.scop.sheduling;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopStatementClustering {
  private final static boolean VERBOSE = true;
  
  private GecosProject p;
  
  public ScopStatementClustering(final GecosProject p) {
    this.p = p;
  }
  
  private void debug(final String string) {
    if (ScopStatementClustering.VERBOSE) {
      InputOutput.<String>println(("[ScopStatementClustering] " + string));
    }
  }
  
  public Object overlap(final ScopWrite w0, final ScopWrite w1) {
    return null;
  }
  
  public void compute() {
    EList<GecosScopBlock> _eAllContentsInstancesOf = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock scop : _eAllContentsInstancesOf) {
      {
        JNIISLUnionMap _valueBasedDepenceGraph = ScopISLConverter.getValueBasedDepenceGraph(scop);
        JNIPtrBoolean _jNIPtrBoolean = new JNIPtrBoolean();
        final JNIISLUnionMap prdg = _valueBasedDepenceGraph.transitiveClosure(_jNIPtrBoolean);
        final EList<Symbol> syms = scop.listAllReferencedSymbols();
        for (final Symbol sym : syms) {
          {
            final Function1<ScopStatement, Boolean> _function = (ScopStatement s) -> {
              final Function1<ScopWrite, Symbol> _function_1 = (ScopWrite w) -> {
                return w.getSym();
              };
              return Boolean.valueOf(ListExtensions.<ScopWrite, Symbol>map(s.listAllWriteAccess(), _function_1).contains(sym));
            };
            final Iterable<ScopStatement> writeStmts = IterableExtensions.<ScopStatement>filter(scop.listAllStatements(), _function);
            for (final ScopStatement wrStmt : writeStmts) {
              {
                final ScopStatement seed = wrStmt;
                final Function1<ScopStatement, Boolean> _function_1 = (ScopStatement s) -> {
                  return Boolean.valueOf((!Objects.equal(s, wrStmt)));
                };
                final List<ScopStatement> remainder = IterableExtensions.<ScopStatement>toList(IterableExtensions.<ScopStatement>filter(writeStmts, _function_1));
              }
            }
          }
        }
      }
    }
  }
}
