package fr.irisa.cairn.gecos.model.scop.transforms;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.query.MapList;
import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import java.util.List;
import java.util.Set;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class IndentifyBijectiveDefUse {
  private GecosProject proj;
  
  public IndentifyBijectiveDefUse(final GecosProject p) {
    this.proj = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      {
        final MapList<Symbol, ScopRead> readmap = ScopAccessQuery.getReadSymbolsMap(scop.getRoot());
        final MapList<Symbol, ScopWrite> writemap = ScopAccessQuery.getWrittenSymbolsMap(scop.getRoot());
        Set<Symbol> _keySet = writemap.keySet();
        for (final Symbol s : _keySet) {
          List<ScopWrite> _get = writemap.get(s);
          for (final ScopWrite wr : _get) {
          }
        }
      }
    }
  }
}
