package fr.irisa.cairn.gecos.model.scop.transforms;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopCopyIdentifier;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class PolyhedralConstantPropagation {
  private GecosProject proj;
  
  private static boolean VERBOSE = true;
  
  public PolyhedralConstantPropagation(final GecosProject p) {
    this.proj = p;
  }
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (PolyhedralConstantPropagation.VERBOSE) {
      _xifexpression = InputOutput.<String>println(string);
    }
    return _xifexpression;
  }
  
  public ScopWrite extractDefSource(final ScopStatement defStmt, final Symbol target) {
    ScopWrite _xblockexpression = null;
    {
      final Function1<ScopWrite, Boolean> _function = (ScopWrite wr) -> {
        Symbol _sym = wr.getSym();
        return Boolean.valueOf(Objects.equal(_sym, target));
      };
      final Iterable<ScopWrite> defSources = IterableExtensions.<ScopWrite>filter(defStmt.listAllWriteAccess(), _function);
      int _size = IterableExtensions.size(defSources);
      boolean _greaterThan = (_size > 1);
      if (_greaterThan) {
        throw new RuntimeException(("Unsupported (multi write) SCOP copy " + defStmt));
      }
      _xblockexpression = ((ScopWrite[])Conversions.unwrapArray(defSources, ScopWrite.class))[0];
    }
    return _xblockexpression;
  }
  
  public JNIISLMap extractMapping(final ScopStatement defStmt, final ScopWrite defSource) {
    JNIISLMap _xblockexpression = null;
    {
      final ScopRead useAccess = defStmt.listAllReadAccess().get(0);
      final JNIISLMap defMap = ScopISLConverter.getAccessMap(defSource);
      final JNIISLMap useMap = ScopISLConverter.getAccessMap(useAccess);
      final JNIISLMap copyMapping = useMap.copy().reverse().applyRange(defMap.copy()).reverse();
      _xblockexpression = copyMapping;
    }
    return _xblockexpression;
  }
  
  public String replaceUse(final ScopRead use, final Symbol defSym, final JNIISLMap remapping) {
    String _xblockexpression = null;
    {
      PolyhedralConstantPropagation.debug(("\t\t- ScopRead " + use));
      final JNIISLMap readMap = ScopISLConverter.getAccessMap(use);
      PolyhedralConstantPropagation.debug(("\t\t\t- read map" + readMap));
      final JNIISLMap src = readMap.copy().applyRange(remapping.copy());
      final Map<JNIISLSet, JNIISLMultiAff> entries = src.copy().getClosedFormRelation();
      PolyhedralConstantPropagation.debug(("\t\t\t- true source " + src));
      String _xifexpression = null;
      int _size = entries.size();
      boolean _equals = (_size == 1);
      if (_equals) {
        String _xblockexpression_1 = null;
        {
          use.setSym(defSym);
          final JNIISLMultiAff funcs = ((JNIISLMultiAff[])Conversions.unwrapArray(entries.values(), JNIISLMultiAff.class))[0];
          final List<IntExpression> exprs = ISLScopConverter.exprList(funcs, use.listAllEnclosingIterators(), use.listAllParameters());
          use.getIndexExpressions().clear();
          EList<IntExpression> _indexExpressions = use.getIndexExpressions();
          Iterables.<IntExpression>addAll(_indexExpressions, exprs);
          _xblockexpression_1 = PolyhedralConstantPropagation.debug(("\t\t\t- Changed into :" + use));
        }
        _xifexpression = _xblockexpression_1;
      } else {
        int _size_1 = entries.size();
        boolean _greaterThan = (_size_1 > 1);
        if (_greaterThan) {
          throw new RuntimeException("Cannot substitute access map (>1 basic map)");
        }
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      {
        HashMap<ScopStatement, Instruction> map = new HashMap<ScopStatement, Instruction>();
        EList<ScopStatement> _listAllStatements = scop.listAllStatements();
        for (final ScopStatement stmt : _listAllStatements) {
          {
            final Instruction cst = ScopCopyIdentifier.isConstantValue(stmt);
            if ((cst != null)) {
              map.put(stmt, cst);
            }
          }
        }
        final JNIISLUnionMap prdg = ScopISLConverter.getValueBasedDepenceGraphWithExplicitRead(scop);
        List<JNIISLMap> _maps = prdg.getMaps();
        for (final JNIISLMap islmap : _maps) {
          {
            InputOutput.<JNIISLMap>print(islmap);
            final ScopStatement defStmt = scop.getStatement(islmap.getInputTupleName());
            final ScopStatement useStmt = scop.getStatement(islmap.getOutputTupleName());
            boolean _containsKey = map.containsKey(defStmt);
            if (_containsKey) {
            }
          }
        }
      }
    }
  }
}
