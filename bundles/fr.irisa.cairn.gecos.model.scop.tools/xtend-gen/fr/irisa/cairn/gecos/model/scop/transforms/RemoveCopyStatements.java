package fr.irisa.cairn.gecos.model.scop.transforms;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopCopyIdentifier;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMultiAff;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.IntExpression;

@GSModule(value = "Remove Copy Statements")
@SuppressWarnings("all")
public class RemoveCopyStatements {
  private GecosProject proj;
  
  private static boolean VERBOSE = true;
  
  private static boolean DEBUG = true;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (RemoveCopyStatements.DEBUG) {
      _xifexpression = InputOutput.<String>println(("[ScopCopyRemover] " + string));
    }
    return _xifexpression;
  }
  
  public static String log(final String string) {
    String _xifexpression = null;
    if (RemoveCopyStatements.VERBOSE) {
      _xifexpression = InputOutput.<String>println(("[ScopCopyRemover] " + string));
    }
    return _xifexpression;
  }
  
  public RemoveCopyStatements(final GecosProject p) {
    this.proj = p;
  }
  
  public ScopWrite extractDefSource(final ScopStatement defStmt, final Symbol target) {
    ScopWrite _xblockexpression = null;
    {
      final Function1<ScopWrite, Boolean> _function = (ScopWrite wr) -> {
        Symbol _sym = wr.getSym();
        return Boolean.valueOf(Objects.equal(_sym, target));
      };
      final Iterable<ScopWrite> defSources = IterableExtensions.<ScopWrite>filter(defStmt.listAllWriteAccess(), _function);
      int _size = IterableExtensions.size(defSources);
      boolean _greaterThan = (_size > 1);
      if (_greaterThan) {
        throw new RuntimeException(("Unsupported (multi write) SCOP copy " + defStmt));
      }
      _xblockexpression = ((ScopWrite[])Conversions.unwrapArray(defSources, ScopWrite.class))[0];
    }
    return _xblockexpression;
  }
  
  public JNIISLMap extractMapping(final ScopStatement defStmt, final ScopWrite defSource) {
    JNIISLMap _xblockexpression = null;
    {
      final ScopRead useAccess = defStmt.listAllReadAccess().get(0);
      final JNIISLMap defMap = ScopISLConverter.getAccessMap(defSource);
      final JNIISLMap useMap = ScopISLConverter.getAccessMap(useAccess);
      final JNIISLMap copyMapping = useMap.copy().reverse().applyRange(defMap.copy()).reverse();
      _xblockexpression = copyMapping;
    }
    return _xblockexpression;
  }
  
  public String replaceUse(final ScopRead use, final Symbol defSym, final JNIISLMap remapping) {
    String _xblockexpression = null;
    {
      RemoveCopyStatements.debug(("\t\t- ScopRead " + use));
      final String old = use.getRoot().toString();
      final JNIISLMap readMap = ScopISLConverter.getAccessMap(use);
      RemoveCopyStatements.debug(("\t\t\t- read map" + readMap));
      final JNIISLMap src = readMap.copy().applyRange(remapping.copy());
      final Map<JNIISLSet, JNIISLMultiAff> entries = src.copy().getClosedFormRelation();
      RemoveCopyStatements.debug(("\t\t\t- true source " + src));
      String _xifexpression = null;
      int _size = entries.size();
      boolean _equals = (_size == 1);
      if (_equals) {
        String _xblockexpression_1 = null;
        {
          use.setSym(defSym);
          final JNIISLMultiAff funcs = ((JNIISLMultiAff[])Conversions.unwrapArray(entries.values(), JNIISLMultiAff.class))[0];
          final List<IntExpression> exprs = ISLScopConverter.exprList(funcs, use.listAllEnclosingIterators(), use.listAllParameters());
          use.getIndexExpressions().clear();
          EList<IntExpression> _indexExpressions = use.getIndexExpressions();
          Iterables.<IntExpression>addAll(_indexExpressions, exprs);
          Instruction _root = use.getRoot();
          String _plus = ((old + " changed into :") + _root);
          _xblockexpression_1 = RemoveCopyStatements.log(_plus);
        }
        _xifexpression = _xblockexpression_1;
      } else {
        int _size_1 = entries.size();
        boolean _greaterThan = (_size_1 > 1);
        if (_greaterThan) {
          throw new RuntimeException("Cannot substitute access map (>1 basic map)");
        }
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      {
        int id = 0;
        EList<ScopStatement> _listAllStatements = scop.listAllStatements();
        for (final ScopStatement s : _listAllStatements) {
          int _plusPlus = id++;
          String _plus = ("S" + Integer.valueOf(_plusPlus));
          s.setId(_plus);
        }
        final EList<ScopStatement> stmts = scop.listAllStatements();
        final Function1<ScopStatement, Boolean> _function = (ScopStatement s_1) -> {
          return Boolean.valueOf(ScopCopyIdentifier.isCopy(s_1));
        };
        final List<ScopStatement> copyStmts = IterableExtensions.<ScopStatement>toList(IterableExtensions.<ScopStatement>filter(stmts, _function));
        for (final ScopStatement stmt : copyStmts) {
          {
            RemoveCopyStatements.log(("Analyzing " + stmt));
            final Symbol readSym = stmt.listAllReadSymbols().get(0);
            final Symbol copySym = stmt.listAllWriteSymbols().get(0);
            final Function1<ScopWrite, Boolean> _function_1 = (ScopWrite wr) -> {
              Symbol _sym = wr.getSym();
              return Boolean.valueOf(Objects.equal(_sym, readSym));
            };
            final Iterable<ScopWrite> writes = IterableExtensions.<ScopWrite>filter(scop.listAllWriteAccess(), _function_1);
            int _size = IterableExtensions.size(writes);
            boolean _greaterThan = (_size > 1);
            if (_greaterThan) {
              RemoveCopyStatements.debug(("Cannot remove copy because of possible WAW on " + readSym));
              final Function1<ScopWrite, Instruction> _function_2 = (ScopWrite s_1) -> {
                return s_1.getRoot();
              };
              final Consumer<Instruction> _function_3 = (Instruction w) -> {
                RemoveCopyStatements.debug(("\t" + w));
              };
              IterableExtensions.<Instruction>toList(IterableExtensions.<ScopWrite, Instruction>map(writes, _function_2)).forEach(_function_3);
              this.checkNoOverlapping(writes);
            } else {
              final ScopWrite defSource = this.extractDefSource(stmt, copySym);
              final JNIISLMap copyMapping = this.extractMapping(stmt, defSource);
              final JNIISLUnionMap prdgSrcSym = ScopISLConverter.getWARDepenceGraph(readSym, scop);
              int _size_1 = prdgSrcSym.copy().getMaps().size();
              boolean _greaterThan_1 = (_size_1 > 0);
              if (_greaterThan_1) {
                RemoveCopyStatements.debug((" Cannot eliminate copy due to WAR dependency on " + copySym));
                List<JNIISLMap> _maps = prdgSrcSym.copy().getMaps();
                for (final JNIISLMap edge : _maps) {
                  {
                    boolean _domainIsWrapping = edge.copy().domainIsWrapping();
                    if (_domainIsWrapping) {
                      final JNIISLMap curry = edge.copy().curry();
                      JNIISLMap _copy = edge.copy();
                      String _plus_1 = ("\t- WAR edge " + _copy);
                      RemoveCopyStatements.debug(_plus_1);
                      JNIISLMap _copy_1 = curry.copy();
                      String _plus_2 = ("\t- PRDG edge " + _copy_1);
                      RemoveCopyStatements.debug(_plus_2);
                      JNIISLMap _unwrap = curry.copy().getRange().unwrap();
                      String _plus_3 = ("\t- PRDG edge " + _unwrap);
                      RemoveCopyStatements.debug(_plus_3);
                    }
                    boolean _domainIsWrapping_1 = edge.copy().domainIsWrapping();
                    String _plus_4 = ("\t\t- PRDG edge " + Boolean.valueOf(_domainIsWrapping_1));
                    RemoveCopyStatements.debug(_plus_4);
                  }
                }
              } else {
                RemoveCopyStatements.debug(((("- No WAR dependency on" + copySym) + " for statement ") + stmt));
                final JNIISLUnionMap prdgCopySym = ScopISLConverter.getValueBasedDepenceGraph(copySym, scop);
                boolean remove = true;
                List<JNIISLMap> _maps_1 = prdgCopySym.copy().getMaps();
                for (final JNIISLMap writeMap : _maps_1) {
                  {
                    final ScopStatement useStmt = scop.getStatement(writeMap.getInputTupleName());
                    RemoveCopyStatements.debug(((("\t- " + copySym) + " Used in : ") + useStmt));
                    final Function1<ScopRead, Boolean> _function_4 = (ScopRead rd) -> {
                      Symbol _sym = rd.getSym();
                      return Boolean.valueOf(Objects.equal(_sym, copySym));
                    };
                    final Iterable<ScopRead> uses = IterableExtensions.<ScopRead>filter(useStmt.listAllReadAccess(), _function_4);
                    int _size_2 = IterableExtensions.size(uses);
                    boolean _greaterThan_2 = (_size_2 > 1);
                    if (_greaterThan_2) {
                      RemoveCopyStatements.log(((("- Multiple access to the same array " + uses) + " in ") + useStmt));
                      remove = false;
                    } else {
                      int _size_3 = IterableExtensions.size(uses);
                      boolean _equals = (_size_3 == 1);
                      if (_equals) {
                        final ScopRead use = ((ScopRead[])Conversions.unwrapArray(uses, ScopRead.class))[0];
                        this.replaceUse(use, readSym, copyMapping);
                        RemoveCopyStatements.log(("Removed copy " + stmt));
                      }
                    }
                  }
                }
                if (remove) {
                  stmt.getParentScop().remove(stmt);
                }
              }
            }
          }
        }
        id = 0;
        scop.relabelStatements();
      }
    }
  }
  
  public Boolean checkNoOverlapping(final Iterable<ScopWrite> writes) {
    final Function1<ScopWrite, JNIISLSet> _function = (ScopWrite w) -> {
      return ScopISLConverter.getAccessMap(w).getRange();
    };
    final Iterable<JNIISLSet> footPrints = IterableExtensions.<ScopWrite, JNIISLSet>map(writes, _function);
    for (final JNIISLSet x : footPrints) {
      for (final JNIISLSet y : footPrints) {
        boolean _notEquals = (!Objects.equal(x, y));
        if (_notEquals) {
          RemoveCopyStatements.debug(((("Comparing " + x) + " and ") + y));
          boolean _isDisjoint = x.copy().isDisjoint(y.copy());
          boolean _not = (!_isDisjoint);
          if (_not) {
            return Boolean.valueOf(true);
          }
        }
      }
    }
    return null;
  }
}
