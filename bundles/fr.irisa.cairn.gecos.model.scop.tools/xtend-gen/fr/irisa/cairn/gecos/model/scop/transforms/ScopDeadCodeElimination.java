package fr.irisa.cairn.gecos.model.scop.transforms;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopDeadCodeElimination {
  private static boolean VERBOSE = true;
  
  private static boolean DEBUG = false;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (ScopDeadCodeElimination.DEBUG) {
      _xifexpression = InputOutput.<String>println(("[ScopDeadcodeElimination] " + string));
    }
    return _xifexpression;
  }
  
  public static String log(final String string) {
    String _xifexpression = null;
    if (ScopDeadCodeElimination.VERBOSE) {
      _xifexpression = InputOutput.<String>println(("[ScopDeadcodeElimination] " + string));
    }
    return _xifexpression;
  }
  
  private GecosProject p;
  
  public ScopDeadCodeElimination(final GecosProject p) {
    this.p = p;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.p, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      this.deadCodeElimination(scop);
    }
  }
  
  public void deadCodeElimination(final GecosScopBlock root) {
    ArrayList<ScopStatement> deadStatements = new ArrayList<ScopStatement>();
    JNIISLUnionMap vprdg = ScopISLConverter.getValueBasedDepenceGraph(root);
    JNIISLUnionMap _reverse = vprdg.reverse();
    JNIPtrBoolean _jNIPtrBoolean = new JNIPtrBoolean();
    JNIISLUnionMap vclosure = _reverse.transitiveClosure(_jNIPtrBoolean);
    final EList<Symbol> liveOutSymbols = root.getLiveOutSymbols();
    final Function1<ScopStatement, String> _function = (ScopStatement a) -> {
      return a.getId();
    };
    List<String> _map = ListExtensions.<ScopStatement, String>map(root.listAllStatements(), _function);
    for (final String stmtName : _map) {
      {
        final ScopStatement wrStmt = root.getStatement(stmtName);
        int _size = wrStmt.listAllWriteAccess().size();
        boolean _greaterThan = (_size > 1);
        if (_greaterThan) {
          throw new UnsupportedOperationException("Multi-Write statements are not supported");
        }
        final ScopWrite wr = wrStmt.listAllWriteAccess().get(0);
        final JNIISLSet wrDomain = ScopISLConverter.getAccessMap(wr).getDomain();
        JNIISLSet useDomain = null;
        ScopDeadCodeElimination.debug(("Analyzing statement " + wrStmt));
        final Function1<JNIISLMap, Boolean> _function_1 = (JNIISLMap e) -> {
          String _inputTupleName = e.getInputTupleName();
          return Boolean.valueOf(Objects.equal(_inputTupleName, stmtName));
        };
        Iterable<JNIISLMap> _filter = IterableExtensions.<JNIISLMap>filter(vclosure.copy().getMaps(), _function_1);
        for (final JNIISLMap edge : _filter) {
          {
            final ScopStatement rdStmt = root.getStatement(edge.getOutputTupleName());
            boolean hasSink = false;
            EList<ScopWrite> _listAllWriteAccess = rdStmt.listAllWriteAccess();
            for (final ScopWrite sink : _listAllWriteAccess) {
              boolean _contains = liveOutSymbols.contains(sink.getSym());
              if (_contains) {
                hasSink = true;
              }
            }
            if (hasSink) {
              final JNIISLSet dom = edge.getDomain();
              if ((useDomain == null)) {
                useDomain = dom;
              } else {
                useDomain = useDomain.union(dom);
              }
              JNIISLSet _copy = useDomain.copy();
              String _plus = ("-> current live  domain :" + _copy);
              ScopDeadCodeElimination.debug(_plus);
            }
          }
        }
        if ((useDomain == null)) {
          boolean _contains = root.getLiveOutSymbols().contains(wr.getSym());
          boolean _not = (!_contains);
          if (_not) {
            ScopDeadCodeElimination.log((("Statement " + wrStmt) + " is dead code "));
            deadStatements.add(wrStmt);
          }
        } else {
          if (((useDomain != null) && useDomain.copy().isStrictSubset(wrDomain))) {
            final JNIISLSet deadDom = wrDomain.copy().subtract(useDomain.copy()).gist(wrDomain.copy());
            ScopDeadCodeElimination.log((((("Statement " + wrStmt) + " result is not used when ") + deadDom) + ""));
            ISLScopConverter.guard(wrStmt, useDomain.copy().gist(wrDomain.copy()));
          }
        }
      }
    }
    for (final ScopStatement s : deadStatements) {
      s.getParentScop().remove(s);
    }
    this.removeEmptyBlocks(root);
    root.relabelStatements();
  }
  
  protected Object _removeEmptyBlocks(final ScopNode node) {
    throw new UnsupportedOperationException("Not yet implemented");
  }
  
  protected Object _removeEmptyBlocks(final ScopStatement node) {
    return null;
  }
  
  protected Object _removeEmptyBlocks(final GecosScopBlock node) {
    return this.removeEmptyBlocks(node.getRoot());
  }
  
  protected Object _removeEmptyBlocks(final ScopGuard node) {
    String _xblockexpression = null;
    {
      ScopNode _thenNode = node.getThenNode();
      boolean _tripleNotEquals = (_thenNode != null);
      if (_tripleNotEquals) {
        this.removeEmptyBlocks(node.getThenNode());
      }
      ScopNode _elseNode = node.getElseNode();
      boolean _tripleNotEquals_1 = (_elseNode != null);
      if (_tripleNotEquals_1) {
        this.removeEmptyBlocks(node.getElseNode());
      }
      String _xifexpression = null;
      if (((node.getThenNode() == null) && (node.getElseNode() == null))) {
        String _xblockexpression_1 = null;
        {
          node.getParentScop().remove(node);
          _xblockexpression_1 = ScopDeadCodeElimination.debug("Dead if block ");
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected Object _removeEmptyBlocks(final ScopBlock node) {
    int _size = node.getChildren().size();
    boolean _notEquals = (_size != 0);
    if (_notEquals) {
      EList<ScopNode> _children = node.getChildren();
      final ArrayList<ScopNode> children = new ArrayList<ScopNode>(_children);
      for (final ScopNode c : children) {
        this.removeEmptyBlocks(c);
      }
    }
    int _size_1 = node.getChildren().size();
    boolean _equals = (_size_1 == 0);
    if (_equals) {
      ScopDeadCodeElimination.debug("Dead block ");
      node.getParentScop().remove(node);
    }
    return null;
  }
  
  protected Object _removeEmptyBlocks(final ScopForLoop node) {
    String _xblockexpression = null;
    {
      ScopNode _body = node.getBody();
      boolean _tripleNotEquals = (_body != null);
      if (_tripleNotEquals) {
        this.removeEmptyBlocks(node.getBody());
      }
      String _xifexpression = null;
      ScopNode _body_1 = node.getBody();
      boolean _tripleEquals = (_body_1 == null);
      if (_tripleEquals) {
        _xifexpression = ScopDeadCodeElimination.debug("Loop with dead body ");
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public Object removeEmptyBlocks(final ScopNode node) {
    if (node instanceof GecosScopBlock) {
      return _removeEmptyBlocks((GecosScopBlock)node);
    } else if (node instanceof ScopBlock) {
      return _removeEmptyBlocks((ScopBlock)node);
    } else if (node instanceof ScopForLoop) {
      return _removeEmptyBlocks((ScopForLoop)node);
    } else if (node instanceof ScopGuard) {
      return _removeEmptyBlocks((ScopGuard)node);
    } else if (node instanceof ScopStatement) {
      return _removeEmptyBlocks((ScopStatement)node);
    } else if (node != null) {
      return _removeEmptyBlocks(node);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(node).toString());
    }
  }
}
