package fr.irisa.cairn.gecos.model.scop.transforms;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopISLConverter;
import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.annotations.IAnnotation;
import gecos.core.Scope;
import gecos.core.Symbol;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.factory.IntExpressionBuilder;

@GSModule("Create copies of scop nodes annotated with a scop_duplicate (index) pragma on the index dimension.")
@SuppressWarnings("all")
public class ScopDuplicateNode {
  private static boolean VERBOSE = false;
  
  public static String debug(final String string) {
    String _xifexpression = null;
    if (ScopDuplicateNode.VERBOSE) {
      _xifexpression = InputOutput.<String>println(string);
    }
    return _xifexpression;
  }
  
  private static boolean UNIQUEFY_LOCAL_SYMBOLS = true;
  
  private EObject obj;
  
  public ScopDuplicateNode(final EObject obj) {
    this.obj = obj;
  }
  
  public void compute() {
    ScopDuplicateNode.debug("ScopDuplicateNode");
    EList<ScopNode> _eAllContentsInstancesOf = EMFUtils.<ScopNode>eAllContentsInstancesOf(this.obj, ScopNode.class);
    for (final ScopNode scop : _eAllContentsInstancesOf) {
      {
        EMap<String, IAnnotation> _annotations = scop.getAnnotations();
        IAnnotation _get = null;
        if (_annotations!=null) {
          _get=_annotations.get("GCS");
        }
        EList<ScopTransformDirective> _directives = null;
        if (((S2S4HLSPragmaAnnotation) _get)!=null) {
          _directives=((S2S4HLSPragmaAnnotation) _get).getDirectives();
        }
        Iterable<DuplicateNodePragma> _filter = null;
        if (_directives!=null) {
          _filter=Iterables.<DuplicateNodePragma>filter(_directives, 
            DuplicateNodePragma.class);
        }
        final Iterable<DuplicateNodePragma> commands = _filter;
        if ((commands != null)) {
          for (final DuplicateNodePragma command : commands) {
            ScopDuplicateNode.duplicate(scop, command.getSymbol(), command.getFactor());
          }
        }
      }
    }
  }
  
  public static ScopBlock duplicate(final ScopNode node, final Symbol index, final int factor) {
    ScopBlock _xblockexpression = null;
    {
      ScopDuplicateNode.debug(("Duplicating ScopNode: " + node));
      final JNIISLSet originalDomain = ScopISLConverter.getDomain(node, node.getScopRoot());
      final ScopDimension iterator = ScopDuplicateNode.findAndStrideForLoop(node, index, factor);
      final ScopBlock block = ScopUserFactory.scopBlock();
      node.getParentScop().replace(node, block);
      block.getChildren().add(node);
      List<Symbol> localSymbols = null;
      if (ScopDuplicateNode.UNIQUEFY_LOCAL_SYMBOLS) {
        final Function1<Symbol, Boolean> _function = (Symbol it) -> {
          return Boolean.valueOf(ScopAccessAnalyzer.computeFlowIn(it, node).isEmpty());
        };
        final Function1<Symbol, Boolean> _function_1 = (Symbol it) -> {
          return Boolean.valueOf(ScopAccessAnalyzer.computeFlowOut(it, node).isEmpty());
        };
        localSymbols = IterableExtensions.<Symbol>toList(IterableExtensions.<Symbol>filter(IterableExtensions.<Symbol>filter(node.listAllReferencedSymbols(), _function), _function_1));
      }
      final JNIISLSet strideDomain = ScopISLConverter.getDomain(node, node.getScopRoot());
      final int iteratorPos = node.listAllEnclosingIterators().indexOf(iterator);
      for (int i = 1; (i < factor); i++) {
        {
          ScopNode duplicate = node.copy();
          block.getChildren().add(duplicate);
          ScopDuplicateNode.shiftIterator(duplicate, iterator, i);
          boolean _verifyShiftValidity = ScopDuplicateNode.verifyShiftValidity(duplicate, originalDomain, strideDomain, iteratorPos, i);
          if (_verifyShiftValidity) {
            if (ScopDuplicateNode.UNIQUEFY_LOCAL_SYMBOLS) {
              ScopDuplicateNode.uniquefyLocalSymbols(duplicate, localSymbols);
            }
          }
        }
      }
      ScopDuplicateNode.debug(("Duplicated ScopNode: " + block));
      _xblockexpression = block;
    }
    return _xblockexpression;
  }
  
  private static ScopDimension findAndStrideForLoop(final ScopNode node, final Symbol index, final int factor) {
    ScopDimension _xblockexpression = null;
    {
      ScopForLoop forLoop = node.getEnclosingForLoop();
      ScopDimension iterator = null;
      while ((iterator == null)) {
        {
          if ((forLoop == null)) {
            throw new UnsupportedOperationException((("Cannot find enclosing for loop with index " + index) + 
              " for duplication"));
          }
          Symbol _symbol = forLoop.getIterator().getSymbol();
          boolean _equals = Objects.equal(_symbol, index);
          if (_equals) {
            iterator = forLoop.getIterator();
          } else {
            forLoop = forLoop.getEnclosingForLoop();
          }
        }
      }
      if (((!Objects.equal(forLoop.getStride().isConstant(), FuzzyBoolean.YES)) && (((AffineExpression) forLoop.getStride()).getConstantTerm().getCoef() != 1))) {
        throw new UnsupportedOperationException(("ScopDuplicate do not support duplication on strided loop " + forLoop));
      }
      forLoop.setStride(IntExpressionBuilder.prod(forLoop.getStride(), IntExpressionBuilder.constant(factor)));
      _xblockexpression = iterator;
    }
    return _xblockexpression;
  }
  
  private static void shiftIterator(final ScopNode node, final ScopDimension iterator, final int shift) {
    final IntExpression newIterator = IntExpressionBuilder.add(IntExpressionBuilder.affine(iterator), shift);
    final EList<IntExpression> exprList = EMFUtils.<IntExpression>eAllContentsInstancesOf(node, IntExpression.class);
    for (final IntExpression expr : exprList) {
      {
        final IntExpression newExpr = expr.substitute(iterator, newIterator.<IntExpression>copy());
        EMFUtils.substituteByNewObjectInContainer(expr, newExpr);
      }
    }
  }
  
  private static boolean verifyShiftValidity(final ScopNode node, final JNIISLSet originalDomain, final JNIISLSet strideDomain, final int iteratorPos, final int shift) {
    boolean _xblockexpression = false;
    {
      StringConcatenation _builder = new StringConcatenation();
      {
        int _nbParams = strideDomain.getNbParams();
        boolean _greaterThan = (_nbParams > 0);
        if (_greaterThan) {
          List<String> _parametersNames = strideDomain.getParametersNames();
          _builder.append(_parametersNames);
          _builder.append(" -> ");
        }
      }
      _builder.append(" { ");
      int _nbDims = strideDomain.getNbDims();
      final Function1<Integer, String> _function = (Integer it) -> {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("c");
        _builder_1.append(it);
        return _builder_1.toString();
      };
      Iterable<String> _map = IterableExtensions.<Integer, String>map(new ExclusiveRange(0, _nbDims, true), _function);
      _builder.append(_map);
      _builder.append(" -> ");
      String iterStr = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(iterStr);
      int _nbDims_1 = strideDomain.getNbDims();
      final Function1<Integer, String> _function_1 = (Integer it) -> {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("c");
        _builder_2.append(it);
        {
          if (((it).intValue() == iteratorPos)) {
            _builder_2.append(" + ");
            _builder_2.append(shift);
          }
        }
        return _builder_2.toString();
      };
      Iterable<String> _map_1 = IterableExtensions.<Integer, String>map(new ExclusiveRange(0, _nbDims_1, true), _function_1);
      _builder_1.append(_map_1);
      _builder_1.append(":}");
      final String str = _builder_1.toString();
      final JNIISLMap func = JNIISLMap.buildFromString(JNIISLContext.getCtx(), str);
      final JNIISLSet shiftedDomain = strideDomain.copy().apply(func.copy());
      final JNIISLSet validshiftedDomain = shiftedDomain.copy().intersect(originalDomain.copy());
      boolean _isEmpty = validshiftedDomain.isEmpty();
      if (_isEmpty) {
        node.getParentScop().remove(node);
        return false;
      }
      JNIISLSet reducedGuard = validshiftedDomain.copy().gist(shiftedDomain.copy());
      boolean _plainIsUniverse = reducedGuard.plainIsUniverse();
      boolean _not = (!_plainIsUniverse);
      if (_not) {
        reducedGuard = reducedGuard.copy().apply(func.copy().reverse());
        ISLScopConverter.guard(node, reducedGuard);
      }
      _xblockexpression = true;
    }
    return _xblockexpression;
  }
  
  private static void uniquefyLocalSymbols(final ScopNode node, final List<Symbol> localSymbols) {
    ScopNode _scopRoot = node.getScopRoot();
    final Scope scope = ((GecosScopBlock) _scopRoot).getScope();
    for (final Symbol sym : localSymbols) {
      {
        int nb = 0;
        while ((scope.lookup(((sym.getName() + "_dup") + Integer.valueOf(nb))) != null)) {
          nb++;
        }
        String _name = sym.getName();
        String _plus = (_name + "_dup");
        String _plus_1 = (_plus + Integer.valueOf(nb));
        final Symbol copy = GecosUserCoreFactory.symbol(_plus_1, sym.getType(), scope);
        EMFUtils.replaceAllRefsInContainement(node, sym, copy);
      }
    }
  }
}
