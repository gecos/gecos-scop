package fr.irisa.cairn.gecos.model.scop.transforms;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.util.ScopIteratorGenerator;
import gecos.core.Symbol;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ScopIteratorNormalizer {
  private final GecosScopBlock scop;
  
  private final ArrayList<ScopDimension> oldIterators = new ArrayList<ScopDimension>();
  
  private final ArrayList<ScopDimension> newIterators = new ArrayList<ScopDimension>();
  
  private int depth = 0;
  
  public ScopIteratorNormalizer(final GecosScopBlock scop) {
    this.scop = scop;
    this.oldIterators.addAll(scop.getIterators());
  }
  
  public void compute() {
    this.scop.getIterators().removeAll(this.oldIterators);
    final Function1<ScopDimension, Symbol> _function = (ScopDimension it) -> {
      return it.getSymbol();
    };
    this.scop.getScope().getSymbols().removeAll(ListExtensions.<ScopDimension, Symbol>map(this.oldIterators, _function));
    for (final ScopDimension i : this.oldIterators) {
      this.newIterators.add(ScopIteratorGenerator.newIterator(this.scop));
    }
    this.normalize(this.scop.getRoot());
  }
  
  protected void _normalize(final ScopNode node) {
  }
  
  protected void _normalize(final ScopBlock block) {
    final Consumer<ScopNode> _function = (ScopNode it) -> {
      this.normalize(it);
    };
    block.getChildren().forEach(_function);
  }
  
  protected void _normalize(final ScopForLoop loop) {
    loop.substitute(loop.getIterator(), this.newIterators.get(this.depth));
    this.depth++;
    this.normalize(loop.getBody());
    this.depth--;
  }
  
  public void normalize(final ScopNode block) {
    if (block instanceof ScopBlock) {
      _normalize((ScopBlock)block);
      return;
    } else if (block instanceof ScopForLoop) {
      _normalize((ScopForLoop)block);
      return;
    } else if (block != null) {
      _normalize(block);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(block).toString());
    }
  }
}
