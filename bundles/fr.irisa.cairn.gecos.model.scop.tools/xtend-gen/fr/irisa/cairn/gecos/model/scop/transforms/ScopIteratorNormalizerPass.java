package fr.irisa.cairn.gecos.model.scop.transforms;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopIteratorNormalizer;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.gecosproject.GecosProject;
import org.eclipse.emf.common.util.EList;

@GSModule("Normalize Scop iterators before new transformations")
@SuppressWarnings("all")
public class ScopIteratorNormalizerPass {
  private GecosProject proj;
  
  public ScopIteratorNormalizerPass(final GecosProject proj) {
    this.proj = proj;
  }
  
  public void compute() {
    final EList<GecosScopBlock> scops = EMFUtils.<GecosScopBlock>eAllContentsInstancesOf(this.proj, GecosScopBlock.class);
    for (final GecosScopBlock scop : scops) {
      new ScopIteratorNormalizer(scop).compute();
    }
  }
}
