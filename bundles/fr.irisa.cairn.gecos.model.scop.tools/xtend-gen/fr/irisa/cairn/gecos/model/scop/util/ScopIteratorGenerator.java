package fr.irisa.cairn.gecos.model.scop.util;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import gecos.core.Scope;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class ScopIteratorGenerator {
  /**
   * Creates a new iterator and adds it to the scop root.
   *  Requires node to be rooted.
   * 
   * @param node Scop in which to create the iterator
   * @return created iterator
   */
  public static ScopDimension newIterator(final ScopNode node) {
    ScopDimension _xblockexpression = null;
    {
      ScopNode _scopRoot = node.getScopRoot();
      final GecosScopBlock root = ((GecosScopBlock) _scopRoot);
      final Scope scope = root.getScope();
      int depth = root.getIterators().size();
      while ((scope.lookup(ScopIteratorGenerator.getName(depth)) != null)) {
        depth++;
      }
      ScopUserFactory.setScope(scope);
      final ScopDimension iterator = ScopUserFactory.dim(ScopIteratorGenerator.getName(depth));
      root.getIterators().add(iterator);
      _xblockexpression = iterator;
    }
    return _xblockexpression;
  }
  
  /**
   * Find or create n available iterators relative to node.
   *  Requires node to be rooted.
   * 
   * @param node Scop used as relative root to find or create iterators
   * @param n number of required iterators
   * @return list of free iterators
   */
  public static Iterable<ScopDimension> freeIterators(final ScopNode node, final int n) {
    final EList<ScopDimension> iterators = node.listAllFreeIterators();
    int _size = iterators.size();
    boolean _greaterEqualsThan = (_size >= n);
    if (_greaterEqualsThan) {
      return IterableExtensions.<ScopDimension>take(iterators, n);
    }
    while ((iterators.size() < n)) {
      iterators.add(ScopIteratorGenerator.newIterator(node));
    }
    return iterators;
  }
  
  private final static List<String> names = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("i", "j", "k", "l", "m", "n"));
  
  private static String getName(final int depth) {
    String _xifexpression = null;
    int _size = ScopIteratorGenerator.names.size();
    boolean _greaterThan = (_size > depth);
    if (_greaterThan) {
      _xifexpression = ScopIteratorGenerator.names.get(depth);
    } else {
      _xifexpression = ("c" + Integer.valueOf(depth));
    }
    return _xifexpression;
  }
}
