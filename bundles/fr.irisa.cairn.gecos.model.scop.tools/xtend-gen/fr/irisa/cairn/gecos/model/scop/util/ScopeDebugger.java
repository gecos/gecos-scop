package fr.irisa.cairn.gecos.model.scop.util;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.types.Type;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class ScopeDebugger {
  private GecosProject proj;
  
  private PrintStream outfile;
  
  public ScopeDebugger(final GecosProject p, final String filename) {
    try {
      this.proj = p;
      PrintStream _printStream = new PrintStream(filename);
      this.outfile = _printStream;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void compute() {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<ProcedureSet> _listProcedureSets = this.proj.listProcedureSets();
      for(final ProcedureSet ps : _listProcedureSets) {
        CharSequence _visit = this.visit(ps.getScope());
        _builder.append(_visit);
        _builder.newLineIfNotEmpty();
        {
          EList<Procedure> _listProcedures = ps.listProcedures();
          for(final Procedure p : _listProcedures) {
            _builder.append("function ");
            String _symbolName = p.getSymbolName();
            _builder.append(_symbolName);
            _builder.append(" {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            CharSequence _visit_1 = this.visit(p.getBody());
            _builder.append(_visit_1, "\t");
            _builder.newLineIfNotEmpty();
            _builder.append("}");
            _builder.newLine();
          }
        }
      }
    }
    this.outfile.append(_builder);
    this.outfile.close();
  }
  
  protected CharSequence _visit(final Scope i) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Scope ");
    int _hashCode = i.hashCode();
    _builder.append(_hashCode);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      int _size = i.getSymbols().size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        _builder.append("\t");
        _builder.append("symbols {");
        _builder.newLine();
        {
          EList<Symbol> _symbols = i.getSymbols();
          boolean _hasElements = false;
          for(final Symbol s : _symbols) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(";", "\t\t");
            }
            _builder.append("\t");
            _builder.append("\t");
            Object _visit = this.visit(s);
            _builder.append(_visit, "\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.append("} ");
        _builder.newLine();
      }
    }
    {
      int _size_1 = i.getTypes().size();
      boolean _greaterThan_1 = (_size_1 > 0);
      if (_greaterThan_1) {
        _builder.append("\t");
        _builder.append("types {");
        _builder.newLine();
        {
          EList<Type> _types = i.getTypes();
          boolean _hasElements_1 = false;
          for(final Type s_1 : _types) {
            if (!_hasElements_1) {
              _hasElements_1 = true;
            } else {
              _builder.appendImmediate(";", "\t\t");
            }
            _builder.append("\t");
            _builder.append("\t");
            Object _visit_1 = this.visit(s_1);
            _builder.append(_visit_1, "\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _visit(final EObject o) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = o.eClass().getName();
    _builder.append(_name);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("uses { ");
    _builder.newLine();
    _builder.append("\t\t");
    {
      Iterable<Symbol> _filter = Iterables.<Symbol>filter(o.eCrossReferences(), Symbol.class);
      boolean _hasElements = false;
      for(final Symbol s : _filter) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "\t\t");
        }
        _builder.append(" ");
        Object _visit = this.visit(s);
        _builder.append(_visit, "\t\t");
        _builder.append(" ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    {
      Iterable<Type> _filter_1 = Iterables.<Type>filter(o.eCrossReferences(), Type.class);
      boolean _hasElements_1 = false;
      for(final Type s_1 : _filter_1) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(",", "\t\t");
        }
        _builder.append(" ");
        Object _visit_1 = this.visit(s_1);
        _builder.append(_visit_1, "\t\t");
        _builder.append(" ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    {
      EList<EObject> _eContents = o.eContents();
      for(final EObject s_2 : _eContents) {
        _builder.append("\t");
        Object _visit_2 = this.visit(s_2);
        _builder.append(_visit_2, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _visit(final IfBlock i) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("IfBlock {");
    _builder.newLine();
    _builder.append("\t");
    Object _visit = this.visit(i.getTestBlock());
    _builder.append(_visit, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      Block _elseBlock = i.getElseBlock();
      boolean _notEquals = (!Objects.equal(_elseBlock, null));
      if (_notEquals) {
        Object _visit_1 = this.visit(i.getElseBlock());
        _builder.append(_visit_1, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_2 = this.visit(i.getThenBlock());
    _builder.append(_visit_2, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _visit(final ForBlock i) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ForBlock");
    int _number = i.getNumber();
    _builder.append(_number);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit = this.visit(i.getInitBlock());
    _builder.append(_visit, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_1 = this.visit(i.getTestBlock());
    _builder.append(_visit_1, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_2 = this.visit(i.getStepBlock());
    _builder.append(_visit_2, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_3 = this.visit(i.getBodyBlock());
    _builder.append(_visit_3, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _visit(final SimpleForBlock i) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("SimpleForBlock");
    int _number = i.getNumber();
    _builder.append(_number);
    _builder.append(" [");
    Symbol _iterator = i.getIterator();
    _builder.append(_iterator);
    _builder.append("] {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit = this.visit(i.getInitBlock());
    _builder.append(_visit, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_1 = this.visit(i.getTestBlock());
    _builder.append(_visit_1, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_2 = this.visit(i.getStepBlock());
    _builder.append(_visit_2, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    Object _visit_3 = this.visit(i.getBodyBlock());
    _builder.append(_visit_3, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _visit(final BasicBlock i) {
    CharSequence _xblockexpression = null;
    {
      HashSet<String> smap = new HashSet<String>();
      EList<ISymbolUse> _eAllContentsInstancesOf = EMFUtils.<ISymbolUse>eAllContentsInstancesOf(i, ISymbolUse.class);
      for (final ISymbolUse suse : _eAllContentsInstancesOf) {
        smap.add(this.visit(suse.getUsedSymbol()).toString());
      }
      HashSet<String> tmap = new HashSet<String>();
      EList<ITypedElement> _eAllContentsInstancesOf_1 = EMFUtils.<ITypedElement>eAllContentsInstancesOf(i, ITypedElement.class);
      for (final ITypedElement tuse : _eAllContentsInstancesOf_1) {
        Type _type = tuse.getType();
        boolean _tripleNotEquals = (_type != null);
        if (_tripleNotEquals) {
          tmap.add(this.visit(tuse.getType()).toString());
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("BB");
      int _number = i.getNumber();
      _builder.append(_number);
      _builder.append("{");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("used_symbols = ");
      String _string = smap.toString();
      _builder.append(_string, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("used_types = ");
      String _string_1 = tmap.toString();
      _builder.append(_string_1, "\t");
      _builder.append("\t\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _visit(final CompositeBlock i) {
    CharSequence _xifexpression = null;
    Scope _scope = i.getScope();
    boolean _notEquals = (!Objects.equal(_scope, null));
    if (_notEquals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("CompositeBlock");
      int _number = i.getNumber();
      _builder.append(_number);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      Object _visit = this.visit(i.getScope());
      _builder.append(_visit, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      {
        EList<Block> _children = i.getChildren();
        for(final Block s : _children) {
          Object _visit_1 = this.visit(s);
          _builder.append(_visit_1, "\t");
        }
      }
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("CompositeBlock");
      int _number_1 = i.getNumber();
      _builder_1.append(_number_1);
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      {
        EList<Block> _children_1 = i.getChildren();
        for(final Block s_1 : _children_1) {
          Object _visit_2 = this.visit(s_1);
          _builder_1.append(_visit_2, "\t");
        }
      }
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("}");
      _builder_1.newLine();
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected String _visit(final Type i) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(i);
    _builder.append("_");
    int _hashCode = i.hashCode();
    int _modulo = (_hashCode % 256);
    _builder.append(_modulo);
    return _builder.toString();
  }
  
  protected String _visit(final Symbol i) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = i.getName();
    _builder.append(_name);
    _builder.append("_");
    int _hashCode = i.hashCode();
    int _modulo = (_hashCode % 256);
    _builder.append(_modulo);
    _builder.append(":");
    CharSequence _visit = this.visit(i.getType());
    _builder.append(_visit);
    return _builder.toString();
  }
  
  public CharSequence visit(final EObject i) {
    if (i instanceof SimpleForBlock) {
      return _visit((SimpleForBlock)i);
    } else if (i instanceof BasicBlock) {
      return _visit((BasicBlock)i);
    } else if (i instanceof CompositeBlock) {
      return _visit((CompositeBlock)i);
    } else if (i instanceof ForBlock) {
      return _visit((ForBlock)i);
    } else if (i instanceof IfBlock) {
      return _visit((IfBlock)i);
    } else if (i instanceof Scope) {
      return _visit((Scope)i);
    } else if (i instanceof Symbol) {
      return _visit((Symbol)i);
    } else if (i instanceof Type) {
      return _visit((Type)i);
    } else if (i != null) {
      return _visit(i);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(i).toString());
    }
  }
}
