package tom.mapping.introspectors.transform;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EClass;

import tom.library.sl.Introspector;
import tom.mapping.IntrospectorManager;

import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;


/* PROTECTED REGION ID(introspector_imports) ENABLED START */
// protected imports
/* PROTECTED REGION END */

/**
* TOM introspector for transform.
* -- Autogenerated by TOM mapping EMF genrator --
*/

public class TransformIntrospector implements Introspector {
	
	public static final TransformIntrospector INSTANCE = new TransformIntrospector();
	
	static {
		IntrospectorManager.INSTANCE.register(TransformPackage.eINSTANCE, INSTANCE);
	}

	/* PROTECTED REGION ID(introspector_members) ENABLED START */
	/* PROTECTED REGION END */
	
	protected TransformIntrospector() {}
	
	public Object getChildAt(Object o, int i) {
		return getChildren(o)[i];
	}
	
	public int getChildCount(Object o) {
		return getChildren(o).length;
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getChildren(Object arg0) {
		List<Object> l = new ArrayList<Object>();
		if (arg0 instanceof List) {
			// Children of a list are its content
			for(Object object : (List<Object>) arg0) {
				l.add(object);
			}
			return l.toArray();
		}
		return TransformChildrenGetter.INSTANCE.children(arg0);
	}
	
	private static class TransformChildrenGetter extends fr.irisa.cairn.gecos.model.scop.transform.util.TransformSwitch<Object[]> {
		public final static TransformChildrenGetter INSTANCE = new TransformChildrenGetter();
		
		private TransformChildrenGetter(){}
		
		public Object[] children(Object i) {
			Object[] children = doSwitch((EObject) i);
			return children != null ? children : new Object[0];
		}
		
		public Object[] caseScopTransform(fr.irisa.cairn.gecos.model.scop.transform.ScopTransform o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getCommands() != null)
				l.add(o.getCommands());
			
			/*PROTECTED REGION ID(getter_transform_ScopTransform) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseAddContextDirective(fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getParams() != null)
				l.add(o.getParams());
			if (o.getContext() != null)
				l.add(o.getContext());
			
			/*PROTECTED REGION ID(getter_transform_AddContextDirective) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDataLayoutDirective(fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getAddresses() != null)
				l.add(o.getAddresses());
			if (o.getVars() != null)
				l.add(o.getVars());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_transform_DataLayoutDirective) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseScheduleDirective(fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getStatement() != null)
				l.add(o.getStatement());
			
			/*PROTECTED REGION ID(getter_transform_ScheduleDirective) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseContractArrayDirective(fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_transform_ContractArrayDirective) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T setChildren(T arg0, Object[] arg1) {
		if (arg0 instanceof List) {
			// If object is a list then content of the original list has to be replaced
			List<Object> list = (List<Object>) arg0;
			list.clear();
			for (int i = 0; i < arg1.length; i++) {
				list.add(arg1[i]);
			}
			return arg0;
		} else {
			return (T) TransformChildrenSetter.INSTANCE.set(arg0, arg1);
		}
	}
	
	private static class TransformChildrenSetter extends fr.irisa.cairn.gecos.model.scop.transform.util.TransformSwitch<Object> {
		public final static TransformChildrenSetter INSTANCE = new TransformChildrenSetter();
		
		private Object[] children;
		private TransformChildrenSetter(){}
		
		public Object set(Object i, Object[] children) {
			this.children = children;
			return doSwitch((EObject) i);
		}
		
		public Object caseAddContextDirective(fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective o) {
			o.setParams((fr.irisa.cairn.gecos.model.scop.ScopDimension)children[0]);
			o.setContext((org.polymodel.algebra.IntConstraintSystem)children[1]);
		
			/*PROTECTED REGION ID(setter_transform_AddContextDirective) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDataLayoutDirective(fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective o) {
			o.setSymbol((gecos.core.Symbol)children[0]);
		
			/*PROTECTED REGION ID(setter_transform_DataLayoutDirective) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseScheduleDirective(fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective o) {
			o.setStatement((fr.irisa.cairn.gecos.model.scop.ScopStatement)children[0]);
		
			/*PROTECTED REGION ID(setter_transform_ScheduleDirective) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseContractArrayDirective(fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective o) {
			o.setSymbol((gecos.core.Symbol)children[0]);
		
			/*PROTECTED REGION ID(setter_transform_ContractArrayDirective) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
	}

	public <T> T setChildAt(T o, int i, Object obj) {
		throw new RuntimeException("Not implemented");
	}
}
