package tom.mapping.introspectors.types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EClass;

import tom.library.sl.Introspector;
import tom.mapping.IntrospectorManager;

import gecos.types.TypesPackage;


/* PROTECTED REGION ID(introspector_imports) ENABLED START */
// protected imports
/* PROTECTED REGION END */

/**
* TOM introspector for scop.
* -- Autogenerated by TOM mapping EMF genrator --
*/

public class TypesIntrospector implements Introspector {
	
	public static final TypesIntrospector INSTANCE = new TypesIntrospector();
	
	static {
		IntrospectorManager.INSTANCE.register(TypesPackage.eINSTANCE, INSTANCE);
	}

	/* PROTECTED REGION ID(introspector_members) ENABLED START */
	/* PROTECTED REGION END */
	
	protected TypesIntrospector() {}
	
	public Object getChildAt(Object o, int i) {
		return getChildren(o)[i];
	}
	
	public int getChildCount(Object o) {
		return getChildren(o).length;
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getChildren(Object arg0) {
		List<Object> l = new ArrayList<Object>();
		if (arg0 instanceof List) {
			// Children of a list are its content
			for(Object object : (List<Object>) arg0) {
				l.add(object);
			}
			return l.toArray();
		}
		return TypesChildrenGetter.INSTANCE.children(arg0);
	}
	
	private static class TypesChildrenGetter extends gecos.types.util.TypesSwitch<Object[]> {
		public final static TypesChildrenGetter INSTANCE = new TypesChildrenGetter();
		
		private TypesChildrenGetter(){}
		
		public Object[] children(Object i) {
			Object[] children = doSwitch((EObject) i);
			return children != null ? children : new Object[0];
		}
		
		public Object[] caseEnumerator(gecos.types.Enumerator o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getExpression() != null)
				l.add(o.getExpression());
			
			/*PROTECTED REGION ID(getter_types_Enumerator) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseArrayType(gecos.types.ArrayType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ArrayType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACIntType(gecos.types.ACIntType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ACIntType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACFixedType(gecos.types.ACFixedType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ACFixedType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T setChildren(T arg0, Object[] arg1) {
		if (arg0 instanceof List) {
			// If object is a list then content of the original list has to be replaced
			List<Object> list = (List<Object>) arg0;
			list.clear();
			for (int i = 0; i < arg1.length; i++) {
				list.add(arg1[i]);
			}
			return arg0;
		} else {
			return (T) TypesChildrenSetter.INSTANCE.set(arg0, arg1);
		}
	}
	
	private static class TypesChildrenSetter extends gecos.types.util.TypesSwitch<Object> {
		public final static TypesChildrenSetter INSTANCE = new TypesChildrenSetter();
		
		private Object[] children;
		private TypesChildrenSetter(){}
		
		public Object set(Object i, Object[] children) {
			this.children = children;
			return doSwitch((EObject) i);
		}
		
		public Object caseEnumerator(gecos.types.Enumerator o) {
			o.setExpression((gecos.instrs.Instruction)children[0]);
		
			/*PROTECTED REGION ID(setter_types_Enumerator) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseArrayType(gecos.types.ArrayType o) {
			o.setSizeExpr((gecos.instrs.Instruction)children[0]);
		
			/*PROTECTED REGION ID(setter_types_ArrayType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACIntType(gecos.types.ACIntType o) {
			o.setSizeExpr((gecos.instrs.Instruction)children[0]);
		
			/*PROTECTED REGION ID(setter_types_ACIntType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACFixedType(gecos.types.ACFixedType o) {
			o.setSizeExpr((gecos.instrs.Instruction)children[0]);
		
			/*PROTECTED REGION ID(setter_types_ACFixedType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
	}

	public <T> T setChildAt(T o, int i, Object obj) {
		throw new RuntimeException("Not implemented");
	}
}
