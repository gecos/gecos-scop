package fr.irisa.cairn.gecos.model.scop.util;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;

public class BuildControlFlowSwitchAdapter extends ScopSwitch<Object> {
	private BuildControlFlow adaptable;

	// Here is the mandatory constructor
	public BuildControlFlowSwitchAdapter(BuildControlFlow adaptable) {
		this.adaptable = adaptable;
	}

	@Override
	public Object caseGecosScopBlock(GecosScopBlock object) {
		adaptable.caseBasicBlock(object);
		return null;
	}
}