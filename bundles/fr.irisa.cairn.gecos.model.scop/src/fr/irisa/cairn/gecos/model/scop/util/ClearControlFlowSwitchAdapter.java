package fr.irisa.cairn.gecos.model.scop.util;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;

public class ClearControlFlowSwitchAdapter extends ScopSwitch<Object> {
	private ClearControlFlow adaptable;

	// Here is the mandatory constructor
	public ClearControlFlowSwitchAdapter(ClearControlFlow adaptable) {
		this.adaptable = adaptable;
	}

	@Override
	public Object caseGecosScopBlock(GecosScopBlock object) {
		object.getOutEdges().clear();
		object.getInEdges().clear();
		return null;
	}
}