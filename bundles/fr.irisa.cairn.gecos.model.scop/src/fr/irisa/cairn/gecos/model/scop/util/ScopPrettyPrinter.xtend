package fr.irisa.cairn.gecos.model.scop.util

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopBlock
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock
import fr.irisa.cairn.gecos.model.scop.ScopFSMState
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition
import fr.irisa.cairn.gecos.model.scop.ScopForLoop
import fr.irisa.cairn.gecos.model.scop.ScopGuard
import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.ScopRead
import fr.irisa.cairn.gecos.model.scop.ScopStatement
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
import fr.irisa.cairn.gecos.model.scop.ScopWrite
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
import java.util.HashSet
import org.polymodel.algebra.Variable
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedBlock
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead

class ScopPrettyPrinter {

	static def  dispatch String print(ScopDimension r) {
		if(r.symbol!==null)
		'''«r.symbol.name»'''
		else
		'''?'''
	}

	static def dispatch String print(ScopNode n) {
		throw new UnsupportedOperationException("NYI")
	}


	static def  dispatch String print(ScopGuard n) {
		if(n===null)
			throw new UnsupportedOperationException("NPE")
		if(n.predicate===null)
			throw new UnsupportedOperationException("no predicate")
		if(n.thenNode===null)
			throw new UnsupportedOperationException("no then branch")
		if(n.elseNode!==null) {
			'''
				if («n.predicate») 
				   «if (n.thenNode!=null) print(n.thenNode) else "[null]"»
				else			
				   «if (n.elseNode!=null) print(n.elseNode) else "[null]"»
			'''
		} else {
			'''
				if («n.predicate») 
				   «print(n.thenNode)»
			'''
		}
	}  
	static def  dispatch String print(ScopForLoop l) {
		if(l===null)
			throw new UnsupportedOperationException("NPE")
		val iterator = l.iterator
		if(iterator!==null ){
			'''
				for («iterator.symName»[«iterator.name»]=«l.getLB»:«l.stride»:«l.getUB») 
					«if (l.body!==null ) print(l.body) else "[null]"»
			'''
		} else {
			'''
				for (?=«l.getLB»:«l.stride»:«l.getUB») 
					«if (l.body!==null ) print(l.body) else "[null]"»
			'''
			
		}
	}

	static def  dispatch String print(ScopDoAllLoop l) {
		'''
			doall («l.iterator.name»=«l.getLB»:«l.stride»:«l.getUB») 
				«print(l.body)»
		'''
		
	}

	static def  dispatch String print(ScopBlock n) {
		if (n.children!==null) {
		'''
			{
				«FOR i:n.children»«if (i!==null) print(i) else "[null]"»«ENDFOR»	
			}'''
		}
	}
	static def  dispatch String print(GecosScopBlock n) {
		'''
		GecosScopBlock_«n.number»«n.parameters» {
			live-in : «n.liveInSymbols»
			live-out : «n.liveOutSymbols»
			iterators : «n.iterators»
			context : «n.context»
			«IF n.root!==null»«print(n.root)»«ENDIF»	
		}'''
	}
	static def  dispatch String print(ScopStatement n) {
		'''
			[«FOR r:n.listAllWriteAccess SEPARATOR ","»«print(r)»«ENDFOR»] := «n.id»(«FOR r:n.listAllReadAccess SEPARATOR ","»«print(r)»«ENDFOR»);
		'''
	}

	static def  dispatch String print(ScopInstructionStatement n) {
		'''
			[«FOR r:n.listAllWriteAccess SEPARATOR ","»«print(r)»«ENDFOR»] := «n.id»(«FOR r:n.listAllReadAccess SEPARATOR ","»«print(r)»«ENDFOR») -> «n.children»;
		'''
	}

	static def  dispatch String print(ScopUnexpandedStatement n) {
		'''
			Unexpanded «n.name»«n.existentials» -> «n.domain» {
				«IF n.transform!==null»schedule : «n.transform.commands.filter(ScheduleDirective).get(0)»«ENDIF»
				: «n.children.get(0)» 
				writes : «FOR r:n.listAllWriteAccess SEPARATOR ","»«print(r)»«ENDFOR»
				reads  : «FOR r:n.listAllReadAccess SEPARATOR ","»«print(r)»«ENDFOR»;
			}
		'''
	}
	
	static def  dispatch String print(ScopUnexpandedBlock n) {
	
		'''
			UnexpandeBlock «n.existentials» -> «n.domain» {
				«IF n.transform!==null»schedule : «n.transform.commands.filter(ScheduleDirective).get(0)»«ENDIF»
				: «n.children» 
			}
		'''
	}
	
	static def  dispatch String print(ScopRead r) {
		if(r.sym!==null)
		'''«r.symName»[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR»]'''
		else
		'''none[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR»]'''
	}

	static def  dispatch String print(ScopRegionRead r) {
		if(r.sym!==null)
		'''«r.symName»[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR» : «r.region»]'''
		else
		'''none[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR» : «r.region»]'''
	}

	static def  dispatch String print(ScopWrite r) {
		if(r.sym!==null)
		'''«r.symName»[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR»]'''
		else
		'''none[«FOR i:r.indexExpressions SEPARATOR ","»«(i)»«ENDFOR»]'''
	}

	static def  dispatch String print(ScopFSMBlock fsm) {
		'''
			// Initialisation
			«fsm.iterators» := «fsm.start.map[t|print(t)]»;
			int done_«fsm.hashCode»=1;
			while(!«fsm.hashCode»_done) {
				done_«fsm.hashCode»=0;
				// Commands
				«FOR c:fsm.commands»«print(c)»«ENDFOR»
				// Transitions
				«FOR n:fsm.next»«print(n)»«ENDFOR»
			}
		'''
	}

	static def  dispatch String print(ScopFSMState s) {
		'''
			c
		'''
	}


	static def  dispatch String printDirective(ScopTransformDirective d) {
		'''PrettyPrint for «d.class» not yet implemented'''
	}
	static def  dispatch String printDirective(ScheduleDirective d) {
		'''schedule «d.statement.id» : «d.schedule»'''
	}
	
	static def  dispatch String printDirective(DataLayoutDirective d) {
		'''remap «d.symbol»«d.vars» to «d.symbol»«d.addresses» '''
	}

	static def  dispatch String printDirective(ContractArrayDirective d) {
		'''comtract «d.symbol»'''
	} 
  
	static def  dispatch String print(ScopFSMTransition t) {
		'''
«««		if [«set»: «s.domain»]:
«««			«FOR t:s.transitions»«print(t)»«ENDFOR» 
«««		}
		'''
	}

}			