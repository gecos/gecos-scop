/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.blocks.BasicBlock;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos Scop Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveInSymbols <em>Live In Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveOutSymbols <em>Live Out Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getIterators <em>Iterators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getContext <em>Context</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock()
 * @model
 * @generated
 */
public interface GecosScopBlock extends ScopNode, BasicBlock, ScopeContainer {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_Name()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Live In Symbols</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Live In Symbols</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Live In Symbols</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_LiveInSymbols()
	 * @model
	 * @generated
	 */
	EList<Symbol> getLiveInSymbols();

	/**
	 * Returns the value of the '<em><b>Live Out Symbols</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Live Out Symbols</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Live Out Symbols</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_LiveOutSymbols()
	 * @model
	 * @generated
	 */
	EList<Symbol> getLiveOutSymbols();

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopDimension> getParameters();

	/**
	 * Returns the value of the '<em><b>Iterators</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterators</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_Iterators()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopDimension> getIterators();

	/**
	 * Returns the value of the '<em><b>Context</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' containment reference.
	 * @see #setContext(IntConstraintSystem)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_Context()
	 * @model containment="true"
	 * @generated
	 */
	IntConstraintSystem getContext();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getContext <em>Context</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' containment reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(IntConstraintSystem value);

	/**
	 * Returns the value of the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root</em>' containment reference.
	 * @see #setRoot(ScopNode)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getGecosScopBlock_Root()
	 * @model containment="true"
	 * @generated
	 */
	ScopNode getRoot();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getRoot <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root</em>' containment reference.
	 * @see #getRoot()
	 * @generated
	 */
	void setRoot(ScopNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return null;'"
	 * @generated
	 */
	ScopNode getParentScop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.GecosScopBlock%&gt;&gt;copy(this);'"
	 * @generated
	 */
	GecosScopBlock copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _root = this.getRoot();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_root, n);\nif (_equals)\n{\n\tthis.setRoot(null);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _root = this.getRoot();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_root, n);\nif (_equals)\n{\n\tthis.setRoot(_new);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void replace(ScopNode n, ScopNode _new);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='int _id = 0;\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt; _listAllStatements = this.listAllStatements();\nfor (final &lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt; s : _listAllStatements)\n{\n\tint _plusPlus = _id++;\n\t&lt;%java.lang.String%&gt; _plus = (\"S\" + &lt;%java.lang.Integer%&gt;.valueOf(_plusPlus));\n\ts.setId(_plus);\n}'"
	 * @generated
	 */
	void relabelStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = this.getName();\nreturn (\"Scop\" + _name);'"
	 * @generated
	 */
	String toShortString();

} // GecosScopBlock
