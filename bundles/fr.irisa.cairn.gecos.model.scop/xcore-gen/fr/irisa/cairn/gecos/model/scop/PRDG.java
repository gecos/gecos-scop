/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PRDG</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.PRDG#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.PRDG#getUseDefs <em>Use Defs</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.PRDG#getDefUses <em>Def Uses</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getPRDG()
 * @model
 * @generated
 */
public interface PRDG extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ScopNode)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getPRDG_Target()
	 * @model
	 * @generated
	 */
	ScopNode getTarget();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.PRDG#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ScopNode value);

	/**
	 * Returns the value of the '<em><b>Use Defs</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.UseDefEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Defs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Defs</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getPRDG_UseDefs()
	 * @model containment="true"
	 * @generated
	 */
	EList<UseDefEdge> getUseDefs();

	/**
	 * Returns the value of the '<em><b>Def Uses</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.DefUseEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def Uses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def Uses</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getPRDG_DefUses()
	 * @model containment="true"
	 * @generated
	 */
	EList<DefUseEdge> getDefUses();

} // PRDG
