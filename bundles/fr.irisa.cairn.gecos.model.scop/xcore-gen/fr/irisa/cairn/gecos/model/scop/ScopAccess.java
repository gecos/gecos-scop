/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.core.Symbol;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopAccess#getSym <em>Sym</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopAccess#getSymName <em>Sym Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopAccess()
 * @model
 * @generated
 */
public interface ScopAccess extends ScopIndexExpression {
	/**
	 * Returns the value of the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sym</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sym</em>' reference.
	 * @see #setSym(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopAccess_Sym()
	 * @model
	 * @generated
	 */
	Symbol getSym();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopAccess#getSym <em>Sym</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sym</em>' reference.
	 * @see #getSym()
	 * @generated
	 */
	void setSym(Symbol value);

	/**
	 * Returns the value of the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sym Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sym Name</em>' attribute.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopAccess_SymName()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%java.lang.Object%&gt; _xblockexpression = null;\n{\n\t&lt;%gecos.core.Symbol%&gt; _sym = this.getSym();\n\tboolean _tripleNotEquals = (_sym != null);\n\tif (_tripleNotEquals)\n\t{\n\t\treturn this.getSym().getName();\n\t}\n\t_xblockexpression = null;\n}\nreturn ((&lt;%java.lang.String%&gt;)_xblockexpression);'"
	 * @generated
	 */
	String getSymName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopAccess copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.Instruction%&gt; _root = super.getRoot();\nreturn ((&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;) _root);'"
	 * @generated
	 */
	ScopStatement getEnclosingStatement();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dimUnique="false" eUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; exprs = this.getIndexExpressions();\nint _size = exprs.size();\nint _minus = (_size - 1);\n&lt;%org.eclipse.xtext.xbase.lib.IntegerRange%&gt; _upTo = new &lt;%org.eclipse.xtext.xbase.lib.IntegerRange%&gt;(0, _minus);\nfor (final &lt;%java.lang.Integer%&gt; offset : _upTo)\n{\n\t{\n\t\tfinal &lt;%org.polymodel.algebra.IntExpression%&gt; substitute = exprs.get((offset).intValue()).substitute(dim, e);\n\t\texprs.set((offset).intValue(), substitute);\n\t}\n}'"
	 * @generated
	 */
	void substituteIndexWith(ScopDimension dim, IntExpression e);

} // ScopAccess
