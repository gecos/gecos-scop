/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.blocks.Block;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Black Box Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getReads <em>Reads</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getWrites <em>Writes</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlackBoxNode()
 * @model
 * @generated
 */
public interface ScopBlackBoxNode extends ScopStatement {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlackBoxNode_Label()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Reads</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopRead}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reads</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reads</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlackBoxNode_Reads()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopRead> getReads();

	/**
	 * Returns the value of the '<em><b>Writes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopWrite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Writes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Writes</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlackBoxNode_Writes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopWrite> getWrites();

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Block)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlackBoxNode_Body()
	 * @model containment="true"
	 * @generated
	 */
	Block getBody();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Block value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getName();'"
	 * @generated
	 */
	String getId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;toEList(java.util.Collections.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode%&gt;&gt;unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode%&gt;&gt;newArrayList(this)));'"
	 * @generated
	 */
	EList<ScopStatement> listAllStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;filter(this.eContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;.class));'"
	 * @generated
	 */
	EList<ScopAccess> listAllAccesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;filter(this.eContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;.class));'"
	 * @generated
	 */
	EList<ScopRead> listAllReads();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;filter(this.eContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;.class));'"
	 * @generated
	 */
	EList<ScopWrite> listAllWrites();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nint _childrenCount = this.getChildrenCount();\nboolean _greaterThan = (_childrenCount &gt; 0);\nif (_greaterThan)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\t&lt;%java.lang.String%&gt; _plus = (_name + \":\");\n\t&lt;%java.lang.String%&gt; _string = this.getChildren().get(0).toString();\n\t_xifexpression = (_plus + _string);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _name_1 = this.getName();\n\t_xifexpression = (_name_1 + \":()\");\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlackBoxNode%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopStatement copy();

} // ScopBlackBoxNode
