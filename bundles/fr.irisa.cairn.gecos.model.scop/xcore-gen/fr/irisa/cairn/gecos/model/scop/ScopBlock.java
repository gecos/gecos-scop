/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlock#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlock()
 * @model
 * @generated
 */
public interface ScopBlock extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlock_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopNode> getChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getChildren().contains(n);\nif (_contains)\n{\n\tthis.getChildren().remove(n);\n\tint _size = this.getChildren().size();\n\tboolean _equals = (_size == 0);\n\tif (_equals)\n\t{\n\t\tthis.getParentScop().remove(this);\n\t}\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int pos = this.getChildren().indexOf(n);\nif ((pos &gt; (-1)))\n{\n\tthis.getChildren().set(pos, _new);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void replace(ScopNode n, ScopNode _new);

} // ScopBlock
