/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.blocks.Block;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getReads <em>Reads</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getWrites <em>Writes</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlockStatement()
 * @model
 * @generated
 */
public interface ScopBlockStatement extends ScopStatement {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlockStatement_Label()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Reads</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopRead}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reads</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reads</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlockStatement_Reads()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopRead> getReads();

	/**
	 * Returns the value of the '<em><b>Writes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopWrite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Writes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Writes</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlockStatement_Writes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopWrite> getWrites();

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Block)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopBlockStatement_Body()
	 * @model containment="true"
	 * @generated
	 */
	Block getBody();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Block value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getLabel();'"
	 * @generated
	 */
	String getId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model txtDataType="fr.irisa.cairn.gecos.model.scop.String" txtUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setLabel(txt);'"
	 * @generated
	 */
	void setId(String txt);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;toEList(java.util.Collections.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlockStatement%&gt;&gt;unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlockStatement%&gt;&gt;newArrayList(this)));'"
	 * @generated
	 */
	EList<ScopStatement> listAllStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;filter(this.eContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;.class));'"
	 * @generated
	 */
	EList<ScopAccess> listAllAccesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getReads();'"
	 * @generated
	 */
	EList<ScopRead> listAllReadAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getWrites();'"
	 * @generated
	 */
	EList<ScopWrite> listAllWriteAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _id = this.getId();\nreturn (_id + \":()\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopBlockStatement%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopBlockStatement copy();

} // ScopBlockStatement
