/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.core.ISymbolUse;
import gecos.core.Symbol;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dimension</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymName <em>Sym Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopDimension()
 * @model
 * @generated
 */
public interface ScopDimension extends Variable, ISymbolUse {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopDimension_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * Returns the value of the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sym Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sym Name</em>' attribute.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopDimension_SymName()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%java.lang.String%&gt; _xblockexpression = null;\n{\n\t&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\n\tboolean _tripleNotEquals = (_symbol != null);\n\tif (_tripleNotEquals)\n\t{\n\t\treturn this.getSymbol().getName();\n\t}\n\t_xblockexpression = \"no_sym\";\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	String getSymName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol();'"
	 * @generated
	 */
	Symbol getUsedSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = this.getName();\n&lt;%java.lang.String%&gt; _plus = (_name + \":\");\n&lt;%java.lang.String%&gt; _symName = this.getSymName();\nreturn (_plus + _symName);'"
	 * @generated
	 */
	String toString();

} // ScopDimension
