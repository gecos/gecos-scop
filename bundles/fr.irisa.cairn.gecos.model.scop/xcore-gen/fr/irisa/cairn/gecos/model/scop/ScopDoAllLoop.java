/**
 */
package fr.irisa.cairn.gecos.model.scop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Do All Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopDoAllLoop()
 * @model
 * @generated
 */
public interface ScopDoAllLoop extends ScopForLoop {
} // ScopDoAllLoop
