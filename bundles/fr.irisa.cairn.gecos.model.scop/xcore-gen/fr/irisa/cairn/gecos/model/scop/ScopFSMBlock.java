/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getIterators <em>Iterators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getStart <em>Start</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getCommands <em>Commands</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMBlock()
 * @model
 * @generated
 */
public interface ScopFSMBlock extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Iterators</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterators</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterators</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMBlock_Iterators()
	 * @model
	 * @generated
	 */
	EList<ScopDimension> getIterators();

	/**
	 * Returns the value of the '<em><b>Start</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMBlock_Start()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopFSMTransition> getStart();

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMBlock_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopNode> getCommands();

	/**
	 * Returns the value of the '<em><b>Next</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMBlock_Next()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopFSMTransition> getNext();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getCommands().contains(n);\nif (_contains)\n{\n\tthis.getCommands().remove(n);\n}\nelse\n{\n\tboolean _contains_1 = this.getNext().contains(n);\n\tif (_contains_1)\n\t{\n\t\tthis.getNext().remove(n);\n\t}\n\telse\n\t{\n\t\tboolean _contains_2 = this.getStart().contains(n);\n\t\tif (_contains_2)\n\t\t{\n\t\t\tthis.getStart().remove(n);\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t\t\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n\t\t}\n\t}\n}\nint _size = this.getCommands().size();\nboolean _equals = (_size == 0);\nif (_equals)\n{\n\tthis.getParentScop().remove(this);\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='super.substitute(old, _new);\nboolean _contains = this.getIterators().contains(old);\nif (_contains)\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;, &lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;, &lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;()\n\t{\n\t\tpublic &lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt; i)\n\t\t{\n\t\t\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt; _xifexpression = null;\n\t\t\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(i, old);\n\t\t\tif (_equals)\n\t\t\t{\n\t\t\t\t_xifexpression = _new;\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\t_xifexpression = i;\n\t\t\t}\n\t\t\treturn _xifexpression;\n\t\t}\n\t};\n\tfinal &lt;%java.util.List%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; newIterators = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;toList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;, &lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;map(this.getIterators(), _function));\n\tthis.getIterators().clear();\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _iterators = this.getIterators();\n\t&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;addAll(_iterators, newIterators);\n}'"
	 * @generated
	 */
	void substitute(ScopDimension old, ScopDimension _new);

} // ScopFSMBlock
