/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getCommands <em>Commands</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMState()
 * @model
 * @generated
 */
public interface ScopFSMState extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Domain</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraintSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMState_Domain()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntConstraintSystem> getDomain();

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMState_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopNode> getCommands();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMState_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopFSMTransition> getTransitions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _parentScop = this.getParentScop();\nreturn ((&lt;%fr.irisa.cairn.gecos.model.scop.ScopFSMBlock%&gt;) _parentScop);'"
	 * @generated
	 */
	ScopFSMBlock getParentFSM();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%fr.irisa.cairn.gecos.model.scop.ScopFSMBlock%&gt; fsm = this.getParentFSM();\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; l = fsm.listAllEnclosingIterators();\n\tl.addAll(0, fsm.getIterators());\n\t_xblockexpression = l;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<ScopDimension> listAllEnclosingIterators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getTransitions().contains(n);\nif (_contains)\n{\n\tthis.getTransitions().remove(n);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

} // ScopFSMState
