/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNextIteration <em>Next Iteration</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMTransition()
 * @model
 * @generated
 */
public interface ScopFSMTransition extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Domain</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraintSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMTransition_Domain()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntConstraintSystem> getDomain();

	/**
	 * Returns the value of the '<em><b>Next Iteration</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Iteration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Iteration</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMTransition_NextIteration()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getNextIteration();

	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference.
	 * @see #setNext(ScopFSMState)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopFSMTransition_Next()
	 * @model
	 * @generated
	 */
	ScopFSMState getNext();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNext <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(ScopFSMState value);

} // ScopFSMTransition
