/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage
 * @generated
 */
public interface ScopFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScopFactory eINSTANCE = fr.irisa.cairn.gecos.model.scop.impl.ScopFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dimension</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dimension</em>'.
	 * @generated
	 */
	ScopDimension createScopDimension();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	ScopParameter createScopParameter();

	/**
	 * Returns a new object of class '<em>Gecos Scop Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gecos Scop Block</em>'.
	 * @generated
	 */
	GecosScopBlock createGecosScopBlock();

	/**
	 * Returns a new object of class '<em>Guard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Guard</em>'.
	 * @generated
	 */
	ScopGuard createScopGuard();

	/**
	 * Returns a new object of class '<em>Index Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Index Expression</em>'.
	 * @generated
	 */
	ScopIndexExpression createScopIndexExpression();

	/**
	 * Returns a new object of class '<em>Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access</em>'.
	 * @generated
	 */
	ScopAccess createScopAccess();

	/**
	 * Returns a new object of class '<em>Read</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Read</em>'.
	 * @generated
	 */
	ScopRead createScopRead();

	/**
	 * Returns a new object of class '<em>Region Read</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Region Read</em>'.
	 * @generated
	 */
	ScopRegionRead createScopRegionRead();

	/**
	 * Returns a new object of class '<em>Region Write</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Region Write</em>'.
	 * @generated
	 */
	ScopRegionWrite createScopRegionWrite();

	/**
	 * Returns a new object of class '<em>Write</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Write</em>'.
	 * @generated
	 */
	ScopWrite createScopWrite();

	/**
	 * Returns a new object of class '<em>Int Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Expression</em>'.
	 * @generated
	 */
	ScopIntExpression createScopIntExpression();

	/**
	 * Returns a new object of class '<em>Int Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Constraint</em>'.
	 * @generated
	 */
	ScopIntConstraint createScopIntConstraint();

	/**
	 * Returns a new object of class '<em>Int Constraint System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Constraint System</em>'.
	 * @generated
	 */
	ScopIntConstraintSystem createScopIntConstraintSystem();

	/**
	 * Returns a new object of class '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Statement</em>'.
	 * @generated
	 */
	ScopStatement createScopStatement();

	/**
	 * Returns a new object of class '<em>Instruction Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instruction Statement</em>'.
	 * @generated
	 */
	ScopInstructionStatement createScopInstructionStatement();

	/**
	 * Returns a new object of class '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block</em>'.
	 * @generated
	 */
	ScopBlock createScopBlock();

	/**
	 * Returns a new object of class '<em>For Loop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Loop</em>'.
	 * @generated
	 */
	ScopForLoop createScopForLoop();

	/**
	 * Returns a new object of class '<em>Do All Loop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Do All Loop</em>'.
	 * @generated
	 */
	ScopDoAllLoop createScopDoAllLoop();

	/**
	 * Returns a new object of class '<em>scop Async Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>scop Async Block</em>'.
	 * @generated
	 */
	scopAsyncBlock createscopAsyncBlock();

	/**
	 * Returns a new object of class '<em>FSM Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Block</em>'.
	 * @generated
	 */
	ScopFSMBlock createScopFSMBlock();

	/**
	 * Returns a new object of class '<em>FSM Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Transition</em>'.
	 * @generated
	 */
	ScopFSMTransition createScopFSMTransition();

	/**
	 * Returns a new object of class '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM State</em>'.
	 * @generated
	 */
	ScopFSMState createScopFSMState();

	/**
	 * Returns a new object of class '<em>Unexpanded Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unexpanded Statement</em>'.
	 * @generated
	 */
	ScopUnexpandedStatement createScopUnexpandedStatement();

	/**
	 * Returns a new object of class '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Statement</em>'.
	 * @generated
	 */
	ScopBlockStatement createScopBlockStatement();

	/**
	 * Returns a new object of class '<em>Def Use Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Def Use Edge</em>'.
	 * @generated
	 */
	DefUseEdge createDefUseEdge();

	/**
	 * Returns a new object of class '<em>Use Def Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Def Edge</em>'.
	 * @generated
	 */
	UseDefEdge createUseDefEdge();

	/**
	 * Returns a new object of class '<em>WAW Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>WAW Edge</em>'.
	 * @generated
	 */
	WAWEdge createWAWEdge();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ScopPackage getScopPackage();

} //ScopFactory
