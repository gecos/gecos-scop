/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getIterator <em>Iterator</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getLB <em>LB</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getUB <em>UB</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getStride <em>Stride</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop()
 * @model
 * @generated
 */
public interface ScopForLoop extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' reference.
	 * @see #setIterator(ScopDimension)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop_Iterator()
	 * @model
	 * @generated
	 */
	ScopDimension getIterator();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getIterator <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' reference.
	 * @see #getIterator()
	 * @generated
	 */
	void setIterator(ScopDimension value);

	/**
	 * Returns the value of the '<em><b>LB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>LB</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LB</em>' containment reference.
	 * @see #setLB(IntExpression)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop_LB()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getLB();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getLB <em>LB</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LB</em>' containment reference.
	 * @see #getLB()
	 * @generated
	 */
	void setLB(IntExpression value);

	/**
	 * Returns the value of the '<em><b>UB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>UB</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UB</em>' containment reference.
	 * @see #setUB(IntExpression)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop_UB()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getUB();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getUB <em>UB</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UB</em>' containment reference.
	 * @see #getUB()
	 * @generated
	 */
	void setUB(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Stride</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stride</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stride</em>' containment reference.
	 * @see #setStride(IntExpression)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop_Stride()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getStride();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getStride <em>Stride</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stride</em>' containment reference.
	 * @see #getStride()
	 * @generated
	 */
	void setStride(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(ScopNode)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopForLoop_Body()
	 * @model containment="true"
	 * @generated
	 */
	ScopNode getBody();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(ScopNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _body = this.getBody();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_body, n);\nif (_equals)\n{\n\tthis.setBody(null);\n\tthis.getParentScop().remove(this);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _body = this.getBody();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_body, n);\nif (_equals)\n{\n\tthis.setBody(_new);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n}'"
	 * @generated
	 */
	void replace(ScopNode n, ScopNode _new);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _parentScop = this.getParentScop();\nboolean _tripleNotEquals = (_parentScop != null);\nif (_tripleNotEquals)\n{\n\tint _numberOfEnclosingDimension = this.getParentScop().getNumberOfEnclosingDimension();\n\treturn (1 + _numberOfEnclosingDimension);\n}\nelse\n{\n\treturn 1;\n}'"
	 * @generated
	 */
	int getNumberOfEnclosingDimension();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _numberOfEnclosedDimension = this.getBody().getNumberOfEnclosedDimension();\nreturn (_numberOfEnclosedDimension + 1);'"
	 * @generated
	 */
	int getNumberOfEnclosedDimension();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='super.substitute(old, _new);\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt; _iterator = this.getIterator();\nboolean _tripleEquals = (_iterator == old);\nif (_tripleEquals)\n{\n\tthis.setIterator(_new);\n}'"
	 * @generated
	 */
	void substitute(ScopDimension old, ScopDimension _new);

} // ScopForLoop
