/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getPredicate <em>Predicate</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getThenNode <em>Then Node</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getElseNode <em>Else Node</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopGuard()
 * @model
 * @generated
 */
public interface ScopGuard extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Predicate</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraintSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopGuard_Predicate()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntConstraintSystem> getPredicate();

	/**
	 * Returns the value of the '<em><b>Then Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Node</em>' containment reference.
	 * @see #setThenNode(ScopNode)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopGuard_ThenNode()
	 * @model containment="true"
	 * @generated
	 */
	ScopNode getThenNode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getThenNode <em>Then Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Node</em>' containment reference.
	 * @see #getThenNode()
	 * @generated
	 */
	void setThenNode(ScopNode value);

	/**
	 * Returns the value of the '<em><b>Else Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Node</em>' containment reference.
	 * @see #setElseNode(ScopNode)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopGuard_ElseNode()
	 * @model containment="true"
	 * @generated
	 */
	ScopNode getElseNode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getElseNode <em>Else Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Node</em>' containment reference.
	 * @see #getElseNode()
	 * @generated
	 */
	void setElseNode(ScopNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _thenNode = this.getThenNode();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_thenNode, n);\nif (_equals)\n{\n\tthis.setThenNode(null);\n}\nelse\n{\n\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _elseNode = this.getElseNode();\n\tboolean _equals_1 = &lt;%com.google.common.base.Objects%&gt;.equal(_elseNode, n);\n\tif (_equals_1)\n\t{\n\t\tthis.setElseNode(null);\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n\t}\n}'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _thenNode = this.getThenNode();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_thenNode, n);\nif (_equals)\n{\n\tthis.setThenNode(_new);\n}\nelse\n{\n\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _elseNode = this.getElseNode();\n\tboolean _equals_1 = &lt;%com.google.common.base.Objects%&gt;.equal(_elseNode, n);\n\tif (_equals_1)\n\t{\n\t\tthis.setElseNode(_new);\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _plus = (n + \" is not a children of \");\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);\n\t}\n}'"
	 * @generated
	 */
	void replace(ScopNode n, ScopNode _new);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _elseNode = this.getElseNode();\nboolean _tripleNotEquals = (_elseNode != null);\nif (_tripleNotEquals)\n{\n\treturn &lt;%java.lang.Math%&gt;.max(this.getThenNode().getNumberOfEnclosedDimension(), this.getElseNode().getNumberOfEnclosedDimension());\n}\nelse\n{\n\treturn this.getThenNode().getNumberOfEnclosedDimension();\n}'"
	 * @generated
	 */
	int maxDepth();

} // ScopGuard
