/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Index Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopIndexExpression#getIndexExpressions <em>Index Expressions</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIndexExpression()
 * @model
 * @generated
 */
public interface ScopIndexExpression extends ScopNode, Instruction {
	/**
	 * Returns the value of the '<em><b>Index Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Expressions</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIndexExpression_IndexExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getIndexExpressions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.Instruction%&gt; _root = this.getRoot();\nreturn ((&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;) _root);'"
	 * @generated
	 */
	ScopNode getParentScop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopIndexExpression%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopIndexExpression copy();

} // ScopIndexExpression
