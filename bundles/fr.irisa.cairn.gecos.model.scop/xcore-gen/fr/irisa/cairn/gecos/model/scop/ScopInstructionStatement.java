/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.instrs.GenericInstruction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instruction Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopInstructionStatement()
 * @model
 * @generated
 */
public interface ScopInstructionStatement extends GenericInstruction, ScopStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getName();'"
	 * @generated
	 */
	String getId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model txtDataType="fr.irisa.cairn.gecos.model.scop.String" txtUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setName(txt);'"
	 * @generated
	 */
	void setId(String txt);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;toEList(java.util.Collections.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement%&gt;&gt;unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement%&gt;&gt;newArrayList(this)));'"
	 * @generated
	 */
	EList<ScopStatement> listAllStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterators%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;filter(this.eAllContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;.class));'"
	 * @generated
	 */
	EList<ScopAccess> listAllAccesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterators%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;filter(this.eAllContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;.class));'"
	 * @generated
	 */
	EList<ScopRead> listAllReadAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterators%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;filter(this.eAllContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;.class));'"
	 * @generated
	 */
	EList<ScopWrite> listAllWriteAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nint _childrenCount = this.getChildrenCount();\nboolean _greaterThan = (_childrenCount &gt; 0);\nif (_greaterThan)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\t&lt;%java.lang.String%&gt; _plus = (_name + \":\");\n\t&lt;%java.lang.String%&gt; _string = this.getChildren().get(0).toString();\n\t_xifexpression = (_plus + _string);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _name_1 = this.getName();\n\t_xifexpression = (_name_1 + \":()\");\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopInstructionStatement copy();

} // ScopInstructionStatement
