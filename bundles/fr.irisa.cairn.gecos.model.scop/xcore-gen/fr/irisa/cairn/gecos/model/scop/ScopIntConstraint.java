/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.instrs.Instruction;
import org.polymodel.algebra.IntConstraint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraint#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIntConstraint()
 * @model
 * @generated
 */
public interface ScopIntConstraint extends Instruction {
	/**
	 * Returns the value of the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' containment reference.
	 * @see #setGuard(IntConstraint)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIntConstraint_Guard()
	 * @model containment="true"
	 * @generated
	 */
	IntConstraint getGuard();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraint#getGuard <em>Guard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' containment reference.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(IntConstraint value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this.getGuard(), &lt;%org.polymodel.algebra.OUTPUT_FORMAT%&gt;.ISL);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopIntConstraint%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopIntConstraint copy();

} // ScopIntConstraint
