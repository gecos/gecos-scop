/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.instrs.Instruction;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Constraint System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem#getSystem <em>System</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIntConstraintSystem()
 * @model
 * @generated
 */
public interface ScopIntConstraintSystem extends Instruction {
	/**
	 * Returns the value of the '<em><b>System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System</em>' containment reference.
	 * @see #setSystem(IntConstraintSystem)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopIntConstraintSystem_System()
	 * @model containment="true"
	 * @generated
	 */
	IntConstraintSystem getSystem();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem#getSystem <em>System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System</em>' containment reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(IntConstraintSystem value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this.getSystem(), &lt;%org.polymodel.algebra.OUTPUT_FORMAT%&gt;.ISL);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopIntConstraintSystem copy();

} // ScopIntConstraintSystem
