/**
 */
package fr.irisa.cairn.gecos.model.scop;

import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;

import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import gecos.annotations.AnnotatedElement;
import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopNode#getTransform <em>Transform</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopNode()
 * @model abstract="true"
 * @generated
 */
public interface ScopNode extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transform</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transform</em>' containment reference.
	 * @see #setTransform(ScopTransform)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopNode_Transform()
	 * @model containment="true"
	 * @generated
	 */
	ScopTransform getTransform();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopNode#getTransform <em>Transform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transform</em>' containment reference.
	 * @see #getTransform()
	 * @generated
	 */
	void setTransform(ScopTransform value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.ecore.EObject%&gt; econtainer = this.eContainer();\nif ((econtainer instanceof &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;))\n{\n\treturn ((&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;)econtainer);\n}\nreturn null;'"
	 * @generated
	 */
	ScopNode getParentScop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _parentScop = this.getParentScop();\nboolean _tripleEquals = (_parentScop == null);\nif (_tripleEquals)\n{\n\treturn this;\n}\nelse\n{\n\treturn this.getParentScop().getScopRoot();\n}'"
	 * @generated
	 */
	ScopNode getScopRoot();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="fr.irisa.cairn.gecos.model.scop.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt; stmt)\n\t{\n\t\t&lt;%java.lang.String%&gt; _id = stmt.getId();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_id, name));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;findFirst(this.listAllStatements(), _function);'"
	 * @generated
	 */
	ScopStatement getStatement(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; n)\n\t{\n\t\treturn n.listAllStatements();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;&gt;map(this.listChildrenScopNodes(), _function)));'"
	 * @generated
	 */
	EList<ScopStatement> listAllStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; n)\n\t{\n\t\treturn n.listAllReadAccess();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;&gt;&gt;map(this.listChildrenScopNodes(), _function)));'"
	 * @generated
	 */
	EList<ScopRead> listAllReadAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; n)\n\t{\n\t\treturn n.listAllAccesses();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;&gt;map(this.listChildrenScopNodes(), _function)));'"
	 * @generated
	 */
	EList<ScopAccess> listAllAccesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Symbol%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt; a)\n\t{\n\t\treturn a.getSym();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toEList(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toSet(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;map(this.listAllAccesses(), _function)));'"
	 * @generated
	 */
	EList<Symbol> listAllReferencedSymbols();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; n)\n\t{\n\t\treturn n.listAllWriteAccess();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;&gt;&gt;map(this.listChildrenScopNodes(), _function)));'"
	 * @generated
	 */
	EList<ScopWrite> listAllWriteAccess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;&gt;filter(this.eContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;.class));'"
	 * @generated
	 */
	EList<ScopNode> listChildrenScopNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterators%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;&gt;filter(this.eAllContents(), &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;.class));'"
	 * @generated
	 */
	EList<ScopNode> listAllChildredScopNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  List root scop parameters as well as parameters of this
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;();\n\tlist.addAll(this.listRootParameters());\n\tlist.addAll(this.listAllEnclosingIterators());\n\t_xblockexpression = list;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<ScopDimension> listAllParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  List all parameters from the root scop
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _xblockexpression = null;\n{\n\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _scopRoot = this.getScopRoot();\n\tfinal &lt;%fr.irisa.cairn.gecos.model.scop.GecosScopBlock%&gt; blk = ((&lt;%fr.irisa.cairn.gecos.model.scop.GecosScopBlock%&gt;) _scopRoot);\n\t_xblockexpression = blk.getParameters();\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<ScopDimension> listRootParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  List the iterators contained by the root scop
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _xblockexpression = null;\n{\n\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _scopRoot = this.getScopRoot();\n\tfinal &lt;%fr.irisa.cairn.gecos.model.scop.GecosScopBlock%&gt; blk = ((&lt;%fr.irisa.cairn.gecos.model.scop.GecosScopBlock%&gt;) _scopRoot);\n\t_xblockexpression = blk.getIterators();\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<ScopDimension> listRootIterators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective%&gt;&gt; _xifexpression = null;\n&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScopTransform%&gt; _transform = this.getTransform();\nboolean _tripleNotEquals = (_transform != null);\nif (_tripleNotEquals)\n{\n\t_xifexpression = this.getTransform().getCommands();\n}\nelse\n{\n\t_xifexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective%&gt;&gt;toEList(java.util.Collections.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective%&gt;&gt;unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective%&gt;&gt;newArrayList()));\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	EList<ScopTransformDirective> listNodeDirectives();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  List iterators enclosing this including relativeRoot
	 * <!-- end-model-doc -->
	 * @model unique="false" relativeRootUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;();\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; parent = relativeRoot;\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _parentScop = relativeRoot.getParentScop();\nboolean _tripleNotEquals = (_parentScop != null);\nif (_tripleNotEquals)\n{\n\tparent = relativeRoot.getParentScop();\n}\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopForLoop%&gt; forLoop = this.getEnclosingForLoop(parent);\nwhile ((forLoop != null))\n{\n\t{\n\t\tlist.add(forLoop.getIterator());\n\t\tforLoop = forLoop.getEnclosingForLoop(parent);\n\t}\n}\nreturn &lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;reverse(list);'"
	 * @generated
	 */
	EList<ScopDimension> listAllEnclosingIterators(ScopNode relativeRoot);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.listAllEnclosingIterators(this.getScopRoot());'"
	 * @generated
	 */
	EList<ScopDimension> listAllEnclosingIterators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopDimension%&gt;&gt;();\n\tlist.addAll(this.listRootIterators());\n\tlist.removeAll(this.listAllEnclosingIterators());\n\t_xblockexpression = list;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<ScopDimension> listAllFreeIterators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Return the immediate parent enclosing loop
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getEnclosingForLoop(this.getScopRoot());'"
	 * @generated
	 */
	ScopForLoop getEnclosingForLoop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Return the immediate parent enclosing loop contained inside relativeRoot
	 * <!-- end-model-doc -->
	 * @model unique="false" relativeRootUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(this, relativeRoot);\nif (_equals)\n{\n\treturn null;\n}\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; parent = this.getParentScop();\nwhile (((parent != null) &amp;&amp; (!&lt;%com.google.common.base.Objects%&gt;.equal(parent, relativeRoot))))\n{\n\t{\n\t\tif ((parent instanceof &lt;%fr.irisa.cairn.gecos.model.scop.ScopForLoop%&gt;))\n\t\t{\n\t\t\treturn ((&lt;%fr.irisa.cairn.gecos.model.scop.ScopForLoop%&gt;)parent);\n\t\t}\n\t\tparent = parent.getParentScop();\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	ScopForLoop getEnclosingForLoop(ScopNode relativeRoot);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Not implemented for class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void remove(ScopNode n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Not implemented for class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void replace(ScopNode n, ScopNode _new);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldUnique="false" _newUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.Iterable%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; _iterable = &lt;%org.eclipse.xtext.xbase.lib.IteratorExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;toIterable(&lt;%com.google.common.collect.Iterators%&gt;.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;filter(this.eAllContents(), &lt;%org.polymodel.algebra.IntExpression%&gt;.class));\nfor (final &lt;%org.polymodel.algebra.IntExpression%&gt; exp : _iterable)\n{\n\t{\n\t\tfinal &lt;%org.polymodel.algebra.IntExpression%&gt; newExp = exp.substitute(old, _new);\n\t\t&lt;%fr.irisa.cairn.tools.ecore.query.EMFUtils%&gt;.substituteByNewObjectInContainer(exp, newExp);\n\t}\n}'"
	 * @generated
	 */
	void substitute(ScopDimension old, ScopDimension _new);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _xifexpression = (int) 0;\n&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; _parentScop = this.getParentScop();\nboolean _tripleNotEquals = (_parentScop != null);\nif (_tripleNotEquals)\n{\n\t_xifexpression = this.getParentScop().getNumberOfEnclosingDimension();\n}\nelse\n{\n\t_xifexpression = 0;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	int getNumberOfEnclosingDimension();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.listChildrenScopNodes().size();\nboolean _equals = (_size == 0);\nif (_equals)\n{\n\treturn 0;\n}\nelse\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%java.lang.Integer%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%java.lang.Integer%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Integer%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt; n)\n\t\t{\n\t\t\treturn &lt;%java.lang.Integer%&gt;.valueOf(n.getNumberOfEnclosedDimension());\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Integer%&gt; apply(final &lt;%java.lang.Integer%&gt; p1, final &lt;%java.lang.Integer%&gt; p2)\n\t\t{\n\t\t\treturn &lt;%java.lang.Integer%&gt;.valueOf(&lt;%java.lang.Math%&gt;.max((p1).intValue(), (p2).intValue()));\n\t\t}\n\t};\n\treturn (int) &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Integer%&gt;&gt;reduce(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;, &lt;%java.lang.Integer%&gt;&gt;map(this.listChildrenScopNodes(), _function), _function_1);\n}'"
	 * @generated
	 */
	int getNumberOfEnclosedDimension();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopNode%&gt;&gt;copy(this);'"
	 * @generated
	 */
	ScopNode copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter%&gt;.print(this).toString();'"
	 * @generated
	 */
	String toString();

	


} // ScopNode
