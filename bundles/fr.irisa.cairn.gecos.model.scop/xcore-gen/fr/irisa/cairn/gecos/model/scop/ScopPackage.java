/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.annotations.AnnotationsPackage;
import gecos.instrs.InstrsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.polymodel.algebra.AlgebraPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.ScopFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' modelDirectory='/fr.irisa.cairn.gecos.model.scop/xcore-gen' basePackage='fr.irisa.cairn.gecos.model'"
 * @generated
 */
public interface ScopPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scop";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/scop";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scop";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScopPackage eINSTANCE = fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopNodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopNodeImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopNode()
	 * @generated
	 */
	int SCOP_NODE = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_NODE__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_NODE__TRANSFORM = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_NODE_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopDimensionImpl <em>Dimension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopDimensionImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopDimension()
	 * @generated
	 */
	int SCOP_DIMENSION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DIMENSION__NAME = AlgebraPackage.VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DIMENSION__SYMBOL = AlgebraPackage.VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DIMENSION__SYM_NAME = AlgebraPackage.VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DIMENSION_FEATURE_COUNT = AlgebraPackage.VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopParameterImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopParameter()
	 * @generated
	 */
	int SCOP_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_PARAMETER__NAME = SCOP_DIMENSION__NAME;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_PARAMETER__SYMBOL = SCOP_DIMENSION__SYMBOL;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_PARAMETER__SYM_NAME = SCOP_DIMENSION__SYM_NAME;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_PARAMETER_FEATURE_COUNT = SCOP_DIMENSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl <em>Gecos Scop Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getGecosScopBlock()
	 * @generated
	 */
	int GECOS_SCOP_BLOCK = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__PARENT = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__NUMBER = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__INSTRUCTIONS = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__DEF_EDGES = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__OUT_EDGES = SCOP_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__USE_EDGES = SCOP_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__IN_EDGES = SCOP_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__LABEL = SCOP_NODE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__FLAGS = SCOP_NODE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__SCOPE = SCOP_NODE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__NAME = SCOP_NODE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Live In Symbols</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS = SCOP_NODE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Live Out Symbols</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS = SCOP_NODE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__PARAMETERS = SCOP_NODE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Iterators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__ITERATORS = SCOP_NODE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Context</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__CONTEXT = SCOP_NODE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK__ROOT = SCOP_NODE_FEATURE_COUNT + 16;

	/**
	 * The number of structural features of the '<em>Gecos Scop Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SCOP_BLOCK_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 17;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl <em>Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopGuard()
	 * @generated
	 */
	int SCOP_GUARD = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD__PREDICATE = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD__THEN_NODE = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD__ELSE_NODE = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_GUARD_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl <em>Index Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIndexExpression()
	 * @generated
	 */
	int SCOP_INDEX_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INDEX_EXPRESSION__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INDEX_EXPRESSION__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Index Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INDEX_EXPRESSION_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl <em>Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopAccess()
	 * @generated
	 */
	int SCOP_ACCESS = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__ANNOTATIONS = SCOP_INDEX_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__TRANSFORM = SCOP_INDEX_EXPRESSION__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__INTERNAL_CACHED_TYPE = SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__INDEX_EXPRESSIONS = SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__SYM = SCOP_INDEX_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS__SYM_NAME = SCOP_INDEX_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ACCESS_FEATURE_COUNT = SCOP_INDEX_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopReadImpl <em>Read</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopReadImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRead()
	 * @generated
	 */
	int SCOP_READ = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__ANNOTATIONS = SCOP_ACCESS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__TRANSFORM = SCOP_ACCESS__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__INTERNAL_CACHED_TYPE = SCOP_ACCESS__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__INDEX_EXPRESSIONS = SCOP_ACCESS__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__SYM = SCOP_ACCESS__SYM;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ__SYM_NAME = SCOP_ACCESS__SYM_NAME;

	/**
	 * The number of structural features of the '<em>Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_READ_FEATURE_COUNT = SCOP_ACCESS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionReadImpl <em>Region Read</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopRegionReadImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRegionRead()
	 * @generated
	 */
	int SCOP_REGION_READ = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__ANNOTATIONS = SCOP_READ__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__TRANSFORM = SCOP_READ__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__INTERNAL_CACHED_TYPE = SCOP_READ__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__INDEX_EXPRESSIONS = SCOP_READ__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__SYM = SCOP_READ__SYM;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__SYM_NAME = SCOP_READ__SYM_NAME;

	/**
	 * The feature id for the '<em><b>Existentials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__EXISTENTIALS = SCOP_READ_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Region</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ__REGION = SCOP_READ_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Region Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_READ_FEATURE_COUNT = SCOP_READ_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl <em>Region Write</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRegionWrite()
	 * @generated
	 */
	int SCOP_REGION_WRITE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__ANNOTATIONS = SCOP_READ__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__TRANSFORM = SCOP_READ__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__INTERNAL_CACHED_TYPE = SCOP_READ__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__INDEX_EXPRESSIONS = SCOP_READ__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__SYM = SCOP_READ__SYM;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__SYM_NAME = SCOP_READ__SYM_NAME;

	/**
	 * The feature id for the '<em><b>Existentials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__EXISTENTIALS = SCOP_READ_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Region</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE__REGION = SCOP_READ_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Region Write</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_REGION_WRITE_FEATURE_COUNT = SCOP_READ_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopWriteImpl <em>Write</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopWriteImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopWrite()
	 * @generated
	 */
	int SCOP_WRITE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__ANNOTATIONS = SCOP_ACCESS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__TRANSFORM = SCOP_ACCESS__TRANSFORM;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__INTERNAL_CACHED_TYPE = SCOP_ACCESS__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__INDEX_EXPRESSIONS = SCOP_ACCESS__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Sym</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__SYM = SCOP_ACCESS__SYM;

	/**
	 * The feature id for the '<em><b>Sym Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE__SYM_NAME = SCOP_ACCESS__SYM_NAME;

	/**
	 * The number of structural features of the '<em>Write</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_WRITE_FEATURE_COUNT = SCOP_ACCESS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntExpressionImpl <em>Int Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntExpressionImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntExpression()
	 * @generated
	 */
	int SCOP_INT_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_EXPRESSION__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_EXPRESSION__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_EXPRESSION__EXPR = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_EXPRESSION_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintImpl <em>Int Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntConstraint()
	 * @generated
	 */
	int SCOP_INT_CONSTRAINT = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT__GUARD = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintSystemImpl <em>Int Constraint System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintSystemImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntConstraintSystem()
	 * @generated
	 */
	int SCOP_INT_CONSTRAINT_SYSTEM = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT_SYSTEM__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT_SYSTEM__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT_SYSTEM__SYSTEM = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Constraint System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INT_CONSTRAINT_SYSTEM_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopStatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopStatementImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopStatement()
	 * @generated
	 */
	int SCOP_STATEMENT = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_STATEMENT__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_STATEMENT__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_STATEMENT_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopInstructionStatementImpl <em>Instruction Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopInstructionStatementImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopInstructionStatement()
	 * @generated
	 */
	int SCOP_INSTRUCTION_STATEMENT = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT__ANNOTATIONS = InstrsPackage.GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT__INTERNAL_CACHED_TYPE = InstrsPackage.GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT__CHILDREN = InstrsPackage.GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT__NAME = InstrsPackage.GENERIC_INSTRUCTION__NAME;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT__TRANSFORM = InstrsPackage.GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Instruction Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_INSTRUCTION_STATEMENT_FEATURE_COUNT = InstrsPackage.GENERIC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopBlockImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopBlock()
	 * @generated
	 */
	int SCOP_BLOCK = 17;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl <em>For Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopForLoop()
	 * @generated
	 */
	int SCOP_FOR_LOOP = 18;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopDoAllLoopImpl <em>Do All Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopDoAllLoopImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopDoAllLoop()
	 * @generated
	 */
	int SCOP_DO_ALL_LOOP = 19;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.scopAsyncBlockImpl <em>scop Async Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.scopAsyncBlockImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getscopAsyncBlock()
	 * @generated
	 */
	int SCOP_ASYNC_BLOCK = 20;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl <em>FSM Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMBlock()
	 * @generated
	 */
	int SCOP_FSM_BLOCK = 21;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl <em>FSM Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMTransition()
	 * @generated
	 */
	int SCOP_FSM_TRANSITION = 22;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl <em>FSM State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMState()
	 * @generated
	 */
	int SCOP_FSM_STATE = 23;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedNodeImpl <em>Unexpanded Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedNodeImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopUnexpandedNode()
	 * @generated
	 */
	int SCOP_UNEXPANDED_NODE = 24;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl <em>Unexpanded Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopUnexpandedStatement()
	 * @generated
	 */
	int SCOP_UNEXPANDED_STATEMENT = 25;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl <em>Block Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopBlockStatement()
	 * @generated
	 */
	int SCOP_BLOCK_STATEMENT = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__ANNOTATIONS = SCOP_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__TRANSFORM = SCOP_STATEMENT__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__LABEL = SCOP_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reads</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__READS = SCOP_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Writes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__WRITES = SCOP_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT__BODY = SCOP_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Block Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_STATEMENT_FEATURE_COUNT = SCOP_STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK__CHILDREN = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_BLOCK_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__ITERATOR = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>LB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__LB = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>UB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__UB = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Stride</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__STRIDE = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP__BODY = SCOP_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>For Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FOR_LOOP_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__ANNOTATIONS = SCOP_FOR_LOOP__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__TRANSFORM = SCOP_FOR_LOOP__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__ITERATOR = SCOP_FOR_LOOP__ITERATOR;

	/**
	 * The feature id for the '<em><b>LB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__LB = SCOP_FOR_LOOP__LB;

	/**
	 * The feature id for the '<em><b>UB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__UB = SCOP_FOR_LOOP__UB;

	/**
	 * The feature id for the '<em><b>Stride</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__STRIDE = SCOP_FOR_LOOP__STRIDE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP__BODY = SCOP_FOR_LOOP__BODY;

	/**
	 * The number of structural features of the '<em>Do All Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_DO_ALL_LOOP_FEATURE_COUNT = SCOP_FOR_LOOP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ASYNC_BLOCK__ANNOTATIONS = SCOP_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ASYNC_BLOCK__TRANSFORM = SCOP_BLOCK__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ASYNC_BLOCK__CHILDREN = SCOP_BLOCK__CHILDREN;

	/**
	 * The number of structural features of the '<em>scop Async Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_ASYNC_BLOCK_FEATURE_COUNT = SCOP_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Iterators</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__ITERATORS = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__START = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__COMMANDS = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Next</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK__NEXT = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_BLOCK_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION__DOMAIN = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Next Iteration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION__NEXT_ITERATION = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION__NEXT = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_TRANSITION_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE__DOMAIN = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE__COMMANDS = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE__TRANSITIONS = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_FSM_STATE_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE__ANNOTATIONS = SCOP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE__TRANSFORM = SCOP_NODE__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE__SCHEDULE = SCOP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE__DOMAIN = SCOP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Existentials</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE__EXISTENTIALS = SCOP_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Unexpanded Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_NODE_FEATURE_COUNT = SCOP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__ANNOTATIONS = SCOP_INSTRUCTION_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__INTERNAL_CACHED_TYPE = SCOP_INSTRUCTION_STATEMENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__CHILDREN = SCOP_INSTRUCTION_STATEMENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__NAME = SCOP_INSTRUCTION_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Transform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__TRANSFORM = SCOP_INSTRUCTION_STATEMENT__TRANSFORM;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__SCHEDULE = SCOP_INSTRUCTION_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__DOMAIN = SCOP_INSTRUCTION_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Existentials</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS = SCOP_INSTRUCTION_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Unexpanded Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_UNEXPANDED_STATEMENT_FEATURE_COUNT = SCOP_INSTRUCTION_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.DefUseEdgeImpl <em>Def Use Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.DefUseEdgeImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getDefUseEdge()
	 * @generated
	 */
	int DEF_USE_EDGE = 26;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEF_USE_EDGE__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Sinks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEF_USE_EDGE__SINKS = 1;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEF_USE_EDGE__DEPENDENCY = 2;

	/**
	 * The number of structural features of the '<em>Def Use Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEF_USE_EDGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.UseDefEdgeImpl <em>Use Def Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.UseDefEdgeImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getUseDefEdge()
	 * @generated
	 */
	int USE_DEF_EDGE = 27;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_DEF_EDGE__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_DEF_EDGE__SINK = 1;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_DEF_EDGE__DEPENDENCY = 2;

	/**
	 * The number of structural features of the '<em>Use Def Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_DEF_EDGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.impl.WAWEdgeImpl <em>WAW Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.WAWEdgeImpl
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getWAWEdge()
	 * @generated
	 */
	int WAW_EDGE = 28;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAW_EDGE__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Sinks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAW_EDGE__SINKS = 1;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAW_EDGE__DEPENDENCY = 2;

	/**
	 * The number of structural features of the '<em>WAW Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAW_EDGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getString()
	 * @generated
	 */
	int STRING = 29;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getint()
	 * @generated
	 */
	int INT = 30;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopNode
	 * @generated
	 */
	EClass getScopNode();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopNode#getTransform <em>Transform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transform</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopNode#getTransform()
	 * @see #getScopNode()
	 * @generated
	 */
	EReference getScopNode_Transform();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dimension</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDimension
	 * @generated
	 */
	EClass getScopDimension();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymbol()
	 * @see #getScopDimension()
	 * @generated
	 */
	EReference getScopDimension_Symbol();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymName <em>Sym Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sym Name</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDimension#getSymName()
	 * @see #getScopDimension()
	 * @generated
	 */
	EAttribute getScopDimension_SymName();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopParameter
	 * @generated
	 */
	EClass getScopParameter();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock <em>Gecos Scop Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos Scop Block</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock
	 * @generated
	 */
	EClass getGecosScopBlock();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getName()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EAttribute getGecosScopBlock_Name();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveInSymbols <em>Live In Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Live In Symbols</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveInSymbols()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_LiveInSymbols();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveOutSymbols <em>Live Out Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Live Out Symbols</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getLiveOutSymbols()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_LiveOutSymbols();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getParameters()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_Parameters();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getIterators <em>Iterators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iterators</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getIterators()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_Iterators();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Context</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getContext()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_Context();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock#getRoot()
	 * @see #getGecosScopBlock()
	 * @generated
	 */
	EReference getGecosScopBlock_Root();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopGuard
	 * @generated
	 */
	EClass getScopGuard();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getPredicate <em>Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Predicate</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopGuard#getPredicate()
	 * @see #getScopGuard()
	 * @generated
	 */
	EReference getScopGuard_Predicate();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getThenNode <em>Then Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Node</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopGuard#getThenNode()
	 * @see #getScopGuard()
	 * @generated
	 */
	EReference getScopGuard_ThenNode();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard#getElseNode <em>Else Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Node</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopGuard#getElseNode()
	 * @see #getScopGuard()
	 * @generated
	 */
	EReference getScopGuard_ElseNode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopIndexExpression <em>Index Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Index Expression</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIndexExpression
	 * @generated
	 */
	EClass getScopIndexExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopIndexExpression#getIndexExpressions <em>Index Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index Expressions</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIndexExpression#getIndexExpressions()
	 * @see #getScopIndexExpression()
	 * @generated
	 */
	EReference getScopIndexExpression_IndexExpressions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopAccess <em>Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopAccess
	 * @generated
	 */
	EClass getScopAccess();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.ScopAccess#getSym <em>Sym</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sym</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopAccess#getSym()
	 * @see #getScopAccess()
	 * @generated
	 */
	EReference getScopAccess_Sym();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.ScopAccess#getSymName <em>Sym Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sym Name</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopAccess#getSymName()
	 * @see #getScopAccess()
	 * @generated
	 */
	EAttribute getScopAccess_SymName();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopRead <em>Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Read</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRead
	 * @generated
	 */
	EClass getScopRead();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionRead <em>Region Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Region Read</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionRead
	 * @generated
	 */
	EClass getScopRegionRead();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionRead#getExistentials <em>Existentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Existentials</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionRead#getExistentials()
	 * @see #getScopRegionRead()
	 * @generated
	 */
	EReference getScopRegionRead_Existentials();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionRead#getRegion <em>Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Region</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionRead#getRegion()
	 * @see #getScopRegionRead()
	 * @generated
	 */
	EReference getScopRegionRead_Region();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite <em>Region Write</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Region Write</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionWrite
	 * @generated
	 */
	EClass getScopRegionWrite();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getExistentials <em>Existentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Existentials</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getExistentials()
	 * @see #getScopRegionWrite()
	 * @generated
	 */
	EReference getScopRegionWrite_Existentials();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getRegion <em>Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Region</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getRegion()
	 * @see #getScopRegionWrite()
	 * @generated
	 */
	EReference getScopRegionWrite_Region();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopWrite <em>Write</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Write</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopWrite
	 * @generated
	 */
	EClass getScopWrite();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntExpression <em>Int Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Expression</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntExpression
	 * @generated
	 */
	EClass getScopIntExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopIntExpression#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expr</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntExpression#getExpr()
	 * @see #getScopIntExpression()
	 * @generated
	 */
	EReference getScopIntExpression_Expr();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraint <em>Int Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Constraint</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraint
	 * @generated
	 */
	EClass getScopIntConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraint#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraint#getGuard()
	 * @see #getScopIntConstraint()
	 * @generated
	 */
	EReference getScopIntConstraint_Guard();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem <em>Int Constraint System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Constraint System</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem
	 * @generated
	 */
	EClass getScopIntConstraintSystem();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem#getSystem <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem#getSystem()
	 * @see #getScopIntConstraintSystem()
	 * @generated
	 */
	EReference getScopIntConstraintSystem_System();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopStatement
	 * @generated
	 */
	EClass getScopStatement();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement <em>Instruction Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instruction Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
	 * @generated
	 */
	EClass getScopInstructionStatement();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlock
	 * @generated
	 */
	EClass getScopBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopBlock#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlock#getChildren()
	 * @see #getScopBlock()
	 * @generated
	 */
	EReference getScopBlock_Children();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop <em>For Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Loop</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop
	 * @generated
	 */
	EClass getScopForLoop();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getIterator <em>Iterator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iterator</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop#getIterator()
	 * @see #getScopForLoop()
	 * @generated
	 */
	EReference getScopForLoop_Iterator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getLB <em>LB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>LB</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop#getLB()
	 * @see #getScopForLoop()
	 * @generated
	 */
	EReference getScopForLoop_LB();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getUB <em>UB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>UB</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop#getUB()
	 * @see #getScopForLoop()
	 * @generated
	 */
	EReference getScopForLoop_UB();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getStride <em>Stride</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Stride</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop#getStride()
	 * @see #getScopForLoop()
	 * @generated
	 */
	EReference getScopForLoop_Stride();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop#getBody()
	 * @see #getScopForLoop()
	 * @generated
	 */
	EReference getScopForLoop_Body();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop <em>Do All Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Do All Loop</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop
	 * @generated
	 */
	EClass getScopDoAllLoop();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.scopAsyncBlock <em>scop Async Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>scop Async Block</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.scopAsyncBlock
	 * @generated
	 */
	EClass getscopAsyncBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock <em>FSM Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Block</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock
	 * @generated
	 */
	EClass getScopFSMBlock();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getIterators <em>Iterators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Iterators</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getIterators()
	 * @see #getScopFSMBlock()
	 * @generated
	 */
	EReference getScopFSMBlock_Iterators();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Start</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getStart()
	 * @see #getScopFSMBlock()
	 * @generated
	 */
	EReference getScopFSMBlock_Start();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getCommands()
	 * @see #getScopFSMBlock()
	 * @generated
	 */
	EReference getScopFSMBlock_Commands();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Next</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock#getNext()
	 * @see #getScopFSMBlock()
	 * @generated
	 */
	EReference getScopFSMBlock_Next();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition <em>FSM Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Transition</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMTransition
	 * @generated
	 */
	EClass getScopFSMTransition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Domain</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getDomain()
	 * @see #getScopFSMTransition()
	 * @generated
	 */
	EReference getScopFSMTransition_Domain();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNextIteration <em>Next Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Next Iteration</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNextIteration()
	 * @see #getScopFSMTransition()
	 * @generated
	 */
	EReference getScopFSMTransition_NextIteration();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMTransition#getNext()
	 * @see #getScopFSMTransition()
	 * @generated
	 */
	EReference getScopFSMTransition_Next();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState <em>FSM State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM State</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMState
	 * @generated
	 */
	EClass getScopFSMState();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Domain</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMState#getDomain()
	 * @see #getScopFSMState()
	 * @generated
	 */
	EReference getScopFSMState_Domain();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMState#getCommands()
	 * @see #getScopFSMState()
	 * @generated
	 */
	EReference getScopFSMState_Commands();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMState#getTransitions()
	 * @see #getScopFSMState()
	 * @generated
	 */
	EReference getScopFSMState_Transitions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode <em>Unexpanded Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unexpanded Node</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode
	 * @generated
	 */
	EClass getScopUnexpandedNode();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Schedule</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getSchedule()
	 * @see #getScopUnexpandedNode()
	 * @generated
	 */
	EAttribute getScopUnexpandedNode_Schedule();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getDomain()
	 * @see #getScopUnexpandedNode()
	 * @generated
	 */
	EAttribute getScopUnexpandedNode_Domain();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getExistentials <em>Existentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Existentials</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode#getExistentials()
	 * @see #getScopUnexpandedNode()
	 * @generated
	 */
	EReference getScopUnexpandedNode_Existentials();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement <em>Unexpanded Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unexpanded Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
	 * @generated
	 */
	EClass getScopUnexpandedStatement();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement <em>Block Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement
	 * @generated
	 */
	EClass getScopBlockStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getLabel()
	 * @see #getScopBlockStatement()
	 * @generated
	 */
	EAttribute getScopBlockStatement_Label();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getReads <em>Reads</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reads</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getReads()
	 * @see #getScopBlockStatement()
	 * @generated
	 */
	EReference getScopBlockStatement_Reads();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getWrites <em>Writes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Writes</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getWrites()
	 * @see #getScopBlockStatement()
	 * @generated
	 */
	EReference getScopBlockStatement_Writes();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement#getBody()
	 * @see #getScopBlockStatement()
	 * @generated
	 */
	EReference getScopBlockStatement_Body();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.DefUseEdge <em>Def Use Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Def Use Edge</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.DefUseEdge
	 * @generated
	 */
	EClass getDefUseEdge();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.DefUseEdge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.DefUseEdge#getSource()
	 * @see #getDefUseEdge()
	 * @generated
	 */
	EReference getDefUseEdge_Source();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.DefUseEdge#getSinks <em>Sinks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sinks</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.DefUseEdge#getSinks()
	 * @see #getDefUseEdge()
	 * @generated
	 */
	EReference getDefUseEdge_Sinks();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.DefUseEdge#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Dependency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.DefUseEdge#getDependency()
	 * @see #getDefUseEdge()
	 * @generated
	 */
	EAttribute getDefUseEdge_Dependency();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge <em>Use Def Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Def Edge</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.UseDefEdge
	 * @generated
	 */
	EClass getUseDefEdge();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sources</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSources()
	 * @see #getUseDefEdge()
	 * @generated
	 */
	EReference getUseDefEdge_Sources();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sink</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSink()
	 * @see #getUseDefEdge()
	 * @generated
	 */
	EReference getUseDefEdge_Sink();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Dependency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.UseDefEdge#getDependency()
	 * @see #getUseDefEdge()
	 * @generated
	 */
	EAttribute getUseDefEdge_Dependency();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge <em>WAW Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>WAW Edge</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.WAWEdge
	 * @generated
	 */
	EClass getWAWEdge();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.WAWEdge#getSource()
	 * @see #getWAWEdge()
	 * @generated
	 */
	EReference getWAWEdge_Source();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getSinks <em>Sinks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sinks</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.WAWEdge#getSinks()
	 * @see #getWAWEdge()
	 * @generated
	 */
	EReference getWAWEdge_Sinks();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Dependency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.WAWEdge#getDependency()
	 * @see #getWAWEdge()
	 * @generated
	 */
	EAttribute getWAWEdge_Dependency();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScopFactory getScopFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopNodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopNodeImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopNode()
		 * @generated
		 */
		EClass SCOP_NODE = eINSTANCE.getScopNode();

		/**
		 * The meta object literal for the '<em><b>Transform</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_NODE__TRANSFORM = eINSTANCE.getScopNode_Transform();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopDimensionImpl <em>Dimension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopDimensionImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopDimension()
		 * @generated
		 */
		EClass SCOP_DIMENSION = eINSTANCE.getScopDimension();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_DIMENSION__SYMBOL = eINSTANCE.getScopDimension_Symbol();

		/**
		 * The meta object literal for the '<em><b>Sym Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCOP_DIMENSION__SYM_NAME = eINSTANCE.getScopDimension_SymName();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopParameterImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopParameter()
		 * @generated
		 */
		EClass SCOP_PARAMETER = eINSTANCE.getScopParameter();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl <em>Gecos Scop Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getGecosScopBlock()
		 * @generated
		 */
		EClass GECOS_SCOP_BLOCK = eINSTANCE.getGecosScopBlock();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GECOS_SCOP_BLOCK__NAME = eINSTANCE.getGecosScopBlock_Name();

		/**
		 * The meta object literal for the '<em><b>Live In Symbols</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS = eINSTANCE.getGecosScopBlock_LiveInSymbols();

		/**
		 * The meta object literal for the '<em><b>Live Out Symbols</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS = eINSTANCE.getGecosScopBlock_LiveOutSymbols();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__PARAMETERS = eINSTANCE.getGecosScopBlock_Parameters();

		/**
		 * The meta object literal for the '<em><b>Iterators</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__ITERATORS = eINSTANCE.getGecosScopBlock_Iterators();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__CONTEXT = eINSTANCE.getGecosScopBlock_Context();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SCOP_BLOCK__ROOT = eINSTANCE.getGecosScopBlock_Root();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl <em>Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopGuard()
		 * @generated
		 */
		EClass SCOP_GUARD = eINSTANCE.getScopGuard();

		/**
		 * The meta object literal for the '<em><b>Predicate</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_GUARD__PREDICATE = eINSTANCE.getScopGuard_Predicate();

		/**
		 * The meta object literal for the '<em><b>Then Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_GUARD__THEN_NODE = eINSTANCE.getScopGuard_ThenNode();

		/**
		 * The meta object literal for the '<em><b>Else Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_GUARD__ELSE_NODE = eINSTANCE.getScopGuard_ElseNode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl <em>Index Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIndexExpression()
		 * @generated
		 */
		EClass SCOP_INDEX_EXPRESSION = eINSTANCE.getScopIndexExpression();

		/**
		 * The meta object literal for the '<em><b>Index Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS = eINSTANCE.getScopIndexExpression_IndexExpressions();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl <em>Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopAccess()
		 * @generated
		 */
		EClass SCOP_ACCESS = eINSTANCE.getScopAccess();

		/**
		 * The meta object literal for the '<em><b>Sym</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_ACCESS__SYM = eINSTANCE.getScopAccess_Sym();

		/**
		 * The meta object literal for the '<em><b>Sym Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCOP_ACCESS__SYM_NAME = eINSTANCE.getScopAccess_SymName();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopReadImpl <em>Read</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopReadImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRead()
		 * @generated
		 */
		EClass SCOP_READ = eINSTANCE.getScopRead();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionReadImpl <em>Region Read</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopRegionReadImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRegionRead()
		 * @generated
		 */
		EClass SCOP_REGION_READ = eINSTANCE.getScopRegionRead();

		/**
		 * The meta object literal for the '<em><b>Existentials</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_REGION_READ__EXISTENTIALS = eINSTANCE.getScopRegionRead_Existentials();

		/**
		 * The meta object literal for the '<em><b>Region</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_REGION_READ__REGION = eINSTANCE.getScopRegionRead_Region();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl <em>Region Write</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopRegionWrite()
		 * @generated
		 */
		EClass SCOP_REGION_WRITE = eINSTANCE.getScopRegionWrite();

		/**
		 * The meta object literal for the '<em><b>Existentials</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_REGION_WRITE__EXISTENTIALS = eINSTANCE.getScopRegionWrite_Existentials();

		/**
		 * The meta object literal for the '<em><b>Region</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_REGION_WRITE__REGION = eINSTANCE.getScopRegionWrite_Region();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopWriteImpl <em>Write</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopWriteImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopWrite()
		 * @generated
		 */
		EClass SCOP_WRITE = eINSTANCE.getScopWrite();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntExpressionImpl <em>Int Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntExpressionImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntExpression()
		 * @generated
		 */
		EClass SCOP_INT_EXPRESSION = eINSTANCE.getScopIntExpression();

		/**
		 * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_INT_EXPRESSION__EXPR = eINSTANCE.getScopIntExpression_Expr();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintImpl <em>Int Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntConstraint()
		 * @generated
		 */
		EClass SCOP_INT_CONSTRAINT = eINSTANCE.getScopIntConstraint();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_INT_CONSTRAINT__GUARD = eINSTANCE.getScopIntConstraint_Guard();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintSystemImpl <em>Int Constraint System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopIntConstraintSystemImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopIntConstraintSystem()
		 * @generated
		 */
		EClass SCOP_INT_CONSTRAINT_SYSTEM = eINSTANCE.getScopIntConstraintSystem();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_INT_CONSTRAINT_SYSTEM__SYSTEM = eINSTANCE.getScopIntConstraintSystem_System();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopStatementImpl <em>Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopStatementImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopStatement()
		 * @generated
		 */
		EClass SCOP_STATEMENT = eINSTANCE.getScopStatement();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopInstructionStatementImpl <em>Instruction Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopInstructionStatementImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopInstructionStatement()
		 * @generated
		 */
		EClass SCOP_INSTRUCTION_STATEMENT = eINSTANCE.getScopInstructionStatement();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopBlockImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopBlock()
		 * @generated
		 */
		EClass SCOP_BLOCK = eINSTANCE.getScopBlock();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_BLOCK__CHILDREN = eINSTANCE.getScopBlock_Children();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl <em>For Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopForLoop()
		 * @generated
		 */
		EClass SCOP_FOR_LOOP = eINSTANCE.getScopForLoop();

		/**
		 * The meta object literal for the '<em><b>Iterator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FOR_LOOP__ITERATOR = eINSTANCE.getScopForLoop_Iterator();

		/**
		 * The meta object literal for the '<em><b>LB</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FOR_LOOP__LB = eINSTANCE.getScopForLoop_LB();

		/**
		 * The meta object literal for the '<em><b>UB</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FOR_LOOP__UB = eINSTANCE.getScopForLoop_UB();

		/**
		 * The meta object literal for the '<em><b>Stride</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FOR_LOOP__STRIDE = eINSTANCE.getScopForLoop_Stride();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FOR_LOOP__BODY = eINSTANCE.getScopForLoop_Body();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopDoAllLoopImpl <em>Do All Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopDoAllLoopImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopDoAllLoop()
		 * @generated
		 */
		EClass SCOP_DO_ALL_LOOP = eINSTANCE.getScopDoAllLoop();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.scopAsyncBlockImpl <em>scop Async Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.scopAsyncBlockImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getscopAsyncBlock()
		 * @generated
		 */
		EClass SCOP_ASYNC_BLOCK = eINSTANCE.getscopAsyncBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl <em>FSM Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMBlock()
		 * @generated
		 */
		EClass SCOP_FSM_BLOCK = eINSTANCE.getScopFSMBlock();

		/**
		 * The meta object literal for the '<em><b>Iterators</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_BLOCK__ITERATORS = eINSTANCE.getScopFSMBlock_Iterators();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_BLOCK__START = eINSTANCE.getScopFSMBlock_Start();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_BLOCK__COMMANDS = eINSTANCE.getScopFSMBlock_Commands();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_BLOCK__NEXT = eINSTANCE.getScopFSMBlock_Next();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl <em>FSM Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMTransition()
		 * @generated
		 */
		EClass SCOP_FSM_TRANSITION = eINSTANCE.getScopFSMTransition();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_TRANSITION__DOMAIN = eINSTANCE.getScopFSMTransition_Domain();

		/**
		 * The meta object literal for the '<em><b>Next Iteration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_TRANSITION__NEXT_ITERATION = eINSTANCE.getScopFSMTransition_NextIteration();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_TRANSITION__NEXT = eINSTANCE.getScopFSMTransition_Next();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl <em>FSM State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopFSMState()
		 * @generated
		 */
		EClass SCOP_FSM_STATE = eINSTANCE.getScopFSMState();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_STATE__DOMAIN = eINSTANCE.getScopFSMState_Domain();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_STATE__COMMANDS = eINSTANCE.getScopFSMState_Commands();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_FSM_STATE__TRANSITIONS = eINSTANCE.getScopFSMState_Transitions();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedNodeImpl <em>Unexpanded Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedNodeImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopUnexpandedNode()
		 * @generated
		 */
		EClass SCOP_UNEXPANDED_NODE = eINSTANCE.getScopUnexpandedNode();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCOP_UNEXPANDED_NODE__SCHEDULE = eINSTANCE.getScopUnexpandedNode_Schedule();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCOP_UNEXPANDED_NODE__DOMAIN = eINSTANCE.getScopUnexpandedNode_Domain();

		/**
		 * The meta object literal for the '<em><b>Existentials</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_UNEXPANDED_NODE__EXISTENTIALS = eINSTANCE.getScopUnexpandedNode_Existentials();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl <em>Unexpanded Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopUnexpandedStatement()
		 * @generated
		 */
		EClass SCOP_UNEXPANDED_STATEMENT = eINSTANCE.getScopUnexpandedStatement();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl <em>Block Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getScopBlockStatement()
		 * @generated
		 */
		EClass SCOP_BLOCK_STATEMENT = eINSTANCE.getScopBlockStatement();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCOP_BLOCK_STATEMENT__LABEL = eINSTANCE.getScopBlockStatement_Label();

		/**
		 * The meta object literal for the '<em><b>Reads</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_BLOCK_STATEMENT__READS = eINSTANCE.getScopBlockStatement_Reads();

		/**
		 * The meta object literal for the '<em><b>Writes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_BLOCK_STATEMENT__WRITES = eINSTANCE.getScopBlockStatement_Writes();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_BLOCK_STATEMENT__BODY = eINSTANCE.getScopBlockStatement_Body();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.DefUseEdgeImpl <em>Def Use Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.DefUseEdgeImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getDefUseEdge()
		 * @generated
		 */
		EClass DEF_USE_EDGE = eINSTANCE.getDefUseEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEF_USE_EDGE__SOURCE = eINSTANCE.getDefUseEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Sinks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEF_USE_EDGE__SINKS = eINSTANCE.getDefUseEdge_Sinks();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEF_USE_EDGE__DEPENDENCY = eINSTANCE.getDefUseEdge_Dependency();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.UseDefEdgeImpl <em>Use Def Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.UseDefEdgeImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getUseDefEdge()
		 * @generated
		 */
		EClass USE_DEF_EDGE = eINSTANCE.getUseDefEdge();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_DEF_EDGE__SOURCES = eINSTANCE.getUseDefEdge_Sources();

		/**
		 * The meta object literal for the '<em><b>Sink</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_DEF_EDGE__SINK = eINSTANCE.getUseDefEdge_Sink();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_DEF_EDGE__DEPENDENCY = eINSTANCE.getUseDefEdge_Dependency();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.impl.WAWEdgeImpl <em>WAW Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.WAWEdgeImpl
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getWAWEdge()
		 * @generated
		 */
		EClass WAW_EDGE = eINSTANCE.getWAWEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAW_EDGE__SOURCE = eINSTANCE.getWAWEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Sinks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAW_EDGE__SINKS = eINSTANCE.getWAWEdge_Sinks();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WAW_EDGE__DEPENDENCY = eINSTANCE.getWAWEdge_Dependency();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

	}

} //ScopPackage
