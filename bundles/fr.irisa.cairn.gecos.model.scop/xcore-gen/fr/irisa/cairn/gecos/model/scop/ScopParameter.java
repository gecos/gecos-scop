/**
 */
package fr.irisa.cairn.gecos.model.scop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopParameter()
 * @model
 * @generated
 */
public interface ScopParameter extends ScopDimension {
} // ScopParameter
