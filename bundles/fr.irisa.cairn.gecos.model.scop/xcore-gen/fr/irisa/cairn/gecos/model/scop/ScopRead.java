/**
 */
package fr.irisa.cairn.gecos.model.scop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopRead()
 * @model
 * @generated
 */
public interface ScopRead extends ScopAccess {
} // ScopRead
