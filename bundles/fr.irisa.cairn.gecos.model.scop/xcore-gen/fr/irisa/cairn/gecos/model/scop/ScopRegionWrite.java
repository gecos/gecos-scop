/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Region Write</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getExistentials <em>Existentials</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getRegion <em>Region</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopRegionWrite()
 * @model
 * @generated
 */
public interface ScopRegionWrite extends ScopRead {
	/**
	 * Returns the value of the '<em><b>Existentials</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Existentials</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Existentials</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopRegionWrite_Existentials()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopDimension> getExistentials();

	/**
	 * Returns the value of the '<em><b>Region</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Region</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Region</em>' containment reference.
	 * @see #setRegion(IntConstraintSystem)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopRegionWrite_Region()
	 * @model containment="true"
	 * @generated
	 */
	IntConstraintSystem getRegion();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite#getRegion <em>Region</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Region</em>' containment reference.
	 * @see #getRegion()
	 * @generated
	 */
	void setRegion(IntConstraintSystem value);

} // ScopRegionWrite
