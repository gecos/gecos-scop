/**
 */
package fr.irisa.cairn.gecos.model.scop;

import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopStatement()
 * @model
 * @generated
 */
public interface ScopStatement extends ScopNode {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.irisa.cairn.gecos.model.scop.String" unique="false"
	 * @generated
	 */
	String getId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameDataType="fr.irisa.cairn.gecos.model.scop.String" nameUnique="false"
	 * @generated
	 */
	void setId(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;toEList(java.util.Collections.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt;&gt;newArrayList(this)));'"
	 * @generated
	 */
	EList<ScopStatement> listAllStatements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt; a)\n\t{\n\t\t&lt;%gecos.core.Symbol%&gt; _sym = a.getSym();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_sym, s));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;&gt;exists(this.listAllAccesses(), _function);'"
	 * @generated
	 */
	boolean useSymbol(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Symbol%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt; a)\n\t{\n\t\treturn a.getSym();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toEList(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toSet(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopAccess%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;map(this.listAllAccesses(), _function)));'"
	 * @generated
	 */
	EList<Symbol> listAllSymbolReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;, &lt;%gecos.core.Symbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Symbol%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt; a)\n\t{\n\t\treturn a.getSym();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toEList(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toSet(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopRead%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;map(this.listAllReadAccess(), _function)));'"
	 * @generated
	 */
	EList<Symbol> listAllReadSymbols();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;, &lt;%gecos.core.Symbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Symbol%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt; a)\n\t{\n\t\treturn a.getSym();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toEList(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;toSet(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.ScopWrite%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;map(this.listAllWriteAccess(), _function)));'"
	 * @generated
	 */
	EList<Symbol> listAllWriteSymbols();

} // ScopStatement
