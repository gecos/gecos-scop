/**
 */
package fr.irisa.cairn.gecos.model.scop;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unexpanded Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopUnexpandedBlock()
 * @model
 * @generated
 */
public interface ScopUnexpandedBlock extends ScopBlock, ScopUnexpandedNode {
} // ScopUnexpandedBlock
