/**
 */
package fr.irisa.cairn.gecos.model.scop;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unexpanded Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopUnexpandedStatement()
 * @model
 * @generated
 */
public interface ScopUnexpandedStatement extends ScopInstructionStatement, ScopUnexpandedNode {

} // ScopUnexpandedStatement
