/**
 */
package fr.irisa.cairn.gecos.model.scop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Write</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getScopWrite()
 * @model
 * @generated
 */
public interface ScopWrite extends ScopAccess {
} // ScopWrite
