/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unexpanded Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getSchedule <em>Schedule</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getExistentials <em>Existentials</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUnexpandedNode()
 * @model
 * @generated
 */
public interface UnexpandedNode extends ScopNode {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUnexpandedNode_Label()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule</em>' attribute.
	 * @see #setSchedule(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUnexpandedNode_Schedule()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getSchedule();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getSchedule <em>Schedule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schedule</em>' attribute.
	 * @see #getSchedule()
	 * @generated
	 */
	void setSchedule(String value);

	/**
	 * Returns the value of the '<em><b>Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' attribute.
	 * @see #setDomain(String)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUnexpandedNode_Domain()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	String getDomain();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.UnexpandedNode#getDomain <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' attribute.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(String value);

	/**
	 * Returns the value of the '<em><b>Existentials</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Existentials</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Existentials</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUnexpandedNode_Existentials()
	 * @model
	 * @generated
	 */
	EList<ScopDimension> getExistentials();

} // UnexpandedNode
