/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Def Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSources <em>Sources</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSink <em>Sink</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getDependency <em>Dependency</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUseDefEdge()
 * @model
 * @generated
 */
public interface UseDefEdge extends EObject {
	/**
	 * Returns the value of the '<em><b>Sources</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopWrite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUseDefEdge_Sources()
	 * @model
	 * @generated
	 */
	EList<ScopWrite> getSources();

	/**
	 * Returns the value of the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink</em>' reference.
	 * @see #setSink(ScopRead)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUseDefEdge_Sink()
	 * @model
	 * @generated
	 */
	ScopRead getSink();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge#getSink <em>Sink</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink</em>' reference.
	 * @see #getSink()
	 * @generated
	 */
	void setSink(ScopRead value);

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getUseDefEdge_Dependency()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	EList<String> getDependency();

} // UseDefEdge
