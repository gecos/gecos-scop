/**
 */
package fr.irisa.cairn.gecos.model.scop;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WAW Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getSinks <em>Sinks</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getDependency <em>Dependency</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getWAWEdge()
 * @model
 * @generated
 */
public interface WAWEdge extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ScopWrite)
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getWAWEdge_Source()
	 * @model
	 * @generated
	 */
	ScopWrite getSource();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ScopWrite value);

	/**
	 * Returns the value of the '<em><b>Sinks</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopWrite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sinks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sinks</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getWAWEdge_Sinks()
	 * @model
	 * @generated
	 */
	EList<ScopWrite> getSinks();

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getWAWEdge_Dependency()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.String"
	 * @generated
	 */
	EList<String> getDependency();

} // WAWEdge
