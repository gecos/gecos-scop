/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitable;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.core.CorePackage;
import gecos.core.CoreVisitable;
import gecos.core.CoreVisitor;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.dag.DAGInstruction;
import gecos.instrs.BranchType;
import gecos.instrs.GotoInstruction;
import gecos.instrs.Instruction;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gecos Scop Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getInstructions <em>Instructions</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getDefEdges <em>Def Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getOutEdges <em>Out Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getUseEdges <em>Use Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getInEdges <em>In Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getFlags <em>Flags</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getLiveInSymbols <em>Live In Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getLiveOutSymbols <em>Live Out Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getIterators <em>Iterators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getContext <em>Context</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.GecosScopBlockImpl#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GecosScopBlockImpl extends ScopNodeImpl implements GecosScopBlock {
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected int number = NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstructions() <em>Instructions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstructions()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> instructions;

	/**
	 * The cached value of the '{@link #getDefEdges() <em>Def Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<DataEdge> defEdges;

	/**
	 * The cached value of the '{@link #getOutEdges() <em>Out Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlEdge> outEdges;

	/**
	 * The cached value of the '{@link #getUseEdges() <em>Use Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<DataEdge> useEdges;

	/**
	 * The cached value of the '{@link #getInEdges() <em>In Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlEdge> inEdges;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected static final int FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected int flags = FLAGS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected Scope scope;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLiveInSymbols() <em>Live In Symbols</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiveInSymbols()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> liveInSymbols;

	/**
	 * The cached value of the '{@link #getLiveOutSymbols() <em>Live Out Symbols</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiveOutSymbols()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> liveOutSymbols;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> parameters;

	/**
	 * The cached value of the '{@link #getIterators() <em>Iterators</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterators()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> iterators;

	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected IntConstraintSystem context;

	/**
	 * The cached value of the '{@link #getRoot() <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoot()
	 * @generated
	 * @ordered
	 */
	protected ScopNode root;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GecosScopBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.GECOS_SCOP_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getParent() {
		EObject _eContainer = this.eContainer();
		if ((_eContainer instanceof Block)) {
			EObject _eContainer_1 = this.eContainer();
			return ((Block) _eContainer_1);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber(int newNumber) {
		int oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getInstructions() {
		if (instructions == null) {
			instructions = new EObjectContainmentEList<Instruction>(Instruction.class, this, ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS);
		}
		return instructions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataEdge> getDefEdges() {
		if (defEdges == null) {
			defEdges = new EObjectContainmentWithInverseEList<DataEdge>(DataEdge.class, this, ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES, BlocksPackage.DATA_EDGE__FROM);
		}
		return defEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> getOutEdges() {
		if (outEdges == null) {
			outEdges = new EObjectContainmentWithInverseEList<ControlEdge>(ControlEdge.class, this, ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES, BlocksPackage.CONTROL_EDGE__FROM);
		}
		return outEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataEdge> getUseEdges() {
		if (useEdges == null) {
			useEdges = new EObjectWithInverseResolvingEList<DataEdge>(DataEdge.class, this, ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES, BlocksPackage.DATA_EDGE__TO);
		}
		return useEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> getInEdges() {
		if (inEdges == null) {
			inEdges = new EObjectWithInverseResolvingEList<ControlEdge>(ControlEdge.class, this, ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES, BlocksPackage.CONTROL_EDGE__TO);
		}
		return inEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFlags() {
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlags(int newFlags) {
		int oldFlags = flags;
		flags = newFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__FLAGS, oldFlags, flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScope(Scope newScope, NotificationChain msgs) {
		Scope oldScope = scope;
		scope = newScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__SCOPE, oldScope, newScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(Scope newScope) {
		if (newScope != scope) {
			NotificationChain msgs = null;
			if (scope != null)
				msgs = ((InternalEObject)scope).eInverseRemove(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			if (newScope != null)
				msgs = ((InternalEObject)newScope).eInverseAdd(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			msgs = basicSetScope(newScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__SCOPE, newScope, newScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getLiveInSymbols() {
		if (liveInSymbols == null) {
			liveInSymbols = new EObjectResolvingEList<Symbol>(Symbol.class, this, ScopPackage.GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS);
		}
		return liveInSymbols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getLiveOutSymbols() {
		if (liveOutSymbols == null) {
			liveOutSymbols = new EObjectResolvingEList<Symbol>(Symbol.class, this, ScopPackage.GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS);
		}
		return liveOutSymbols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ScopDimension>(ScopDimension.class, this, ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getIterators() {
		if (iterators == null) {
			iterators = new EObjectContainmentEList<ScopDimension>(ScopDimension.class, this, ScopPackage.GECOS_SCOP_BLOCK__ITERATORS);
		}
		return iterators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem getContext() {
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContext(IntConstraintSystem newContext, NotificationChain msgs) {
		IntConstraintSystem oldContext = context;
		context = newContext;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__CONTEXT, oldContext, newContext);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(IntConstraintSystem newContext) {
		if (newContext != context) {
			NotificationChain msgs = null;
			if (context != null)
				msgs = ((InternalEObject)context).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.GECOS_SCOP_BLOCK__CONTEXT, null, msgs);
			if (newContext != null)
				msgs = ((InternalEObject)newContext).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.GECOS_SCOP_BLOCK__CONTEXT, null, msgs);
			msgs = basicSetContext(newContext, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__CONTEXT, newContext, newContext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getRoot() {
		return root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoot(ScopNode newRoot, NotificationChain msgs) {
		ScopNode oldRoot = root;
		root = newRoot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__ROOT, oldRoot, newRoot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoot(ScopNode newRoot) {
		if (newRoot != root) {
			NotificationChain msgs = null;
			if (root != null)
				msgs = ((InternalEObject)root).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.GECOS_SCOP_BLOCK__ROOT, null, msgs);
			if (newRoot != null)
				msgs = ((InternalEObject)newRoot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.GECOS_SCOP_BLOCK__ROOT, null, msgs);
			msgs = basicSetRoot(newRoot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.GECOS_SCOP_BLOCK__ROOT, newRoot, newRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getParentScop() {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		ScopNode _root = this.getRoot();
		boolean _equals = Objects.equal(_root, n);
		if (_equals) {
			this.setRoot(null);
		}
		else {
			String _plus = (n + " is not a children of ");
			String _plus_1 = (_plus + this);
			throw new UnsupportedOperationException(_plus_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final ScopNode n, final ScopNode _new) {
		ScopNode _root = this.getRoot();
		boolean _equals = Objects.equal(_root, n);
		if (_equals) {
			this.setRoot(_new);
		}
		else {
			String _plus = (n + " is not a children of ");
			String _plus_1 = (_plus + this);
			throw new UnsupportedOperationException(_plus_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void relabelStatements() {
		int _id = 0;
		EList<ScopStatement> _listAllStatements = this.listAllStatements();
		for (final ScopStatement s : _listAllStatements) {
			int _plusPlus = _id++;
			String _plus = ("S" + Integer.valueOf(_plusPlus));
			s.setId(_plus);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		String content = this.getInstructions().toString();
		int _length = content.length();
		boolean _greaterThan = (_length > 40);
		if (_greaterThan) {
			int _size = this.getInstructions().size();
			boolean _greaterThan_1 = (_size > 1);
			if (_greaterThan_1) {
				String _string = this.getInstructions().get(0).toString();
				String _plus = (_string + ", ...");
				content = _plus;
			}
		}
		String _pathString = this.getPathString();
		String _string_1 = sb.toString();
		return (_pathString + _string_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> getPredecessors() {
		EList<BasicBlock> _xblockexpression = null; {
			final Function1<ControlEdge, BasicBlock> _function = new Function1<ControlEdge, BasicBlock>() {
				public BasicBlock apply(final ControlEdge it) {
					return it.getFrom();
				}
			};
			final EList<BasicBlock> res = ECollections.<BasicBlock>unmodifiableEList(ECollections.<BasicBlock>asEList(XcoreEListExtensions.<ControlEdge, BasicBlock>map(this.getInEdges(), _function)));
			final Consumer<BasicBlock> _function_1 = new Consumer<BasicBlock>() {
				public void accept(final BasicBlock bb) {
					if ((bb == null)) {
						throw new UnsupportedOperationException("Inconsistent control flow in IR");
					}
				}
			};
			res.forEach(_function_1);
			_xblockexpression = res;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitBasicBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearOutEdges() {
		final Consumer<ControlEdge> _function = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
				it.setFrom(null);
				it.setCond(null);
			}
		};
		((List<ControlEdge>)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getOutEdges(), ControlEdge.class)).clone())).forEach(_function);
		this.getOutEdges().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> getSuccessors() {
		EList<BasicBlock> _xblockexpression = null; {
			final Function1<ControlEdge, BasicBlock> _function = new Function1<ControlEdge, BasicBlock>() {
				public BasicBlock apply(final ControlEdge it) {
					return it.getTo();
				}
			};
			final EList<BasicBlock> res = ECollections.<BasicBlock>unmodifiableEList(ECollections.<BasicBlock>asEList(XcoreEListExtensions.<ControlEdge, BasicBlock>map(this.getOutEdges(), _function)));
			final Consumer<BasicBlock> _function_1 = new Consumer<BasicBlock>() {
				public void accept(final BasicBlock bb) {
					if ((bb == null)) {
						throw new UnsupportedOperationException("Inconsistent control flow in IR");
					}
				}
			};
			res.forEach(_function_1);
			_xblockexpression = res;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeInstruction(final Instruction s) {
		this.getInstructions().remove(s);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String contentString() {
		return IterableExtensions.join(this.getInstructions(), "\\n");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeWithBasic(final BasicBlock b) {
		int _size = this.getOutEdges().size();
		boolean _greaterThan = (_size > 1);
		if (_greaterThan) {
			throw new IllegalArgumentException("Cannot merge a basicBloc with more than one out Edge");
		}
		int _size_1 = b.getInEdges().size();
		boolean _greaterThan_1 = (_size_1 > 1);
		if (_greaterThan_1) {
			throw new IllegalArgumentException("Cannot merge with a basicBloc with more than one in Edge");
		}
		final Consumer<Instruction> _function = new Consumer<Instruction>() {
			public void accept(final Instruction it) {
				b.prependInstruction(it);
			}
		};
		XcoreEListExtensions.<Instruction>reverseView(this.getInstructions()).forEach(_function);
		this.getParent().removeBlock(this);
		this.getOutEdges().get(0).disconnect();
		while ((this.getInEdges().size() != 0)) {
			{
				final ControlEdge e = this.getInEdges().get(0);
				b.connectFromBasic(e.getFrom(), e.getCond());
				e.disconnect();
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock splitAt(final Instruction g) {
		final int idx = this.getInstructions().indexOf(g);
		if ((idx < 0)) {
			throw new IllegalArgumentException("The specified instruction is not contained in this basic block!");
		}
		final BasicBlock b = BlocksFactory.eINSTANCE.createBasicBlock();
		this.getParent().addBlockAfter(b, this);
		this.removeInstruction(g);
		final Consumer<Instruction> _function = new Consumer<Instruction>() {
			public void accept(final Instruction it) {
				b.addInstruction(it);
			}
		};
		((List<Instruction>)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((Instruction[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Instruction>drop(this.getInstructions(), idx), Instruction.class)).clone())).forEach(_function);
		while ((this.getOutEdges().size() != 0)) {
			{
				final ControlEdge e = this.getOutEdges().get(0);
				e.disconnect();
				BasicBlock _to = e.getTo();
				if (_to!=null) {
					_to.connectFromBasic(b, e.getCond());
				}
			}
		}
		return b;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void prependInstruction(final Instruction instr) {
		this.getInstructions().add(0, instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getAssociatedScope() {
		Block _parent = this.getParent();
		boolean _tripleEquals = (_parent == null);
		if (_tripleEquals) {
			throw new NullPointerException((("Basic block " + this) + " has an empty Scope"));
		}
		Block _parent_1 = this.getParent();
		return ((ScopeContainer) _parent_1).getScope();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock from, final BranchType cond) {
		if ((from == null)) {
			throw new IllegalArgumentException("Cannot connect to a null BB");
		}
		EList<ControlEdge> _outEdges = from.getOutEdges();
		for (final ControlEdge e : _outEdges) {
			if ((((e.getFrom() == from) && (e.getTo() == this)) && (e.getCond() == cond))) {
				return null;
			}
		}
		final ControlEdge e_1 = BlocksFactory.eINSTANCE.createControlEdge();
		e_1.setCond(cond);
		e_1.setFrom(from);
		e_1.setTo(this);
		return e_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		final BranchInstructionFinder visitor = new BranchInstructionFinder();
		visitor.doSwitch(this);
		boolean _containsGotos = visitor.containsGotos();
		if (_containsGotos) {
			final GotoInstruction g = visitor.getGotos().get(0);
			final BasicBlock sndTo = g.getLabelInstruction().getBasicBlock();
			if ((sndTo != to)) {
				System.out.println("[BasicBlock] Warning : avoid connection. Basic Block has goto instruction.");
				return ECollections.<ControlEdge>asEList();
			}
		}
		final ControlEdge edge = to.connectFromBasic(this, cond);
		return ECollections.<ControlEdge>newBasicEList(edge);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addInstruction(final Instruction instr) {
		this.getInstructions().add(instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getInstruction(final int i) {
		return this.getInstructions().get(i);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void insertInstructionBefore(final Instruction instr, final Instruction pos) {
		final int i = this.getInstructions().indexOf(pos);
		if ((i >= 0)) {
			this.getInstructions().add(i, instr);
		}
		else {
			System.err.println(((("Warning : could not insert " + instr) + " before ") + pos));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void insertInstructionAfter(final Instruction instr, final Instruction pos) {
		final int i = this.getInstructions().indexOf(pos);
		if ((i >= 0)) {
			this.getInstructions().add((i + 1), instr);
		}
		else {
			System.err.println(((("Warning : could not insert " + instr) + " after ") + pos));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceInstruction(final Instruction oldi, final Instruction newi) {
		final int i = this.getInstructions().indexOf(oldi);
		if ((i >= 0)) {
			this.getInstructions().set(i, newi);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeAllInstructions() {
		while ((this.getInstructions().size() > 0)) {
			this.removeInstruction(this.getInstructions().get(0));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction lastInstruction() {
		return IterableExtensions.<Instruction>last(this.getInstructions());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConnectedTo(final BasicBlock b) {
		EList<ControlEdge> _outEdges = this.getOutEdges();
		for (final ControlEdge e : _outEdges) {
			BasicBlock _to = e.getTo();
			boolean _tripleEquals = (_to == b);
			if (_tripleEquals) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnectFromBasic(final BasicBlock from) {
		final Function1<ControlEdge, Boolean> _function = new Function1<ControlEdge, Boolean>() {
			public Boolean apply(final ControlEdge it) {
				BasicBlock _to = it.getTo();
				return Boolean.valueOf((_to == GecosScopBlockImpl.this));
			}
		};
		final EList<ControlEdge> res = ECollections.<ControlEdge>asEList(((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<ControlEdge>filter(from.getOutEdges(), _function), ControlEdge.class)));
		final Consumer<ControlEdge> _function_1 = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
				it.getFrom().getOutEdges().remove(it);
			}
		};
		res.forEach(_function_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnect() {
		final Consumer<ControlEdge> _function = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
			}
		};
		this.getOutEdges().forEach(_function);
		final Consumer<DataEdge> _function_1 = new Consumer<DataEdge>() {
			public void accept(final DataEdge it) {
				it.setTo(null);
			}
		};
		this.getDefEdges().forEach(_function_1);
		this.getOutEdges().clear();
		this.getInEdges().clear();
		this.getDefEdges().clear();
		this.getUseEdges().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int whichPredecessor(final BasicBlock b) {
		EList<ControlEdge> _inEdges = this.getInEdges();
		for (final ControlEdge e : _inEdges) {
			BasicBlock _from = e.getFrom();
			boolean _equals = Objects.equal(_from, b);
			if (_equals) {
				return this.getInEdges().indexOf(e);
			}
		}
		return (-1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlag(final int f) {
		this.setFlags(f);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addFlag(final int f) {
		this.setFlags((this.getFlags() | f));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeFlag(final int f) {
		this.setFlags((this.getFlags() & (~f)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet(final int f) {
		return ((this.getFlags() & f) != 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getFirstInstruction() {
		final Instruction i = IterableExtensions.<Instruction>head(this.getInstructions());
		if ((i == null)) {
			throw new RuntimeException(("Cannot get first instruction of an empty BasicBlock " + this));
		}
		return i;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getLastInstruction() {
		final Instruction i = IterableExtensions.<Instruction>last(this.getInstructions());
		if ((i == null)) {
			throw new RuntimeException(("Cannot get last instruction of an empty BasicBlock " + this));
		}
		return i;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInstructionCount() {
		return this.getInstructions().size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		int _number = this.getNumber();
		String _plus = ("BB" + Integer.valueOf(_number));
		return this.getPathString(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final BasicBlock newBB = BlocksFactory.eINSTANCE.createBasicBlock();
		EList<Instruction> _instructions = this.getInstructions();
		for (final Instruction inst : _instructions) {
			{
				final Instruction newInst = inst.copy();
				newBB.getInstructions().add(newInst);
			}
		}
		GecosUserAnnotationFactory.copyAnnotations(this, newBB);
		mng.link(this, newBB);
		return newBB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDAG() {
		return ((this.getInstructionCount() == 1) && (this.getInstruction(0) instanceof DAGInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block insertedBlock, final Block existingBlock) {
		final Block parent = this.getParent();
		final CompositeBlock compositeBlock = GecosUserBlockFactory.CompositeBlock();
		parent.replace(existingBlock, compositeBlock);
		compositeBlock.getChildren().add(0, existingBlock);
		compositeBlock.getChildren().add(1, insertedBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeChildComposite(final CompositeBlock b) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldBlock, final Block newBlock) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block mergeWithComposite(final CompositeBlock cblock) {
		cblock.getScope().mergeWith(this.getScope());
		return cblock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean mergeChildren() {
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getContainingProcedure() {
		EObject containr = this.eContainer();
		while ((containr != null)) {
			if ((containr instanceof Procedure)) {
				return ((Procedure)containr);
			}
			else {
				containr = containr.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getContainingProcedureSet() {
		Procedure _containingProcedure = this.getContainingProcedure();
		ProcedureSet _containingProcedureSet = null;
		if (_containingProcedure!=null) {
			_containingProcedureSet=_containingProcedure.getContainingProcedureSet();
		}
		return _containingProcedureSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString(final String nodeName) {
		final Block parent = this.getParent();
		try {
			if ((parent == null)) {
				Procedure _containingProcedure = this.getContainingProcedure();
				boolean _tripleNotEquals = (_containingProcedure != null);
				if (_tripleNotEquals) {
					final ProcedureSymbol symbol = this.getContainingProcedure().getSymbol();
					if ((symbol != null)) {
						String _name = symbol.getName();
						String _plus = ("/proc:" + _name);
						String _plus_1 = (_plus + "/");
						return (_plus_1 + nodeName);
					}
					else {
						return ("/??/" + nodeName);
					}
				}
				else {
					return ("/" + nodeName);
				}
			}
			else {
				final int indexOf = parent.eContents().indexOf(this);
				String _pathString = parent.getPathString();
				String _plus_2 = (_pathString + "/");
				String _plus_3 = (_plus_2 + nodeName);
				String _plus_4 = (_plus_3 + "#");
				return (_plus_4 + Integer.valueOf(indexOf));
			}
		}
		catch (final Throwable _t) {
			if (_t instanceof Exception) {
				return ("/exception_" + nodeName);
			}
			else {
				throw Exceptions.sneakyThrow(_t);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRootBlock() {
		EObject _eContainer = this.eContainer();
		return (_eContainer instanceof Procedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getRootProcedure() {
		return this.getContainingProcedure();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDefEdges()).basicAdd(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutEdges()).basicAdd(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUseEdges()).basicAdd(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInEdges()).basicAdd(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				if (scope != null)
					msgs = ((InternalEObject)scope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.GECOS_SCOP_BLOCK__SCOPE, null, msgs);
				return basicSetScope((Scope)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosScopBlock copy() {
		return EcoreUtil.<GecosScopBlock>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		String _name = this.getName();
		return ("Scop" + _name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSymbol(final Symbol symbol) {
		this.getScope().addSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeSymbol(final Symbol symbol) {
		this.getScope().removeSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(CoreVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS:
				return ((InternalEList<?>)getInstructions()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				return ((InternalEList<?>)getDefEdges()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				return ((InternalEList<?>)getOutEdges()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				return ((InternalEList<?>)getUseEdges()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				return ((InternalEList<?>)getInEdges()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				return basicSetScope(null, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__ITERATORS:
				return ((InternalEList<?>)getIterators()).basicRemove(otherEnd, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__CONTEXT:
				return basicSetContext(null, msgs);
			case ScopPackage.GECOS_SCOP_BLOCK__ROOT:
				return basicSetRoot(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__PARENT:
				return getParent();
			case ScopPackage.GECOS_SCOP_BLOCK__NUMBER:
				return getNumber();
			case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS:
				return getInstructions();
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				return getDefEdges();
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				return getOutEdges();
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				return getUseEdges();
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				return getInEdges();
			case ScopPackage.GECOS_SCOP_BLOCK__LABEL:
				return getLabel();
			case ScopPackage.GECOS_SCOP_BLOCK__FLAGS:
				return getFlags();
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				return getScope();
			case ScopPackage.GECOS_SCOP_BLOCK__NAME:
				return getName();
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS:
				return getLiveInSymbols();
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS:
				return getLiveOutSymbols();
			case ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS:
				return getParameters();
			case ScopPackage.GECOS_SCOP_BLOCK__ITERATORS:
				return getIterators();
			case ScopPackage.GECOS_SCOP_BLOCK__CONTEXT:
				return getContext();
			case ScopPackage.GECOS_SCOP_BLOCK__ROOT:
				return getRoot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__NUMBER:
				setNumber((Integer)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS:
				getInstructions().clear();
				getInstructions().addAll((Collection<? extends Instruction>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				getDefEdges().clear();
				getDefEdges().addAll((Collection<? extends DataEdge>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				getOutEdges().clear();
				getOutEdges().addAll((Collection<? extends ControlEdge>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				getUseEdges().clear();
				getUseEdges().addAll((Collection<? extends DataEdge>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				getInEdges().clear();
				getInEdges().addAll((Collection<? extends ControlEdge>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LABEL:
				setLabel((String)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__FLAGS:
				setFlags((Integer)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				setScope((Scope)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__NAME:
				setName((String)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS:
				getLiveInSymbols().clear();
				getLiveInSymbols().addAll((Collection<? extends Symbol>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS:
				getLiveOutSymbols().clear();
				getLiveOutSymbols().addAll((Collection<? extends Symbol>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ScopDimension>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__ITERATORS:
				getIterators().clear();
				getIterators().addAll((Collection<? extends ScopDimension>)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__CONTEXT:
				setContext((IntConstraintSystem)newValue);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__ROOT:
				setRoot((ScopNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS:
				getInstructions().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				getDefEdges().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				getOutEdges().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				getUseEdges().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				getInEdges().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__FLAGS:
				setFlags(FLAGS_EDEFAULT);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				setScope((Scope)null);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS:
				getLiveInSymbols().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS:
				getLiveOutSymbols().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS:
				getParameters().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__ITERATORS:
				getIterators().clear();
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__CONTEXT:
				setContext((IntConstraintSystem)null);
				return;
			case ScopPackage.GECOS_SCOP_BLOCK__ROOT:
				setRoot((ScopNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.GECOS_SCOP_BLOCK__PARENT:
				return getParent() != null;
			case ScopPackage.GECOS_SCOP_BLOCK__NUMBER:
				return number != NUMBER_EDEFAULT;
			case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS:
				return instructions != null && !instructions.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES:
				return defEdges != null && !defEdges.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES:
				return outEdges != null && !outEdges.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES:
				return useEdges != null && !useEdges.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES:
				return inEdges != null && !inEdges.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case ScopPackage.GECOS_SCOP_BLOCK__FLAGS:
				return flags != FLAGS_EDEFAULT;
			case ScopPackage.GECOS_SCOP_BLOCK__SCOPE:
				return scope != null;
			case ScopPackage.GECOS_SCOP_BLOCK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS:
				return liveInSymbols != null && !liveInSymbols.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS:
				return liveOutSymbols != null && !liveOutSymbols.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__ITERATORS:
				return iterators != null && !iterators.isEmpty();
			case ScopPackage.GECOS_SCOP_BLOCK__CONTEXT:
				return context != null;
			case ScopPackage.GECOS_SCOP_BLOCK__ROOT:
				return root != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == BlocksVisitable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Block.class) {
			switch (derivedFeatureID) {
				case ScopPackage.GECOS_SCOP_BLOCK__PARENT: return BlocksPackage.BLOCK__PARENT;
				case ScopPackage.GECOS_SCOP_BLOCK__NUMBER: return BlocksPackage.BLOCK__NUMBER;
				default: return -1;
			}
		}
		if (baseClass == BasicBlock.class) {
			switch (derivedFeatureID) {
				case ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS: return BlocksPackage.BASIC_BLOCK__INSTRUCTIONS;
				case ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES: return BlocksPackage.BASIC_BLOCK__DEF_EDGES;
				case ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES: return BlocksPackage.BASIC_BLOCK__OUT_EDGES;
				case ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES: return BlocksPackage.BASIC_BLOCK__USE_EDGES;
				case ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES: return BlocksPackage.BASIC_BLOCK__IN_EDGES;
				case ScopPackage.GECOS_SCOP_BLOCK__LABEL: return BlocksPackage.BASIC_BLOCK__LABEL;
				case ScopPackage.GECOS_SCOP_BLOCK__FLAGS: return BlocksPackage.BASIC_BLOCK__FLAGS;
				default: return -1;
			}
		}
		if (baseClass == CoreVisitable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ScopeContainer.class) {
			switch (derivedFeatureID) {
				case ScopPackage.GECOS_SCOP_BLOCK__SCOPE: return CorePackage.SCOPE_CONTAINER__SCOPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == BlocksVisitable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Block.class) {
			switch (baseFeatureID) {
				case BlocksPackage.BLOCK__PARENT: return ScopPackage.GECOS_SCOP_BLOCK__PARENT;
				case BlocksPackage.BLOCK__NUMBER: return ScopPackage.GECOS_SCOP_BLOCK__NUMBER;
				default: return -1;
			}
		}
		if (baseClass == BasicBlock.class) {
			switch (baseFeatureID) {
				case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS: return ScopPackage.GECOS_SCOP_BLOCK__INSTRUCTIONS;
				case BlocksPackage.BASIC_BLOCK__DEF_EDGES: return ScopPackage.GECOS_SCOP_BLOCK__DEF_EDGES;
				case BlocksPackage.BASIC_BLOCK__OUT_EDGES: return ScopPackage.GECOS_SCOP_BLOCK__OUT_EDGES;
				case BlocksPackage.BASIC_BLOCK__USE_EDGES: return ScopPackage.GECOS_SCOP_BLOCK__USE_EDGES;
				case BlocksPackage.BASIC_BLOCK__IN_EDGES: return ScopPackage.GECOS_SCOP_BLOCK__IN_EDGES;
				case BlocksPackage.BASIC_BLOCK__LABEL: return ScopPackage.GECOS_SCOP_BLOCK__LABEL;
				case BlocksPackage.BASIC_BLOCK__FLAGS: return ScopPackage.GECOS_SCOP_BLOCK__FLAGS;
				default: return -1;
			}
		}
		if (baseClass == CoreVisitable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ScopeContainer.class) {
			switch (baseFeatureID) {
				case CorePackage.SCOPE_CONTAINER__SCOPE: return ScopPackage.GECOS_SCOP_BLOCK__SCOPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //GecosScopBlockImpl
