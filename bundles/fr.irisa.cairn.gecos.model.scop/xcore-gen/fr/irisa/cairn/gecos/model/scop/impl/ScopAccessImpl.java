/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl#getSym <em>Sym</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopAccessImpl#getSymName <em>Sym Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopAccessImpl extends ScopIndexExpressionImpl implements ScopAccess {
	/**
	 * The cached value of the '{@link #getSym() <em>Sym</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSym()
	 * @generated
	 * @ordered
	 */
	protected Symbol sym;

	/**
	 * The default value of the '{@link #getSymName() <em>Sym Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYM_NAME_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_ACCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSym() {
		if (sym != null && sym.eIsProxy()) {
			InternalEObject oldSym = (InternalEObject)sym;
			sym = (Symbol)eResolveProxy(oldSym);
			if (sym != oldSym) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScopPackage.SCOP_ACCESS__SYM, oldSym, sym));
			}
		}
		return sym;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetSym() {
		return sym;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSym(Symbol newSym) {
		Symbol oldSym = sym;
		sym = newSym;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_ACCESS__SYM, oldSym, sym));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymName() {
		Object _xblockexpression = null; {
			Symbol _sym = this.getSym();
			boolean _tripleNotEquals = (_sym != null);
			if (_tripleNotEquals) {
				return this.getSym().getName();
			}
			_xblockexpression = null;
		}
		return ((String)_xblockexpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopAccess copy() {
		return EcoreUtil.<ScopAccess>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopStatement getEnclosingStatement() {
		Instruction _root = super.getRoot();
		return ((ScopStatement) _root);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substituteIndexWith(final ScopDimension dim, final IntExpression e) {
		final EList<IntExpression> exprs = this.getIndexExpressions();
		int _size = exprs.size();
		int _minus = (_size - 1);
		IntegerRange _upTo = new IntegerRange(0, _minus);
		for (final Integer offset : _upTo) {
			{
				final IntExpression substitute = exprs.get((offset).intValue()).substitute(dim, e);
				exprs.set((offset).intValue(), substitute);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_ACCESS__SYM:
				if (resolve) return getSym();
				return basicGetSym();
			case ScopPackage.SCOP_ACCESS__SYM_NAME:
				return getSymName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_ACCESS__SYM:
				setSym((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_ACCESS__SYM:
				setSym((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_ACCESS__SYM:
				return sym != null;
			case ScopPackage.SCOP_ACCESS__SYM_NAME:
				return SYM_NAME_EDEFAULT == null ? getSymName() != null : !SYM_NAME_EDEFAULT.equals(getSymName());
		}
		return super.eIsSet(featureID);
	}

} //ScopAccessImpl
