/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopBlockStatement;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import gecos.blocks.Block;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl#getReads <em>Reads</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl#getWrites <em>Writes</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopBlockStatementImpl#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopBlockStatementImpl extends ScopStatementImpl implements ScopBlockStatement {
	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReads() <em>Reads</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReads()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopRead> reads;

	/**
	 * The cached value of the '{@link #getWrites() <em>Writes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrites()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopWrite> writes;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected Block body;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopBlockStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_BLOCK_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_BLOCK_STATEMENT__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopRead> getReads() {
		if (reads == null) {
			reads = new EObjectContainmentEList<ScopRead>(ScopRead.class, this, ScopPackage.SCOP_BLOCK_STATEMENT__READS);
		}
		return reads;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopWrite> getWrites() {
		if (writes == null) {
			writes = new EObjectContainmentEList<ScopWrite>(ScopWrite.class, this, ScopPackage.SCOP_BLOCK_STATEMENT__WRITES);
		}
		return writes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBody(Block newBody, NotificationChain msgs) {
		Block oldBody = body;
		body = newBody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_BLOCK_STATEMENT__BODY, oldBody, newBody);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBody(Block newBody) {
		if (newBody != body) {
			NotificationChain msgs = null;
			if (body != null)
				msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_BLOCK_STATEMENT__BODY, null, msgs);
			if (newBody != null)
				msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_BLOCK_STATEMENT__BODY, null, msgs);
			msgs = basicSetBody(newBody, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_BLOCK_STATEMENT__BODY, newBody, newBody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return this.getLabel();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(final String txt) {
		this.setLabel(txt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopStatement> listAllStatements() {
		return ECollections.<ScopStatement>toEList(java.util.Collections.<ScopBlockStatement>unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.<ScopBlockStatement>newArrayList(this)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopAccess> listAllAccesses() {
		return ECollections.<ScopAccess>toEList(Iterables.<ScopAccess>filter(this.eContents(), ScopAccess.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopRead> listAllReadAccess() {
		return this.getReads();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopWrite> listAllWriteAccess() {
		return this.getWrites();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _id = this.getId();
		return (_id + ":()");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopBlockStatement copy() {
		return EcoreUtil.<ScopBlockStatement>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_BLOCK_STATEMENT__READS:
				return ((InternalEList<?>)getReads()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_BLOCK_STATEMENT__WRITES:
				return ((InternalEList<?>)getWrites()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_BLOCK_STATEMENT__BODY:
				return basicSetBody(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_BLOCK_STATEMENT__LABEL:
				return getLabel();
			case ScopPackage.SCOP_BLOCK_STATEMENT__READS:
				return getReads();
			case ScopPackage.SCOP_BLOCK_STATEMENT__WRITES:
				return getWrites();
			case ScopPackage.SCOP_BLOCK_STATEMENT__BODY:
				return getBody();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_BLOCK_STATEMENT__LABEL:
				setLabel((String)newValue);
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__READS:
				getReads().clear();
				getReads().addAll((Collection<? extends ScopRead>)newValue);
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__WRITES:
				getWrites().clear();
				getWrites().addAll((Collection<? extends ScopWrite>)newValue);
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__BODY:
				setBody((Block)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_BLOCK_STATEMENT__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__READS:
				getReads().clear();
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__WRITES:
				getWrites().clear();
				return;
			case ScopPackage.SCOP_BLOCK_STATEMENT__BODY:
				setBody((Block)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_BLOCK_STATEMENT__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case ScopPackage.SCOP_BLOCK_STATEMENT__READS:
				return reads != null && !reads.isEmpty();
			case ScopPackage.SCOP_BLOCK_STATEMENT__WRITES:
				return writes != null && !writes.isEmpty();
			case ScopPackage.SCOP_BLOCK_STATEMENT__BODY:
				return body != null;
		}
		return super.eIsSet(featureID);
	}

} //ScopBlockStatementImpl
