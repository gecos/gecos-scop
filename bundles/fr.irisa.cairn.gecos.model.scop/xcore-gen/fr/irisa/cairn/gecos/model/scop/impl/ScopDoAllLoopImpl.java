/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Do All Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ScopDoAllLoopImpl extends ScopForLoopImpl implements ScopDoAllLoop {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopDoAllLoopImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_DO_ALL_LOOP;
	}

} //ScopDoAllLoopImpl
