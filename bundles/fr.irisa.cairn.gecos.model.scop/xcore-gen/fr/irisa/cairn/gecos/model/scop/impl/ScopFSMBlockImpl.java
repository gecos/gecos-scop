/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl#getIterators <em>Iterators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl#getStart <em>Start</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMBlockImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopFSMBlockImpl extends ScopNodeImpl implements ScopFSMBlock {
	/**
	 * The cached value of the '{@link #getIterators() <em>Iterators</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterators()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> iterators;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopFSMTransition> start;

	/**
	 * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopNode> commands;

	/**
	 * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNext()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopFSMTransition> next;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopFSMBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_FSM_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getIterators() {
		if (iterators == null) {
			iterators = new EObjectResolvingEList<ScopDimension>(ScopDimension.class, this, ScopPackage.SCOP_FSM_BLOCK__ITERATORS);
		}
		return iterators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopFSMTransition> getStart() {
		if (start == null) {
			start = new EObjectContainmentEList<ScopFSMTransition>(ScopFSMTransition.class, this, ScopPackage.SCOP_FSM_BLOCK__START);
		}
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopNode> getCommands() {
		if (commands == null) {
			commands = new EObjectContainmentEList<ScopNode>(ScopNode.class, this, ScopPackage.SCOP_FSM_BLOCK__COMMANDS);
		}
		return commands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopFSMTransition> getNext() {
		if (next == null) {
			next = new EObjectContainmentEList<ScopFSMTransition>(ScopFSMTransition.class, this, ScopPackage.SCOP_FSM_BLOCK__NEXT);
		}
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		boolean _contains = this.getCommands().contains(n);
		if (_contains) {
			this.getCommands().remove(n);
		}
		else {
			boolean _contains_1 = this.getNext().contains(n);
			if (_contains_1) {
				this.getNext().remove(n);
			}
			else {
				boolean _contains_2 = this.getStart().contains(n);
				if (_contains_2) {
					this.getStart().remove(n);
				}
				else {
					String _plus = (n + " is not a children of ");
					String _plus_1 = (_plus + this);
					throw new UnsupportedOperationException(_plus_1);
				}
			}
		}
		int _size = this.getCommands().size();
		boolean _equals = (_size == 0);
		if (_equals) {
			this.getParentScop().remove(this);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substitute(final ScopDimension old, final ScopDimension _new) {
		super.substitute(old, _new);
		boolean _contains = this.getIterators().contains(old);
		if (_contains) {
			final Function1<ScopDimension, ScopDimension> _function = new Function1<ScopDimension, ScopDimension>() {
				public ScopDimension apply(final ScopDimension i) {
					ScopDimension _xifexpression = null;
					boolean _equals = Objects.equal(i, old);
					if (_equals) {
						_xifexpression = _new;
					}
					else {
						_xifexpression = i;
					}
					return _xifexpression;
				}
			};
			final List<ScopDimension> newIterators = IterableExtensions.<ScopDimension>toList(XcoreEListExtensions.<ScopDimension, ScopDimension>map(this.getIterators(), _function));
			this.getIterators().clear();
			EList<ScopDimension> _iterators = this.getIterators();
			Iterables.<ScopDimension>addAll(_iterators, newIterators);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_BLOCK__START:
				return ((InternalEList<?>)getStart()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_FSM_BLOCK__COMMANDS:
				return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_FSM_BLOCK__NEXT:
				return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_BLOCK__ITERATORS:
				return getIterators();
			case ScopPackage.SCOP_FSM_BLOCK__START:
				return getStart();
			case ScopPackage.SCOP_FSM_BLOCK__COMMANDS:
				return getCommands();
			case ScopPackage.SCOP_FSM_BLOCK__NEXT:
				return getNext();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_BLOCK__ITERATORS:
				getIterators().clear();
				getIterators().addAll((Collection<? extends ScopDimension>)newValue);
				return;
			case ScopPackage.SCOP_FSM_BLOCK__START:
				getStart().clear();
				getStart().addAll((Collection<? extends ScopFSMTransition>)newValue);
				return;
			case ScopPackage.SCOP_FSM_BLOCK__COMMANDS:
				getCommands().clear();
				getCommands().addAll((Collection<? extends ScopNode>)newValue);
				return;
			case ScopPackage.SCOP_FSM_BLOCK__NEXT:
				getNext().clear();
				getNext().addAll((Collection<? extends ScopFSMTransition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_BLOCK__ITERATORS:
				getIterators().clear();
				return;
			case ScopPackage.SCOP_FSM_BLOCK__START:
				getStart().clear();
				return;
			case ScopPackage.SCOP_FSM_BLOCK__COMMANDS:
				getCommands().clear();
				return;
			case ScopPackage.SCOP_FSM_BLOCK__NEXT:
				getNext().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_BLOCK__ITERATORS:
				return iterators != null && !iterators.isEmpty();
			case ScopPackage.SCOP_FSM_BLOCK__START:
				return start != null && !start.isEmpty();
			case ScopPackage.SCOP_FSM_BLOCK__COMMANDS:
				return commands != null && !commands.isEmpty();
			case ScopPackage.SCOP_FSM_BLOCK__NEXT:
				return next != null && !next.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScopFSMBlockImpl
