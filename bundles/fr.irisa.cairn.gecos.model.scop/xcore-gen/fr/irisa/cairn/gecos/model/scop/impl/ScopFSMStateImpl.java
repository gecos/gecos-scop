/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMState;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMStateImpl#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopFSMStateImpl extends ScopNodeImpl implements ScopFSMState {
	/**
	 * The cached value of the '{@link #getDomain() <em>Domain</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraintSystem> domain;

	/**
	 * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopNode> commands;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopFSMTransition> transitions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopFSMStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_FSM_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraintSystem> getDomain() {
		if (domain == null) {
			domain = new EObjectContainmentEList<IntConstraintSystem>(IntConstraintSystem.class, this, ScopPackage.SCOP_FSM_STATE__DOMAIN);
		}
		return domain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopNode> getCommands() {
		if (commands == null) {
			commands = new EObjectContainmentEList<ScopNode>(ScopNode.class, this, ScopPackage.SCOP_FSM_STATE__COMMANDS);
		}
		return commands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopFSMTransition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<ScopFSMTransition>(ScopFSMTransition.class, this, ScopPackage.SCOP_FSM_STATE__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMBlock getParentFSM() {
		ScopNode _parentScop = this.getParentScop();
		return ((ScopFSMBlock) _parentScop);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listAllEnclosingIterators() {
		EList<ScopDimension> _xblockexpression = null; {
			final ScopFSMBlock fsm = this.getParentFSM();
			EList<ScopDimension> l = fsm.listAllEnclosingIterators();
			l.addAll(0, fsm.getIterators());
			_xblockexpression = l;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		boolean _contains = this.getTransitions().contains(n);
		if (_contains) {
			this.getTransitions().remove(n);
		}
		else {
			String _plus = (n + " is not a children of ");
			String _plus_1 = (_plus + this);
			throw new UnsupportedOperationException(_plus_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_STATE__DOMAIN:
				return ((InternalEList<?>)getDomain()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_FSM_STATE__COMMANDS:
				return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_FSM_STATE__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_STATE__DOMAIN:
				return getDomain();
			case ScopPackage.SCOP_FSM_STATE__COMMANDS:
				return getCommands();
			case ScopPackage.SCOP_FSM_STATE__TRANSITIONS:
				return getTransitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_STATE__DOMAIN:
				getDomain().clear();
				getDomain().addAll((Collection<? extends IntConstraintSystem>)newValue);
				return;
			case ScopPackage.SCOP_FSM_STATE__COMMANDS:
				getCommands().clear();
				getCommands().addAll((Collection<? extends ScopNode>)newValue);
				return;
			case ScopPackage.SCOP_FSM_STATE__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends ScopFSMTransition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_STATE__DOMAIN:
				getDomain().clear();
				return;
			case ScopPackage.SCOP_FSM_STATE__COMMANDS:
				getCommands().clear();
				return;
			case ScopPackage.SCOP_FSM_STATE__TRANSITIONS:
				getTransitions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_STATE__DOMAIN:
				return domain != null && !domain.isEmpty();
			case ScopPackage.SCOP_FSM_STATE__COMMANDS:
				return commands != null && !commands.isEmpty();
			case ScopPackage.SCOP_FSM_STATE__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScopFSMStateImpl
