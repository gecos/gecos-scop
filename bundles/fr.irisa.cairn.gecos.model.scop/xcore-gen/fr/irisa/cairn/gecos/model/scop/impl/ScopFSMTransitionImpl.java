/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopFSMState;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl#getNextIteration <em>Next Iteration</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopFSMTransitionImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopFSMTransitionImpl extends ScopNodeImpl implements ScopFSMTransition {
	/**
	 * The cached value of the '{@link #getDomain() <em>Domain</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraintSystem> domain;

	/**
	 * The cached value of the '{@link #getNextIteration() <em>Next Iteration</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextIteration()
	 * @generated
	 * @ordered
	 */
	protected EList<IntExpression> nextIteration;

	/**
	 * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNext()
	 * @generated
	 * @ordered
	 */
	protected ScopFSMState next;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopFSMTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_FSM_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraintSystem> getDomain() {
		if (domain == null) {
			domain = new EObjectContainmentEList<IntConstraintSystem>(IntConstraintSystem.class, this, ScopPackage.SCOP_FSM_TRANSITION__DOMAIN);
		}
		return domain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntExpression> getNextIteration() {
		if (nextIteration == null) {
			nextIteration = new EObjectContainmentEList<IntExpression>(IntExpression.class, this, ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION);
		}
		return nextIteration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMState getNext() {
		if (next != null && next.eIsProxy()) {
			InternalEObject oldNext = (InternalEObject)next;
			next = (ScopFSMState)eResolveProxy(oldNext);
			if (next != oldNext) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScopPackage.SCOP_FSM_TRANSITION__NEXT, oldNext, next));
			}
		}
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMState basicGetNext() {
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNext(ScopFSMState newNext) {
		ScopFSMState oldNext = next;
		next = newNext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FSM_TRANSITION__NEXT, oldNext, next));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_TRANSITION__DOMAIN:
				return ((InternalEList<?>)getDomain()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION:
				return ((InternalEList<?>)getNextIteration()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_TRANSITION__DOMAIN:
				return getDomain();
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION:
				return getNextIteration();
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT:
				if (resolve) return getNext();
				return basicGetNext();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_TRANSITION__DOMAIN:
				getDomain().clear();
				getDomain().addAll((Collection<? extends IntConstraintSystem>)newValue);
				return;
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION:
				getNextIteration().clear();
				getNextIteration().addAll((Collection<? extends IntExpression>)newValue);
				return;
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT:
				setNext((ScopFSMState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_TRANSITION__DOMAIN:
				getDomain().clear();
				return;
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION:
				getNextIteration().clear();
				return;
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT:
				setNext((ScopFSMState)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FSM_TRANSITION__DOMAIN:
				return domain != null && !domain.isEmpty();
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT_ITERATION:
				return nextIteration != null && !nextIteration.isEmpty();
			case ScopPackage.SCOP_FSM_TRANSITION__NEXT:
				return next != null;
		}
		return super.eIsSet(featureID);
	}

} //ScopFSMTransitionImpl
