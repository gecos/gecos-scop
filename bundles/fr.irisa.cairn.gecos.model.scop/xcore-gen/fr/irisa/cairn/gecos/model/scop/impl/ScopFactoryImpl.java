/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScopFactoryImpl extends EFactoryImpl implements ScopFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScopFactory init() {
		try {
			ScopFactory theScopFactory = (ScopFactory)EPackage.Registry.INSTANCE.getEFactory(ScopPackage.eNS_URI);
			if (theScopFactory != null) {
				return theScopFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScopFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScopPackage.SCOP_DIMENSION: return createScopDimension();
			case ScopPackage.SCOP_PARAMETER: return createScopParameter();
			case ScopPackage.GECOS_SCOP_BLOCK: return createGecosScopBlock();
			case ScopPackage.SCOP_GUARD: return createScopGuard();
			case ScopPackage.SCOP_INDEX_EXPRESSION: return createScopIndexExpression();
			case ScopPackage.SCOP_ACCESS: return createScopAccess();
			case ScopPackage.SCOP_READ: return createScopRead();
			case ScopPackage.SCOP_REGION_READ: return createScopRegionRead();
			case ScopPackage.SCOP_REGION_WRITE: return createScopRegionWrite();
			case ScopPackage.SCOP_WRITE: return createScopWrite();
			case ScopPackage.SCOP_INT_EXPRESSION: return createScopIntExpression();
			case ScopPackage.SCOP_INT_CONSTRAINT: return createScopIntConstraint();
			case ScopPackage.SCOP_INT_CONSTRAINT_SYSTEM: return createScopIntConstraintSystem();
			case ScopPackage.SCOP_STATEMENT: return createScopStatement();
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT: return createScopInstructionStatement();
			case ScopPackage.SCOP_BLOCK_STATEMENT: return createScopBlockStatement();
			case ScopPackage.SCOP_BLOCK: return createScopBlock();
			case ScopPackage.SCOP_FOR_LOOP: return createScopForLoop();
			case ScopPackage.SCOP_DO_ALL_LOOP: return createScopDoAllLoop();
			case ScopPackage.SCOP_ASYNC_BLOCK: return createscopAsyncBlock();
			case ScopPackage.SCOP_FSM_BLOCK: return createScopFSMBlock();
			case ScopPackage.SCOP_FSM_TRANSITION: return createScopFSMTransition();
			case ScopPackage.SCOP_FSM_STATE: return createScopFSMState();
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT: return createScopUnexpandedStatement();
			case ScopPackage.DEF_USE_EDGE: return createDefUseEdge();
			case ScopPackage.USE_DEF_EDGE: return createUseDefEdge();
			case ScopPackage.WAW_EDGE: return createWAWEdge();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ScopPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case ScopPackage.INT:
				return createintFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ScopPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case ScopPackage.INT:
				return convertintToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDimension createScopDimension() {
		ScopDimensionImpl scopDimension = new ScopDimensionImpl();
		return scopDimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopParameter createScopParameter() {
		ScopParameterImpl scopParameter = new ScopParameterImpl();
		return scopParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosScopBlock createGecosScopBlock() {
		GecosScopBlockImpl gecosScopBlock = new GecosScopBlockImpl();
		return gecosScopBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopGuard createScopGuard() {
		ScopGuardImpl scopGuard = new ScopGuardImpl();
		return scopGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopIndexExpression createScopIndexExpression() {
		ScopIndexExpressionImpl scopIndexExpression = new ScopIndexExpressionImpl();
		return scopIndexExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopAccess createScopAccess() {
		ScopAccessImpl scopAccess = new ScopAccessImpl();
		return scopAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopRead createScopRead() {
		ScopReadImpl scopRead = new ScopReadImpl();
		return scopRead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopRegionRead createScopRegionRead() {
		ScopRegionReadImpl scopRegionRead = new ScopRegionReadImpl();
		return scopRegionRead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopRegionWrite createScopRegionWrite() {
		ScopRegionWriteImpl scopRegionWrite = new ScopRegionWriteImpl();
		return scopRegionWrite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopWrite createScopWrite() {
		ScopWriteImpl scopWrite = new ScopWriteImpl();
		return scopWrite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopIntExpression createScopIntExpression() {
		ScopIntExpressionImpl scopIntExpression = new ScopIntExpressionImpl();
		return scopIntExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopIntConstraint createScopIntConstraint() {
		ScopIntConstraintImpl scopIntConstraint = new ScopIntConstraintImpl();
		return scopIntConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopIntConstraintSystem createScopIntConstraintSystem() {
		ScopIntConstraintSystemImpl scopIntConstraintSystem = new ScopIntConstraintSystemImpl();
		return scopIntConstraintSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopStatement createScopStatement() {
		ScopStatementImpl scopStatement = new ScopStatementImpl();
		return scopStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopInstructionStatement createScopInstructionStatement() {
		ScopInstructionStatementImpl scopInstructionStatement = new ScopInstructionStatementImpl();
		return scopInstructionStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopBlock createScopBlock() {
		ScopBlockImpl scopBlock = new ScopBlockImpl();
		return scopBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopForLoop createScopForLoop() {
		ScopForLoopImpl scopForLoop = new ScopForLoopImpl();
		return scopForLoop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDoAllLoop createScopDoAllLoop() {
		ScopDoAllLoopImpl scopDoAllLoop = new ScopDoAllLoopImpl();
		return scopDoAllLoop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public scopAsyncBlock createscopAsyncBlock() {
		scopAsyncBlockImpl scopAsyncBlock = new scopAsyncBlockImpl();
		return scopAsyncBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMBlock createScopFSMBlock() {
		ScopFSMBlockImpl scopFSMBlock = new ScopFSMBlockImpl();
		return scopFSMBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMTransition createScopFSMTransition() {
		ScopFSMTransitionImpl scopFSMTransition = new ScopFSMTransitionImpl();
		return scopFSMTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFSMState createScopFSMState() {
		ScopFSMStateImpl scopFSMState = new ScopFSMStateImpl();
		return scopFSMState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopUnexpandedStatement createScopUnexpandedStatement() {
		ScopUnexpandedStatementImpl scopUnexpandedStatement = new ScopUnexpandedStatementImpl();
		return scopUnexpandedStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopBlockStatement createScopBlockStatement() {
		ScopBlockStatementImpl scopBlockStatement = new ScopBlockStatementImpl();
		return scopBlockStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefUseEdge createDefUseEdge() {
		DefUseEdgeImpl defUseEdge = new DefUseEdgeImpl();
		return defUseEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseDefEdge createUseDefEdge() {
		UseDefEdgeImpl useDefEdge = new UseDefEdgeImpl();
		return useDefEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WAWEdge createWAWEdge() {
		WAWEdgeImpl wawEdge = new WAWEdgeImpl();
		return wawEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopPackage getScopPackage() {
		return (ScopPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScopPackage getPackage() {
		return ScopPackage.eINSTANCE;
	}

} //ScopFactoryImpl
