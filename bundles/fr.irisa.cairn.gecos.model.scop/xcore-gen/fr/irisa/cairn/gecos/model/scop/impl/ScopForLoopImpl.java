/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Loop</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl#getIterator <em>Iterator</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl#getLB <em>LB</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl#getUB <em>UB</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl#getStride <em>Stride</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopForLoopImpl#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopForLoopImpl extends ScopNodeImpl implements ScopForLoop {
	/**
	 * The cached value of the '{@link #getIterator() <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterator()
	 * @generated
	 * @ordered
	 */
	protected ScopDimension iterator;

	/**
	 * The cached value of the '{@link #getLB() <em>LB</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLB()
	 * @generated
	 * @ordered
	 */
	protected IntExpression lb;

	/**
	 * The cached value of the '{@link #getUB() <em>UB</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUB()
	 * @generated
	 * @ordered
	 */
	protected IntExpression ub;

	/**
	 * The cached value of the '{@link #getStride() <em>Stride</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStride()
	 * @generated
	 * @ordered
	 */
	protected IntExpression stride;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected ScopNode body;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopForLoopImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_FOR_LOOP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDimension getIterator() {
		if (iterator != null && iterator.eIsProxy()) {
			InternalEObject oldIterator = (InternalEObject)iterator;
			iterator = (ScopDimension)eResolveProxy(oldIterator);
			if (iterator != oldIterator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScopPackage.SCOP_FOR_LOOP__ITERATOR, oldIterator, iterator));
			}
		}
		return iterator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDimension basicGetIterator() {
		return iterator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIterator(ScopDimension newIterator) {
		ScopDimension oldIterator = iterator;
		iterator = newIterator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__ITERATOR, oldIterator, iterator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getLB() {
		return lb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLB(IntExpression newLB, NotificationChain msgs) {
		IntExpression oldLB = lb;
		lb = newLB;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__LB, oldLB, newLB);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLB(IntExpression newLB) {
		if (newLB != lb) {
			NotificationChain msgs = null;
			if (lb != null)
				msgs = ((InternalEObject)lb).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__LB, null, msgs);
			if (newLB != null)
				msgs = ((InternalEObject)newLB).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__LB, null, msgs);
			msgs = basicSetLB(newLB, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__LB, newLB, newLB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getUB() {
		return ub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUB(IntExpression newUB, NotificationChain msgs) {
		IntExpression oldUB = ub;
		ub = newUB;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__UB, oldUB, newUB);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUB(IntExpression newUB) {
		if (newUB != ub) {
			NotificationChain msgs = null;
			if (ub != null)
				msgs = ((InternalEObject)ub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__UB, null, msgs);
			if (newUB != null)
				msgs = ((InternalEObject)newUB).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__UB, null, msgs);
			msgs = basicSetUB(newUB, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__UB, newUB, newUB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getStride() {
		return stride;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStride(IntExpression newStride, NotificationChain msgs) {
		IntExpression oldStride = stride;
		stride = newStride;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__STRIDE, oldStride, newStride);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStride(IntExpression newStride) {
		if (newStride != stride) {
			NotificationChain msgs = null;
			if (stride != null)
				msgs = ((InternalEObject)stride).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__STRIDE, null, msgs);
			if (newStride != null)
				msgs = ((InternalEObject)newStride).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__STRIDE, null, msgs);
			msgs = basicSetStride(newStride, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__STRIDE, newStride, newStride));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBody(ScopNode newBody, NotificationChain msgs) {
		ScopNode oldBody = body;
		body = newBody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__BODY, oldBody, newBody);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBody(ScopNode newBody) {
		if (newBody != body) {
			NotificationChain msgs = null;
			if (body != null)
				msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__BODY, null, msgs);
			if (newBody != null)
				msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_FOR_LOOP__BODY, null, msgs);
			msgs = basicSetBody(newBody, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_FOR_LOOP__BODY, newBody, newBody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		ScopNode _body = this.getBody();
		boolean _equals = Objects.equal(_body, n);
		if (_equals) {
			this.setBody(null);
			this.getParentScop().remove(this);
		}
		else {
			String _plus = (n + " is not a children of ");
			String _plus_1 = (_plus + this);
			throw new UnsupportedOperationException(_plus_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final ScopNode n, final ScopNode _new) {
		ScopNode _body = this.getBody();
		boolean _equals = Objects.equal(_body, n);
		if (_equals) {
			this.setBody(_new);
		}
		else {
			String _plus = (n + " is not a children of ");
			String _plus_1 = (_plus + this);
			throw new UnsupportedOperationException(_plus_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfEnclosingDimension() {
		ScopNode _parentScop = this.getParentScop();
		boolean _tripleNotEquals = (_parentScop != null);
		if (_tripleNotEquals) {
			int _numberOfEnclosingDimension = this.getParentScop().getNumberOfEnclosingDimension();
			return (1 + _numberOfEnclosingDimension);
		}
		else {
			return 1;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfEnclosedDimension() {
		int _numberOfEnclosedDimension = this.getBody().getNumberOfEnclosedDimension();
		return (_numberOfEnclosedDimension + 1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substitute(final ScopDimension old, final ScopDimension _new) {
		super.substitute(old, _new);
		ScopDimension _iterator = this.getIterator();
		boolean _tripleEquals = (_iterator == old);
		if (_tripleEquals) {
			this.setIterator(_new);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_FOR_LOOP__LB:
				return basicSetLB(null, msgs);
			case ScopPackage.SCOP_FOR_LOOP__UB:
				return basicSetUB(null, msgs);
			case ScopPackage.SCOP_FOR_LOOP__STRIDE:
				return basicSetStride(null, msgs);
			case ScopPackage.SCOP_FOR_LOOP__BODY:
				return basicSetBody(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_FOR_LOOP__ITERATOR:
				if (resolve) return getIterator();
				return basicGetIterator();
			case ScopPackage.SCOP_FOR_LOOP__LB:
				return getLB();
			case ScopPackage.SCOP_FOR_LOOP__UB:
				return getUB();
			case ScopPackage.SCOP_FOR_LOOP__STRIDE:
				return getStride();
			case ScopPackage.SCOP_FOR_LOOP__BODY:
				return getBody();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_FOR_LOOP__ITERATOR:
				setIterator((ScopDimension)newValue);
				return;
			case ScopPackage.SCOP_FOR_LOOP__LB:
				setLB((IntExpression)newValue);
				return;
			case ScopPackage.SCOP_FOR_LOOP__UB:
				setUB((IntExpression)newValue);
				return;
			case ScopPackage.SCOP_FOR_LOOP__STRIDE:
				setStride((IntExpression)newValue);
				return;
			case ScopPackage.SCOP_FOR_LOOP__BODY:
				setBody((ScopNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FOR_LOOP__ITERATOR:
				setIterator((ScopDimension)null);
				return;
			case ScopPackage.SCOP_FOR_LOOP__LB:
				setLB((IntExpression)null);
				return;
			case ScopPackage.SCOP_FOR_LOOP__UB:
				setUB((IntExpression)null);
				return;
			case ScopPackage.SCOP_FOR_LOOP__STRIDE:
				setStride((IntExpression)null);
				return;
			case ScopPackage.SCOP_FOR_LOOP__BODY:
				setBody((ScopNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_FOR_LOOP__ITERATOR:
				return iterator != null;
			case ScopPackage.SCOP_FOR_LOOP__LB:
				return lb != null;
			case ScopPackage.SCOP_FOR_LOOP__UB:
				return ub != null;
			case ScopPackage.SCOP_FOR_LOOP__STRIDE:
				return stride != null;
			case ScopPackage.SCOP_FOR_LOOP__BODY:
				return body != null;
		}
		return super.eIsSet(featureID);
	}

} //ScopForLoopImpl
