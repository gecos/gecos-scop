/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl#getPredicate <em>Predicate</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl#getThenNode <em>Then Node</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopGuardImpl#getElseNode <em>Else Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopGuardImpl extends ScopNodeImpl implements ScopGuard {
	/**
	 * The cached value of the '{@link #getPredicate() <em>Predicate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredicate()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraintSystem> predicate;

	/**
	 * The cached value of the '{@link #getThenNode() <em>Then Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenNode()
	 * @generated
	 * @ordered
	 */
	protected ScopNode thenNode;

	/**
	 * The cached value of the '{@link #getElseNode() <em>Else Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseNode()
	 * @generated
	 * @ordered
	 */
	protected ScopNode elseNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopGuardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_GUARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraintSystem> getPredicate() {
		if (predicate == null) {
			predicate = new EObjectContainmentEList<IntConstraintSystem>(IntConstraintSystem.class, this, ScopPackage.SCOP_GUARD__PREDICATE);
		}
		return predicate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getThenNode() {
		return thenNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenNode(ScopNode newThenNode, NotificationChain msgs) {
		ScopNode oldThenNode = thenNode;
		thenNode = newThenNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_GUARD__THEN_NODE, oldThenNode, newThenNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenNode(ScopNode newThenNode) {
		if (newThenNode != thenNode) {
			NotificationChain msgs = null;
			if (thenNode != null)
				msgs = ((InternalEObject)thenNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_GUARD__THEN_NODE, null, msgs);
			if (newThenNode != null)
				msgs = ((InternalEObject)newThenNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_GUARD__THEN_NODE, null, msgs);
			msgs = basicSetThenNode(newThenNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_GUARD__THEN_NODE, newThenNode, newThenNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getElseNode() {
		return elseNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseNode(ScopNode newElseNode, NotificationChain msgs) {
		ScopNode oldElseNode = elseNode;
		elseNode = newElseNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_GUARD__ELSE_NODE, oldElseNode, newElseNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseNode(ScopNode newElseNode) {
		if (newElseNode != elseNode) {
			NotificationChain msgs = null;
			if (elseNode != null)
				msgs = ((InternalEObject)elseNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_GUARD__ELSE_NODE, null, msgs);
			if (newElseNode != null)
				msgs = ((InternalEObject)newElseNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_GUARD__ELSE_NODE, null, msgs);
			msgs = basicSetElseNode(newElseNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_GUARD__ELSE_NODE, newElseNode, newElseNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		ScopNode _thenNode = this.getThenNode();
		boolean _equals = Objects.equal(_thenNode, n);
		if (_equals) {
			this.setThenNode(null);
		}
		else {
			ScopNode _elseNode = this.getElseNode();
			boolean _equals_1 = Objects.equal(_elseNode, n);
			if (_equals_1) {
				this.setElseNode(null);
			}
			else {
				String _plus = (n + " is not a children of ");
				String _plus_1 = (_plus + this);
				throw new UnsupportedOperationException(_plus_1);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final ScopNode n, final ScopNode _new) {
		ScopNode _thenNode = this.getThenNode();
		boolean _equals = Objects.equal(_thenNode, n);
		if (_equals) {
			this.setThenNode(_new);
		}
		else {
			ScopNode _elseNode = this.getElseNode();
			boolean _equals_1 = Objects.equal(_elseNode, n);
			if (_equals_1) {
				this.setElseNode(_new);
			}
			else {
				String _plus = (n + " is not a children of ");
				String _plus_1 = (_plus + this);
				throw new UnsupportedOperationException(_plus_1);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int maxDepth() {
		ScopNode _elseNode = this.getElseNode();
		boolean _tripleNotEquals = (_elseNode != null);
		if (_tripleNotEquals) {
			return Math.max(this.getThenNode().getNumberOfEnclosedDimension(), this.getElseNode().getNumberOfEnclosedDimension());
		}
		else {
			return this.getThenNode().getNumberOfEnclosedDimension();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_GUARD__PREDICATE:
				return ((InternalEList<?>)getPredicate()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_GUARD__THEN_NODE:
				return basicSetThenNode(null, msgs);
			case ScopPackage.SCOP_GUARD__ELSE_NODE:
				return basicSetElseNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_GUARD__PREDICATE:
				return getPredicate();
			case ScopPackage.SCOP_GUARD__THEN_NODE:
				return getThenNode();
			case ScopPackage.SCOP_GUARD__ELSE_NODE:
				return getElseNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_GUARD__PREDICATE:
				getPredicate().clear();
				getPredicate().addAll((Collection<? extends IntConstraintSystem>)newValue);
				return;
			case ScopPackage.SCOP_GUARD__THEN_NODE:
				setThenNode((ScopNode)newValue);
				return;
			case ScopPackage.SCOP_GUARD__ELSE_NODE:
				setElseNode((ScopNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_GUARD__PREDICATE:
				getPredicate().clear();
				return;
			case ScopPackage.SCOP_GUARD__THEN_NODE:
				setThenNode((ScopNode)null);
				return;
			case ScopPackage.SCOP_GUARD__ELSE_NODE:
				setElseNode((ScopNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_GUARD__PREDICATE:
				return predicate != null && !predicate.isEmpty();
			case ScopPackage.SCOP_GUARD__THEN_NODE:
				return thenNode != null;
			case ScopPackage.SCOP_GUARD__ELSE_NODE:
				return elseNode != null;
		}
		return super.eIsSet(featureID);
	}

} //ScopGuardImpl
