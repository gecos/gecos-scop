/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopIndexExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import gecos.blocks.BasicBlock;
import gecos.core.ITypedElement;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitable;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.types.Type;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Index Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl#get___internal_cached___type <em>internal cached type</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopIndexExpressionImpl#getIndexExpressions <em>Index Expressions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopIndexExpressionImpl extends ScopNodeImpl implements ScopIndexExpression {
	/**
	 * The cached value of the '{@link #get___internal_cached___type() <em>internal cached type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #get___internal_cached___type()
	 * @generated
	 * @ordered
	 */
	protected Type ___internal_cached___type;
	/**
	 * The cached value of the '{@link #getIndexExpressions() <em>Index Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<IntExpression> indexExpressions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopIndexExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_INDEX_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type get___internal_cached___type() {
		return ___internal_cached___type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void set___internal_cached___type(Type new___internal_cached___type) {
		Type old___internal_cached___type = ___internal_cached___type;
		___internal_cached___type = new___internal_cached___type;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE, old___internal_cached___type, ___internal_cached___type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntExpression> getIndexExpressions() {
		if (indexExpressions == null) {
			indexExpressions = new EObjectContainmentEList<IntExpression>(IntExpression.class, this, ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS);
		}
		return indexExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getParentScop() {
		Instruction _root = this.getRoot();
		return ((ScopStatement) _root);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopIndexExpression copy() {
		return EcoreUtil.<ScopIndexExpression>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexInstruction getParent() {
		EObject cont = this.eContainer();
		while (((!(cont instanceof ComplexInstruction)) && (cont instanceof Instruction))) {
			cont = cont.eContainer();
		}
		if ((!(cont instanceof ComplexInstruction))) {
			return null;
		}
		return ((ComplexInstruction) cont);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		Type ____internal_cached___type = this.get___internal_cached___type();
		boolean _tripleEquals = (____internal_cached___type == null);
		if (_tripleEquals) {
			try {
				final Type t = this.computeType();
				if ((t == null)) {
					return null;
				}
				if (((this.getBasicBlock() != null) && (this.getBasicBlock().getScope() != null))) {
					final Scope scope = this.getBasicBlock().getScope();
					final Type existingType = scope.lookup(t);
					if ((existingType == null)) {
						this.setType(t);
						scope.getTypes().add(t);
					}
					else {
						this.setType(existingType);
					}
				}
				else {
					return t;
				}
			}
			catch (final Throwable _t) {
				if (_t instanceof Exception) {
					return null;
				}
				else {
					throw Exceptions.sneakyThrow(_t);
				}
			}
		}
		return this.get___internal_cached___type();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(final Type t) {
		this.set___internal_cached___type(t);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		boolean isSame = false;
		if (((instr.getType() == null) && (this.getType() == null))) {
			isSame = true;
		}
		else {
			Type _type = instr.getType();
			boolean _tripleEquals = (_type == null);
			Type _type_1 = this.getType();
			boolean _tripleEquals_1 = (_type_1 == null);
			boolean _xor = (_tripleEquals ^ _tripleEquals_1);
			if (_xor) {
				return false;
			}
			else {
				isSame = instr.getType().isEqual(this.getType());
			}
		}
		return isSame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = (_simpleName + " not supported by ");
		String _simpleName_1 = visitor.getClass().getSimpleName();
		String _plus_1 = (_plus + _simpleName_1);
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceChild(final Instruction s, final Instruction value) {
		throw new UnsupportedOperationException(("Not applicable to " + s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getContainingProcedure() {
		EObject container = this.eContainer();
		while ((container != null)) {
			if ((container instanceof Procedure)) {
				return ((Procedure)container);
			}
			else {
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getContainingProcedureSet() {
		Procedure _containingProcedure = this.getContainingProcedure();
		ProcedureSet _containingProcedureSet = null;
		if (_containingProcedure!=null) {
			_containingProcedureSet=_containingProcedure.getContainingProcedureSet();
		}
		return _containingProcedureSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getRoot() {
		ComplexInstruction _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			return this.getParent().getRoot();
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getBasicBlock() {
		EObject container = this.eContainer();
		while ((container != null)) {
			{
				if ((container instanceof BasicBlock)) {
					return ((BasicBlock)container);
				}
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstant() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUnconditionalBranch() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRet() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBranch() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSymbol() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substituteWith(final Instruction inst) {
		ComplexInstruction _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			this.getParent().replaceChild(this, inst);
		}
		else {
			if (((this.getBasicBlock() != null) && this.getBasicBlock().getInstructions().contains(this))) {
				this.getBasicBlock().replaceInstruction(this, inst);
			}
			else {
				EObject _eContainer = this.eContainer();
				boolean _tripleNotEquals_1 = (_eContainer != null);
				if (_tripleNotEquals_1) {
					final EObject container = this.eContainer();
					final EStructuralFeature feature = this.eContainingFeature();
					boolean _isMany = feature.isMany();
					if (_isMany) {
						Object _eGet = container.eGet(feature);
						final EList<EObject> list = ((EList<EObject>) _eGet);
						boolean _contains = list.contains(this);
						boolean _not = (!_contains);
						if (_not) {
							throw new RuntimeException((((("Cannot find [" + this) + "] in the structural features of [") + container) + "]."));
						}
						list.set(list.indexOf(this), inst);
					}
					else {
						container.eSet(feature, inst);
					}
				}
				else {
					throw new UnsupportedOperationException((((("Cannot substitute [" + this) + "] with [") + inst) + "] as this latter one is not contained."));
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove() {
		EObject _eContainer = this.eContainer();
		boolean _tripleEquals = (_eContainer == null);
		if (_tripleEquals) {
			throw new UnsupportedOperationException((("Cannot remove [" + this) + "] as it is not contained."));
		}
		EcoreUtil.remove(((EObject) this));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS:
				return ((InternalEList<?>)getIndexExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE:
				return get___internal_cached___type();
			case ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS:
				return getIndexExpressions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE:
				set___internal_cached___type((Type)newValue);
				return;
			case ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS:
				getIndexExpressions().clear();
				getIndexExpressions().addAll((Collection<? extends IntExpression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE:
				set___internal_cached___type((Type)null);
				return;
			case ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS:
				getIndexExpressions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE:
				return ___internal_cached___type != null;
			case ScopPackage.SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS:
				return indexExpressions != null && !indexExpressions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == InstrsVisitable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ITypedElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Instruction.class) {
			switch (derivedFeatureID) {
				case ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE: return InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == InstrsVisitable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ITypedElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Instruction.class) {
			switch (baseFeatureID) {
				case InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE: return ScopPackage.SCOP_INDEX_EXPRESSION__INTERNAL_CACHED_TYPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ScopIndexExpressionImpl
