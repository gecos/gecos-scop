/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;

import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Symbol;
import gecos.instrs.impl.GenericInstructionImpl;
import java.lang.Iterable;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;

import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instruction Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopInstructionStatementImpl#getTransform <em>Transform</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopInstructionStatementImpl extends GenericInstructionImpl implements ScopInstructionStatement {
	/**
	 * The cached value of the '{@link #getTransform() <em>Transform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransform()
	 * @generated
	 * @ordered
	 */
	protected ScopTransform transform;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopInstructionStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_INSTRUCTION_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopTransform getTransform() {
		return transform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransform(ScopTransform newTransform, NotificationChain msgs) {
		ScopTransform oldTransform = transform;
		transform = newTransform;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM, oldTransform, newTransform);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransform(ScopTransform newTransform) {
		if (newTransform != transform) {
			NotificationChain msgs = null;
			if (transform != null)
				msgs = ((InternalEObject)transform).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM, null, msgs);
			if (newTransform != null)
				msgs = ((InternalEObject)newTransform).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM, null, msgs);
			msgs = basicSetTransform(newTransform, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM, newTransform, newTransform));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return this.getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(final String txt) {
		this.setName(txt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopStatement> listAllStatements() {
		return ECollections.<ScopStatement>toEList(java.util.Collections.<ScopInstructionStatement>unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.<ScopInstructionStatement>newArrayList(this)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopAccess> listAllAccesses() {
		return ECollections.<ScopAccess>toEList(Iterators.<ScopAccess>filter(this.eAllContents(), ScopAccess.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xifexpression = null;
		int _childrenCount = this.getChildrenCount();
		boolean _greaterThan = (_childrenCount > 0);
		if (_greaterThan) {
			String _name = this.getName();
			String _plus = (_name + ":");
			String _string = this.getChildren().get(0).toString();
			_xifexpression = (_plus + _string);
		}
		else {
			String _name_1 = this.getName();
			_xifexpression = (_name_1 + ":()");
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopInstructionStatement copy() {
		return EcoreUtil.<ScopInstructionStatement>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean useSymbol(final Symbol s) {
		final Function1<ScopAccess, Boolean> _function = new Function1<ScopAccess, Boolean>() {
			public Boolean apply(final ScopAccess a) {
				Symbol _sym = a.getSym();
				return Boolean.valueOf(Objects.equal(_sym, s));
			}
		};
		return IterableExtensions.<ScopAccess>exists(this.listAllAccesses(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllSymbolReferences() {
		final Function1<ScopAccess, Symbol> _function = new Function1<ScopAccess, Symbol>() {
			public Symbol apply(final ScopAccess a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopAccess, Symbol>map(this.listAllAccesses(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllReadSymbols() {
		final Function1<ScopRead, Symbol> _function = new Function1<ScopRead, Symbol>() {
			public Symbol apply(final ScopRead a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopRead, Symbol>map(this.listAllReadAccess(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllWriteSymbols() {
		final Function1<ScopWrite, Symbol> _function = new Function1<ScopWrite, Symbol>() {
			public Symbol apply(final ScopWrite a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopWrite, Symbol>map(this.listAllWriteAccess(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getParentScop() {
		final EObject econtainer = this.eContainer();
		if ((econtainer instanceof ScopNode)) {
			return ((ScopNode)econtainer);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopNode getScopRoot() {
		ScopNode _parentScop = this.getParentScop();
		boolean _tripleEquals = (_parentScop == null);
		if (_tripleEquals) {
			return this;
		}
		else {
			return this.getParentScop().getScopRoot();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopStatement getStatement(final String name) {
		final Function1<ScopStatement, Boolean> _function = new Function1<ScopStatement, Boolean>() {
			public Boolean apply(final ScopStatement stmt) {
				String _id = stmt.getId();
				return Boolean.valueOf(Objects.equal(_id, name));
			}
		};
		return IterableExtensions.<ScopStatement>findFirst(this.listAllStatements(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopRead> listAllReadAccess() {
		return ECollections.<ScopRead>toEList(Iterators.<ScopRead>filter(this.eAllContents(), ScopRead.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllReferencedSymbols() {
		final Function1<ScopAccess, Symbol> _function = new Function1<ScopAccess, Symbol>() {
			public Symbol apply(final ScopAccess a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopAccess, Symbol>map(this.listAllAccesses(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopWrite> listAllWriteAccess() {
		return ECollections.<ScopWrite>toEList(Iterators.<ScopWrite>filter(this.eAllContents(), ScopWrite.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopNode> listChildrenScopNodes() {
		return ECollections.<ScopNode>toEList(Iterables.<ScopNode>filter(this.eContents(), ScopNode.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopNode> listAllChildredScopNodes() {
		return ECollections.<ScopNode>toEList(Iterators.<ScopNode>filter(this.eAllContents(), ScopNode.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listAllParameters() {
		BasicEList<ScopDimension> _xblockexpression = null; {
			final BasicEList<ScopDimension> list = new BasicEList<ScopDimension>();
			list.addAll(this.listRootParameters());
			list.addAll(this.listAllEnclosingIterators());
			_xblockexpression = list;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listRootParameters() {
		EList<ScopDimension> _xblockexpression = null; {
			ScopNode _scopRoot = this.getScopRoot();
			final GecosScopBlock blk = ((GecosScopBlock) _scopRoot);
			_xblockexpression = blk.getParameters();
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listRootIterators() {
		EList<ScopDimension> _xblockexpression = null; {
			ScopNode _scopRoot = this.getScopRoot();
			final GecosScopBlock blk = ((GecosScopBlock) _scopRoot);
			_xblockexpression = blk.getIterators();
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopTransformDirective> listNodeDirectives() {
		EList<ScopTransformDirective> _xifexpression = null;
		ScopTransform _transform = this.getTransform();
		boolean _tripleNotEquals = (_transform != null);
		if (_tripleNotEquals) {
			_xifexpression = this.getTransform().getCommands();
		}
		else {
			_xifexpression = ECollections.<ScopTransformDirective>toEList(java.util.Collections.<ScopTransformDirective>unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.<ScopTransformDirective>newArrayList()));
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listAllEnclosingIterators(final ScopNode relativeRoot) {
		final BasicEList<ScopDimension> list = new BasicEList<ScopDimension>();
		ScopNode parent = relativeRoot;
		ScopNode _parentScop = relativeRoot.getParentScop();
		boolean _tripleNotEquals = (_parentScop != null);
		if (_tripleNotEquals) {
			parent = relativeRoot.getParentScop();
		}
		ScopForLoop forLoop = this.getEnclosingForLoop(parent);
		while ((forLoop != null)) {
			{
				list.add(forLoop.getIterator());
				forLoop = forLoop.getEnclosingForLoop(parent);
			}
		}
		return XcoreEListExtensions.<ScopDimension>reverse(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listAllEnclosingIterators() {
		return this.listAllEnclosingIterators(this.getScopRoot());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> listAllFreeIterators() {
		BasicEList<ScopDimension> _xblockexpression = null; {
			final BasicEList<ScopDimension> list = new BasicEList<ScopDimension>();
			list.addAll(this.listRootIterators());
			list.removeAll(this.listAllEnclosingIterators());
			_xblockexpression = list;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopForLoop getEnclosingForLoop() {
		return this.getEnclosingForLoop(this.getScopRoot());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopForLoop getEnclosingForLoop(final ScopNode relativeRoot) {
		boolean _equals = Objects.equal(this, relativeRoot);
		if (_equals) {
			return null;
		}
		ScopNode parent = this.getParentScop();
		while (((parent != null) && (!Objects.equal(parent, relativeRoot)))) {
			{
				if ((parent instanceof ScopForLoop)) {
					return ((ScopForLoop)parent);
				}
				parent = parent.getParentScop();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove(final ScopNode n) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Not implemented for class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final ScopNode n, final ScopNode _new) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Not implemented for class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substitute(final ScopDimension old, final ScopDimension _new) {
		Iterable<IntExpression> _iterable = IteratorExtensions.<IntExpression>toIterable(Iterators.<IntExpression>filter(this.eAllContents(), IntExpression.class));
		for (final IntExpression exp : _iterable) {
			{
				final IntExpression newExp = exp.substitute(old, _new);
				EMFUtils.substituteByNewObjectInContainer(exp, newExp);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfEnclosingDimension() {
		int _xifexpression = (int) 0;
		ScopNode _parentScop = this.getParentScop();
		boolean _tripleNotEquals = (_parentScop != null);
		if (_tripleNotEquals) {
			_xifexpression = this.getParentScop().getNumberOfEnclosingDimension();
		}
		else {
			_xifexpression = 0;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfEnclosedDimension() {
		int _size = this.listChildrenScopNodes().size();
		boolean _equals = (_size == 0);
		if (_equals) {
			return 0;
		}
		else {
			final Function1<ScopNode, Integer> _function = new Function1<ScopNode, Integer>() {
				public Integer apply(final ScopNode n) {
					return Integer.valueOf(n.getNumberOfEnclosedDimension());
				}
			};
			final Function2<Integer, Integer, Integer> _function_1 = new Function2<Integer, Integer, Integer>() {
				public Integer apply(final Integer p1, final Integer p2) {
					return Integer.valueOf(Math.max((p1).intValue(), (p2).intValue()));
				}
			};
			return (int) IterableExtensions.<Integer>reduce(XcoreEListExtensions.<ScopNode, Integer>map(this.listChildrenScopNodes(), _function), _function_1);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM:
				return basicSetTransform(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM:
				return getTransform();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM:
				setTransform((ScopTransform)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM:
				setTransform((ScopTransform)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM:
				return transform != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ScopNode.class) {
			switch (derivedFeatureID) {
				case ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM: return ScopPackage.SCOP_NODE__TRANSFORM;
				default: return -1;
			}
		}
		if (baseClass == ScopStatement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ScopNode.class) {
			switch (baseFeatureID) {
				case ScopPackage.SCOP_NODE__TRANSFORM: return ScopPackage.SCOP_INSTRUCTION_STATEMENT__TRANSFORM;
				default: return -1;
			}
		}
		if (baseClass == ScopStatement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ScopInstructionStatementImpl
