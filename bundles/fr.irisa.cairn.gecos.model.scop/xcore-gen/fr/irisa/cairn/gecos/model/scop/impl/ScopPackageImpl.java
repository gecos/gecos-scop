/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.DefUseEdge;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlockStatement;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMState;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopIndexExpression;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraint;
import fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem;
import fr.irisa.cairn.gecos.model.scop.ScopIntExpression;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopParameter;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionWrite;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.UseDefEdge;
import fr.irisa.cairn.gecos.model.scop.WAWEdge;
import fr.irisa.cairn.gecos.model.scop.scopAsyncBlock;

import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.affine.AffinePackage;
import org.polymodel.algebra.polynomials.PolynomialsPackage;
import org.polymodel.algebra.quasiAffine.QuasiAffinePackage;
import org.polymodel.algebra.reductions.ReductionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScopPackageImpl extends EPackageImpl implements ScopPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopDimensionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosScopBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopGuardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopIndexExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopReadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopRegionReadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopRegionWriteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopWriteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopIntExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopIntConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopIntConstraintSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopInstructionStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopForLoopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopDoAllLoopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopAsyncBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopFSMBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopFSMTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopFSMStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopUnexpandedNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopUnexpandedStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopBlockStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defUseEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useDefEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wawEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScopPackageImpl() {
		super(eNS_URI, ScopFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ScopPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScopPackage init() {
		if (isInited) return (ScopPackage)EPackage.Registry.INSTANCE.getEPackage(ScopPackage.eNS_URI);

		// Obtain or create and register package
		ScopPackageImpl theScopPackage = (ScopPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ScopPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ScopPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AnnotationsPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		AlgebraPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		AffinePackage.eINSTANCE.eClass();
		QuasiAffinePackage.eINSTANCE.eClass();
		PolynomialsPackage.eINSTANCE.eClass();
		ReductionsPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TransformPackageImpl theTransformPackage = (TransformPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TransformPackage.eNS_URI) instanceof TransformPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TransformPackage.eNS_URI) : TransformPackage.eINSTANCE);

		// Create package meta-data objects
		theScopPackage.createPackageContents();
		theTransformPackage.createPackageContents();

		// Initialize created meta-data
		theScopPackage.initializePackageContents();
		theTransformPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theScopPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScopPackage.eNS_URI, theScopPackage);
		return theScopPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopNode() {
		return scopNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopNode_Transform() {
		return (EReference)scopNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopDimension() {
		return scopDimensionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopDimension_Symbol() {
		return (EReference)scopDimensionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScopDimension_SymName() {
		return (EAttribute)scopDimensionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopParameter() {
		return scopParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosScopBlock() {
		return gecosScopBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGecosScopBlock_Name() {
		return (EAttribute)gecosScopBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_LiveInSymbols() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_LiveOutSymbols() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_Parameters() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_Iterators() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_Context() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosScopBlock_Root() {
		return (EReference)gecosScopBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopGuard() {
		return scopGuardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopGuard_Predicate() {
		return (EReference)scopGuardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopGuard_ThenNode() {
		return (EReference)scopGuardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopGuard_ElseNode() {
		return (EReference)scopGuardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopIndexExpression() {
		return scopIndexExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopIndexExpression_IndexExpressions() {
		return (EReference)scopIndexExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopAccess() {
		return scopAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopAccess_Sym() {
		return (EReference)scopAccessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScopAccess_SymName() {
		return (EAttribute)scopAccessEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopRead() {
		return scopReadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopRegionRead() {
		return scopRegionReadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopRegionRead_Existentials() {
		return (EReference)scopRegionReadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopRegionRead_Region() {
		return (EReference)scopRegionReadEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopRegionWrite() {
		return scopRegionWriteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopRegionWrite_Existentials() {
		return (EReference)scopRegionWriteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopRegionWrite_Region() {
		return (EReference)scopRegionWriteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopWrite() {
		return scopWriteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopIntExpression() {
		return scopIntExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopIntExpression_Expr() {
		return (EReference)scopIntExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopIntConstraint() {
		return scopIntConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopIntConstraint_Guard() {
		return (EReference)scopIntConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopIntConstraintSystem() {
		return scopIntConstraintSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopIntConstraintSystem_System() {
		return (EReference)scopIntConstraintSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopStatement() {
		return scopStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopInstructionStatement() {
		return scopInstructionStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopBlock() {
		return scopBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopBlock_Children() {
		return (EReference)scopBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopForLoop() {
		return scopForLoopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopForLoop_Iterator() {
		return (EReference)scopForLoopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopForLoop_LB() {
		return (EReference)scopForLoopEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopForLoop_UB() {
		return (EReference)scopForLoopEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopForLoop_Stride() {
		return (EReference)scopForLoopEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopForLoop_Body() {
		return (EReference)scopForLoopEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopDoAllLoop() {
		return scopDoAllLoopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getscopAsyncBlock() {
		return scopAsyncBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopFSMBlock() {
		return scopFSMBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMBlock_Iterators() {
		return (EReference)scopFSMBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMBlock_Start() {
		return (EReference)scopFSMBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMBlock_Commands() {
		return (EReference)scopFSMBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMBlock_Next() {
		return (EReference)scopFSMBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopFSMTransition() {
		return scopFSMTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMTransition_Domain() {
		return (EReference)scopFSMTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMTransition_NextIteration() {
		return (EReference)scopFSMTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMTransition_Next() {
		return (EReference)scopFSMTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopFSMState() {
		return scopFSMStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMState_Domain() {
		return (EReference)scopFSMStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMState_Commands() {
		return (EReference)scopFSMStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopFSMState_Transitions() {
		return (EReference)scopFSMStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopUnexpandedNode() {
		return scopUnexpandedNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScopUnexpandedNode_Schedule() {
		return (EAttribute)scopUnexpandedNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScopUnexpandedNode_Domain() {
		return (EAttribute)scopUnexpandedNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopUnexpandedNode_Existentials() {
		return (EReference)scopUnexpandedNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopUnexpandedStatement() {
		return scopUnexpandedStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopBlockStatement() {
		return scopBlockStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScopBlockStatement_Label() {
		return (EAttribute)scopBlockStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopBlockStatement_Reads() {
		return (EReference)scopBlockStatementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopBlockStatement_Writes() {
		return (EReference)scopBlockStatementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopBlockStatement_Body() {
		return (EReference)scopBlockStatementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefUseEdge() {
		return defUseEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDefUseEdge_Source() {
		return (EReference)defUseEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDefUseEdge_Sinks() {
		return (EReference)defUseEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDefUseEdge_Dependency() {
		return (EAttribute)defUseEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseDefEdge() {
		return useDefEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseDefEdge_Sources() {
		return (EReference)useDefEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseDefEdge_Sink() {
		return (EReference)useDefEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUseDefEdge_Dependency() {
		return (EAttribute)useDefEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWAWEdge() {
		return wawEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWAWEdge_Source() {
		return (EReference)wawEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWAWEdge_Sinks() {
		return (EReference)wawEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWAWEdge_Dependency() {
		return (EAttribute)wawEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopFactory getScopFactory() {
		return (ScopFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scopNodeEClass = createEClass(SCOP_NODE);
		createEReference(scopNodeEClass, SCOP_NODE__TRANSFORM);

		scopDimensionEClass = createEClass(SCOP_DIMENSION);
		createEReference(scopDimensionEClass, SCOP_DIMENSION__SYMBOL);
		createEAttribute(scopDimensionEClass, SCOP_DIMENSION__SYM_NAME);

		scopParameterEClass = createEClass(SCOP_PARAMETER);

		gecosScopBlockEClass = createEClass(GECOS_SCOP_BLOCK);
		createEAttribute(gecosScopBlockEClass, GECOS_SCOP_BLOCK__NAME);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__LIVE_IN_SYMBOLS);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__LIVE_OUT_SYMBOLS);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__PARAMETERS);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__ITERATORS);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__CONTEXT);
		createEReference(gecosScopBlockEClass, GECOS_SCOP_BLOCK__ROOT);

		scopGuardEClass = createEClass(SCOP_GUARD);
		createEReference(scopGuardEClass, SCOP_GUARD__PREDICATE);
		createEReference(scopGuardEClass, SCOP_GUARD__THEN_NODE);
		createEReference(scopGuardEClass, SCOP_GUARD__ELSE_NODE);

		scopIndexExpressionEClass = createEClass(SCOP_INDEX_EXPRESSION);
		createEReference(scopIndexExpressionEClass, SCOP_INDEX_EXPRESSION__INDEX_EXPRESSIONS);

		scopAccessEClass = createEClass(SCOP_ACCESS);
		createEReference(scopAccessEClass, SCOP_ACCESS__SYM);
		createEAttribute(scopAccessEClass, SCOP_ACCESS__SYM_NAME);

		scopReadEClass = createEClass(SCOP_READ);

		scopRegionReadEClass = createEClass(SCOP_REGION_READ);
		createEReference(scopRegionReadEClass, SCOP_REGION_READ__EXISTENTIALS);
		createEReference(scopRegionReadEClass, SCOP_REGION_READ__REGION);

		scopRegionWriteEClass = createEClass(SCOP_REGION_WRITE);
		createEReference(scopRegionWriteEClass, SCOP_REGION_WRITE__EXISTENTIALS);
		createEReference(scopRegionWriteEClass, SCOP_REGION_WRITE__REGION);

		scopWriteEClass = createEClass(SCOP_WRITE);

		scopIntExpressionEClass = createEClass(SCOP_INT_EXPRESSION);
		createEReference(scopIntExpressionEClass, SCOP_INT_EXPRESSION__EXPR);

		scopIntConstraintEClass = createEClass(SCOP_INT_CONSTRAINT);
		createEReference(scopIntConstraintEClass, SCOP_INT_CONSTRAINT__GUARD);

		scopIntConstraintSystemEClass = createEClass(SCOP_INT_CONSTRAINT_SYSTEM);
		createEReference(scopIntConstraintSystemEClass, SCOP_INT_CONSTRAINT_SYSTEM__SYSTEM);

		scopStatementEClass = createEClass(SCOP_STATEMENT);

		scopInstructionStatementEClass = createEClass(SCOP_INSTRUCTION_STATEMENT);

		scopBlockStatementEClass = createEClass(SCOP_BLOCK_STATEMENT);
		createEAttribute(scopBlockStatementEClass, SCOP_BLOCK_STATEMENT__LABEL);
		createEReference(scopBlockStatementEClass, SCOP_BLOCK_STATEMENT__READS);
		createEReference(scopBlockStatementEClass, SCOP_BLOCK_STATEMENT__WRITES);
		createEReference(scopBlockStatementEClass, SCOP_BLOCK_STATEMENT__BODY);

		scopBlockEClass = createEClass(SCOP_BLOCK);
		createEReference(scopBlockEClass, SCOP_BLOCK__CHILDREN);

		scopForLoopEClass = createEClass(SCOP_FOR_LOOP);
		createEReference(scopForLoopEClass, SCOP_FOR_LOOP__ITERATOR);
		createEReference(scopForLoopEClass, SCOP_FOR_LOOP__LB);
		createEReference(scopForLoopEClass, SCOP_FOR_LOOP__UB);
		createEReference(scopForLoopEClass, SCOP_FOR_LOOP__STRIDE);
		createEReference(scopForLoopEClass, SCOP_FOR_LOOP__BODY);

		scopDoAllLoopEClass = createEClass(SCOP_DO_ALL_LOOP);

		scopAsyncBlockEClass = createEClass(SCOP_ASYNC_BLOCK);

		scopFSMBlockEClass = createEClass(SCOP_FSM_BLOCK);
		createEReference(scopFSMBlockEClass, SCOP_FSM_BLOCK__ITERATORS);
		createEReference(scopFSMBlockEClass, SCOP_FSM_BLOCK__START);
		createEReference(scopFSMBlockEClass, SCOP_FSM_BLOCK__COMMANDS);
		createEReference(scopFSMBlockEClass, SCOP_FSM_BLOCK__NEXT);

		scopFSMTransitionEClass = createEClass(SCOP_FSM_TRANSITION);
		createEReference(scopFSMTransitionEClass, SCOP_FSM_TRANSITION__DOMAIN);
		createEReference(scopFSMTransitionEClass, SCOP_FSM_TRANSITION__NEXT_ITERATION);
		createEReference(scopFSMTransitionEClass, SCOP_FSM_TRANSITION__NEXT);

		scopFSMStateEClass = createEClass(SCOP_FSM_STATE);
		createEReference(scopFSMStateEClass, SCOP_FSM_STATE__DOMAIN);
		createEReference(scopFSMStateEClass, SCOP_FSM_STATE__COMMANDS);
		createEReference(scopFSMStateEClass, SCOP_FSM_STATE__TRANSITIONS);

		scopUnexpandedNodeEClass = createEClass(SCOP_UNEXPANDED_NODE);
		createEAttribute(scopUnexpandedNodeEClass, SCOP_UNEXPANDED_NODE__SCHEDULE);
		createEAttribute(scopUnexpandedNodeEClass, SCOP_UNEXPANDED_NODE__DOMAIN);
		createEReference(scopUnexpandedNodeEClass, SCOP_UNEXPANDED_NODE__EXISTENTIALS);

		scopUnexpandedStatementEClass = createEClass(SCOP_UNEXPANDED_STATEMENT);

		defUseEdgeEClass = createEClass(DEF_USE_EDGE);
		createEReference(defUseEdgeEClass, DEF_USE_EDGE__SOURCE);
		createEReference(defUseEdgeEClass, DEF_USE_EDGE__SINKS);
		createEAttribute(defUseEdgeEClass, DEF_USE_EDGE__DEPENDENCY);

		useDefEdgeEClass = createEClass(USE_DEF_EDGE);
		createEReference(useDefEdgeEClass, USE_DEF_EDGE__SOURCES);
		createEReference(useDefEdgeEClass, USE_DEF_EDGE__SINK);
		createEAttribute(useDefEdgeEClass, USE_DEF_EDGE__DEPENDENCY);

		wawEdgeEClass = createEClass(WAW_EDGE);
		createEReference(wawEdgeEClass, WAW_EDGE__SOURCE);
		createEReference(wawEdgeEClass, WAW_EDGE__SINKS);
		createEAttribute(wawEdgeEClass, WAW_EDGE__DEPENDENCY);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		TransformPackage theTransformPackage = (TransformPackage)EPackage.Registry.INSTANCE.getEPackage(TransformPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		AlgebraPackage theAlgebraPackage = (AlgebraPackage)EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		scopNodeEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		scopDimensionEClass.getESuperTypes().add(theAlgebraPackage.getVariable());
		scopDimensionEClass.getESuperTypes().add(theCorePackage.getISymbolUse());
		scopParameterEClass.getESuperTypes().add(this.getScopDimension());
		gecosScopBlockEClass.getESuperTypes().add(this.getScopNode());
		gecosScopBlockEClass.getESuperTypes().add(theBlocksPackage.getBasicBlock());
		gecosScopBlockEClass.getESuperTypes().add(theCorePackage.getScopeContainer());
		scopGuardEClass.getESuperTypes().add(this.getScopNode());
		scopIndexExpressionEClass.getESuperTypes().add(this.getScopNode());
		scopIndexExpressionEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		scopAccessEClass.getESuperTypes().add(this.getScopIndexExpression());
		scopReadEClass.getESuperTypes().add(this.getScopAccess());
		scopRegionReadEClass.getESuperTypes().add(this.getScopRead());
		scopRegionWriteEClass.getESuperTypes().add(this.getScopRead());
		scopWriteEClass.getESuperTypes().add(this.getScopAccess());
		scopIntExpressionEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		scopIntConstraintEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		scopIntConstraintSystemEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		scopStatementEClass.getESuperTypes().add(this.getScopNode());
		scopInstructionStatementEClass.getESuperTypes().add(theInstrsPackage.getGenericInstruction());
		scopInstructionStatementEClass.getESuperTypes().add(this.getScopStatement());
		scopBlockStatementEClass.getESuperTypes().add(this.getScopStatement());
		scopBlockEClass.getESuperTypes().add(this.getScopNode());
		scopForLoopEClass.getESuperTypes().add(this.getScopNode());
		scopDoAllLoopEClass.getESuperTypes().add(this.getScopForLoop());
		scopAsyncBlockEClass.getESuperTypes().add(this.getScopBlock());
		scopFSMBlockEClass.getESuperTypes().add(this.getScopNode());
		scopFSMTransitionEClass.getESuperTypes().add(this.getScopNode());
		scopFSMStateEClass.getESuperTypes().add(this.getScopNode());
		scopUnexpandedNodeEClass.getESuperTypes().add(this.getScopNode());
		scopUnexpandedStatementEClass.getESuperTypes().add(this.getScopInstructionStatement());
		scopUnexpandedStatementEClass.getESuperTypes().add(this.getScopUnexpandedNode());

		// Initialize classes and features; add operations and parameters
		initEClass(scopNodeEClass, ScopNode.class, "ScopNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopNode_Transform(), theTransformPackage.getScopTransform(), null, "transform", null, 0, 1, ScopNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopNode(), "getParentScop", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopNode(), "getScopRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(scopNodeEClass, this.getScopStatement(), "getStatement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopStatement(), "listAllStatements", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopRead(), "listAllReadAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopAccess(), "listAllAccesses", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, theCorePackage.getSymbol(), "listAllReferencedSymbols", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopWrite(), "listAllWriteAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopNode(), "listChildrenScopNodes", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopNode(), "listAllChildredScopNodes", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopDimension(), "listAllParameters", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopDimension(), "listRootParameters", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopDimension(), "listRootIterators", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, theTransformPackage.getScopTransformDirective(), "listNodeDirectives", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopNodeEClass, this.getScopDimension(), "listAllEnclosingIterators", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "relativeRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopDimension(), "listAllEnclosingIterators", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopDimension(), "listAllFreeIterators", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopForLoop(), "getEnclosingForLoop", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopNodeEClass, this.getScopForLoop(), "getEnclosingForLoop", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "relativeRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopNodeEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopNodeEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopNodeEClass, null, "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "old", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getint(), "getNumberOfEnclosingDimension", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getint(), "getNumberOfEnclosedDimension", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getScopNode(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopDimensionEClass, ScopDimension.class, "ScopDimension", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopDimension_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, ScopDimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScopDimension_SymName(), this.getString(), "symName", null, 0, 1, ScopDimension.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		addEOperation(scopDimensionEClass, theCorePackage.getSymbol(), "getUsedSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopDimensionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopParameterEClass, ScopParameter.class, "ScopParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gecosScopBlockEClass, GecosScopBlock.class, "GecosScopBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGecosScopBlock_Name(), this.getString(), "name", null, 0, 1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_LiveInSymbols(), theCorePackage.getSymbol(), null, "liveInSymbols", null, 0, -1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_LiveOutSymbols(), theCorePackage.getSymbol(), null, "liveOutSymbols", null, 0, -1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_Parameters(), this.getScopDimension(), null, "parameters", null, 0, -1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_Iterators(), this.getScopDimension(), null, "iterators", null, 0, -1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_Context(), theAlgebraPackage.getIntConstraintSystem(), null, "context", null, 0, 1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosScopBlock_Root(), this.getScopNode(), null, "root", null, 0, 1, GecosScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(gecosScopBlockEClass, this.getScopNode(), "getParentScop", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosScopBlockEClass, this.getGecosScopBlock(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gecosScopBlockEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gecosScopBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosScopBlockEClass, null, "relabelStatements", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosScopBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopGuardEClass, ScopGuard.class, "ScopGuard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopGuard_Predicate(), theAlgebraPackage.getIntConstraintSystem(), null, "predicate", null, 0, -1, ScopGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopGuard_ThenNode(), this.getScopNode(), null, "thenNode", null, 0, 1, ScopGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopGuard_ElseNode(), this.getScopNode(), null, "elseNode", null, 0, 1, ScopGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scopGuardEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopGuardEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopGuardEClass, this.getint(), "maxDepth", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopIndexExpressionEClass, ScopIndexExpression.class, "ScopIndexExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopIndexExpression_IndexExpressions(), theAlgebraPackage.getIntExpression(), null, "indexExpressions", null, 0, -1, ScopIndexExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopIndexExpressionEClass, this.getScopNode(), "getParentScop", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopIndexExpressionEClass, this.getScopIndexExpression(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopAccessEClass, ScopAccess.class, "ScopAccess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopAccess_Sym(), theCorePackage.getSymbol(), null, "sym", null, 0, 1, ScopAccess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScopAccess_SymName(), this.getString(), "symName", null, 0, 1, ScopAccess.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		addEOperation(scopAccessEClass, this.getScopAccess(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopAccessEClass, this.getScopStatement(), "getEnclosingStatement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopAccessEClass, null, "substituteIndexWith", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "dim", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getIntExpression(), "e", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopReadEClass, ScopRead.class, "ScopRead", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scopRegionReadEClass, ScopRegionRead.class, "ScopRegionRead", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopRegionRead_Existentials(), this.getScopDimension(), null, "existentials", null, 0, -1, ScopRegionRead.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopRegionRead_Region(), theAlgebraPackage.getIntConstraintSystem(), null, "region", null, 0, 1, ScopRegionRead.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scopRegionWriteEClass, ScopRegionWrite.class, "ScopRegionWrite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopRegionWrite_Existentials(), this.getScopDimension(), null, "existentials", null, 0, -1, ScopRegionWrite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopRegionWrite_Region(), theAlgebraPackage.getIntConstraintSystem(), null, "region", null, 0, 1, ScopRegionWrite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scopWriteEClass, ScopWrite.class, "ScopWrite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scopIntExpressionEClass, ScopIntExpression.class, "ScopIntExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopIntExpression_Expr(), theAlgebraPackage.getIntExpression(), null, "expr", null, 0, 1, ScopIntExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopIntExpressionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopIntExpressionEClass, this.getScopIntExpression(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopIntConstraintEClass, ScopIntConstraint.class, "ScopIntConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopIntConstraint_Guard(), theAlgebraPackage.getIntConstraint(), null, "guard", null, 0, 1, ScopIntConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopIntConstraintEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopIntConstraintEClass, this.getScopIntConstraint(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopIntConstraintSystemEClass, ScopIntConstraintSystem.class, "ScopIntConstraintSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopIntConstraintSystem_System(), theAlgebraPackage.getIntConstraintSystem(), null, "system", null, 0, 1, ScopIntConstraintSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopIntConstraintSystemEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopIntConstraintSystemEClass, this.getScopIntConstraintSystem(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopStatementEClass, ScopStatement.class, "ScopStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(scopStatementEClass, this.getString(), "getId", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopStatementEClass, null, "setId", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopStatementEClass, this.getScopStatement(), "listAllStatements", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopStatementEClass, theEcorePackage.getEBoolean(), "useSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopStatementEClass, theCorePackage.getSymbol(), "listAllSymbolReferences", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopStatementEClass, theCorePackage.getSymbol(), "listAllReadSymbols", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopStatementEClass, theCorePackage.getSymbol(), "listAllWriteSymbols", 0, -1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopInstructionStatementEClass, ScopInstructionStatement.class, "ScopInstructionStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(scopInstructionStatementEClass, this.getString(), "getId", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopInstructionStatementEClass, null, "setId", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "txt", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getScopStatement(), "listAllStatements", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getScopAccess(), "listAllAccesses", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getScopRead(), "listAllReadAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getScopWrite(), "listAllWriteAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopInstructionStatementEClass, this.getScopInstructionStatement(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopBlockStatementEClass, ScopBlockStatement.class, "ScopBlockStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScopBlockStatement_Label(), this.getString(), "label", null, 0, 1, ScopBlockStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopBlockStatement_Reads(), this.getScopRead(), null, "reads", null, 0, -1, ScopBlockStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopBlockStatement_Writes(), this.getScopWrite(), null, "writes", null, 0, -1, ScopBlockStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopBlockStatement_Body(), theBlocksPackage.getBlock(), null, "body", null, 0, 1, ScopBlockStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getString(), "getId", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopBlockStatementEClass, null, "setId", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "txt", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getScopStatement(), "listAllStatements", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getScopAccess(), "listAllAccesses", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getScopRead(), "listAllReadAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getScopWrite(), "listAllWriteAccess", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopBlockStatementEClass, this.getScopBlockStatement(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopBlockEClass, ScopBlock.class, "ScopBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopBlock_Children(), this.getScopNode(), null, "children", null, 0, -1, ScopBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scopBlockEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopForLoopEClass, ScopForLoop.class, "ScopForLoop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopForLoop_Iterator(), this.getScopDimension(), null, "iterator", null, 0, 1, ScopForLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopForLoop_LB(), theAlgebraPackage.getIntExpression(), null, "LB", null, 0, 1, ScopForLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopForLoop_UB(), theAlgebraPackage.getIntExpression(), null, "UB", null, 0, 1, ScopForLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopForLoop_Stride(), theAlgebraPackage.getIntExpression(), null, "stride", null, 0, 1, ScopForLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopForLoop_Body(), this.getScopNode(), null, "body", null, 0, 1, ScopForLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scopForLoopEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopForLoopEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopForLoopEClass, this.getint(), "getNumberOfEnclosingDimension", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopForLoopEClass, this.getint(), "getNumberOfEnclosedDimension", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopForLoopEClass, null, "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "old", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopDoAllLoopEClass, ScopDoAllLoop.class, "ScopDoAllLoop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scopAsyncBlockEClass, scopAsyncBlock.class, "scopAsyncBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scopFSMBlockEClass, ScopFSMBlock.class, "ScopFSMBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopFSMBlock_Iterators(), this.getScopDimension(), null, "iterators", null, 0, -1, ScopFSMBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMBlock_Start(), this.getScopFSMTransition(), null, "start", null, 0, -1, ScopFSMBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMBlock_Commands(), this.getScopNode(), null, "commands", null, 0, -1, ScopFSMBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMBlock_Next(), this.getScopFSMTransition(), null, "next", null, 0, -1, ScopFSMBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scopFSMBlockEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopFSMBlockEClass, null, "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "old", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopDimension(), "_new", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopFSMTransitionEClass, ScopFSMTransition.class, "ScopFSMTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopFSMTransition_Domain(), theAlgebraPackage.getIntConstraintSystem(), null, "domain", null, 0, -1, ScopFSMTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMTransition_NextIteration(), theAlgebraPackage.getIntExpression(), null, "nextIteration", null, 0, -1, ScopFSMTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMTransition_Next(), this.getScopFSMState(), null, "next", null, 0, 1, ScopFSMTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scopFSMStateEClass, ScopFSMState.class, "ScopFSMState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopFSMState_Domain(), theAlgebraPackage.getIntConstraintSystem(), null, "domain", null, 0, -1, ScopFSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMState_Commands(), this.getScopNode(), null, "commands", null, 0, -1, ScopFSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopFSMState_Transitions(), this.getScopFSMTransition(), null, "transitions", null, 0, -1, ScopFSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopFSMStateEClass, this.getScopFSMBlock(), "getParentFSM", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopFSMStateEClass, this.getScopDimension(), "listAllEnclosingIterators", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopFSMStateEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopNode(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopUnexpandedNodeEClass, ScopUnexpandedNode.class, "ScopUnexpandedNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScopUnexpandedNode_Schedule(), this.getString(), "schedule", null, 0, 1, ScopUnexpandedNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScopUnexpandedNode_Domain(), this.getString(), "domain", null, 0, 1, ScopUnexpandedNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScopUnexpandedNode_Existentials(), this.getScopDimension(), null, "existentials", null, 0, -1, ScopUnexpandedNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scopUnexpandedStatementEClass, ScopUnexpandedStatement.class, "ScopUnexpandedStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(defUseEdgeEClass, DefUseEdge.class, "DefUseEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDefUseEdge_Source(), this.getScopWrite(), null, "source", null, 0, 1, DefUseEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDefUseEdge_Sinks(), this.getScopRead(), null, "sinks", null, 0, -1, DefUseEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDefUseEdge_Dependency(), this.getString(), "dependency", null, 0, -1, DefUseEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useDefEdgeEClass, UseDefEdge.class, "UseDefEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseDefEdge_Sources(), this.getScopWrite(), null, "sources", null, 0, -1, UseDefEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseDefEdge_Sink(), this.getScopRead(), null, "sink", null, 0, 1, UseDefEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseDefEdge_Dependency(), this.getString(), "dependency", null, 0, -1, UseDefEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wawEdgeEClass, WAWEdge.class, "WAWEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWAWEdge_Source(), this.getScopWrite(), null, "source", null, 0, 1, WAWEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWAWEdge_Sinks(), this.getScopWrite(), null, "sinks", null, 0, -1, WAWEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWAWEdge_Dependency(), this.getString(), "dependency", null, 0, -1, WAWEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //ScopPackageImpl
