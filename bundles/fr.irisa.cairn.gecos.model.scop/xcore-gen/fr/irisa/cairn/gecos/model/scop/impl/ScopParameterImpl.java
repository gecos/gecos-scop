/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopParameter;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ScopParameterImpl extends ScopDimensionImpl implements ScopParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_PARAMETER;
	}

} //ScopParameterImpl
