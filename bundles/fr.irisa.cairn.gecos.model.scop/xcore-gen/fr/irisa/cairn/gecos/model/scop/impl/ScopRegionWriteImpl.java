/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopRegionWrite;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Region Write</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl#getExistentials <em>Existentials</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopRegionWriteImpl#getRegion <em>Region</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopRegionWriteImpl extends ScopReadImpl implements ScopRegionWrite {
	/**
	 * The cached value of the '{@link #getExistentials() <em>Existentials</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExistentials()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> existentials;

	/**
	 * The cached value of the '{@link #getRegion() <em>Region</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegion()
	 * @generated
	 * @ordered
	 */
	protected IntConstraintSystem region;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopRegionWriteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_REGION_WRITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getExistentials() {
		if (existentials == null) {
			existentials = new EObjectContainmentEList<ScopDimension>(ScopDimension.class, this, ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS);
		}
		return existentials;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem getRegion() {
		return region;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRegion(IntConstraintSystem newRegion, NotificationChain msgs) {
		IntConstraintSystem oldRegion = region;
		region = newRegion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_REGION_WRITE__REGION, oldRegion, newRegion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRegion(IntConstraintSystem newRegion) {
		if (newRegion != region) {
			NotificationChain msgs = null;
			if (region != null)
				msgs = ((InternalEObject)region).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_REGION_WRITE__REGION, null, msgs);
			if (newRegion != null)
				msgs = ((InternalEObject)newRegion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScopPackage.SCOP_REGION_WRITE__REGION, null, msgs);
			msgs = basicSetRegion(newRegion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_REGION_WRITE__REGION, newRegion, newRegion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS:
				return ((InternalEList<?>)getExistentials()).basicRemove(otherEnd, msgs);
			case ScopPackage.SCOP_REGION_WRITE__REGION:
				return basicSetRegion(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS:
				return getExistentials();
			case ScopPackage.SCOP_REGION_WRITE__REGION:
				return getRegion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS:
				getExistentials().clear();
				getExistentials().addAll((Collection<? extends ScopDimension>)newValue);
				return;
			case ScopPackage.SCOP_REGION_WRITE__REGION:
				setRegion((IntConstraintSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS:
				getExistentials().clear();
				return;
			case ScopPackage.SCOP_REGION_WRITE__REGION:
				setRegion((IntConstraintSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_REGION_WRITE__EXISTENTIALS:
				return existentials != null && !existentials.isEmpty();
			case ScopPackage.SCOP_REGION_WRITE__REGION:
				return region != null;
		}
		return super.eIsSet(featureID);
	}

} //ScopRegionWriteImpl
