/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.ScopAccess;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import gecos.core.Symbol;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ScopStatementImpl extends ScopNodeImpl implements ScopStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopStatement> listAllStatements() {
		return ECollections.<ScopStatement>toEList(java.util.Collections.<ScopStatement>unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.<ScopStatement>newArrayList(this)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean useSymbol(final Symbol s) {
		final Function1<ScopAccess, Boolean> _function = new Function1<ScopAccess, Boolean>() {
			public Boolean apply(final ScopAccess a) {
				Symbol _sym = a.getSym();
				return Boolean.valueOf(Objects.equal(_sym, s));
			}
		};
		return IterableExtensions.<ScopAccess>exists(this.listAllAccesses(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllSymbolReferences() {
		final Function1<ScopAccess, Symbol> _function = new Function1<ScopAccess, Symbol>() {
			public Symbol apply(final ScopAccess a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopAccess, Symbol>map(this.listAllAccesses(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllReadSymbols() {
		final Function1<ScopRead, Symbol> _function = new Function1<ScopRead, Symbol>() {
			public Symbol apply(final ScopRead a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopRead, Symbol>map(this.listAllReadAccess(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> listAllWriteSymbols() {
		final Function1<ScopWrite, Symbol> _function = new Function1<ScopWrite, Symbol>() {
			public Symbol apply(final ScopWrite a) {
				return a.getSym();
			}
		};
		return ECollections.<Symbol>toEList(IterableExtensions.<Symbol>toSet(XcoreEListExtensions.<ScopWrite, Symbol>map(this.listAllWriteAccess(), _function)));
	}

} //ScopStatementImpl
