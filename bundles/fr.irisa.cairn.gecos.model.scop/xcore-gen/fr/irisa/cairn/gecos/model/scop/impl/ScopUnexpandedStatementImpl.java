/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unexpanded Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl#getSchedule <em>Schedule</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.impl.ScopUnexpandedStatementImpl#getExistentials <em>Existentials</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopUnexpandedStatementImpl extends ScopInstructionStatementImpl implements ScopUnexpandedStatement {
	/**
	 * The default value of the '{@link #getSchedule() <em>Schedule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedule()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEDULE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSchedule() <em>Schedule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedule()
	 * @generated
	 * @ordered
	 */
	protected String schedule = SCHEDULE_EDEFAULT;
	/**
	 * The default value of the '{@link #getDomain() <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected static final String DOMAIN_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDomain() <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected String domain = DOMAIN_EDEFAULT;
	/**
	 * The cached value of the '{@link #getExistentials() <em>Existentials</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExistentials()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> existentials;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopUnexpandedStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_UNEXPANDED_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchedule() {
		return schedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchedule(String newSchedule) {
		String oldSchedule = schedule;
		schedule = newSchedule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE, oldSchedule, schedule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(String newDomain) {
		String oldDomain = domain;
		domain = newDomain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN, oldDomain, domain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getExistentials() {
		if (existentials == null) {
			existentials = new EObjectResolvingEList<ScopDimension>(ScopDimension.class, this, ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS);
		}
		return existentials;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE:
				return getSchedule();
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN:
				return getDomain();
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS:
				return getExistentials();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE:
				setSchedule((String)newValue);
				return;
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN:
				setDomain((String)newValue);
				return;
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS:
				getExistentials().clear();
				getExistentials().addAll((Collection<? extends ScopDimension>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE:
				setSchedule(SCHEDULE_EDEFAULT);
				return;
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN:
				setDomain(DOMAIN_EDEFAULT);
				return;
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS:
				getExistentials().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE:
				return SCHEDULE_EDEFAULT == null ? schedule != null : !SCHEDULE_EDEFAULT.equals(schedule);
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN:
				return DOMAIN_EDEFAULT == null ? domain != null : !DOMAIN_EDEFAULT.equals(domain);
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS:
				return existentials != null && !existentials.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ScopUnexpandedNode.class) {
			switch (derivedFeatureID) {
				case ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE: return ScopPackage.SCOP_UNEXPANDED_NODE__SCHEDULE;
				case ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN: return ScopPackage.SCOP_UNEXPANDED_NODE__DOMAIN;
				case ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS: return ScopPackage.SCOP_UNEXPANDED_NODE__EXISTENTIALS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ScopUnexpandedNode.class) {
			switch (baseFeatureID) {
				case ScopPackage.SCOP_UNEXPANDED_NODE__SCHEDULE: return ScopPackage.SCOP_UNEXPANDED_STATEMENT__SCHEDULE;
				case ScopPackage.SCOP_UNEXPANDED_NODE__DOMAIN: return ScopPackage.SCOP_UNEXPANDED_STATEMENT__DOMAIN;
				case ScopPackage.SCOP_UNEXPANDED_NODE__EXISTENTIALS: return ScopPackage.SCOP_UNEXPANDED_STATEMENT__EXISTENTIALS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (schedule: ");
		result.append(schedule);
		result.append(", domain: ");
		result.append(domain);
		result.append(')');
		return result.toString();
	}

} //ScopUnexpandedStatementImpl
