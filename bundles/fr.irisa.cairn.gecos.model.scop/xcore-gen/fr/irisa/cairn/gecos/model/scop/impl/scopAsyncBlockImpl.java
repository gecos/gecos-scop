/**
 */
package fr.irisa.cairn.gecos.model.scop.impl;

import fr.irisa.cairn.gecos.model.scop.ScopPackage;
import fr.irisa.cairn.gecos.model.scop.scopAsyncBlock;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>scop Async Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class scopAsyncBlockImpl extends ScopBlockImpl implements scopAsyncBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected scopAsyncBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScopPackage.Literals.SCOP_ASYNC_BLOCK;
	}

} //scopAsyncBlockImpl
