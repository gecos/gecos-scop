/**
 */
package fr.irisa.cairn.gecos.model.scop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>scop Async Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage#getscopAsyncBlock()
 * @model
 * @generated
 */
public interface scopAsyncBlock extends ScopBlock {
} // scopAsyncBlock
