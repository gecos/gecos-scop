/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;

import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Add Context Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getParams <em>Params</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getContext <em>Context</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAddContextDirective()
 * @model
 * @generated
 */
public interface AddContextDirective extends EObject {
	/**
	 * Returns the value of the '<em><b>Params</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Params</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Params</em>' reference.
	 * @see #setParams(ScopDimension)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAddContextDirective_Params()
	 * @model
	 * @generated
	 */
	ScopDimension getParams();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getParams <em>Params</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Params</em>' reference.
	 * @see #getParams()
	 * @generated
	 */
	void setParams(ScopDimension value);

	/**
	 * Returns the value of the '<em><b>Context</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' containment reference.
	 * @see #setContext(IntConstraintSystem)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAddContextDirective_Context()
	 * @model containment="true"
	 * @generated
	 */
	IntConstraintSystem getContext();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getContext <em>Context</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' containment reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(IntConstraintSystem value);

} // AddContextDirective
