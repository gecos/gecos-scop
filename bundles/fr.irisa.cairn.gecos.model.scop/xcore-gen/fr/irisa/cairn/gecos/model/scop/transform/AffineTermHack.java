/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.polymodel.algebra.affine.AffineTerm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Affine Term Hack</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAffineTermHack()
 * @model
 * @generated
 */
public interface AffineTermHack extends AffineTerm {
} // AffineTermHack
