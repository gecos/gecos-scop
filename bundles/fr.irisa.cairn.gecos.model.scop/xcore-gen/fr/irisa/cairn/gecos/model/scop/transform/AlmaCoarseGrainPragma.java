/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alma Coarse Grain Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getNbTasks <em>Nb Tasks</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTaskIds <em>Task Ids</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTileSizes <em>Tile Sizes</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaCoarseGrainPragma()
 * @model
 * @generated
 */
public interface AlmaCoarseGrainPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Nb Tasks</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Tasks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Tasks</em>' attribute.
	 * @see #setNbTasks(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaCoarseGrainPragma_NbTasks()
	 * @model default="-1" unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getNbTasks();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getNbTasks <em>Nb Tasks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Tasks</em>' attribute.
	 * @see #getNbTasks()
	 * @generated
	 */
	void setNbTasks(int value);

	/**
	 * Returns the value of the '<em><b>Task Ids</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Ids</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Ids</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaCoarseGrainPragma_TaskIds()
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getTaskIds();

	/**
	 * Returns the value of the '<em><b>Tile Sizes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tile Sizes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tile Sizes</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaCoarseGrainPragma_TileSizes()
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getTileSizes();

} // AlmaCoarseGrainPragma
