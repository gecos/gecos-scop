/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alma Fine Grain Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma#getTileSizes <em>Tile Sizes</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaFineGrainPragma()
 * @model
 * @generated
 */
public interface AlmaFineGrainPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Tile Sizes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tile Sizes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tile Sizes</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAlmaFineGrainPragma_TileSizes()
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getTileSizes();

} // AlmaFineGrainPragma
