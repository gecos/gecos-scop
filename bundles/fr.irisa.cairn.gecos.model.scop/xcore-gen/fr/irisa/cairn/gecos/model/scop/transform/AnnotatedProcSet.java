/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.ProcedureSet;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotated Proc Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getProcset <em>Procset</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getPragmas <em>Pragmas</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAnnotatedProcSet()
 * @model
 * @generated
 */
public interface AnnotatedProcSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Procset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procset</em>' containment reference.
	 * @see #setProcset(ProcedureSet)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAnnotatedProcSet_Procset()
	 * @model containment="true"
	 * @generated
	 */
	ProcedureSet getProcset();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getProcset <em>Procset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procset</em>' containment reference.
	 * @see #getProcset()
	 * @generated
	 */
	void setProcset(ProcedureSet value);

	/**
	 * Returns the value of the '<em><b>Pragmas</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragmas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragmas</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getAnnotatedProcSet_Pragmas()
	 * @model containment="true"
	 * @generated
	 */
	EList<S2S4HLSPragmaAnnotation> getPragmas();

} // AnnotatedProcSet
