/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bubble Insertion Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getBubbleSet <em>Bubble Set</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getDepth <em>Depth</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getLatency <em>Latency</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getCorrectionAlgorithm <em>Correction Algorithm</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getBubbleInsertionDirective()
 * @model
 * @generated
 */
public interface BubbleInsertionDirective extends EObject {
	/**
	 * Returns the value of the '<em><b>Bubble Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bubble Set</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bubble Set</em>' attribute.
	 * @see #setBubbleSet(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getBubbleInsertionDirective_BubbleSet()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getBubbleSet();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getBubbleSet <em>Bubble Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bubble Set</em>' attribute.
	 * @see #getBubbleSet()
	 * @generated
	 */
	void setBubbleSet(String value);

	/**
	 * Returns the value of the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depth</em>' attribute.
	 * @see #setDepth(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getBubbleInsertionDirective_Depth()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getDepth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getDepth <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Depth</em>' attribute.
	 * @see #getDepth()
	 * @generated
	 */
	void setDepth(int value);

	/**
	 * Returns the value of the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latency</em>' attribute.
	 * @see #setLatency(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getBubbleInsertionDirective_Latency()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getLatency();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getLatency <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latency</em>' attribute.
	 * @see #getLatency()
	 * @generated
	 */
	void setLatency(int value);

	/**
	 * Returns the value of the '<em><b>Correction Algorithm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correction Algorithm</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correction Algorithm</em>' attribute.
	 * @see #setCorrectionAlgorithm(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getBubbleInsertionDirective_CorrectionAlgorithm()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getCorrectionAlgorithm();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getCorrectionAlgorithm <em>Correction Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correction Algorithm</em>' attribute.
	 * @see #getCorrectionAlgorithm()
	 * @generated
	 */
	void setCorrectionAlgorithm(String value);

} // BubbleInsertionDirective
