/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cloog Options</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getEqualitySpreading <em>Equality Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStop <em>Stop</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthToOptimize <em>First Depth To Optimize</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthSpreading <em>First Depth Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStrides <em>Strides</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getBlock <em>Block</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getLastDepthToOptimize <em>Last Depth To Optimize</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getConstantSpreading <em>Constant Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getOnceTimeLoopElim <em>Once Time Loop Elim</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getCoalescingDepth <em>Coalescing Depth</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions()
 * @model
 * @generated
 */
public interface CloogOptions extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Equality Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equality Spreading</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equality Spreading</em>' attribute.
	 * @see #setEqualitySpreading(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_EqualitySpreading()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getEqualitySpreading();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getEqualitySpreading <em>Equality Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equality Spreading</em>' attribute.
	 * @see #getEqualitySpreading()
	 * @generated
	 */
	void setEqualitySpreading(int value);

	/**
	 * Returns the value of the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stop</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stop</em>' attribute.
	 * @see #setStop(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_Stop()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getStop();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStop <em>Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stop</em>' attribute.
	 * @see #getStop()
	 * @generated
	 */
	void setStop(int value);

	/**
	 * Returns the value of the '<em><b>First Depth To Optimize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Depth To Optimize</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Depth To Optimize</em>' attribute.
	 * @see #setFirstDepthToOptimize(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_FirstDepthToOptimize()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getFirstDepthToOptimize();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthToOptimize <em>First Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Depth To Optimize</em>' attribute.
	 * @see #getFirstDepthToOptimize()
	 * @generated
	 */
	void setFirstDepthToOptimize(int value);

	/**
	 * Returns the value of the '<em><b>First Depth Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Depth Spreading</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Depth Spreading</em>' attribute.
	 * @see #setFirstDepthSpreading(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_FirstDepthSpreading()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getFirstDepthSpreading();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthSpreading <em>First Depth Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Depth Spreading</em>' attribute.
	 * @see #getFirstDepthSpreading()
	 * @generated
	 */
	void setFirstDepthSpreading(int value);

	/**
	 * Returns the value of the '<em><b>Strides</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strides</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strides</em>' attribute.
	 * @see #setStrides(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_Strides()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getStrides();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStrides <em>Strides</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strides</em>' attribute.
	 * @see #getStrides()
	 * @generated
	 */
	void setStrides(int value);

	/**
	 * Returns the value of the '<em><b>Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' attribute.
	 * @see #setBlock(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_Block()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getBlock();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getBlock <em>Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' attribute.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(int value);

	/**
	 * Returns the value of the '<em><b>Last Depth To Optimize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Depth To Optimize</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Depth To Optimize</em>' attribute.
	 * @see #setLastDepthToOptimize(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_LastDepthToOptimize()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getLastDepthToOptimize();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getLastDepthToOptimize <em>Last Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Depth To Optimize</em>' attribute.
	 * @see #getLastDepthToOptimize()
	 * @generated
	 */
	void setLastDepthToOptimize(int value);

	/**
	 * Returns the value of the '<em><b>Constant Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Spreading</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Spreading</em>' attribute.
	 * @see #setConstantSpreading(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_ConstantSpreading()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getConstantSpreading();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getConstantSpreading <em>Constant Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Spreading</em>' attribute.
	 * @see #getConstantSpreading()
	 * @generated
	 */
	void setConstantSpreading(int value);

	/**
	 * Returns the value of the '<em><b>Once Time Loop Elim</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Once Time Loop Elim</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Once Time Loop Elim</em>' attribute.
	 * @see #setOnceTimeLoopElim(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_OnceTimeLoopElim()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getOnceTimeLoopElim();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getOnceTimeLoopElim <em>Once Time Loop Elim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Once Time Loop Elim</em>' attribute.
	 * @see #getOnceTimeLoopElim()
	 * @generated
	 */
	void setOnceTimeLoopElim(int value);

	/**
	 * Returns the value of the '<em><b>Coalescing Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coalescing Depth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coalescing Depth</em>' attribute.
	 * @see #setCoalescingDepth(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getCloogOptions_CoalescingDepth()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getCoalescingDepth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getCoalescingDepth <em>Coalescing Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coalescing Depth</em>' attribute.
	 * @see #getCoalescingDepth()
	 * @generated
	 */
	void setCoalescingDepth(int value);

} // CloogOptions
