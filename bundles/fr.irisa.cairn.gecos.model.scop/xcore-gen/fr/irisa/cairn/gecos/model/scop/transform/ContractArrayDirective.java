/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract Array Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getContractArrayDirective()
 * @model
 * @generated
 */
public interface ContractArrayDirective extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getContractArrayDirective_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

} // ContractArrayDirective
