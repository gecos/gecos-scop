/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Layout Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getAddresses <em>Addresses</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getVars <em>Vars</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataLayoutDirective()
 * @model
 * @generated
 */
public interface DataLayoutDirective extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Addresses</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Addresses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Addresses</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataLayoutDirective_Addresses()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getAddresses();

	/**
	 * Returns the value of the '<em><b>Vars</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vars</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vars</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataLayoutDirective_Vars()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopDimension> getVars();

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataLayoutDirective_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

} // DataLayoutDirective
