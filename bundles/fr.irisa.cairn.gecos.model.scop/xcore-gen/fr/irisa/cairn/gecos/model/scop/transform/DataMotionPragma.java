/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Motion Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getSymbols <em>Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getReuseDims <em>Reuse Dims</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataMotionPragma()
 * @model
 * @generated
 */
public interface DataMotionPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Symbols</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbols</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbols</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataMotionPragma_Symbols()
	 * @model
	 * @generated
	 */
	EList<Symbol> getSymbols();

	/**
	 * Returns the value of the '<em><b>Reuse Dims</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reuse Dims</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reuse Dims</em>' attribute.
	 * @see #setReuseDims(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDataMotionPragma_ReuseDims()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getReuseDims();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getReuseDims <em>Reuse Dims</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reuse Dims</em>' attribute.
	 * @see #getReuseDims()
	 * @generated
	 */
	void setReuseDims(int value);

} // DataMotionPragma
