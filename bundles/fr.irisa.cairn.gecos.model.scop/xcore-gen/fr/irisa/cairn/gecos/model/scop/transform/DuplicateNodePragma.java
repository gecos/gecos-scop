/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Duplicate Node Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getFactor <em>Factor</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDuplicateNodePragma()
 * @model
 * @generated
 */
public interface DuplicateNodePragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDuplicateNodePragma_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * Returns the value of the '<em><b>Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Factor</em>' attribute.
	 * @see #setFactor(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getDuplicateNodePragma_Factor()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getFactor();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getFactor <em>Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Factor</em>' attribute.
	 * @see #getFactor()
	 * @generated
	 */
	void setFactor(int value);

} // DuplicateNodePragma
