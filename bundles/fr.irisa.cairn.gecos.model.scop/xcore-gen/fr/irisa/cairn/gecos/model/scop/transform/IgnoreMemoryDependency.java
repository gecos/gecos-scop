/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;
import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ignore Memory Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getSymbols <em>Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getFrom <em>From</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getTo <em>To</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getIgnoreMemoryDependency()
 * @model
 * @generated
 */
public interface IgnoreMemoryDependency extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Symbols</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbols</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbols</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getIgnoreMemoryDependency_Symbols()
	 * @model
	 * @generated
	 */
	EList<Symbol> getSymbols();

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getIgnoreMemoryDependency_From()
	 * @model
	 * @generated
	 */
	Instruction getFrom();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Instruction value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getIgnoreMemoryDependency_To()
	 * @model
	 * @generated
	 */
	Instruction getTo();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Instruction value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.gecos.model.scop.transform.DepType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DepType
	 * @see #setType(DepType)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getIgnoreMemoryDependency_Type()
	 * @model unique="false"
	 * @generated
	 */
	DepType getType();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DepType
	 * @see #getType()
	 * @generated
	 */
	void setType(DepType value);

} // IgnoreMemoryDependency
