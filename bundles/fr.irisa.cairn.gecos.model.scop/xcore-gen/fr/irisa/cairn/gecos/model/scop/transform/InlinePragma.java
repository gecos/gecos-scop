/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.ProcedureSymbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inline Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.InlinePragma#getProc <em>Proc</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getInlinePragma()
 * @model
 * @generated
 */
public interface InlinePragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Proc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proc</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proc</em>' reference.
	 * @see #setProc(ProcedureSymbol)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getInlinePragma_Proc()
	 * @model
	 * @generated
	 */
	ProcedureSymbol getProc();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.InlinePragma#getProc <em>Proc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proc</em>' reference.
	 * @see #getProc()
	 * @generated
	 */
	void setProc(ProcedureSymbol value);

} // InlinePragma
