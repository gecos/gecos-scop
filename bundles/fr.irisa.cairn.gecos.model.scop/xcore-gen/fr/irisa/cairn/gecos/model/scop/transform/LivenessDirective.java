/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Liveness Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isKill <em>Kill</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveIn <em>Live In</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveOut <em>Live Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLivenessDirective()
 * @model
 * @generated
 */
public interface LivenessDirective extends EObject {
	/**
	 * Returns the value of the '<em><b>Kill</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kill</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kill</em>' attribute.
	 * @see #setKill(boolean)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLivenessDirective_Kill()
	 * @model unique="false"
	 * @generated
	 */
	boolean isKill();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isKill <em>Kill</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kill</em>' attribute.
	 * @see #isKill()
	 * @generated
	 */
	void setKill(boolean value);

	/**
	 * Returns the value of the '<em><b>Live In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Live In</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Live In</em>' attribute.
	 * @see #setLiveIn(boolean)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLivenessDirective_LiveIn()
	 * @model unique="false"
	 * @generated
	 */
	boolean isLiveIn();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveIn <em>Live In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Live In</em>' attribute.
	 * @see #isLiveIn()
	 * @generated
	 */
	void setLiveIn(boolean value);

	/**
	 * Returns the value of the '<em><b>Live Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Live Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Live Out</em>' attribute.
	 * @see #setLiveOut(boolean)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLivenessDirective_LiveOut()
	 * @model unique="false"
	 * @generated
	 */
	boolean isLiveOut();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveOut <em>Live Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Live Out</em>' attribute.
	 * @see #isLiveOut()
	 * @generated
	 */
	void setLiveOut(boolean value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLivenessDirective_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

} // LivenessDirective
