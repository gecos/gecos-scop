/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Pipeline Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getLatency <em>Latency</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getHashFunction <em>Hash Function</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLoopPipelinePragma()
 * @model
 * @generated
 */
public interface LoopPipelinePragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latency</em>' attribute.
	 * @see #setLatency(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLoopPipelinePragma_Latency()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getLatency();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getLatency <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latency</em>' attribute.
	 * @see #getLatency()
	 * @generated
	 */
	void setLatency(int value);

	/**
	 * Returns the value of the '<em><b>Hash Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hash Function</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hash Function</em>' attribute.
	 * @see #setHashFunction(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getLoopPipelinePragma_HashFunction()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getHashFunction();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getHashFunction <em>Hash Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hash Function</em>' attribute.
	 * @see #getHashFunction()
	 * @generated
	 */
	void setHashFunction(String value);

} // LoopPipelinePragma
