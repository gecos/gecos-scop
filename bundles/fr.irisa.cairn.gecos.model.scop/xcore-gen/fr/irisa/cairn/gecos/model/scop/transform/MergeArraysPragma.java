/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Merge Arrays Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma#getSymbols <em>Symbols</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getMergeArraysPragma()
 * @model
 * @generated
 */
public interface MergeArraysPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Symbols</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbols</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbols</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getMergeArraysPragma_Symbols()
	 * @model
	 * @generated
	 */
	EList<Symbol> getSymbols();

} // MergeArraysPragma
