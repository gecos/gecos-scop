/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Omp Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#isParallelFor <em>Parallel For</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getPrivates <em>Privates</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getShared <em>Shared</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getReductions <em>Reductions</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getOmpPragma()
 * @model
 * @generated
 */
public interface OmpPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Parallel For</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parallel For</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parallel For</em>' attribute.
	 * @see #setParallelFor(boolean)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getOmpPragma_ParallelFor()
	 * @model unique="false"
	 * @generated
	 */
	boolean isParallelFor();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#isParallelFor <em>Parallel For</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parallel For</em>' attribute.
	 * @see #isParallelFor()
	 * @generated
	 */
	void setParallelFor(boolean value);

	/**
	 * Returns the value of the '<em><b>Privates</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Privates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Privates</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getOmpPragma_Privates()
	 * @model
	 * @generated
	 */
	EList<Symbol> getPrivates();

	/**
	 * Returns the value of the '<em><b>Shared</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getOmpPragma_Shared()
	 * @model
	 * @generated
	 */
	EList<Symbol> getShared();

	/**
	 * Returns the value of the '<em><b>Reductions</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reductions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reductions</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getOmpPragma_Reductions()
	 * @model
	 * @generated
	 */
	EList<Symbol> getReductions();

} // OmpPragma
