/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pure Function Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getPureFunctionPragma()
 * @model
 * @generated
 */
public interface PureFunctionPragma extends ScopTransformDirective {
} // PureFunctionPragma
