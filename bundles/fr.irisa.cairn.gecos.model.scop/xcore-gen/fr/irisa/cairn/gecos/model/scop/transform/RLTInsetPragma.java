/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RLT Inset Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getRLTInsetPragma()
 * @model
 * @generated
 */
public interface RLTInsetPragma extends ScopTransformDirective {
} // RLTInsetPragma
