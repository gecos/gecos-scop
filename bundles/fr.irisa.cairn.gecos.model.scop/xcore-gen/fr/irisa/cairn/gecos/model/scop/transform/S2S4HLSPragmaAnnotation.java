/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>S2S4HLS Pragma Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getDirectives <em>Directives</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getOriginalPragma <em>Original Pragma</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getS2S4HLSPragmaAnnotation()
 * @model
 * @generated
 */
public interface S2S4HLSPragmaAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Directives</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Directives</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Directives</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getS2S4HLSPragmaAnnotation_Directives()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopTransformDirective> getDirectives();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getS2S4HLSPragmaAnnotation_Target()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer().eContainer();\nreturn ((&lt;%gecos.annotations.AnnotatedElement%&gt;) _eContainer);'"
	 * @generated
	 */
	AnnotatedElement getTarget();

	/**
	 * Returns the value of the '<em><b>Original Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Pragma</em>' containment reference.
	 * @see #setOriginalPragma(PragmaAnnotation)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getS2S4HLSPragmaAnnotation_OriginalPragma()
	 * @model containment="true"
	 * @generated
	 */
	PragmaAnnotation getOriginalPragma();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getOriginalPragma <em>Original Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Pragma</em>' containment reference.
	 * @see #getOriginalPragma()
	 * @generated
	 */
	void setOriginalPragma(PragmaAnnotation value);

} // S2S4HLSPragmaAnnotation
