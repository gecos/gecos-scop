/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Block Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getStatements <em>Statements</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getExprs <em>Exprs</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleBlockPragma()
 * @model
 * @generated
 */
public interface ScheduleBlockPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Statements</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.ScopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleBlockPragma_Statements()
	 * @model
	 * @generated
	 */
	EList<ScopStatement> getStatements();

	/**
	 * Returns the value of the '<em><b>Exprs</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exprs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exprs</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleBlockPragma_Exprs()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getExprs();

} // ScheduleBlockPragma
