/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntConstraint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Context Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getScheduling <em>Scheduling</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleContextPragma()
 * @model
 * @generated
 */
public interface ScheduleContextPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleContextPragma_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntConstraint> getConstraints();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleContextPragma_Name()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Scheduling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling</em>' attribute.
	 * @see #setScheduling(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleContextPragma_Scheduling()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getScheduling();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getScheduling <em>Scheduling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduling</em>' attribute.
	 * @see #getScheduling()
	 * @generated
	 */
	void setScheduling(String value);

} // ScheduleContextPragma
