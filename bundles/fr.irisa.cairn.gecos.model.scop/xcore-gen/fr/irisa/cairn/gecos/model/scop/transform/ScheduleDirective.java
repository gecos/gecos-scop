/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getSchedule <em>Schedule</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleDirective()
 * @model
 * @generated
 */
public interface ScheduleDirective extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule</em>' attribute.
	 * @see #setSchedule(String)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleDirective_Schedule()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.String"
	 * @generated
	 */
	String getSchedule();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getSchedule <em>Schedule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schedule</em>' attribute.
	 * @see #getSchedule()
	 * @generated
	 */
	void setSchedule(String value);

	/**
	 * Returns the value of the '<em><b>Statement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement</em>' reference.
	 * @see #setStatement(ScopStatement)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleDirective_Statement()
	 * @model
	 * @generated
	 */
	ScopStatement getStatement();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getStatement <em>Statement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement</em>' reference.
	 * @see #getStatement()
	 * @generated
	 */
	void setStatement(ScopStatement value);

} // ScheduleDirective
