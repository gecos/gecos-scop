/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

import org.eclipse.emf.common.util.EList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Statement Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getStatement <em>Statement</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getExprs <em>Exprs</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleStatementPragma()
 * @model
 * @generated
 */
public interface ScheduleStatementPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Statement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement</em>' reference.
	 * @see #setStatement(ScopStatement)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleStatementPragma_Statement()
	 * @model
	 * @generated
	 */
	ScopStatement getStatement();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getStatement <em>Statement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement</em>' reference.
	 * @see #getStatement()
	 * @generated
	 */
	void setStatement(ScopStatement value);

	/**
	 * Returns the value of the '<em><b>Exprs</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exprs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exprs</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleStatementPragma_Exprs()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getExprs();

} // ScheduleStatementPragma
