/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Vector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleVector()
 * @model
 * @generated
 */
public interface ScheduleVector extends EObject {
	/**
	 * Returns the value of the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expr</em>' containment reference.
	 * @see #setExpr(IntExpression)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScheduleVector_Expr()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getExpr();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector#getExpr <em>Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expr</em>' containment reference.
	 * @see #getExpr()
	 * @generated
	 */
	void setExpr(IntExpression value);

} // ScheduleVector
