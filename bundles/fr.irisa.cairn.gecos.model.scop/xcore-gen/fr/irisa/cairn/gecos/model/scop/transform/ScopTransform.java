/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scop Transform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransform#getCommands <em>Commands</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScopTransform()
 * @model
 * @generated
 */
public interface ScopTransform extends EObject {
	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScopTransform_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScopTransformDirective> getCommands();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;&gt;filter(this.getCommands(), &lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;.class));'"
	 * @generated
	 */
	EList<ScheduleDirective> listAllScheduleDirectives();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective%&gt;&gt;toEList(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective%&gt;&gt;filter(this.getCommands(), &lt;%fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective%&gt;.class));'"
	 * @generated
	 */
	EList<DataLayoutDirective> listAllLayoutDirectives();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt; schd)\n\t{\n\t\t&lt;%fr.irisa.cairn.gecos.model.scop.ScopStatement%&gt; _statement = schd.getStatement();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_statement, s));\n\t}\n};\nreturn ((&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;&gt;filter(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;&gt;filter(this.getCommands(), &lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;.class), _function), &lt;%fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective%&gt;.class))[0];'"
	 * @generated
	 */
	ScheduleDirective getScheduleFor(ScopStatement s);

} // ScopTransform
