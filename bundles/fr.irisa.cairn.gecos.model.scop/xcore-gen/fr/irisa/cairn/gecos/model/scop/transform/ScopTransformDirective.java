/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scop Transform Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getScopTransformDirective()
 * @model
 * @generated
 */
public interface ScopTransformDirective extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.irisa.cairn.gecos.model.scop.transform.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter%&gt;.printDirective(this);'"
	 * @generated
	 */
	String toString();

} // ScopTransformDirective
