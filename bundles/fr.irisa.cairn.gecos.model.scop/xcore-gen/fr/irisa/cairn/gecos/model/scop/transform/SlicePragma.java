/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Slice Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getSizes <em>Sizes</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getUnrolls <em>Unrolls</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getSlicePragma()
 * @model
 * @generated
 */
public interface SlicePragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Sizes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sizes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sizes</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getSlicePragma_Sizes()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getSizes();

	/**
	 * Returns the value of the '<em><b>Unrolls</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unrolls</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unrolls</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getSlicePragma_Unrolls()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getUnrolls();

} // SlicePragma
