/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tiling Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getNested <em>Nested</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getSizes <em>Sizes</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getTilingDirective()
 * @model
 * @generated
 */
public interface TilingDirective extends EObject {
	/**
	 * Returns the value of the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested</em>' containment reference.
	 * @see #setNested(TilingDirective)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getTilingDirective_Nested()
	 * @model containment="true"
	 * @generated
	 */
	TilingDirective getNested();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getNested <em>Nested</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested</em>' containment reference.
	 * @see #getNested()
	 * @generated
	 */
	void setNested(TilingDirective value);

	/**
	 * Returns the value of the '<em><b>Sizes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sizes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sizes</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getTilingDirective_Sizes()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	EList<Integer> getSizes();

} // TilingDirective
