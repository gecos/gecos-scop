/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage
 * @generated
 */
public interface TransformFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TransformFactory eINSTANCE = fr.irisa.cairn.gecos.model.scop.transform.impl.TransformFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Scop Transform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scop Transform</em>'.
	 * @generated
	 */
	ScopTransform createScopTransform();

	/**
	 * Returns a new object of class '<em>Scop Transform Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scop Transform Directive</em>'.
	 * @generated
	 */
	ScopTransformDirective createScopTransformDirective();

	/**
	 * Returns a new object of class '<em>Add Context Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Add Context Directive</em>'.
	 * @generated
	 */
	AddContextDirective createAddContextDirective();

	/**
	 * Returns a new object of class '<em>Bubble Insertion Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bubble Insertion Directive</em>'.
	 * @generated
	 */
	BubbleInsertionDirective createBubbleInsertionDirective();

	/**
	 * Returns a new object of class '<em>Liveness Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Liveness Directive</em>'.
	 * @generated
	 */
	LivenessDirective createLivenessDirective();

	/**
	 * Returns a new object of class '<em>Tiling Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tiling Directive</em>'.
	 * @generated
	 */
	TilingDirective createTilingDirective();

	/**
	 * Returns a new object of class '<em>Data Layout Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Layout Directive</em>'.
	 * @generated
	 */
	DataLayoutDirective createDataLayoutDirective();

	/**
	 * Returns a new object of class '<em>Schedule Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schedule Directive</em>'.
	 * @generated
	 */
	ScheduleDirective createScheduleDirective();

	/**
	 * Returns a new object of class '<em>Contract Array Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contract Array Directive</em>'.
	 * @generated
	 */
	ContractArrayDirective createContractArrayDirective();

	/**
	 * Returns a new object of class '<em>Cloog Options</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cloog Options</em>'.
	 * @generated
	 */
	CloogOptions createCloogOptions();

	/**
	 * Returns a new object of class '<em>Pure Function Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pure Function Pragma</em>'.
	 * @generated
	 */
	PureFunctionPragma createPureFunctionPragma();

	/**
	 * Returns a new object of class '<em>Schedule Context Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schedule Context Pragma</em>'.
	 * @generated
	 */
	ScheduleContextPragma createScheduleContextPragma();

	/**
	 * Returns a new object of class '<em>Unroll Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unroll Pragma</em>'.
	 * @generated
	 */
	UnrollPragma createUnrollPragma();

	/**
	 * Returns a new object of class '<em>Inline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline Pragma</em>'.
	 * @generated
	 */
	InlinePragma createInlinePragma();

	/**
	 * Returns a new object of class '<em>Data Motion Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Motion Pragma</em>'.
	 * @generated
	 */
	DataMotionPragma createDataMotionPragma();

	/**
	 * Returns a new object of class '<em>Duplicate Node Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Duplicate Node Pragma</em>'.
	 * @generated
	 */
	DuplicateNodePragma createDuplicateNodePragma();

	/**
	 * Returns a new object of class '<em>Slice Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Slice Pragma</em>'.
	 * @generated
	 */
	SlicePragma createSlicePragma();

	/**
	 * Returns a new object of class '<em>Merge Arrays Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Arrays Pragma</em>'.
	 * @generated
	 */
	MergeArraysPragma createMergeArraysPragma();

	/**
	 * Returns a new object of class '<em>S2S4HLS Pragma Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>S2S4HLS Pragma Annotation</em>'.
	 * @generated
	 */
	S2S4HLSPragmaAnnotation createS2S4HLSPragmaAnnotation();

	/**
	 * Returns a new object of class '<em>Annotated Proc Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotated Proc Set</em>'.
	 * @generated
	 */
	AnnotatedProcSet createAnnotatedProcSet();

	/**
	 * Returns a new object of class '<em>Affine Term Hack</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Affine Term Hack</em>'.
	 * @generated
	 */
	AffineTermHack createAffineTermHack();

	/**
	 * Returns a new object of class '<em>Schedule Vector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schedule Vector</em>'.
	 * @generated
	 */
	ScheduleVector createScheduleVector();

	/**
	 * Returns a new object of class '<em>Schedule Statement Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schedule Statement Pragma</em>'.
	 * @generated
	 */
	ScheduleStatementPragma createScheduleStatementPragma();

	/**
	 * Returns a new object of class '<em>Schedule Block Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schedule Block Pragma</em>'.
	 * @generated
	 */
	ScheduleBlockPragma createScheduleBlockPragma();

	/**
	 * Returns a new object of class '<em>Omp Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Omp Pragma</em>'.
	 * @generated
	 */
	OmpPragma createOmpPragma();

	/**
	 * Returns a new object of class '<em>Ignore Memory Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ignore Memory Dependency</em>'.
	 * @generated
	 */
	IgnoreMemoryDependency createIgnoreMemoryDependency();

	/**
	 * Returns a new object of class '<em>Loop Pipeline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Loop Pipeline Pragma</em>'.
	 * @generated
	 */
	LoopPipelinePragma createLoopPipelinePragma();

	/**
	 * Returns a new object of class '<em>Alma Coarse Grain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alma Coarse Grain Pragma</em>'.
	 * @generated
	 */
	AlmaCoarseGrainPragma createAlmaCoarseGrainPragma();

	/**
	 * Returns a new object of class '<em>Alma Fine Grain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alma Fine Grain Pragma</em>'.
	 * @generated
	 */
	AlmaFineGrainPragma createAlmaFineGrainPragma();

	/**
	 * Returns a new object of class '<em>RLT Inset Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RLT Inset Pragma</em>'.
	 * @generated
	 */
	RLTInsetPragma createRLTInsetPragma();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TransformPackage getTransformPackage();

} //TransformFactory
