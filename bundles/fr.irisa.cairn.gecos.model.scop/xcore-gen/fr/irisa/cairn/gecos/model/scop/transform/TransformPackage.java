/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;

import gecos.annotations.AnnotationsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.polymodel.algebra.affine.AffinePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' modelDirectory='/fr.irisa.cairn.gecos.model.scop/xcore-gen' basePackage='fr.irisa.cairn.gecos.model.scop'"
 * @generated
 */
public interface TransformPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "transform";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/scoptransform";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "transform";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TransformPackage eINSTANCE = fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformImpl <em>Scop Transform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScopTransform()
	 * @generated
	 */
	int SCOP_TRANSFORM = 0;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_TRANSFORM__COMMANDS = 0;

	/**
	 * The number of structural features of the '<em>Scop Transform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_TRANSFORM_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformDirectiveImpl <em>Scop Transform Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScopTransformDirective()
	 * @generated
	 */
	int SCOP_TRANSFORM_DIRECTIVE = 1;

	/**
	 * The number of structural features of the '<em>Scop Transform Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl <em>Add Context Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAddContextDirective()
	 * @generated
	 */
	int ADD_CONTEXT_DIRECTIVE = 2;

	/**
	 * The feature id for the '<em><b>Params</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CONTEXT_DIRECTIVE__PARAMS = 0;

	/**
	 * The feature id for the '<em><b>Context</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CONTEXT_DIRECTIVE__CONTEXT = 1;

	/**
	 * The number of structural features of the '<em>Add Context Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CONTEXT_DIRECTIVE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl <em>Bubble Insertion Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getBubbleInsertionDirective()
	 * @generated
	 */
	int BUBBLE_INSERTION_DIRECTIVE = 3;

	/**
	 * The feature id for the '<em><b>Bubble Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET = 0;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUBBLE_INSERTION_DIRECTIVE__DEPTH = 1;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUBBLE_INSERTION_DIRECTIVE__LATENCY = 2;

	/**
	 * The feature id for the '<em><b>Correction Algorithm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM = 3;

	/**
	 * The number of structural features of the '<em>Bubble Insertion Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUBBLE_INSERTION_DIRECTIVE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl <em>Liveness Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getLivenessDirective()
	 * @generated
	 */
	int LIVENESS_DIRECTIVE = 4;

	/**
	 * The feature id for the '<em><b>Kill</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_DIRECTIVE__KILL = 0;

	/**
	 * The feature id for the '<em><b>Live In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_DIRECTIVE__LIVE_IN = 1;

	/**
	 * The feature id for the '<em><b>Live Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_DIRECTIVE__LIVE_OUT = 2;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_DIRECTIVE__SYMBOL = 3;

	/**
	 * The number of structural features of the '<em>Liveness Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_DIRECTIVE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.TilingDirectiveImpl <em>Tiling Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TilingDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getTilingDirective()
	 * @generated
	 */
	int TILING_DIRECTIVE = 5;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TILING_DIRECTIVE__NESTED = 0;

	/**
	 * The feature id for the '<em><b>Sizes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TILING_DIRECTIVE__SIZES = 1;

	/**
	 * The number of structural features of the '<em>Tiling Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TILING_DIRECTIVE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl <em>Data Layout Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDataLayoutDirective()
	 * @generated
	 */
	int DATA_LAYOUT_DIRECTIVE = 6;

	/**
	 * The feature id for the '<em><b>Addresses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LAYOUT_DIRECTIVE__ADDRESSES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vars</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LAYOUT_DIRECTIVE__VARS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LAYOUT_DIRECTIVE__SYMBOL = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data Layout Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LAYOUT_DIRECTIVE_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleDirectiveImpl <em>Schedule Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleDirective()
	 * @generated
	 */
	int SCHEDULE_DIRECTIVE = 7;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_DIRECTIVE__SCHEDULE = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_DIRECTIVE__STATEMENT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Schedule Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_DIRECTIVE_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ContractArrayDirectiveImpl <em>Contract Array Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ContractArrayDirectiveImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getContractArrayDirective()
	 * @generated
	 */
	int CONTRACT_ARRAY_DIRECTIVE = 8;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARRAY_DIRECTIVE__SYMBOL = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Contract Array Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARRAY_DIRECTIVE_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl <em>Cloog Options</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getCloogOptions()
	 * @generated
	 */
	int CLOOG_OPTIONS = 9;

	/**
	 * The feature id for the '<em><b>Equality Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__EQUALITY_SPREADING = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__STOP = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>First Depth To Optimize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>First Depth Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__FIRST_DEPTH_SPREADING = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Strides</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__STRIDES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__BLOCK = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Last Depth To Optimize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Constant Spreading</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__CONSTANT_SPREADING = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Once Time Loop Elim</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Coalescing Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS__COALESCING_DEPTH = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Cloog Options</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOOG_OPTIONS_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 10;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.PureFunctionPragmaImpl <em>Pure Function Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.PureFunctionPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getPureFunctionPragma()
	 * @generated
	 */
	int PURE_FUNCTION_PRAGMA = 10;

	/**
	 * The number of structural features of the '<em>Pure Function Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PURE_FUNCTION_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl <em>Schedule Context Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleContextPragma()
	 * @generated
	 */
	int SCHEDULE_CONTEXT_PRAGMA = 11;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_CONTEXT_PRAGMA__NAME = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scheduling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_CONTEXT_PRAGMA__SCHEDULING = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Schedule Context Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_CONTEXT_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.UnrollPragmaImpl <em>Unroll Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.UnrollPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getUnrollPragma()
	 * @generated
	 */
	int UNROLL_PRAGMA = 12;

	/**
	 * The feature id for the '<em><b>Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNROLL_PRAGMA__FACTOR = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unroll Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNROLL_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.InlinePragmaImpl <em>Inline Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.InlinePragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getInlinePragma()
	 * @generated
	 */
	int INLINE_PRAGMA = 13;

	/**
	 * The feature id for the '<em><b>Proc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_PRAGMA__PROC = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inline Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl <em>Data Motion Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDataMotionPragma()
	 * @generated
	 */
	int DATA_MOTION_PRAGMA = 14;

	/**
	 * The feature id for the '<em><b>Symbols</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MOTION_PRAGMA__SYMBOLS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reuse Dims</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MOTION_PRAGMA__REUSE_DIMS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Motion Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MOTION_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DuplicateNodePragmaImpl <em>Duplicate Node Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DuplicateNodePragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDuplicateNodePragma()
	 * @generated
	 */
	int DUPLICATE_NODE_PRAGMA = 15;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUPLICATE_NODE_PRAGMA__SYMBOL = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUPLICATE_NODE_PRAGMA__FACTOR = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Duplicate Node Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUPLICATE_NODE_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl <em>Slice Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getSlicePragma()
	 * @generated
	 */
	int SLICE_PRAGMA = 16;

	/**
	 * The feature id for the '<em><b>Sizes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLICE_PRAGMA__SIZES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unrolls</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLICE_PRAGMA__UNROLLS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Slice Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLICE_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.MergeArraysPragmaImpl <em>Merge Arrays Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.MergeArraysPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getMergeArraysPragma()
	 * @generated
	 */
	int MERGE_ARRAYS_PRAGMA = 17;

	/**
	 * The feature id for the '<em><b>Symbols</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ARRAYS_PRAGMA__SYMBOLS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Merge Arrays Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ARRAYS_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl <em>S2S4HLS Pragma Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getS2S4HLSPragmaAnnotation()
	 * @generated
	 */
	int S2S4HLS_PRAGMA_ANNOTATION = 18;

	/**
	 * The feature id for the '<em><b>Directives</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES = AnnotationsPackage.IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int S2S4HLS_PRAGMA_ANNOTATION__TARGET = AnnotationsPackage.IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Original Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA = AnnotationsPackage.IANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>S2S4HLS Pragma Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int S2S4HLS_PRAGMA_ANNOTATION_FEATURE_COUNT = AnnotationsPackage.IANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl <em>Annotated Proc Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAnnotatedProcSet()
	 * @generated
	 */
	int ANNOTATED_PROC_SET = 19;

	/**
	 * The feature id for the '<em><b>Procset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_PROC_SET__PROCSET = 0;

	/**
	 * The feature id for the '<em><b>Pragmas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_PROC_SET__PRAGMAS = 1;

	/**
	 * The number of structural features of the '<em>Annotated Proc Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_PROC_SET_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AffineTermHackImpl <em>Affine Term Hack</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AffineTermHackImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAffineTermHack()
	 * @generated
	 */
	int AFFINE_TERM_HACK = 20;

	/**
	 * The feature id for the '<em><b>Coef</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM_HACK__COEF = AffinePackage.AFFINE_TERM__COEF;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM_HACK__VARIABLE = AffinePackage.AFFINE_TERM__VARIABLE;

	/**
	 * The number of structural features of the '<em>Affine Term Hack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM_HACK_FEATURE_COUNT = AffinePackage.AFFINE_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleVectorImpl <em>Schedule Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleVectorImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleVector()
	 * @generated
	 */
	int SCHEDULE_VECTOR = 21;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_VECTOR__EXPR = 0;

	/**
	 * The number of structural features of the '<em>Schedule Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_VECTOR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleStatementPragmaImpl <em>Schedule Statement Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleStatementPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleStatementPragma()
	 * @generated
	 */
	int SCHEDULE_STATEMENT_PRAGMA = 22;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_STATEMENT_PRAGMA__STATEMENT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exprs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_STATEMENT_PRAGMA__EXPRS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Schedule Statement Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_STATEMENT_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl <em>Schedule Block Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleBlockPragma()
	 * @generated
	 */
	int SCHEDULE_BLOCK_PRAGMA = 23;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_BLOCK_PRAGMA__STATEMENTS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exprs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_BLOCK_PRAGMA__EXPRS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Schedule Block Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_BLOCK_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl <em>Omp Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getOmpPragma()
	 * @generated
	 */
	int OMP_PRAGMA = 24;

	/**
	 * The feature id for the '<em><b>Parallel For</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OMP_PRAGMA__PARALLEL_FOR = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Privates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OMP_PRAGMA__PRIVATES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Shared</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OMP_PRAGMA__SHARED = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Reductions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OMP_PRAGMA__REDUCTIONS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Omp Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OMP_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.IgnoreMemoryDependencyImpl <em>Ignore Memory Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.IgnoreMemoryDependencyImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getIgnoreMemoryDependency()
	 * @generated
	 */
	int IGNORE_MEMORY_DEPENDENCY = 25;

	/**
	 * The feature id for the '<em><b>Symbols</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGNORE_MEMORY_DEPENDENCY__SYMBOLS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGNORE_MEMORY_DEPENDENCY__FROM = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGNORE_MEMORY_DEPENDENCY__TO = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGNORE_MEMORY_DEPENDENCY__TYPE = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Ignore Memory Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGNORE_MEMORY_DEPENDENCY_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl <em>Loop Pipeline Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getLoopPipelinePragma()
	 * @generated
	 */
	int LOOP_PIPELINE_PRAGMA = 26;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_PIPELINE_PRAGMA__LATENCY = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hash Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_PIPELINE_PRAGMA__HASH_FUNCTION = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Loop Pipeline Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_PIPELINE_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl <em>Alma Coarse Grain Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAlmaCoarseGrainPragma()
	 * @generated
	 */
	int ALMA_COARSE_GRAIN_PRAGMA = 27;

	/**
	 * The feature id for the '<em><b>Nb Tasks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Task Ids</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tile Sizes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Alma Coarse Grain Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_COARSE_GRAIN_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaFineGrainPragmaImpl <em>Alma Fine Grain Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaFineGrainPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAlmaFineGrainPragma()
	 * @generated
	 */
	int ALMA_FINE_GRAIN_PRAGMA = 28;

	/**
	 * The feature id for the '<em><b>Tile Sizes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Alma Fine Grain Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALMA_FINE_GRAIN_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.RLTInsetPragmaImpl <em>RLT Inset Pragma</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.RLTInsetPragmaImpl
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getRLTInsetPragma()
	 * @generated
	 */
	int RLT_INSET_PRAGMA = 29;

	/**
	 * The number of structural features of the '<em>RLT Inset Pragma</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RLT_INSET_PRAGMA_FEATURE_COUNT = SCOP_TRANSFORM_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.model.scop.transform.DepType <em>Dep Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DepType
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDepType()
	 * @generated
	 */
	int DEP_TYPE = 30;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getString()
	 * @generated
	 */
	int STRING = 31;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getint()
	 * @generated
	 */
	int INT = 32;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransform <em>Scop Transform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scop Transform</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScopTransform
	 * @generated
	 */
	EClass getScopTransform();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransform#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScopTransform#getCommands()
	 * @see #getScopTransform()
	 * @generated
	 */
	EReference getScopTransform_Commands();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective <em>Scop Transform Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scop Transform Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
	 * @generated
	 */
	EClass getScopTransformDirective();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective <em>Add Context Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Add Context Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective
	 * @generated
	 */
	EClass getAddContextDirective();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getParams <em>Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Params</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getParams()
	 * @see #getAddContextDirective()
	 * @generated
	 */
	EReference getAddContextDirective_Params();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Context</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective#getContext()
	 * @see #getAddContextDirective()
	 * @generated
	 */
	EReference getAddContextDirective_Context();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective <em>Bubble Insertion Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bubble Insertion Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective
	 * @generated
	 */
	EClass getBubbleInsertionDirective();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getBubbleSet <em>Bubble Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bubble Set</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getBubbleSet()
	 * @see #getBubbleInsertionDirective()
	 * @generated
	 */
	EAttribute getBubbleInsertionDirective_BubbleSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getDepth()
	 * @see #getBubbleInsertionDirective()
	 * @generated
	 */
	EAttribute getBubbleInsertionDirective_Depth();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getLatency <em>Latency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getLatency()
	 * @see #getBubbleInsertionDirective()
	 * @generated
	 */
	EAttribute getBubbleInsertionDirective_Latency();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getCorrectionAlgorithm <em>Correction Algorithm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correction Algorithm</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective#getCorrectionAlgorithm()
	 * @see #getBubbleInsertionDirective()
	 * @generated
	 */
	EAttribute getBubbleInsertionDirective_CorrectionAlgorithm();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective <em>Liveness Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Liveness Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective
	 * @generated
	 */
	EClass getLivenessDirective();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isKill <em>Kill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kill</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isKill()
	 * @see #getLivenessDirective()
	 * @generated
	 */
	EAttribute getLivenessDirective_Kill();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveIn <em>Live In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Live In</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveIn()
	 * @see #getLivenessDirective()
	 * @generated
	 */
	EAttribute getLivenessDirective_LiveIn();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveOut <em>Live Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Live Out</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#isLiveOut()
	 * @see #getLivenessDirective()
	 * @generated
	 */
	EAttribute getLivenessDirective_LiveOut();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective#getSymbol()
	 * @see #getLivenessDirective()
	 * @generated
	 */
	EReference getLivenessDirective_Symbol();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective <em>Tiling Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tiling Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TilingDirective
	 * @generated
	 */
	EClass getTilingDirective();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getNested <em>Nested</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nested</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getNested()
	 * @see #getTilingDirective()
	 * @generated
	 */
	EReference getTilingDirective_Nested();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getSizes <em>Sizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sizes</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TilingDirective#getSizes()
	 * @see #getTilingDirective()
	 * @generated
	 */
	EAttribute getTilingDirective_Sizes();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective <em>Data Layout Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Layout Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
	 * @generated
	 */
	EClass getDataLayoutDirective();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getAddresses <em>Addresses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Addresses</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getAddresses()
	 * @see #getDataLayoutDirective()
	 * @generated
	 */
	EReference getDataLayoutDirective_Addresses();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getVars <em>Vars</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vars</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getVars()
	 * @see #getDataLayoutDirective()
	 * @generated
	 */
	EReference getDataLayoutDirective_Vars();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective#getSymbol()
	 * @see #getDataLayoutDirective()
	 * @generated
	 */
	EReference getDataLayoutDirective_Symbol();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective <em>Schedule Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective
	 * @generated
	 */
	EClass getScheduleDirective();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Schedule</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getSchedule()
	 * @see #getScheduleDirective()
	 * @generated
	 */
	EAttribute getScheduleDirective_Schedule();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective#getStatement()
	 * @see #getScheduleDirective()
	 * @generated
	 */
	EReference getScheduleDirective_Statement();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective <em>Contract Array Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract Array Directive</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective
	 * @generated
	 */
	EClass getContractArrayDirective();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective#getSymbol()
	 * @see #getContractArrayDirective()
	 * @generated
	 */
	EReference getContractArrayDirective_Symbol();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions <em>Cloog Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cloog Options</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions
	 * @generated
	 */
	EClass getCloogOptions();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getEqualitySpreading <em>Equality Spreading</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equality Spreading</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getEqualitySpreading()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_EqualitySpreading();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStop <em>Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stop</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStop()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_Stop();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthToOptimize <em>First Depth To Optimize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Depth To Optimize</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthToOptimize()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_FirstDepthToOptimize();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthSpreading <em>First Depth Spreading</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Depth Spreading</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getFirstDepthSpreading()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_FirstDepthSpreading();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStrides <em>Strides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strides</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getStrides()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_Strides();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Block</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getBlock()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_Block();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getLastDepthToOptimize <em>Last Depth To Optimize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Depth To Optimize</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getLastDepthToOptimize()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_LastDepthToOptimize();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getConstantSpreading <em>Constant Spreading</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant Spreading</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getConstantSpreading()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_ConstantSpreading();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getOnceTimeLoopElim <em>Once Time Loop Elim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Once Time Loop Elim</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getOnceTimeLoopElim()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_OnceTimeLoopElim();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getCoalescingDepth <em>Coalescing Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coalescing Depth</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions#getCoalescingDepth()
	 * @see #getCloogOptions()
	 * @generated
	 */
	EAttribute getCloogOptions_CoalescingDepth();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma <em>Pure Function Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pure Function Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma
	 * @generated
	 */
	EClass getPureFunctionPragma();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma <em>Schedule Context Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Context Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma
	 * @generated
	 */
	EClass getScheduleContextPragma();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getConstraints()
	 * @see #getScheduleContextPragma()
	 * @generated
	 */
	EReference getScheduleContextPragma_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getName()
	 * @see #getScheduleContextPragma()
	 * @generated
	 */
	EAttribute getScheduleContextPragma_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getScheduling <em>Scheduling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scheduling</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma#getScheduling()
	 * @see #getScheduleContextPragma()
	 * @generated
	 */
	EAttribute getScheduleContextPragma_Scheduling();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma <em>Unroll Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unroll Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma
	 * @generated
	 */
	EClass getUnrollPragma();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma#getFactor <em>Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Factor</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma#getFactor()
	 * @see #getUnrollPragma()
	 * @generated
	 */
	EAttribute getUnrollPragma_Factor();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.InlinePragma <em>Inline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inline Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.InlinePragma
	 * @generated
	 */
	EClass getInlinePragma();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.InlinePragma#getProc <em>Proc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Proc</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.InlinePragma#getProc()
	 * @see #getInlinePragma()
	 * @generated
	 */
	EReference getInlinePragma_Proc();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma <em>Data Motion Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Motion Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma
	 * @generated
	 */
	EClass getDataMotionPragma();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getSymbols <em>Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Symbols</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getSymbols()
	 * @see #getDataMotionPragma()
	 * @generated
	 */
	EReference getDataMotionPragma_Symbols();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getReuseDims <em>Reuse Dims</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reuse Dims</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma#getReuseDims()
	 * @see #getDataMotionPragma()
	 * @generated
	 */
	EAttribute getDataMotionPragma_ReuseDims();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma <em>Duplicate Node Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Duplicate Node Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma
	 * @generated
	 */
	EClass getDuplicateNodePragma();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getSymbol()
	 * @see #getDuplicateNodePragma()
	 * @generated
	 */
	EReference getDuplicateNodePragma_Symbol();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getFactor <em>Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Factor</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma#getFactor()
	 * @see #getDuplicateNodePragma()
	 * @generated
	 */
	EAttribute getDuplicateNodePragma_Factor();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma <em>Slice Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Slice Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.SlicePragma
	 * @generated
	 */
	EClass getSlicePragma();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getSizes <em>Sizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sizes</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getSizes()
	 * @see #getSlicePragma()
	 * @generated
	 */
	EAttribute getSlicePragma_Sizes();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getUnrolls <em>Unrolls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Unrolls</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.SlicePragma#getUnrolls()
	 * @see #getSlicePragma()
	 * @generated
	 */
	EAttribute getSlicePragma_Unrolls();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma <em>Merge Arrays Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Arrays Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma
	 * @generated
	 */
	EClass getMergeArraysPragma();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma#getSymbols <em>Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Symbols</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma#getSymbols()
	 * @see #getMergeArraysPragma()
	 * @generated
	 */
	EReference getMergeArraysPragma_Symbols();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation <em>S2S4HLS Pragma Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>S2S4HLS Pragma Annotation</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation
	 * @generated
	 */
	EClass getS2S4HLSPragmaAnnotation();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getDirectives <em>Directives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Directives</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getDirectives()
	 * @see #getS2S4HLSPragmaAnnotation()
	 * @generated
	 */
	EReference getS2S4HLSPragmaAnnotation_Directives();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getTarget()
	 * @see #getS2S4HLSPragmaAnnotation()
	 * @generated
	 */
	EReference getS2S4HLSPragmaAnnotation_Target();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getOriginalPragma <em>Original Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Original Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation#getOriginalPragma()
	 * @see #getS2S4HLSPragmaAnnotation()
	 * @generated
	 */
	EReference getS2S4HLSPragmaAnnotation_OriginalPragma();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet <em>Annotated Proc Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Proc Set</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet
	 * @generated
	 */
	EClass getAnnotatedProcSet();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getProcset <em>Procset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Procset</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getProcset()
	 * @see #getAnnotatedProcSet()
	 * @generated
	 */
	EReference getAnnotatedProcSet_Procset();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getPragmas <em>Pragmas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pragmas</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet#getPragmas()
	 * @see #getAnnotatedProcSet()
	 * @generated
	 */
	EReference getAnnotatedProcSet_Pragmas();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack <em>Affine Term Hack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Affine Term Hack</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack
	 * @generated
	 */
	EClass getAffineTermHack();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector <em>Schedule Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Vector</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector
	 * @generated
	 */
	EClass getScheduleVector();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expr</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector#getExpr()
	 * @see #getScheduleVector()
	 * @generated
	 */
	EReference getScheduleVector_Expr();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma <em>Schedule Statement Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Statement Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma
	 * @generated
	 */
	EClass getScheduleStatementPragma();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Statement</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getStatement()
	 * @see #getScheduleStatementPragma()
	 * @generated
	 */
	EReference getScheduleStatementPragma_Statement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getExprs <em>Exprs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exprs</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma#getExprs()
	 * @see #getScheduleStatementPragma()
	 * @generated
	 */
	EReference getScheduleStatementPragma_Exprs();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma <em>Schedule Block Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Block Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma
	 * @generated
	 */
	EClass getScheduleBlockPragma();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Statements</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getStatements()
	 * @see #getScheduleBlockPragma()
	 * @generated
	 */
	EReference getScheduleBlockPragma_Statements();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getExprs <em>Exprs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exprs</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma#getExprs()
	 * @see #getScheduleBlockPragma()
	 * @generated
	 */
	EReference getScheduleBlockPragma_Exprs();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma <em>Omp Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Omp Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma
	 * @generated
	 */
	EClass getOmpPragma();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#isParallelFor <em>Parallel For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parallel For</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#isParallelFor()
	 * @see #getOmpPragma()
	 * @generated
	 */
	EAttribute getOmpPragma_ParallelFor();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getPrivates <em>Privates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Privates</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getPrivates()
	 * @see #getOmpPragma()
	 * @generated
	 */
	EReference getOmpPragma_Privates();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getShared <em>Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Shared</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getShared()
	 * @see #getOmpPragma()
	 * @generated
	 */
	EReference getOmpPragma_Shared();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getReductions <em>Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reductions</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma#getReductions()
	 * @see #getOmpPragma()
	 * @generated
	 */
	EReference getOmpPragma_Reductions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency <em>Ignore Memory Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ignore Memory Dependency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency
	 * @generated
	 */
	EClass getIgnoreMemoryDependency();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getSymbols <em>Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Symbols</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getSymbols()
	 * @see #getIgnoreMemoryDependency()
	 * @generated
	 */
	EReference getIgnoreMemoryDependency_Symbols();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getFrom()
	 * @see #getIgnoreMemoryDependency()
	 * @generated
	 */
	EReference getIgnoreMemoryDependency_From();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getTo()
	 * @see #getIgnoreMemoryDependency()
	 * @generated
	 */
	EReference getIgnoreMemoryDependency_To();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency#getType()
	 * @see #getIgnoreMemoryDependency()
	 * @generated
	 */
	EAttribute getIgnoreMemoryDependency_Type();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma <em>Loop Pipeline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop Pipeline Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma
	 * @generated
	 */
	EClass getLoopPipelinePragma();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getLatency <em>Latency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latency</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getLatency()
	 * @see #getLoopPipelinePragma()
	 * @generated
	 */
	EAttribute getLoopPipelinePragma_Latency();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getHashFunction <em>Hash Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hash Function</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma#getHashFunction()
	 * @see #getLoopPipelinePragma()
	 * @generated
	 */
	EAttribute getLoopPipelinePragma_HashFunction();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma <em>Alma Coarse Grain Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alma Coarse Grain Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma
	 * @generated
	 */
	EClass getAlmaCoarseGrainPragma();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getNbTasks <em>Nb Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Tasks</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getNbTasks()
	 * @see #getAlmaCoarseGrainPragma()
	 * @generated
	 */
	EAttribute getAlmaCoarseGrainPragma_NbTasks();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTaskIds <em>Task Ids</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Task Ids</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTaskIds()
	 * @see #getAlmaCoarseGrainPragma()
	 * @generated
	 */
	EAttribute getAlmaCoarseGrainPragma_TaskIds();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTileSizes <em>Tile Sizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tile Sizes</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma#getTileSizes()
	 * @see #getAlmaCoarseGrainPragma()
	 * @generated
	 */
	EAttribute getAlmaCoarseGrainPragma_TileSizes();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma <em>Alma Fine Grain Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alma Fine Grain Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma
	 * @generated
	 */
	EClass getAlmaFineGrainPragma();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma#getTileSizes <em>Tile Sizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tile Sizes</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma#getTileSizes()
	 * @see #getAlmaFineGrainPragma()
	 * @generated
	 */
	EAttribute getAlmaFineGrainPragma_TileSizes();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma <em>RLT Inset Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RLT Inset Pragma</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma
	 * @generated
	 */
	EClass getRLTInsetPragma();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.gecos.model.scop.transform.DepType <em>Dep Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dep Type</em>'.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DepType
	 * @generated
	 */
	EEnum getDepType();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TransformFactory getTransformFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformImpl <em>Scop Transform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScopTransform()
		 * @generated
		 */
		EClass SCOP_TRANSFORM = eINSTANCE.getScopTransform();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOP_TRANSFORM__COMMANDS = eINSTANCE.getScopTransform_Commands();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformDirectiveImpl <em>Scop Transform Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScopTransformDirective()
		 * @generated
		 */
		EClass SCOP_TRANSFORM_DIRECTIVE = eINSTANCE.getScopTransformDirective();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl <em>Add Context Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAddContextDirective()
		 * @generated
		 */
		EClass ADD_CONTEXT_DIRECTIVE = eINSTANCE.getAddContextDirective();

		/**
		 * The meta object literal for the '<em><b>Params</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADD_CONTEXT_DIRECTIVE__PARAMS = eINSTANCE.getAddContextDirective_Params();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADD_CONTEXT_DIRECTIVE__CONTEXT = eINSTANCE.getAddContextDirective_Context();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl <em>Bubble Insertion Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getBubbleInsertionDirective()
		 * @generated
		 */
		EClass BUBBLE_INSERTION_DIRECTIVE = eINSTANCE.getBubbleInsertionDirective();

		/**
		 * The meta object literal for the '<em><b>Bubble Set</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET = eINSTANCE.getBubbleInsertionDirective_BubbleSet();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUBBLE_INSERTION_DIRECTIVE__DEPTH = eINSTANCE.getBubbleInsertionDirective_Depth();

		/**
		 * The meta object literal for the '<em><b>Latency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUBBLE_INSERTION_DIRECTIVE__LATENCY = eINSTANCE.getBubbleInsertionDirective_Latency();

		/**
		 * The meta object literal for the '<em><b>Correction Algorithm</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM = eINSTANCE.getBubbleInsertionDirective_CorrectionAlgorithm();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl <em>Liveness Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getLivenessDirective()
		 * @generated
		 */
		EClass LIVENESS_DIRECTIVE = eINSTANCE.getLivenessDirective();

		/**
		 * The meta object literal for the '<em><b>Kill</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIVENESS_DIRECTIVE__KILL = eINSTANCE.getLivenessDirective_Kill();

		/**
		 * The meta object literal for the '<em><b>Live In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIVENESS_DIRECTIVE__LIVE_IN = eINSTANCE.getLivenessDirective_LiveIn();

		/**
		 * The meta object literal for the '<em><b>Live Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIVENESS_DIRECTIVE__LIVE_OUT = eINSTANCE.getLivenessDirective_LiveOut();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIVENESS_DIRECTIVE__SYMBOL = eINSTANCE.getLivenessDirective_Symbol();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.TilingDirectiveImpl <em>Tiling Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TilingDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getTilingDirective()
		 * @generated
		 */
		EClass TILING_DIRECTIVE = eINSTANCE.getTilingDirective();

		/**
		 * The meta object literal for the '<em><b>Nested</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TILING_DIRECTIVE__NESTED = eINSTANCE.getTilingDirective_Nested();

		/**
		 * The meta object literal for the '<em><b>Sizes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TILING_DIRECTIVE__SIZES = eINSTANCE.getTilingDirective_Sizes();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl <em>Data Layout Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDataLayoutDirective()
		 * @generated
		 */
		EClass DATA_LAYOUT_DIRECTIVE = eINSTANCE.getDataLayoutDirective();

		/**
		 * The meta object literal for the '<em><b>Addresses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LAYOUT_DIRECTIVE__ADDRESSES = eINSTANCE.getDataLayoutDirective_Addresses();

		/**
		 * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LAYOUT_DIRECTIVE__VARS = eINSTANCE.getDataLayoutDirective_Vars();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LAYOUT_DIRECTIVE__SYMBOL = eINSTANCE.getDataLayoutDirective_Symbol();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleDirectiveImpl <em>Schedule Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleDirective()
		 * @generated
		 */
		EClass SCHEDULE_DIRECTIVE = eINSTANCE.getScheduleDirective();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_DIRECTIVE__SCHEDULE = eINSTANCE.getScheduleDirective_Schedule();

		/**
		 * The meta object literal for the '<em><b>Statement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_DIRECTIVE__STATEMENT = eINSTANCE.getScheduleDirective_Statement();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ContractArrayDirectiveImpl <em>Contract Array Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ContractArrayDirectiveImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getContractArrayDirective()
		 * @generated
		 */
		EClass CONTRACT_ARRAY_DIRECTIVE = eINSTANCE.getContractArrayDirective();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_ARRAY_DIRECTIVE__SYMBOL = eINSTANCE.getContractArrayDirective_Symbol();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl <em>Cloog Options</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getCloogOptions()
		 * @generated
		 */
		EClass CLOOG_OPTIONS = eINSTANCE.getCloogOptions();

		/**
		 * The meta object literal for the '<em><b>Equality Spreading</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__EQUALITY_SPREADING = eINSTANCE.getCloogOptions_EqualitySpreading();

		/**
		 * The meta object literal for the '<em><b>Stop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__STOP = eINSTANCE.getCloogOptions_Stop();

		/**
		 * The meta object literal for the '<em><b>First Depth To Optimize</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE = eINSTANCE.getCloogOptions_FirstDepthToOptimize();

		/**
		 * The meta object literal for the '<em><b>First Depth Spreading</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__FIRST_DEPTH_SPREADING = eINSTANCE.getCloogOptions_FirstDepthSpreading();

		/**
		 * The meta object literal for the '<em><b>Strides</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__STRIDES = eINSTANCE.getCloogOptions_Strides();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__BLOCK = eINSTANCE.getCloogOptions_Block();

		/**
		 * The meta object literal for the '<em><b>Last Depth To Optimize</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE = eINSTANCE.getCloogOptions_LastDepthToOptimize();

		/**
		 * The meta object literal for the '<em><b>Constant Spreading</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__CONSTANT_SPREADING = eINSTANCE.getCloogOptions_ConstantSpreading();

		/**
		 * The meta object literal for the '<em><b>Once Time Loop Elim</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM = eINSTANCE.getCloogOptions_OnceTimeLoopElim();

		/**
		 * The meta object literal for the '<em><b>Coalescing Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOOG_OPTIONS__COALESCING_DEPTH = eINSTANCE.getCloogOptions_CoalescingDepth();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.PureFunctionPragmaImpl <em>Pure Function Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.PureFunctionPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getPureFunctionPragma()
		 * @generated
		 */
		EClass PURE_FUNCTION_PRAGMA = eINSTANCE.getPureFunctionPragma();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl <em>Schedule Context Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleContextPragma()
		 * @generated
		 */
		EClass SCHEDULE_CONTEXT_PRAGMA = eINSTANCE.getScheduleContextPragma();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS = eINSTANCE.getScheduleContextPragma_Constraints();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_CONTEXT_PRAGMA__NAME = eINSTANCE.getScheduleContextPragma_Name();

		/**
		 * The meta object literal for the '<em><b>Scheduling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_CONTEXT_PRAGMA__SCHEDULING = eINSTANCE.getScheduleContextPragma_Scheduling();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.UnrollPragmaImpl <em>Unroll Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.UnrollPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getUnrollPragma()
		 * @generated
		 */
		EClass UNROLL_PRAGMA = eINSTANCE.getUnrollPragma();

		/**
		 * The meta object literal for the '<em><b>Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNROLL_PRAGMA__FACTOR = eINSTANCE.getUnrollPragma_Factor();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.InlinePragmaImpl <em>Inline Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.InlinePragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getInlinePragma()
		 * @generated
		 */
		EClass INLINE_PRAGMA = eINSTANCE.getInlinePragma();

		/**
		 * The meta object literal for the '<em><b>Proc</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INLINE_PRAGMA__PROC = eINSTANCE.getInlinePragma_Proc();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl <em>Data Motion Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDataMotionPragma()
		 * @generated
		 */
		EClass DATA_MOTION_PRAGMA = eINSTANCE.getDataMotionPragma();

		/**
		 * The meta object literal for the '<em><b>Symbols</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_MOTION_PRAGMA__SYMBOLS = eINSTANCE.getDataMotionPragma_Symbols();

		/**
		 * The meta object literal for the '<em><b>Reuse Dims</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_MOTION_PRAGMA__REUSE_DIMS = eINSTANCE.getDataMotionPragma_ReuseDims();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DuplicateNodePragmaImpl <em>Duplicate Node Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.DuplicateNodePragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDuplicateNodePragma()
		 * @generated
		 */
		EClass DUPLICATE_NODE_PRAGMA = eINSTANCE.getDuplicateNodePragma();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DUPLICATE_NODE_PRAGMA__SYMBOL = eINSTANCE.getDuplicateNodePragma_Symbol();

		/**
		 * The meta object literal for the '<em><b>Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DUPLICATE_NODE_PRAGMA__FACTOR = eINSTANCE.getDuplicateNodePragma_Factor();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl <em>Slice Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getSlicePragma()
		 * @generated
		 */
		EClass SLICE_PRAGMA = eINSTANCE.getSlicePragma();

		/**
		 * The meta object literal for the '<em><b>Sizes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLICE_PRAGMA__SIZES = eINSTANCE.getSlicePragma_Sizes();

		/**
		 * The meta object literal for the '<em><b>Unrolls</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLICE_PRAGMA__UNROLLS = eINSTANCE.getSlicePragma_Unrolls();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.MergeArraysPragmaImpl <em>Merge Arrays Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.MergeArraysPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getMergeArraysPragma()
		 * @generated
		 */
		EClass MERGE_ARRAYS_PRAGMA = eINSTANCE.getMergeArraysPragma();

		/**
		 * The meta object literal for the '<em><b>Symbols</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ARRAYS_PRAGMA__SYMBOLS = eINSTANCE.getMergeArraysPragma_Symbols();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl <em>S2S4HLS Pragma Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getS2S4HLSPragmaAnnotation()
		 * @generated
		 */
		EClass S2S4HLS_PRAGMA_ANNOTATION = eINSTANCE.getS2S4HLSPragmaAnnotation();

		/**
		 * The meta object literal for the '<em><b>Directives</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES = eINSTANCE.getS2S4HLSPragmaAnnotation_Directives();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference S2S4HLS_PRAGMA_ANNOTATION__TARGET = eINSTANCE.getS2S4HLSPragmaAnnotation_Target();

		/**
		 * The meta object literal for the '<em><b>Original Pragma</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA = eINSTANCE.getS2S4HLSPragmaAnnotation_OriginalPragma();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl <em>Annotated Proc Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAnnotatedProcSet()
		 * @generated
		 */
		EClass ANNOTATED_PROC_SET = eINSTANCE.getAnnotatedProcSet();

		/**
		 * The meta object literal for the '<em><b>Procset</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED_PROC_SET__PROCSET = eINSTANCE.getAnnotatedProcSet_Procset();

		/**
		 * The meta object literal for the '<em><b>Pragmas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED_PROC_SET__PRAGMAS = eINSTANCE.getAnnotatedProcSet_Pragmas();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AffineTermHackImpl <em>Affine Term Hack</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AffineTermHackImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAffineTermHack()
		 * @generated
		 */
		EClass AFFINE_TERM_HACK = eINSTANCE.getAffineTermHack();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleVectorImpl <em>Schedule Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleVectorImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleVector()
		 * @generated
		 */
		EClass SCHEDULE_VECTOR = eINSTANCE.getScheduleVector();

		/**
		 * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_VECTOR__EXPR = eINSTANCE.getScheduleVector_Expr();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleStatementPragmaImpl <em>Schedule Statement Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleStatementPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleStatementPragma()
		 * @generated
		 */
		EClass SCHEDULE_STATEMENT_PRAGMA = eINSTANCE.getScheduleStatementPragma();

		/**
		 * The meta object literal for the '<em><b>Statement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_STATEMENT_PRAGMA__STATEMENT = eINSTANCE.getScheduleStatementPragma_Statement();

		/**
		 * The meta object literal for the '<em><b>Exprs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_STATEMENT_PRAGMA__EXPRS = eINSTANCE.getScheduleStatementPragma_Exprs();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl <em>Schedule Block Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getScheduleBlockPragma()
		 * @generated
		 */
		EClass SCHEDULE_BLOCK_PRAGMA = eINSTANCE.getScheduleBlockPragma();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_BLOCK_PRAGMA__STATEMENTS = eINSTANCE.getScheduleBlockPragma_Statements();

		/**
		 * The meta object literal for the '<em><b>Exprs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_BLOCK_PRAGMA__EXPRS = eINSTANCE.getScheduleBlockPragma_Exprs();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl <em>Omp Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getOmpPragma()
		 * @generated
		 */
		EClass OMP_PRAGMA = eINSTANCE.getOmpPragma();

		/**
		 * The meta object literal for the '<em><b>Parallel For</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OMP_PRAGMA__PARALLEL_FOR = eINSTANCE.getOmpPragma_ParallelFor();

		/**
		 * The meta object literal for the '<em><b>Privates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OMP_PRAGMA__PRIVATES = eINSTANCE.getOmpPragma_Privates();

		/**
		 * The meta object literal for the '<em><b>Shared</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OMP_PRAGMA__SHARED = eINSTANCE.getOmpPragma_Shared();

		/**
		 * The meta object literal for the '<em><b>Reductions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OMP_PRAGMA__REDUCTIONS = eINSTANCE.getOmpPragma_Reductions();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.IgnoreMemoryDependencyImpl <em>Ignore Memory Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.IgnoreMemoryDependencyImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getIgnoreMemoryDependency()
		 * @generated
		 */
		EClass IGNORE_MEMORY_DEPENDENCY = eINSTANCE.getIgnoreMemoryDependency();

		/**
		 * The meta object literal for the '<em><b>Symbols</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGNORE_MEMORY_DEPENDENCY__SYMBOLS = eINSTANCE.getIgnoreMemoryDependency_Symbols();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGNORE_MEMORY_DEPENDENCY__FROM = eINSTANCE.getIgnoreMemoryDependency_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGNORE_MEMORY_DEPENDENCY__TO = eINSTANCE.getIgnoreMemoryDependency_To();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IGNORE_MEMORY_DEPENDENCY__TYPE = eINSTANCE.getIgnoreMemoryDependency_Type();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl <em>Loop Pipeline Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getLoopPipelinePragma()
		 * @generated
		 */
		EClass LOOP_PIPELINE_PRAGMA = eINSTANCE.getLoopPipelinePragma();

		/**
		 * The meta object literal for the '<em><b>Latency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_PIPELINE_PRAGMA__LATENCY = eINSTANCE.getLoopPipelinePragma_Latency();

		/**
		 * The meta object literal for the '<em><b>Hash Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_PIPELINE_PRAGMA__HASH_FUNCTION = eINSTANCE.getLoopPipelinePragma_HashFunction();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl <em>Alma Coarse Grain Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAlmaCoarseGrainPragma()
		 * @generated
		 */
		EClass ALMA_COARSE_GRAIN_PRAGMA = eINSTANCE.getAlmaCoarseGrainPragma();

		/**
		 * The meta object literal for the '<em><b>Nb Tasks</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS = eINSTANCE.getAlmaCoarseGrainPragma_NbTasks();

		/**
		 * The meta object literal for the '<em><b>Task Ids</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS = eINSTANCE.getAlmaCoarseGrainPragma_TaskIds();

		/**
		 * The meta object literal for the '<em><b>Tile Sizes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES = eINSTANCE.getAlmaCoarseGrainPragma_TileSizes();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaFineGrainPragmaImpl <em>Alma Fine Grain Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaFineGrainPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getAlmaFineGrainPragma()
		 * @generated
		 */
		EClass ALMA_FINE_GRAIN_PRAGMA = eINSTANCE.getAlmaFineGrainPragma();

		/**
		 * The meta object literal for the '<em><b>Tile Sizes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES = eINSTANCE.getAlmaFineGrainPragma_TileSizes();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.impl.RLTInsetPragmaImpl <em>RLT Inset Pragma</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.RLTInsetPragmaImpl
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getRLTInsetPragma()
		 * @generated
		 */
		EClass RLT_INSET_PRAGMA = eINSTANCE.getRLTInsetPragma();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.model.scop.transform.DepType <em>Dep Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.DepType
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getDepType()
		 * @generated
		 */
		EEnum DEP_TYPE = eINSTANCE.getDepType();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.scop.transform.impl.TransformPackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

	}

} //TransformPackage
