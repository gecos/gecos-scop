/**
 */
package fr.irisa.cairn.gecos.model.scop.transform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unroll Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma#getFactor <em>Factor</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getUnrollPragma()
 * @model
 * @generated
 */
public interface UnrollPragma extends ScopTransformDirective {
	/**
	 * Returns the value of the '<em><b>Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Factor</em>' attribute.
	 * @see #setFactor(int)
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#getUnrollPragma_Factor()
	 * @model unique="false" dataType="fr.irisa.cairn.gecos.model.scop.transform.int"
	 * @generated
	 */
	int getFactor();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma#getFactor <em>Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Factor</em>' attribute.
	 * @see #getFactor()
	 * @generated
	 */
	void setFactor(int value);

} // UnrollPragma
