/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;

import fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.polymodel.algebra.IntConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Add Context Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl#getParams <em>Params</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AddContextDirectiveImpl#getContext <em>Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AddContextDirectiveImpl extends MinimalEObjectImpl.Container implements AddContextDirective {
	/**
	 * The cached value of the '{@link #getParams() <em>Params</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParams()
	 * @generated
	 * @ordered
	 */
	protected ScopDimension params;

	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected IntConstraintSystem context;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AddContextDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.ADD_CONTEXT_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDimension getParams() {
		if (params != null && params.eIsProxy()) {
			InternalEObject oldParams = (InternalEObject)params;
			params = (ScopDimension)eResolveProxy(oldParams);
			if (params != oldParams) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS, oldParams, params));
			}
		}
		return params;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopDimension basicGetParams() {
		return params;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParams(ScopDimension newParams) {
		ScopDimension oldParams = params;
		params = newParams;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS, oldParams, params));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem getContext() {
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContext(IntConstraintSystem newContext, NotificationChain msgs) {
		IntConstraintSystem oldContext = context;
		context = newContext;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT, oldContext, newContext);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(IntConstraintSystem newContext) {
		if (newContext != context) {
			NotificationChain msgs = null;
			if (context != null)
				msgs = ((InternalEObject)context).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT, null, msgs);
			if (newContext != null)
				msgs = ((InternalEObject)newContext).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT, null, msgs);
			msgs = basicSetContext(newContext, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT, newContext, newContext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT:
				return basicSetContext(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS:
				if (resolve) return getParams();
				return basicGetParams();
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT:
				return getContext();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS:
				setParams((ScopDimension)newValue);
				return;
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT:
				setContext((IntConstraintSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS:
				setParams((ScopDimension)null);
				return;
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT:
				setContext((IntConstraintSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__PARAMS:
				return params != null;
			case TransformPackage.ADD_CONTEXT_DIRECTIVE__CONTEXT:
				return context != null;
		}
		return super.eIsSet(featureID);
	}

} //AddContextDirectiveImpl
