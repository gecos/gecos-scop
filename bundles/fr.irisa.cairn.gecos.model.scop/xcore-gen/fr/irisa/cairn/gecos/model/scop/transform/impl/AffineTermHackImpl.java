/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.ecore.EClass;
import org.polymodel.algebra.affine.impl.AffineTermImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Affine Term Hack</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AffineTermHackImpl extends AffineTermImpl implements AffineTermHack {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AffineTermHackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.AFFINE_TERM_HACK;
	}

} //AffineTermHackImpl
