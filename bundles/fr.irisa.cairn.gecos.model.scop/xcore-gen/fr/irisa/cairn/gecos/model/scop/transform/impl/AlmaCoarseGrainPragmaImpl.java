/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alma Coarse Grain Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl#getNbTasks <em>Nb Tasks</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl#getTaskIds <em>Task Ids</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaCoarseGrainPragmaImpl#getTileSizes <em>Tile Sizes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlmaCoarseGrainPragmaImpl extends ScopTransformDirectiveImpl implements AlmaCoarseGrainPragma {
	/**
	 * The default value of the '{@link #getNbTasks() <em>Nb Tasks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbTasks()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_TASKS_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getNbTasks() <em>Nb Tasks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbTasks()
	 * @generated
	 * @ordered
	 */
	protected int nbTasks = NB_TASKS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTaskIds() <em>Task Ids</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskIds()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> taskIds;

	/**
	 * The cached value of the '{@link #getTileSizes() <em>Tile Sizes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTileSizes()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> tileSizes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlmaCoarseGrainPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.ALMA_COARSE_GRAIN_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbTasks() {
		return nbTasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbTasks(int newNbTasks) {
		int oldNbTasks = nbTasks;
		nbTasks = newNbTasks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS, oldNbTasks, nbTasks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getTaskIds() {
		if (taskIds == null) {
			taskIds = new EDataTypeUniqueEList<Integer>(Integer.class, this, TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS);
		}
		return taskIds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getTileSizes() {
		if (tileSizes == null) {
			tileSizes = new EDataTypeUniqueEList<Integer>(Integer.class, this, TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES);
		}
		return tileSizes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS:
				return getNbTasks();
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS:
				return getTaskIds();
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES:
				return getTileSizes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS:
				setNbTasks((Integer)newValue);
				return;
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS:
				getTaskIds().clear();
				getTaskIds().addAll((Collection<? extends Integer>)newValue);
				return;
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES:
				getTileSizes().clear();
				getTileSizes().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS:
				setNbTasks(NB_TASKS_EDEFAULT);
				return;
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS:
				getTaskIds().clear();
				return;
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES:
				getTileSizes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS:
				return nbTasks != NB_TASKS_EDEFAULT;
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS:
				return taskIds != null && !taskIds.isEmpty();
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES:
				return tileSizes != null && !tileSizes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nbTasks: ");
		result.append(nbTasks);
		result.append(", taskIds: ");
		result.append(taskIds);
		result.append(", tileSizes: ");
		result.append(tileSizes);
		result.append(')');
		return result.toString();
	}

} //AlmaCoarseGrainPragmaImpl
