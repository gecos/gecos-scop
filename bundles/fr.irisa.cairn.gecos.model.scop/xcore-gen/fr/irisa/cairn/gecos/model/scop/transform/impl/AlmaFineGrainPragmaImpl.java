/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alma Fine Grain Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AlmaFineGrainPragmaImpl#getTileSizes <em>Tile Sizes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlmaFineGrainPragmaImpl extends ScopTransformDirectiveImpl implements AlmaFineGrainPragma {
	/**
	 * The cached value of the '{@link #getTileSizes() <em>Tile Sizes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTileSizes()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> tileSizes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlmaFineGrainPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.ALMA_FINE_GRAIN_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getTileSizes() {
		if (tileSizes == null) {
			tileSizes = new EDataTypeUniqueEList<Integer>(Integer.class, this, TransformPackage.ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES);
		}
		return tileSizes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES:
				return getTileSizes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES:
				getTileSizes().clear();
				getTileSizes().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES:
				getTileSizes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES:
				return tileSizes != null && !tileSizes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tileSizes: ");
		result.append(tileSizes);
		result.append(')');
		return result.toString();
	}

} //AlmaFineGrainPragmaImpl
