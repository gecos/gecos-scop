/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.ProcedureSet;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotated Proc Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl#getProcset <em>Procset</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.AnnotatedProcSetImpl#getPragmas <em>Pragmas</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotatedProcSetImpl extends MinimalEObjectImpl.Container implements AnnotatedProcSet {
	/**
	 * The cached value of the '{@link #getProcset() <em>Procset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcset()
	 * @generated
	 * @ordered
	 */
	protected ProcedureSet procset;

	/**
	 * The cached value of the '{@link #getPragmas() <em>Pragmas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmas()
	 * @generated
	 * @ordered
	 */
	protected EList<S2S4HLSPragmaAnnotation> pragmas;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotatedProcSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.ANNOTATED_PROC_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getProcset() {
		return procset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcset(ProcedureSet newProcset, NotificationChain msgs) {
		ProcedureSet oldProcset = procset;
		procset = newProcset;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TransformPackage.ANNOTATED_PROC_SET__PROCSET, oldProcset, newProcset);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcset(ProcedureSet newProcset) {
		if (newProcset != procset) {
			NotificationChain msgs = null;
			if (procset != null)
				msgs = ((InternalEObject)procset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TransformPackage.ANNOTATED_PROC_SET__PROCSET, null, msgs);
			if (newProcset != null)
				msgs = ((InternalEObject)newProcset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TransformPackage.ANNOTATED_PROC_SET__PROCSET, null, msgs);
			msgs = basicSetProcset(newProcset, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.ANNOTATED_PROC_SET__PROCSET, newProcset, newProcset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<S2S4HLSPragmaAnnotation> getPragmas() {
		if (pragmas == null) {
			pragmas = new EObjectContainmentEList<S2S4HLSPragmaAnnotation>(S2S4HLSPragmaAnnotation.class, this, TransformPackage.ANNOTATED_PROC_SET__PRAGMAS);
		}
		return pragmas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.ANNOTATED_PROC_SET__PROCSET:
				return basicSetProcset(null, msgs);
			case TransformPackage.ANNOTATED_PROC_SET__PRAGMAS:
				return ((InternalEList<?>)getPragmas()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.ANNOTATED_PROC_SET__PROCSET:
				return getProcset();
			case TransformPackage.ANNOTATED_PROC_SET__PRAGMAS:
				return getPragmas();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.ANNOTATED_PROC_SET__PROCSET:
				setProcset((ProcedureSet)newValue);
				return;
			case TransformPackage.ANNOTATED_PROC_SET__PRAGMAS:
				getPragmas().clear();
				getPragmas().addAll((Collection<? extends S2S4HLSPragmaAnnotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.ANNOTATED_PROC_SET__PROCSET:
				setProcset((ProcedureSet)null);
				return;
			case TransformPackage.ANNOTATED_PROC_SET__PRAGMAS:
				getPragmas().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.ANNOTATED_PROC_SET__PROCSET:
				return procset != null;
			case TransformPackage.ANNOTATED_PROC_SET__PRAGMAS:
				return pragmas != null && !pragmas.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AnnotatedProcSetImpl
