/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bubble Insertion Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl#getBubbleSet <em>Bubble Set</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl#getDepth <em>Depth</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl#getLatency <em>Latency</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.BubbleInsertionDirectiveImpl#getCorrectionAlgorithm <em>Correction Algorithm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BubbleInsertionDirectiveImpl extends MinimalEObjectImpl.Container implements BubbleInsertionDirective {
	/**
	 * The default value of the '{@link #getBubbleSet() <em>Bubble Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBubbleSet()
	 * @generated
	 * @ordered
	 */
	protected static final String BUBBLE_SET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBubbleSet() <em>Bubble Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBubbleSet()
	 * @generated
	 * @ordered
	 */
	protected String bubbleSet = BUBBLE_SET_EDEFAULT;

	/**
	 * The default value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected int depth = DEPTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getLatency() <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatency()
	 * @generated
	 * @ordered
	 */
	protected static final int LATENCY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLatency() <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatency()
	 * @generated
	 * @ordered
	 */
	protected int latency = LATENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getCorrectionAlgorithm() <em>Correction Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectionAlgorithm()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRECTION_ALGORITHM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCorrectionAlgorithm() <em>Correction Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectionAlgorithm()
	 * @generated
	 * @ordered
	 */
	protected String correctionAlgorithm = CORRECTION_ALGORITHM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BubbleInsertionDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.BUBBLE_INSERTION_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBubbleSet() {
		return bubbleSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBubbleSet(String newBubbleSet) {
		String oldBubbleSet = bubbleSet;
		bubbleSet = newBubbleSet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET, oldBubbleSet, bubbleSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepth(int newDepth) {
		int oldDepth = depth;
		depth = newDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.BUBBLE_INSERTION_DIRECTIVE__DEPTH, oldDepth, depth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLatency() {
		return latency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatency(int newLatency) {
		int oldLatency = latency;
		latency = newLatency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.BUBBLE_INSERTION_DIRECTIVE__LATENCY, oldLatency, latency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCorrectionAlgorithm() {
		return correctionAlgorithm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrectionAlgorithm(String newCorrectionAlgorithm) {
		String oldCorrectionAlgorithm = correctionAlgorithm;
		correctionAlgorithm = newCorrectionAlgorithm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM, oldCorrectionAlgorithm, correctionAlgorithm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET:
				return getBubbleSet();
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__DEPTH:
				return getDepth();
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__LATENCY:
				return getLatency();
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM:
				return getCorrectionAlgorithm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET:
				setBubbleSet((String)newValue);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__DEPTH:
				setDepth((Integer)newValue);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__LATENCY:
				setLatency((Integer)newValue);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM:
				setCorrectionAlgorithm((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET:
				setBubbleSet(BUBBLE_SET_EDEFAULT);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__DEPTH:
				setDepth(DEPTH_EDEFAULT);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__LATENCY:
				setLatency(LATENCY_EDEFAULT);
				return;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM:
				setCorrectionAlgorithm(CORRECTION_ALGORITHM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET:
				return BUBBLE_SET_EDEFAULT == null ? bubbleSet != null : !BUBBLE_SET_EDEFAULT.equals(bubbleSet);
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__DEPTH:
				return depth != DEPTH_EDEFAULT;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__LATENCY:
				return latency != LATENCY_EDEFAULT;
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM:
				return CORRECTION_ALGORITHM_EDEFAULT == null ? correctionAlgorithm != null : !CORRECTION_ALGORITHM_EDEFAULT.equals(correctionAlgorithm);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bubbleSet: ");
		result.append(bubbleSet);
		result.append(", depth: ");
		result.append(depth);
		result.append(", latency: ");
		result.append(latency);
		result.append(", correctionAlgorithm: ");
		result.append(correctionAlgorithm);
		result.append(')');
		return result.toString();
	}

} //BubbleInsertionDirectiveImpl
