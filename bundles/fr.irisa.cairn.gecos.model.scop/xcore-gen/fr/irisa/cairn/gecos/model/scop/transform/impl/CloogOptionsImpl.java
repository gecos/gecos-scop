/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.CloogOptions;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cloog Options</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getEqualitySpreading <em>Equality Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getStop <em>Stop</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getFirstDepthToOptimize <em>First Depth To Optimize</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getFirstDepthSpreading <em>First Depth Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getStrides <em>Strides</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getBlock <em>Block</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getLastDepthToOptimize <em>Last Depth To Optimize</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getConstantSpreading <em>Constant Spreading</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getOnceTimeLoopElim <em>Once Time Loop Elim</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.CloogOptionsImpl#getCoalescingDepth <em>Coalescing Depth</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CloogOptionsImpl extends ScopTransformDirectiveImpl implements CloogOptions {
	/**
	 * The default value of the '{@link #getEqualitySpreading() <em>Equality Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEqualitySpreading()
	 * @generated
	 * @ordered
	 */
	protected static final int EQUALITY_SPREADING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEqualitySpreading() <em>Equality Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEqualitySpreading()
	 * @generated
	 * @ordered
	 */
	protected int equalitySpreading = EQUALITY_SPREADING_EDEFAULT;

	/**
	 * The default value of the '{@link #getStop() <em>Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStop()
	 * @generated
	 * @ordered
	 */
	protected static final int STOP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStop() <em>Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStop()
	 * @generated
	 * @ordered
	 */
	protected int stop = STOP_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstDepthToOptimize() <em>First Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstDepthToOptimize()
	 * @generated
	 * @ordered
	 */
	protected static final int FIRST_DEPTH_TO_OPTIMIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFirstDepthToOptimize() <em>First Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstDepthToOptimize()
	 * @generated
	 * @ordered
	 */
	protected int firstDepthToOptimize = FIRST_DEPTH_TO_OPTIMIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstDepthSpreading() <em>First Depth Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstDepthSpreading()
	 * @generated
	 * @ordered
	 */
	protected static final int FIRST_DEPTH_SPREADING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFirstDepthSpreading() <em>First Depth Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstDepthSpreading()
	 * @generated
	 * @ordered
	 */
	protected int firstDepthSpreading = FIRST_DEPTH_SPREADING_EDEFAULT;

	/**
	 * The default value of the '{@link #getStrides() <em>Strides</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrides()
	 * @generated
	 * @ordered
	 */
	protected static final int STRIDES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStrides() <em>Strides</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrides()
	 * @generated
	 * @ordered
	 */
	protected int strides = STRIDES_EDEFAULT;

	/**
	 * The default value of the '{@link #getBlock() <em>Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected static final int BLOCK_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBlock() <em>Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected int block = BLOCK_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastDepthToOptimize() <em>Last Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastDepthToOptimize()
	 * @generated
	 * @ordered
	 */
	protected static final int LAST_DEPTH_TO_OPTIMIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLastDepthToOptimize() <em>Last Depth To Optimize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastDepthToOptimize()
	 * @generated
	 * @ordered
	 */
	protected int lastDepthToOptimize = LAST_DEPTH_TO_OPTIMIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getConstantSpreading() <em>Constant Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantSpreading()
	 * @generated
	 * @ordered
	 */
	protected static final int CONSTANT_SPREADING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getConstantSpreading() <em>Constant Spreading</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantSpreading()
	 * @generated
	 * @ordered
	 */
	protected int constantSpreading = CONSTANT_SPREADING_EDEFAULT;

	/**
	 * The default value of the '{@link #getOnceTimeLoopElim() <em>Once Time Loop Elim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnceTimeLoopElim()
	 * @generated
	 * @ordered
	 */
	protected static final int ONCE_TIME_LOOP_ELIM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOnceTimeLoopElim() <em>Once Time Loop Elim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnceTimeLoopElim()
	 * @generated
	 * @ordered
	 */
	protected int onceTimeLoopElim = ONCE_TIME_LOOP_ELIM_EDEFAULT;

	/**
	 * The default value of the '{@link #getCoalescingDepth() <em>Coalescing Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoalescingDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int COALESCING_DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCoalescingDepth() <em>Coalescing Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoalescingDepth()
	 * @generated
	 * @ordered
	 */
	protected int coalescingDepth = COALESCING_DEPTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CloogOptionsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.CLOOG_OPTIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEqualitySpreading() {
		return equalitySpreading;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEqualitySpreading(int newEqualitySpreading) {
		int oldEqualitySpreading = equalitySpreading;
		equalitySpreading = newEqualitySpreading;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__EQUALITY_SPREADING, oldEqualitySpreading, equalitySpreading));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStop() {
		return stop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStop(int newStop) {
		int oldStop = stop;
		stop = newStop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__STOP, oldStop, stop));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFirstDepthToOptimize() {
		return firstDepthToOptimize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstDepthToOptimize(int newFirstDepthToOptimize) {
		int oldFirstDepthToOptimize = firstDepthToOptimize;
		firstDepthToOptimize = newFirstDepthToOptimize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE, oldFirstDepthToOptimize, firstDepthToOptimize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFirstDepthSpreading() {
		return firstDepthSpreading;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstDepthSpreading(int newFirstDepthSpreading) {
		int oldFirstDepthSpreading = firstDepthSpreading;
		firstDepthSpreading = newFirstDepthSpreading;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_SPREADING, oldFirstDepthSpreading, firstDepthSpreading));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStrides() {
		return strides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrides(int newStrides) {
		int oldStrides = strides;
		strides = newStrides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__STRIDES, oldStrides, strides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBlock() {
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlock(int newBlock) {
		int oldBlock = block;
		block = newBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__BLOCK, oldBlock, block));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLastDepthToOptimize() {
		return lastDepthToOptimize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastDepthToOptimize(int newLastDepthToOptimize) {
		int oldLastDepthToOptimize = lastDepthToOptimize;
		lastDepthToOptimize = newLastDepthToOptimize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE, oldLastDepthToOptimize, lastDepthToOptimize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getConstantSpreading() {
		return constantSpreading;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantSpreading(int newConstantSpreading) {
		int oldConstantSpreading = constantSpreading;
		constantSpreading = newConstantSpreading;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__CONSTANT_SPREADING, oldConstantSpreading, constantSpreading));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOnceTimeLoopElim() {
		return onceTimeLoopElim;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnceTimeLoopElim(int newOnceTimeLoopElim) {
		int oldOnceTimeLoopElim = onceTimeLoopElim;
		onceTimeLoopElim = newOnceTimeLoopElim;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM, oldOnceTimeLoopElim, onceTimeLoopElim));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCoalescingDepth() {
		return coalescingDepth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoalescingDepth(int newCoalescingDepth) {
		int oldCoalescingDepth = coalescingDepth;
		coalescingDepth = newCoalescingDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.CLOOG_OPTIONS__COALESCING_DEPTH, oldCoalescingDepth, coalescingDepth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.CLOOG_OPTIONS__EQUALITY_SPREADING:
				return getEqualitySpreading();
			case TransformPackage.CLOOG_OPTIONS__STOP:
				return getStop();
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE:
				return getFirstDepthToOptimize();
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_SPREADING:
				return getFirstDepthSpreading();
			case TransformPackage.CLOOG_OPTIONS__STRIDES:
				return getStrides();
			case TransformPackage.CLOOG_OPTIONS__BLOCK:
				return getBlock();
			case TransformPackage.CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE:
				return getLastDepthToOptimize();
			case TransformPackage.CLOOG_OPTIONS__CONSTANT_SPREADING:
				return getConstantSpreading();
			case TransformPackage.CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM:
				return getOnceTimeLoopElim();
			case TransformPackage.CLOOG_OPTIONS__COALESCING_DEPTH:
				return getCoalescingDepth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.CLOOG_OPTIONS__EQUALITY_SPREADING:
				setEqualitySpreading((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__STOP:
				setStop((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE:
				setFirstDepthToOptimize((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_SPREADING:
				setFirstDepthSpreading((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__STRIDES:
				setStrides((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__BLOCK:
				setBlock((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE:
				setLastDepthToOptimize((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__CONSTANT_SPREADING:
				setConstantSpreading((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM:
				setOnceTimeLoopElim((Integer)newValue);
				return;
			case TransformPackage.CLOOG_OPTIONS__COALESCING_DEPTH:
				setCoalescingDepth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.CLOOG_OPTIONS__EQUALITY_SPREADING:
				setEqualitySpreading(EQUALITY_SPREADING_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__STOP:
				setStop(STOP_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE:
				setFirstDepthToOptimize(FIRST_DEPTH_TO_OPTIMIZE_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_SPREADING:
				setFirstDepthSpreading(FIRST_DEPTH_SPREADING_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__STRIDES:
				setStrides(STRIDES_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__BLOCK:
				setBlock(BLOCK_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE:
				setLastDepthToOptimize(LAST_DEPTH_TO_OPTIMIZE_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__CONSTANT_SPREADING:
				setConstantSpreading(CONSTANT_SPREADING_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM:
				setOnceTimeLoopElim(ONCE_TIME_LOOP_ELIM_EDEFAULT);
				return;
			case TransformPackage.CLOOG_OPTIONS__COALESCING_DEPTH:
				setCoalescingDepth(COALESCING_DEPTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.CLOOG_OPTIONS__EQUALITY_SPREADING:
				return equalitySpreading != EQUALITY_SPREADING_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__STOP:
				return stop != STOP_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE:
				return firstDepthToOptimize != FIRST_DEPTH_TO_OPTIMIZE_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__FIRST_DEPTH_SPREADING:
				return firstDepthSpreading != FIRST_DEPTH_SPREADING_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__STRIDES:
				return strides != STRIDES_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__BLOCK:
				return block != BLOCK_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE:
				return lastDepthToOptimize != LAST_DEPTH_TO_OPTIMIZE_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__CONSTANT_SPREADING:
				return constantSpreading != CONSTANT_SPREADING_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM:
				return onceTimeLoopElim != ONCE_TIME_LOOP_ELIM_EDEFAULT;
			case TransformPackage.CLOOG_OPTIONS__COALESCING_DEPTH:
				return coalescingDepth != COALESCING_DEPTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (equalitySpreading: ");
		result.append(equalitySpreading);
		result.append(", stop: ");
		result.append(stop);
		result.append(", firstDepthToOptimize: ");
		result.append(firstDepthToOptimize);
		result.append(", firstDepthSpreading: ");
		result.append(firstDepthSpreading);
		result.append(", strides: ");
		result.append(strides);
		result.append(", block: ");
		result.append(block);
		result.append(", lastDepthToOptimize: ");
		result.append(lastDepthToOptimize);
		result.append(", constantSpreading: ");
		result.append(constantSpreading);
		result.append(", onceTimeLoopElim: ");
		result.append(onceTimeLoopElim);
		result.append(", coalescingDepth: ");
		result.append(coalescingDepth);
		result.append(')');
		return result.toString();
	}

} //CloogOptionsImpl
