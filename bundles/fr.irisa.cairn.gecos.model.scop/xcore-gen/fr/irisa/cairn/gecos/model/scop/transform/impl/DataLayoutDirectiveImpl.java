/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;

import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.Symbol;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Layout Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl#getAddresses <em>Addresses</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl#getVars <em>Vars</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataLayoutDirectiveImpl#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataLayoutDirectiveImpl extends ScopTransformDirectiveImpl implements DataLayoutDirective {
	/**
	 * The cached value of the '{@link #getAddresses() <em>Addresses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddresses()
	 * @generated
	 * @ordered
	 */
	protected EList<IntExpression> addresses;

	/**
	 * The cached value of the '{@link #getVars() <em>Vars</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVars()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopDimension> vars;

	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected Symbol symbol;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLayoutDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.DATA_LAYOUT_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntExpression> getAddresses() {
		if (addresses == null) {
			addresses = new EObjectContainmentEList<IntExpression>(IntExpression.class, this, TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES);
		}
		return addresses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopDimension> getVars() {
		if (vars == null) {
			vars = new EObjectContainmentEList<ScopDimension>(ScopDimension.class, this, TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS);
		}
		return vars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSymbol() {
		if (symbol != null && symbol.eIsProxy()) {
			InternalEObject oldSymbol = (InternalEObject)symbol;
			symbol = (Symbol)eResolveProxy(oldSymbol);
			if (symbol != oldSymbol) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL, oldSymbol, symbol));
			}
		}
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(Symbol newSymbol) {
		Symbol oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES:
				return ((InternalEList<?>)getAddresses()).basicRemove(otherEnd, msgs);
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS:
				return ((InternalEList<?>)getVars()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES:
				return getAddresses();
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS:
				return getVars();
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL:
				if (resolve) return getSymbol();
				return basicGetSymbol();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES:
				getAddresses().clear();
				getAddresses().addAll((Collection<? extends IntExpression>)newValue);
				return;
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS:
				getVars().clear();
				getVars().addAll((Collection<? extends ScopDimension>)newValue);
				return;
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL:
				setSymbol((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES:
				getAddresses().clear();
				return;
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS:
				getVars().clear();
				return;
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL:
				setSymbol((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__ADDRESSES:
				return addresses != null && !addresses.isEmpty();
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__VARS:
				return vars != null && !vars.isEmpty();
			case TransformPackage.DATA_LAYOUT_DIRECTIVE__SYMBOL:
				return symbol != null;
		}
		return super.eIsSet(featureID);
	}

} //DataLayoutDirectiveImpl
