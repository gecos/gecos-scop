/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.Symbol;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Motion Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl#getSymbols <em>Symbols</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.DataMotionPragmaImpl#getReuseDims <em>Reuse Dims</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataMotionPragmaImpl extends ScopTransformDirectiveImpl implements DataMotionPragma {
	/**
	 * The cached value of the '{@link #getSymbols() <em>Symbols</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbols()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> symbols;

	/**
	 * The default value of the '{@link #getReuseDims() <em>Reuse Dims</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReuseDims()
	 * @generated
	 * @ordered
	 */
	protected static final int REUSE_DIMS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getReuseDims() <em>Reuse Dims</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReuseDims()
	 * @generated
	 * @ordered
	 */
	protected int reuseDims = REUSE_DIMS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataMotionPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.DATA_MOTION_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getSymbols() {
		if (symbols == null) {
			symbols = new EObjectResolvingEList<Symbol>(Symbol.class, this, TransformPackage.DATA_MOTION_PRAGMA__SYMBOLS);
		}
		return symbols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getReuseDims() {
		return reuseDims;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReuseDims(int newReuseDims) {
		int oldReuseDims = reuseDims;
		reuseDims = newReuseDims;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.DATA_MOTION_PRAGMA__REUSE_DIMS, oldReuseDims, reuseDims));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.DATA_MOTION_PRAGMA__SYMBOLS:
				return getSymbols();
			case TransformPackage.DATA_MOTION_PRAGMA__REUSE_DIMS:
				return getReuseDims();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.DATA_MOTION_PRAGMA__SYMBOLS:
				getSymbols().clear();
				getSymbols().addAll((Collection<? extends Symbol>)newValue);
				return;
			case TransformPackage.DATA_MOTION_PRAGMA__REUSE_DIMS:
				setReuseDims((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.DATA_MOTION_PRAGMA__SYMBOLS:
				getSymbols().clear();
				return;
			case TransformPackage.DATA_MOTION_PRAGMA__REUSE_DIMS:
				setReuseDims(REUSE_DIMS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.DATA_MOTION_PRAGMA__SYMBOLS:
				return symbols != null && !symbols.isEmpty();
			case TransformPackage.DATA_MOTION_PRAGMA__REUSE_DIMS:
				return reuseDims != REUSE_DIMS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (reuseDims: ");
		result.append(reuseDims);
		result.append(')');
		return result.toString();
	}

} //DataMotionPragmaImpl
