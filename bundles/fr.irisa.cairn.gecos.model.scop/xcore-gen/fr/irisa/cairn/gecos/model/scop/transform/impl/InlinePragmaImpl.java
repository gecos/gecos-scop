/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.InlinePragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.ProcedureSymbol;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inline Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.InlinePragmaImpl#getProc <em>Proc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InlinePragmaImpl extends ScopTransformDirectiveImpl implements InlinePragma {
	/**
	 * The cached value of the '{@link #getProc() <em>Proc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProc()
	 * @generated
	 * @ordered
	 */
	protected ProcedureSymbol proc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InlinePragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.INLINE_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSymbol getProc() {
		if (proc != null && proc.eIsProxy()) {
			InternalEObject oldProc = (InternalEObject)proc;
			proc = (ProcedureSymbol)eResolveProxy(oldProc);
			if (proc != oldProc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformPackage.INLINE_PRAGMA__PROC, oldProc, proc));
			}
		}
		return proc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSymbol basicGetProc() {
		return proc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProc(ProcedureSymbol newProc) {
		ProcedureSymbol oldProc = proc;
		proc = newProc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.INLINE_PRAGMA__PROC, oldProc, proc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.INLINE_PRAGMA__PROC:
				if (resolve) return getProc();
				return basicGetProc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.INLINE_PRAGMA__PROC:
				setProc((ProcedureSymbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.INLINE_PRAGMA__PROC:
				setProc((ProcedureSymbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.INLINE_PRAGMA__PROC:
				return proc != null;
		}
		return super.eIsSet(featureID);
	}

} //InlinePragmaImpl
