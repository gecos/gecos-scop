/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.Symbol;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Liveness Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl#isKill <em>Kill</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl#isLiveIn <em>Live In</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl#isLiveOut <em>Live Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LivenessDirectiveImpl#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LivenessDirectiveImpl extends MinimalEObjectImpl.Container implements LivenessDirective {
	/**
	 * The default value of the '{@link #isKill() <em>Kill</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKill()
	 * @generated
	 * @ordered
	 */
	protected static final boolean KILL_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isKill() <em>Kill</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKill()
	 * @generated
	 * @ordered
	 */
	protected boolean kill = KILL_EDEFAULT;
	/**
	 * The default value of the '{@link #isLiveIn() <em>Live In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLiveIn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LIVE_IN_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isLiveIn() <em>Live In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLiveIn()
	 * @generated
	 * @ordered
	 */
	protected boolean liveIn = LIVE_IN_EDEFAULT;
	/**
	 * The default value of the '{@link #isLiveOut() <em>Live Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLiveOut()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LIVE_OUT_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isLiveOut() <em>Live Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLiveOut()
	 * @generated
	 * @ordered
	 */
	protected boolean liveOut = LIVE_OUT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected Symbol symbol;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LivenessDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.LIVENESS_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isKill() {
		return kill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKill(boolean newKill) {
		boolean oldKill = kill;
		kill = newKill;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LIVENESS_DIRECTIVE__KILL, oldKill, kill));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLiveIn() {
		return liveIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiveIn(boolean newLiveIn) {
		boolean oldLiveIn = liveIn;
		liveIn = newLiveIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LIVENESS_DIRECTIVE__LIVE_IN, oldLiveIn, liveIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLiveOut() {
		return liveOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiveOut(boolean newLiveOut) {
		boolean oldLiveOut = liveOut;
		liveOut = newLiveOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LIVENESS_DIRECTIVE__LIVE_OUT, oldLiveOut, liveOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSymbol() {
		if (symbol != null && symbol.eIsProxy()) {
			InternalEObject oldSymbol = (InternalEObject)symbol;
			symbol = (Symbol)eResolveProxy(oldSymbol);
			if (symbol != oldSymbol) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformPackage.LIVENESS_DIRECTIVE__SYMBOL, oldSymbol, symbol));
			}
		}
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(Symbol newSymbol) {
		Symbol oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LIVENESS_DIRECTIVE__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.LIVENESS_DIRECTIVE__KILL:
				return isKill();
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_IN:
				return isLiveIn();
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_OUT:
				return isLiveOut();
			case TransformPackage.LIVENESS_DIRECTIVE__SYMBOL:
				if (resolve) return getSymbol();
				return basicGetSymbol();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.LIVENESS_DIRECTIVE__KILL:
				setKill((Boolean)newValue);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_IN:
				setLiveIn((Boolean)newValue);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_OUT:
				setLiveOut((Boolean)newValue);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__SYMBOL:
				setSymbol((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.LIVENESS_DIRECTIVE__KILL:
				setKill(KILL_EDEFAULT);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_IN:
				setLiveIn(LIVE_IN_EDEFAULT);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_OUT:
				setLiveOut(LIVE_OUT_EDEFAULT);
				return;
			case TransformPackage.LIVENESS_DIRECTIVE__SYMBOL:
				setSymbol((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.LIVENESS_DIRECTIVE__KILL:
				return kill != KILL_EDEFAULT;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_IN:
				return liveIn != LIVE_IN_EDEFAULT;
			case TransformPackage.LIVENESS_DIRECTIVE__LIVE_OUT:
				return liveOut != LIVE_OUT_EDEFAULT;
			case TransformPackage.LIVENESS_DIRECTIVE__SYMBOL:
				return symbol != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (kill: ");
		result.append(kill);
		result.append(", liveIn: ");
		result.append(liveIn);
		result.append(", liveOut: ");
		result.append(liveOut);
		result.append(')');
		return result.toString();
	}

} //LivenessDirectiveImpl
