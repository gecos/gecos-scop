/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Loop Pipeline Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl#getLatency <em>Latency</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.LoopPipelinePragmaImpl#getHashFunction <em>Hash Function</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoopPipelinePragmaImpl extends ScopTransformDirectiveImpl implements LoopPipelinePragma {
	/**
	 * The default value of the '{@link #getLatency() <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatency()
	 * @generated
	 * @ordered
	 */
	protected static final int LATENCY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLatency() <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatency()
	 * @generated
	 * @ordered
	 */
	protected int latency = LATENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getHashFunction() <em>Hash Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHashFunction()
	 * @generated
	 * @ordered
	 */
	protected static final String HASH_FUNCTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHashFunction() <em>Hash Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHashFunction()
	 * @generated
	 * @ordered
	 */
	protected String hashFunction = HASH_FUNCTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoopPipelinePragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.LOOP_PIPELINE_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLatency() {
		return latency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatency(int newLatency) {
		int oldLatency = latency;
		latency = newLatency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LOOP_PIPELINE_PRAGMA__LATENCY, oldLatency, latency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHashFunction() {
		return hashFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHashFunction(String newHashFunction) {
		String oldHashFunction = hashFunction;
		hashFunction = newHashFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.LOOP_PIPELINE_PRAGMA__HASH_FUNCTION, oldHashFunction, hashFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.LOOP_PIPELINE_PRAGMA__LATENCY:
				return getLatency();
			case TransformPackage.LOOP_PIPELINE_PRAGMA__HASH_FUNCTION:
				return getHashFunction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.LOOP_PIPELINE_PRAGMA__LATENCY:
				setLatency((Integer)newValue);
				return;
			case TransformPackage.LOOP_PIPELINE_PRAGMA__HASH_FUNCTION:
				setHashFunction((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.LOOP_PIPELINE_PRAGMA__LATENCY:
				setLatency(LATENCY_EDEFAULT);
				return;
			case TransformPackage.LOOP_PIPELINE_PRAGMA__HASH_FUNCTION:
				setHashFunction(HASH_FUNCTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.LOOP_PIPELINE_PRAGMA__LATENCY:
				return latency != LATENCY_EDEFAULT;
			case TransformPackage.LOOP_PIPELINE_PRAGMA__HASH_FUNCTION:
				return HASH_FUNCTION_EDEFAULT == null ? hashFunction != null : !HASH_FUNCTION_EDEFAULT.equals(hashFunction);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (latency: ");
		result.append(latency);
		result.append(", hashFunction: ");
		result.append(hashFunction);
		result.append(')');
		return result.toString();
	}

} //LoopPipelinePragmaImpl
