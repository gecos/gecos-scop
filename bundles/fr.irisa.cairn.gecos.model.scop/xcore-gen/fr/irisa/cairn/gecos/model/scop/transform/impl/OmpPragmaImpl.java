/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.OmpPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.core.Symbol;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Omp Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl#isParallelFor <em>Parallel For</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl#getPrivates <em>Privates</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl#getShared <em>Shared</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.OmpPragmaImpl#getReductions <em>Reductions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OmpPragmaImpl extends ScopTransformDirectiveImpl implements OmpPragma {
	/**
	 * The default value of the '{@link #isParallelFor() <em>Parallel For</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParallelFor()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARALLEL_FOR_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isParallelFor() <em>Parallel For</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParallelFor()
	 * @generated
	 * @ordered
	 */
	protected boolean parallelFor = PARALLEL_FOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPrivates() <em>Privates</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivates()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> privates;

	/**
	 * The cached value of the '{@link #getShared() <em>Shared</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> shared;

	/**
	 * The cached value of the '{@link #getReductions() <em>Reductions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReductions()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> reductions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OmpPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.OMP_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParallelFor() {
		return parallelFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParallelFor(boolean newParallelFor) {
		boolean oldParallelFor = parallelFor;
		parallelFor = newParallelFor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.OMP_PRAGMA__PARALLEL_FOR, oldParallelFor, parallelFor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getPrivates() {
		if (privates == null) {
			privates = new EObjectResolvingEList<Symbol>(Symbol.class, this, TransformPackage.OMP_PRAGMA__PRIVATES);
		}
		return privates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getShared() {
		if (shared == null) {
			shared = new EObjectResolvingEList<Symbol>(Symbol.class, this, TransformPackage.OMP_PRAGMA__SHARED);
		}
		return shared;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getReductions() {
		if (reductions == null) {
			reductions = new EObjectResolvingEList<Symbol>(Symbol.class, this, TransformPackage.OMP_PRAGMA__REDUCTIONS);
		}
		return reductions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.OMP_PRAGMA__PARALLEL_FOR:
				return isParallelFor();
			case TransformPackage.OMP_PRAGMA__PRIVATES:
				return getPrivates();
			case TransformPackage.OMP_PRAGMA__SHARED:
				return getShared();
			case TransformPackage.OMP_PRAGMA__REDUCTIONS:
				return getReductions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.OMP_PRAGMA__PARALLEL_FOR:
				setParallelFor((Boolean)newValue);
				return;
			case TransformPackage.OMP_PRAGMA__PRIVATES:
				getPrivates().clear();
				getPrivates().addAll((Collection<? extends Symbol>)newValue);
				return;
			case TransformPackage.OMP_PRAGMA__SHARED:
				getShared().clear();
				getShared().addAll((Collection<? extends Symbol>)newValue);
				return;
			case TransformPackage.OMP_PRAGMA__REDUCTIONS:
				getReductions().clear();
				getReductions().addAll((Collection<? extends Symbol>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.OMP_PRAGMA__PARALLEL_FOR:
				setParallelFor(PARALLEL_FOR_EDEFAULT);
				return;
			case TransformPackage.OMP_PRAGMA__PRIVATES:
				getPrivates().clear();
				return;
			case TransformPackage.OMP_PRAGMA__SHARED:
				getShared().clear();
				return;
			case TransformPackage.OMP_PRAGMA__REDUCTIONS:
				getReductions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.OMP_PRAGMA__PARALLEL_FOR:
				return parallelFor != PARALLEL_FOR_EDEFAULT;
			case TransformPackage.OMP_PRAGMA__PRIVATES:
				return privates != null && !privates.isEmpty();
			case TransformPackage.OMP_PRAGMA__SHARED:
				return shared != null && !shared.isEmpty();
			case TransformPackage.OMP_PRAGMA__REDUCTIONS:
				return reductions != null && !reductions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (parallelFor: ");
		result.append(parallelFor);
		result.append(')');
		return result.toString();
	}

} //OmpPragmaImpl
