/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pure Function Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PureFunctionPragmaImpl extends ScopTransformDirectiveImpl implements PureFunctionPragma {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PureFunctionPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.PURE_FUNCTION_PRAGMA;
	}

} //PureFunctionPragmaImpl
