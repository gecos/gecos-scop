/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RLT Inset Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RLTInsetPragmaImpl extends ScopTransformDirectiveImpl implements RLTInsetPragma {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RLTInsetPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.RLT_INSET_PRAGMA;
	}

} //RLTInsetPragmaImpl
