/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.impl.IAnnotationImpl;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>S2S4HLS Pragma Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl#getDirectives <em>Directives</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.S2S4HLSPragmaAnnotationImpl#getOriginalPragma <em>Original Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class S2S4HLSPragmaAnnotationImpl extends IAnnotationImpl implements S2S4HLSPragmaAnnotation {
	/**
	 * The cached value of the '{@link #getDirectives() <em>Directives</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectives()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopTransformDirective> directives;

	/**
	 * The cached value of the '{@link #getOriginalPragma() <em>Original Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalPragma()
	 * @generated
	 * @ordered
	 */
	protected PragmaAnnotation originalPragma;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected S2S4HLSPragmaAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.S2S4HLS_PRAGMA_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopTransformDirective> getDirectives() {
		if (directives == null) {
			directives = new EObjectContainmentEList<ScopTransformDirective>(ScopTransformDirective.class, this, TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES);
		}
		return directives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement getTarget() {
		AnnotatedElement target = basicGetTarget();
		return target != null && target.eIsProxy() ? (AnnotatedElement)eResolveProxy((InternalEObject)target) : target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement basicGetTarget() {
		EObject _eContainer = this.eContainer().eContainer();
		return ((AnnotatedElement) _eContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation getOriginalPragma() {
		return originalPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginalPragma(PragmaAnnotation newOriginalPragma, NotificationChain msgs) {
		PragmaAnnotation oldOriginalPragma = originalPragma;
		originalPragma = newOriginalPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA, oldOriginalPragma, newOriginalPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalPragma(PragmaAnnotation newOriginalPragma) {
		if (newOriginalPragma != originalPragma) {
			NotificationChain msgs = null;
			if (originalPragma != null)
				msgs = ((InternalEObject)originalPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA, null, msgs);
			if (newOriginalPragma != null)
				msgs = ((InternalEObject)newOriginalPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA, null, msgs);
			msgs = basicSetOriginalPragma(newOriginalPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA, newOriginalPragma, newOriginalPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES:
				return ((InternalEList<?>)getDirectives()).basicRemove(otherEnd, msgs);
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA:
				return basicSetOriginalPragma(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES:
				return getDirectives();
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA:
				return getOriginalPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES:
				getDirectives().clear();
				getDirectives().addAll((Collection<? extends ScopTransformDirective>)newValue);
				return;
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA:
				setOriginalPragma((PragmaAnnotation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES:
				getDirectives().clear();
				return;
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA:
				setOriginalPragma((PragmaAnnotation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES:
				return directives != null && !directives.isEmpty();
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__TARGET:
				return basicGetTarget() != null;
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA:
				return originalPragma != null;
		}
		return super.eIsSet(featureID);
	}

} //S2S4HLSPragmaAnnotationImpl
