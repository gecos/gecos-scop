/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

import fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schedule Block Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl#getStatements <em>Statements</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleBlockPragmaImpl#getExprs <em>Exprs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduleBlockPragmaImpl extends ScopTransformDirectiveImpl implements ScheduleBlockPragma {
	/**
	 * The cached value of the '{@link #getStatements() <em>Statements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatements()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopStatement> statements;

	/**
	 * The cached value of the '{@link #getExprs() <em>Exprs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExprs()
	 * @generated
	 * @ordered
	 */
	protected EList<IntExpression> exprs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduleBlockPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.SCHEDULE_BLOCK_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopStatement> getStatements() {
		if (statements == null) {
			statements = new EObjectResolvingEList<ScopStatement>(ScopStatement.class, this, TransformPackage.SCHEDULE_BLOCK_PRAGMA__STATEMENTS);
		}
		return statements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntExpression> getExprs() {
		if (exprs == null) {
			exprs = new EObjectContainmentEList<IntExpression>(IntExpression.class, this, TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS);
		}
		return exprs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS:
				return ((InternalEList<?>)getExprs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__STATEMENTS:
				return getStatements();
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS:
				return getExprs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__STATEMENTS:
				getStatements().clear();
				getStatements().addAll((Collection<? extends ScopStatement>)newValue);
				return;
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS:
				getExprs().clear();
				getExprs().addAll((Collection<? extends IntExpression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__STATEMENTS:
				getStatements().clear();
				return;
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS:
				getExprs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__STATEMENTS:
				return statements != null && !statements.isEmpty();
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA__EXPRS:
				return exprs != null && !exprs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScheduleBlockPragmaImpl
