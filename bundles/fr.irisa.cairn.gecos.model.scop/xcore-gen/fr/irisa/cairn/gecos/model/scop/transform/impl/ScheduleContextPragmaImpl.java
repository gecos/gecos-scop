/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polymodel.algebra.IntConstraint;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schedule Context Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScheduleContextPragmaImpl#getScheduling <em>Scheduling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduleContextPragmaImpl extends ScopTransformDirectiveImpl implements ScheduleContextPragma {
	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraint> constraints;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getScheduling() <em>Scheduling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduling()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEDULING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScheduling() <em>Scheduling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduling()
	 * @generated
	 * @ordered
	 */
	protected String scheduling = SCHEDULING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduleContextPragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.SCHEDULE_CONTEXT_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<IntConstraint>(IntConstraint.class, this, TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.SCHEDULE_CONTEXT_PRAGMA__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScheduling() {
		return scheduling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduling(String newScheduling) {
		String oldScheduling = scheduling;
		scheduling = newScheduling;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformPackage.SCHEDULE_CONTEXT_PRAGMA__SCHEDULING, oldScheduling, scheduling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS:
				return getConstraints();
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__NAME:
				return getName();
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__SCHEDULING:
				return getScheduling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends IntConstraint>)newValue);
				return;
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__NAME:
				setName((String)newValue);
				return;
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__SCHEDULING:
				setScheduling((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS:
				getConstraints().clear();
				return;
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__SCHEDULING:
				setScheduling(SCHEDULING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA__SCHEDULING:
				return SCHEDULING_EDEFAULT == null ? scheduling != null : !SCHEDULING_EDEFAULT.equals(scheduling);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", scheduling: ");
		result.append(scheduling);
		result.append(')');
		return result.toString();
	}

} //ScheduleContextPragmaImpl
