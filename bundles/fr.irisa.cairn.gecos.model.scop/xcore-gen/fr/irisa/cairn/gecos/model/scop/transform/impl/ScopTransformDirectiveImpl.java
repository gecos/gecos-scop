/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;
import fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scop Transform Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ScopTransformDirectiveImpl extends MinimalEObjectImpl.Container implements ScopTransformDirective {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopTransformDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.SCOP_TRANSFORM_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return ScopPrettyPrinter.printDirective(this);
	}

} //ScopTransformDirectiveImpl
