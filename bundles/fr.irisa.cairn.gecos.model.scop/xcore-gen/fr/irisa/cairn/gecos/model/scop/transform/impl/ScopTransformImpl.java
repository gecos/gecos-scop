/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import com.google.common.base.Objects;

import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.model.scop.ScopStatement;

import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scop Transform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.ScopTransformImpl#getCommands <em>Commands</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopTransformImpl extends MinimalEObjectImpl.Container implements ScopTransform {
	/**
	 * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<ScopTransformDirective> commands;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopTransformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.SCOP_TRANSFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScopTransformDirective> getCommands() {
		if (commands == null) {
			commands = new EObjectContainmentEList<ScopTransformDirective>(ScopTransformDirective.class, this, TransformPackage.SCOP_TRANSFORM__COMMANDS);
		}
		return commands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScheduleDirective> listAllScheduleDirectives() {
		return ECollections.<ScheduleDirective>toEList(Iterables.<ScheduleDirective>filter(this.getCommands(), ScheduleDirective.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataLayoutDirective> listAllLayoutDirectives() {
		return ECollections.<DataLayoutDirective>toEList(Iterables.<DataLayoutDirective>filter(this.getCommands(), DataLayoutDirective.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleDirective getScheduleFor(final ScopStatement s) {
		final Function1<ScheduleDirective, Boolean> _function = new Function1<ScheduleDirective, Boolean>() {
			public Boolean apply(final ScheduleDirective schd) {
				ScopStatement _statement = schd.getStatement();
				return Boolean.valueOf(Objects.equal(_statement, s));
			}
		};
		return ((ScheduleDirective[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<ScheduleDirective>filter(Iterables.<ScheduleDirective>filter(this.getCommands(), ScheduleDirective.class), _function), ScheduleDirective.class))[0];
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TransformPackage.SCOP_TRANSFORM__COMMANDS:
				return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.SCOP_TRANSFORM__COMMANDS:
				return getCommands();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.SCOP_TRANSFORM__COMMANDS:
				getCommands().clear();
				getCommands().addAll((Collection<? extends ScopTransformDirective>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.SCOP_TRANSFORM__COMMANDS:
				getCommands().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.SCOP_TRANSFORM__COMMANDS:
				return commands != null && !commands.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScopTransformImpl
