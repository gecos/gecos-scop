/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.SlicePragma;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Slice Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl#getSizes <em>Sizes</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scop.transform.impl.SlicePragmaImpl#getUnrolls <em>Unrolls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SlicePragmaImpl extends ScopTransformDirectiveImpl implements SlicePragma {
	/**
	 * The cached value of the '{@link #getSizes() <em>Sizes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizes()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> sizes;

	/**
	 * The cached value of the '{@link #getUnrolls() <em>Unrolls</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnrolls()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> unrolls;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SlicePragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformPackage.Literals.SLICE_PRAGMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getSizes() {
		if (sizes == null) {
			sizes = new EDataTypeEList<Integer>(Integer.class, this, TransformPackage.SLICE_PRAGMA__SIZES);
		}
		return sizes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getUnrolls() {
		if (unrolls == null) {
			unrolls = new EDataTypeEList<Integer>(Integer.class, this, TransformPackage.SLICE_PRAGMA__UNROLLS);
		}
		return unrolls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformPackage.SLICE_PRAGMA__SIZES:
				return getSizes();
			case TransformPackage.SLICE_PRAGMA__UNROLLS:
				return getUnrolls();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformPackage.SLICE_PRAGMA__SIZES:
				getSizes().clear();
				getSizes().addAll((Collection<? extends Integer>)newValue);
				return;
			case TransformPackage.SLICE_PRAGMA__UNROLLS:
				getUnrolls().clear();
				getUnrolls().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformPackage.SLICE_PRAGMA__SIZES:
				getSizes().clear();
				return;
			case TransformPackage.SLICE_PRAGMA__UNROLLS:
				getUnrolls().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformPackage.SLICE_PRAGMA__SIZES:
				return sizes != null && !sizes.isEmpty();
			case TransformPackage.SLICE_PRAGMA__UNROLLS:
				return unrolls != null && !unrolls.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sizes: ");
		result.append(sizes);
		result.append(", unrolls: ");
		result.append(unrolls);
		result.append(')');
		return result.toString();
	}

} //SlicePragmaImpl
