/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.transform.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TransformFactoryImpl extends EFactoryImpl implements TransformFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TransformFactory init() {
		try {
			TransformFactory theTransformFactory = (TransformFactory)EPackage.Registry.INSTANCE.getEFactory(TransformPackage.eNS_URI);
			if (theTransformFactory != null) {
				return theTransformFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TransformFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TransformPackage.SCOP_TRANSFORM: return createScopTransform();
			case TransformPackage.SCOP_TRANSFORM_DIRECTIVE: return createScopTransformDirective();
			case TransformPackage.ADD_CONTEXT_DIRECTIVE: return createAddContextDirective();
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE: return createBubbleInsertionDirective();
			case TransformPackage.LIVENESS_DIRECTIVE: return createLivenessDirective();
			case TransformPackage.TILING_DIRECTIVE: return createTilingDirective();
			case TransformPackage.DATA_LAYOUT_DIRECTIVE: return createDataLayoutDirective();
			case TransformPackage.SCHEDULE_DIRECTIVE: return createScheduleDirective();
			case TransformPackage.CONTRACT_ARRAY_DIRECTIVE: return createContractArrayDirective();
			case TransformPackage.CLOOG_OPTIONS: return createCloogOptions();
			case TransformPackage.PURE_FUNCTION_PRAGMA: return createPureFunctionPragma();
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA: return createScheduleContextPragma();
			case TransformPackage.UNROLL_PRAGMA: return createUnrollPragma();
			case TransformPackage.INLINE_PRAGMA: return createInlinePragma();
			case TransformPackage.DATA_MOTION_PRAGMA: return createDataMotionPragma();
			case TransformPackage.DUPLICATE_NODE_PRAGMA: return createDuplicateNodePragma();
			case TransformPackage.SLICE_PRAGMA: return createSlicePragma();
			case TransformPackage.MERGE_ARRAYS_PRAGMA: return createMergeArraysPragma();
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION: return createS2S4HLSPragmaAnnotation();
			case TransformPackage.ANNOTATED_PROC_SET: return createAnnotatedProcSet();
			case TransformPackage.AFFINE_TERM_HACK: return createAffineTermHack();
			case TransformPackage.SCHEDULE_VECTOR: return createScheduleVector();
			case TransformPackage.SCHEDULE_STATEMENT_PRAGMA: return createScheduleStatementPragma();
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA: return createScheduleBlockPragma();
			case TransformPackage.OMP_PRAGMA: return createOmpPragma();
			case TransformPackage.IGNORE_MEMORY_DEPENDENCY: return createIgnoreMemoryDependency();
			case TransformPackage.LOOP_PIPELINE_PRAGMA: return createLoopPipelinePragma();
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA: return createAlmaCoarseGrainPragma();
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA: return createAlmaFineGrainPragma();
			case TransformPackage.RLT_INSET_PRAGMA: return createRLTInsetPragma();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TransformPackage.DEP_TYPE:
				return createDepTypeFromString(eDataType, initialValue);
			case TransformPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case TransformPackage.INT:
				return createintFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TransformPackage.DEP_TYPE:
				return convertDepTypeToString(eDataType, instanceValue);
			case TransformPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case TransformPackage.INT:
				return convertintToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopTransform createScopTransform() {
		ScopTransformImpl scopTransform = new ScopTransformImpl();
		return scopTransform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopTransformDirective createScopTransformDirective() {
		ScopTransformDirectiveImpl scopTransformDirective = new ScopTransformDirectiveImpl();
		return scopTransformDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddContextDirective createAddContextDirective() {
		AddContextDirectiveImpl addContextDirective = new AddContextDirectiveImpl();
		return addContextDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BubbleInsertionDirective createBubbleInsertionDirective() {
		BubbleInsertionDirectiveImpl bubbleInsertionDirective = new BubbleInsertionDirectiveImpl();
		return bubbleInsertionDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LivenessDirective createLivenessDirective() {
		LivenessDirectiveImpl livenessDirective = new LivenessDirectiveImpl();
		return livenessDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TilingDirective createTilingDirective() {
		TilingDirectiveImpl tilingDirective = new TilingDirectiveImpl();
		return tilingDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataLayoutDirective createDataLayoutDirective() {
		DataLayoutDirectiveImpl dataLayoutDirective = new DataLayoutDirectiveImpl();
		return dataLayoutDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleDirective createScheduleDirective() {
		ScheduleDirectiveImpl scheduleDirective = new ScheduleDirectiveImpl();
		return scheduleDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContractArrayDirective createContractArrayDirective() {
		ContractArrayDirectiveImpl contractArrayDirective = new ContractArrayDirectiveImpl();
		return contractArrayDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CloogOptions createCloogOptions() {
		CloogOptionsImpl cloogOptions = new CloogOptionsImpl();
		return cloogOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PureFunctionPragma createPureFunctionPragma() {
		PureFunctionPragmaImpl pureFunctionPragma = new PureFunctionPragmaImpl();
		return pureFunctionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleContextPragma createScheduleContextPragma() {
		ScheduleContextPragmaImpl scheduleContextPragma = new ScheduleContextPragmaImpl();
		return scheduleContextPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnrollPragma createUnrollPragma() {
		UnrollPragmaImpl unrollPragma = new UnrollPragmaImpl();
		return unrollPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma createInlinePragma() {
		InlinePragmaImpl inlinePragma = new InlinePragmaImpl();
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMotionPragma createDataMotionPragma() {
		DataMotionPragmaImpl dataMotionPragma = new DataMotionPragmaImpl();
		return dataMotionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DuplicateNodePragma createDuplicateNodePragma() {
		DuplicateNodePragmaImpl duplicateNodePragma = new DuplicateNodePragmaImpl();
		return duplicateNodePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SlicePragma createSlicePragma() {
		SlicePragmaImpl slicePragma = new SlicePragmaImpl();
		return slicePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeArraysPragma createMergeArraysPragma() {
		MergeArraysPragmaImpl mergeArraysPragma = new MergeArraysPragmaImpl();
		return mergeArraysPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public S2S4HLSPragmaAnnotation createS2S4HLSPragmaAnnotation() {
		S2S4HLSPragmaAnnotationImpl s2S4HLSPragmaAnnotation = new S2S4HLSPragmaAnnotationImpl();
		return s2S4HLSPragmaAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedProcSet createAnnotatedProcSet() {
		AnnotatedProcSetImpl annotatedProcSet = new AnnotatedProcSetImpl();
		return annotatedProcSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffineTermHack createAffineTermHack() {
		AffineTermHackImpl affineTermHack = new AffineTermHackImpl();
		return affineTermHack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleVector createScheduleVector() {
		ScheduleVectorImpl scheduleVector = new ScheduleVectorImpl();
		return scheduleVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleStatementPragma createScheduleStatementPragma() {
		ScheduleStatementPragmaImpl scheduleStatementPragma = new ScheduleStatementPragmaImpl();
		return scheduleStatementPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleBlockPragma createScheduleBlockPragma() {
		ScheduleBlockPragmaImpl scheduleBlockPragma = new ScheduleBlockPragmaImpl();
		return scheduleBlockPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OmpPragma createOmpPragma() {
		OmpPragmaImpl ompPragma = new OmpPragmaImpl();
		return ompPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IgnoreMemoryDependency createIgnoreMemoryDependency() {
		IgnoreMemoryDependencyImpl ignoreMemoryDependency = new IgnoreMemoryDependencyImpl();
		return ignoreMemoryDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopPipelinePragma createLoopPipelinePragma() {
		LoopPipelinePragmaImpl loopPipelinePragma = new LoopPipelinePragmaImpl();
		return loopPipelinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlmaCoarseGrainPragma createAlmaCoarseGrainPragma() {
		AlmaCoarseGrainPragmaImpl almaCoarseGrainPragma = new AlmaCoarseGrainPragmaImpl();
		return almaCoarseGrainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlmaFineGrainPragma createAlmaFineGrainPragma() {
		AlmaFineGrainPragmaImpl almaFineGrainPragma = new AlmaFineGrainPragmaImpl();
		return almaFineGrainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RLTInsetPragma createRLTInsetPragma() {
		RLTInsetPragmaImpl rltInsetPragma = new RLTInsetPragmaImpl();
		return rltInsetPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DepType createDepTypeFromString(EDataType eDataType, String initialValue) {
		DepType result = DepType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDepTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformPackage getTransformPackage() {
		return (TransformPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TransformPackage getPackage() {
		return TransformPackage.eINSTANCE;
	}

} //TransformFactoryImpl
