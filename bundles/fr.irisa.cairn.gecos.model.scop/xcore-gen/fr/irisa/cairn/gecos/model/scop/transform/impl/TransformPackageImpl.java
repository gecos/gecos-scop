/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.impl;

import fr.irisa.cairn.gecos.model.scop.ScopPackage;

import fr.irisa.cairn.gecos.model.scop.impl.ScopPackageImpl;

import fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective;
import fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack;
import fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma;
import fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet;
import fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective;
import fr.irisa.cairn.gecos.model.scop.transform.CloogOptions;
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.DepType;
import fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma;
import fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency;
import fr.irisa.cairn.gecos.model.scop.transform.InlinePragma;
import fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective;
import fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma;
import fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma;
import fr.irisa.cairn.gecos.model.scop.transform.OmpPragma;
import fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma;
import fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma;
import fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import fr.irisa.cairn.gecos.model.scop.transform.SlicePragma;
import fr.irisa.cairn.gecos.model.scop.transform.TilingDirective;
import fr.irisa.cairn.gecos.model.scop.transform.TransformFactory;
import fr.irisa.cairn.gecos.model.scop.transform.TransformPackage;

import fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.affine.AffinePackage;
import org.polymodel.algebra.polynomials.PolynomialsPackage;
import org.polymodel.algebra.quasiAffine.QuasiAffinePackage;
import org.polymodel.algebra.reductions.ReductionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TransformPackageImpl extends EPackageImpl implements TransformPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopTransformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopTransformDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addContextDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bubbleInsertionDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass livenessDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tilingDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataLayoutDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractArrayDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cloogOptionsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pureFunctionPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleContextPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unrollPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inlinePragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataMotionPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass duplicateNodePragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass slicePragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mergeArraysPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass s2S4HLSPragmaAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedProcSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass affineTermHackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleVectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleStatementPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleBlockPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ompPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ignoreMemoryDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loopPipelinePragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass almaCoarseGrainPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass almaFineGrainPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rltInsetPragmaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum depTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TransformPackageImpl() {
		super(eNS_URI, TransformFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TransformPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TransformPackage init() {
		if (isInited) return (TransformPackage)EPackage.Registry.INSTANCE.getEPackage(TransformPackage.eNS_URI);

		// Obtain or create and register package
		TransformPackageImpl theTransformPackage = (TransformPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TransformPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TransformPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AlgebraPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		AffinePackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		QuasiAffinePackage.eINSTANCE.eClass();
		PolynomialsPackage.eINSTANCE.eClass();
		ReductionsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ScopPackageImpl theScopPackage = (ScopPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScopPackage.eNS_URI) instanceof ScopPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScopPackage.eNS_URI) : ScopPackage.eINSTANCE);

		// Create package meta-data objects
		theTransformPackage.createPackageContents();
		theScopPackage.createPackageContents();

		// Initialize created meta-data
		theTransformPackage.initializePackageContents();
		theScopPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTransformPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TransformPackage.eNS_URI, theTransformPackage);
		return theTransformPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopTransform() {
		return scopTransformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopTransform_Commands() {
		return (EReference)scopTransformEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopTransformDirective() {
		return scopTransformDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddContextDirective() {
		return addContextDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddContextDirective_Params() {
		return (EReference)addContextDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddContextDirective_Context() {
		return (EReference)addContextDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBubbleInsertionDirective() {
		return bubbleInsertionDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBubbleInsertionDirective_BubbleSet() {
		return (EAttribute)bubbleInsertionDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBubbleInsertionDirective_Depth() {
		return (EAttribute)bubbleInsertionDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBubbleInsertionDirective_Latency() {
		return (EAttribute)bubbleInsertionDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBubbleInsertionDirective_CorrectionAlgorithm() {
		return (EAttribute)bubbleInsertionDirectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLivenessDirective() {
		return livenessDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLivenessDirective_Kill() {
		return (EAttribute)livenessDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLivenessDirective_LiveIn() {
		return (EAttribute)livenessDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLivenessDirective_LiveOut() {
		return (EAttribute)livenessDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLivenessDirective_Symbol() {
		return (EReference)livenessDirectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTilingDirective() {
		return tilingDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTilingDirective_Nested() {
		return (EReference)tilingDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTilingDirective_Sizes() {
		return (EAttribute)tilingDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataLayoutDirective() {
		return dataLayoutDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLayoutDirective_Addresses() {
		return (EReference)dataLayoutDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLayoutDirective_Vars() {
		return (EReference)dataLayoutDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLayoutDirective_Symbol() {
		return (EReference)dataLayoutDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleDirective() {
		return scheduleDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleDirective_Schedule() {
		return (EAttribute)scheduleDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleDirective_Statement() {
		return (EReference)scheduleDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContractArrayDirective() {
		return contractArrayDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractArrayDirective_Symbol() {
		return (EReference)contractArrayDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCloogOptions() {
		return cloogOptionsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_EqualitySpreading() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_Stop() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_FirstDepthToOptimize() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_FirstDepthSpreading() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_Strides() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_Block() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_LastDepthToOptimize() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_ConstantSpreading() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_OnceTimeLoopElim() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloogOptions_CoalescingDepth() {
		return (EAttribute)cloogOptionsEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPureFunctionPragma() {
		return pureFunctionPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleContextPragma() {
		return scheduleContextPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleContextPragma_Constraints() {
		return (EReference)scheduleContextPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleContextPragma_Name() {
		return (EAttribute)scheduleContextPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleContextPragma_Scheduling() {
		return (EAttribute)scheduleContextPragmaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnrollPragma() {
		return unrollPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnrollPragma_Factor() {
		return (EAttribute)unrollPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInlinePragma() {
		return inlinePragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInlinePragma_Proc() {
		return (EReference)inlinePragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataMotionPragma() {
		return dataMotionPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataMotionPragma_Symbols() {
		return (EReference)dataMotionPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataMotionPragma_ReuseDims() {
		return (EAttribute)dataMotionPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDuplicateNodePragma() {
		return duplicateNodePragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDuplicateNodePragma_Symbol() {
		return (EReference)duplicateNodePragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuplicateNodePragma_Factor() {
		return (EAttribute)duplicateNodePragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSlicePragma() {
		return slicePragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSlicePragma_Sizes() {
		return (EAttribute)slicePragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSlicePragma_Unrolls() {
		return (EAttribute)slicePragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMergeArraysPragma() {
		return mergeArraysPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeArraysPragma_Symbols() {
		return (EReference)mergeArraysPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getS2S4HLSPragmaAnnotation() {
		return s2S4HLSPragmaAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getS2S4HLSPragmaAnnotation_Directives() {
		return (EReference)s2S4HLSPragmaAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getS2S4HLSPragmaAnnotation_Target() {
		return (EReference)s2S4HLSPragmaAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getS2S4HLSPragmaAnnotation_OriginalPragma() {
		return (EReference)s2S4HLSPragmaAnnotationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatedProcSet() {
		return annotatedProcSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatedProcSet_Procset() {
		return (EReference)annotatedProcSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatedProcSet_Pragmas() {
		return (EReference)annotatedProcSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAffineTermHack() {
		return affineTermHackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleVector() {
		return scheduleVectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleVector_Expr() {
		return (EReference)scheduleVectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleStatementPragma() {
		return scheduleStatementPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleStatementPragma_Statement() {
		return (EReference)scheduleStatementPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleStatementPragma_Exprs() {
		return (EReference)scheduleStatementPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleBlockPragma() {
		return scheduleBlockPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleBlockPragma_Statements() {
		return (EReference)scheduleBlockPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleBlockPragma_Exprs() {
		return (EReference)scheduleBlockPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOmpPragma() {
		return ompPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOmpPragma_ParallelFor() {
		return (EAttribute)ompPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOmpPragma_Privates() {
		return (EReference)ompPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOmpPragma_Shared() {
		return (EReference)ompPragmaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOmpPragma_Reductions() {
		return (EReference)ompPragmaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIgnoreMemoryDependency() {
		return ignoreMemoryDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIgnoreMemoryDependency_Symbols() {
		return (EReference)ignoreMemoryDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIgnoreMemoryDependency_From() {
		return (EReference)ignoreMemoryDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIgnoreMemoryDependency_To() {
		return (EReference)ignoreMemoryDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIgnoreMemoryDependency_Type() {
		return (EAttribute)ignoreMemoryDependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoopPipelinePragma() {
		return loopPipelinePragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopPipelinePragma_Latency() {
		return (EAttribute)loopPipelinePragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopPipelinePragma_HashFunction() {
		return (EAttribute)loopPipelinePragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlmaCoarseGrainPragma() {
		return almaCoarseGrainPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlmaCoarseGrainPragma_NbTasks() {
		return (EAttribute)almaCoarseGrainPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlmaCoarseGrainPragma_TaskIds() {
		return (EAttribute)almaCoarseGrainPragmaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlmaCoarseGrainPragma_TileSizes() {
		return (EAttribute)almaCoarseGrainPragmaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlmaFineGrainPragma() {
		return almaFineGrainPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlmaFineGrainPragma_TileSizes() {
		return (EAttribute)almaFineGrainPragmaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRLTInsetPragma() {
		return rltInsetPragmaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDepType() {
		return depTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformFactory getTransformFactory() {
		return (TransformFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scopTransformEClass = createEClass(SCOP_TRANSFORM);
		createEReference(scopTransformEClass, SCOP_TRANSFORM__COMMANDS);

		scopTransformDirectiveEClass = createEClass(SCOP_TRANSFORM_DIRECTIVE);

		addContextDirectiveEClass = createEClass(ADD_CONTEXT_DIRECTIVE);
		createEReference(addContextDirectiveEClass, ADD_CONTEXT_DIRECTIVE__PARAMS);
		createEReference(addContextDirectiveEClass, ADD_CONTEXT_DIRECTIVE__CONTEXT);

		bubbleInsertionDirectiveEClass = createEClass(BUBBLE_INSERTION_DIRECTIVE);
		createEAttribute(bubbleInsertionDirectiveEClass, BUBBLE_INSERTION_DIRECTIVE__BUBBLE_SET);
		createEAttribute(bubbleInsertionDirectiveEClass, BUBBLE_INSERTION_DIRECTIVE__DEPTH);
		createEAttribute(bubbleInsertionDirectiveEClass, BUBBLE_INSERTION_DIRECTIVE__LATENCY);
		createEAttribute(bubbleInsertionDirectiveEClass, BUBBLE_INSERTION_DIRECTIVE__CORRECTION_ALGORITHM);

		livenessDirectiveEClass = createEClass(LIVENESS_DIRECTIVE);
		createEAttribute(livenessDirectiveEClass, LIVENESS_DIRECTIVE__KILL);
		createEAttribute(livenessDirectiveEClass, LIVENESS_DIRECTIVE__LIVE_IN);
		createEAttribute(livenessDirectiveEClass, LIVENESS_DIRECTIVE__LIVE_OUT);
		createEReference(livenessDirectiveEClass, LIVENESS_DIRECTIVE__SYMBOL);

		tilingDirectiveEClass = createEClass(TILING_DIRECTIVE);
		createEReference(tilingDirectiveEClass, TILING_DIRECTIVE__NESTED);
		createEAttribute(tilingDirectiveEClass, TILING_DIRECTIVE__SIZES);

		dataLayoutDirectiveEClass = createEClass(DATA_LAYOUT_DIRECTIVE);
		createEReference(dataLayoutDirectiveEClass, DATA_LAYOUT_DIRECTIVE__ADDRESSES);
		createEReference(dataLayoutDirectiveEClass, DATA_LAYOUT_DIRECTIVE__VARS);
		createEReference(dataLayoutDirectiveEClass, DATA_LAYOUT_DIRECTIVE__SYMBOL);

		scheduleDirectiveEClass = createEClass(SCHEDULE_DIRECTIVE);
		createEAttribute(scheduleDirectiveEClass, SCHEDULE_DIRECTIVE__SCHEDULE);
		createEReference(scheduleDirectiveEClass, SCHEDULE_DIRECTIVE__STATEMENT);

		contractArrayDirectiveEClass = createEClass(CONTRACT_ARRAY_DIRECTIVE);
		createEReference(contractArrayDirectiveEClass, CONTRACT_ARRAY_DIRECTIVE__SYMBOL);

		cloogOptionsEClass = createEClass(CLOOG_OPTIONS);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__EQUALITY_SPREADING);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__STOP);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__FIRST_DEPTH_TO_OPTIMIZE);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__FIRST_DEPTH_SPREADING);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__STRIDES);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__BLOCK);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__LAST_DEPTH_TO_OPTIMIZE);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__CONSTANT_SPREADING);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__ONCE_TIME_LOOP_ELIM);
		createEAttribute(cloogOptionsEClass, CLOOG_OPTIONS__COALESCING_DEPTH);

		pureFunctionPragmaEClass = createEClass(PURE_FUNCTION_PRAGMA);

		scheduleContextPragmaEClass = createEClass(SCHEDULE_CONTEXT_PRAGMA);
		createEReference(scheduleContextPragmaEClass, SCHEDULE_CONTEXT_PRAGMA__CONSTRAINTS);
		createEAttribute(scheduleContextPragmaEClass, SCHEDULE_CONTEXT_PRAGMA__NAME);
		createEAttribute(scheduleContextPragmaEClass, SCHEDULE_CONTEXT_PRAGMA__SCHEDULING);

		unrollPragmaEClass = createEClass(UNROLL_PRAGMA);
		createEAttribute(unrollPragmaEClass, UNROLL_PRAGMA__FACTOR);

		inlinePragmaEClass = createEClass(INLINE_PRAGMA);
		createEReference(inlinePragmaEClass, INLINE_PRAGMA__PROC);

		dataMotionPragmaEClass = createEClass(DATA_MOTION_PRAGMA);
		createEReference(dataMotionPragmaEClass, DATA_MOTION_PRAGMA__SYMBOLS);
		createEAttribute(dataMotionPragmaEClass, DATA_MOTION_PRAGMA__REUSE_DIMS);

		duplicateNodePragmaEClass = createEClass(DUPLICATE_NODE_PRAGMA);
		createEReference(duplicateNodePragmaEClass, DUPLICATE_NODE_PRAGMA__SYMBOL);
		createEAttribute(duplicateNodePragmaEClass, DUPLICATE_NODE_PRAGMA__FACTOR);

		slicePragmaEClass = createEClass(SLICE_PRAGMA);
		createEAttribute(slicePragmaEClass, SLICE_PRAGMA__SIZES);
		createEAttribute(slicePragmaEClass, SLICE_PRAGMA__UNROLLS);

		mergeArraysPragmaEClass = createEClass(MERGE_ARRAYS_PRAGMA);
		createEReference(mergeArraysPragmaEClass, MERGE_ARRAYS_PRAGMA__SYMBOLS);

		s2S4HLSPragmaAnnotationEClass = createEClass(S2S4HLS_PRAGMA_ANNOTATION);
		createEReference(s2S4HLSPragmaAnnotationEClass, S2S4HLS_PRAGMA_ANNOTATION__DIRECTIVES);
		createEReference(s2S4HLSPragmaAnnotationEClass, S2S4HLS_PRAGMA_ANNOTATION__TARGET);
		createEReference(s2S4HLSPragmaAnnotationEClass, S2S4HLS_PRAGMA_ANNOTATION__ORIGINAL_PRAGMA);

		annotatedProcSetEClass = createEClass(ANNOTATED_PROC_SET);
		createEReference(annotatedProcSetEClass, ANNOTATED_PROC_SET__PROCSET);
		createEReference(annotatedProcSetEClass, ANNOTATED_PROC_SET__PRAGMAS);

		affineTermHackEClass = createEClass(AFFINE_TERM_HACK);

		scheduleVectorEClass = createEClass(SCHEDULE_VECTOR);
		createEReference(scheduleVectorEClass, SCHEDULE_VECTOR__EXPR);

		scheduleStatementPragmaEClass = createEClass(SCHEDULE_STATEMENT_PRAGMA);
		createEReference(scheduleStatementPragmaEClass, SCHEDULE_STATEMENT_PRAGMA__STATEMENT);
		createEReference(scheduleStatementPragmaEClass, SCHEDULE_STATEMENT_PRAGMA__EXPRS);

		scheduleBlockPragmaEClass = createEClass(SCHEDULE_BLOCK_PRAGMA);
		createEReference(scheduleBlockPragmaEClass, SCHEDULE_BLOCK_PRAGMA__STATEMENTS);
		createEReference(scheduleBlockPragmaEClass, SCHEDULE_BLOCK_PRAGMA__EXPRS);

		ompPragmaEClass = createEClass(OMP_PRAGMA);
		createEAttribute(ompPragmaEClass, OMP_PRAGMA__PARALLEL_FOR);
		createEReference(ompPragmaEClass, OMP_PRAGMA__PRIVATES);
		createEReference(ompPragmaEClass, OMP_PRAGMA__SHARED);
		createEReference(ompPragmaEClass, OMP_PRAGMA__REDUCTIONS);

		ignoreMemoryDependencyEClass = createEClass(IGNORE_MEMORY_DEPENDENCY);
		createEReference(ignoreMemoryDependencyEClass, IGNORE_MEMORY_DEPENDENCY__SYMBOLS);
		createEReference(ignoreMemoryDependencyEClass, IGNORE_MEMORY_DEPENDENCY__FROM);
		createEReference(ignoreMemoryDependencyEClass, IGNORE_MEMORY_DEPENDENCY__TO);
		createEAttribute(ignoreMemoryDependencyEClass, IGNORE_MEMORY_DEPENDENCY__TYPE);

		loopPipelinePragmaEClass = createEClass(LOOP_PIPELINE_PRAGMA);
		createEAttribute(loopPipelinePragmaEClass, LOOP_PIPELINE_PRAGMA__LATENCY);
		createEAttribute(loopPipelinePragmaEClass, LOOP_PIPELINE_PRAGMA__HASH_FUNCTION);

		almaCoarseGrainPragmaEClass = createEClass(ALMA_COARSE_GRAIN_PRAGMA);
		createEAttribute(almaCoarseGrainPragmaEClass, ALMA_COARSE_GRAIN_PRAGMA__NB_TASKS);
		createEAttribute(almaCoarseGrainPragmaEClass, ALMA_COARSE_GRAIN_PRAGMA__TASK_IDS);
		createEAttribute(almaCoarseGrainPragmaEClass, ALMA_COARSE_GRAIN_PRAGMA__TILE_SIZES);

		almaFineGrainPragmaEClass = createEClass(ALMA_FINE_GRAIN_PRAGMA);
		createEAttribute(almaFineGrainPragmaEClass, ALMA_FINE_GRAIN_PRAGMA__TILE_SIZES);

		rltInsetPragmaEClass = createEClass(RLT_INSET_PRAGMA);

		// Create enums
		depTypeEEnum = createEEnum(DEP_TYPE);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ScopPackage theScopPackage = (ScopPackage)EPackage.Registry.INSTANCE.getEPackage(ScopPackage.eNS_URI);
		AlgebraPackage theAlgebraPackage = (AlgebraPackage)EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		AffinePackage theAffinePackage = (AffinePackage)EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dataLayoutDirectiveEClass.getESuperTypes().add(this.getScopTransformDirective());
		scheduleDirectiveEClass.getESuperTypes().add(this.getScopTransformDirective());
		contractArrayDirectiveEClass.getESuperTypes().add(this.getScopTransformDirective());
		cloogOptionsEClass.getESuperTypes().add(this.getScopTransformDirective());
		pureFunctionPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		scheduleContextPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		unrollPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		inlinePragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		dataMotionPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		duplicateNodePragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		slicePragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		mergeArraysPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		s2S4HLSPragmaAnnotationEClass.getESuperTypes().add(theAnnotationsPackage.getIAnnotation());
		affineTermHackEClass.getESuperTypes().add(theAffinePackage.getAffineTerm());
		scheduleStatementPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		scheduleBlockPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		ompPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		ignoreMemoryDependencyEClass.getESuperTypes().add(this.getScopTransformDirective());
		loopPipelinePragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		almaCoarseGrainPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		almaFineGrainPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());
		rltInsetPragmaEClass.getESuperTypes().add(this.getScopTransformDirective());

		// Initialize classes and features; add operations and parameters
		initEClass(scopTransformEClass, ScopTransform.class, "ScopTransform", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopTransform_Commands(), this.getScopTransformDirective(), null, "commands", null, 0, -1, ScopTransform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(scopTransformEClass, this.getScheduleDirective(), "listAllScheduleDirectives", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopTransformEClass, this.getDataLayoutDirective(), "listAllLayoutDirectives", 0, -1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(scopTransformEClass, this.getScheduleDirective(), "getScheduleFor", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theScopPackage.getScopStatement(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopTransformDirectiveEClass, ScopTransformDirective.class, "ScopTransformDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(scopTransformDirectiveEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(addContextDirectiveEClass, AddContextDirective.class, "AddContextDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAddContextDirective_Params(), theScopPackage.getScopDimension(), null, "params", null, 0, 1, AddContextDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAddContextDirective_Context(), theAlgebraPackage.getIntConstraintSystem(), null, "context", null, 0, 1, AddContextDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bubbleInsertionDirectiveEClass, BubbleInsertionDirective.class, "BubbleInsertionDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBubbleInsertionDirective_BubbleSet(), this.getString(), "bubbleSet", null, 0, 1, BubbleInsertionDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBubbleInsertionDirective_Depth(), this.getint(), "depth", null, 0, 1, BubbleInsertionDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBubbleInsertionDirective_Latency(), this.getint(), "latency", null, 0, 1, BubbleInsertionDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBubbleInsertionDirective_CorrectionAlgorithm(), this.getString(), "correctionAlgorithm", null, 0, 1, BubbleInsertionDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(livenessDirectiveEClass, LivenessDirective.class, "LivenessDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLivenessDirective_Kill(), theEcorePackage.getEBoolean(), "kill", null, 0, 1, LivenessDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLivenessDirective_LiveIn(), theEcorePackage.getEBoolean(), "liveIn", null, 0, 1, LivenessDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLivenessDirective_LiveOut(), theEcorePackage.getEBoolean(), "liveOut", null, 0, 1, LivenessDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLivenessDirective_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, LivenessDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tilingDirectiveEClass, TilingDirective.class, "TilingDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTilingDirective_Nested(), this.getTilingDirective(), null, "nested", null, 0, 1, TilingDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTilingDirective_Sizes(), this.getint(), "sizes", null, 0, -1, TilingDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataLayoutDirectiveEClass, DataLayoutDirective.class, "DataLayoutDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataLayoutDirective_Addresses(), theAlgebraPackage.getIntExpression(), null, "addresses", null, 0, -1, DataLayoutDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataLayoutDirective_Vars(), theScopPackage.getScopDimension(), null, "vars", null, 0, -1, DataLayoutDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataLayoutDirective_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, DataLayoutDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleDirectiveEClass, ScheduleDirective.class, "ScheduleDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScheduleDirective_Schedule(), this.getString(), "schedule", null, 0, 1, ScheduleDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduleDirective_Statement(), theScopPackage.getScopStatement(), null, "statement", null, 0, 1, ScheduleDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contractArrayDirectiveEClass, ContractArrayDirective.class, "ContractArrayDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContractArrayDirective_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, ContractArrayDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cloogOptionsEClass, CloogOptions.class, "CloogOptions", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCloogOptions_EqualitySpreading(), this.getint(), "equalitySpreading", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_Stop(), this.getint(), "stop", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_FirstDepthToOptimize(), this.getint(), "firstDepthToOptimize", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_FirstDepthSpreading(), this.getint(), "firstDepthSpreading", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_Strides(), this.getint(), "strides", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_Block(), this.getint(), "block", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_LastDepthToOptimize(), this.getint(), "lastDepthToOptimize", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_ConstantSpreading(), this.getint(), "constantSpreading", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_OnceTimeLoopElim(), this.getint(), "onceTimeLoopElim", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloogOptions_CoalescingDepth(), this.getint(), "coalescingDepth", null, 0, 1, CloogOptions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pureFunctionPragmaEClass, PureFunctionPragma.class, "PureFunctionPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scheduleContextPragmaEClass, ScheduleContextPragma.class, "ScheduleContextPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScheduleContextPragma_Constraints(), theAlgebraPackage.getIntConstraint(), null, "constraints", null, 0, -1, ScheduleContextPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleContextPragma_Name(), this.getString(), "name", null, 0, 1, ScheduleContextPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleContextPragma_Scheduling(), this.getString(), "scheduling", null, 0, 1, ScheduleContextPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unrollPragmaEClass, UnrollPragma.class, "UnrollPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnrollPragma_Factor(), this.getint(), "factor", null, 0, 1, UnrollPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inlinePragmaEClass, InlinePragma.class, "InlinePragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInlinePragma_Proc(), theCorePackage.getProcedureSymbol(), null, "proc", null, 0, 1, InlinePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataMotionPragmaEClass, DataMotionPragma.class, "DataMotionPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataMotionPragma_Symbols(), theCorePackage.getSymbol(), null, "symbols", null, 0, -1, DataMotionPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataMotionPragma_ReuseDims(), this.getint(), "reuseDims", null, 0, 1, DataMotionPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(duplicateNodePragmaEClass, DuplicateNodePragma.class, "DuplicateNodePragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDuplicateNodePragma_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, DuplicateNodePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuplicateNodePragma_Factor(), this.getint(), "factor", null, 0, 1, DuplicateNodePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(slicePragmaEClass, SlicePragma.class, "SlicePragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSlicePragma_Sizes(), this.getint(), "sizes", null, 0, -1, SlicePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSlicePragma_Unrolls(), this.getint(), "unrolls", null, 0, -1, SlicePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mergeArraysPragmaEClass, MergeArraysPragma.class, "MergeArraysPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMergeArraysPragma_Symbols(), theCorePackage.getSymbol(), null, "symbols", null, 0, -1, MergeArraysPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(s2S4HLSPragmaAnnotationEClass, S2S4HLSPragmaAnnotation.class, "S2S4HLSPragmaAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getS2S4HLSPragmaAnnotation_Directives(), this.getScopTransformDirective(), null, "directives", null, 0, -1, S2S4HLSPragmaAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getS2S4HLSPragmaAnnotation_Target(), theAnnotationsPackage.getAnnotatedElement(), null, "target", null, 0, 1, S2S4HLSPragmaAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getS2S4HLSPragmaAnnotation_OriginalPragma(), theAnnotationsPackage.getPragmaAnnotation(), null, "originalPragma", null, 0, 1, S2S4HLSPragmaAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotatedProcSetEClass, AnnotatedProcSet.class, "AnnotatedProcSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotatedProcSet_Procset(), theCorePackage.getProcedureSet(), null, "procset", null, 0, 1, AnnotatedProcSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotatedProcSet_Pragmas(), this.getS2S4HLSPragmaAnnotation(), null, "pragmas", null, 0, -1, AnnotatedProcSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(affineTermHackEClass, AffineTermHack.class, "AffineTermHack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scheduleVectorEClass, ScheduleVector.class, "ScheduleVector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScheduleVector_Expr(), theAlgebraPackage.getIntExpression(), null, "expr", null, 0, 1, ScheduleVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleStatementPragmaEClass, ScheduleStatementPragma.class, "ScheduleStatementPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScheduleStatementPragma_Statement(), theScopPackage.getScopStatement(), null, "statement", null, 0, 1, ScheduleStatementPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduleStatementPragma_Exprs(), theAlgebraPackage.getIntExpression(), null, "exprs", null, 0, -1, ScheduleStatementPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleBlockPragmaEClass, ScheduleBlockPragma.class, "ScheduleBlockPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScheduleBlockPragma_Statements(), theScopPackage.getScopStatement(), null, "statements", null, 0, -1, ScheduleBlockPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduleBlockPragma_Exprs(), theAlgebraPackage.getIntExpression(), null, "exprs", null, 0, -1, ScheduleBlockPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ompPragmaEClass, OmpPragma.class, "OmpPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOmpPragma_ParallelFor(), theEcorePackage.getEBoolean(), "parallelFor", null, 0, 1, OmpPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOmpPragma_Privates(), theCorePackage.getSymbol(), null, "privates", null, 0, -1, OmpPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOmpPragma_Shared(), theCorePackage.getSymbol(), null, "shared", null, 0, -1, OmpPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOmpPragma_Reductions(), theCorePackage.getSymbol(), null, "reductions", null, 0, -1, OmpPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ignoreMemoryDependencyEClass, IgnoreMemoryDependency.class, "IgnoreMemoryDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIgnoreMemoryDependency_Symbols(), theCorePackage.getSymbol(), null, "symbols", null, 0, -1, IgnoreMemoryDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIgnoreMemoryDependency_From(), theInstrsPackage.getInstruction(), null, "from", null, 0, 1, IgnoreMemoryDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIgnoreMemoryDependency_To(), theInstrsPackage.getInstruction(), null, "to", null, 0, 1, IgnoreMemoryDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIgnoreMemoryDependency_Type(), this.getDepType(), "type", null, 0, 1, IgnoreMemoryDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loopPipelinePragmaEClass, LoopPipelinePragma.class, "LoopPipelinePragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLoopPipelinePragma_Latency(), this.getint(), "latency", null, 0, 1, LoopPipelinePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoopPipelinePragma_HashFunction(), this.getString(), "hashFunction", null, 0, 1, LoopPipelinePragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(almaCoarseGrainPragmaEClass, AlmaCoarseGrainPragma.class, "AlmaCoarseGrainPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAlmaCoarseGrainPragma_NbTasks(), this.getint(), "nbTasks", "-1", 0, 1, AlmaCoarseGrainPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAlmaCoarseGrainPragma_TaskIds(), this.getint(), "taskIds", null, 0, -1, AlmaCoarseGrainPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAlmaCoarseGrainPragma_TileSizes(), this.getint(), "tileSizes", null, 0, -1, AlmaCoarseGrainPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(almaFineGrainPragmaEClass, AlmaFineGrainPragma.class, "AlmaFineGrainPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAlmaFineGrainPragma_TileSizes(), this.getint(), "tileSizes", null, 0, -1, AlmaFineGrainPragma.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rltInsetPragmaEClass, RLTInsetPragma.class, "RLTInsetPragma", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(depTypeEEnum, DepType.class, "DepType");
		addEEnumLiteral(depTypeEEnum, DepType.RAW);
		addEEnumLiteral(depTypeEEnum, DepType.WAW);
		addEEnumLiteral(depTypeEEnum, DepType.WAR);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //TransformPackageImpl
