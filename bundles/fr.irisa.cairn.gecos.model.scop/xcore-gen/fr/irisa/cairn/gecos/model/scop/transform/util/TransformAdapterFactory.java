/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.util;

import fr.irisa.cairn.gecos.model.scop.transform.*;
import gecos.annotations.IAnnotation;
import gecos.core.GecosNode;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.affine.AffineTerm;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage
 * @generated
 */
public class TransformAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TransformPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TransformPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformSwitch<Adapter> modelSwitch =
		new TransformSwitch<Adapter>() {
			@Override
			public Adapter caseScopTransform(ScopTransform object) {
				return createScopTransformAdapter();
			}
			@Override
			public Adapter caseScopTransformDirective(ScopTransformDirective object) {
				return createScopTransformDirectiveAdapter();
			}
			@Override
			public Adapter caseAddContextDirective(AddContextDirective object) {
				return createAddContextDirectiveAdapter();
			}
			@Override
			public Adapter caseBubbleInsertionDirective(BubbleInsertionDirective object) {
				return createBubbleInsertionDirectiveAdapter();
			}
			@Override
			public Adapter caseLivenessDirective(LivenessDirective object) {
				return createLivenessDirectiveAdapter();
			}
			@Override
			public Adapter caseTilingDirective(TilingDirective object) {
				return createTilingDirectiveAdapter();
			}
			@Override
			public Adapter caseDataLayoutDirective(DataLayoutDirective object) {
				return createDataLayoutDirectiveAdapter();
			}
			@Override
			public Adapter caseScheduleDirective(ScheduleDirective object) {
				return createScheduleDirectiveAdapter();
			}
			@Override
			public Adapter caseContractArrayDirective(ContractArrayDirective object) {
				return createContractArrayDirectiveAdapter();
			}
			@Override
			public Adapter caseCloogOptions(CloogOptions object) {
				return createCloogOptionsAdapter();
			}
			@Override
			public Adapter casePureFunctionPragma(PureFunctionPragma object) {
				return createPureFunctionPragmaAdapter();
			}
			@Override
			public Adapter caseScheduleContextPragma(ScheduleContextPragma object) {
				return createScheduleContextPragmaAdapter();
			}
			@Override
			public Adapter caseUnrollPragma(UnrollPragma object) {
				return createUnrollPragmaAdapter();
			}
			@Override
			public Adapter caseInlinePragma(InlinePragma object) {
				return createInlinePragmaAdapter();
			}
			@Override
			public Adapter caseDataMotionPragma(DataMotionPragma object) {
				return createDataMotionPragmaAdapter();
			}
			@Override
			public Adapter caseDuplicateNodePragma(DuplicateNodePragma object) {
				return createDuplicateNodePragmaAdapter();
			}
			@Override
			public Adapter caseSlicePragma(SlicePragma object) {
				return createSlicePragmaAdapter();
			}
			@Override
			public Adapter caseMergeArraysPragma(MergeArraysPragma object) {
				return createMergeArraysPragmaAdapter();
			}
			@Override
			public Adapter caseS2S4HLSPragmaAnnotation(S2S4HLSPragmaAnnotation object) {
				return createS2S4HLSPragmaAnnotationAdapter();
			}
			@Override
			public Adapter caseAnnotatedProcSet(AnnotatedProcSet object) {
				return createAnnotatedProcSetAdapter();
			}
			@Override
			public Adapter caseAffineTermHack(AffineTermHack object) {
				return createAffineTermHackAdapter();
			}
			@Override
			public Adapter caseScheduleVector(ScheduleVector object) {
				return createScheduleVectorAdapter();
			}
			@Override
			public Adapter caseScheduleStatementPragma(ScheduleStatementPragma object) {
				return createScheduleStatementPragmaAdapter();
			}
			@Override
			public Adapter caseScheduleBlockPragma(ScheduleBlockPragma object) {
				return createScheduleBlockPragmaAdapter();
			}
			@Override
			public Adapter caseOmpPragma(OmpPragma object) {
				return createOmpPragmaAdapter();
			}
			@Override
			public Adapter caseIgnoreMemoryDependency(IgnoreMemoryDependency object) {
				return createIgnoreMemoryDependencyAdapter();
			}
			@Override
			public Adapter caseLoopPipelinePragma(LoopPipelinePragma object) {
				return createLoopPipelinePragmaAdapter();
			}
			@Override
			public Adapter caseAlmaCoarseGrainPragma(AlmaCoarseGrainPragma object) {
				return createAlmaCoarseGrainPragmaAdapter();
			}
			@Override
			public Adapter caseAlmaFineGrainPragma(AlmaFineGrainPragma object) {
				return createAlmaFineGrainPragmaAdapter();
			}
			@Override
			public Adapter caseRLTInsetPragma(RLTInsetPragma object) {
				return createRLTInsetPragmaAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseIAnnotation(IAnnotation object) {
				return createIAnnotationAdapter();
			}
			@Override
			public Adapter caseAlgebraVisitable(AlgebraVisitable object) {
				return createAlgebraVisitableAdapter();
			}
			@Override
			public Adapter caseIntTerm(IntTerm object) {
				return createIntTermAdapter();
			}
			@Override
			public Adapter caseAffineTerm(AffineTerm object) {
				return createAffineTermAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransform <em>Scop Transform</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScopTransform
	 * @generated
	 */
	public Adapter createScopTransformAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective <em>Scop Transform Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective
	 * @generated
	 */
	public Adapter createScopTransformDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective <em>Add Context Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AddContextDirective
	 * @generated
	 */
	public Adapter createAddContextDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective <em>Bubble Insertion Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.BubbleInsertionDirective
	 * @generated
	 */
	public Adapter createBubbleInsertionDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective <em>Liveness Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LivenessDirective
	 * @generated
	 */
	public Adapter createLivenessDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.TilingDirective <em>Tiling Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.TilingDirective
	 * @generated
	 */
	public Adapter createTilingDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective <em>Data Layout Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective
	 * @generated
	 */
	public Adapter createDataLayoutDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective <em>Schedule Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective
	 * @generated
	 */
	public Adapter createScheduleDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective <em>Contract Array Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective
	 * @generated
	 */
	public Adapter createContractArrayDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.CloogOptions <em>Cloog Options</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.CloogOptions
	 * @generated
	 */
	public Adapter createCloogOptionsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma <em>Pure Function Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.PureFunctionPragma
	 * @generated
	 */
	public Adapter createPureFunctionPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma <em>Schedule Context Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleContextPragma
	 * @generated
	 */
	public Adapter createScheduleContextPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma <em>Unroll Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.UnrollPragma
	 * @generated
	 */
	public Adapter createUnrollPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.InlinePragma <em>Inline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.InlinePragma
	 * @generated
	 */
	public Adapter createInlinePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma <em>Data Motion Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DataMotionPragma
	 * @generated
	 */
	public Adapter createDataMotionPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma <em>Duplicate Node Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.DuplicateNodePragma
	 * @generated
	 */
	public Adapter createDuplicateNodePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.SlicePragma <em>Slice Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.SlicePragma
	 * @generated
	 */
	public Adapter createSlicePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma <em>Merge Arrays Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.MergeArraysPragma
	 * @generated
	 */
	public Adapter createMergeArraysPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation <em>S2S4HLS Pragma Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.S2S4HLSPragmaAnnotation
	 * @generated
	 */
	public Adapter createS2S4HLSPragmaAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet <em>Annotated Proc Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AnnotatedProcSet
	 * @generated
	 */
	public Adapter createAnnotatedProcSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack <em>Affine Term Hack</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AffineTermHack
	 * @generated
	 */
	public Adapter createAffineTermHackAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector <em>Schedule Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleVector
	 * @generated
	 */
	public Adapter createScheduleVectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma <em>Schedule Statement Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleStatementPragma
	 * @generated
	 */
	public Adapter createScheduleStatementPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma <em>Schedule Block Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.ScheduleBlockPragma
	 * @generated
	 */
	public Adapter createScheduleBlockPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.OmpPragma <em>Omp Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.OmpPragma
	 * @generated
	 */
	public Adapter createOmpPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency <em>Ignore Memory Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.IgnoreMemoryDependency
	 * @generated
	 */
	public Adapter createIgnoreMemoryDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma <em>Loop Pipeline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.LoopPipelinePragma
	 * @generated
	 */
	public Adapter createLoopPipelinePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma <em>Alma Coarse Grain Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaCoarseGrainPragma
	 * @generated
	 */
	public Adapter createAlmaCoarseGrainPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma <em>Alma Fine Grain Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.AlmaFineGrainPragma
	 * @generated
	 */
	public Adapter createAlmaFineGrainPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma <em>RLT Inset Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.transform.RLTInsetPragma
	 * @generated
	 */
	public Adapter createRLTInsetPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.IAnnotation <em>IAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.IAnnotation
	 * @generated
	 */
	public Adapter createIAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polymodel.algebra.AlgebraVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.polymodel.algebra.AlgebraVisitable
	 * @generated
	 */
	public Adapter createAlgebraVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polymodel.algebra.IntTerm <em>Int Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.polymodel.algebra.IntTerm
	 * @generated
	 */
	public Adapter createIntTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polymodel.algebra.affine.AffineTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.polymodel.algebra.affine.AffineTerm
	 * @generated
	 */
	public Adapter createAffineTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TransformAdapterFactory
