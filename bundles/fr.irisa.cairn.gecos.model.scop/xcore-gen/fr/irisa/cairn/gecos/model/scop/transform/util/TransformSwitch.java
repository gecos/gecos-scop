/**
 */
package fr.irisa.cairn.gecos.model.scop.transform.util;

import fr.irisa.cairn.gecos.model.scop.transform.*;
import gecos.annotations.IAnnotation;
import gecos.core.GecosNode;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;
import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.affine.AffineTerm;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.transform.TransformPackage
 * @generated
 */
public class TransformSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TransformPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformSwitch() {
		if (modelPackage == null) {
			modelPackage = TransformPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TransformPackage.SCOP_TRANSFORM: {
				ScopTransform scopTransform = (ScopTransform)theEObject;
				T result = caseScopTransform(scopTransform);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCOP_TRANSFORM_DIRECTIVE: {
				ScopTransformDirective scopTransformDirective = (ScopTransformDirective)theEObject;
				T result = caseScopTransformDirective(scopTransformDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.ADD_CONTEXT_DIRECTIVE: {
				AddContextDirective addContextDirective = (AddContextDirective)theEObject;
				T result = caseAddContextDirective(addContextDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.BUBBLE_INSERTION_DIRECTIVE: {
				BubbleInsertionDirective bubbleInsertionDirective = (BubbleInsertionDirective)theEObject;
				T result = caseBubbleInsertionDirective(bubbleInsertionDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.LIVENESS_DIRECTIVE: {
				LivenessDirective livenessDirective = (LivenessDirective)theEObject;
				T result = caseLivenessDirective(livenessDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.TILING_DIRECTIVE: {
				TilingDirective tilingDirective = (TilingDirective)theEObject;
				T result = caseTilingDirective(tilingDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.DATA_LAYOUT_DIRECTIVE: {
				DataLayoutDirective dataLayoutDirective = (DataLayoutDirective)theEObject;
				T result = caseDataLayoutDirective(dataLayoutDirective);
				if (result == null) result = caseScopTransformDirective(dataLayoutDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCHEDULE_DIRECTIVE: {
				ScheduleDirective scheduleDirective = (ScheduleDirective)theEObject;
				T result = caseScheduleDirective(scheduleDirective);
				if (result == null) result = caseScopTransformDirective(scheduleDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.CONTRACT_ARRAY_DIRECTIVE: {
				ContractArrayDirective contractArrayDirective = (ContractArrayDirective)theEObject;
				T result = caseContractArrayDirective(contractArrayDirective);
				if (result == null) result = caseScopTransformDirective(contractArrayDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.CLOOG_OPTIONS: {
				CloogOptions cloogOptions = (CloogOptions)theEObject;
				T result = caseCloogOptions(cloogOptions);
				if (result == null) result = caseScopTransformDirective(cloogOptions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.PURE_FUNCTION_PRAGMA: {
				PureFunctionPragma pureFunctionPragma = (PureFunctionPragma)theEObject;
				T result = casePureFunctionPragma(pureFunctionPragma);
				if (result == null) result = caseScopTransformDirective(pureFunctionPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCHEDULE_CONTEXT_PRAGMA: {
				ScheduleContextPragma scheduleContextPragma = (ScheduleContextPragma)theEObject;
				T result = caseScheduleContextPragma(scheduleContextPragma);
				if (result == null) result = caseScopTransformDirective(scheduleContextPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.UNROLL_PRAGMA: {
				UnrollPragma unrollPragma = (UnrollPragma)theEObject;
				T result = caseUnrollPragma(unrollPragma);
				if (result == null) result = caseScopTransformDirective(unrollPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.INLINE_PRAGMA: {
				InlinePragma inlinePragma = (InlinePragma)theEObject;
				T result = caseInlinePragma(inlinePragma);
				if (result == null) result = caseScopTransformDirective(inlinePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.DATA_MOTION_PRAGMA: {
				DataMotionPragma dataMotionPragma = (DataMotionPragma)theEObject;
				T result = caseDataMotionPragma(dataMotionPragma);
				if (result == null) result = caseScopTransformDirective(dataMotionPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.DUPLICATE_NODE_PRAGMA: {
				DuplicateNodePragma duplicateNodePragma = (DuplicateNodePragma)theEObject;
				T result = caseDuplicateNodePragma(duplicateNodePragma);
				if (result == null) result = caseScopTransformDirective(duplicateNodePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SLICE_PRAGMA: {
				SlicePragma slicePragma = (SlicePragma)theEObject;
				T result = caseSlicePragma(slicePragma);
				if (result == null) result = caseScopTransformDirective(slicePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.MERGE_ARRAYS_PRAGMA: {
				MergeArraysPragma mergeArraysPragma = (MergeArraysPragma)theEObject;
				T result = caseMergeArraysPragma(mergeArraysPragma);
				if (result == null) result = caseScopTransformDirective(mergeArraysPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.S2S4HLS_PRAGMA_ANNOTATION: {
				S2S4HLSPragmaAnnotation s2S4HLSPragmaAnnotation = (S2S4HLSPragmaAnnotation)theEObject;
				T result = caseS2S4HLSPragmaAnnotation(s2S4HLSPragmaAnnotation);
				if (result == null) result = caseIAnnotation(s2S4HLSPragmaAnnotation);
				if (result == null) result = caseGecosNode(s2S4HLSPragmaAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.ANNOTATED_PROC_SET: {
				AnnotatedProcSet annotatedProcSet = (AnnotatedProcSet)theEObject;
				T result = caseAnnotatedProcSet(annotatedProcSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.AFFINE_TERM_HACK: {
				AffineTermHack affineTermHack = (AffineTermHack)theEObject;
				T result = caseAffineTermHack(affineTermHack);
				if (result == null) result = caseAffineTerm(affineTermHack);
				if (result == null) result = caseIntTerm(affineTermHack);
				if (result == null) result = caseAlgebraVisitable(affineTermHack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCHEDULE_VECTOR: {
				ScheduleVector scheduleVector = (ScheduleVector)theEObject;
				T result = caseScheduleVector(scheduleVector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCHEDULE_STATEMENT_PRAGMA: {
				ScheduleStatementPragma scheduleStatementPragma = (ScheduleStatementPragma)theEObject;
				T result = caseScheduleStatementPragma(scheduleStatementPragma);
				if (result == null) result = caseScopTransformDirective(scheduleStatementPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.SCHEDULE_BLOCK_PRAGMA: {
				ScheduleBlockPragma scheduleBlockPragma = (ScheduleBlockPragma)theEObject;
				T result = caseScheduleBlockPragma(scheduleBlockPragma);
				if (result == null) result = caseScopTransformDirective(scheduleBlockPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.OMP_PRAGMA: {
				OmpPragma ompPragma = (OmpPragma)theEObject;
				T result = caseOmpPragma(ompPragma);
				if (result == null) result = caseScopTransformDirective(ompPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.IGNORE_MEMORY_DEPENDENCY: {
				IgnoreMemoryDependency ignoreMemoryDependency = (IgnoreMemoryDependency)theEObject;
				T result = caseIgnoreMemoryDependency(ignoreMemoryDependency);
				if (result == null) result = caseScopTransformDirective(ignoreMemoryDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.LOOP_PIPELINE_PRAGMA: {
				LoopPipelinePragma loopPipelinePragma = (LoopPipelinePragma)theEObject;
				T result = caseLoopPipelinePragma(loopPipelinePragma);
				if (result == null) result = caseScopTransformDirective(loopPipelinePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.ALMA_COARSE_GRAIN_PRAGMA: {
				AlmaCoarseGrainPragma almaCoarseGrainPragma = (AlmaCoarseGrainPragma)theEObject;
				T result = caseAlmaCoarseGrainPragma(almaCoarseGrainPragma);
				if (result == null) result = caseScopTransformDirective(almaCoarseGrainPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.ALMA_FINE_GRAIN_PRAGMA: {
				AlmaFineGrainPragma almaFineGrainPragma = (AlmaFineGrainPragma)theEObject;
				T result = caseAlmaFineGrainPragma(almaFineGrainPragma);
				if (result == null) result = caseScopTransformDirective(almaFineGrainPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TransformPackage.RLT_INSET_PRAGMA: {
				RLTInsetPragma rltInsetPragma = (RLTInsetPragma)theEObject;
				T result = caseRLTInsetPragma(rltInsetPragma);
				if (result == null) result = caseScopTransformDirective(rltInsetPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scop Transform</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scop Transform</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopTransform(ScopTransform object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scop Transform Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scop Transform Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopTransformDirective(ScopTransformDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Add Context Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Add Context Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddContextDirective(AddContextDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bubble Insertion Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bubble Insertion Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBubbleInsertionDirective(BubbleInsertionDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Liveness Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Liveness Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLivenessDirective(LivenessDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tiling Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tiling Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTilingDirective(TilingDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Layout Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Layout Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataLayoutDirective(DataLayoutDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduleDirective(ScheduleDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract Array Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract Array Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContractArrayDirective(ContractArrayDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cloog Options</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cloog Options</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCloogOptions(CloogOptions object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pure Function Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pure Function Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePureFunctionPragma(PureFunctionPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule Context Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule Context Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduleContextPragma(ScheduleContextPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unroll Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unroll Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnrollPragma(UnrollPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlinePragma(InlinePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Motion Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Motion Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataMotionPragma(DataMotionPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duplicate Node Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duplicate Node Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDuplicateNodePragma(DuplicateNodePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Slice Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Slice Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSlicePragma(SlicePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Merge Arrays Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Merge Arrays Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMergeArraysPragma(MergeArraysPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>S2S4HLS Pragma Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>S2S4HLS Pragma Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseS2S4HLSPragmaAnnotation(S2S4HLSPragmaAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Proc Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Proc Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedProcSet(AnnotatedProcSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Affine Term Hack</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Affine Term Hack</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAffineTermHack(AffineTermHack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduleVector(ScheduleVector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule Statement Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule Statement Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduleStatementPragma(ScheduleStatementPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule Block Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule Block Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduleBlockPragma(ScheduleBlockPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Omp Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Omp Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOmpPragma(OmpPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ignore Memory Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ignore Memory Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIgnoreMemoryDependency(IgnoreMemoryDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Pipeline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Pipeline Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopPipelinePragma(LoopPipelinePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alma Coarse Grain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alma Coarse Grain Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlmaCoarseGrainPragma(AlmaCoarseGrainPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alma Fine Grain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alma Fine Grain Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlmaFineGrainPragma(AlmaFineGrainPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RLT Inset Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RLT Inset Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRLTInsetPragma(RLTInsetPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotation(IAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlgebraVisitable(AlgebraVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntTerm(IntTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAffineTerm(AffineTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TransformSwitch
