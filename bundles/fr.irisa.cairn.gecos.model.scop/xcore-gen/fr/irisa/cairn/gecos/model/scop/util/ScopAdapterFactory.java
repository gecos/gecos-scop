/**
 */
package fr.irisa.cairn.gecos.model.scop.util;

import fr.irisa.cairn.gecos.model.scop.*;
import gecos.annotations.AnnotatedElement;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitable;
import gecos.core.CoreVisitable;
import gecos.core.GecosNode;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;
import gecos.core.ScopeContainer;
import gecos.instrs.ChildrenListInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.InstrsVisitable;
import gecos.instrs.Instruction;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;
import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage
 * @generated
 */
public class ScopAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScopPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ScopPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopSwitch<Adapter> modelSwitch =
		new ScopSwitch<Adapter>() {
			@Override
			public Adapter caseScopNode(ScopNode object) {
				return createScopNodeAdapter();
			}
			@Override
			public Adapter caseScopDimension(ScopDimension object) {
				return createScopDimensionAdapter();
			}
			@Override
			public Adapter caseScopParameter(ScopParameter object) {
				return createScopParameterAdapter();
			}
			@Override
			public Adapter caseGecosScopBlock(GecosScopBlock object) {
				return createGecosScopBlockAdapter();
			}
			@Override
			public Adapter caseScopGuard(ScopGuard object) {
				return createScopGuardAdapter();
			}
			@Override
			public Adapter caseScopIndexExpression(ScopIndexExpression object) {
				return createScopIndexExpressionAdapter();
			}
			@Override
			public Adapter caseScopAccess(ScopAccess object) {
				return createScopAccessAdapter();
			}
			@Override
			public Adapter caseScopRead(ScopRead object) {
				return createScopReadAdapter();
			}
			@Override
			public Adapter caseScopRegionRead(ScopRegionRead object) {
				return createScopRegionReadAdapter();
			}
			@Override
			public Adapter caseScopRegionWrite(ScopRegionWrite object) {
				return createScopRegionWriteAdapter();
			}
			@Override
			public Adapter caseScopWrite(ScopWrite object) {
				return createScopWriteAdapter();
			}
			@Override
			public Adapter caseScopIntExpression(ScopIntExpression object) {
				return createScopIntExpressionAdapter();
			}
			@Override
			public Adapter caseScopIntConstraint(ScopIntConstraint object) {
				return createScopIntConstraintAdapter();
			}
			@Override
			public Adapter caseScopIntConstraintSystem(ScopIntConstraintSystem object) {
				return createScopIntConstraintSystemAdapter();
			}
			@Override
			public Adapter caseScopStatement(ScopStatement object) {
				return createScopStatementAdapter();
			}
			@Override
			public Adapter caseScopInstructionStatement(ScopInstructionStatement object) {
				return createScopInstructionStatementAdapter();
			}
			@Override
			public Adapter caseScopBlockStatement(ScopBlockStatement object) {
				return createScopBlockStatementAdapter();
			}
			@Override
			public Adapter caseScopBlock(ScopBlock object) {
				return createScopBlockAdapter();
			}
			@Override
			public Adapter caseScopForLoop(ScopForLoop object) {
				return createScopForLoopAdapter();
			}
			@Override
			public Adapter caseScopDoAllLoop(ScopDoAllLoop object) {
				return createScopDoAllLoopAdapter();
			}
			@Override
			public Adapter casescopAsyncBlock(scopAsyncBlock object) {
				return createscopAsyncBlockAdapter();
			}
			@Override
			public Adapter caseScopFSMBlock(ScopFSMBlock object) {
				return createScopFSMBlockAdapter();
			}
			@Override
			public Adapter caseScopFSMTransition(ScopFSMTransition object) {
				return createScopFSMTransitionAdapter();
			}
			@Override
			public Adapter caseScopFSMState(ScopFSMState object) {
				return createScopFSMStateAdapter();
			}
			@Override
			public Adapter caseScopUnexpandedNode(ScopUnexpandedNode object) {
				return createScopUnexpandedNodeAdapter();
			}
			@Override
			public Adapter caseScopUnexpandedStatement(ScopUnexpandedStatement object) {
				return createScopUnexpandedStatementAdapter();
			}
			@Override
			public Adapter caseDefUseEdge(DefUseEdge object) {
				return createDefUseEdgeAdapter();
			}
			@Override
			public Adapter caseUseDefEdge(UseDefEdge object) {
				return createUseDefEdgeAdapter();
			}
			@Override
			public Adapter caseWAWEdge(WAWEdge object) {
				return createWAWEdgeAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseAlgebraVisitable(AlgebraVisitable object) {
				return createAlgebraVisitableAdapter();
			}
			@Override
			public Adapter caseVariable(Variable object) {
				return createVariableAdapter();
			}
			@Override
			public Adapter caseISymbolUse(ISymbolUse object) {
				return createISymbolUseAdapter();
			}
			@Override
			public Adapter caseBlocksVisitable(BlocksVisitable object) {
				return createBlocksVisitableAdapter();
			}
			@Override
			public Adapter caseBlock(Block object) {
				return createBlockAdapter();
			}
			@Override
			public Adapter caseBasicBlock(BasicBlock object) {
				return createBasicBlockAdapter();
			}
			@Override
			public Adapter caseCoreVisitable(CoreVisitable object) {
				return createCoreVisitableAdapter();
			}
			@Override
			public Adapter caseScopeContainer(ScopeContainer object) {
				return createScopeContainerAdapter();
			}
			@Override
			public Adapter caseInstrsVisitable(InstrsVisitable object) {
				return createInstrsVisitableAdapter();
			}
			@Override
			public Adapter caseITypedElement(ITypedElement object) {
				return createITypedElementAdapter();
			}
			@Override
			public Adapter caseInstruction(Instruction object) {
				return createInstructionAdapter();
			}
			@Override
			public Adapter caseComplexInstruction(ComplexInstruction object) {
				return createComplexInstructionAdapter();
			}
			@Override
			public Adapter caseChildrenListInstruction(ChildrenListInstruction object) {
				return createChildrenListInstructionAdapter();
			}
			@Override
			public Adapter caseGenericInstruction(GenericInstruction object) {
				return createGenericInstructionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopNode
	 * @generated
	 */
	public Adapter createScopNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDimension
	 * @generated
	 */
	public Adapter createScopDimensionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopParameter
	 * @generated
	 */
	public Adapter createScopParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.GecosScopBlock <em>Gecos Scop Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.GecosScopBlock
	 * @generated
	 */
	public Adapter createGecosScopBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopGuard
	 * @generated
	 */
	public Adapter createScopGuardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopIndexExpression <em>Index Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIndexExpression
	 * @generated
	 */
	public Adapter createScopIndexExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopAccess <em>Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopAccess
	 * @generated
	 */
	public Adapter createScopAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopRead <em>Read</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRead
	 * @generated
	 */
	public Adapter createScopReadAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionRead <em>Region Read</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionRead
	 * @generated
	 */
	public Adapter createScopRegionReadAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopRegionWrite <em>Region Write</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopRegionWrite
	 * @generated
	 */
	public Adapter createScopRegionWriteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopWrite <em>Write</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopWrite
	 * @generated
	 */
	public Adapter createScopWriteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntExpression <em>Int Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntExpression
	 * @generated
	 */
	public Adapter createScopIntExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraint <em>Int Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraint
	 * @generated
	 */
	public Adapter createScopIntConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem <em>Int Constraint System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopIntConstraintSystem
	 * @generated
	 */
	public Adapter createScopIntConstraintSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopStatement
	 * @generated
	 */
	public Adapter createScopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement <em>Instruction Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement
	 * @generated
	 */
	public Adapter createScopInstructionStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlock
	 * @generated
	 */
	public Adapter createScopBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopForLoop <em>For Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopForLoop
	 * @generated
	 */
	public Adapter createScopForLoopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop <em>Do All Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop
	 * @generated
	 */
	public Adapter createScopDoAllLoopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.scopAsyncBlock <em>scop Async Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.scopAsyncBlock
	 * @generated
	 */
	public Adapter createscopAsyncBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMBlock <em>FSM Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMBlock
	 * @generated
	 */
	public Adapter createScopFSMBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMTransition <em>FSM Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMTransition
	 * @generated
	 */
	public Adapter createScopFSMTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopFSMState <em>FSM State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopFSMState
	 * @generated
	 */
	public Adapter createScopFSMStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode <em>Unexpanded Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedNode
	 * @generated
	 */
	public Adapter createScopUnexpandedNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement <em>Unexpanded Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement
	 * @generated
	 */
	public Adapter createScopUnexpandedStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.ScopBlockStatement <em>Block Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.ScopBlockStatement
	 * @generated
	 */
	public Adapter createScopBlockStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.DefUseEdge <em>Def Use Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.DefUseEdge
	 * @generated
	 */
	public Adapter createDefUseEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.UseDefEdge <em>Use Def Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.UseDefEdge
	 * @generated
	 */
	public Adapter createUseDefEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.gecos.model.scop.WAWEdge <em>WAW Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.gecos.model.scop.WAWEdge
	 * @generated
	 */
	public Adapter createWAWEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polymodel.algebra.AlgebraVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.polymodel.algebra.AlgebraVisitable
	 * @generated
	 */
	public Adapter createAlgebraVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polymodel.algebra.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.polymodel.algebra.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ISymbolUse <em>ISymbol Use</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ISymbolUse
	 * @generated
	 */
	public Adapter createISymbolUseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BlocksVisitable
	 * @generated
	 */
	public Adapter createBlocksVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Block
	 * @generated
	 */
	public Adapter createBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BasicBlock <em>Basic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BasicBlock
	 * @generated
	 */
	public Adapter createBasicBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.CoreVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.CoreVisitable
	 * @generated
	 */
	public Adapter createCoreVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ScopeContainer <em>Scope Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ScopeContainer
	 * @generated
	 */
	public Adapter createScopeContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.InstrsVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.InstrsVisitable
	 * @generated
	 */
	public Adapter createInstrsVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ITypedElement <em>ITyped Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ITypedElement
	 * @generated
	 */
	public Adapter createITypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.Instruction
	 * @generated
	 */
	public Adapter createInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ComplexInstruction <em>Complex Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ComplexInstruction
	 * @generated
	 */
	public Adapter createComplexInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ChildrenListInstruction <em>Children List Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ChildrenListInstruction
	 * @generated
	 */
	public Adapter createChildrenListInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.GenericInstruction <em>Generic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.GenericInstruction
	 * @generated
	 */
	public Adapter createGenericInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ScopAdapterFactory
