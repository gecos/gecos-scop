/**
 */
package fr.irisa.cairn.gecos.model.scop.util;

import fr.irisa.cairn.gecos.model.scop.*;
import gecos.annotations.AnnotatedElement;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitable;
import gecos.core.CoreVisitable;
import gecos.core.GecosNode;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;
import gecos.core.ScopeContainer;
import gecos.instrs.ChildrenListInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.InstrsVisitable;
import gecos.instrs.Instruction;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;
import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scop.ScopPackage
 * @generated
 */
public class ScopSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScopPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopSwitch() {
		if (modelPackage == null) {
			modelPackage = ScopPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ScopPackage.SCOP_NODE: {
				ScopNode scopNode = (ScopNode)theEObject;
				T result = caseScopNode(scopNode);
				if (result == null) result = caseAnnotatedElement(scopNode);
				if (result == null) result = caseGecosNode(scopNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_DIMENSION: {
				ScopDimension scopDimension = (ScopDimension)theEObject;
				T result = caseScopDimension(scopDimension);
				if (result == null) result = caseVariable(scopDimension);
				if (result == null) result = caseISymbolUse(scopDimension);
				if (result == null) result = caseAlgebraVisitable(scopDimension);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_PARAMETER: {
				ScopParameter scopParameter = (ScopParameter)theEObject;
				T result = caseScopParameter(scopParameter);
				if (result == null) result = caseScopDimension(scopParameter);
				if (result == null) result = caseVariable(scopParameter);
				if (result == null) result = caseISymbolUse(scopParameter);
				if (result == null) result = caseAlgebraVisitable(scopParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.GECOS_SCOP_BLOCK: {
				GecosScopBlock gecosScopBlock = (GecosScopBlock)theEObject;
				T result = caseGecosScopBlock(gecosScopBlock);
				if (result == null) result = caseScopNode(gecosScopBlock);
				if (result == null) result = caseBasicBlock(gecosScopBlock);
				if (result == null) result = caseScopeContainer(gecosScopBlock);
				if (result == null) result = caseBlock(gecosScopBlock);
				if (result == null) result = caseCoreVisitable(gecosScopBlock);
				if (result == null) result = caseAnnotatedElement(gecosScopBlock);
				if (result == null) result = caseBlocksVisitable(gecosScopBlock);
				if (result == null) result = caseGecosNode(gecosScopBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_GUARD: {
				ScopGuard scopGuard = (ScopGuard)theEObject;
				T result = caseScopGuard(scopGuard);
				if (result == null) result = caseScopNode(scopGuard);
				if (result == null) result = caseAnnotatedElement(scopGuard);
				if (result == null) result = caseGecosNode(scopGuard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_INDEX_EXPRESSION: {
				ScopIndexExpression scopIndexExpression = (ScopIndexExpression)theEObject;
				T result = caseScopIndexExpression(scopIndexExpression);
				if (result == null) result = caseScopNode(scopIndexExpression);
				if (result == null) result = caseInstruction(scopIndexExpression);
				if (result == null) result = caseAnnotatedElement(scopIndexExpression);
				if (result == null) result = caseInstrsVisitable(scopIndexExpression);
				if (result == null) result = caseITypedElement(scopIndexExpression);
				if (result == null) result = caseGecosNode(scopIndexExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_ACCESS: {
				ScopAccess scopAccess = (ScopAccess)theEObject;
				T result = caseScopAccess(scopAccess);
				if (result == null) result = caseScopIndexExpression(scopAccess);
				if (result == null) result = caseScopNode(scopAccess);
				if (result == null) result = caseInstruction(scopAccess);
				if (result == null) result = caseAnnotatedElement(scopAccess);
				if (result == null) result = caseInstrsVisitable(scopAccess);
				if (result == null) result = caseITypedElement(scopAccess);
				if (result == null) result = caseGecosNode(scopAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_READ: {
				ScopRead scopRead = (ScopRead)theEObject;
				T result = caseScopRead(scopRead);
				if (result == null) result = caseScopAccess(scopRead);
				if (result == null) result = caseScopIndexExpression(scopRead);
				if (result == null) result = caseScopNode(scopRead);
				if (result == null) result = caseInstruction(scopRead);
				if (result == null) result = caseAnnotatedElement(scopRead);
				if (result == null) result = caseInstrsVisitable(scopRead);
				if (result == null) result = caseITypedElement(scopRead);
				if (result == null) result = caseGecosNode(scopRead);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_REGION_READ: {
				ScopRegionRead scopRegionRead = (ScopRegionRead)theEObject;
				T result = caseScopRegionRead(scopRegionRead);
				if (result == null) result = caseScopRead(scopRegionRead);
				if (result == null) result = caseScopAccess(scopRegionRead);
				if (result == null) result = caseScopIndexExpression(scopRegionRead);
				if (result == null) result = caseScopNode(scopRegionRead);
				if (result == null) result = caseInstruction(scopRegionRead);
				if (result == null) result = caseAnnotatedElement(scopRegionRead);
				if (result == null) result = caseInstrsVisitable(scopRegionRead);
				if (result == null) result = caseITypedElement(scopRegionRead);
				if (result == null) result = caseGecosNode(scopRegionRead);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_REGION_WRITE: {
				ScopRegionWrite scopRegionWrite = (ScopRegionWrite)theEObject;
				T result = caseScopRegionWrite(scopRegionWrite);
				if (result == null) result = caseScopRead(scopRegionWrite);
				if (result == null) result = caseScopAccess(scopRegionWrite);
				if (result == null) result = caseScopIndexExpression(scopRegionWrite);
				if (result == null) result = caseScopNode(scopRegionWrite);
				if (result == null) result = caseInstruction(scopRegionWrite);
				if (result == null) result = caseAnnotatedElement(scopRegionWrite);
				if (result == null) result = caseInstrsVisitable(scopRegionWrite);
				if (result == null) result = caseITypedElement(scopRegionWrite);
				if (result == null) result = caseGecosNode(scopRegionWrite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_WRITE: {
				ScopWrite scopWrite = (ScopWrite)theEObject;
				T result = caseScopWrite(scopWrite);
				if (result == null) result = caseScopAccess(scopWrite);
				if (result == null) result = caseScopIndexExpression(scopWrite);
				if (result == null) result = caseScopNode(scopWrite);
				if (result == null) result = caseInstruction(scopWrite);
				if (result == null) result = caseAnnotatedElement(scopWrite);
				if (result == null) result = caseInstrsVisitable(scopWrite);
				if (result == null) result = caseITypedElement(scopWrite);
				if (result == null) result = caseGecosNode(scopWrite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_INT_EXPRESSION: {
				ScopIntExpression scopIntExpression = (ScopIntExpression)theEObject;
				T result = caseScopIntExpression(scopIntExpression);
				if (result == null) result = caseInstruction(scopIntExpression);
				if (result == null) result = caseAnnotatedElement(scopIntExpression);
				if (result == null) result = caseInstrsVisitable(scopIntExpression);
				if (result == null) result = caseITypedElement(scopIntExpression);
				if (result == null) result = caseGecosNode(scopIntExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_INT_CONSTRAINT: {
				ScopIntConstraint scopIntConstraint = (ScopIntConstraint)theEObject;
				T result = caseScopIntConstraint(scopIntConstraint);
				if (result == null) result = caseInstruction(scopIntConstraint);
				if (result == null) result = caseAnnotatedElement(scopIntConstraint);
				if (result == null) result = caseInstrsVisitable(scopIntConstraint);
				if (result == null) result = caseITypedElement(scopIntConstraint);
				if (result == null) result = caseGecosNode(scopIntConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_INT_CONSTRAINT_SYSTEM: {
				ScopIntConstraintSystem scopIntConstraintSystem = (ScopIntConstraintSystem)theEObject;
				T result = caseScopIntConstraintSystem(scopIntConstraintSystem);
				if (result == null) result = caseInstruction(scopIntConstraintSystem);
				if (result == null) result = caseAnnotatedElement(scopIntConstraintSystem);
				if (result == null) result = caseInstrsVisitable(scopIntConstraintSystem);
				if (result == null) result = caseITypedElement(scopIntConstraintSystem);
				if (result == null) result = caseGecosNode(scopIntConstraintSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_STATEMENT: {
				ScopStatement scopStatement = (ScopStatement)theEObject;
				T result = caseScopStatement(scopStatement);
				if (result == null) result = caseScopNode(scopStatement);
				if (result == null) result = caseAnnotatedElement(scopStatement);
				if (result == null) result = caseGecosNode(scopStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_INSTRUCTION_STATEMENT: {
				ScopInstructionStatement scopInstructionStatement = (ScopInstructionStatement)theEObject;
				T result = caseScopInstructionStatement(scopInstructionStatement);
				if (result == null) result = caseGenericInstruction(scopInstructionStatement);
				if (result == null) result = caseScopStatement(scopInstructionStatement);
				if (result == null) result = caseChildrenListInstruction(scopInstructionStatement);
				if (result == null) result = caseScopNode(scopInstructionStatement);
				if (result == null) result = caseComplexInstruction(scopInstructionStatement);
				if (result == null) result = caseInstruction(scopInstructionStatement);
				if (result == null) result = caseAnnotatedElement(scopInstructionStatement);
				if (result == null) result = caseInstrsVisitable(scopInstructionStatement);
				if (result == null) result = caseITypedElement(scopInstructionStatement);
				if (result == null) result = caseGecosNode(scopInstructionStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_BLOCK_STATEMENT: {
				ScopBlockStatement scopBlockStatement = (ScopBlockStatement)theEObject;
				T result = caseScopBlockStatement(scopBlockStatement);
				if (result == null) result = caseScopStatement(scopBlockStatement);
				if (result == null) result = caseScopNode(scopBlockStatement);
				if (result == null) result = caseAnnotatedElement(scopBlockStatement);
				if (result == null) result = caseGecosNode(scopBlockStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_BLOCK: {
				ScopBlock scopBlock = (ScopBlock)theEObject;
				T result = caseScopBlock(scopBlock);
				if (result == null) result = caseScopNode(scopBlock);
				if (result == null) result = caseAnnotatedElement(scopBlock);
				if (result == null) result = caseGecosNode(scopBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_FOR_LOOP: {
				ScopForLoop scopForLoop = (ScopForLoop)theEObject;
				T result = caseScopForLoop(scopForLoop);
				if (result == null) result = caseScopNode(scopForLoop);
				if (result == null) result = caseAnnotatedElement(scopForLoop);
				if (result == null) result = caseGecosNode(scopForLoop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_DO_ALL_LOOP: {
				ScopDoAllLoop scopDoAllLoop = (ScopDoAllLoop)theEObject;
				T result = caseScopDoAllLoop(scopDoAllLoop);
				if (result == null) result = caseScopForLoop(scopDoAllLoop);
				if (result == null) result = caseScopNode(scopDoAllLoop);
				if (result == null) result = caseAnnotatedElement(scopDoAllLoop);
				if (result == null) result = caseGecosNode(scopDoAllLoop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_ASYNC_BLOCK: {
				scopAsyncBlock scopAsyncBlock = (scopAsyncBlock)theEObject;
				T result = casescopAsyncBlock(scopAsyncBlock);
				if (result == null) result = caseScopBlock(scopAsyncBlock);
				if (result == null) result = caseScopNode(scopAsyncBlock);
				if (result == null) result = caseAnnotatedElement(scopAsyncBlock);
				if (result == null) result = caseGecosNode(scopAsyncBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_FSM_BLOCK: {
				ScopFSMBlock scopFSMBlock = (ScopFSMBlock)theEObject;
				T result = caseScopFSMBlock(scopFSMBlock);
				if (result == null) result = caseScopNode(scopFSMBlock);
				if (result == null) result = caseAnnotatedElement(scopFSMBlock);
				if (result == null) result = caseGecosNode(scopFSMBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_FSM_TRANSITION: {
				ScopFSMTransition scopFSMTransition = (ScopFSMTransition)theEObject;
				T result = caseScopFSMTransition(scopFSMTransition);
				if (result == null) result = caseScopNode(scopFSMTransition);
				if (result == null) result = caseAnnotatedElement(scopFSMTransition);
				if (result == null) result = caseGecosNode(scopFSMTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_FSM_STATE: {
				ScopFSMState scopFSMState = (ScopFSMState)theEObject;
				T result = caseScopFSMState(scopFSMState);
				if (result == null) result = caseScopNode(scopFSMState);
				if (result == null) result = caseAnnotatedElement(scopFSMState);
				if (result == null) result = caseGecosNode(scopFSMState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_UNEXPANDED_NODE: {
				ScopUnexpandedNode scopUnexpandedNode = (ScopUnexpandedNode)theEObject;
				T result = caseScopUnexpandedNode(scopUnexpandedNode);
				if (result == null) result = caseScopNode(scopUnexpandedNode);
				if (result == null) result = caseAnnotatedElement(scopUnexpandedNode);
				if (result == null) result = caseGecosNode(scopUnexpandedNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.SCOP_UNEXPANDED_STATEMENT: {
				ScopUnexpandedStatement scopUnexpandedStatement = (ScopUnexpandedStatement)theEObject;
				T result = caseScopUnexpandedStatement(scopUnexpandedStatement);
				if (result == null) result = caseScopInstructionStatement(scopUnexpandedStatement);
				if (result == null) result = caseScopUnexpandedNode(scopUnexpandedStatement);
				if (result == null) result = caseGenericInstruction(scopUnexpandedStatement);
				if (result == null) result = caseScopStatement(scopUnexpandedStatement);
				if (result == null) result = caseChildrenListInstruction(scopUnexpandedStatement);
				if (result == null) result = caseScopNode(scopUnexpandedStatement);
				if (result == null) result = caseComplexInstruction(scopUnexpandedStatement);
				if (result == null) result = caseInstruction(scopUnexpandedStatement);
				if (result == null) result = caseAnnotatedElement(scopUnexpandedStatement);
				if (result == null) result = caseInstrsVisitable(scopUnexpandedStatement);
				if (result == null) result = caseITypedElement(scopUnexpandedStatement);
				if (result == null) result = caseGecosNode(scopUnexpandedStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.DEF_USE_EDGE: {
				DefUseEdge defUseEdge = (DefUseEdge)theEObject;
				T result = caseDefUseEdge(defUseEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.USE_DEF_EDGE: {
				UseDefEdge useDefEdge = (UseDefEdge)theEObject;
				T result = caseUseDefEdge(useDefEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScopPackage.WAW_EDGE: {
				WAWEdge wawEdge = (WAWEdge)theEObject;
				T result = caseWAWEdge(wawEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopNode(ScopNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dimension</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dimension</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopDimension(ScopDimension object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopParameter(ScopParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Scop Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Scop Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosScopBlock(GecosScopBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopGuard(ScopGuard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Index Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Index Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopIndexExpression(ScopIndexExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopAccess(ScopAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Read</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Read</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopRead(ScopRead object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Region Read</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Region Read</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopRegionRead(ScopRegionRead object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Region Write</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Region Write</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopRegionWrite(ScopRegionWrite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Write</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Write</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopWrite(ScopWrite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopIntExpression(ScopIntExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopIntConstraint(ScopIntConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Constraint System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Constraint System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopIntConstraintSystem(ScopIntConstraintSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopStatement(ScopStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instruction Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instruction Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopInstructionStatement(ScopInstructionStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopBlock(ScopBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopForLoop(ScopForLoop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Do All Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Do All Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopDoAllLoop(ScopDoAllLoop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>scop Async Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>scop Async Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casescopAsyncBlock(scopAsyncBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopFSMBlock(ScopFSMBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopFSMTransition(ScopFSMTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopFSMState(ScopFSMState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unexpanded Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unexpanded Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopUnexpandedNode(ScopUnexpandedNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unexpanded Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unexpanded Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopUnexpandedStatement(ScopUnexpandedStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopBlockStatement(ScopBlockStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Def Use Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Def Use Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefUseEdge(DefUseEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Use Def Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Def Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUseDefEdge(UseDefEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>WAW Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>WAW Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWAWEdge(WAWEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlgebraVisitable(AlgebraVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISymbol Use</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISymbol Use</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISymbolUse(ISymbolUse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlocksVisitable(BlocksVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicBlock(BasicBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoreVisitable(CoreVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scope Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scope Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopeContainer(ScopeContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstrsVisitable(InstrsVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITypedElement(ITypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstruction(Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexInstruction(ComplexInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Children List Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Children List Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChildrenListInstruction(ChildrenListInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericInstruction(GenericInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ScopSwitch
