package fr.irisa.cairn.gecos.model.scop.util;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopDoAllLoop;
import fr.irisa.cairn.gecos.model.scop.ScopFSMBlock;
import fr.irisa.cairn.gecos.model.scop.ScopFSMState;
import fr.irisa.cairn.gecos.model.scop.ScopFSMTransition;
import fr.irisa.cairn.gecos.model.scop.ScopForLoop;
import fr.irisa.cairn.gecos.model.scop.ScopGuard;
import fr.irisa.cairn.gecos.model.scop.ScopInstructionStatement;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopRead;
import fr.irisa.cairn.gecos.model.scop.ScopRegionRead;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedBlock;
import fr.irisa.cairn.gecos.model.scop.ScopUnexpandedStatement;
import fr.irisa.cairn.gecos.model.scop.ScopWrite;
import fr.irisa.cairn.gecos.model.scop.transform.ContractArrayDirective;
import fr.irisa.cairn.gecos.model.scop.transform.DataLayoutDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScheduleDirective;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransform;
import fr.irisa.cairn.gecos.model.scop.transform.ScopTransformDirective;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;

@SuppressWarnings("all")
public class ScopPrettyPrinter {
  protected static String _print(final ScopDimension r) {
    String _xifexpression = null;
    Symbol _symbol = r.getSymbol();
    boolean _tripleNotEquals = (_symbol != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = r.getSymbol().getName();
      _builder.append(_name);
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("?");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  protected static String _print(final ScopNode n) {
    throw new UnsupportedOperationException("NYI");
  }
  
  protected static String _print(final ScopGuard n) {
    String _xblockexpression = null;
    {
      if ((n == null)) {
        throw new UnsupportedOperationException("NPE");
      }
      EList<IntConstraintSystem> _predicate = n.getPredicate();
      boolean _tripleEquals = (_predicate == null);
      if (_tripleEquals) {
        throw new UnsupportedOperationException("no predicate");
      }
      ScopNode _thenNode = n.getThenNode();
      boolean _tripleEquals_1 = (_thenNode == null);
      if (_tripleEquals_1) {
        throw new UnsupportedOperationException("no then branch");
      }
      String _xifexpression = null;
      ScopNode _elseNode = n.getElseNode();
      boolean _tripleNotEquals = (_elseNode != null);
      if (_tripleNotEquals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("if (");
        EList<IntConstraintSystem> _predicate_1 = n.getPredicate();
        _builder.append(_predicate_1);
        _builder.append(") ");
        _builder.newLineIfNotEmpty();
        _builder.append("   ");
        String _xifexpression_1 = null;
        ScopNode _thenNode_1 = n.getThenNode();
        boolean _notEquals = (!Objects.equal(_thenNode_1, null));
        if (_notEquals) {
          _xifexpression_1 = ScopPrettyPrinter.print(n.getThenNode());
        } else {
          _xifexpression_1 = "[null]";
        }
        _builder.append(_xifexpression_1, "   ");
        _builder.newLineIfNotEmpty();
        _builder.append("else\t\t\t");
        _builder.newLine();
        _builder.append("   ");
        String _xifexpression_2 = null;
        ScopNode _elseNode_1 = n.getElseNode();
        boolean _notEquals_1 = (!Objects.equal(_elseNode_1, null));
        if (_notEquals_1) {
          _xifexpression_2 = ScopPrettyPrinter.print(n.getElseNode());
        } else {
          _xifexpression_2 = "[null]";
        }
        _builder.append(_xifexpression_2, "   ");
        _builder.newLineIfNotEmpty();
        _xifexpression = _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if (");
        EList<IntConstraintSystem> _predicate_2 = n.getPredicate();
        _builder_1.append(_predicate_2);
        _builder_1.append(") ");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("   ");
        String _print = ScopPrettyPrinter.print(n.getThenNode());
        _builder_1.append(_print, "   ");
        _builder_1.newLineIfNotEmpty();
        _xifexpression = _builder_1.toString();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected static String _print(final ScopForLoop l) {
    String _xblockexpression = null;
    {
      if ((l == null)) {
        throw new UnsupportedOperationException("NPE");
      }
      final ScopDimension iterator = l.getIterator();
      String _xifexpression = null;
      if ((iterator != null)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("for (");
        String _symName = iterator.getSymName();
        _builder.append(_symName);
        _builder.append("[");
        String _name = iterator.getName();
        _builder.append(_name);
        _builder.append("]=");
        IntExpression _lB = l.getLB();
        _builder.append(_lB);
        _builder.append(":");
        IntExpression _stride = l.getStride();
        _builder.append(_stride);
        _builder.append(":");
        IntExpression _uB = l.getUB();
        _builder.append(_uB);
        _builder.append(") ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        String _xifexpression_1 = null;
        ScopNode _body = l.getBody();
        boolean _tripleNotEquals = (_body != null);
        if (_tripleNotEquals) {
          _xifexpression_1 = ScopPrettyPrinter.print(l.getBody());
        } else {
          _xifexpression_1 = "[null]";
        }
        _builder.append(_xifexpression_1, "\t");
        _builder.newLineIfNotEmpty();
        _xifexpression = _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("for (?=");
        IntExpression _lB_1 = l.getLB();
        _builder_1.append(_lB_1);
        _builder_1.append(":");
        IntExpression _stride_1 = l.getStride();
        _builder_1.append(_stride_1);
        _builder_1.append(":");
        IntExpression _uB_1 = l.getUB();
        _builder_1.append(_uB_1);
        _builder_1.append(") ");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        String _xifexpression_2 = null;
        ScopNode _body_1 = l.getBody();
        boolean _tripleNotEquals_1 = (_body_1 != null);
        if (_tripleNotEquals_1) {
          _xifexpression_2 = ScopPrettyPrinter.print(l.getBody());
        } else {
          _xifexpression_2 = "[null]";
        }
        _builder_1.append(_xifexpression_2, "\t");
        _builder_1.newLineIfNotEmpty();
        _xifexpression = _builder_1.toString();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected static String _print(final ScopDoAllLoop l) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("doall (");
    String _name = l.getIterator().getName();
    _builder.append(_name);
    _builder.append("=");
    IntExpression _lB = l.getLB();
    _builder.append(_lB);
    _builder.append(":");
    IntExpression _stride = l.getStride();
    _builder.append(_stride);
    _builder.append(":");
    IntExpression _uB = l.getUB();
    _builder.append(_uB);
    _builder.append(") ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _print = ScopPrettyPrinter.print(l.getBody());
    _builder.append(_print, "\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  protected static String _print(final ScopBlock n) {
    String _xifexpression = null;
    EList<ScopNode> _children = n.getChildren();
    boolean _tripleNotEquals = (_children != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("{");
      _builder.newLine();
      _builder.append("\t");
      {
        EList<ScopNode> _children_1 = n.getChildren();
        for(final ScopNode i : _children_1) {
          String _xifexpression_1 = null;
          if ((i != null)) {
            _xifexpression_1 = ScopPrettyPrinter.print(i);
          } else {
            _xifexpression_1 = "[null]";
          }
          _builder.append(_xifexpression_1, "\t");
        }
      }
      _builder.append("\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _xifexpression = _builder.toString();
    }
    return _xifexpression;
  }
  
  protected static String _print(final GecosScopBlock n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("GecosScopBlock_");
    int _number = n.getNumber();
    _builder.append(_number);
    EList<ScopDimension> _parameters = n.getParameters();
    _builder.append(_parameters);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("live-in : ");
    EList<Symbol> _liveInSymbols = n.getLiveInSymbols();
    _builder.append(_liveInSymbols, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("live-out : ");
    EList<Symbol> _liveOutSymbols = n.getLiveOutSymbols();
    _builder.append(_liveOutSymbols, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("iterators : ");
    EList<ScopDimension> _iterators = n.getIterators();
    _builder.append(_iterators, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("context : ");
    IntConstraintSystem _context = n.getContext();
    _builder.append(_context, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      ScopNode _root = n.getRoot();
      boolean _tripleNotEquals = (_root != null);
      if (_tripleNotEquals) {
        String _print = ScopPrettyPrinter.print(n.getRoot());
        _builder.append(_print, "\t");
      }
    }
    _builder.append("\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    return _builder.toString();
  }
  
  protected static String _print(final ScopStatement n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[");
    {
      EList<ScopWrite> _listAllWriteAccess = n.listAllWriteAccess();
      boolean _hasElements = false;
      for(final ScopWrite r : _listAllWriteAccess) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        String _print = ScopPrettyPrinter.print(r);
        _builder.append(_print);
      }
    }
    _builder.append("] := ");
    String _id = n.getId();
    _builder.append(_id);
    _builder.append("(");
    {
      EList<ScopRead> _listAllReadAccess = n.listAllReadAccess();
      boolean _hasElements_1 = false;
      for(final ScopRead r_1 : _listAllReadAccess) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        String _print_1 = ScopPrettyPrinter.print(r_1);
        _builder.append(_print_1);
      }
    }
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  protected static String _print(final ScopInstructionStatement n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[");
    {
      EList<ScopWrite> _listAllWriteAccess = n.listAllWriteAccess();
      boolean _hasElements = false;
      for(final ScopWrite r : _listAllWriteAccess) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        String _print = ScopPrettyPrinter.print(r);
        _builder.append(_print);
      }
    }
    _builder.append("] := ");
    String _id = n.getId();
    _builder.append(_id);
    _builder.append("(");
    {
      EList<ScopRead> _listAllReadAccess = n.listAllReadAccess();
      boolean _hasElements_1 = false;
      for(final ScopRead r_1 : _listAllReadAccess) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        String _print_1 = ScopPrettyPrinter.print(r_1);
        _builder.append(_print_1);
      }
    }
    _builder.append(") -> ");
    EList<Instruction> _children = n.getChildren();
    _builder.append(_children);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  protected static String _print(final ScopUnexpandedStatement n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Unexpanded ");
    String _name = n.getName();
    _builder.append(_name);
    EList<ScopDimension> _existentials = n.getExistentials();
    _builder.append(_existentials);
    _builder.append(" -> ");
    String _domain = n.getDomain();
    _builder.append(_domain);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      ScopTransform _transform = n.getTransform();
      boolean _tripleNotEquals = (_transform != null);
      if (_tripleNotEquals) {
        _builder.append("schedule : ");
        ScheduleDirective _get = ((ScheduleDirective[])Conversions.unwrapArray(Iterables.<ScheduleDirective>filter(n.getTransform().getCommands(), ScheduleDirective.class), ScheduleDirective.class))[0];
        _builder.append(_get, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append(": ");
    Instruction _get_1 = n.getChildren().get(0);
    _builder.append(_get_1, "\t");
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("writes : ");
    {
      EList<ScopWrite> _listAllWriteAccess = n.listAllWriteAccess();
      boolean _hasElements = false;
      for(final ScopWrite r : _listAllWriteAccess) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "\t");
        }
        String _print = ScopPrettyPrinter.print(r);
        _builder.append(_print, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("reads  : ");
    {
      EList<ScopRead> _listAllReadAccess = n.listAllReadAccess();
      boolean _hasElements_1 = false;
      for(final ScopRead r_1 : _listAllReadAccess) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(",", "\t");
        }
        String _print_1 = ScopPrettyPrinter.print(r_1);
        _builder.append(_print_1, "\t");
      }
    }
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _print(final ScopUnexpandedBlock n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("UnexpandeBlock ");
    EList<ScopDimension> _existentials = n.getExistentials();
    _builder.append(_existentials);
    _builder.append(" -> ");
    String _domain = n.getDomain();
    _builder.append(_domain);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      ScopTransform _transform = n.getTransform();
      boolean _tripleNotEquals = (_transform != null);
      if (_tripleNotEquals) {
        _builder.append("schedule : ");
        ScheduleDirective _get = ((ScheduleDirective[])Conversions.unwrapArray(Iterables.<ScheduleDirective>filter(n.getTransform().getCommands(), ScheduleDirective.class), ScheduleDirective.class))[0];
        _builder.append(_get, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append(": ");
    EList<ScopNode> _children = n.getChildren();
    _builder.append(_children, "\t");
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _print(final ScopRead r) {
    String _xifexpression = null;
    Symbol _sym = r.getSym();
    boolean _tripleNotEquals = (_sym != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      String _symName = r.getSymName();
      _builder.append(_symName);
      _builder.append("[");
      {
        EList<IntExpression> _indexExpressions = r.getIndexExpressions();
        boolean _hasElements = false;
        for(final IntExpression i : _indexExpressions) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(",", "");
          }
          _builder.append(i);
        }
      }
      _builder.append("]");
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("none[");
      {
        EList<IntExpression> _indexExpressions_1 = r.getIndexExpressions();
        boolean _hasElements_1 = false;
        for(final IntExpression i_1 : _indexExpressions_1) {
          if (!_hasElements_1) {
            _hasElements_1 = true;
          } else {
            _builder_1.appendImmediate(",", "");
          }
          _builder_1.append(i_1);
        }
      }
      _builder_1.append("]");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  protected static String _print(final ScopRegionRead r) {
    String _xifexpression = null;
    Symbol _sym = r.getSym();
    boolean _tripleNotEquals = (_sym != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      String _symName = r.getSymName();
      _builder.append(_symName);
      _builder.append("[");
      {
        EList<IntExpression> _indexExpressions = r.getIndexExpressions();
        boolean _hasElements = false;
        for(final IntExpression i : _indexExpressions) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(",", "");
          }
          _builder.append(i);
        }
      }
      _builder.append(" : ");
      IntConstraintSystem _region = r.getRegion();
      _builder.append(_region);
      _builder.append("]");
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("none[");
      {
        EList<IntExpression> _indexExpressions_1 = r.getIndexExpressions();
        boolean _hasElements_1 = false;
        for(final IntExpression i_1 : _indexExpressions_1) {
          if (!_hasElements_1) {
            _hasElements_1 = true;
          } else {
            _builder_1.appendImmediate(",", "");
          }
          _builder_1.append(i_1);
        }
      }
      _builder_1.append(" : ");
      IntConstraintSystem _region_1 = r.getRegion();
      _builder_1.append(_region_1);
      _builder_1.append("]");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  protected static String _print(final ScopWrite r) {
    String _xifexpression = null;
    Symbol _sym = r.getSym();
    boolean _tripleNotEquals = (_sym != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      String _symName = r.getSymName();
      _builder.append(_symName);
      _builder.append("[");
      {
        EList<IntExpression> _indexExpressions = r.getIndexExpressions();
        boolean _hasElements = false;
        for(final IntExpression i : _indexExpressions) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(",", "");
          }
          _builder.append(i);
        }
      }
      _builder.append("]");
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("none[");
      {
        EList<IntExpression> _indexExpressions_1 = r.getIndexExpressions();
        boolean _hasElements_1 = false;
        for(final IntExpression i_1 : _indexExpressions_1) {
          if (!_hasElements_1) {
            _hasElements_1 = true;
          } else {
            _builder_1.appendImmediate(",", "");
          }
          _builder_1.append(i_1);
        }
      }
      _builder_1.append("]");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  protected static String _print(final ScopFSMBlock fsm) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Initialisation");
    _builder.newLine();
    EList<ScopDimension> _iterators = fsm.getIterators();
    _builder.append(_iterators);
    _builder.append(" := ");
    final Function1<ScopFSMTransition, String> _function = (ScopFSMTransition t) -> {
      return ScopPrettyPrinter.print(t);
    };
    List<String> _map = ListExtensions.<ScopFSMTransition, String>map(fsm.getStart(), _function);
    _builder.append(_map);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("int done_");
    int _hashCode = fsm.hashCode();
    _builder.append(_hashCode);
    _builder.append("=1;");
    _builder.newLineIfNotEmpty();
    _builder.append("while(!");
    int _hashCode_1 = fsm.hashCode();
    _builder.append(_hashCode_1);
    _builder.append("_done) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("done_");
    int _hashCode_2 = fsm.hashCode();
    _builder.append(_hashCode_2, "\t");
    _builder.append("=0;");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("// Commands");
    _builder.newLine();
    _builder.append("\t");
    {
      EList<ScopNode> _commands = fsm.getCommands();
      for(final ScopNode c : _commands) {
        String _print = ScopPrettyPrinter.print(c);
        _builder.append(_print, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("// Transitions");
    _builder.newLine();
    _builder.append("\t");
    {
      EList<ScopFSMTransition> _next = fsm.getNext();
      for(final ScopFSMTransition n : _next) {
        String _print_1 = ScopPrettyPrinter.print(n);
        _builder.append(_print_1, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _print(final ScopFSMState s) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("c");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _printDirective(final ScopTransformDirective d) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("PrettyPrint for ");
    Class<? extends ScopTransformDirective> _class = d.getClass();
    _builder.append(_class);
    _builder.append(" not yet implemented");
    return _builder.toString();
  }
  
  protected static String _printDirective(final ScheduleDirective d) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("schedule ");
    String _id = d.getStatement().getId();
    _builder.append(_id);
    _builder.append(" : ");
    String _schedule = d.getSchedule();
    _builder.append(_schedule);
    return _builder.toString();
  }
  
  protected static String _printDirective(final DataLayoutDirective d) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("remap ");
    Symbol _symbol = d.getSymbol();
    _builder.append(_symbol);
    EList<ScopDimension> _vars = d.getVars();
    _builder.append(_vars);
    _builder.append(" to ");
    Symbol _symbol_1 = d.getSymbol();
    _builder.append(_symbol_1);
    EList<IntExpression> _addresses = d.getAddresses();
    _builder.append(_addresses);
    _builder.append(" ");
    return _builder.toString();
  }
  
  protected static String _printDirective(final ContractArrayDirective d) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("comtract ");
    Symbol _symbol = d.getSymbol();
    _builder.append(_symbol);
    return _builder.toString();
  }
  
  protected static String _print(final ScopFSMTransition t) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder.toString();
  }
  
  public static String print(final EObject n) {
    if (n instanceof ScopUnexpandedStatement) {
      return _print((ScopUnexpandedStatement)n);
    } else if (n instanceof ScopInstructionStatement) {
      return _print((ScopInstructionStatement)n);
    } else if (n instanceof ScopRegionRead) {
      return _print((ScopRegionRead)n);
    } else if (n instanceof ScopRead) {
      return _print((ScopRead)n);
    } else if (n instanceof ScopWrite) {
      return _print((ScopWrite)n);
    } else if (n instanceof GecosScopBlock) {
      return _print((GecosScopBlock)n);
    } else if (n instanceof ScopDoAllLoop) {
      return _print((ScopDoAllLoop)n);
    } else if (n instanceof ScopUnexpandedBlock) {
      return _print((ScopUnexpandedBlock)n);
    } else if (n instanceof ScopBlock) {
      return _print((ScopBlock)n);
    } else if (n instanceof ScopFSMBlock) {
      return _print((ScopFSMBlock)n);
    } else if (n instanceof ScopFSMState) {
      return _print((ScopFSMState)n);
    } else if (n instanceof ScopFSMTransition) {
      return _print((ScopFSMTransition)n);
    } else if (n instanceof ScopForLoop) {
      return _print((ScopForLoop)n);
    } else if (n instanceof ScopGuard) {
      return _print((ScopGuard)n);
    } else if (n instanceof ScopStatement) {
      return _print((ScopStatement)n);
    } else if (n instanceof ScopDimension) {
      return _print((ScopDimension)n);
    } else if (n instanceof ScopNode) {
      return _print((ScopNode)n);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(n).toString());
    }
  }
  
  public static String printDirective(final ScopTransformDirective d) {
    if (d instanceof ContractArrayDirective) {
      return _printDirective((ContractArrayDirective)d);
    } else if (d instanceof DataLayoutDirective) {
      return _printDirective((DataLayoutDirective)d);
    } else if (d instanceof ScheduleDirective) {
      return _printDirective((ScheduleDirective)d);
    } else if (d != null) {
      return _printDirective(d);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(d).toString());
    }
  }
}
