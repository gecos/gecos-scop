@GenModel(
    importerID="org.eclipse.emf.importer.ecore"
    , operationReflection="false"
//  , editDirectory="/org.polymodel.algebra.edit/src"
//  , editorDirectory="/org.polymodel.algebra.editor/src"
//  , editorPluginID="org.polymodel.algebra.editor"
)
@Ecore(nsURI="http://polymodel/algebra/reductions")
package org.polymodel.algebra.reductions

import org.polymodel.algebra.IntExpression
import org.polymodel.algebra.AlgebraVisitor
import org.polymodel.algebra.AlgebraVisitable
import org.polymodel.algebra.OUTPUT_FORMAT
import org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter
import java.util.ArrayList
import org.polymodel.algebra.Variable

@GenModel(image="false")
class ReductionExpression extends IntExpression , AlgebraVisitable {
	contains IntExpression[] expressions
	ReductionOperator[1] operator
	
	op void accept(AlgebraVisitor visitor) {
		visitor.visitReductionExpression(this)
	}
	
	op String toString() {
		operator.toString() + getExpressions()
	}
	
	op String toString(OUTPUT_FORMAT format) {
		AlgebraPrettyPrinter.print(this, format)
	}
	
	op IntExpression substitute(Variable substituted, IntExpression newExpr) {
		val newExprs = new ArrayList<IntExpression>(getExpressions().size());
		for (IntExpression expr : getExpressions()) {
			newExprs.add(expr.substitute(substituted, newExpr));
		}
		val copy = this.<ReductionExpression>copy
		copy.getExpressions().clear();
		copy.getExpressions().addAll(newExprs);
		return copy;
	}
	
	op IntExpression substitute(Variable substituted, Variable newVar) {
		val newExprs = new ArrayList<IntExpression>(getExpressions().size());
		for (IntExpression expr : getExpressions()) {
			newExprs.add(expr.substitute(substituted, newVar));
		}
		val copy = this.<ReductionExpression>copy
		copy.getExpressions().clear();
		copy.getExpressions().addAll(newExprs);
		return copy;
	}
}

enum ReductionOperator {
	MIN
	MAX = 1
	SUM = 2
	PROD = 3
}