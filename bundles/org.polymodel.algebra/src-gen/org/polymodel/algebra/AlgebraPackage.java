/**
 */
package org.polymodel.algebra;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polymodel.algebra.AlgebraFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='org.polymodel'"
 * @generated
 */
public interface AlgebraPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "algebra";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://polymodel/algebra";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "algebra";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AlgebraPackage eINSTANCE = org.polymodel.algebra.impl.AlgebraPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.AlgebraVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.AlgebraVisitable
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getAlgebraVisitable()
	 * @generated
	 */
	int ALGEBRA_VISITABLE = 0;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALGEBRA_VISITABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.IntExpressionImpl <em>Int Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.IntExpressionImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntExpression()
	 * @generated
	 */
	int INT_EXPRESSION = 1;

	/**
	 * The number of structural features of the '<em>Int Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_EXPRESSION_FEATURE_COUNT = ALGEBRA_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.IntTermImpl <em>Int Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.IntTermImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntTerm()
	 * @generated
	 */
	int INT_TERM = 2;

	/**
	 * The feature id for the '<em><b>Coef</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_TERM__COEF = ALGEBRA_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_TERM_FEATURE_COUNT = ALGEBRA_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.VariableImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = ALGEBRA_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = ALGEBRA_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.IntConstraintImpl <em>Int Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.IntConstraintImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntConstraint()
	 * @generated
	 */
	int INT_CONSTRAINT = 4;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT__LHS = ALGEBRA_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT__RHS = ALGEBRA_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comparison Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT__COMPARISON_OPERATOR = ALGEBRA_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Int Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT_FEATURE_COUNT = ALGEBRA_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.IntConstraintSystemImpl <em>Int Constraint System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.IntConstraintSystemImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntConstraintSystem()
	 * @generated
	 */
	int INT_CONSTRAINT_SYSTEM = 5;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT_SYSTEM__CONSTRAINTS = ALGEBRA_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Constraint System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_CONSTRAINT_SYSTEM_FEATURE_COUNT = ALGEBRA_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.AlgebraVisitorImpl <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.AlgebraVisitorImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getAlgebraVisitor()
	 * @generated
	 */
	int ALGEBRA_VISITOR = 6;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALGEBRA_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.CompositeIntExpressionImpl <em>Composite Int Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.CompositeIntExpressionImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getCompositeIntExpression()
	 * @generated
	 */
	int COMPOSITE_INT_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INT_EXPRESSION__LEFT = INT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INT_EXPRESSION__RIGHT = INT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INT_EXPRESSION__OPERATOR = INT_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Composite Int Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INT_EXPRESSION_FEATURE_COUNT = INT_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.impl.SelectExpressionImpl <em>Select Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.SelectExpressionImpl
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getSelectExpression()
	 * @generated
	 */
	int SELECT_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT_EXPRESSION__CONDITION = INT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT_EXPRESSION__THEN = INT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT_EXPRESSION__ELSE = INT_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Select Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT_EXPRESSION_FEATURE_COUNT = INT_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.FuzzyBoolean <em>Fuzzy Boolean</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.FuzzyBoolean
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getFuzzyBoolean()
	 * @generated
	 */
	int FUZZY_BOOLEAN = 9;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.OUTPUT_FORMAT <em>OUTPUT FORMAT</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.OUTPUT_FORMAT
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getOUTPUT_FORMAT()
	 * @generated
	 */
	int OUTPUT_FORMAT = 10;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.ComparisonOperator <em>Comparison Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.ComparisonOperator
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getComparisonOperator()
	 * @generated
	 */
	int COMPARISON_OPERATOR = 11;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.CompositeOperator <em>Composite Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.CompositeOperator
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getCompositeOperator()
	 * @generated
	 */
	int COMPOSITE_OPERATOR = 12;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getString()
	 * @generated
	 */
	int STRING = 13;

	/**
	 * The meta object id for the '<em>long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getlong()
	 * @generated
	 */
	int LONG = 14;

	/**
	 * The meta object id for the '<em>Value</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 15;


	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.AlgebraVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see org.polymodel.algebra.AlgebraVisitable
	 * @generated
	 */
	EClass getAlgebraVisitable();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.IntExpression <em>Int Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Expression</em>'.
	 * @see org.polymodel.algebra.IntExpression
	 * @generated
	 */
	EClass getIntExpression();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.IntTerm <em>Int Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Term</em>'.
	 * @see org.polymodel.algebra.IntTerm
	 * @generated
	 */
	EClass getIntTerm();

	/**
	 * Returns the meta object for the attribute '{@link org.polymodel.algebra.IntTerm#getCoef <em>Coef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coef</em>'.
	 * @see org.polymodel.algebra.IntTerm#getCoef()
	 * @see #getIntTerm()
	 * @generated
	 */
	EAttribute getIntTerm_Coef();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.polymodel.algebra.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.polymodel.algebra.Variable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.polymodel.algebra.Variable#getName()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Name();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.IntConstraint <em>Int Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Constraint</em>'.
	 * @see org.polymodel.algebra.IntConstraint
	 * @generated
	 */
	EClass getIntConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.IntConstraint#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see org.polymodel.algebra.IntConstraint#getLhs()
	 * @see #getIntConstraint()
	 * @generated
	 */
	EReference getIntConstraint_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.IntConstraint#getRhs <em>Rhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs</em>'.
	 * @see org.polymodel.algebra.IntConstraint#getRhs()
	 * @see #getIntConstraint()
	 * @generated
	 */
	EReference getIntConstraint_Rhs();

	/**
	 * Returns the meta object for the attribute '{@link org.polymodel.algebra.IntConstraint#getComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comparison Operator</em>'.
	 * @see org.polymodel.algebra.IntConstraint#getComparisonOperator()
	 * @see #getIntConstraint()
	 * @generated
	 */
	EAttribute getIntConstraint_ComparisonOperator();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.IntConstraintSystem <em>Int Constraint System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Constraint System</em>'.
	 * @see org.polymodel.algebra.IntConstraintSystem
	 * @generated
	 */
	EClass getIntConstraintSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link org.polymodel.algebra.IntConstraintSystem#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see org.polymodel.algebra.IntConstraintSystem#getConstraints()
	 * @see #getIntConstraintSystem()
	 * @generated
	 */
	EReference getIntConstraintSystem_Constraints();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.AlgebraVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see org.polymodel.algebra.AlgebraVisitor
	 * @generated
	 */
	EClass getAlgebraVisitor();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.CompositeIntExpression <em>Composite Int Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Int Expression</em>'.
	 * @see org.polymodel.algebra.CompositeIntExpression
	 * @generated
	 */
	EClass getCompositeIntExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.CompositeIntExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see org.polymodel.algebra.CompositeIntExpression#getLeft()
	 * @see #getCompositeIntExpression()
	 * @generated
	 */
	EReference getCompositeIntExpression_Left();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.CompositeIntExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see org.polymodel.algebra.CompositeIntExpression#getRight()
	 * @see #getCompositeIntExpression()
	 * @generated
	 */
	EReference getCompositeIntExpression_Right();

	/**
	 * Returns the meta object for the attribute '{@link org.polymodel.algebra.CompositeIntExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see org.polymodel.algebra.CompositeIntExpression#getOperator()
	 * @see #getCompositeIntExpression()
	 * @generated
	 */
	EAttribute getCompositeIntExpression_Operator();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.SelectExpression <em>Select Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Select Expression</em>'.
	 * @see org.polymodel.algebra.SelectExpression
	 * @generated
	 */
	EClass getSelectExpression();

	/**
	 * Returns the meta object for the reference list '{@link org.polymodel.algebra.SelectExpression#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Condition</em>'.
	 * @see org.polymodel.algebra.SelectExpression#getCondition()
	 * @see #getSelectExpression()
	 * @generated
	 */
	EReference getSelectExpression_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.SelectExpression#getThen <em>Then</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then</em>'.
	 * @see org.polymodel.algebra.SelectExpression#getThen()
	 * @see #getSelectExpression()
	 * @generated
	 */
	EReference getSelectExpression_Then();

	/**
	 * Returns the meta object for the containment reference '{@link org.polymodel.algebra.SelectExpression#getElse <em>Else</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else</em>'.
	 * @see org.polymodel.algebra.SelectExpression#getElse()
	 * @see #getSelectExpression()
	 * @generated
	 */
	EReference getSelectExpression_Else();

	/**
	 * Returns the meta object for enum '{@link org.polymodel.algebra.FuzzyBoolean <em>Fuzzy Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Fuzzy Boolean</em>'.
	 * @see org.polymodel.algebra.FuzzyBoolean
	 * @generated
	 */
	EEnum getFuzzyBoolean();

	/**
	 * Returns the meta object for enum '{@link org.polymodel.algebra.OUTPUT_FORMAT <em>OUTPUT FORMAT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>OUTPUT FORMAT</em>'.
	 * @see org.polymodel.algebra.OUTPUT_FORMAT
	 * @generated
	 */
	EEnum getOUTPUT_FORMAT();

	/**
	 * Returns the meta object for enum '{@link org.polymodel.algebra.ComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator</em>'.
	 * @see org.polymodel.algebra.ComparisonOperator
	 * @generated
	 */
	EEnum getComparisonOperator();

	/**
	 * Returns the meta object for enum '{@link org.polymodel.algebra.CompositeOperator <em>Composite Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Composite Operator</em>'.
	 * @see org.polymodel.algebra.CompositeOperator
	 * @generated
	 */
	EEnum getCompositeOperator();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>long</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>long</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getlong();

	/**
	 * Returns the meta object for data type '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Value</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AlgebraFactory getAlgebraFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.AlgebraVisitable <em>Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.AlgebraVisitable
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getAlgebraVisitable()
		 * @generated
		 */
		EClass ALGEBRA_VISITABLE = eINSTANCE.getAlgebraVisitable();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.IntExpressionImpl <em>Int Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.IntExpressionImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntExpression()
		 * @generated
		 */
		EClass INT_EXPRESSION = eINSTANCE.getIntExpression();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.IntTermImpl <em>Int Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.IntTermImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntTerm()
		 * @generated
		 */
		EClass INT_TERM = eINSTANCE.getIntTerm();

		/**
		 * The meta object literal for the '<em><b>Coef</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_TERM__COEF = eINSTANCE.getIntTerm_Coef();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.VariableImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.IntConstraintImpl <em>Int Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.IntConstraintImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntConstraint()
		 * @generated
		 */
		EClass INT_CONSTRAINT = eINSTANCE.getIntConstraint();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INT_CONSTRAINT__LHS = eINSTANCE.getIntConstraint_Lhs();

		/**
		 * The meta object literal for the '<em><b>Rhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INT_CONSTRAINT__RHS = eINSTANCE.getIntConstraint_Rhs();

		/**
		 * The meta object literal for the '<em><b>Comparison Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_CONSTRAINT__COMPARISON_OPERATOR = eINSTANCE.getIntConstraint_ComparisonOperator();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.IntConstraintSystemImpl <em>Int Constraint System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.IntConstraintSystemImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getIntConstraintSystem()
		 * @generated
		 */
		EClass INT_CONSTRAINT_SYSTEM = eINSTANCE.getIntConstraintSystem();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INT_CONSTRAINT_SYSTEM__CONSTRAINTS = eINSTANCE.getIntConstraintSystem_Constraints();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.AlgebraVisitorImpl <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.AlgebraVisitorImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getAlgebraVisitor()
		 * @generated
		 */
		EClass ALGEBRA_VISITOR = eINSTANCE.getAlgebraVisitor();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.CompositeIntExpressionImpl <em>Composite Int Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.CompositeIntExpressionImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getCompositeIntExpression()
		 * @generated
		 */
		EClass COMPOSITE_INT_EXPRESSION = eINSTANCE.getCompositeIntExpression();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_INT_EXPRESSION__LEFT = eINSTANCE.getCompositeIntExpression_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_INT_EXPRESSION__RIGHT = eINSTANCE.getCompositeIntExpression_Right();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_INT_EXPRESSION__OPERATOR = eINSTANCE.getCompositeIntExpression_Operator();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.impl.SelectExpressionImpl <em>Select Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.SelectExpressionImpl
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getSelectExpression()
		 * @generated
		 */
		EClass SELECT_EXPRESSION = eINSTANCE.getSelectExpression();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELECT_EXPRESSION__CONDITION = eINSTANCE.getSelectExpression_Condition();

		/**
		 * The meta object literal for the '<em><b>Then</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELECT_EXPRESSION__THEN = eINSTANCE.getSelectExpression_Then();

		/**
		 * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELECT_EXPRESSION__ELSE = eINSTANCE.getSelectExpression_Else();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.FuzzyBoolean <em>Fuzzy Boolean</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.FuzzyBoolean
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getFuzzyBoolean()
		 * @generated
		 */
		EEnum FUZZY_BOOLEAN = eINSTANCE.getFuzzyBoolean();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.OUTPUT_FORMAT <em>OUTPUT FORMAT</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.OUTPUT_FORMAT
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getOUTPUT_FORMAT()
		 * @generated
		 */
		EEnum OUTPUT_FORMAT = eINSTANCE.getOUTPUT_FORMAT();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.ComparisonOperator <em>Comparison Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.ComparisonOperator
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getComparisonOperator()
		 * @generated
		 */
		EEnum COMPARISON_OPERATOR = eINSTANCE.getComparisonOperator();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.CompositeOperator <em>Composite Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.CompositeOperator
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getCompositeOperator()
		 * @generated
		 */
		EEnum COMPOSITE_OPERATOR = eINSTANCE.getCompositeOperator();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>long</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getlong()
		 * @generated
		 */
		EDataType LONG = eINSTANCE.getlong();

		/**
		 * The meta object literal for the '<em>Value</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.impl.AlgebraPackageImpl#getValue()
		 * @generated
		 */
		EDataType VALUE = eINSTANCE.getValue();

	}

} //AlgebraPackage
