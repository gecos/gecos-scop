/**
 */
package org.polymodel.algebra;

import org.eclipse.emf.ecore.EObject;

import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffineTerm;

import org.polymodel.algebra.polynomials.PolynomialExpression;
import org.polymodel.algebra.polynomials.PolynomialTerm;
import org.polymodel.algebra.polynomials.PolynomialVariable;

import org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineTerm;
import org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm;

import org.polymodel.algebra.reductions.ReductionExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polymodel.algebra.AlgebraPackage#getAlgebraVisitor()
 * @model abstract="true"
 * @generated
 */
public interface AlgebraVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitIntTerm(IntTerm i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model vUnique="false"
	 * @generated
	 */
	void visitVariable(Variable v);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='i.getLhs().accept(this);\ni.getRhs().accept(this);'"
	 * @generated
	 */
	void visitIntConstraint(IntConstraint i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _constraints = i.getConstraints();\nfor (final &lt;%org.polymodel.algebra.IntConstraint%&gt; o : _constraints)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitIntConstraintSystem(IntConstraintSystem i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms = a.getTerms();\nfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; o : _terms)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitAffineExpression(AffineExpression a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitAffineTerm(AffineTerm a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt; _terms = q.getTerms();\nfor (final &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt; o : _terms)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitQuasiAffineExpression(QuasiAffineExpression q);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;();'"
	 * @generated
	 */
	void visitQuasiAffineTerm(QuasiAffineTerm q);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='q.getExpression().accept(this);'"
	 * @generated
	 */
	void visitSimpleQuasiAffineTerm(SimpleQuasiAffineTerm q);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='q.getExpression().accept(this);'"
	 * @generated
	 */
	void visitNestedQuasiAffineTerm(NestedQuasiAffineTerm q);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;&gt; _terms = p.getTerms();\nfor (final &lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt; o : _terms)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitPolynomialExpression(PolynomialExpression p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt;&gt; _variables = p.getVariables();\nfor (final &lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt; o : _variables)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitPolynomialTerm(PolynomialTerm p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pUnique="false"
	 * @generated
	 */
	void visitPolynomialVariable(PolynomialVariable p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; _expressions = r.getExpressions();\nfor (final &lt;%org.polymodel.algebra.IntExpression%&gt; o : _expressions)\n{\n\to.accept(this);\n}'"
	 * @generated
	 */
	void visitReductionExpression(ReductionExpression r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ieUnique="false"
	 * @generated
	 */
	void visitIntExpression(IntExpression ie);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='r.getLeft().accept(this);\nr.getRight().accept(this);'"
	 * @generated
	 */
	void visitCompositeIntExpression(CompositeIntExpression r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ieUnique="false"
	 * @generated
	 */
	void visitSelectExpression(SelectExpression ie);

} // AlgebraVisitor
