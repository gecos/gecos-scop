/**
 */
package org.polymodel.algebra;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Int Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.CompositeIntExpression#getLeft <em>Left</em>}</li>
 *   <li>{@link org.polymodel.algebra.CompositeIntExpression#getRight <em>Right</em>}</li>
 *   <li>{@link org.polymodel.algebra.CompositeIntExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getCompositeIntExpression()
 * @model
 * @generated
 */
public interface CompositeIntExpression extends IntExpression {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getCompositeIntExpression_Left()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntExpression getLeft();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.CompositeIntExpression#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getCompositeIntExpression_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntExpression getRight();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.CompositeIntExpression#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polymodel.algebra.CompositeOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see org.polymodel.algebra.CompositeOperator
	 * @see #setOperator(CompositeOperator)
	 * @see org.polymodel.algebra.AlgebraPackage#getCompositeIntExpression_Operator()
	 * @model unique="false" required="true"
	 * @generated
	 */
	CompositeOperator getOperator();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.CompositeIntExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see org.polymodel.algebra.CompositeOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(CompositeOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCompositeIntExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntExpression%&gt; _copy = this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy();\nfinal &lt;%org.polymodel.algebra.CompositeIntExpression%&gt; cp = ((&lt;%org.polymodel.algebra.CompositeIntExpression%&gt;) _copy);\ncp.setRight(cp.getRight().substitute(substituted, newExpr));\ncp.setLeft(cp.getLeft().substitute(substituted, newExpr));\nreturn cp;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntExpression%&gt; _copy = this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy();\nfinal &lt;%org.polymodel.algebra.CompositeIntExpression%&gt; cp = ((&lt;%org.polymodel.algebra.CompositeIntExpression%&gt;) _copy);\ncp.setRight(cp.getRight().substitute(substituted, newVar));\ncp.setLeft(cp.getLeft().substitute(substituted, newVar));\nreturn cp;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.CompositeOperator%&gt; _operator = this.getOperator();\n&lt;%java.lang.String%&gt; _plus = (_operator + \"(\");\n&lt;%org.polymodel.algebra.IntExpression%&gt; _left = this.getLeft();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _left);\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \",\");\n&lt;%org.polymodel.algebra.IntExpression%&gt; _right = this.getRight();\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + _right);\nreturn (_plus_3 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

} // CompositeIntExpression
