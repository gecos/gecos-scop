/**
 */
package org.polymodel.algebra;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.IntConstraint#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.polymodel.algebra.IntConstraint#getRhs <em>Rhs</em>}</li>
 *   <li>{@link org.polymodel.algebra.IntConstraint#getComparisonOperator <em>Comparison Operator</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraint()
 * @model
 * @generated
 */
public interface IntConstraint extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraint_Lhs()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getLhs();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.IntConstraint#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs</em>' containment reference.
	 * @see #setRhs(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraint_Rhs()
	 * @model containment="true"
	 * @generated
	 */
	IntExpression getRhs();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.IntConstraint#getRhs <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs</em>' containment reference.
	 * @see #getRhs()
	 * @generated
	 */
	void setRhs(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Comparison Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polymodel.algebra.ComparisonOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison Operator</em>' attribute.
	 * @see org.polymodel.algebra.ComparisonOperator
	 * @see #setComparisonOperator(ComparisonOperator)
	 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraint_ComparisonOperator()
	 * @model unique="false"
	 * @generated
	 */
	ComparisonOperator getComparisonOperator();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.IntConstraint#getComparisonOperator <em>Comparison Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison Operator</em>' attribute.
	 * @see org.polymodel.algebra.ComparisonOperator
	 * @see #getComparisonOperator()
	 * @generated
	 */
	void setComparisonOperator(ComparisonOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getRhs() instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;) &amp;&amp; (this.getLhs() instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;));'"
	 * @generated
	 */
	boolean isAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getRhs() instanceof &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;) &amp;&amp; (this.getLhs() instanceof &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;));'"
	 * @generated
	 */
	boolean isQuasiAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getRhs() instanceof &lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt;) &amp;&amp; (this.getLhs() instanceof &lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt;));'"
	 * @generated
	 */
	boolean isPolynomial();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIntConstraint(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntExpression%&gt; lhs = this.getLhs().substitute(substituted, newVar);\nfinal &lt;%org.polymodel.algebra.IntExpression%&gt; rhs2 = this.getRhs();\nfinal &lt;%org.polymodel.algebra.IntExpression%&gt; rhs = rhs2.substitute(substituted, newVar);\nreturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraint(lhs, rhs, this.getComparisonOperator());'"
	 * @generated
	 */
	IntConstraint substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraint(this.getLhs().substitute(substituted, newExpr), \n\tthis.getRhs().substitute(substituted, newExpr), this.getComparisonOperator());'"
	 * @generated
	 */
	IntConstraint substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; stringBuffer = new &lt;%java.lang.StringBuffer%&gt;();\nstringBuffer.append(this.getLhs().toString());\n&lt;%java.lang.String%&gt; _switchResult = null;\n&lt;%org.polymodel.algebra.ComparisonOperator%&gt; _comparisonOperator = this.getComparisonOperator();\nif (_comparisonOperator != null)\n{\n\tswitch (_comparisonOperator)\n\t{\n\t\tcase EQ:\n\t\t\t_switchResult = \" == \";\n\t\t\tbreak;\n\t\tcase GE:\n\t\t\t_switchResult = \" &gt;= \";\n\t\t\tbreak;\n\t\tcase GT:\n\t\t\t_switchResult = \" &gt; \";\n\t\t\tbreak;\n\t\tcase LE:\n\t\t\t_switchResult = \" &lt;= \";\n\t\t\tbreak;\n\t\tcase LT:\n\t\t\t_switchResult = \" &lt; \";\n\t\t\tbreak;\n\t\tcase NE:\n\t\t\t_switchResult = \" != \";\n\t\t\tbreak;\n\t\tdefault:\n\t\t\tbreak;\n\t}\n}\nfinal &lt;%java.lang.String%&gt; cop = _switchResult;\nstringBuffer.append(cop);\nstringBuffer.append(this.getRhs().toString());\nreturn stringBuffer.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.ecore.util.EcoreUtil.Copier%&gt; copier = new &lt;%org.eclipse.emf.ecore.util.EcoreUtil.Copier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = copier.copy(this);\nfinal &lt;%org.polymodel.algebra.IntConstraint%&gt; res = ((&lt;%org.polymodel.algebra.IntConstraint%&gt;) _copy);\ncopier.copyReferences();\nreturn res;'"
	 * @generated
	 */
	IntConstraint copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraint(this.getLhs().simplify(), this.getRhs().simplify(), this.getComparisonOperator());'"
	 * @generated
	 */
	IntConstraint simplify();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Current implementation check if LHS==LHS and RHS == RHS and OP == OP and nothing else.
	 * <!-- end-model-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.FuzzyBoolean%&gt; lhsEquivalency = &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.MAYBE;\n&lt;%org.polymodel.algebra.FuzzyBoolean%&gt; rhsEquivalency = &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.MAYBE;\nfinal &lt;%org.polymodel.algebra.IntExpression%&gt; olhs = other.getLhs().simplify();\nfinal &lt;%org.polymodel.algebra.IntExpression%&gt; orhs = other.getRhs().simplify();\n&lt;%org.polymodel.algebra.ComparisonOperator%&gt; _comparisonOperator = this.getComparisonOperator();\n&lt;%org.polymodel.algebra.ComparisonOperator%&gt; _comparisonOperator_1 = other.getComparisonOperator();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_comparisonOperator, _comparisonOperator_1);\nif (_equals)\n{\n\tlhsEquivalency = this.getLhs().simplify().isEquivalent(olhs);\n\trhsEquivalency = this.getRhs().simplify().isEquivalent(orhs);\n}\nelse\n{\n\t&lt;%org.polymodel.algebra.ComparisonOperator%&gt; _comparisonOperator_2 = this.getComparisonOperator();\n\t&lt;%org.polymodel.algebra.ComparisonOperator%&gt; _opposite = &lt;%org.polymodel.algebra.util.ComparisonOperatorUtils%&gt;.getOpposite(other.getComparisonOperator());\n\tboolean _equals_1 = &lt;%com.google.common.base.Objects%&gt;.equal(_comparisonOperator_2, _opposite);\n\tif (_equals_1)\n\t{\n\t\tlhsEquivalency = this.getLhs().simplify().isEquivalent(orhs);\n\t\trhsEquivalency = this.getRhs().simplify().isEquivalent(olhs);\n\t}\n\telse\n\t{\n\t\tlhsEquivalency = null;\n\t\trhsEquivalency = null;\n\t}\n}\nif (((lhsEquivalency != null) &amp;&amp; (rhsEquivalency != null)))\n{\n\tboolean _equals_2 = &lt;%com.google.common.base.Objects%&gt;.equal(lhsEquivalency, &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.YES);\n\tif (_equals_2)\n\t{\n\t\treturn rhsEquivalency;\n\t}\n\telse\n\t{\n\t\tboolean _equals_3 = &lt;%com.google.common.base.Objects%&gt;.equal(lhsEquivalency, &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.MAYBE);\n\t\tif (_equals_3)\n\t\t{\n\t\t\tif ((&lt;%com.google.common.base.Objects%&gt;.equal(rhsEquivalency, &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.YES) || &lt;%com.google.common.base.Objects%&gt;.equal(rhsEquivalency, &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.MAYBE)))\n\t\t\t{\n\t\t\t\treturn &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.MAYBE;\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\treturn &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.NO;\n\t\t\t}\n\t\t}\n\t}\n}\nreturn &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.NO;'"
	 * @generated
	 */
	FuzzyBoolean isEquivalent(IntConstraint other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The semantics of this function is not very well defined, and more
	 * importantly its scope of application is somewhat unclear.
	 * 
	 * @deprecated
	 * <!-- end-model-doc -->
	 * @model unique="false" varUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntConstraint%&gt; constraint = &lt;%org.polymodel.algebra.tom.Isolate%&gt;.isolateToLhs(this, var);\nif ((constraint == null))\n{\n\treturn null;\n}\nfinal &lt;%org.polymodel.algebra.ComparisonOperator%&gt; cop = constraint.getComparisonOperator();\nif ((&lt;%com.google.common.base.Objects%&gt;.equal(cop, &lt;%org.polymodel.algebra.ComparisonOperator%&gt;.LE) || &lt;%com.google.common.base.Objects%&gt;.equal(cop, &lt;%org.polymodel.algebra.ComparisonOperator%&gt;.EQ)))\n{\n\treturn constraint.getRhs();\n}\nelse\n{\n\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(cop, &lt;%org.polymodel.algebra.ComparisonOperator%&gt;.LT);\n\tif (_equals)\n\t{\n\t\treturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.add(constraint.getRhs(), (-1));\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	IntExpression getUB(Variable var);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" varUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return null;'"
	 * @generated
	 */
	IntExpression getLB(Variable var);

} // IntConstraint
