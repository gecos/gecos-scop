/**
 */
package org.polymodel.algebra;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Constraint System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.IntConstraintSystem#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraintSystem()
 * @model
 * @generated
 */
public interface IntConstraintSystem extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see org.polymodel.algebra.AlgebraPackage#getIntConstraintSystem_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntConstraint> getConstraints();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIntConstraintSystem(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.getConstraints(), \" and \");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntConstraintSystem%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;()\n\t{\n\t\tpublic &lt;%org.polymodel.algebra.IntConstraint%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraint%&gt; it)\n\t\t{\n\t\t\treturn it.substitute(substituted, newVar);\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; cl = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;map(this.getConstraints(), _function)));\n\t_xblockexpression = &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraintSystem(cl);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	IntConstraintSystem substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntConstraintSystem%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;()\n\t{\n\t\tpublic &lt;%org.polymodel.algebra.IntConstraint%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraint%&gt; it)\n\t\t{\n\t\t\treturn it.substitute(substituted, newExpr);\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; cl = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;map(this.getConstraints(), _function)));\n\t_xblockexpression = &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraintSystem(cl);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	IntConstraintSystem substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;()\n{\n\tpublic &lt;%org.polymodel.algebra.IntConstraint%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraint%&gt; it)\n\t{\n\t\treturn it.simplify();\n\t}\n};\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; cl = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;map(this.getConstraints(), _function)));\nfinal &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; redundant = new &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;();\nint _size = cl.size();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _size, true);\nfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n{\n\tint _size_1 = cl.size();\n\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan_1 = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(((i).intValue() + 1), _size_1, true);\n\tfor (final &lt;%java.lang.Integer%&gt; j : _doubleDotLessThan_1)\n\t{\n\t\t{\n\t\t\tfinal &lt;%org.polymodel.algebra.IntConstraint%&gt; c1 = cl.get((i).intValue());\n\t\t\tfinal &lt;%org.polymodel.algebra.IntConstraint%&gt; c2 = cl.get((j).intValue());\n\t\t\t&lt;%org.polymodel.algebra.FuzzyBoolean%&gt; _isEquivalent = c1.isEquivalent(c2);\n\t\t\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_isEquivalent, &lt;%org.polymodel.algebra.FuzzyBoolean%&gt;.YES);\n\t\t\tif (_equals)\n\t\t\t{\n\t\t\t\tboolean _not = (!(redundant.contains(c1) || redundant.contains(c2)));\n\t\t\t\tif (_not)\n\t\t\t\t{\n\t\t\t\t\tredundant.add(c1);\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n}\nthis.getConstraints().removeAll(redundant);\nreturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraintSystem(this.getConstraints());'"
	 * @generated
	 */
	IntConstraintSystem simplify();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntConstraintSystem%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;()\n\t{\n\t\tpublic &lt;%org.polymodel.algebra.IntConstraint%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraint%&gt; it)\n\t\t{\n\t\t\treturn it.copy();\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; cl = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;, &lt;%org.polymodel.algebra.IntConstraint%&gt;&gt;map(this.getConstraints(), _function)));\n\t_xblockexpression = &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constraintSystem(cl);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	IntConstraintSystem copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" varUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; ubs = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _constraints = this.getConstraints();\nfor (final &lt;%org.polymodel.algebra.IntConstraint%&gt; ic : _constraints)\n{\n\t{\n\t\tfinal &lt;%org.polymodel.algebra.IntExpression%&gt; ub = ic.getUB(var);\n\t\tif ((ub != null))\n\t\t{\n\t\t\tubs.add(ub);\n\t\t}\n\t}\n}\nint _size = ubs.size();\nboolean _equals = (_size == 0);\nif (_equals)\n{\n\treturn null;\n}\nelse\n{\n\tint _size_1 = ubs.size();\n\tboolean _equals_1 = (_size_1 == 1);\n\tif (_equals_1)\n\t{\n\t\treturn ubs.get(0);\n\t}\n}\nreturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.max(ubs);'"
	 * @generated
	 */
	IntExpression getUB(Variable var);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" varUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; lbs = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraint%&gt;&gt; _constraints = this.getConstraints();\nfor (final &lt;%org.polymodel.algebra.IntConstraint%&gt; ic : _constraints)\n{\n\t{\n\t\tfinal &lt;%org.polymodel.algebra.IntExpression%&gt; lb = ic.getLB(var);\n\t\tif ((lb != null))\n\t\t{\n\t\t\tlbs.add(lb);\n\t\t}\n\t}\n}\nint _size = lbs.size();\nboolean _equals = (_size == 0);\nif (_equals)\n{\n\treturn null;\n}\nelse\n{\n\tint _size_1 = lbs.size();\n\tboolean _equals_1 = (_size_1 == 1);\n\tif (_equals_1)\n\t{\n\t\treturn lbs.get(0);\n\t}\n}\nreturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.min(lbs);'"
	 * @generated
	 */
	IntExpression getLB(Variable var);

} // IntConstraintSystem
