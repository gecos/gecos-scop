/**
 */
package org.polymodel.algebra;

import org.polymodel.algebra.affine.AffineExpression;

import org.polymodel.algebra.polynomials.PolynomialExpression;

import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;

import org.polymodel.algebra.reductions.ReductionExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polymodel.algebra.AlgebraPackage#getIntExpression()
 * @model abstract="true"
 * @generated
 */
public interface IntExpression extends AlgebraVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.tom.EquivalentIntExpression%&gt;.isEquivalent(this.simplify(), other.simplify());'"
	 * @generated
	 */
	FuzzyBoolean isEquivalent(IntExpression other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.tom.ConstantIntExpression%&gt;.isConstant(this.simplify());'"
	 * @generated
	 */
	FuzzyBoolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.tom.ZeroIntExpression%&gt;.isZero(this.simplify());'"
	 * @generated
	 */
	FuzzyBoolean isZero();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.tom.SimplifyIntExpression%&gt;.simplify(this);'"
	 * @generated
	 */
	IntExpression simplify();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (((((((\"Cannot (yet) substitute \" + substituted) + \" by \") + newVar) + \" in \") + this) + \" (of type \") + _simpleName);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")\");\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" substitutedRequired="true" newExprUnique="false" newExprRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (((((((\"Cannot (yet) substitute \" + substituted) + \" by \") + newExpr) + \" in \") + this) + \" (of type \") + _simpleName);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")\");\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntExpression%&gt; res = &lt;%org.polymodel.algebra.tom.SimplifyIntExpression%&gt;.simplify(this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy());\nif ((res instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;))\n{\n\treturn ((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;)res);\n}\nreturn null;'"
	 * @generated
	 */
	AffineExpression toAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntExpression%&gt; res = this.simplify();\nif ((res instanceof &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;))\n{\n\treturn ((&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;)res);\n}\nreturn null;'"
	 * @generated
	 */
	QuasiAffineExpression toQuasiAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntExpression%&gt; res = this.simplify();\nif ((res instanceof &lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt;))\n{\n\treturn ((&lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt;)res);\n}\nreturn null;'"
	 * @generated
	 */
	PolynomialExpression toPolynomial();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.IntExpression%&gt; res = this.simplify();\nif ((res instanceof &lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt;))\n{\n\treturn ((&lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt;)res);\n}\nreturn null;'"
	 * @generated
	 */
	ReductionExpression toReduction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;T&gt;copy(((T) this));'"
	 * @generated
	 */
	<T extends IntExpression> T copy();

} // IntExpression
