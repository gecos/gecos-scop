/**
 */
package org.polymodel.algebra;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.IntTerm#getCoef <em>Coef</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getIntTerm()
 * @model
 * @generated
 */
public interface IntTerm extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Coef</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coef</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coef</em>' attribute.
	 * @see #setCoef(long)
	 * @see org.polymodel.algebra.AlgebraPackage#getIntTerm_Coef()
	 * @model unique="false" dataType="org.polymodel.algebra.long"
	 * @generated
	 */
	long getCoef();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.IntTerm#getCoef <em>Coef</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coef</em>' attribute.
	 * @see #getCoef()
	 * @generated
	 */
	void setCoef(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIntTerm(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 * @generated
	 */
	boolean isEquivalent(IntTerm other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" oUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='long _coef = this.getCoef();\nlong _coef_1 = o.getCoef();\nlong _minus = (_coef - _coef_1);\nreturn ((int) _minus);'"
	 * @generated
	 */
	int compareTo(IntTerm o);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

} // IntTerm
