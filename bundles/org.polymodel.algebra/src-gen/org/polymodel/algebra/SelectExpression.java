/**
 */
package org.polymodel.algebra;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.SelectExpression#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.polymodel.algebra.SelectExpression#getThen <em>Then</em>}</li>
 *   <li>{@link org.polymodel.algebra.SelectExpression#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getSelectExpression()
 * @model
 * @generated
 */
public interface SelectExpression extends IntExpression {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntConstraintSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' reference list.
	 * @see org.polymodel.algebra.AlgebraPackage#getSelectExpression_Condition()
	 * @model
	 * @generated
	 */
	EList<IntConstraintSystem> getCondition();

	/**
	 * Returns the value of the '<em><b>Then</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then</em>' containment reference.
	 * @see #setThen(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getSelectExpression_Then()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntExpression getThen();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.SelectExpression#getThen <em>Then</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then</em>' containment reference.
	 * @see #getThen()
	 * @generated
	 */
	void setThen(IntExpression value);

	/**
	 * Returns the value of the '<em><b>Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else</em>' containment reference.
	 * @see #setElse(IntExpression)
	 * @see org.polymodel.algebra.AlgebraPackage#getSelectExpression_Else()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntExpression getElse();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.SelectExpression#getElse <em>Else</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else</em>' containment reference.
	 * @see #getElse()
	 * @generated
	 */
	void setElse(IntExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt; _condition = this.getCondition();\n&lt;%java.lang.String%&gt; _plus = (\"Select(\" + _condition);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \", \");\n&lt;%org.polymodel.algebra.IntExpression%&gt; _then = this.getThen();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _then);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \", \");\n&lt;%org.polymodel.algebra.IntExpression%&gt; _else = this.getElse();\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + _else);\nreturn (_plus_4 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;()\n{\n\tpublic &lt;%org.polymodel.algebra.IntConstraintSystem%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraintSystem%&gt; it)\n\t{\n\t\treturn it.substitute(substituted, newExpr);\n\t}\n};\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt; newCond = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;map(this.getCondition(), _function)));\n&lt;%org.polymodel.algebra.IntExpression%&gt; _copy = this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy();\nfinal &lt;%org.polymodel.algebra.SelectExpression%&gt; copy = ((&lt;%org.polymodel.algebra.SelectExpression%&gt;) _copy);\ncopy.getCondition().clear();\ncopy.getCondition().addAll(newCond);\ncopy.setThen(this.getThen().substitute(substituted, newExpr));\ncopy.setElse(this.getElse().substitute(substituted, newExpr));\nreturn copy;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;()\n{\n\tpublic &lt;%org.polymodel.algebra.IntConstraintSystem%&gt; apply(final &lt;%org.polymodel.algebra.IntConstraintSystem%&gt; it)\n\t{\n\t\treturn it.substitute(substituted, newVar);\n\t}\n};\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt; newCond = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.IntConstraintSystem%&gt;, &lt;%org.polymodel.algebra.IntConstraintSystem%&gt;&gt;map(this.getCondition(), _function)));\n&lt;%org.polymodel.algebra.IntExpression%&gt; _copy = this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy();\nfinal &lt;%org.polymodel.algebra.SelectExpression%&gt; copy = ((&lt;%org.polymodel.algebra.SelectExpression%&gt;) _copy);\ncopy.getCondition().clear();\ncopy.getCondition().addAll(newCond);\ncopy.setThen(this.getThen().substitute(substituted, newVar));\ncopy.setElse(this.getElse().substitute(substituted, newVar));\nreturn copy;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSelectExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

} // SelectExpression
