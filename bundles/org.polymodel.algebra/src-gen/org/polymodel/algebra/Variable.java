/**
 */
package org.polymodel.algebra;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.Variable#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.AlgebraPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.polymodel.algebra.AlgebraPackage#getVariable_Name()
	 * @model unique="false" dataType="org.polymodel.algebra.String" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.Variable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitVariable(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getName();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.polymodel.algebra.String" unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

} // Variable
