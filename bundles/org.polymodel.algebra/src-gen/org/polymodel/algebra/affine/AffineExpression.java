/**
 */
package org.polymodel.algebra.affine;

import org.eclipse.emf.common.util.EList;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.affine.AffineExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.affine.AffinePackage#getAffineExpression()
 * @model
 * @generated
 */
public interface AffineExpression extends IntExpression, AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.affine.AffineTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see org.polymodel.algebra.affine.AffinePackage#getAffineExpression_Terms()
	 * @model containment="true"
	 * @generated
	 */
	EList<AffineTerm> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitAffineExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, &lt;%org.polymodel.algebra.OUTPUT_FORMAT%&gt;.ISL);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.affine.AffineExpression%&gt; expr = &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;&gt;copy(this);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms = expr.getTerms();\nfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; term : _terms)\n{\n\tif (((term.getVariable() != null) &amp;&amp; term.getVariable().getName().contentEquals(substituted.getName())))\n\t{\n\t\tterm.setVariable(newVar);\n\t}\n}\nreturn expr;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntExpression%&gt; _copy = this.&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;copy();\nfinal &lt;%org.polymodel.algebra.affine.AffineExpression%&gt; affineExpression = ((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;) _copy);\nif ((newExpr instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;))\n{\n\tif ((substituted == null))\n\t{\n\t\treturn affineExpression;\n\t}\n\tfinal &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; substituteList = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt;();\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms = affineExpression.getTerms();\n\tfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; term : _terms)\n\t{\n\t\tif (((term.getVariable() != null) &amp;&amp; (substituted != null)))\n\t\t{\n\t\t\tboolean _contentEquals = term.getVariable().getName().contentEquals(substituted.getName());\n\t\t\tif (_contentEquals)\n\t\t\t{\n\t\t\t\tsubstituteList.add(term);\n\t\t\t}\n\t\t}\n\t}\n\taffineExpression.getTerms().removeAll(substituteList);\n\tfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; term_1 : substituteList)\n\t{\n\t\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms_1 = ((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;)newExpr).getTerms();\n\t\tfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; newTerm : _terms_1)\n\t\t{\n\t\t\tlong _coef = term_1.getCoef();\n\t\t\tlong _coef_1 = newTerm.getCoef();\n\t\t\tlong _multiply = (_coef * _coef_1);\n\t\t\taffineExpression.getTerms().add(\n\t\t\t\t&lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.term(_multiply, \n\t\t\t\t\tnewTerm.getVariable()));\n\t\t}\n\t}\n\treturn affineExpression;\n}\nelse\n{\n\treturn &lt;%org.polymodel.algebra.tom.Replace%&gt;.replace(affineExpression, substituted, newExpr);\n}'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" vUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms = this.getTerms();\nfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; t : _terms)\n{\n\t&lt;%org.polymodel.algebra.Variable%&gt; _variable = t.getVariable();\n\tboolean _tripleEquals = (_variable == v);\n\tif (_tripleEquals)\n\t{\n\t\treturn t;\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	AffineTerm getTerm(Variable v);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntExpression%&gt; _simplify = this.simplify();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; _terms = ((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;) _simplify).getTerms();\nfor (final &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; t : _terms)\n{\n\t&lt;%org.polymodel.algebra.Variable%&gt; _variable = t.getVariable();\n\tboolean _tripleEquals = (_variable == null);\n\tif (_tripleEquals)\n\t{\n\t\treturn t;\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	AffineTerm getConstantTerm();

} // AffineExpression
