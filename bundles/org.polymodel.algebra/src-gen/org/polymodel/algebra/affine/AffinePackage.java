/**
 */
package org.polymodel.algebra.affine;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polymodel.algebra.AlgebraPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polymodel.algebra.affine.AffineFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='org.polymodel.algebra'"
 * @generated
 */
public interface AffinePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "affine";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://polymodel/algebra/affine";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "affine";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AffinePackage eINSTANCE = org.polymodel.algebra.affine.impl.AffinePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.affine.impl.AffineExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.affine.impl.AffineExpressionImpl
	 * @see org.polymodel.algebra.affine.impl.AffinePackageImpl#getAffineExpression()
	 * @generated
	 */
	int AFFINE_EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_EXPRESSION__TERMS = AlgebraPackage.INT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_EXPRESSION_FEATURE_COUNT = AlgebraPackage.INT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.polymodel.algebra.affine.impl.AffineTermImpl <em>Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polymodel.algebra.affine.impl.AffineTermImpl
	 * @see org.polymodel.algebra.affine.impl.AffinePackageImpl#getAffineTerm()
	 * @generated
	 */
	int AFFINE_TERM = 1;

	/**
	 * The feature id for the '<em><b>Coef</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM__COEF = AlgebraPackage.INT_TERM__COEF;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM__VARIABLE = AlgebraPackage.INT_TERM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFINE_TERM_FEATURE_COUNT = AlgebraPackage.INT_TERM_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.affine.AffineExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see org.polymodel.algebra.affine.AffineExpression
	 * @generated
	 */
	EClass getAffineExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.polymodel.algebra.affine.AffineExpression#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Terms</em>'.
	 * @see org.polymodel.algebra.affine.AffineExpression#getTerms()
	 * @see #getAffineExpression()
	 * @generated
	 */
	EReference getAffineExpression_Terms();

	/**
	 * Returns the meta object for class '{@link org.polymodel.algebra.affine.AffineTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Term</em>'.
	 * @see org.polymodel.algebra.affine.AffineTerm
	 * @generated
	 */
	EClass getAffineTerm();

	/**
	 * Returns the meta object for the reference '{@link org.polymodel.algebra.affine.AffineTerm#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see org.polymodel.algebra.affine.AffineTerm#getVariable()
	 * @see #getAffineTerm()
	 * @generated
	 */
	EReference getAffineTerm_Variable();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AffineFactory getAffineFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.affine.impl.AffineExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.affine.impl.AffineExpressionImpl
		 * @see org.polymodel.algebra.affine.impl.AffinePackageImpl#getAffineExpression()
		 * @generated
		 */
		EClass AFFINE_EXPRESSION = eINSTANCE.getAffineExpression();

		/**
		 * The meta object literal for the '<em><b>Terms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AFFINE_EXPRESSION__TERMS = eINSTANCE.getAffineExpression_Terms();

		/**
		 * The meta object literal for the '{@link org.polymodel.algebra.affine.impl.AffineTermImpl <em>Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polymodel.algebra.affine.impl.AffineTermImpl
		 * @see org.polymodel.algebra.affine.impl.AffinePackageImpl#getAffineTerm()
		 * @generated
		 */
		EClass AFFINE_TERM = eINSTANCE.getAffineTerm();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AFFINE_TERM__VARIABLE = eINSTANCE.getAffineTerm_Variable();

	}

} //AffinePackage
