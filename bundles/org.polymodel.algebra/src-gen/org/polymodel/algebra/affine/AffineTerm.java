/**
 */
package org.polymodel.algebra.affine;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.affine.AffineTerm#getVariable <em>Variable</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.affine.AffinePackage#getAffineTerm()
 * @model
 * @generated
 */
public interface AffineTerm extends IntTerm, AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' reference.
	 * @see #setVariable(Variable)
	 * @see org.polymodel.algebra.affine.AffinePackage#getAffineTerm_Variable()
	 * @model
	 * @generated
	 */
	Variable getVariable();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.affine.AffineTerm#getVariable <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' reference.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(Variable value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitAffineTerm(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.Variable%&gt; _variable = this.getVariable();\nboolean _tripleEquals = (_variable == null);\nif (_tripleEquals)\n{\n\treturn &lt;%java.lang.String%&gt;.valueOf(this.getCoef());\n}\nelse\n{\n\tlong _coef = this.getCoef();\n\tboolean _equals = (_coef == 1);\n\tif (_equals)\n\t{\n\t\treturn this.getVariable().toString();\n\t}\n\telse\n\t{\n\t\tlong _coef_1 = this.getCoef();\n\t\tboolean _equals_1 = (_coef_1 == (-1));\n\t\tif (_equals_1)\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _string = this.getVariable().toString();\n\t\t\treturn (\"-\" + _string);\n\t\t}\n\t\telse\n\t\t{\n\t\t\tlong _coef_2 = this.getCoef();\n\t\t\t&lt;%java.lang.String%&gt; _plus = (&lt;%java.lang.Long%&gt;.valueOf(_coef_2) + \"*\");\n\t\t\t&lt;%java.lang.String%&gt; _string_1 = this.getVariable().toString();\n\t\t\treturn (_plus + _string_1);\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((!(other instanceof &lt;%org.polymodel.algebra.affine.AffineTerm%&gt;)))\n{\n\treturn false;\n}\nfinal &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; otherAffine = ((&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;) other);\nif ((((this.getVariable() == null) &amp;&amp; (otherAffine.getVariable() != null)) || ((this.getVariable() != null) &amp;&amp; (otherAffine.getVariable() == null))))\n{\n\treturn false;\n}\nif (((this.getVariable() != null) &amp;&amp; (otherAffine.getVariable() != null)))\n{\n\tint _compareTo = this.getVariable().getName().compareTo(otherAffine.getVariable().getName());\n\tboolean _notEquals = (_compareTo != 0);\n\tif (_notEquals)\n\t{\n\t\treturn false;\n\t}\n}\nlong _coef = this.getCoef();\nlong _coef_1 = otherAffine.getCoef();\nreturn (_coef == _coef_1);'"
	 * @generated
	 */
	boolean isEquivalent(IntTerm other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" oUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((o instanceof &lt;%org.polymodel.algebra.affine.AffineTerm%&gt;))\n{\n\tfinal &lt;%org.polymodel.algebra.affine.AffineTerm%&gt; affineOther = ((&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;)o);\n\tif (((this.getVariable() == null) &amp;&amp; (affineOther.getVariable() == null)))\n\t{\n\t\treturn super.compareTo(affineOther);\n\t}\n\telse\n\t{\n\t\tif (((this.getVariable() == null) &amp;&amp; (affineOther.getVariable() != null)))\n\t\t{\n\t\t\treturn 1;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tif (((this.getVariable() != null) &amp;&amp; (affineOther.getVariable() == null)))\n\t\t\t{\n\t\t\t\treturn (-1);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tfinal int var = this.getVariable().getName().compareTo(affineOther.getVariable().getName());\n\t\t\t\tif ((var == 0))\n\t\t\t\t{\n\t\t\t\t\treturn super.compareTo(o);\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tif (((this.getCoef() &lt; 0) &amp;&amp; (((&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;)o).getCoef() &gt; 0)))\n\t\t\t\t\t{\n\t\t\t\t\t\treturn 1;\n\t\t\t\t\t}\n\t\t\t\t\treturn var;\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n}\nelse\n{\n\treturn super.compareTo(o);\n}'"
	 * @generated
	 */
	int compareTo(IntTerm o);

} // AffineTerm
