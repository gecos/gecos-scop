/**
 */
package org.polymodel.algebra.affine.impl;

import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffinePackage;
import org.polymodel.algebra.affine.AffineTerm;

import org.polymodel.algebra.factory.IntExpressionBuilder;

import org.polymodel.algebra.impl.IntExpressionImpl;

import org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter;

import org.polymodel.algebra.tom.Replace;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.affine.impl.AffineExpressionImpl#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AffineExpressionImpl extends IntExpressionImpl implements AffineExpression {
	/**
	 * The cached value of the '{@link #getTerms() <em>Terms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerms()
	 * @generated
	 * @ordered
	 */
	protected EList<AffineTerm> terms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AffineExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AffinePackage.Literals.AFFINE_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AffineTerm> getTerms() {
		if (terms == null) {
			terms = new EObjectContainmentEList<AffineTerm>(AffineTerm.class, this, AffinePackage.AFFINE_EXPRESSION__TERMS);
		}
		return terms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitAffineExpression(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return AlgebraPrettyPrinter.print(this, OUTPUT_FORMAT.ISL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final Variable newVar) {
		final AffineExpression expr = EcoreUtil.<AffineExpression>copy(this);
		EList<AffineTerm> _terms = expr.getTerms();
		for (final AffineTerm term : _terms) {
			if (((term.getVariable() != null) && term.getVariable().getName().contentEquals(substituted.getName()))) {
				term.setVariable(newVar);
			}
		}
		return expr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final IntExpression newExpr) {
		IntExpression _copy = this.<IntExpression>copy();
		final AffineExpression affineExpression = ((AffineExpression) _copy);
		if ((newExpr instanceof AffineExpression)) {
			if ((substituted == null)) {
				return affineExpression;
			}
			final LinkedList<AffineTerm> substituteList = new LinkedList<AffineTerm>();
			EList<AffineTerm> _terms = affineExpression.getTerms();
			for (final AffineTerm term : _terms) {
				if (((term.getVariable() != null) && (substituted != null))) {
					boolean _contentEquals = term.getVariable().getName().contentEquals(substituted.getName());
					if (_contentEquals) {
						substituteList.add(term);
					}
				}
			}
			affineExpression.getTerms().removeAll(substituteList);
			for (final AffineTerm term_1 : substituteList) {
				EList<AffineTerm> _terms_1 = ((AffineExpression)newExpr).getTerms();
				for (final AffineTerm newTerm : _terms_1) {
					long _coef = term_1.getCoef();
					long _coef_1 = newTerm.getCoef();
					long _multiply = (_coef * _coef_1);
					affineExpression.getTerms().add(
						IntExpressionBuilder.term(_multiply, 
							newTerm.getVariable()));
				}
			}
			return affineExpression;
		}
		else {
			return Replace.replace(affineExpression, substituted, newExpr);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffineTerm getTerm(final Variable v) {
		EList<AffineTerm> _terms = this.getTerms();
		for (final AffineTerm t : _terms) {
			Variable _variable = t.getVariable();
			boolean _tripleEquals = (_variable == v);
			if (_tripleEquals) {
				return t;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString(final OUTPUT_FORMAT format) {
		return AlgebraPrettyPrinter.print(this, format);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffineTerm getConstantTerm() {
		IntExpression _simplify = this.simplify();
		EList<AffineTerm> _terms = ((AffineExpression) _simplify).getTerms();
		for (final AffineTerm t : _terms) {
			Variable _variable = t.getVariable();
			boolean _tripleEquals = (_variable == null);
			if (_tripleEquals) {
				return t;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AffinePackage.AFFINE_EXPRESSION__TERMS:
				return ((InternalEList<?>)getTerms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AffinePackage.AFFINE_EXPRESSION__TERMS:
				return getTerms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AffinePackage.AFFINE_EXPRESSION__TERMS:
				getTerms().clear();
				getTerms().addAll((Collection<? extends AffineTerm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AffinePackage.AFFINE_EXPRESSION__TERMS:
				getTerms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AffinePackage.AFFINE_EXPRESSION__TERMS:
				return terms != null && !terms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AffineExpressionImpl
