/**
 */
package org.polymodel.algebra.affine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffinePackage;
import org.polymodel.algebra.affine.AffineTerm;

import org.polymodel.algebra.impl.IntTermImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.affine.impl.AffineTermImpl#getVariable <em>Variable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AffineTermImpl extends IntTermImpl implements AffineTerm {
	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable variable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AffineTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AffinePackage.Literals.AFFINE_TERM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getVariable() {
		if (variable != null && variable.eIsProxy()) {
			InternalEObject oldVariable = (InternalEObject)variable;
			variable = (Variable)eResolveProxy(oldVariable);
			if (variable != oldVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AffinePackage.AFFINE_TERM__VARIABLE, oldVariable, variable));
			}
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetVariable() {
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariable(Variable newVariable) {
		Variable oldVariable = variable;
		variable = newVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AffinePackage.AFFINE_TERM__VARIABLE, oldVariable, variable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitAffineTerm(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Variable _variable = this.getVariable();
		boolean _tripleEquals = (_variable == null);
		if (_tripleEquals) {
			return String.valueOf(this.getCoef());
		}
		else {
			long _coef = this.getCoef();
			boolean _equals = (_coef == 1);
			if (_equals) {
				return this.getVariable().toString();
			}
			else {
				long _coef_1 = this.getCoef();
				boolean _equals_1 = (_coef_1 == (-1));
				if (_equals_1) {
					String _string = this.getVariable().toString();
					return ("-" + _string);
				}
				else {
					long _coef_2 = this.getCoef();
					String _plus = (Long.valueOf(_coef_2) + "*");
					String _string_1 = this.getVariable().toString();
					return (_plus + _string_1);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEquivalent(final IntTerm other) {
		if ((!(other instanceof AffineTerm))) {
			return false;
		}
		final AffineTerm otherAffine = ((AffineTerm) other);
		if ((((this.getVariable() == null) && (otherAffine.getVariable() != null)) || ((this.getVariable() != null) && (otherAffine.getVariable() == null)))) {
			return false;
		}
		if (((this.getVariable() != null) && (otherAffine.getVariable() != null))) {
			int _compareTo = this.getVariable().getName().compareTo(otherAffine.getVariable().getName());
			boolean _notEquals = (_compareTo != 0);
			if (_notEquals) {
				return false;
			}
		}
		long _coef = this.getCoef();
		long _coef_1 = otherAffine.getCoef();
		return (_coef == _coef_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int compareTo(final IntTerm o) {
		if ((o instanceof AffineTerm)) {
			final AffineTerm affineOther = ((AffineTerm)o);
			if (((this.getVariable() == null) && (affineOther.getVariable() == null))) {
				return super.compareTo(affineOther);
			}
			else {
				if (((this.getVariable() == null) && (affineOther.getVariable() != null))) {
					return 1;
				}
				else {
					if (((this.getVariable() != null) && (affineOther.getVariable() == null))) {
						return (-1);
					}
					else {
						final int var = this.getVariable().getName().compareTo(affineOther.getVariable().getName());
						if ((var == 0)) {
							return super.compareTo(o);
						}
						else {
							if (((this.getCoef() < 0) && (((AffineTerm)o).getCoef() > 0))) {
								return 1;
							}
							return var;
						}
					}
				}
			}
		}
		else {
			return super.compareTo(o);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AffinePackage.AFFINE_TERM__VARIABLE:
				if (resolve) return getVariable();
				return basicGetVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AffinePackage.AFFINE_TERM__VARIABLE:
				setVariable((Variable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AffinePackage.AFFINE_TERM__VARIABLE:
				setVariable((Variable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AffinePackage.AFFINE_TERM__VARIABLE:
				return variable != null;
		}
		return super.eIsSet(featureID);
	}

} //AffineTermImpl
