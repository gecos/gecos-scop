/**
 */
package org.polymodel.algebra.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.polymodel.algebra.AlgebraFactory;
import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.CompositeIntExpression;
import org.polymodel.algebra.CompositeOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.IntTerm;
import org.polymodel.algebra.SelectExpression;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffinePackage;

import org.polymodel.algebra.affine.impl.AffinePackageImpl;

import org.polymodel.algebra.polynomials.PolynomialsPackage;

import org.polymodel.algebra.polynomials.impl.PolynomialsPackageImpl;

import org.polymodel.algebra.quasiAffine.QuasiAffinePackage;

import org.polymodel.algebra.quasiAffine.impl.QuasiAffinePackageImpl;

import org.polymodel.algebra.reductions.ReductionsPackage;

import org.polymodel.algebra.reductions.impl.ReductionsPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AlgebraPackageImpl extends EPackageImpl implements AlgebraPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass algebraVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intConstraintSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass algebraVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeIntExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selectExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fuzzyBooleanEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum outpuT_FORMATEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum comparisonOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum compositeOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType valueEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polymodel.algebra.AlgebraPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AlgebraPackageImpl() {
		super(eNS_URI, AlgebraFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AlgebraPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AlgebraPackage init() {
		if (isInited) return (AlgebraPackage)EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI);

		// Obtain or create and register package
		AlgebraPackageImpl theAlgebraPackage = (AlgebraPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AlgebraPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AlgebraPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		AffinePackageImpl theAffinePackage = (AffinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI) instanceof AffinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI) : AffinePackage.eINSTANCE);
		QuasiAffinePackageImpl theQuasiAffinePackage = (QuasiAffinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(QuasiAffinePackage.eNS_URI) instanceof QuasiAffinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(QuasiAffinePackage.eNS_URI) : QuasiAffinePackage.eINSTANCE);
		PolynomialsPackageImpl thePolynomialsPackage = (PolynomialsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PolynomialsPackage.eNS_URI) instanceof PolynomialsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PolynomialsPackage.eNS_URI) : PolynomialsPackage.eINSTANCE);
		ReductionsPackageImpl theReductionsPackage = (ReductionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReductionsPackage.eNS_URI) instanceof ReductionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReductionsPackage.eNS_URI) : ReductionsPackage.eINSTANCE);

		// Create package meta-data objects
		theAlgebraPackage.createPackageContents();
		theAffinePackage.createPackageContents();
		theQuasiAffinePackage.createPackageContents();
		thePolynomialsPackage.createPackageContents();
		theReductionsPackage.createPackageContents();

		// Initialize created meta-data
		theAlgebraPackage.initializePackageContents();
		theAffinePackage.initializePackageContents();
		theQuasiAffinePackage.initializePackageContents();
		thePolynomialsPackage.initializePackageContents();
		theReductionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAlgebraPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AlgebraPackage.eNS_URI, theAlgebraPackage);
		return theAlgebraPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlgebraVisitable() {
		return algebraVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntExpression() {
		return intExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntTerm() {
		return intTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntTerm_Coef() {
		return (EAttribute)intTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariable() {
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_Name() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntConstraint() {
		return intConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntConstraint_Lhs() {
		return (EReference)intConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntConstraint_Rhs() {
		return (EReference)intConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntConstraint_ComparisonOperator() {
		return (EAttribute)intConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntConstraintSystem() {
		return intConstraintSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntConstraintSystem_Constraints() {
		return (EReference)intConstraintSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlgebraVisitor() {
		return algebraVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeIntExpression() {
		return compositeIntExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeIntExpression_Left() {
		return (EReference)compositeIntExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeIntExpression_Right() {
		return (EReference)compositeIntExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompositeIntExpression_Operator() {
		return (EAttribute)compositeIntExpressionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSelectExpression() {
		return selectExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectExpression_Condition() {
		return (EReference)selectExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectExpression_Then() {
		return (EReference)selectExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectExpression_Else() {
		return (EReference)selectExpressionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFuzzyBoolean() {
		return fuzzyBooleanEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOUTPUT_FORMAT() {
		return outpuT_FORMATEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getComparisonOperator() {
		return comparisonOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCompositeOperator() {
		return compositeOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getValue() {
		return valueEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlgebraFactory getAlgebraFactory() {
		return (AlgebraFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		algebraVisitableEClass = createEClass(ALGEBRA_VISITABLE);

		intExpressionEClass = createEClass(INT_EXPRESSION);

		intTermEClass = createEClass(INT_TERM);
		createEAttribute(intTermEClass, INT_TERM__COEF);

		variableEClass = createEClass(VARIABLE);
		createEAttribute(variableEClass, VARIABLE__NAME);

		intConstraintEClass = createEClass(INT_CONSTRAINT);
		createEReference(intConstraintEClass, INT_CONSTRAINT__LHS);
		createEReference(intConstraintEClass, INT_CONSTRAINT__RHS);
		createEAttribute(intConstraintEClass, INT_CONSTRAINT__COMPARISON_OPERATOR);

		intConstraintSystemEClass = createEClass(INT_CONSTRAINT_SYSTEM);
		createEReference(intConstraintSystemEClass, INT_CONSTRAINT_SYSTEM__CONSTRAINTS);

		algebraVisitorEClass = createEClass(ALGEBRA_VISITOR);

		compositeIntExpressionEClass = createEClass(COMPOSITE_INT_EXPRESSION);
		createEReference(compositeIntExpressionEClass, COMPOSITE_INT_EXPRESSION__LEFT);
		createEReference(compositeIntExpressionEClass, COMPOSITE_INT_EXPRESSION__RIGHT);
		createEAttribute(compositeIntExpressionEClass, COMPOSITE_INT_EXPRESSION__OPERATOR);

		selectExpressionEClass = createEClass(SELECT_EXPRESSION);
		createEReference(selectExpressionEClass, SELECT_EXPRESSION__CONDITION);
		createEReference(selectExpressionEClass, SELECT_EXPRESSION__THEN);
		createEReference(selectExpressionEClass, SELECT_EXPRESSION__ELSE);

		// Create enums
		fuzzyBooleanEEnum = createEEnum(FUZZY_BOOLEAN);
		outpuT_FORMATEEnum = createEEnum(OUTPUT_FORMAT);
		comparisonOperatorEEnum = createEEnum(COMPARISON_OPERATOR);
		compositeOperatorEEnum = createEEnum(COMPOSITE_OPERATOR);

		// Create data types
		stringEDataType = createEDataType(STRING);
		longEDataType = createEDataType(LONG);
		valueEDataType = createEDataType(VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AffinePackage theAffinePackage = (AffinePackage)EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI);
		QuasiAffinePackage theQuasiAffinePackage = (QuasiAffinePackage)EPackage.Registry.INSTANCE.getEPackage(QuasiAffinePackage.eNS_URI);
		PolynomialsPackage thePolynomialsPackage = (PolynomialsPackage)EPackage.Registry.INSTANCE.getEPackage(PolynomialsPackage.eNS_URI);
		ReductionsPackage theReductionsPackage = (ReductionsPackage)EPackage.Registry.INSTANCE.getEPackage(ReductionsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		intExpressionEClass.getESuperTypes().add(this.getAlgebraVisitable());
		intTermEClass.getESuperTypes().add(this.getAlgebraVisitable());
		variableEClass.getESuperTypes().add(this.getAlgebraVisitable());
		intConstraintEClass.getESuperTypes().add(this.getAlgebraVisitable());
		intConstraintSystemEClass.getESuperTypes().add(this.getAlgebraVisitable());
		compositeIntExpressionEClass.getESuperTypes().add(this.getIntExpression());
		selectExpressionEClass.getESuperTypes().add(this.getIntExpression());

		// Initialize classes and features; add operations and parameters
		initEClass(algebraVisitableEClass, AlgebraVisitable.class, "AlgebraVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(algebraVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitableEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(intExpressionEClass, IntExpression.class, "IntExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(intExpressionEClass, this.getFuzzyBoolean(), "isEquivalent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, this.getFuzzyBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, this.getFuzzyBoolean(), "isZero", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, this.getIntExpression(), "simplify", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "newVar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 1, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "newExpr", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, theAffinePackage.getAffineExpression(), "toAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, theQuasiAffinePackage.getQuasiAffineExpression(), "toQuasiAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, thePolynomialsPackage.getPolynomialExpression(), "toPolynomial", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intExpressionEClass, theReductionsPackage.getReductionExpression(), "toReduction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intExpressionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intExpressionEClass, null, "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		EGenericType g1 = createEGenericType(this.getIntExpression());
		t1.getEBounds().add(g1);
		g1 = createEGenericType(t1);
		initEOperation(op, g1);

		initEClass(intTermEClass, IntTerm.class, "IntTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntTerm_Coef(), this.getlong(), "coef", null, 0, 1, IntTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(intTermEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intTermEClass, theEcorePackage.getEBoolean(), "isEquivalent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntTerm(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intTermEClass, theEcorePackage.getEInt(), "compareTo", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntTerm(), "o", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intTermEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(variableEClass, Variable.class, "Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariable_Name(), this.getString(), "name", null, 1, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(variableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(variableEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(variableEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(intConstraintEClass, IntConstraint.class, "IntConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntConstraint_Lhs(), this.getIntExpression(), null, "lhs", null, 0, 1, IntConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntConstraint_Rhs(), this.getIntExpression(), null, "rhs", null, 0, 1, IntConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntConstraint_ComparisonOperator(), this.getComparisonOperator(), "comparisonOperator", null, 0, 1, IntConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(intConstraintEClass, theEcorePackage.getEBoolean(), "isAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintEClass, theEcorePackage.getEBoolean(), "isQuasiAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintEClass, theEcorePackage.getEBoolean(), "isPolynomial", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getIntConstraint(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "newVar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getIntConstraint(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "newExpr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintEClass, this.getIntConstraint(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintEClass, this.getIntConstraint(), "simplify", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getFuzzyBoolean(), "isEquivalent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntConstraint(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getIntExpression(), "getUB", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "var", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintEClass, this.getIntExpression(), "getLB", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "var", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(intConstraintSystemEClass, IntConstraintSystem.class, "IntConstraintSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntConstraintSystem_Constraints(), this.getIntConstraint(), null, "constraints", null, 0, -1, IntConstraintSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintSystemEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, this.getIntConstraintSystem(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "newVar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, this.getIntConstraintSystem(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "newExpr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintSystemEClass, this.getIntConstraintSystem(), "simplify", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intConstraintSystemEClass, this.getIntConstraintSystem(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, this.getIntExpression(), "getUB", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "var", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intConstraintSystemEClass, this.getIntExpression(), "getLB", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "var", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(algebraVisitorEClass, AlgebraVisitor.class, "AlgebraVisitor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(algebraVisitorEClass, null, "visitIntTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntTerm(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitVariable", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "v", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitIntConstraint", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntConstraint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitIntConstraintSystem", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntConstraintSystem(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitAffineExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAffinePackage.getAffineExpression(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitAffineTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAffinePackage.getAffineTerm(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitQuasiAffineExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQuasiAffinePackage.getQuasiAffineExpression(), "q", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitQuasiAffineTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQuasiAffinePackage.getQuasiAffineTerm(), "q", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitSimpleQuasiAffineTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQuasiAffinePackage.getSimpleQuasiAffineTerm(), "q", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitNestedQuasiAffineTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQuasiAffinePackage.getNestedQuasiAffineTerm(), "q", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitPolynomialExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePolynomialsPackage.getPolynomialExpression(), "p", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitPolynomialTerm", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePolynomialsPackage.getPolynomialTerm(), "p", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitPolynomialVariable", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePolynomialsPackage.getPolynomialVariable(), "p", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitReductionExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theReductionsPackage.getReductionExpression(), "r", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitIntExpression", 1, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "ie", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitCompositeIntExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeIntExpression(), "r", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(algebraVisitorEClass, null, "visitSelectExpression", 1, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSelectExpression(), "ie", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(compositeIntExpressionEClass, CompositeIntExpression.class, "CompositeIntExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeIntExpression_Left(), this.getIntExpression(), null, "left", null, 1, 1, CompositeIntExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeIntExpression_Right(), this.getIntExpression(), null, "right", null, 1, 1, CompositeIntExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCompositeIntExpression_Operator(), this.getCompositeOperator(), "operator", null, 1, 1, CompositeIntExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(compositeIntExpressionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeIntExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "newExpr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeIntExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "newVar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeIntExpressionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeIntExpressionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(selectExpressionEClass, SelectExpression.class, "SelectExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSelectExpression_Condition(), this.getIntConstraintSystem(), null, "condition", null, 0, -1, SelectExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSelectExpression_Then(), this.getIntExpression(), null, "then", null, 1, 1, SelectExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSelectExpression_Else(), this.getIntExpression(), null, "else", null, 1, 1, SelectExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(selectExpressionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(selectExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntExpression(), "newExpr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(selectExpressionEClass, this.getIntExpression(), "substitute", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "substituted", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVariable(), "newVar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(selectExpressionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(fuzzyBooleanEEnum, FuzzyBoolean.class, "FuzzyBoolean");
		addEEnumLiteral(fuzzyBooleanEEnum, FuzzyBoolean.YES);
		addEEnumLiteral(fuzzyBooleanEEnum, FuzzyBoolean.NO);
		addEEnumLiteral(fuzzyBooleanEEnum, FuzzyBoolean.MAYBE);

		initEEnum(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.class, "OUTPUT_FORMAT");
		addEEnumLiteral(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.ALPHABETS);
		addEEnumLiteral(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.C);
		addEEnumLiteral(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.ISL);
		addEEnumLiteral(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.LATEX);
		addEEnumLiteral(outpuT_FORMATEEnum, org.polymodel.algebra.OUTPUT_FORMAT.SMT2);

		initEEnum(comparisonOperatorEEnum, ComparisonOperator.class, "ComparisonOperator");
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.EQ);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.NE);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GE);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LE);

		initEEnum(compositeOperatorEEnum, CompositeOperator.class, "CompositeOperator");
		addEEnumLiteral(compositeOperatorEEnum, CompositeOperator.DIV);
		addEEnumLiteral(compositeOperatorEEnum, CompositeOperator.MOD);
		addEEnumLiteral(compositeOperatorEEnum, CompositeOperator.CEIL);
		addEEnumLiteral(compositeOperatorEEnum, CompositeOperator.FLOOR);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(valueEDataType, long.class, "Value", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //AlgebraPackageImpl
