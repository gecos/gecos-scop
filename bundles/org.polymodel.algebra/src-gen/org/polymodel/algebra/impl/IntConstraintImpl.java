/**
 */
package org.polymodel.algebra.impl;

import com.google.common.base.Objects;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.ComparisonOperator;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffineExpression;

import org.polymodel.algebra.factory.IntExpressionBuilder;

import org.polymodel.algebra.polynomials.PolynomialExpression;

import org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter;

import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;

import org.polymodel.algebra.tom.Isolate;

import org.polymodel.algebra.util.ComparisonOperatorUtils;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.impl.IntConstraintImpl#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.polymodel.algebra.impl.IntConstraintImpl#getRhs <em>Rhs</em>}</li>
 *   <li>{@link org.polymodel.algebra.impl.IntConstraintImpl#getComparisonOperator <em>Comparison Operator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntConstraintImpl extends MinimalEObjectImpl.Container implements IntConstraint {
	/**
	 * The cached value of the '{@link #getLhs() <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLhs()
	 * @generated
	 * @ordered
	 */
	protected IntExpression lhs;

	/**
	 * The cached value of the '{@link #getRhs() <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRhs()
	 * @generated
	 * @ordered
	 */
	protected IntExpression rhs;

	/**
	 * The default value of the '{@link #getComparisonOperator() <em>Comparison Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonOperator()
	 * @generated
	 * @ordered
	 */
	protected static final ComparisonOperator COMPARISON_OPERATOR_EDEFAULT = ComparisonOperator.EQ;

	/**
	 * The cached value of the '{@link #getComparisonOperator() <em>Comparison Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonOperator()
	 * @generated
	 * @ordered
	 */
	protected ComparisonOperator comparisonOperator = COMPARISON_OPERATOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AlgebraPackage.Literals.INT_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getLhs() {
		return lhs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLhs(IntExpression newLhs, NotificationChain msgs) {
		IntExpression oldLhs = lhs;
		lhs = newLhs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AlgebraPackage.INT_CONSTRAINT__LHS, oldLhs, newLhs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLhs(IntExpression newLhs) {
		if (newLhs != lhs) {
			NotificationChain msgs = null;
			if (lhs != null)
				msgs = ((InternalEObject)lhs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.INT_CONSTRAINT__LHS, null, msgs);
			if (newLhs != null)
				msgs = ((InternalEObject)newLhs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.INT_CONSTRAINT__LHS, null, msgs);
			msgs = basicSetLhs(newLhs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AlgebraPackage.INT_CONSTRAINT__LHS, newLhs, newLhs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getRhs() {
		return rhs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRhs(IntExpression newRhs, NotificationChain msgs) {
		IntExpression oldRhs = rhs;
		rhs = newRhs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AlgebraPackage.INT_CONSTRAINT__RHS, oldRhs, newRhs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRhs(IntExpression newRhs) {
		if (newRhs != rhs) {
			NotificationChain msgs = null;
			if (rhs != null)
				msgs = ((InternalEObject)rhs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.INT_CONSTRAINT__RHS, null, msgs);
			if (newRhs != null)
				msgs = ((InternalEObject)newRhs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.INT_CONSTRAINT__RHS, null, msgs);
			msgs = basicSetRhs(newRhs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AlgebraPackage.INT_CONSTRAINT__RHS, newRhs, newRhs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComparisonOperator(ComparisonOperator newComparisonOperator) {
		ComparisonOperator oldComparisonOperator = comparisonOperator;
		comparisonOperator = newComparisonOperator == null ? COMPARISON_OPERATOR_EDEFAULT : newComparisonOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AlgebraPackage.INT_CONSTRAINT__COMPARISON_OPERATOR, oldComparisonOperator, comparisonOperator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAffine() {
		return ((this.getRhs() instanceof AffineExpression) && (this.getLhs() instanceof AffineExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isQuasiAffine() {
		return ((this.getRhs() instanceof QuasiAffineExpression) && (this.getLhs() instanceof QuasiAffineExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPolynomial() {
		return ((this.getRhs() instanceof PolynomialExpression) && (this.getLhs() instanceof PolynomialExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitIntConstraint(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraint substitute(final Variable substituted, final Variable newVar) {
		final IntExpression lhs = this.getLhs().substitute(substituted, newVar);
		final IntExpression rhs2 = this.getRhs();
		final IntExpression rhs = rhs2.substitute(substituted, newVar);
		return IntExpressionBuilder.constraint(lhs, rhs, this.getComparisonOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraint substitute(final Variable substituted, final IntExpression newExpr) {
		return IntExpressionBuilder.constraint(this.getLhs().substitute(substituted, newExpr), 
			this.getRhs().substitute(substituted, newExpr), this.getComparisonOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(this.getLhs().toString());
		String _switchResult = null;
		ComparisonOperator _comparisonOperator = this.getComparisonOperator();
		if (_comparisonOperator != null) {
			switch (_comparisonOperator) {
				case EQ:
					_switchResult = " == ";
					break;
				case GE:
					_switchResult = " >= ";
					break;
				case GT:
					_switchResult = " > ";
					break;
				case LE:
					_switchResult = " <= ";
					break;
				case LT:
					_switchResult = " < ";
					break;
				case NE:
					_switchResult = " != ";
					break;
				default:
					break;
			}
		}
		final String cop = _switchResult;
		stringBuffer.append(cop);
		stringBuffer.append(this.getRhs().toString());
		return stringBuffer.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraint copy() {
		final Copier copier = new Copier();
		EObject _copy = copier.copy(this);
		final IntConstraint res = ((IntConstraint) _copy);
		copier.copyReferences();
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraint simplify() {
		return IntExpressionBuilder.constraint(this.getLhs().simplify(), this.getRhs().simplify(), this.getComparisonOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FuzzyBoolean isEquivalent(final IntConstraint other) {
		FuzzyBoolean lhsEquivalency = FuzzyBoolean.MAYBE;
		FuzzyBoolean rhsEquivalency = FuzzyBoolean.MAYBE;
		final IntExpression olhs = other.getLhs().simplify();
		final IntExpression orhs = other.getRhs().simplify();
		ComparisonOperator _comparisonOperator = this.getComparisonOperator();
		ComparisonOperator _comparisonOperator_1 = other.getComparisonOperator();
		boolean _equals = Objects.equal(_comparisonOperator, _comparisonOperator_1);
		if (_equals) {
			lhsEquivalency = this.getLhs().simplify().isEquivalent(olhs);
			rhsEquivalency = this.getRhs().simplify().isEquivalent(orhs);
		}
		else {
			ComparisonOperator _comparisonOperator_2 = this.getComparisonOperator();
			ComparisonOperator _opposite = ComparisonOperatorUtils.getOpposite(other.getComparisonOperator());
			boolean _equals_1 = Objects.equal(_comparisonOperator_2, _opposite);
			if (_equals_1) {
				lhsEquivalency = this.getLhs().simplify().isEquivalent(orhs);
				rhsEquivalency = this.getRhs().simplify().isEquivalent(olhs);
			}
			else {
				lhsEquivalency = null;
				rhsEquivalency = null;
			}
		}
		if (((lhsEquivalency != null) && (rhsEquivalency != null))) {
			boolean _equals_2 = Objects.equal(lhsEquivalency, FuzzyBoolean.YES);
			if (_equals_2) {
				return rhsEquivalency;
			}
			else {
				boolean _equals_3 = Objects.equal(lhsEquivalency, FuzzyBoolean.MAYBE);
				if (_equals_3) {
					if ((Objects.equal(rhsEquivalency, FuzzyBoolean.YES) || Objects.equal(rhsEquivalency, FuzzyBoolean.MAYBE))) {
						return FuzzyBoolean.MAYBE;
					}
					else {
						return FuzzyBoolean.NO;
					}
				}
			}
		}
		return FuzzyBoolean.NO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString(final OUTPUT_FORMAT format) {
		return AlgebraPrettyPrinter.print(this, format);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getUB(final Variable var) {
		final IntConstraint constraint = Isolate.isolateToLhs(this, var);
		if ((constraint == null)) {
			return null;
		}
		final ComparisonOperator cop = constraint.getComparisonOperator();
		if ((Objects.equal(cop, ComparisonOperator.LE) || Objects.equal(cop, ComparisonOperator.EQ))) {
			return constraint.getRhs();
		}
		else {
			boolean _equals = Objects.equal(cop, ComparisonOperator.LT);
			if (_equals) {
				return IntExpressionBuilder.add(constraint.getRhs(), (-1));
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getLB(final Variable var) {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT__LHS:
				return basicSetLhs(null, msgs);
			case AlgebraPackage.INT_CONSTRAINT__RHS:
				return basicSetRhs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT__LHS:
				return getLhs();
			case AlgebraPackage.INT_CONSTRAINT__RHS:
				return getRhs();
			case AlgebraPackage.INT_CONSTRAINT__COMPARISON_OPERATOR:
				return getComparisonOperator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT__LHS:
				setLhs((IntExpression)newValue);
				return;
			case AlgebraPackage.INT_CONSTRAINT__RHS:
				setRhs((IntExpression)newValue);
				return;
			case AlgebraPackage.INT_CONSTRAINT__COMPARISON_OPERATOR:
				setComparisonOperator((ComparisonOperator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT__LHS:
				setLhs((IntExpression)null);
				return;
			case AlgebraPackage.INT_CONSTRAINT__RHS:
				setRhs((IntExpression)null);
				return;
			case AlgebraPackage.INT_CONSTRAINT__COMPARISON_OPERATOR:
				setComparisonOperator(COMPARISON_OPERATOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT__LHS:
				return lhs != null;
			case AlgebraPackage.INT_CONSTRAINT__RHS:
				return rhs != null;
			case AlgebraPackage.INT_CONSTRAINT__COMPARISON_OPERATOR:
				return comparisonOperator != COMPARISON_OPERATOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //IntConstraintImpl
