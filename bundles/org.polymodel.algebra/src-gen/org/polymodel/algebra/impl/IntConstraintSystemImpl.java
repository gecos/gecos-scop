/**
 */
package org.polymodel.algebra.impl;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.factory.IntExpressionBuilder;

import org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Constraint System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.impl.IntConstraintSystemImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntConstraintSystemImpl extends MinimalEObjectImpl.Container implements IntConstraintSystem {
	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntConstraintSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AlgebraPackage.Literals.INT_CONSTRAINT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<IntConstraint>(IntConstraint.class, this, AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitIntConstraintSystem(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return IterableExtensions.join(this.getConstraints(), " and ");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem substitute(final Variable substituted, final Variable newVar) {
		IntConstraintSystem _xblockexpression = null;
		{
			final Function1<IntConstraint, IntConstraint> _function = new Function1<IntConstraint, IntConstraint>() {
				public IntConstraint apply(final IntConstraint it) {
					return it.substitute(substituted, newVar);
				}
			};
			final EList<IntConstraint> cl = ECollections.<IntConstraint>unmodifiableEList(ECollections.<IntConstraint>asEList(XcoreEListExtensions.<IntConstraint, IntConstraint>map(this.getConstraints(), _function)));
			_xblockexpression = IntExpressionBuilder.constraintSystem(cl);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem substitute(final Variable substituted, final IntExpression newExpr) {
		IntConstraintSystem _xblockexpression = null;
		{
			final Function1<IntConstraint, IntConstraint> _function = new Function1<IntConstraint, IntConstraint>() {
				public IntConstraint apply(final IntConstraint it) {
					return it.substitute(substituted, newExpr);
				}
			};
			final EList<IntConstraint> cl = ECollections.<IntConstraint>unmodifiableEList(ECollections.<IntConstraint>asEList(XcoreEListExtensions.<IntConstraint, IntConstraint>map(this.getConstraints(), _function)));
			_xblockexpression = IntExpressionBuilder.constraintSystem(cl);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem simplify() {
		final Function1<IntConstraint, IntConstraint> _function = new Function1<IntConstraint, IntConstraint>() {
			public IntConstraint apply(final IntConstraint it) {
				return it.simplify();
			}
		};
		final EList<IntConstraint> cl = ECollections.<IntConstraint>unmodifiableEList(ECollections.<IntConstraint>asEList(XcoreEListExtensions.<IntConstraint, IntConstraint>map(this.getConstraints(), _function)));
		final ArrayList<IntConstraint> redundant = new ArrayList<IntConstraint>();
		int _size = cl.size();
		ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size, true);
		for (final Integer i : _doubleDotLessThan) {
			int _size_1 = cl.size();
			ExclusiveRange _doubleDotLessThan_1 = new ExclusiveRange(((i).intValue() + 1), _size_1, true);
			for (final Integer j : _doubleDotLessThan_1) {
				{
					final IntConstraint c1 = cl.get((i).intValue());
					final IntConstraint c2 = cl.get((j).intValue());
					FuzzyBoolean _isEquivalent = c1.isEquivalent(c2);
					boolean _equals = Objects.equal(_isEquivalent, FuzzyBoolean.YES);
					if (_equals) {
						boolean _not = (!(redundant.contains(c1) || redundant.contains(c2)));
						if (_not) {
							redundant.add(c1);
						}
					}
				}
			}
		}
		this.getConstraints().removeAll(redundant);
		return IntExpressionBuilder.constraintSystem(this.getConstraints());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntConstraintSystem copy() {
		IntConstraintSystem _xblockexpression = null;
		{
			final Function1<IntConstraint, IntConstraint> _function = new Function1<IntConstraint, IntConstraint>() {
				public IntConstraint apply(final IntConstraint it) {
					return it.copy();
				}
			};
			final EList<IntConstraint> cl = ECollections.<IntConstraint>unmodifiableEList(ECollections.<IntConstraint>asEList(XcoreEListExtensions.<IntConstraint, IntConstraint>map(this.getConstraints(), _function)));
			_xblockexpression = IntExpressionBuilder.constraintSystem(cl);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString(final OUTPUT_FORMAT format) {
		return AlgebraPrettyPrinter.print(this, format);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getUB(final Variable var) {
		final LinkedList<IntExpression> ubs = new LinkedList<IntExpression>();
		EList<IntConstraint> _constraints = this.getConstraints();
		for (final IntConstraint ic : _constraints) {
			{
				final IntExpression ub = ic.getUB(var);
				if ((ub != null)) {
					ubs.add(ub);
				}
			}
		}
		int _size = ubs.size();
		boolean _equals = (_size == 0);
		if (_equals) {
			return null;
		}
		else {
			int _size_1 = ubs.size();
			boolean _equals_1 = (_size_1 == 1);
			if (_equals_1) {
				return ubs.get(0);
			}
		}
		return IntExpressionBuilder.max(ubs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getLB(final Variable var) {
		final LinkedList<IntExpression> lbs = new LinkedList<IntExpression>();
		EList<IntConstraint> _constraints = this.getConstraints();
		for (final IntConstraint ic : _constraints) {
			{
				final IntExpression lb = ic.getLB(var);
				if ((lb != null)) {
					lbs.add(lb);
				}
			}
		}
		int _size = lbs.size();
		boolean _equals = (_size == 0);
		if (_equals) {
			return null;
		}
		else {
			int _size_1 = lbs.size();
			boolean _equals_1 = (_size_1 == 1);
			if (_equals_1) {
				return lbs.get(0);
			}
		}
		return IntExpressionBuilder.min(lbs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends IntConstraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AlgebraPackage.INT_CONSTRAINT_SYSTEM__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IntConstraintSystemImpl
