/**
 */
package org.polymodel.algebra.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.FuzzyBoolean;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffineExpression;

import org.polymodel.algebra.polynomials.PolynomialExpression;

import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;

import org.polymodel.algebra.reductions.ReductionExpression;

import org.polymodel.algebra.tom.ConstantIntExpression;
import org.polymodel.algebra.tom.EquivalentIntExpression;
import org.polymodel.algebra.tom.SimplifyIntExpression;
import org.polymodel.algebra.tom.ZeroIntExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class IntExpressionImpl extends MinimalEObjectImpl.Container implements IntExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AlgebraPackage.Literals.INT_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FuzzyBoolean isEquivalent(final IntExpression other) {
		return EquivalentIntExpression.isEquivalent(this.simplify(), other.simplify());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FuzzyBoolean isConstant() {
		return ConstantIntExpression.isConstant(this.simplify());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FuzzyBoolean isZero() {
		return ZeroIntExpression.isZero(this.simplify());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression simplify() {
		return SimplifyIntExpression.simplify(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final Variable newVar) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ((((((("Cannot (yet) substitute " + substituted) + " by ") + newVar) + " in ") + this) + " (of type ") + _simpleName);
		String _plus_1 = (_plus + ")");
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final IntExpression newExpr) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ((((((("Cannot (yet) substitute " + substituted) + " by ") + newExpr) + " in ") + this) + " (of type ") + _simpleName);
		String _plus_1 = (_plus + ")");
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffineExpression toAffine() {
		final IntExpression res = SimplifyIntExpression.simplify(this.<IntExpression>copy());
		if ((res instanceof AffineExpression)) {
			return ((AffineExpression)res);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuasiAffineExpression toQuasiAffine() {
		final IntExpression res = this.simplify();
		if ((res instanceof QuasiAffineExpression)) {
			return ((QuasiAffineExpression)res);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolynomialExpression toPolynomial() {
		final IntExpression res = this.simplify();
		if ((res instanceof PolynomialExpression)) {
			return ((PolynomialExpression)res);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReductionExpression toReduction() {
		final IntExpression res = this.simplify();
		if ((res instanceof ReductionExpression)) {
			return ((ReductionExpression)res);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(AlgebraVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <T extends IntExpression> T copy() {
		return EcoreUtil.<T>copy(((T) this));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString(OUTPUT_FORMAT format) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} //IntExpressionImpl
