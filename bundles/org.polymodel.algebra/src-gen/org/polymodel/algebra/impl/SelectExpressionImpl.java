/**
 */
package org.polymodel.algebra.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.polymodel.algebra.AlgebraPackage;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntConstraintSystem;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.SelectExpression;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Select Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.impl.SelectExpressionImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.polymodel.algebra.impl.SelectExpressionImpl#getThen <em>Then</em>}</li>
 *   <li>{@link org.polymodel.algebra.impl.SelectExpressionImpl#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SelectExpressionImpl extends IntExpressionImpl implements SelectExpression {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected EList<IntConstraintSystem> condition;

	/**
	 * The cached value of the '{@link #getThen() <em>Then</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThen()
	 * @generated
	 * @ordered
	 */
	protected IntExpression then;

	/**
	 * The cached value of the '{@link #getElse() <em>Else</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElse()
	 * @generated
	 * @ordered
	 */
	protected IntExpression else_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SelectExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AlgebraPackage.Literals.SELECT_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntConstraintSystem> getCondition() {
		if (condition == null) {
			condition = new EObjectResolvingEList<IntConstraintSystem>(IntConstraintSystem.class, this, AlgebraPackage.SELECT_EXPRESSION__CONDITION);
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getThen() {
		return then;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThen(IntExpression newThen, NotificationChain msgs) {
		IntExpression oldThen = then;
		then = newThen;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AlgebraPackage.SELECT_EXPRESSION__THEN, oldThen, newThen);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThen(IntExpression newThen) {
		if (newThen != then) {
			NotificationChain msgs = null;
			if (then != null)
				msgs = ((InternalEObject)then).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.SELECT_EXPRESSION__THEN, null, msgs);
			if (newThen != null)
				msgs = ((InternalEObject)newThen).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.SELECT_EXPRESSION__THEN, null, msgs);
			msgs = basicSetThen(newThen, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AlgebraPackage.SELECT_EXPRESSION__THEN, newThen, newThen));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression getElse() {
		return else_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElse(IntExpression newElse, NotificationChain msgs) {
		IntExpression oldElse = else_;
		else_ = newElse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AlgebraPackage.SELECT_EXPRESSION__ELSE, oldElse, newElse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElse(IntExpression newElse) {
		if (newElse != else_) {
			NotificationChain msgs = null;
			if (else_ != null)
				msgs = ((InternalEObject)else_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.SELECT_EXPRESSION__ELSE, null, msgs);
			if (newElse != null)
				msgs = ((InternalEObject)newElse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AlgebraPackage.SELECT_EXPRESSION__ELSE, null, msgs);
			msgs = basicSetElse(newElse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AlgebraPackage.SELECT_EXPRESSION__ELSE, newElse, newElse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		EList<IntConstraintSystem> _condition = this.getCondition();
		String _plus = ("Select(" + _condition);
		String _plus_1 = (_plus + ", ");
		IntExpression _then = this.getThen();
		String _plus_2 = (_plus_1 + _then);
		String _plus_3 = (_plus_2 + ", ");
		IntExpression _else = this.getElse();
		String _plus_4 = (_plus_3 + _else);
		return (_plus_4 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final IntExpression newExpr) {
		final Function1<IntConstraintSystem, IntConstraintSystem> _function = new Function1<IntConstraintSystem, IntConstraintSystem>() {
			public IntConstraintSystem apply(final IntConstraintSystem it) {
				return it.substitute(substituted, newExpr);
			}
		};
		final EList<IntConstraintSystem> newCond = ECollections.<IntConstraintSystem>unmodifiableEList(ECollections.<IntConstraintSystem>asEList(XcoreEListExtensions.<IntConstraintSystem, IntConstraintSystem>map(this.getCondition(), _function)));
		IntExpression _copy = this.<IntExpression>copy();
		final SelectExpression copy = ((SelectExpression) _copy);
		copy.getCondition().clear();
		copy.getCondition().addAll(newCond);
		copy.setThen(this.getThen().substitute(substituted, newExpr));
		copy.setElse(this.getElse().substitute(substituted, newExpr));
		return copy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntExpression substitute(final Variable substituted, final Variable newVar) {
		final Function1<IntConstraintSystem, IntConstraintSystem> _function = new Function1<IntConstraintSystem, IntConstraintSystem>() {
			public IntConstraintSystem apply(final IntConstraintSystem it) {
				return it.substitute(substituted, newVar);
			}
		};
		final EList<IntConstraintSystem> newCond = ECollections.<IntConstraintSystem>unmodifiableEList(ECollections.<IntConstraintSystem>asEList(XcoreEListExtensions.<IntConstraintSystem, IntConstraintSystem>map(this.getCondition(), _function)));
		IntExpression _copy = this.<IntExpression>copy();
		final SelectExpression copy = ((SelectExpression) _copy);
		copy.getCondition().clear();
		copy.getCondition().addAll(newCond);
		copy.setThen(this.getThen().substitute(substituted, newVar));
		copy.setElse(this.getElse().substitute(substituted, newVar));
		return copy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitSelectExpression(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AlgebraPackage.SELECT_EXPRESSION__THEN:
				return basicSetThen(null, msgs);
			case AlgebraPackage.SELECT_EXPRESSION__ELSE:
				return basicSetElse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AlgebraPackage.SELECT_EXPRESSION__CONDITION:
				return getCondition();
			case AlgebraPackage.SELECT_EXPRESSION__THEN:
				return getThen();
			case AlgebraPackage.SELECT_EXPRESSION__ELSE:
				return getElse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AlgebraPackage.SELECT_EXPRESSION__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends IntConstraintSystem>)newValue);
				return;
			case AlgebraPackage.SELECT_EXPRESSION__THEN:
				setThen((IntExpression)newValue);
				return;
			case AlgebraPackage.SELECT_EXPRESSION__ELSE:
				setElse((IntExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AlgebraPackage.SELECT_EXPRESSION__CONDITION:
				getCondition().clear();
				return;
			case AlgebraPackage.SELECT_EXPRESSION__THEN:
				setThen((IntExpression)null);
				return;
			case AlgebraPackage.SELECT_EXPRESSION__ELSE:
				setElse((IntExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AlgebraPackage.SELECT_EXPRESSION__CONDITION:
				return condition != null && !condition.isEmpty();
			case AlgebraPackage.SELECT_EXPRESSION__THEN:
				return then != null;
			case AlgebraPackage.SELECT_EXPRESSION__ELSE:
				return else_ != null;
		}
		return super.eIsSet(featureID);
	}

} //SelectExpressionImpl
