/**
 */
package org.polymodel.algebra.polynomials;

import org.eclipse.emf.common.util.EList;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;

import org.polymodel.algebra.affine.AffineExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polynomial Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialExpression()
 * @model
 * @generated
 */
public interface PolynomialExpression extends IntExpression, AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.polynomials.PolynomialTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialExpression_Terms()
	 * @model containment="true"
	 * @generated
	 */
	EList<PolynomialTerm> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitPolynomialExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isAffine());\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;&gt;forall(this.getTerms(), _function);'"
	 * @generated
	 */
	boolean isAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isQuasiAffine());\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;&gt;forall(this.getTerms(), _function);'"
	 * @generated
	 */
	boolean isQuasiAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.toString(&lt;%org.polymodel.algebra.OUTPUT_FORMAT%&gt;.C);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.IntExpression%&gt; _simplify = this.simplify();\nfinal &lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt; simplified = ((&lt;%org.polymodel.algebra.polynomials.PolynomialExpression%&gt;) _simplify);\nboolean _isAffine = simplified.isAffine();\nif (_isAffine)\n{\n\tint _size = simplified.getTerms().size();\n\tfinal &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt; affineTerms = new &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.affine.AffineTerm%&gt;&gt;(_size);\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt;&gt; _terms = this.getTerms();\n\tfor (final &lt;%org.polymodel.algebra.polynomials.PolynomialTerm%&gt; term : _terms)\n\t{\n\t\t{\n\t\t\t&lt;%org.polymodel.algebra.Variable%&gt; _elvis = null;\n\t\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t\t\t{\n\t\t\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt; it)\n\t\t\t\t{\n\t\t\t\t\tlong _exponent = it.getExponent();\n\t\t\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((_exponent == 1));\n\t\t\t\t}\n\t\t\t};\n\t\t\t&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt;&gt;findFirst(term.getVariables(), _function);\n\t\t\t&lt;%org.polymodel.algebra.Variable%&gt; _variable = null;\n\t\t\tif (_findFirst!=null)\n\t\t\t{\n\t\t\t\t_variable=_findFirst.getVariable();\n\t\t\t}\n\t\t\tif (_variable != null)\n\t\t\t{\n\t\t\t\t_elvis = _variable;\n\t\t\t} else\n\t\t\t{\n\t\t\t\t_elvis = null;\n\t\t\t}\n\t\t\tfinal &lt;%org.polymodel.algebra.Variable%&gt; variable = _elvis;\n\t\t\tlong _numerator = term.getNumerator();\n\t\t\tlong _denominator = term.getDenominator();\n\t\t\tfinal long coef = (_numerator / _denominator);\n\t\t\taffineTerms.add(&lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.term(coef, variable));\n\t\t}\n\t}\n\tint _size_1 = affineTerms.size();\n\tboolean _equals = (_size_1 == 0);\n\tif (_equals)\n\t{\n\t\treturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.constant(0).toAffine();\n\t}\n\telse\n\t{\n\t\treturn &lt;%org.polymodel.algebra.factory.IntExpressionBuilder%&gt;.affine(affineTerms);\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	AffineExpression toAffine();

} // PolynomialExpression
