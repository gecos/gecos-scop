/**
 */
package org.polymodel.algebra.polynomials;

import org.eclipse.emf.common.util.EList;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polynomial Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialTerm#getNumerator <em>Numerator</em>}</li>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialTerm#getDenominator <em>Denominator</em>}</li>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialTerm#getVariables <em>Variables</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialTerm()
 * @model
 * @generated
 */
public interface PolynomialTerm extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Numerator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Numerator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numerator</em>' attribute.
	 * @see #setNumerator(long)
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialTerm_Numerator()
	 * @model unique="false" dataType="org.polymodel.algebra.polynomials.long"
	 * @generated
	 */
	long getNumerator();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.polynomials.PolynomialTerm#getNumerator <em>Numerator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Numerator</em>' attribute.
	 * @see #getNumerator()
	 * @generated
	 */
	void setNumerator(long value);

	/**
	 * Returns the value of the '<em><b>Denominator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denominator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denominator</em>' attribute.
	 * @see #setDenominator(long)
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialTerm_Denominator()
	 * @model unique="false" dataType="org.polymodel.algebra.polynomials.long"
	 * @generated
	 */
	long getDenominator();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.polynomials.PolynomialTerm#getDenominator <em>Denominator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denominator</em>' attribute.
	 * @see #getDenominator()
	 * @generated
	 */
	void setDenominator(long value);

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.polynomials.PolynomialVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialTerm_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<PolynomialVariable> getVariables();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitPolynomialTerm(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (this.isQuasiAffine() &amp;&amp; ((this.getNumerator() % this.getDenominator()) == 0));'"
	 * @generated
	 */
	boolean isAffine();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean isQAffine = true;\nlong exponents = ((long) 0);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt;&gt; _variables = this.getVariables();\nfor (final &lt;%org.polymodel.algebra.polynomials.PolynomialVariable%&gt; v : _variables)\n{\n\t{\n\t\tisQAffine = (isQAffine &amp;&amp; ((v.getExponent() == 0) || (v.getExponent() == 1)));\n\t\tlong _exponents = exponents;\n\t\tlong _exponent = v.getExponent();\n\t\texponents = (_exponents + _exponent);\n\t}\n}\nreturn (isQAffine &amp;&amp; ((exponents == 0) || (exponents == 1)));'"
	 * @generated
	 */
	boolean isQuasiAffine();

} // PolynomialTerm
