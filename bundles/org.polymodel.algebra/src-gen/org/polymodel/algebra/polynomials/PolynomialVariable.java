/**
 */
package org.polymodel.algebra.polynomials;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polynomial Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialVariable#getVariable <em>Variable</em>}</li>
 *   <li>{@link org.polymodel.algebra.polynomials.PolynomialVariable#getExponent <em>Exponent</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialVariable()
 * @model
 * @generated
 */
public interface PolynomialVariable extends AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' reference.
	 * @see #setVariable(Variable)
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialVariable_Variable()
	 * @model
	 * @generated
	 */
	Variable getVariable();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.polynomials.PolynomialVariable#getVariable <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' reference.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(Variable value);

	/**
	 * Returns the value of the '<em><b>Exponent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent</em>' attribute.
	 * @see #setExponent(long)
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#getPolynomialVariable_Exponent()
	 * @model unique="false" dataType="org.polymodel.algebra.polynomials.long"
	 * @generated
	 */
	long getExponent();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.polynomials.PolynomialVariable#getExponent <em>Exponent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent</em>' attribute.
	 * @see #getExponent()
	 * @generated
	 */
	void setExponent(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitPolynomialVariable(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false" otherRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((((other == null) || (this.getVariable() == null)) || (other.getVariable() == null)))\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"Invalid input to isEquivalent: null variables\");\n}\nreturn (other.getVariable().equals(this.getVariable()) &amp;&amp; (other.getExponent() == this.getExponent()));'"
	 * @generated
	 */
	boolean isEquivalent(PolynomialVariable other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.toString(&lt;%org.polymodel.algebra.OUTPUT_FORMAT%&gt;.C);'"
	 * @generated
	 */
	String toString();

} // PolynomialVariable
