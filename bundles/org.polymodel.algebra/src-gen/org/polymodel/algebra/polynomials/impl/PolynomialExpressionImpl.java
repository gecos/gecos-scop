/**
 */
package org.polymodel.algebra.polynomials.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffineTerm;

import org.polymodel.algebra.factory.IntExpressionBuilder;

import org.polymodel.algebra.impl.IntExpressionImpl;

import org.polymodel.algebra.polynomials.PolynomialExpression;
import org.polymodel.algebra.polynomials.PolynomialTerm;
import org.polymodel.algebra.polynomials.PolynomialVariable;
import org.polymodel.algebra.polynomials.PolynomialsPackage;

import org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Polynomial Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.polynomials.impl.PolynomialExpressionImpl#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolynomialExpressionImpl extends IntExpressionImpl implements PolynomialExpression {
	/**
	 * The cached value of the '{@link #getTerms() <em>Terms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerms()
	 * @generated
	 * @ordered
	 */
	protected EList<PolynomialTerm> terms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolynomialExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PolynomialsPackage.Literals.POLYNOMIAL_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PolynomialTerm> getTerms() {
		if (terms == null) {
			terms = new EObjectContainmentEList<PolynomialTerm>(PolynomialTerm.class, this, PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS);
		}
		return terms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final AlgebraVisitor visitor) {
		visitor.visitPolynomialExpression(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAffine() {
		final Function1<PolynomialTerm, Boolean> _function = new Function1<PolynomialTerm, Boolean>() {
			public Boolean apply(final PolynomialTerm it) {
				return Boolean.valueOf(it.isAffine());
			}
		};
		return IterableExtensions.<PolynomialTerm>forall(this.getTerms(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isQuasiAffine() {
		final Function1<PolynomialTerm, Boolean> _function = new Function1<PolynomialTerm, Boolean>() {
			public Boolean apply(final PolynomialTerm it) {
				return Boolean.valueOf(it.isQuasiAffine());
			}
		};
		return IterableExtensions.<PolynomialTerm>forall(this.getTerms(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString(final OUTPUT_FORMAT format) {
		return AlgebraPrettyPrinter.print(this, format);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return this.toString(OUTPUT_FORMAT.C);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffineExpression toAffine() {
		IntExpression _simplify = this.simplify();
		final PolynomialExpression simplified = ((PolynomialExpression) _simplify);
		boolean _isAffine = simplified.isAffine();
		if (_isAffine) {
			int _size = simplified.getTerms().size();
			final ArrayList<AffineTerm> affineTerms = new ArrayList<AffineTerm>(_size);
			EList<PolynomialTerm> _terms = this.getTerms();
			for (final PolynomialTerm term : _terms) {
				{
					Variable _elvis = null;
					final Function1<PolynomialVariable, Boolean> _function = new Function1<PolynomialVariable, Boolean>() {
						public Boolean apply(final PolynomialVariable it) {
							long _exponent = it.getExponent();
							return Boolean.valueOf((_exponent == 1));
						}
					};
					PolynomialVariable _findFirst = IterableExtensions.<PolynomialVariable>findFirst(term.getVariables(), _function);
					Variable _variable = null;
					if (_findFirst!=null) {
						_variable=_findFirst.getVariable();
					}
					if (_variable != null) {
						_elvis = _variable;
					} else {
						_elvis = null;
					}
					final Variable variable = _elvis;
					long _numerator = term.getNumerator();
					long _denominator = term.getDenominator();
					final long coef = (_numerator / _denominator);
					affineTerms.add(IntExpressionBuilder.term(coef, variable));
				}
			}
			int _size_1 = affineTerms.size();
			boolean _equals = (_size_1 == 0);
			if (_equals) {
				return IntExpressionBuilder.constant(0).toAffine();
			}
			else {
				return IntExpressionBuilder.affine(affineTerms);
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS:
				return ((InternalEList<?>)getTerms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS:
				return getTerms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS:
				getTerms().clear();
				getTerms().addAll((Collection<? extends PolynomialTerm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS:
				getTerms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PolynomialsPackage.POLYNOMIAL_EXPRESSION__TERMS:
				return terms != null && !terms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PolynomialExpressionImpl
