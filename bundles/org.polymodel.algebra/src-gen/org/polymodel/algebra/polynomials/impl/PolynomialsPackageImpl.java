/**
 */
package org.polymodel.algebra.polynomials.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.polymodel.algebra.AlgebraPackage;

import org.polymodel.algebra.affine.AffinePackage;

import org.polymodel.algebra.affine.impl.AffinePackageImpl;

import org.polymodel.algebra.impl.AlgebraPackageImpl;

import org.polymodel.algebra.polynomials.PolynomialExpression;
import org.polymodel.algebra.polynomials.PolynomialTerm;
import org.polymodel.algebra.polynomials.PolynomialVariable;
import org.polymodel.algebra.polynomials.PolynomialsFactory;
import org.polymodel.algebra.polynomials.PolynomialsPackage;

import org.polymodel.algebra.quasiAffine.QuasiAffinePackage;

import org.polymodel.algebra.quasiAffine.impl.QuasiAffinePackageImpl;

import org.polymodel.algebra.reductions.ReductionsPackage;

import org.polymodel.algebra.reductions.impl.ReductionsPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PolynomialsPackageImpl extends EPackageImpl implements PolynomialsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass polynomialExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass polynomialVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass polynomialTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polymodel.algebra.polynomials.PolynomialsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PolynomialsPackageImpl() {
		super(eNS_URI, PolynomialsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PolynomialsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PolynomialsPackage init() {
		if (isInited) return (PolynomialsPackage)EPackage.Registry.INSTANCE.getEPackage(PolynomialsPackage.eNS_URI);

		// Obtain or create and register package
		PolynomialsPackageImpl thePolynomialsPackage = (PolynomialsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PolynomialsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PolynomialsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		AlgebraPackageImpl theAlgebraPackage = (AlgebraPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI) instanceof AlgebraPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI) : AlgebraPackage.eINSTANCE);
		AffinePackageImpl theAffinePackage = (AffinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI) instanceof AffinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI) : AffinePackage.eINSTANCE);
		QuasiAffinePackageImpl theQuasiAffinePackage = (QuasiAffinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(QuasiAffinePackage.eNS_URI) instanceof QuasiAffinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(QuasiAffinePackage.eNS_URI) : QuasiAffinePackage.eINSTANCE);
		ReductionsPackageImpl theReductionsPackage = (ReductionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReductionsPackage.eNS_URI) instanceof ReductionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReductionsPackage.eNS_URI) : ReductionsPackage.eINSTANCE);

		// Create package meta-data objects
		thePolynomialsPackage.createPackageContents();
		theAlgebraPackage.createPackageContents();
		theAffinePackage.createPackageContents();
		theQuasiAffinePackage.createPackageContents();
		theReductionsPackage.createPackageContents();

		// Initialize created meta-data
		thePolynomialsPackage.initializePackageContents();
		theAlgebraPackage.initializePackageContents();
		theAffinePackage.initializePackageContents();
		theQuasiAffinePackage.initializePackageContents();
		theReductionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePolynomialsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PolynomialsPackage.eNS_URI, thePolynomialsPackage);
		return thePolynomialsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolynomialExpression() {
		return polynomialExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolynomialExpression_Terms() {
		return (EReference)polynomialExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolynomialVariable() {
		return polynomialVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolynomialVariable_Variable() {
		return (EReference)polynomialVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolynomialVariable_Exponent() {
		return (EAttribute)polynomialVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolynomialTerm() {
		return polynomialTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolynomialTerm_Numerator() {
		return (EAttribute)polynomialTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolynomialTerm_Denominator() {
		return (EAttribute)polynomialTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolynomialTerm_Variables() {
		return (EReference)polynomialTermEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolynomialsFactory getPolynomialsFactory() {
		return (PolynomialsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		polynomialExpressionEClass = createEClass(POLYNOMIAL_EXPRESSION);
		createEReference(polynomialExpressionEClass, POLYNOMIAL_EXPRESSION__TERMS);

		polynomialVariableEClass = createEClass(POLYNOMIAL_VARIABLE);
		createEReference(polynomialVariableEClass, POLYNOMIAL_VARIABLE__VARIABLE);
		createEAttribute(polynomialVariableEClass, POLYNOMIAL_VARIABLE__EXPONENT);

		polynomialTermEClass = createEClass(POLYNOMIAL_TERM);
		createEAttribute(polynomialTermEClass, POLYNOMIAL_TERM__NUMERATOR);
		createEAttribute(polynomialTermEClass, POLYNOMIAL_TERM__DENOMINATOR);
		createEReference(polynomialTermEClass, POLYNOMIAL_TERM__VARIABLES);

		// Create data types
		longEDataType = createEDataType(LONG);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AlgebraPackage theAlgebraPackage = (AlgebraPackage)EPackage.Registry.INSTANCE.getEPackage(AlgebraPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		AffinePackage theAffinePackage = (AffinePackage)EPackage.Registry.INSTANCE.getEPackage(AffinePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		polynomialExpressionEClass.getESuperTypes().add(theAlgebraPackage.getIntExpression());
		polynomialExpressionEClass.getESuperTypes().add(theAlgebraPackage.getAlgebraVisitable());
		polynomialVariableEClass.getESuperTypes().add(theAlgebraPackage.getAlgebraVisitable());
		polynomialTermEClass.getESuperTypes().add(theAlgebraPackage.getAlgebraVisitable());

		// Initialize classes and features; add operations and parameters
		initEClass(polynomialExpressionEClass, PolynomialExpression.class, "PolynomialExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPolynomialExpression_Terms(), this.getPolynomialTerm(), null, "terms", null, 0, -1, PolynomialExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(polynomialExpressionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialExpressionEClass, theEcorePackage.getEBoolean(), "isAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialExpressionEClass, theEcorePackage.getEBoolean(), "isQuasiAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(polynomialExpressionEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialExpressionEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialExpressionEClass, theAffinePackage.getAffineExpression(), "toAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(polynomialVariableEClass, PolynomialVariable.class, "PolynomialVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPolynomialVariable_Variable(), theAlgebraPackage.getVariable(), null, "variable", null, 0, 1, PolynomialVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolynomialVariable_Exponent(), this.getlong(), "exponent", null, 0, 1, PolynomialVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(polynomialVariableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(polynomialVariableEClass, theEcorePackage.getEBoolean(), "isEquivalent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPolynomialVariable(), "other", 1, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(polynomialVariableEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getOUTPUT_FORMAT(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialVariableEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(polynomialTermEClass, PolynomialTerm.class, "PolynomialTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolynomialTerm_Numerator(), this.getlong(), "numerator", null, 0, 1, PolynomialTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolynomialTerm_Denominator(), this.getlong(), "denominator", null, 0, 1, PolynomialTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPolynomialTerm_Variables(), this.getPolynomialVariable(), null, "variables", null, 0, -1, PolynomialTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(polynomialTermEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAlgebraPackage.getAlgebraVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialTermEClass, theEcorePackage.getEBoolean(), "isAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(polynomialTermEClass, theEcorePackage.getEBoolean(), "isQuasiAffine", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //PolynomialsPackageImpl
