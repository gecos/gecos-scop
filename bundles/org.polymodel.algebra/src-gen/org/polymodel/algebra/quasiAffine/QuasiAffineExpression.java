/**
 */
package org.polymodel.algebra.quasiAffine;

import org.eclipse.emf.common.util.EList;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.quasiAffine.QuasiAffineExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.quasiAffine.QuasiAffinePackage#getQuasiAffineExpression()
 * @model
 * @generated
 */
public interface QuasiAffineExpression extends IntExpression, AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.quasiAffine.QuasiAffineTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see org.polymodel.algebra.quasiAffine.QuasiAffinePackage#getQuasiAffineExpression_Terms()
	 * @model containment="true"
	 * @generated
	 */
	EList<QuasiAffineTerm> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitQuasiAffineExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getTerms().toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt; expr = &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;&gt;copy(this);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt; _terms = expr.getTerms();\nfor (final &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt; term : _terms)\n{\n\t{\n\t\t&lt;%org.polymodel.algebra.IntExpression%&gt; e = term.getExpression();\n\t\te = e.substitute(substituted, newVar);\n\t\tif ((term instanceof &lt;%org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm%&gt;))\n\t\t{\n\t\t\t((&lt;%org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm%&gt;)term).setExpression(((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;) e));\n\t\t}\n\t\telse\n\t\t{\n\t\t\tif ((term instanceof &lt;%org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm%&gt;))\n\t\t\t{\n\t\t\t\t((&lt;%org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm%&gt;)term).setExpression(((&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;) e));\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tthrow new &lt;%java.lang.RuntimeException%&gt;();\n\t\t\t}\n\t\t}\n\t}\n}\nreturn expr;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt; expr = this.&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;&gt;copy();\nfinal &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt; termsToRemove = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt;();\nfinal &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt; termsToAdd = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt;&gt; _terms = expr.getTerms();\nfor (final &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineTerm%&gt; term : _terms)\n{\n\t{\n\t\t&lt;%org.polymodel.algebra.IntExpression%&gt; e = term.getExpression();\n\t\te = e.substitute(substituted, newExpr);\n\t\tif ((term instanceof &lt;%org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm%&gt;))\n\t\t{\n\t\t\tif ((e instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;))\n\t\t\t{\n\t\t\t\t((&lt;%org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm%&gt;)term).setExpression(((&lt;%org.polymodel.algebra.affine.AffineExpression%&gt;)e));\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tif ((e instanceof &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;))\n\t\t\t\t{\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tthrow new &lt;%java.lang.RuntimeException%&gt;(((\"Cannot handle expression \" + e) + \" in substitute\"));\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\tif ((term instanceof &lt;%org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm%&gt;))\n\t\t\t{\n\t\t\t\tif ((e instanceof &lt;%org.polymodel.algebra.affine.AffineExpression%&gt;))\n\t\t\t\t{\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tif ((e instanceof &lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;))\n\t\t\t\t\t{\n\t\t\t\t\t\t((&lt;%org.polymodel.algebra.quasiAffine.NestedQuasiAffineTerm%&gt;)term).setExpression(((&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineExpression%&gt;)e));\n\t\t\t\t\t}\n\t\t\t\t\telse\n\t\t\t\t\t{\n\t\t\t\t\t\tthrow new &lt;%java.lang.RuntimeException%&gt;(((\"Cannot handle expression \" + e) + \" in substitute\"));\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tthrow new &lt;%java.lang.RuntimeException%&gt;();\n\t\t\t}\n\t\t}\n\t}\n}\nexpr.getTerms().removeAll(termsToRemove);\nexpr.getTerms().addAll(termsToAdd);\nreturn expr;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

} // QuasiAffineExpression
