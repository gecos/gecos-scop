/**
 */
package org.polymodel.algebra.quasiAffine;

import org.polymodel.algebra.AlgebraVisitor;

import org.polymodel.algebra.affine.AffineExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Quasi Affine Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.quasiAffine.QuasiAffinePackage#getSimpleQuasiAffineTerm()
 * @model
 * @generated
 */
public interface SimpleQuasiAffineTerm extends QuasiAffineTerm {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(AffineExpression)
	 * @see org.polymodel.algebra.quasiAffine.QuasiAffinePackage#getSimpleQuasiAffineTerm_Expression()
	 * @model containment="true"
	 * @generated
	 */
	AffineExpression getExpression();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.quasiAffine.SimpleQuasiAffineTerm#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(AffineExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimpleQuasiAffineTerm(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.polymodel.algebra.quasiAffine.QuasiAffineOperator%&gt; _operator = this.getOperator();\n&lt;%java.lang.String%&gt; _plus = (_operator + \"(\");\n&lt;%org.polymodel.algebra.affine.AffineExpression%&gt; _expression = this.getExpression();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _expression);\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \",\");\nlong _coef = this.getCoef();\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + &lt;%java.lang.Long%&gt;.valueOf(_coef));\nreturn (_plus_3 + \")\");'"
	 * @generated
	 */
	String toString();

} // SimpleQuasiAffineTerm
