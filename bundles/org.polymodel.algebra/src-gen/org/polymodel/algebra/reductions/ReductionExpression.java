/**
 */
package org.polymodel.algebra.reductions;

import org.eclipse.emf.common.util.EList;

import org.polymodel.algebra.AlgebraVisitable;
import org.polymodel.algebra.AlgebraVisitor;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.OUTPUT_FORMAT;
import org.polymodel.algebra.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reduction Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polymodel.algebra.reductions.ReductionExpression#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link org.polymodel.algebra.reductions.ReductionExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see org.polymodel.algebra.reductions.ReductionsPackage#getReductionExpression()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface ReductionExpression extends IntExpression, AlgebraVisitable {
	/**
	 * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link org.polymodel.algebra.IntExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expressions</em>' containment reference list.
	 * @see org.polymodel.algebra.reductions.ReductionsPackage#getReductionExpression_Expressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<IntExpression> getExpressions();

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polymodel.algebra.reductions.ReductionOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see org.polymodel.algebra.reductions.ReductionOperator
	 * @see #setOperator(ReductionOperator)
	 * @see org.polymodel.algebra.reductions.ReductionsPackage#getReductionExpression_Operator()
	 * @model unique="false" required="true"
	 * @generated
	 */
	ReductionOperator getOperator();

	/**
	 * Sets the value of the '{@link org.polymodel.algebra.reductions.ReductionExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see org.polymodel.algebra.reductions.ReductionOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ReductionOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitReductionExpression(this);'"
	 * @generated
	 */
	void accept(AlgebraVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _string = this.getOperator().toString();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; _expressions = this.getExpressions();\nreturn (_string + _expressions);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" formatUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.polymodel.algebra.prettyprinter.algebra.AlgebraPrettyPrinter%&gt;.print(this, format);'"
	 * @generated
	 */
	String toString(OUTPUT_FORMAT format);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newExprUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getExpressions().size();\nfinal &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; newExprs = new &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;(_size);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; _expressions = this.getExpressions();\nfor (final &lt;%org.polymodel.algebra.IntExpression%&gt; expr : _expressions)\n{\n\tnewExprs.add(expr.substitute(substituted, newExpr));\n}\nfinal &lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt; copy = this.&lt;&lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt;&gt;copy();\ncopy.getExpressions().clear();\ncopy.getExpressions().addAll(newExprs);\nreturn copy;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, IntExpression newExpr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" substitutedUnique="false" newVarUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getExpressions().size();\nfinal &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; newExprs = new &lt;%java.util.ArrayList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt;(_size);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.polymodel.algebra.IntExpression%&gt;&gt; _expressions = this.getExpressions();\nfor (final &lt;%org.polymodel.algebra.IntExpression%&gt; expr : _expressions)\n{\n\tnewExprs.add(expr.substitute(substituted, newVar));\n}\nfinal &lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt; copy = this.&lt;&lt;%org.polymodel.algebra.reductions.ReductionExpression%&gt;&gt;copy();\ncopy.getExpressions().clear();\ncopy.getExpressions().addAll(newExprs);\nreturn copy;'"
	 * @generated
	 */
	IntExpression substitute(Variable substituted, Variable newVar);

} // ReductionExpression
