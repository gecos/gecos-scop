package org.polymodel.algebra.util;

import org.polymodel.algebra.ComparisonOperator;

public class ComparisonOperatorUtils {
	
	public static ComparisonOperator getOpposite(ComparisonOperator comparisonOperator) {
		switch (comparisonOperator) {
		case EQ: return ComparisonOperator.NE;
		case NE: return ComparisonOperator.EQ;
		case LT: return ComparisonOperator.GE;
		case LE: return ComparisonOperator.GT;
		case GT: return ComparisonOperator.LE;
		case GE: return ComparisonOperator.LT;
		default:
			throw new RuntimeException();
		}
	}
	
	/**
	 * Returns the Comparison Operator corresponding to the multiplication 
	 * of a constraint by a negative number.
	 */
	public static ComparisonOperator negateOperator(ComparisonOperator op) {
		switch(op) {
		case LT: return ComparisonOperator.GT;
		case LE: return ComparisonOperator.GE;
		case GE: return ComparisonOperator.LE;
		case GT: return ComparisonOperator.LT;
		default: return op;
		}
	}
	
	public static String toMathString(ComparisonOperator comparisonOperator) {
		switch (comparisonOperator) {
		case EQ: return "=";
		case NE: return "!=";
		case LT: return "<";
		case LE: return "<=";
		case GT: return ">";
		case GE: return ">=";
		default:
			throw new RuntimeException();
		}
	}
	
	public String getCString(ComparisonOperator comparisonOperator) {
		switch (comparisonOperator) {
		case EQ: return "==";
		case NE: return "!=";
		case LT: return "<";
		case LE: return "<=";
		case GT: return ">";
		case GE: return ">=";
		default: return comparisonOperator.getName();
		}
	}
}
