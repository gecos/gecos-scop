package org.polymodel.algebra.util;

import org.polymodel.algebra.IntConstraint;
import org.polymodel.algebra.IntExpression;
import org.polymodel.algebra.Variable;
import org.polymodel.algebra.affine.AffineExpression;
import org.polymodel.algebra.affine.AffineTerm;
import org.polymodel.algebra.factory.IntExpressionBuilder;
import org.polymodel.algebra.quasiAffine.QuasiAffineExpression;
import org.polymodel.algebra.quasiAffine.QuasiAffineTerm;
import org.polymodel.algebra.reductions.ReductionExpression;

public class IntExpressionUtils {
	
	public static long getCoefficientOf(IntConstraint c, Variable var) {
		return getCoefficientOfIn(c, var, allToLHS(c).simplify().getLhs());
	}
	
	/**
	 * Moves RHS to LHS so that the RHS expression is 0.
	 */
	private static IntConstraint allToLHS(IntConstraint c) {
		IntConstraint expr = c.copy();
		
		expr.setLhs(IntExpressionBuilder.sub(expr.getLhs(), expr.getRhs()));
		expr.setRhs(IntExpressionBuilder.constant(0));
		
		return expr;
	}

	public static long getCoefficientOfIn(IntConstraint c, Variable var, IntExpression expr) {
		long targetCoef = 0;
	
		if(expr.toAffine() != null){
			AffineExpression affine = expr.toAffine();
			for (AffineTerm term : affine.getTerms()) {
				if (term.getVariable() != null && term.getVariable().equals(var)) {
					targetCoef = term.getCoef();
					break;
				}
			}
		}	
		else if(expr.toQuasiAffine() != null){
			QuasiAffineExpression qaffine = expr.toQuasiAffine();
			for (QuasiAffineTerm term : qaffine.getTerms()) {
				targetCoef = getCoefficientOfIn(c, var, term.getExpression());
				if (targetCoef != 0) {
					break;
				}
			}
		}
		else if(expr.toReduction() != null){
			ReductionExpression red = expr.toReduction();
			for (IntExpression e : red.getExpressions()) {
				targetCoef = getCoefficientOfIn(c, var, e);
				if (targetCoef != 0) {
					break; 
				}
			}
		}
		else {
			throw new RuntimeException("polynomial expressions are not handled at the moment.");
		}
		
		return targetCoef;
	}

	/**
	 * Returns the expression with the term that multiplies the Variable specified removed.
	 * If the negate flag is true, negates all other terms.
	 * 
	 * @param var
	 * @param negate
	 * @return
	 */
	public static AffineExpression getExpressionWithout(IntConstraint c, Variable var, boolean negate) {
		long targetCoef = 0;

		//Construct the term without the target variable
		AffineExpression Bexpr = IntExpressionBuilder.affine();

		IntExpression expr = allToLHS(c).simplify().getLhs();
		
		try {
			AffineExpression affine = expr.toAffine();
			for (AffineTerm term : affine.getTerms()) {
				//Constant
				if (term.getVariable() == null) {
					Bexpr.getTerms().add(IntExpressionBuilder.term(term.getCoef(), term.getVariable()));
				//linear term
				} else if (!term.getVariable().equals(var)) {
					Bexpr.getTerms().add(IntExpressionBuilder.term(term.getCoef(), term.getVariable()));
				//When it is the target variable, keep track of its coefficient
				} else {
					targetCoef = term.getCoef();
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Only affine expressions are handled at the moment.");
		}
		
		//If the target was not involved in the expression, skip
		if (targetCoef == 0) {
			return null;
		}
		
		//When no term is set, give 0
		if (Bexpr.getTerms().size() == 0) {
			Bexpr.getTerms().add(IntExpressionBuilder.term(0));
		}
		
		//When specified negate all terms
		//This is for lower bounds
		if (negate) {
			for (AffineTerm term : Bexpr.getTerms()) {
				term.setCoef(term.getCoef()*-1);
			}
		}
		
		
		return Bexpr;
	}
}
