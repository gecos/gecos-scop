## Release version 0.3.5 (13 fév 2019)

* modified feature description
* prepare version v0.3.3
* - Yet another target mapping change
* Merge branch 'gecos-scop-argointegration' of https://gitlab.inria.fr/gecos/gecos-scop.git into gecos-scop-argointegration
* - Early support for region reads - Pattern matching for (abstract) sum and sop vector instructions
* - changed target to gecos core 0.7.8
* Update fr.irisa.cairn.gecos.scop.target.target (again)
* - Try to fix target bug when loading CDT
* Change repository URL for eclipse oxygen
* - added constructor
* - Added stub for while conversion using predicated as in Bastoul CC'04 paper.
* - added xtend-gen files
* - added java generated files
* Refactored Gecos scop passes : added method to operate directly at the GecosScopBlock level
* - Added adapters to support the construction of control flow edges along SCoP blocks.
* - Made GecosScopBlock inherit from BasicBlovck to allow building control-flow
* - Minor changes to Datamotion pass
* - Add toShortString method to GecosScopBlock
* - Fixed bugs preventing the merging of scops when they appear as contiguous in composite block
* start working on next version SNAPSHOT
---
