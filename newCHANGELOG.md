## Release version 0.3.3 (08 fév 2019)

* - Yet another target mapping change
* Merge branch 'gecos-scop-argointegration' of https://gitlab.inria.fr/gecos/gecos-scop.git into gecos-scop-argointegration
* - Early support for region reads - Pattern matching for (abstract) sum and sop vector instructions
* - changed target to gecos core 0.7.8
* Update fr.irisa.cairn.gecos.scop.target.target (again)
* - Try to fix target bug when loading CDT
* Change repository URL for eclipse oxygen
* - added constructor
* - Added stub for while conversion using predicated as in Bastoul CC'04 paper.
* - added xtend-gen files
* - added java generated files
* Refactored Gecos scop passes : added method to operate directly at the GecosScopBlock level
* - Added adapters to support the construction of control flow edges along SCoP blocks.
* - Made GecosScopBlock inherit from BasicBlovck to allow building control-flow
* - Minor changes to Datamotion pass
* - Add toShortString method to GecosScopBlock
* - Fixed bugs preventing the merging of scops when they appear as contiguous in composite block
* start working on next version SNAPSHOT
---
# Changelog


## Release version 0.3.1 (28 sep 2018)

* Changed target mapping to use Gecos core 0.7.6
* - Added test script - Added method to build Id schedule from a collection of statements
* Fix domain constructor to construct context as well as domain
* Added automatic extraction of SimpleForBlock from ForBlock in Gecos IR
* Added comments explaining how to bound the sliding window size
* Added comments explaining how to bound the sliding window size
* start working on next version SNAPSHOT

## Release version 0.3.0 (24 sept. 2018)

* Update jni isl and core dependencies to latest
* - Added support for FSM code generation following B&F approach. - Early support for unexpanded node in code generator
* Merge with develop
* Add test for Slicer
* Remove procedure symbol from liveness analysis
* Do not try to contract non array types (i.e. scalar).
* Fix initialization of array
* Merge branch 'ScopConverterInitialization' into 'develop'
* Modified code generator to support B&F FSMs and partially unexpanded ScopNode for hierarchical code generation
* Modified model representing B&F FSM
* Correct symbols passed for liveness analysis, and add test
* Merge branch 'ScopConverterInitialization' into 'develop'
* Fix ScopConverter bugs on initialization and symbols definition.
* Update JNI mapper dependency
* Move Gecos Scop to latest ISL
* More explicit guard on number of outputs for slicer.
* Fix DuplicateNode Pass on parameterized domains.
* Move xtext version back to 2.12
* Add pragma to slicer, integrate data mover and unrolling.
* Modularize ScopSlicer and change API to slice on ScopNode.
* Modularize ScopDuplicateNode and uniquefy symbols duplicated.
* Rewrite duplicate node pass to use polyhedral domains for selecting guards. Fix domain construction on strided values.
* Add missing generated java file
* Added java files in addition to xtend files (see previous commit)
* - fixed bug when dealing with scope having one output arrays, but more than one live-out symbols (ex : scalars or arrays untouched by the scop)
* Make sure we only contract local arrays (the pass would annotate scalars and global symbols)
* - Modified Scop model to handle ScopBlockStatement and ScopInstruction statements - Refactored ScopExtractor (introduced xtend classes) - Fixed dimension management bug in ScopISLCodegen - Improved ScopDeadCodeElimination - Added functions to build PRDG with ScopRead/ScopWrite relations instead of Statement level relations
* Reduce guard complexity for cleaner codegen without ISLCodegen cleanup.
* Fix schedule generation for stride and domain construction.
* Fixes for strided loops and slicing.
* Add support for decrementing for loops in Scop
* Add safety checks to tile prdg computation
* Remove copies from slicer, creates sliced prdg directly
* [Fix] Update dependencies and fix missing APIs from ISL
* Improved SCOPExtracion to operate on block in addition to procedure (this is to ease integration with emmtrix framework)
* Add scop iterator normalizer to cleanup between transformations
* Fix containment issues linked to bug in ScopSlicer
---

## Release version 0.2.0 (20 juin 2018)

* Add support for single time on given dimension, plus stencil tests
* Fix update remover for multiple dimension array and enable tests with ScopISLCodegen
* Rewrite encloseWithLoop Method, fix AST converter to use existentials in ScopUnexpandedStatement
* Rewrite ScopSlicer and add integration tests
* Fix domain and context building to return either a zero dimensionned space or parameter constraints
* Cleanup update remover to reuse SSA implementation. Fix node domain computation to return correct empty domain
* Merge branch 'scop-relative-analysis' of gitlab.inria.fr:gecos/gecos-scop into scop-relative-analysis
* Update dependencies, fix liveness analysis update function adding and increase test coverage
* New version of SCoP slicer
* fixed bug in ISL Domain construction for loop nests without parameters
* fixed bug in ISL Domain construction for loop nests without parameters
* Added pass to remove update() instructions
* Added pass to remove update() instructions
* Update scop dependencies target to latest, and commit changes in generated files
* Add threshold and safeguard for dead code to array contraction
* Improve FlowOut with relative analysis support and additional tests
* Fixed Pragma parser
* Merge with mickaël data-motion
* Fix FlowOut to be less conservative. Creates copies of all inspected variables in DataMotion to create local copies
* Add Duplicate node module to gecos script
* Add support for non-unit stride on DomainConstraintCollector. Improve DataMotion support for multiple copies of the same symbol
* Add Duplicate node pragma, transform and integration tests
* Fix DomainConstraintCollector which transformed an union into a disjunction
* Fix memory layout and add tests
* Fix bugs in memory contraction and scop converter
* Add method to request iterators on demand using a normalized naming
* Add modulo after array contraction only when needed
* Move Array Contraction to relative, and first tests using DataMotion
* Fix scop pragma build after xcore model move
* Move Pragma directives to ScopTransform, and update DataMotion to use created directive
* Fixed bug in directive extraction
* Fix Scop pragma generation
* Merge branch 'scop-relative-analysis' of https://gitlab.inria.fr/gecos/gecos-scop into scop-relative-analysis
* Added support for data motion pragma scop_motion_pragma
* Correct test ressources naming
* Add script module to call DataMotionPass
* Remove pragma after DataMotionPass
* Refactor DataMotionTransformer and add list of symbols to copy
* Move Scop towards relative analysis and in place modification to ease containment

## Release version 0.1.0 (04 avril 2018)

initial version


---
