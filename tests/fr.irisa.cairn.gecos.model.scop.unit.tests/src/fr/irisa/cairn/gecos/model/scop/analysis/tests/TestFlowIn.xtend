package fr.irisa.cairn.gecos.model.scop.analysis.tests

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory
import gecos.core.Symbol
import org.junit.Assert
import org.junit.Test
import fr.irisa.cairn.gecos.model.scop.unit.tests.utils.MockGecosScopBlocks

class TestFlowIn {
	
	@Test
	def void testFlowIn2DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < 100 }", #[100, 100])
		val srcSym = copy.scope.lookup("src")
		
		verifyFlowIn(srcSym, copy, "{ src[i,j] : 0 <= i < 100 and 0 <= j < 100 }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowIn(srcSym, innerLoop, "[i] -> { src[i,j] : 0 <= j < 100 }")
	}
		
	@Test
	def void testFlowInParam2DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("[N] -> { S[i,j] : 0 <= i < N and 0 <= j < N }", #[100, 100])
		val srcSym = copy.scope.lookup("src")
		
		verifyFlowIn(srcSym, copy, "[N] -> { src[i,j] : 0 <= i < N and 0 <= j < N }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowIn(srcSym, innerLoop, "[N,i] -> { src[i,j] : 0 <= j < N }")
	}
	
	@Test
	def void testFlowInTriangleCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < i }", #[100, 100])
		val srcSym = copy.scope.lookup("src")
		
		verifyFlowIn(srcSym, copy, "{ src[i,j] : 0 <= i < 100 and 0 <= j < i }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowIn(srcSym, innerLoop, "[i] -> { src[i,j] : 0 <= j < i }")
	}
	
	@Test
	def void testFlowIn3DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", #[50, 200, 100])
		val srcSym = copy.scope.lookup("src")
		
		verifyFlowIn(srcSym, copy, "{ src[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowIn(srcSym, innerLoop, "[i,j] -> { src[i,j,k] : 0 <= k < 100 }")		
		verifyFlowIn(srcSym, innerLoop.parentScop, "[i] -> { src[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }")		
	}
	
	private def void verifyFlowIn(Symbol sym, ScopNode node, String ref) {
		Assert.assertNotNull("Cannot compute FlowIn on null symbol", sym);
		val flowIn = ScopAccessAnalyzer.computeFlowIn(sym, node)
		val flowInRef = ISLFactory.islSet(ref)
		Assert.assertTrue('''Expected «flowInRef» instead of «flowIn»''',flowIn.isEqual(flowInRef))
	}
}