package fr.irisa.cairn.gecos.model.scop.analysis.tests

import fr.irisa.cairn.gecos.model.scop.ScopNode
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory
import gecos.core.Symbol
import org.junit.Assert
import org.junit.Test
import fr.irisa.cairn.gecos.model.scop.unit.tests.utils.MockGecosScopBlocks

class TestFlowOut {
	
	@Test
	def void testFlowOut2DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < 100 }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")
		copy.liveOutSymbols.add(snkSym)
		
		verifyFlowOut(snkSym, copy, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < 100 }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < 100 }")
	}
	
	@Test
	def void testFlowOutParam2DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("[N] -> { S[i,j] : 0 <= i < N and 0 <= j < N }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")
		copy.liveOutSymbols.add(snkSym)
		
		verifyFlowOut(snkSym, copy, "[N] -> { snk[i,j] : 0 <= i < N and 0 <= j < N }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowOut(snkSym, innerLoop, "[N,i] -> { snk[i,j] : 0 <= j < N }")
	}
	
	@Test
	def void testFlowOutTriangleCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < i }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")
		copy.liveOutSymbols.add(snkSym)
		
		verifyFlowOut(snkSym, copy, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < i }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < i }")
	}
	
	@Test
	def void testFlowOut3DCopy() {
		val copy = MockGecosScopBlocks.mockCopy("{ S[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", #[50, 200, 100])
		val snkSym = copy.scope.lookup("snk")
		copy.liveOutSymbols.add(snkSym)
		
		verifyFlowOut(snkSym, copy, "{ snk[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }")
		var innerLoop = copy.listAllStatements.get(0).parentScop 
		verifyFlowOut(snkSym, innerLoop, "[i,j] -> { snk[i,j,k] : 0 <= k < 100 }")		
		verifyFlowOut(snkSym, innerLoop.parentScop, "[i] -> { snk[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }")		
	}
	
	@Test
	def void testFlowOut2DDualCopy() {
		val copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j] : 0 <= i < 100 and 0 <= j < 100 }",
			"{ S2[i,j] : 0 <= i < 100 and 0 <= j < 100 }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")

		val innerLoop = copy.listAllStatements.findFirst[it.id == "S1"].parentScop
		verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < 100 }")
		verifyFlowOut(snkSym, innerLoop.parentScop, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < 100 }")
	}
	
	@Test
	def void testFlowOutParam2DDualCopy() {
		val copy = MockGecosScopBlocks.mockCopyCopy("[N] -> { S1[i,j] : 0 <= i < N and 0 <= j < N }",
			"[N] -> { S1[i,j] : 0 <= i < N and 0 <= j < N }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")

		var innerLoop = copy.listAllStatements.get(0).parentScop
		verifyFlowOut(snkSym, innerLoop, "[N,i] -> { snk[i,j] : 0 <= j < N }")
		verifyFlowOut(snkSym, innerLoop.parentScop, "[N] -> { snk[i,j] : 0 <= i < N and 0 <= j < N }")
	}
	
	@Test
	def void testFlowOutTriangleDualCopy() {
		val copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j] : 0 <= i < 100 and 0 <= j < 100 }", "{ S2[i,j] : 0 <= i < 100 and 0 <= j < i }", #[100, 100])
		val snkSym = copy.scope.lookup("snk")
		
		val innerLoop = copy.listAllStatements.findFirst[it.id == "S1"].parentScop 
		verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < i }")
		verifyFlowOut(snkSym, innerLoop.parentScop, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < i }")
	}
	
	@Test
	def void testFlowOut3DDualCopy() {		
		val copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", "{ S2[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", #[50, 200, 100])
		val snkSym = copy.scope.lookup("snk")
		
		val innerLoop = copy.listAllStatements.findFirst[it.id == "S1"].parentScop 
		verifyFlowOut(snkSym, innerLoop, "[i,j] -> { snk[i,j,k] : 0 <= k < 100 }")		
		verifyFlowOut(snkSym, innerLoop.parentScop, "[i] -> { snk[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }")
		verifyFlowOut(snkSym, innerLoop.parentScop.parentScop, "{ snk[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }")
	}
	
	private def void verifyFlowOut(Symbol sym, ScopNode node, String ref) {
		Assert.assertNotNull("Cannot compute FlowOut on null symbol", sym);
		val flowOut = ScopAccessAnalyzer.computeFlowOut(sym, node)
		val flowOutRef = ISLFactory.islSet(ref)
		Assert.assertTrue('''Expected «flowOutRef» instead of «flowOut»''',flowOut.isSubset(flowOutRef))
	}
}