package fr.irisa.cairn.gecos.model.scop.tests

import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean
import org.junit.Test

import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.*

class TestClosure {
	
	@Test 
	def void testEx2() {
		val JNIISLDimType a =null;
		
		val dom = islUnionMap('''
			[] -> { 
				S4[i]->S1[k] : 0<=i<=100 and i-3<=k<=i+3 ;
				S1[i]->S0[k] : 0<=i<=100 and i-2<=k<=i+2 ;
				
			}''');
		val JNIPtrBoolean v = new JNIPtrBoolean();
		val r = dom.transitiveClosure(v);
		val t = '''
			{ 
				S4[i] -> S1[k] : 5 <= i <= 9 and -3 + i <= k <= 3 + i; 
				S4[i] -> S0[k] : 5 <= i <= 9 and -5 + i <= k <= 5 + i; 
				S1[i] -> S0[k] : 0 <= i <= 100 and -2 + i <= k <= 2 + i
			}

		'''
		
		println(r)
		
		//TODO ?
	}
}