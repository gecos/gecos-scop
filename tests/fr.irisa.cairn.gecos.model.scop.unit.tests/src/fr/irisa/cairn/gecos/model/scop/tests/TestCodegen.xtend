package fr.irisa.cairn.gecos.model.scop.tests

import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter
import fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import org.junit.Test

import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islSet
import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islUnionMap
import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islUnionSet

class TestCodegen {
	
	def codegen(JNIISLSet context, JNIISLUnionMap umap) {
		var JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context)
		var JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, umap)
		//println(root)
//		JNIISLASTToScopConverter.adapt(root)
	}

//	@Test def void test0() {
//		var JNIISLSet context = islSet("[M,N]-> { : M>=1 & N >= 1 }")
//		var JNIISLUnionMap umap = islUnionMap('''
//			[N,M] -> { 
//				A[i,j] -> [i,j] : 0 <= i <= N and 0 <= j <= M ;
//				B[i,j] -> [i+2,j] : 0 <= i <= j and 0 <= j <= M
//			}''')
//		var print =  ScopPrettyPrinter.print(codegen(context,umap))
//		System.out.println("*"+print+"*");
//	}

	@Test def void test1() {
		var JNIISLSet context = islSet("[M,N]-> { : M>=1 & N >= 1 }")
		var JNIISLUnionMap umap = islUnionMap('''
			[N,M] -> { 
				A[j] -> [2j] : 0 <= j <= M ;
				B[j] -> [j] : 0 <= j <= M 
			}''')
			
//		var print =  ScopPrettyPrinter.print(codegen(context,umap))
//		System.out.println("*"+print+"*");
	}
	
	@Test 
	def void testEx2() {
		val dom = islUnionSet("[N,M] -> { S1[i,j] : 0<=i<N and 0<=j<M; S2[i,j] : 0<=i<N and 0<=j<M }");
		val sch = islUnionMap("[N, M] -> { S1[i, j] -> [i]; S2[i,j] -> [i+1] }");
		//JNIISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i,j] }");
		
		val JNIISLSet context = islSet("[M,N]-> { : }")
		
		val build = JNIISLASTBuild.buildFromContext(context);
		val node = build.generateWithExpansionNodes(sch.intersectDomain(dom), "EX");
		System.out.println(node.toString());
//		print(ScopPrettyPrinter.print(JNIISLASTToScopConverter.adapt(node)));
		
	}

	@Test 
	def void testEx3() {
		var JNIISLSet context = islSet("[]-> { : }")
		var JNIISLUnionMap sched = islUnionMap('''
		{ 
			S1[i, j] -> [i,j,0] : 0 <= i <= 255 and 0 <= j <= 255; 
			S0[i, j] -> [i,j,1] : 0 <= i <= 255 and 0 <= j <= 255 
		}
		''')
			
//		var print =  ScopPrettyPrinter.print(codegen(context,sched))
//		System.out.println("*"+print+"*");
		
	}
}
