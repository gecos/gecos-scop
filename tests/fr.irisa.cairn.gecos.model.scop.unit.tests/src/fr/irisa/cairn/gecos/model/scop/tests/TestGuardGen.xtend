package fr.irisa.cairn.gecos.model.scop.tests

import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType
import org.junit.Test

import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islSet
//import fr.irisa.cairn.gecos.model.scop.codegen.converter.ISLScopConverter

class TestGuardGen {
	
		@Test 
	def void testEx2() {
		 val JNIISLDimType a =null;
		
		val dom = islSet("[N,M] -> { S[i,j] : 0<=i<N and 0<=j<M or N+4<=i<M }");

		val params = #[
			ScopUserFactory.dim("M"),
			ScopUserFactory.dim("N")
		];
		val idx = #[
			ScopUserFactory.dim("i"),
			ScopUserFactory.dim("j")
		];
				
//		val r = ISLScopConverter.convert(dom,params,idx);
//
//		
//		println(r)
		
	}
}