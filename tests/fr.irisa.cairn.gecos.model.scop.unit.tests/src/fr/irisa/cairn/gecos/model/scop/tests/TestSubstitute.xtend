package fr.irisa.cairn.gecos.model.scop.tests

import fr.irisa.cairn.gecos.model.scop.codegen.converter.JNIISLASTToScopConverter
import fr.irisa.cairn.gecos.model.scop.util.ScopPrettyPrinter
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode
import fr.irisa.cairn.jnimap.isl.jni.JNIISLContext
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap
import org.junit.Test

import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islSet
import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islUnionMap
import static fr.irisa.cairn.jnimap.isl.jni.ISLFactory.islUnionSet

class TestSubstitute {
	
	def codegen(JNIISLSet context, JNIISLUnionMap umap) {
		var JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context)
		var JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, umap)
		//println(root)
//		JNIISLASTToScopConverter.adapt(root)
	}

//	@Test def void test0() {
//		var JNIISLSet context = islSet("[M,N]-> { : M>=1 & N >= 1 }")
//		var JNIISLUnionMap umap = islUnionMap('''
//			[N,M] -> { 
//				A[i,j] -> [i,j] : 0 <= i <= N and 0 <= j <= M ;
//				B[i,j] -> [i+2,j] : 0 <= i <= j and 0 <= j <= M
//			}''')
//		var print =  ScopPrettyPrinter.print(codegen(context,umap))
//		System.out.println("*"+print+"*");
//	}



	@Test 
	def void testEx3() {
		var JNIISLSet context = islSet("[I,J]-> { : }")
		var JNIISLUnionMap sched = islUnionMap('''
		[I,J] -> { 
			S0[i, j] -> S0[i,j] : I=2i and I=j; 
		}
		''')
			
//		var print =  ScopPrettyPrinter.print(codegen(context,sched))
//		System.out.println("*"+print+"*");
		
	}
}
