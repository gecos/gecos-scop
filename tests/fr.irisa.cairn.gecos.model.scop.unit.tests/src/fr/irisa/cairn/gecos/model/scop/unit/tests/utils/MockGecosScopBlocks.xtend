package fr.irisa.cairn.gecos.model.scop.unit.tests.utils

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory
import fr.irisa.cairn.gecos.model.scop.ScopDimension
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory
import fr.irisa.cairn.gecos.model.scop.ScopFactory
import fr.irisa.cairn.gecos.model.scop.ScopBlock

class MockGecosScopBlocks {
	static public def mockCopy(String copyDomain, long[] sizes) {
		val block = ScopFactory.eINSTANCE.createScopBlock
		val root = ScopUserFactory.root(block)
		ScopUserFactory.scope = root.scope
		GecosUserTypeFactory.scope = root.scope
		
		val copySet = ISLFactory.islSet(copyDomain)
		root.iterators.addAll(copySet.indicesNames.map[ScopUserFactory.dim(it)])
		root.parameters.addAll(copySet.parametersNames.map[ScopUserFactory.param(it) as ScopDimension])
		val srcSym = GecosUserCoreFactory.symbol("src", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT, sizes.length, sizes), root.scope)
		val snkSym = GecosUserCoreFactory.symbol("snk", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT, sizes.length, sizes), root.scope)
		ISLCodegenUtils.dataMover(copySet, block, srcSym, snkSym)
		
		root
	}
	
	static public def mockCopyCopy(String copyDomain, String copyBisDomain, long[] sizes) {
		val root = mockCopy(copyDomain, sizes)
		val snkSym = root.scope.lookup("snk")
		
		val copyBisSet = ISLFactory.islSet(copyBisDomain)
		val snkBisSym = GecosUserCoreFactory.symbol("snkBis", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT, sizes.length, sizes), root.scope)
		ISLCodegenUtils.dataMover(copyBisSet, root.root as ScopBlock, snkSym, snkBisSym)
		
		root
	}
}
