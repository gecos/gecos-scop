package fr.irisa.cairn.gecos.model.scop.analysis.tests;

import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.unit.tests.utils.MockGecosScopBlocks;
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import gecos.core.Symbol;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class TestFlowIn {
  @Test
  public void testFlowIn2DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < 100 }", new long[] { 100, 100 });
    final Symbol srcSym = copy.getScope().lookup("src");
    this.verifyFlowIn(srcSym, copy, "{ src[i,j] : 0 <= i < 100 and 0 <= j < 100 }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowIn(srcSym, innerLoop, "[i] -> { src[i,j] : 0 <= j < 100 }");
  }
  
  @Test
  public void testFlowInParam2DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("[N] -> { S[i,j] : 0 <= i < N and 0 <= j < N }", new long[] { 100, 100 });
    final Symbol srcSym = copy.getScope().lookup("src");
    this.verifyFlowIn(srcSym, copy, "[N] -> { src[i,j] : 0 <= i < N and 0 <= j < N }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowIn(srcSym, innerLoop, "[N,i] -> { src[i,j] : 0 <= j < N }");
  }
  
  @Test
  public void testFlowInTriangleCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < i }", new long[] { 100, 100 });
    final Symbol srcSym = copy.getScope().lookup("src");
    this.verifyFlowIn(srcSym, copy, "{ src[i,j] : 0 <= i < 100 and 0 <= j < i }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowIn(srcSym, innerLoop, "[i] -> { src[i,j] : 0 <= j < i }");
  }
  
  @Test
  public void testFlowIn3DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", new long[] { 50, 200, 100 });
    final Symbol srcSym = copy.getScope().lookup("src");
    this.verifyFlowIn(srcSym, copy, "{ src[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowIn(srcSym, innerLoop, "[i,j] -> { src[i,j,k] : 0 <= k < 100 }");
    this.verifyFlowIn(srcSym, innerLoop.getParentScop(), "[i] -> { src[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }");
  }
  
  private void verifyFlowIn(final Symbol sym, final ScopNode node, final String ref) {
    Assert.assertNotNull("Cannot compute FlowIn on null symbol", sym);
    final JNIISLSet flowIn = ScopAccessAnalyzer.computeFlowIn(sym, node);
    final JNIISLSet flowInRef = ISLFactory.islSet(ref);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Expected ");
    _builder.append(flowInRef);
    _builder.append(" instead of ");
    _builder.append(flowIn);
    Assert.assertTrue(_builder.toString(), flowIn.isEqual(flowInRef));
  }
}
