package fr.irisa.cairn.gecos.model.scop.analysis.tests;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopStatement;
import fr.irisa.cairn.gecos.model.scop.analysis.ScopAccessAnalyzer;
import fr.irisa.cairn.gecos.model.scop.unit.tests.utils.MockGecosScopBlocks;
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import gecos.core.Symbol;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class TestFlowOut {
  @Test
  public void testFlowOut2DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < 100 }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    copy.getLiveOutSymbols().add(snkSym);
    this.verifyFlowOut(snkSym, copy, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < 100 }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < 100 }");
  }
  
  @Test
  public void testFlowOutParam2DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("[N] -> { S[i,j] : 0 <= i < N and 0 <= j < N }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    copy.getLiveOutSymbols().add(snkSym);
    this.verifyFlowOut(snkSym, copy, "[N] -> { snk[i,j] : 0 <= i < N and 0 <= j < N }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[N,i] -> { snk[i,j] : 0 <= j < N }");
  }
  
  @Test
  public void testFlowOutTriangleCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j] : 0 <= i < 100 and 0 <= j < i }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    copy.getLiveOutSymbols().add(snkSym);
    this.verifyFlowOut(snkSym, copy, "{ snk[i,j] : 0 <= i < 100 and 0 <= j < i }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < i }");
  }
  
  @Test
  public void testFlowOut3DCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopy("{ S[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", new long[] { 50, 200, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    copy.getLiveOutSymbols().add(snkSym);
    this.verifyFlowOut(snkSym, copy, "{ snk[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i,j] -> { snk[i,j,k] : 0 <= k < 100 }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop(), "[i] -> { snk[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }");
  }
  
  @Test
  public void testFlowOut2DDualCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j] : 0 <= i < 100 and 0 <= j < 100 }", 
      "{ S2[i,j] : 0 <= i < 100 and 0 <= j < 100 }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    final Function1<ScopStatement, Boolean> _function = (ScopStatement it) -> {
      String _id = it.getId();
      return Boolean.valueOf(Objects.equal(_id, "S1"));
    };
    final ScopNode innerLoop = IterableExtensions.<ScopStatement>findFirst(copy.listAllStatements(), _function).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < 100 }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop(), "{ snk[i,j] : 0 <= i < 100 and 0 <= j < 100 }");
  }
  
  @Test
  public void testFlowOutParam2DDualCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopyCopy("[N] -> { S1[i,j] : 0 <= i < N and 0 <= j < N }", 
      "[N] -> { S1[i,j] : 0 <= i < N and 0 <= j < N }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    ScopNode innerLoop = copy.listAllStatements().get(0).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[N,i] -> { snk[i,j] : 0 <= j < N }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop(), "[N] -> { snk[i,j] : 0 <= i < N and 0 <= j < N }");
  }
  
  @Test
  public void testFlowOutTriangleDualCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j] : 0 <= i < 100 and 0 <= j < 100 }", "{ S2[i,j] : 0 <= i < 100 and 0 <= j < i }", new long[] { 100, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    final Function1<ScopStatement, Boolean> _function = (ScopStatement it) -> {
      String _id = it.getId();
      return Boolean.valueOf(Objects.equal(_id, "S1"));
    };
    final ScopNode innerLoop = IterableExtensions.<ScopStatement>findFirst(copy.listAllStatements(), _function).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i] -> { snk[i,j] : 0 <= j < i }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop(), "{ snk[i,j] : 0 <= i < 100 and 0 <= j < i }");
  }
  
  @Test
  public void testFlowOut3DDualCopy() {
    final GecosScopBlock copy = MockGecosScopBlocks.mockCopyCopy("{ S1[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", "{ S2[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }", new long[] { 50, 200, 100 });
    final Symbol snkSym = copy.getScope().lookup("snk");
    final Function1<ScopStatement, Boolean> _function = (ScopStatement it) -> {
      String _id = it.getId();
      return Boolean.valueOf(Objects.equal(_id, "S1"));
    };
    final ScopNode innerLoop = IterableExtensions.<ScopStatement>findFirst(copy.listAllStatements(), _function).getParentScop();
    this.verifyFlowOut(snkSym, innerLoop, "[i,j] -> { snk[i,j,k] : 0 <= k < 100 }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop(), "[i] -> { snk[i,j,k] : i <= j < 150 + i and 0 <= k < 100 }");
    this.verifyFlowOut(snkSym, innerLoop.getParentScop().getParentScop(), "{ snk[i,j,k] : 0 <= i <= 50 and i <= j < 150 + i and 0 <= k < 100 }");
  }
  
  private void verifyFlowOut(final Symbol sym, final ScopNode node, final String ref) {
    Assert.assertNotNull("Cannot compute FlowOut on null symbol", sym);
    final JNIISLSet flowOut = ScopAccessAnalyzer.computeFlowOut(sym, node);
    final JNIISLSet flowOutRef = ISLFactory.islSet(ref);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Expected ");
    _builder.append(flowOutRef);
    _builder.append(" instead of ");
    _builder.append(flowOut);
    Assert.assertTrue(_builder.toString(), flowOut.isSubset(flowOutRef));
  }
}
