package fr.irisa.cairn.gecos.model.scop.tests;

import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIPtrBoolean;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;

@SuppressWarnings("all")
public class TestClosure {
  @Test
  public void testEx2() {
    final JNIISLDimType a = null;
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[] -> { ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("S4[i]->S1[k] : 0<=i<=100 and i-3<=k<=i+3 ;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("S1[i]->S0[k] : 0<=i<=100 and i-2<=k<=i+2 ;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("}");
    final JNIISLUnionMap dom = ISLFactory.islUnionMap(_builder.toString());
    final JNIPtrBoolean v = new JNIPtrBoolean();
    final JNIISLUnionMap r = dom.transitiveClosure(v);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("{ ");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("S4[i] -> S1[k] : 5 <= i <= 9 and -3 + i <= k <= 3 + i; ");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("S4[i] -> S0[k] : 5 <= i <= 9 and -5 + i <= k <= 5 + i; ");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("S1[i] -> S0[k] : 0 <= i <= 100 and -2 + i <= k <= 2 + i");
    _builder_1.newLine();
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.newLine();
    final String t = _builder_1.toString();
    InputOutput.<JNIISLUnionMap>println(r);
  }
}
