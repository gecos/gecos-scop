package fr.irisa.cairn.gecos.model.scop.tests;

import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionSet;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.junit.Test;

@SuppressWarnings("all")
public class TestCodegen {
  public void codegen(final JNIISLSet context, final JNIISLUnionMap umap) {
    JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context);
    JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, umap);
  }
  
  @Test
  public void test1() {
    JNIISLSet context = ISLFactory.islSet("[M,N]-> { : M>=1 & N >= 1 }");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[N,M] -> { ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("A[j] -> [2j] : 0 <= j <= M ;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("B[j] -> [j] : 0 <= j <= M ");
    _builder.newLine();
    _builder.append("}");
    JNIISLUnionMap umap = ISLFactory.islUnionMap(_builder.toString());
  }
  
  @Test
  public void testEx2() {
    final JNIISLUnionSet dom = ISLFactory.islUnionSet("[N,M] -> { S1[i,j] : 0<=i<N and 0<=j<M; S2[i,j] : 0<=i<N and 0<=j<M }");
    final JNIISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i]; S2[i,j] -> [i+1] }");
    final JNIISLSet context = ISLFactory.islSet("[M,N]-> { : }");
    final JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context);
    final JNIISLASTNode node = build.generateWithExpansionNodes(sch.intersectDomain(dom), "EX");
    System.out.println(node.toString());
  }
  
  @Test
  public void testEx3() {
    JNIISLSet context = ISLFactory.islSet("[]-> { : }");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{ ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("S1[i, j] -> [i,j,0] : 0 <= i <= 255 and 0 <= j <= 255; ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("S0[i, j] -> [i,j,1] : 0 <= i <= 255 and 0 <= j <= 255 ");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    JNIISLUnionMap sched = ISLFactory.islUnionMap(_builder.toString());
  }
}
