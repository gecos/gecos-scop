package fr.irisa.cairn.gecos.model.scop.tests;

import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLDimType;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import java.util.Collections;
import java.util.List;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.junit.Test;

@SuppressWarnings("all")
public class TestGuardGen {
  @Test
  public void testEx2() {
    final JNIISLDimType a = null;
    final JNIISLSet dom = ISLFactory.islSet("[N,M] -> { S[i,j] : 0<=i<N and 0<=j<M or N+4<=i<M }");
    ScopDimension _dim = ScopUserFactory.dim("M");
    ScopDimension _dim_1 = ScopUserFactory.dim("N");
    final List<ScopDimension> params = Collections.<ScopDimension>unmodifiableList(CollectionLiterals.<ScopDimension>newArrayList(_dim, _dim_1));
    ScopDimension _dim_2 = ScopUserFactory.dim("i");
    ScopDimension _dim_3 = ScopUserFactory.dim("j");
    final List<ScopDimension> idx = Collections.<ScopDimension>unmodifiableList(CollectionLiterals.<ScopDimension>newArrayList(_dim_2, _dim_3));
  }
}
