package fr.irisa.cairn.gecos.model.scop.tests;

import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTBuild;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLASTNode;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLUnionMap;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.junit.Test;

@SuppressWarnings("all")
public class TestSubstitute {
  public void codegen(final JNIISLSet context, final JNIISLUnionMap umap) {
    JNIISLASTBuild build = JNIISLASTBuild.buildFromContext(context);
    JNIISLASTNode root = JNIISLASTNode.buildFromSchedule(build, umap);
  }
  
  @Test
  public void testEx3() {
    JNIISLSet context = ISLFactory.islSet("[I,J]-> { : }");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[I,J] -> { ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("S0[i, j] -> S0[i,j] : I=2i and I=j; ");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    JNIISLUnionMap sched = ISLFactory.islUnionMap(_builder.toString());
  }
}
