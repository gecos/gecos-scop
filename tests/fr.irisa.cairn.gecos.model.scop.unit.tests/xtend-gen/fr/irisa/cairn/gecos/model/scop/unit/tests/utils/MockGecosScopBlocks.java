package fr.irisa.cairn.gecos.model.scop.unit.tests.utils;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.scop.GecosScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopBlock;
import fr.irisa.cairn.gecos.model.scop.ScopDimension;
import fr.irisa.cairn.gecos.model.scop.ScopFactory;
import fr.irisa.cairn.gecos.model.scop.ScopNode;
import fr.irisa.cairn.gecos.model.scop.ScopParameter;
import fr.irisa.cairn.gecos.model.scop.codegen.ISLCodegenUtils;
import fr.irisa.cairn.gecos.model.scop.factory.ScopUserFactory;
import fr.irisa.cairn.jnimap.isl.jni.ISLFactory;
import fr.irisa.cairn.jnimap.isl.jni.JNIISLSet;
import gecos.core.Symbol;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class MockGecosScopBlocks {
  public static GecosScopBlock mockCopy(final String copyDomain, final long[] sizes) {
    GecosScopBlock _xblockexpression = null;
    {
      final ScopBlock block = ScopFactory.eINSTANCE.createScopBlock();
      final GecosScopBlock root = ScopUserFactory.root(block);
      ScopUserFactory.setScope(root.getScope());
      GecosUserTypeFactory.setScope(root.getScope());
      final JNIISLSet copySet = ISLFactory.islSet(copyDomain);
      final Function1<String, ScopDimension> _function = (String it) -> {
        return ScopUserFactory.dim(it);
      };
      root.getIterators().addAll(ListExtensions.<String, ScopDimension>map(copySet.getIndicesNames(), _function));
      final Function1<String, ScopDimension> _function_1 = (String it) -> {
        ScopParameter _param = ScopUserFactory.param(it);
        return ((ScopDimension) _param);
      };
      root.getParameters().addAll(ListExtensions.<String, ScopDimension>map(copySet.getParametersNames(), _function_1));
      final Symbol srcSym = GecosUserCoreFactory.symbol("src", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT(), sizes.length, sizes), root.getScope());
      final Symbol snkSym = GecosUserCoreFactory.symbol("snk", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT(), sizes.length, sizes), root.getScope());
      ISLCodegenUtils.dataMover(copySet, block, srcSym, snkSym);
      _xblockexpression = root;
    }
    return _xblockexpression;
  }
  
  public static GecosScopBlock mockCopyCopy(final String copyDomain, final String copyBisDomain, final long[] sizes) {
    GecosScopBlock _xblockexpression = null;
    {
      final GecosScopBlock root = MockGecosScopBlocks.mockCopy(copyDomain, sizes);
      final Symbol snkSym = root.getScope().lookup("snk");
      final JNIISLSet copyBisSet = ISLFactory.islSet(copyBisDomain);
      final Symbol snkBisSym = GecosUserCoreFactory.symbol("snkBis", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT(), sizes.length, sizes), root.getScope());
      ScopNode _root = root.getRoot();
      ISLCodegenUtils.dataMover(copyBisSet, ((ScopBlock) _root), snkSym, snkBisSym);
      _xblockexpression = root;
    }
    return _xblockexpression;
  }
}
