#include <stdio.h>

void main() {
	int input[10];
	int output[10];
	int i, t;

	for (i = 0; i < 10; i++) {
		input[i] = i;
	}

	#pragma scop_data_motion (input, output)
	for (t = 0; t < 5; t++) {
		for (i = 0; i < 10; i++) {
			input[i] = input[i] + 1;
			output[i] = input[i];
		}
	}

	for (i = 0; i < 10; i++) {
		printf("%i:%i\n", i, output[i]);
	}
}
