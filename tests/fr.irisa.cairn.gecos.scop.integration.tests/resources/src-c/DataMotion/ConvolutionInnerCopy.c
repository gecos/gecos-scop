#include <stdio.h>

#define M 100
#define N 50

void main() {
	int input[M][N];
	int output[M][N];
	int i, j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			input[i][j] = (i + j) % 97;
			output[i][j] = 0;
		}
	}

	for (i = 1; i < M - 1; i++) {
#pragma scop_data_motion (input, output)
		for (j = 1; j < N - 1; j++) {
			output[i][j] = input[i - 1][j - 1] + input[i][j]
					+ input[i + 1][j + 1];
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
}
