#include <stdio.h>

int main();

int main() {
	int input[100][50];
	int output[100][50];
	int i;
	int j;
	int P_i;
	int P_j;

	for (i = 0; i <= 100 - 1; i = i + 1) {
		for (j = 0; j <= 50 - 1; j = j + 1) {
			input[i][j] = (i + j) % 256;
		}
	}
	for (P_i = 0; P_i <= 3; P_i = P_i + 1) {
		for (P_j = 0; P_j <= 1; P_j = P_j + 1) {
#pragma scop_data_motion (input, output)
			for (i = 32 * P_i; i <= 32 * P_i + 31; i = i + 1) {
				if (i >= 1 && i <= 98) {
					for (j = 32 * P_j; j <= 32 * P_j + 31; j = j + 1) {
						if (j >= 1 && j <= 48) {
							output[i][j] = input[i + 1][j + 1]
									+ input[i - 1][j - 1] + input[i][j];
						}
					}
				}
			}
		}
	}
	for (i = 1; i <= 100 - 1 - 1; i = i + 1) {
		for (j = 1; j <= 50 - 1 - 1; j = j + 1) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
	return 0;
}
