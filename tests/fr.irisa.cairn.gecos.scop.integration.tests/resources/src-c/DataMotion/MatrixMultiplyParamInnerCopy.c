#include <stdio.h>

#define M 100
#define N 50
#define O 80

void main() {
	int A[M][N];
	int B[N][O];
	int C[M][O];

	kernel(M, N, O, A, B, C);
}

void kernel(int m, int n, int o, int A[m][n], int B[n][o], int C[m][o]) {
	int i,j,k;
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			A[i][j] = (i + j) % 19;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < o; j++) {
			B[i][j] = (i * j) % 17;
		}
	}


	for (i = 0; i < m; i++) {
			#pragma scop_data_motion (A, B, C)
			for (j = 0; j < o; j++) {
				C[i][j] = 0;
				for (k = 0; k < n; k++) {
					C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	for (i = 0; i < m; i++) {
		for (j = 0; j < o; j++) {
			printf("%i,%i:%i\n", i, j, C[i][j]);
		}
	}
}
