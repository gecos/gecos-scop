#include <stdio.h>

#define M 100
#define N 50

void main() {
	int input[M][N];
	int output[M][N];

	kernel(M, N, input, output);
}

void kernel(int m, int n, int input[m][n], int output[m][n]) {
	int i,j;
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			input[i][j] = (i + j) % 19;
		}
	}

	#pragma scop_data_motion (input, output)
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			output[i][j] = input[i][j] + 1;
		}
	}

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
}
