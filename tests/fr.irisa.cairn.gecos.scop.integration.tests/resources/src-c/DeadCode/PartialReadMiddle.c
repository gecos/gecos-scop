#include <stdio.h>

#define M 100
#define N 50


void main() {
	int input[M];
	int output[M];
	int i;

	for (i = 0; i < M; i++) {
		input[i] = i % 19;
	}

	for (i = 0; i < N; i++) {
		output[i] = input[i + N/2] + 1;
	}

	for (i = 0; i < N; i++) {
		printf("%i:%i\n", i, output[i]);
	}
}
