#include <stdio.h>

#define M 100
#define N 50

void main() {
	int input[M];
	int temp[M];
	int output[M];
	int i,j;

	for (i = 0; i < M; i++) {
			input[i] = i % 19;
	}

	for (i = 0; i < M/N; i++) {
#pragma scop_duplicate i 4
#pragma scop_data_motion (input, output, temp)
		{
		for (j = i*N; j < (i+1)*N; j++) {
			temp[j] = input[j] + 1;
		}
		for (j = i*N; j < (i+1)*N; j++) {
			output[j] = temp[j];
		}
		}
	}

	for (i = 0; i < M; i++) {
		printf("%i:%i\n", i, output[i]);
	}
}
