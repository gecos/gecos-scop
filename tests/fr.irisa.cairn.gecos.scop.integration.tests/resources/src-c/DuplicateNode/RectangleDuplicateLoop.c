#include <stdio.h>

#define M 100
#define N 50


void main() {
	int input[M][N];
	int output[M][N];
	int i,j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			input[i][j] = (i + j) % 19;
		}
	}

	for (i = 0; i < M; i++) {
		#pragma scop_duplicate i 2
		#pragma scop_data_motion (input, output)
		for (j = 0; j < N; j++) {
			output[i][j] = input[i][j] + 1;
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
}
