#include <stdio.h>

#define M 100


void main() {
	int input[M][M];
	int output[M][M];
	int i,j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < i; j++) {
			input[i][j] = (i + j) % 19;
		}
	}

	for (i = 0; i < M; i++) {
		#pragma scop_duplicate i 4
		#pragma scop_data_motion (input, output)
		for (j = 0; j < i; j++) {
			output[i][j] = input[i][j] + 1;
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < i; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
}
