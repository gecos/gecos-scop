#include "typedef.h"

#define T	8

void dog(int M, int N, TYPE_0 inout[M][N], TYPE_0 inter[M][N]) {

	int i, j, t;

	for(t=0; t<= T; t++) {
		for (i = 1; i < M-1; i++)
			for (j = 0; j < N; j++)
				inter[i][j] = 0.25*inout[i-1][j] + 0.5*inout[i][j] + 0.25*inout[i+1][j];

		for (i = 1; i < M-1; i++)
			for (j = 1; j< N-1; j++)
				inout[i][j] = 0.25*inter[i][j-1] + 0.5*inter[i][j] + 0.25*inter[i][j+1];
	}
}
