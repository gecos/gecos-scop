#include "typedef.h"

void gauss(int M, int N, TYPE_0 in[M][N], TYPE_0 out[M][N], TYPE_0 inter[M][N]) {

	int i, j;

	for (i = 1; i < M-1; i++)
		for (j = 0; j < N; j++)
			inter[i][j] = 0.25*in[i-1][j] + 0.5*in[i][j] + 0.25*in[i+1][j];

	for (i = 1; i < M-1; i++)
		for (j = 1; j< N-1; j++)
			out[i][j] = 0.25*inter[i][j-1] + 0.5*inter[i][j] + 0.25*inter[i][j+1];
}
