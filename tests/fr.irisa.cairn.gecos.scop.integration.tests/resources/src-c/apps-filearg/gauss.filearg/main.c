#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "typedef.h"

#define MACRO_VAL(macro) 	#macro
#define TYPE2STR(type)		MACRO_VAL(type)
#define PRINT_ID(type)		(strcmp(TYPE2STR(type), "float")? "%d" : "%f")


extern void gauss(int M, int N, TYPE_0 in[M][N], TYPE_0 out[M][N], TYPE_0 inter[M][N]);


int main(int argc, const char **argv) {
	int i,j;
	int N, M, nDims;
	FILE *infile;
	char tmp[128];

	/** check arguments **/
	if(argc < 2) {
		fprintf(stderr, "Usage: %s input_file\n", argv[0]);
		exit(-1);
	}

	infile = fopen(argv[1], "r");
	if(infile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	fgets(tmp, 128, infile);
	nDims = atoi(tmp);
	if(nDims != 2) {
		fprintf(stderr, "nb of dimensions in '%s' (%d) is different from 2\n", argv[1], nDims);
		exit(-3);
	}
	
	fgets(tmp, 128, infile);
	M = atoi(tmp);

	fgets(tmp, 128, infile);
	N = atoi(tmp);
	if(M <= 0 || N <= 0) {
		fprintf(stderr, "invalid nb of input samples in '%s'\n", argv[1]);
		exit(-3);
	}

	/* init data */
	TYPE_0 (*in)[N] = malloc(M*N*sizeof(TYPE_0));
	TYPE_0 (*out)[N] = malloc(M*N*sizeof(TYPE_0));
	TYPE_0 (*inter)[N] = malloc(M*N*sizeof(TYPE_0));


	for(i=0; i<M; i++) {
		for(j=0; j<N; j++) {
			fgets(tmp, 128, infile);
			in[i][j] = (TYPE_0) (strcmp(TYPE2STR(TYPE_0), "float")? strtol(tmp, NULL, 10) : atof(tmp));
			out[i][j] = 0;
			inter[i][j] = 0;
		}
	}
	fclose(infile);


	gauss(M, N, in, out, inter);


	/* output result */
	printf("2\n");
	printf("%d\n%d\n", M, N);
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			if(i < 7 || i > M-8 || j < 2 || j > N-2)
				printf(PRINT_ID(TYPE_0), 0);
			else
				printf(PRINT_ID(TYPE_0), out[i][j]);
			printf("\n");
		}
	}

	free(in);
	free(out);
	free(inter);

	return 0;
}
