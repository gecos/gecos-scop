#include <stdio.h>

#define T 20
#define M 100

int main() {
	int data[T][M];
	int t, i;

	for (i = 0; i < M; i++) {
		data[0][i] = i % 59;
	}

	for (t = 1; t < T; t++) {
		data[t][0] = 0;
		data[t][M - 1] = 0;
		for (i = 1; i < M - 1; i++) {
			data[t][i] = (data[t - 1][i - 1] + data[t - 1][i]
					+ data[t - 1][i + 1]) / 3;
		}
	}

	for (i = 0; i < M; i++) {
		printf("%i:%i\n", i, data[T - 1][i]);
	}
	return 0;
}
