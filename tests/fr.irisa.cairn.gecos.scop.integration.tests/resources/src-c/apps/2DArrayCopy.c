#include <stdio.h>

#define M 128
#define N 60

int main() {
	int input[M][N];
	int temp[M][N];
	int output[M][N];
	int i,j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			input[i][j] = (i+j) % 19;
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			temp[i][j] = input[i][j] + 1;
		}
	}
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			output[i][j] = temp[i][j];
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
	return 0;
}
