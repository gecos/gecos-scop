#include <stdio.h>

#define M 100
#define N 60
#define O 20
#define P 15

int main() {
	int input[M][N];
	int temp[M][N];
	int output[M][N];
	int i,j,k,l,min;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			input[i][j] = (i+j) % 19;
		}
	}

	for (i = 0; i < M/O; i++) {
		for (j = 0; j < N/P; j++) {
			for (k = i*O; k < (i+1)*O; k++) {
				for (l = j*P; l < (j+1)*P; l++) {
					temp[k][l] = input[k][l] + 1;
				}
			}
			for (k = i*O; k < (i+1)*O; k++) {
				for (l = j*P; l < (j+1)*P; l++) {
					output[k][l] = temp[k][l];
				}
			}
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%i,%i:%i\n", i, j, output[i][j]);
		}
	}
	return 0;
}
