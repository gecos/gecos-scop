#include <stdio.h>

#define M 20

void main() {
	int output[M];
	int i;

	for (i = 0; i < M; i++) {
		int tmp = 3;
		output[i] = tmp + i;
	}

	for (i = 0; i < M; i++) {
		printf("%i:%f\n", i, output[i]);
	}
}
