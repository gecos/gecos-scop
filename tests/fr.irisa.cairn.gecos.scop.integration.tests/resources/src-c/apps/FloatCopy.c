#include <stdio.h>

#define M 100

int main() {
	float output[M];
	int i;

	for (i = 0; i < M; i++) {
		output[i] = 0.5 * i;
	}

	for (i = 0; i < M; i++) {
		printf("%i:%f\n", i, output[i]);
	}
	return 0;
}
