#include <stdio.h>

#define M 20

int read(int i) {
	return i % 7;
}

int main() {
	int input[M];
	int output[M];
	int i;

	for (i = 0; i < M; i++) {
		input[i] = read(i);
	}

	for (i = 0; i < M; i++) {
		output[i] = input[i];
	}

	for (i = 0; i < M; i++) {
		printf("%i:%f\n", i, output[i]);
	}

	return 0;
}
