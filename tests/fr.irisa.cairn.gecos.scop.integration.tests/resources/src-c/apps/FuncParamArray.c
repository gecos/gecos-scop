#include <stdio.h>

#define N 10

void func(int in[N], int out[N]) {
	int i;
	for (i = 0; i < N; i++) {
		out[i] = in[i];
	}
}


void main() {
	int input[N];
	int output[N];
	int i;

	for (i = 0; i < N; i++) {
		input[i] = i % 7;
	}

	func(input, output);

	for (i = 0; i < N; i++) {
		printf("%i:%i\n", i, output[i]);
	}
}
