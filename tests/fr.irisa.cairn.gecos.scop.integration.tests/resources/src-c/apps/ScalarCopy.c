#include <stdio.h>

#define M 20

void main() {
	int input[M];
	int output[M];
	int i;

	for (i = 0; i < M; i++) {
		input[i] = i % 19;
	}

	for (i = 0; i < M; i++) {
		int tmp = input[i];
		output[i] = tmp;
	}

	for (i = 0; i < M; i++) {
		printf("%i:%f\n", i, output[i]);
	}
}
