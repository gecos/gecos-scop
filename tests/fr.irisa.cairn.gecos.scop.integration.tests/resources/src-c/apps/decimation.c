#include <stdio.h>

#define INPUT_SIZE 100
#define OUTPUT_SIZE 25 - 1


int main() {
	int input[INPUT_SIZE];
	int output[OUTPUT_SIZE];
	int i;

	for (i = 0; i < INPUT_SIZE; i++) {
		input[i] = i % 19;
	}

	int tmp[INPUT_SIZE];
	for (i = 2; i < INPUT_SIZE - 2; i++) {
		tmp[i] = input[i - 2] + input[i - 1] + input[i] + input[i + 1] + input[i + 2];
	}
	for (i = 0; i < OUTPUT_SIZE; i++) {
		output[i] = tmp[i * 4 + 2];
	}
	for (i = 0; i < OUTPUT_SIZE; i++) {
		printf("%i,", output[i]);
	}
	return 0;
}
