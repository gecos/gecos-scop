#include <stdio.h>

#define N 100
#define T 10

void main() {
	int data[N];
	int i,t;

	for (i = 0; i < N; i++) {
		data[i] = 1 + i % 61;
	}

#pragma scop_slice sizes=(32,32) unrolls=(1,2)
	for (t = 0; t < T; t++) {
		for (i = N - T + t; i > t; i--) {
			data[i] = data[i - 1];
		}
	}

	for (i = 0; i < N; i++) {
		printf("%i,", data[i]);
	}
}
