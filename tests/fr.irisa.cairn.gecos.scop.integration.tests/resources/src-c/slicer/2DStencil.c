#include <stdio.h>

#define T 20
#define M 100
#define N 50

void main() {
	int data[T][M][N];
	int t, i, j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			data[0][i][j] = (i + j) % 59;
		}
	}

#pragma scop_slice sizes=(10,20,20) unrolls=(1,2,2)
	for (t = 1; t < T; t++) {
		for (i = 0; i < M; i++) {
			data[t][i][0] = 0;
			data[t][i][N - 1] = 0;
		}
		for (j = 1; j < N - 1; j++) {
			data[t][0][j] = 0;
			data[t][M - 1][j] = 0;
		}
		for (i = 1; i < M - 1; i++) {
			for (j = 1; j < N - 1; j++) {
				data[t][i][j] = (data[t - 1][i - 1][j - 1]
						+ data[t - 1][i - 1][j] + data[t - 1][i - 1][j + 1]
						+ data[t - 1][i][j - 1] + data[t - 1][i][j + 1]
						+ data[t - 1][i + 1][j - 1] + data[t - 1][i + 1][j]
						+ data[t - 1][i + 1][j + 1]) / 9;

			}
		}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%i,%i:%i\n", i, j, data[T - 1][i][j]);
		}
	}
}
