#include <stdio.h>

#define M 640
#define N 480

float input[M][N];
float output[M][N];
int Ix[M][N];
int Iy[M][N];
int Ixx[M][N];
int Ixy[M][N];
int Iyy[M][N];
int Sxx[M][N];
int Sxy[M][N];
int Syy[M][N];
int d[M][N];
int trace[M][N];

int main() {
	int i,j;

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			input[i][j] = (i * j) % 101;
		}
	}

#pragma scop_slice sizes=(32,32) unrolls=(2,2)
	{
	for (i = 0; i <= M-1; i = i + 1) {
		for (j = 0; j <= N-1; j = j + 1) {
			Ix[i][j] = 0;
			if (i > 1 && i < M-2) {
				Ix[i][j] = input[i - 1][j] - input[i + 1][j];
			}
		}
	}
	for (i = 0; i <= M-1; i = i + 1) {
		for (j = 0; j <= N-1; j = j + 1) {
			Iy[i][j] = 0;
			if (j > 1 && j < N-2) {
				Iy[i][j] = input[i][j - 1] - input[i][j + 1];
			}
		}
	}
	for (i = 0; i <= M-1; i = i + 1) {
		for (j = 0; j <= N-1; j = j + 1) {
			Ixx[i][j] = Ix[i][j] * Ix[i][j];
			Ixy[i][j] = Ix[i][j] * Iy[i][j];
			Iyy[i][j] = Iy[i][j] * Iy[i][j];
		}
	}
	for (i = 0; i <= M-1; i = i + 1) {
		for (j = 0; j <= N-1; j = j + 1) {
			Sxx[i][j] = 0.0;
			Sxy[i][j] = 0.0;
			Syy[i][j] = 0.0;
			if (j > 1 && j < N-2 && i > 1 && i < M-2) {
				Sxx[i][j] = 1.0 / 16 * Ixx[i - 1][j - 1]
						+ 1.0 / 8 * Ixx[i][j - 1]
						+ 1.0 / 16 * Ixx[i + 1][j - 1]
						+ 1.0 / 8 * Ixx[i - 1][j] + 1.0 / 4 * Ixx[i][j]
						+ 1.0 / 8 * Ixx[i + 1][j]
						+ 1.0 / 16 * Ixx[i - 1][j + 1]
						+ 1.0 / 8 * Ixx[i][j + 1]
						+ 1.0 / 16 * Ixx[i + 1][j + 1];
				Sxy[i][j] = 1.0 / 16 * Ixy[i - 1][j - 1]
						+ 1.0 / 8 * Ixy[i][j - 1]
						+ 1.0 / 16 * Ixy[i + 1][j - 1]
						+ 1.0 / 8 * Ixy[i - 1][j] + 1.0 / 4 * Ixy[i][j]
						+ 1.0 / 8 * Ixy[i + 1][j]
						+ 1.0 / 16 * Ixy[i - 1][j + 1]
						+ 1.0 / 8 * Ixy[i][j + 1]
						+ 1.0 / 16 * Ixy[i + 1][j + 1];
				Syy[i][j] = 1.0 / 16 * Iyy[i - 1][j - 1]
						+ 1.0 / 8 * Iyy[i][j - 1]
						+ 1.0 / 16 * Iyy[i + 1][j - 1]
						+ 1.0 / 8 * Iyy[i - 1][j] + 1.0 / 4 * Iyy[i][j]
						+ 1.0 / 8 * Iyy[i + 1][j]
						+ 1.0 / 16 * Iyy[i - 1][j + 1]
						+ 1.0 / 8 * Iyy[i][j + 1]
						+ 1.0 / 16 * Iyy[i + 1][j + 1];
			}
		}
	}
	for (i = 0; i <= M-1; i = i + 1) {
		for (j = 0; j <= N-1; j = j + 1) {
			d[i][j] = Sxx[i][j] * Syy[i][j] - Sxy[i][j] * Sxy[i][j];
			trace[i][j] = Sxx[i][j] + Syy[i][j];
			output[i][j] = d[i][j] - 0.04 * trace[i][j] * d[i][j];
		}
	}
	}

	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%f;",output[i][j]);
		}
		printf("\n");
	}

	return 0;
}
