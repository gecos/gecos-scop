package fr.irisa.cairn.gecos.scop.integration.tests.utils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.data.AbstractCxxData;
import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

public class FileArgData extends AbstractCxxData {

	private static final String FILE_ARG_DATA_DIRNAME = "^\\S+\\.filearg$";

	public static boolean isValidPath(Path dir) {
		return dir != null && Files.isDirectory(dir) 
				&& dir.getFileName().toString().matches(FILE_ARG_DATA_DIRNAME);
	}
	
	public static FileArgData createFromPath(Path dir) throws InvalidDataException {
		if(!isValidPath(dir))
			throw new InvalidDataException("Not a valid '" + FileArgData.class.getSimpleName() + "' data: " + dir);
	
		FileArgData data = new FileArgData();
		data.path = dir;
		data.srcFiles = DataProviderUtils.lookupFiles(dir, FileNamePatterns.CXX_SRC).collect(Collectors.toList());
		if(data.srcFiles.isEmpty())
			throw new InvalidDataException("Group directory of '" + FileArgData.class.getSimpleName() 
					+ "' data must contain at least one source file: " + dir);
		data.incDirs = DataProviderUtils.lookupDirsContainingFile(dir, FileNamePatterns.HEADERFILE_NAME_PATTERN)
				.collect(Collectors.toList());
		return data;
	}
	
}
