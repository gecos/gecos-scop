package fr.irisa.cairn.gecos.scop.integration.tests.utils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.data.AbstractCxxData;
import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

/**
 * Assumes that each polybench application is contained in a directory 'DIR', that:
 * <li> is not named 'utilities'
 * <li> contains a source file named 'DIR.c'
 * <li> contains a header file named 'DIR.h'
 * 
 * @author aelmouss
 */
public class PolybenchData extends AbstractCxxData {


	public static boolean isValidPath(Path dir) {
		return dir != null 
				&& Files.isDirectory(dir) 
				&& ! dir.getFileName().toString().equals("utilities")
				&& DataProviderUtils.lookupFiles(dir, 1, "^"+dir.getFileName()+".c$").findFirst().isPresent()
				&& DataProviderUtils.lookupFiles(dir, 1, "^"+dir.getFileName()+".h$").findFirst().isPresent();
	}
	
	public static PolybenchData createFromPath(Path dir) throws InvalidDataException {
		if(!isValidPath(dir))
			throw new InvalidDataException("Not a valid '" + PolybenchData.class.getSimpleName() + "' data: " + dir);
	
		PolybenchData data = new PolybenchData();
		data.path = dir;
		data.srcFiles = DataProviderUtils.lookupFiles(dir, 1, "^"+dir.getFileName()+".c$").collect(Collectors.toList());
		if(data.srcFiles.isEmpty())
			throw new InvalidDataException("Group directory of '" + PolybenchData.class.getSimpleName() 
					+ "' data must contain at least one source file: " + dir);
		data.incDirs = Arrays.asList(dir);
		return data;
	}
	
}
