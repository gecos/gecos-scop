package fr.irisa.cairn.gecos.scop.integration.tests.utils;

import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.customDataConvertor;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.versionCopier;
import static fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils.setProjectSourceFiles;
import static fr.irisa.cairn.gecos.testframework.s2s.Comparators.stderrEquals;
import static fr.irisa.cairn.gecos.testframework.s2s.Comparators.stdoutEquals;
import static fr.irisa.cairn.gecos.testframework.s2s.Operators.defaultExecutor;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.COMPILE;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CONVERT;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.LINK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.RUN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.TRANSFORM;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.VERIFY;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.chain;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.convert;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEachPairWithFirst;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.filter;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.join;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import fr.irisa.cairn.gecos.core.testframework.impl.GSDefaultTestFlows;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.modules.AddIncludeDirToGecosProject;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersionFactory;
import fr.irisa.cairn.gecos.testframework.s2s.Linkers;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.cairn.gecos.testframework.utils.ResourceLocator;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.remove.RemoveScopModule;
import gecos.gecosproject.GecosProject;


/**
 * @author aelmouss
 */
public class ScopTestTemplate extends GecosS2STestTemplate<GSProjectVersion> {

	protected static final ResourceLocator locator = new ResourceLocator(ScopTestTemplate.class, "resources/");

	protected static final Path POLYBENCH_UTILITIES_DIR = locator.locate("resources/src-c/polybench-c-4.2.1-beta/utilities").toAbsolutePath();
	
	protected List<Path> inputFiles;
	
	@Override
	protected void configure() {
		super.configure();
		
		/** CxxFileData test flow **/
		findTestFlow(CxxFileData.class)
			.addStageAfterLast(TRANSFORM, Stages.<GSProjectVersion>forEach(v -> v.setEnableStdRedirection(true)))
			.replaceStage(VERIFY, forEachPairWithFirst(stdoutEquals()));
		
		/** FileArgData test flow **/
		inputFiles = Arrays.asList(
			locator.locate("resources/src-c/apps-filearg/inputs/images/lena_128x128.input")
		);
		IVersionFactory<GSProjectVersion> versionFactory = GSProjectVersion::new;
		Predicate<Path> sourceFilter = s -> !s.getFileName().toString().startsWith("main.");
		registerTestFlow(FileArgData.class, GSDefaultTestFlows.cxxFileDataTestFlow()
			.replaceStage(CONVERT, convert(customDataConvertor(versionFactory,
					(data, proj) -> setProjectSourceFiles(proj, filter(data.getSourceFiles(), sourceFilter)), 
					(data, v) -> v.setExternalSources(filter(data.getSourceFiles(), sourceFilter.negate())))))
			.replaceStage(TRANSFORM, Stages.transform(versionCopier(versionFactory, v -> 
					v.setExternalSources(v.getPrevious().getExternalSourceFiles()))))
			.addStageAfterLast(TRANSFORM, chain(
					Stages.<GSProjectVersion>forEach(v -> v.setEnableStdRedirection(true)),
					forEach(v -> v.setTimeout(50L))))
			.replaceStage(RUN, chain(inputFiles.stream() //TODO redirect stdout to a different file for each input
					.map(i -> forEach(defaultExecutor(v -> asList(i.toString()))))
					.toArray(ITestStage[]::new)))
			.replaceStage(VERIFY, forEachPairWithFirst(stdoutEquals()))); //TODO compare all output files

		/** PolybechData test flow **/
		List<String> macros = asList("POLYBENCH_TIME", "POLYBENCH_DUMP_ARRAYS", "MEDIUM_DATASET");
		registerTestFlow(PolybenchData.class, GSDefaultTestFlows.cxxFileDataTestFlow()
			.replaceStage(CONVERT, convert(customDataConvertor(versionFactory,
					(d,p) -> {
						new AddIncludeDirToGecosProject(p, POLYBENCH_UTILITIES_DIR.toString()).compute();
						macros.forEach(m -> p.getMacros().put(m,""));
					},
					(d,v) -> v.setExternalSources(asList(POLYBENCH_UTILITIES_DIR.resolve("polybench.c"))))))
			.addStageAfterLast(TRANSFORM, chain(
					Stages.<GSProjectVersion>forEach(v -> v.setEnableStdRedirection(true)),
					forEach(v -> v.setTimeout(50L))))
			.replaceStage(COMPILE, NOP)
			.replaceStage(LINK, forEach(Linkers.gcc()
					.changeSourcesProvider(v -> concat(v.getGeneratedSourceFiles().stream(), v.getExternalSourceFiles().stream()).collect(toList()))
					.changeFlagsProvider(v -> join(macros, "-D"::concat))
					.changeLibsProvider(v -> asList("m"))))
			.replaceStage(VERIFY, forEachPairWithFirst(stderrEquals())));
	}
	
	protected void transform(GSProjectVersion v, Consumer<GecosProject> scopTransformation) {
		GecosProject p = v.getProject();
		new ScopExtractorPass(p).compute();
		
		scopTransformation.accept(p);
		
		new RemoveScopModule(p).compute();
	}

}
