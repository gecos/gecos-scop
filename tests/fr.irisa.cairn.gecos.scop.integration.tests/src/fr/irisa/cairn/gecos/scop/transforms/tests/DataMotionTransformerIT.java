package fr.irisa.cairn.gecos.scop.transforms.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionPass;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.ScopPragmaParser;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.model.IData;

public class DataMotionTransformerIT extends ScopTestTemplate {
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/DataMotion", dataClasses = CxxFileData.class)
	@ResourcesLocation(value = "resources/src-c/DuplicateNode", dataClasses = CxxFileData.class)
	public void testDataMotionTransformer(IData d) {
		runTest(d, v -> transform(v, p -> {
			new ScopPragmaParser(p).compute();
			new DataMotionPass(p).compute();
		}));
	}
}
