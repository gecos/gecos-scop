package fr.irisa.cairn.gecos.scop.transforms.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.layout.ApplyMemoryLayout;
import fr.irisa.cairn.gecos.model.scop.layout.FindArrayContractionCandidates;
import fr.irisa.cairn.gecos.model.scop.layout.ScopArrayContract;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.model.IData;
import gecos.gecosproject.GecosProject;

public class ScopArrayContractionIT extends ScopTestTemplate {
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/DeadCode", dataClasses = CxxFileData.class)
	@ResourcesLocation(value = "resources/src-c/apps", dataClasses = CxxFileData.class)
	public void testScopArrayContraction(IData d) {
		//FIXME temporarily disabled : presence of dangling references !!!
		findTestFlow(d.getClass()).replaceStage(CHECK, NOP);
		runTest(d, v -> transform(v, p -> {transforms(p, false);}));
	}

	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/DeadCode", dataClasses = CxxFileData.class)
	@ResourcesLocation(value = "resources/src-c/apps", dataClasses = CxxFileData.class)
	public void testScopArrayContractionPowerOfTwo(IData d) {
		//FIXME temporarily disabled : presence of dangling references !!!
		findTestFlow(d.getClass()).replaceStage(CHECK, NOP);
		runTest(d, v -> transform(v, p -> {transforms(p, true);}));
	}
	
	private void transforms(GecosProject p, boolean usePowerOfTwo) {
		new FindArrayContractionCandidates(p).compute();
		new ScopArrayContract(p, usePowerOfTwo).compute();
		new ApplyMemoryLayout(p).compute();
	}
}
