package fr.irisa.cairn.gecos.scop.transforms.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.transforms.ScopDeadCodeElimination;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.FileArgData;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;

public class ScopDeadCodeEliminationIT extends ScopTestTemplate {
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
//	@ResourcesLocation(value = "resources/src-c/polybench-c-4.2.1-beta", dataClasses = PolybenchData.class)
	@ResourcesLocation(value = "resources/src-c/apps-filearg", dataClasses = FileArgData.class)
	@ResourcesLocation(value = "resources/src-c/DeadCode", dataClasses = CxxFileData.class)
	@ResourcesLocation(value = "resources/src-c/apps", dataClasses = CxxFileData.class)
	public void testDeadCodeElimination(ICxxProjectData d) {
		findTestFlow(d.getClass())
		.replaceStage(CHECK, NOP) //FIXME temporarily disabled : presence of dangling references !!!
		;
		runTest(d, v -> transform(v, p -> new ScopDeadCodeElimination(p).compute()));
	}
}
