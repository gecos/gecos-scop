package fr.irisa.cairn.gecos.scop.transforms.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.datamotion.DataMotionPass;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.ScopPragmaParser;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopDuplicateNode;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.model.IData;

public class ScopDuplicateNodeIT extends ScopTestTemplate {
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/DuplicateNode", dataClasses = CxxFileData.class)
	public void testDuplicateNode(IData d) {
		findTestFlow(d.getClass())
		.replaceStage(CHECK, NOP) //FIXME temporarily disabled : presence of dangling references !!!
		;
		runTest(d, v -> transform(v, p -> {
			new ScopPragmaParser(p).compute();
			new ScopDuplicateNode(p).compute();
		}));
	}
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/DuplicateNode", dataClasses = CxxFileData.class)
	public void testDuplicateNodeAndDataMotion(IData d) {
		findTestFlow(d.getClass())
		.replaceStage(CHECK, NOP) //FIXME temporarily disabled : presence of dangling references !!!
		;
		runTest(d, v -> transform(v, p -> {
			new ScopPragmaParser(p).compute();
			new DataMotionPass(p).compute();
			new ScopDuplicateNode(p).compute();
		}));
	}
}
