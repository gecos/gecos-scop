package fr.irisa.cairn.gecos.scop.transforms.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.query.ScopAccessQuery;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.FileArgData;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.PolybenchData;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.model.gecos.scop.extractor.ScopExtractorPass;
import fr.irisa.cairn.model.gecos.scop.remove.RemoveScopModule;
import gecos.gecosproject.GecosProject;


/**
 * Tests {@link ScopExtractorPass} and {@link RemoveScopModule}
 * 
 * @author aelmouss
 */
public class ScopExtractorIT extends ScopTestTemplate {

	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/polybench-c-4.2.1-beta", dataClasses = PolybenchData.class)
	@ResourcesLocation(value = "resources/src-c/apps-filearg", dataClasses = FileArgData.class)
	@ResourcesLocation(value = "resources/src-c/apps", dataClasses = CxxFileData.class)
	public void testScopExtract(ICxxProjectData d) {
		findTestFlow(d.getClass())
			.replaceStage(CHECK, NOP) //FIXME temporarily disabled : presence of dangling references !!!
			;
		
		runTest(d, v -> {
			GecosProject p = v.getProject();
			assumeTrue("WARNING: original gProject (before ScopExtraction) contains SCoPs! skipping", ScopAccessQuery.getAllGecosScopBlocks(p).isEmpty());
			
			new ScopExtractorPass(p).compute();
			assertTrue("ScopExtractorPass did not extract any SCoPs", !ScopAccessQuery.getAllGecosScopBlocks(p).isEmpty());
			//TODO if PolybenchData, check for pragma scop
			
			new RemoveScopModule(p).compute();
			assertTrue("RemoveScopModule left some SCoPs behind!", ScopAccessQuery.getAllGecosScopBlocks(p).isEmpty());
		});
	}

}
