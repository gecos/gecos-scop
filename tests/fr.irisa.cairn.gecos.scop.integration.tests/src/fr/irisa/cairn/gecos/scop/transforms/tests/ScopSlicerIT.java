package fr.irisa.cairn.gecos.scop.transforms.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.scop.codegen.ScopISLCodegen;
import fr.irisa.cairn.gecos.model.scop.pragma.extractor.ScopPragmaParser;
import fr.irisa.cairn.gecos.model.scop.sheduling.ScopSlicerPass;
import fr.irisa.cairn.gecos.model.scop.transforms.ScopIteratorNormalizerPass;
import fr.irisa.cairn.gecos.scop.integration.tests.utils.ScopTestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.model.IData;

public class ScopSlicerIT extends ScopTestTemplate {
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/slicer", dataClasses = CxxFileData.class)
	public void testScopSlicer(IData d) {
		//FIXME temporarily disabled : presence of dangling references !!!
		findTestFlow(d.getClass()).replaceStage(CHECK, NOP);
		runTest(d, v -> transform(v, p -> {
			new ScopPragmaParser(p).compute();
			new ScopSlicerPass(p).compute();
			new ScopIteratorNormalizerPass(p).compute();
			new ScopISLCodegen(p).compute();
		}));
	}
}
